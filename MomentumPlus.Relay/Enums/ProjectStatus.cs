﻿using System.ComponentModel;

namespace MomentumPlus.Relay
{
    public enum ProjectStatus
    {
        [Description("Not Started")]
        NotStarted = 0,

        [Description("Active")]
        Active = 1,

        [Description("Complete")]
        Complete = 2
    }
}
