﻿using System.ComponentModel;

namespace MomentumPlus.Relay.Models
{
    public enum RotationType
    {
        [Description("Swing")]
        Swing = 0,

        [Description("Shift")]
        Shift = 1
    }
}
