﻿namespace MomentumPlus.Relay
{
    public enum TimeLinePeriodType
    {
        DayOn = 0,
        DayOff = 1
    }
}
