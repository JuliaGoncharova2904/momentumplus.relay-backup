﻿using System.ComponentModel;

namespace MomentumPlus.Relay.Models
{
    public enum ModuleType
    {
        [Description("HSE")]
        HSE = 0,
        [Description("Core")]
        Core = 1,
        [Description("Projects")]
        Project = 2,
        [Description("Team")]
        Team = 3,
        [Description("Daily Notes")]
        DailyNote = 4
    }
}
