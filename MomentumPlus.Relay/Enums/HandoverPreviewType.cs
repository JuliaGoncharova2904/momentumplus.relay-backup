﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public enum HandoverPreviewType
    {
        [Description("Penultimate Day Of Swing")]
        [Display(Name = "Penultimate Day Of Swing")]
        PenultimateDayOfSwing = 0
    }
}
