﻿using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IProjectService
    {
        [Obsolete]
        IEnumerable<SelectListItem> GetProjectList();

        [Obsolete]
        string GetProjectName(Guid projectId);

        ProjectDialogViewModel PopulateEditProjectModel();
        ProjectDialogViewModel PopulateEditProjectModel(ProjectDialogViewModel model);

        ProjectDialogViewModel GetProject(Guid projectId);
        Guid AddProject(ProjectDialogViewModel model, Guid creatorId);
        void UpdateProject(ProjectDialogViewModel model);

        ProjectSlideViewModel GetProjectSlide(Guid projectId);
        ProjectFullSlideViewModel GetFullProjectSlide(Guid projectId);

        IEnumerable<ProjectSlideViewModel> GetProjectsSlides();
        IEnumerable<ProjectSlideViewModel> GetProjectsSlidesForLineManager(Guid lineManagerId);
        IEnumerable<ProjectSlideViewModel> GetProjectsSlidesForProjectManager(Guid projectManagerId);

        ProjectTeamPanelViewModel GetProjectFollowers(Guid projectid, int page, int pageSize);
        ProjectFollowersTabViewModel GetTeamsMembersForProject(Guid projectid, string searchQuery);
        void UpdateFollowersOfProject(ProjectFollowersTabViewModel model);

        ProjectsTeamsTabViewModel GetTeamsForProject(Guid projectId);
        void UpdateTeamsForProject(ProjectsTeamsTabViewModel model);

        ProjectTasksPanelViewModel GetPlannedTasksForProject(Guid userId, Guid projectId, int page, int pageSize, ProjectTasksFilterOptions filterOptions, ProjectTasksSortOptions sortOptions);
        ProjectTasksPanelViewModel GetAdHocTasksForProject(Guid projectId, int page, int pageSize, ProjectTasksFilterOptions filterOptions, ProjectTasksSortOptions sortOptions);
        ProjectTaskTopicViewModel GetProjectTask(Guid userId, Guid taskId);
        ProjectTaskTopicViewModel UpdateProjectTask(Guid userId, ProjectTaskTopicViewModel taskModel);
        IEnumerable<SelectListItem> GetProjectFollowerForAssigning(Guid projectId);

        ProjectAdminAddTaskDialogViewModel PopulateAddTaskModel(Guid userId, Guid projectId);
        void AddTask(ProjectAdminAddTaskDialogViewModel task);
        ProjectAdminEditTaskDialogViewModel PopulateEditTaskModel(Guid userId, Guid taskId);
        void UpdateTask(ProjectAdminEditTaskDialogViewModel model);
    }
}
