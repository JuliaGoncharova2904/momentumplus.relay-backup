﻿using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ISiteService
    {
        IEnumerable<SiteViewModel> GetAllSites();

        IEnumerable<SelectListItem> GetSitesList();

        SiteViewModel GetSiteForPosition(Guid positionId);

        bool AddSite(SiteViewModel site);

        SiteViewModel GetSite(Guid siteId);

        bool UpdateSite(SiteViewModel site);
    }
}
