﻿using System;

namespace MomentumPlus.Relay.Interfaces
{
    public interface IMailerService
    {
        void SendHandoverPreviewEmail(Guid rotationId);

        void SendShiftHandoverPreviewEmail(Guid shiftId);

        void SendForgotPasswordEmail(string to, string toName, string resetUrl);

        void SendRotationShareReportEmail(Guid rotationId);
    }
}