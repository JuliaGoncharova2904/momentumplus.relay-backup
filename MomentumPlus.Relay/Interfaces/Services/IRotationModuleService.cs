﻿using System;
using System.Collections.Generic;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IRotationModuleService
    {
        void ChangeTemplateModuleChildRotationModulesStatus(Guid templateModuleId, bool status);

        void ChangeRotationModuleStatus(Guid moduleId, bool status);

        IEnumerable<HSETopicViewModel> GetHseModuleTopics(Guid moduleOwnerId, ModuleSourceType sourceType,
            RotationType rotationType = RotationType.Swing, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems);

        IEnumerable<CoreTopicViewModel> GetCoreModuleTopics(Guid moduleOwnerId, ModuleSourceType sourceType,
            RotationType rotationType = RotationType.Swing, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems);

        IEnumerable<TeamTopicViewModel> GetTeamModuleTopics(Guid moduleOwnerId, ModuleSourceType sourceType,
            RotationType rotationType = RotationType.Swing, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems);

        IEnumerable<ProjectsTopicViewModel> GetProjectModuleTopics(Guid moduleOwnerId, ModuleSourceType sourceType,
            RotationType rotationType = RotationType.Swing, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems);

        bool IsModuleEnabled(ModuleType moduleType, Guid moduleOwnerId, ModuleSourceType sourceType, RotationType rotationType = RotationType.Swing);

        int ModuleTopicsCount(Guid rotationId, ModuleType moduleType, ModuleSourceType sourceType, RotationType rotationType = RotationType.Swing);

        int ModuleTasksCount(Guid rotationId, ModuleSourceType sourceType);

    }
}
