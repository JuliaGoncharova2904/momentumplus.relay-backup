﻿using System;
using System.Collections.Generic;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IModuleService
    {
        rModuleViewModel GetBaseModuleFromTypeViewModel(ModuleType type);

        rModuleViewModel GetModuleViewModel(Guid moduleId);

        IEnumerable<rModuleViewModel> GetBaseModulesViewModels();

        rModuleViewModel GetTemplateModuleViewModel(ModuleType type, Guid templateId);

        rModuleViewModel PopulateBaseModuleViewModel(ModuleType moduleType, Guid? activeTopicGroupId, Guid? activeTopicId, int? topicGroupsPageNo, int? topicsPageNo, int? tasksPageNo);

        rModuleViewModel PopulateTemplateModuleViewModel(Guid templateId, ModuleType moduleType, Guid? activeTopicGroupId, Guid? activeTopicId, int? topicGroupsPageNo, int? topicsPageNo, int? tasksPageNo);

        void ChangeModuleStatus(Guid moduleId, bool status);

        void ChangeTemplateModuleStatus(Guid baseModuleId, Guid templateId, bool status);
    }
}
