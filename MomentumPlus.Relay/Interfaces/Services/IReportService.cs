﻿using MomentumPlus.Relay.Models;
using System;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IReportService
    {
        ReportPreviewViewModel PopulateRotationReportViewModel(Guid rotationId, ModuleSourceType sourceType, Guid? selectedRotationId = null);

        ShiftReportPreviewViewModel PopulateShiftReportViewModel(Guid shiftId, ModuleSourceType sourceType, Guid? selectedRotationId = null);

        QMEReportViewModel PopulateQMERotationReportViewModel(Guid sourceId, ModuleSourceType sourceType, bool isShift, Guid? selectedSourceId = null);

        HandoverReportViewModel PopulateHandoverReportViewModel(Guid sourceId, ModuleSourceType sourceType, bool isShift, Guid? selectedSourceId = null);
    }
}
