﻿using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IRotationService
    {

        EditRotationViewModel PopulateEditRotationModel(EditRotationViewModel model);
        CreateRotationViewModel PopulateEditRotationModel(CreateRotationViewModel model);
        IEnumerable<ReceivedHistorySlideViewModel> GetRotationsReceivedHistorySlidesByUser(Guid userId);
        IEnumerable<RotationHistorySlideViewModel> GetRotationsHistorySlidesByUser(Guid userId);

        Guid? GetCurrentRotationIdForUser(Guid userId);

        SummaryViewModel GetSummaryForRotation(Guid rotationId, Guid currentUserId, bool viewMode);

        DashboardViewModel GetDashboardForRotation(Guid rotationId, Guid currentUserId);

        EditRotationViewModel GetRotation(Guid Id);
        bool UpdateRotation(EditRotationViewModel model);

        FromToBlockViewModel GetFromToBlock(Guid rotationId);

        IEnumerable<EditRotationViewModel> GetRotationsForPosition(Guid positionId);

        bool CreateFirstRotation(CreateRotationViewModel model);
        bool CheckRotationPatternCompatibility(Guid employeeId, ConfirmRotationViewModel model);
        bool ConfirmRotation(Guid employeeId, ConfirmRotationViewModel model);
        bool RotationIsConfirmed(Guid rotationid);

        ConfirmRotationViewModel PopulateConfirmModelWithCurrentRotation(Guid employeeId);

        string GetRotationOwnerFullName(Guid rotationId);

        void ChangeFinalizeStatusForDraftTopicsAndTasks(Guid rotationId, FinalizeStatus status);

        IEnumerable<ReceivedRotationSlideViewModel> GetRotationsWhoPopulateMyReceivedSection(Guid rotationId);

        bool ExpireRotation(Guid rotationId);

        bool IsRotationExpiredOrNotStarted(Guid rotationId);

        string GetRotationPeriod(Guid rotationId);
        string GetReceivedRotationPeriod(Guid? rotationId, Guid? sourceRotationId);

        Guid GetCurrentRotationId(Guid userAnyoneRotationId);

        Guid? GetRotationOwnerId(Guid? rotationId);

        Guid? GetRotationBackToBackId(Guid? rotationId);

        EditRotationDatesViewModel GetRotationDates(Guid rotationId);

        bool UpdateRotationDates(EditRotationDatesViewModel model);

        bool EndSwingOfRotation(Guid rotationId);

        bool IsRotationSwingEnd(Guid rotationId);

        bool IsShiftRotation(Guid rotationId);

        IEnumerable<ShiftReceivedSlideViewModel> GetShiftsReceivedHistorySlidesByUser(Guid userId);

        bool IsRotationHavePrevRotation(Guid rotationId);
    }
}
