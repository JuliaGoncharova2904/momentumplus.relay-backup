﻿using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IAttachmentService
    {
        IEnumerable<AttachmentViewModel> GetAttachmentForTopic(Guid topicId);
        IEnumerable<AttachmentViewModel> GetAttachmentForTask(Guid taskId);

        void AddAttachmentToTopic(AttachmentViewModel attachment);
        void AddAttachmentToTask(AttachmentViewModel attachment);

        void RemoveAttachmentFromTopic(Guid topicId, Guid attachmentId);
        void RemoveAttachmentFromTask(Guid taskId, Guid attachmentId);
    }
}
