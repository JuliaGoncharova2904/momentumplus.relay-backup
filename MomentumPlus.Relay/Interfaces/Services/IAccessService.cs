﻿using System;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IAccessService
    {
        bool UserIsProjectManager(Guid userId);
        bool UserHasAccessToProject(Guid userId, Guid projectId, bool throwException = false);

        bool SafetyMessageArchiveV2IsEnable();

        bool IsGlobalReportAvailable(Guid reportId, Guid userId, RotationType reportType);

        bool IsReportSharedForUser(Guid reportId, Guid userId);

        bool IsGlobalReportHistoryAndReceivedDisable(Guid userId);
    }
}
