﻿using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IChecklistService
    {
        ChecklistViewModel GetChecklistForRotation(Guid rotationId, Guid currentUserId);

        ChecklistRotationFilterViewModel GetRotationsForFilter(Guid? rotationId);

        TeamCheckListViewModel GetTeamForCheckList(Guid? rotationId);
    }
}
