﻿using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ICriticalControlV2Service
    {
        EditCriticalControlV2ViewModel PopulateCriticalControlEditModel(Guid majorHazardId);
        EditCriticalControlV2ViewModel PopulateCriticalControlEditModel(EditCriticalControlV2ViewModel model);
        string GetCriticalControlOwnerName(Guid criticalControlId);
        IEnumerable<SelectListItem> GetCriticalControlsList(Guid majorHazardId);
        EditCriticalControlV2ViewModel GetCriticalControlForEdit(Guid criticalControlId);
        void AddCriticalControl(EditCriticalControlV2ViewModel model);
        void UpdateCriticalControl(EditCriticalControlV2ViewModel model);
        IEnumerable<string> GetCriticalControlChampions(Guid criticalControlId);
    }
}
