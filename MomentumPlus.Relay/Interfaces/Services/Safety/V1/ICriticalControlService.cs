﻿using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ICriticalControlService
    {
        EditCriticalControlViewModel PopulateCriticalControlModel();
        EditCriticalControlViewModel PopulateCriticalControlModel(EditCriticalControlViewModel model);

        IEnumerable<SelectListItem> GetCriticalControlsList();
        IEnumerable<SelectListItem> GetCriticalControlsListByMajorHazard(Guid majorHazardId);

        IEnumerable<CriticalControlViewModel> GetCriticalControlsByMajorHazard(Guid majorHazardId);
        EditCriticalControlViewModel GetCriticalControl(Guid criticalControlId);
        void UpdateCriticalControl(EditCriticalControlViewModel model);
        void AddCriticalControl(EditCriticalControlViewModel model);
    }
}
