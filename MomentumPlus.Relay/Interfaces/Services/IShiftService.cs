﻿using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IShiftService
    {
        void ConfirmShift(Guid userId, ConfirmShiftViewModel model);

        ConfirmShiftViewModel PopulateConfirmShiftViewModel(Guid userId);

        ChooseShiftRecipientModel PopulateChooseShiftRecipientViewModel(Guid shiftId);

        void UpdateShiftRecipient(ChooseShiftRecipientModel model);

        Guid? GetCurrentShiftIdForRotation(Guid rotationId);

        EditShiftDatesViewModel GetShiftDates(Guid shiftId);
        void UpdateRotationDates(EditShiftDatesViewModel model);

        bool RotationHasWorkingShift(Guid rotationId);
        bool ShiftHasRecipient(Guid shiftId);
        bool IsShiftWorkTimeEnd(Guid shiftId);

        bool IsShiftEnd(Guid shiftId);

        string GetShiftPeriod(Guid? shiftId);

        string GetShiftHandoverRecipientName(Guid? shiftId);

        Guid ShiftRotationId(Guid shiftId);

        List<ShiftReceivedSlideViewModel> PopulateReceivedShiftsPanel(Guid shiftId);

        void SetFinalizeStatusForDraftItems(Guid shiftId, FinalizeStatus status);

        string GetReceivedShiftPeriod(Guid? shiftId, Guid? sourceShiftId);

        string GetShiftOwnerName(Guid shiftId);

        void ExpireShift(Guid shiftId);
    }
}
