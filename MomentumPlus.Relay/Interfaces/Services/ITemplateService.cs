﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ITemplateService
    {
        TemplateViewModel GetTemplateViewModel(Guid templateId);

        IEnumerable<SelectListItem> GetTemplateList();

        TemplatesViewModel PopulateTemplatesViewModel(
            string selectedTemplate,
            ModuleType? selectedModule,
            int? topicGroupsPageNo,
            int? topicsPageNo,
            int? tasksPageNo,
            Guid? topic = null,
            Guid? topicGroup = null);

        void AddTemplate(TemplateViewModel model);

        void UpdateTemplate(TemplateViewModel model);

        bool TemplateExist(TemplateViewModel model);

    }
}
