﻿using System;
using System.Collections.Generic;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ITopicSearchService
    {
        List<SearchBarItem> GetBaseTopicGroups();

        TopicSearchResultViewModel SearchTopics(Guid currentUserId, TopicSearchViewModel model);

        TopicReportPdfViewModel PopulateTopicSearchReportViewModel(IEnumerable<Guid?> selectedTopicGroupsIds, IEnumerable<Guid?> selectedUserIds, TopicSearchDateRangeType? dateRangeType);
    }
}
