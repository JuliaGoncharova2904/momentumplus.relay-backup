﻿using System;
using System.Collections.Generic;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ISchedulerService
    {
        SchedulerFilterPanelViewModel GetFilterPanel(Guid userId);
        SchedulerPanelViewModel GetTimelinesPanel(SchedulerFilterOptionsViewModel model, Guid userId);
        SchedulerTimelinePanelViewModel GetTimelineForUser(Guid userId, bool isMyTimeline = false);
    }
}
