﻿using System;
using System.Collections.Generic;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ITopicService
    {
        IEnumerable<rTopicViewModel> GetTopicGroupTopicsViewModels(Guid topicGroupId);

        rTopicViewModel GetTopicViewModel(Guid topicId);

        void AddTopic(rTopicViewModel model);

        void UpdateTopic(rTopicViewModel model);

        bool TopicExist(rTopicViewModel model);

        IEnumerable<rTopicViewModel> PopulateTemplateTopicGroupTopicsViewModels(Guid baseTopicGroupId, Guid templateModuleId);

        bool CheckChildTopicStatus(Guid parentTopicId, Guid templateModuleId);

        void UpdateTopicStatus(Guid topicId, bool status);

        void UpdateChildTopicStatus(Guid baseTopicId, Guid templateId, bool status);

        void UpdateTopicGroupTopicsStatus(Guid topicGroupId, bool status);

    }
}
