﻿using System;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IRotationTopicService
    {
        void UpdateRotationTopicGroupTopicsStatus(Guid rotationTopicGroupId, bool status);

        void UpdateRotationTopicStatus(Guid rotationTopicId, bool status);

        void UpdateTemplateTopicChildTopicsStatus(Guid templateTopicId, bool status);

        bool IsTopicExist(Guid topicId);

        bool IsTopicExist(Guid topicGroupId, Guid topicId, string name);

        bool IsTopicAssignedWithSameNameExist(Guid destId, Guid assignedToId, string name, RotationType rotationType = RotationType.Swing);

        bool IsTopicAssignedWithSameNameExistByTopic(Guid rotationTopicId, Guid assignedToId, string name);

        void UpdateHSETopic(HSETopicDialogViewModel model);

        HSETopicViewModel UpdateHSETopic(HSETopicViewModel model);

        HSETopicViewModel GetHSETopic(Guid topicId);

        void UpdateCoreTopic(CoreTopicDialogViewModel model);

        CoreTopicViewModel UpdateCoreTopic(CoreTopicViewModel model);

        CoreTopicViewModel GetCoreTopic(Guid topicId);

        void UpdateProjectTopic(ProjectTopicDialogViewModel model);

        ProjectsTopicViewModel GetProjectTopic(Guid topicId);

        ProjectsTopicViewModel UpdateProjectTopic(ProjectsTopicViewModel model);

        void UpdateTeamTopic(TeamTopicDialogViewModel model);

        TeamTopicViewModel UpdateTeamTopic(TeamTopicViewModel model);

        TeamTopicViewModel GetTeamTopic(Guid topicId);

        HSETopicDialogViewModel PopulateHSETopicDialogModel(Guid topicId);

        CoreTopicDialogViewModel PopulateCoreTopicDialogModel(Guid topicId);

        ProjectTopicDialogViewModel PopulateProjectTopicDialogModel(Guid topicId);

        TeamTopicDialogViewModel PopulateTeamTopicDialogModel(Guid topicId);

        HSEAddInlineTopicViewModel PopulateHSETopicInlineModel(Guid rotationId);

        RotationCoreAddInlineTopicViewModel PopulateCoreTopicInlineModel(Guid rotationId);

        ProjectAddInlineTopicViewModel PopulateProjectTopicInlineModel(Guid rotationId);

        TeamAddInlineTopicViewModel PopulateTeamTopicInlineModel(Guid rotationId);

        void AddHSETopic(HSEAddInlineTopicViewModel model);

        void AddRotationCoreTopic(RotationCoreAddInlineTopicViewModel model);

        void AddProjectTopic(ProjectAddInlineTopicViewModel model);

        void AddTeamTopic(TeamAddInlineTopicViewModel model);


        ManagerCommentsViewModel PopulateManagerCommentViewModel(Guid topicId);

        void UpdateTopicManagerComment(ManagerCommentsViewModel model);

        void CarryforwardTopic(Guid topicId);

        bool IsMyTopic(Guid userId, Guid topicId);

        bool RemoveTopic(Guid topicId);

        ModuleType GetTopicModuleType(Guid topicid);

        ShiftCoreAddInlineTopicViewModel PopulateShiftCoreTopicInlineModel(Guid shiftId);

        void AddShiftCoreTopic(ShiftCoreAddInlineTopicViewModel model);

        ShiftHSEAddInlineTopicViewModel PopulateShiftHSETopicInlineModel(Guid shiftId);

        void AddShiftHSETopic(ShiftHSEAddInlineTopicViewModel model);

        ShiftProjectAddInlineTopicViewModel PopulateShiftProjectTopicInlineModel(Guid shiftId);

        void AddShiftProjectTopic(ShiftProjectAddInlineTopicViewModel model);

        ShiftTeamAddInlineTopicViewModel PopulateShiftTeamTopicInlineModel(Guid shiftId);

        void AddShiftTeamTopic(ShiftTeamAddInlineTopicViewModel model);

        bool IsMyShiftTopic(Guid userId, Guid topicId);

        bool IsShiftTopic(Guid topicId);

        TeamTopicDialogViewModel PopulateShiftTeamTopicDialogModel(Guid topicId);
    }
}
