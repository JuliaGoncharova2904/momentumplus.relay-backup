﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IAdministrationService
    {
        void InitializeAdminSettings();

        AccountDetailsViewModel PopulateAccountDetailsViewModel();

        VersionDetailsViewModel PopulateVersionDetailsViewModel();

        void UpdateVersionDetails(VersionDetailsViewModel model);

        ThirdPartySettingsViewModel PopulateThirdPartySettingsViewModel();

        void UpdateThirdPartySettings(ThirdPartySettingsViewModel model);

        PreferencesSettingsViewModel PopulatePreferencesSettingsViewModel();

        void UpdatePreferencesSettings(PreferencesSettingsViewModel model, HttpPostedFileBase logo);


    }
}
