﻿using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IUserService
    {
        EmployeeViewModel PopulateEmployeeModel();
        EmployeeViewModel PopulateEmployeeModel(EmployeeViewModel model);

        IEnumerable<SelectListItem> GetEmployeesList();
        IEnumerable<EmployeeViewModel> GetAllEmployees();
        IEnumerable<EmployeeViewModel> GetEmployeesForPosition(Guid positionId);

        EmployeeViewModel GetEmployeeById(Guid Id);
    
        IEnumerable<EmployeeViewModel> GetEmployeesForTeam(Guid teamId);

        bool IsEmployeeExist(string login);
        bool IsEmployeeExist(string login, Guid exeptId);
        void AddEmployee(CreateEmployeeViewModel employeeViewModel);

        IEnumerable<UserDetailsViewModel> GetUsersDetails();
        bool RemoveUser(Guid userId);

        SecurityViewModel GetSecurityDetails(Guid personId);
        bool UpdateSecurityDetails(Guid personId, SecurityViewModel model);

        PersonalDetailsViewModel GetPersonDetails(Guid personId);
        bool UpdatePersonDetails(Guid personId, PersonalDetailsViewModel personDetails, HttpPostedFileBase foto);

        bool UpdateEmployee(EmployeeViewModel employee);
   
        void ResetEmployeePassword(Guid userId, string newPassword);

        List<TeamForWidgetViewModel> GetTeamForWidjet(Guid? rotationId, Guid? userId);

        void ForgotPassword(Guid userId, Guid resetGuid, string resetUrl);

        Guid? GetEmployeeIdByResetHash(Guid resetPasswordId);

        void UpdateLastLoginDate(Guid userId);
    }
}
