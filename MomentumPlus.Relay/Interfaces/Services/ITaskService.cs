﻿using System;
using System.Collections.Generic;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ITaskService
    {
        IEnumerable<rTaskViewModel> GetTopicTasksViewModels(Guid topicId);

        rTaskViewModel GetTaskViewModel(Guid taskId);

        void AddTask(rTaskViewModel model);

        void UpdateTask(rTaskViewModel model);

        bool TaskExist(rTaskViewModel model);

        IEnumerable<rTaskViewModel> PopulateTemplateTopicTasksViewModels(Guid baseTopicId, Guid templateModuleId);

        bool CheckChildTaskStatus(Guid parentTaskId, Guid templateModuleId);

        void UpdateTaskStatus(Guid taskId, bool status);

        void UpdateChildTaskStatus(Guid baseTaskId, Guid templateId, bool status);

        void UpdateTopicTasksStatus(Guid topicId, bool status);


    }
}
