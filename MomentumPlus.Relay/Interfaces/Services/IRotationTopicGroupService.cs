﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IRotationTopicGroupService
    {
        void UpdateRotationModuleTopicGroupsStatus(Guid rotationModuleId, bool status);

        void UpdateRotationTopicGroupStatus(Guid rotationTopicGroupId, bool status);

        void UpdateTemplateTopicGroupChildRotationTopicGroupsStatus(Guid templateTopicGroupId, bool status);

        IEnumerable<SelectListItem> GetRotationModuleTopicGroupsList(Guid moduleId);
    }
}
