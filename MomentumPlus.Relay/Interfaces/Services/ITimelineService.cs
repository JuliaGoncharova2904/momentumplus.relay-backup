﻿using MomentumPlus.Relay.Models;
using System;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ITimelineService
    {
        TimelineViewModel GetLastTwoYearsTimeline(Guid rotationId);
        TimelineViewModel GetTimelineForYear(Guid rotationId, int year);
    }
}
