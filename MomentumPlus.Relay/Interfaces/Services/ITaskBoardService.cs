﻿using MomentumPlus.Relay.Models;
using MvcPaging;
using System;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ITaskBoardService
    {
        TaskBoardAddTaskDialogViewModel PopulateAddTaskModel(Guid userId);
        void AddTask(TaskBoardAddTaskDialogViewModel task);
        TaskBoardEditTaskDialogViewModel PopulateEditTaskModel(Guid userId, Guid taskId);
        TaskBoardEditTaskDialogViewModel PopulateEditTaskModel(TaskBoardEditTaskDialogViewModel model);
        void UpdateTask(TaskBoardEditTaskDialogViewModel model);
        TaskBoardCompleteTaskDialogViewModel PopulateCompleteChecklistTaskViewModel(Guid taskId);
        TaskBoardCompleteHandbackTaskDialogViewModel PopulateCompleteHandbackTaskViewModel(Guid taskId);
        IPagedList<TaskBoardTaskTopicViewModel> GetReceivedTasks(Guid userId, int page, int pageSize, TaskBoardSortOptions sortStrategy, TaskBoardFilterOptions filter);
        IPagedList<TaskBoardTaskTopicViewModel> GetAssignedTasks(Guid userId, int page, int pageSize, TaskBoardSortOptions sortStrategy, TaskBoardFilterOptions filter);
        IPagedList<TaskBoardTaskTopicViewModel> GetMyTasks(Guid userId, int page, int pageSize, TaskBoardSortOptions sortStrategy, TaskBoardFilterOptions filter);
        void SaveCompleteTaskBoardTask(TaskBoardCompleteOutcomesTabViewModel model, CompleteDialogOption completeOption);
        TaskBoardTaskDetailsDialogViewModel PopulateTaskBoardTaskDetailsDialogViewModel(Guid taskId);
        TaskBoardTaskTopicViewModel GetTaskTopic(Guid taskId);
        TaskBoardTaskTopicViewModel UpdateTaskTopic(TaskBoardTaskTopicViewModel model);

        TaskBoardEditTaskDialogViewModel PopulateReceivedEditTaskModel(Guid userId, Guid taskId);

        TaskBoardEditTaskDialogViewModel PopulateReceivedEditTaskModel(TaskBoardEditTaskDialogViewModel model);

        void EditReceivedTask(Guid currentUserId, TaskBoardEditTaskDialogViewModel model);
        bool IsTaskBoardTask(Guid taskId);

        TaskBoardCompleteItemTabViewModel PopulateTaskBoardTopicDetailsDialogViewModel(Guid taskId);

        TaskBoardPdfReportViewModel TaskBoardTasksForPdfReport(Guid userId, TaskBoardSection section,
            TaskBoardFilterOptions filter);

    }
}
