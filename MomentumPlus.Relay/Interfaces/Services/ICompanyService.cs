﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ICompanyService
    {
        IEnumerable<SelectListItem> GetCompaniesList();

        CompanyViewModel GetCompany(Guid companyId);

        IEnumerable<CompanyViewModel> GetCompanies();

        bool CompanyExist(CompanyViewModel model);

        void AddCompany(CompanyViewModel model);

        void UpdateCompany(CompanyViewModel model);

        bool IsDefaultCompanyExist(CompanyViewModel model);
    }
}