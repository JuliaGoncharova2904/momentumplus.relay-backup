﻿using System;
using MomentumPlus.Relay.Models.Api.Enums;

namespace MomentumPlus.Relay.Models.Api
{
    public class HomePageViewModel
    {
        public Guid UserId { get; set; }

        public string Avatar { get; set; }

        public int NotViewedNotificationsCount { get; set; }

        public string CurrentWorkPeriod { get; set; }

        public Guid? CurrentHandoverId { get; set; }

        public HandoverType HandoverType { get; set; }

        public int HandoverTopicsCount { get; set; }

        public int HandoverTasksCount { get; set; }

        public bool IsAdminOrManager { get; set; }
    }
}
