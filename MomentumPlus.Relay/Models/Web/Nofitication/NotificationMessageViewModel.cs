﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class NotificationMessageViewModel
    {
        public Guid Id { get; set; }
        public string Message { get; set; }
        public string UrlImage { get; set; }
        public DateTime Date { get; set; }
        public bool IsOpened { get; set; }
        public string Type { get; set; }
    }
}
