﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class SiteViewModel
    {
        public Guid? Id { get; set; }

        [Required]
        [Display(Name = "Name*")]
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }

        [Display(Name = "Address")]
        [StringLength(750)]
        public string Address { get; set; }

        [Display(Name = "Map Link")]
        [StringLength(2000)]
        public string MapLink { get; set; }
    }
}
