﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class PositionViewModel
    {
        public Guid? Id { get; set; }

        [Required]
        [Display(Name = "Name*")]
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Site*")]
        public string SiteId { get; set; }
        public IEnumerable<SelectListItem> Sites { get; set; }

        [Required]
        [Display(Name = "Template*")]
        public Guid? TemplateId { get; set; }
        public IEnumerable<SelectListItem> Templates { get; set; }
    }
}
