﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class EditShiftRotationViewModel
    {
        public Guid RotationId { get; set; }
        public Guid? ShiftId { get; set; }
    }
}
