﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class EditRotationViewModel : BaseRotationViewModel
    {
        [Display(Name = "Rotation Owner")]
        public string RotationOwner { get; set; }
        public string RotationOwnerId { get; set; }

        public bool CanChangeLineManager { get; set; }
    }
}
