﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class CreateRotationViewModel : BaseRotationViewModel
    {
        [Required]
        [Display(Name = "Rotation Owner*")]
        public string RotationOwnerId { get; set; }
        public IEnumerable<SelectListItem> RotationOwnerEmployees { get; set; }
    }
}
