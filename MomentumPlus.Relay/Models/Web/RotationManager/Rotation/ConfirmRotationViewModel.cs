﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class ConfirmRotationViewModel
    {
        [Required]
        [Display(Name = "Start of your swing*")]
        public DateTime? StartSwingDate { get; set; }

        [Required]
        [Display(Name = "End of your swing*")]
        public DateTime? EndSwingDate { get; set; }

        [Required]
        [Display(Name = "Back on-site*")]
        public DateTime? BackOnSiteDate { get; set; }

        public DateTime? PrevRotationEndDate { get; set; }

        [Required]
        [Display(Name = "Repeat Times")]
        public int RepeatTimes { get; set; }

        public IEnumerable<SelectListItem> RepeatRotationRange { get; set; }

        public ConfirmRotationState ConfirmState { get; set; }

        public ConfirmRotationViewModel FillRepeatRotationRange()
        {
            this.RepeatRotationRange = new[] {
                new SelectListItem() { Text = "No", Value = "0" },
                new SelectListItem() { Text = "Once", Value = "1" },
                new SelectListItem() { Text = "Twice", Value = "2" },
                new SelectListItem() { Text = "3 times", Value = "3" },
                new SelectListItem() { Text = "4 times", Value = "4" },
                new SelectListItem() { Text = "5 times", Value = "5" }
            };

            return this;
        }

    }
}
