﻿using MvcPaging;

namespace MomentumPlus.Relay.Models
{
    public class TopicSearchResultViewModel
    {
        public IPagedList<TopicSearchResultItem> ResultItems { get; set; }
        public TopicSearchViewModel Params { get; set; }
    }
}
