﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class SearchBarItem
    {
        public Guid Id { get; set; }

        public  string Name { get; set; }
    }
}
