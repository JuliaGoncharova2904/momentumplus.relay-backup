﻿using System.ComponentModel;

namespace MomentumPlus.Relay.Models
{
    public enum TopicSearchDateRangeType
    {
        [Description("All Time")]
        AllTime = 0,
        [Description("Last Day")]
        LastDay = 1,
        [Description("Last 7 Days")]
        Last7Days = 2,
        [Description("Last Month")]
        LastMonth = 3,
        [Description("Last Year")]
        LastYear = 4
    }
}
