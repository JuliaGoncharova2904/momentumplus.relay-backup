﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class TopicSearchResultItem
    {
        public Guid Id { get; set; }

        public string CreatedDate { get; set; }

        public string CreatedTime { get; set; }

        public  string CreatorName { get; set; }

        public Guid CreatorId { get; set; }

        public string Name { get; set; }

        public string Reference { get; set; }

        public string Notes { get; set; }

        public bool HasLocation { get; set; }

        public bool HasTasks { get; set; }

        public bool HasAttachments { get; set; }

        public bool IsFinalized { get; set; }
    }
}
