﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class TopicReportPdfViewModel
    {
        public Guid? CompanyLogoImageId { get; set; }

        public List<TopicSearchResultItem> Topics { get; set; }

        public string Employees { get; set; }

        public string DateRange { get; set; }

        public string TopicsInSearch { get; set; }

        public string ReportDate { get; set; }
    }
}
