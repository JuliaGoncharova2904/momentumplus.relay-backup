﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class TopicSearchViewModel
    {
        public IEnumerable<Guid?> SelectedTopicGroupsIds { get; set; }

        public IEnumerable<Guid?> SelectedUserIds { get; set; }

        public TopicSearchDateRangeType DateRangeType { get; set; }

        public TopicSearchPrintType PrintType { get; set; }

        public int? PageNumber { get; set; }

        public int? PageSize { get; set; }

    }
}
