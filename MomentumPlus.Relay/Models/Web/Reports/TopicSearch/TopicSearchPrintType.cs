﻿using System.ComponentModel;

namespace MomentumPlus.Relay.Models
{
    public enum TopicSearchPrintType
    {
        [Description("Portrait")]
        portrait = 0,
        [Description("Landscape")]
        landscape = 1,
    }
}
