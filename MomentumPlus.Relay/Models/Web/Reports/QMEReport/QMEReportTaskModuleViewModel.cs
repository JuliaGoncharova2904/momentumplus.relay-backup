﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models
{
    public class QMEReportTaskModuleViewModel
    {
        public string Name { get; set; }

        public IEnumerable<QMEReportTaskViewModel> Tasks { get; set; }
    }
}
