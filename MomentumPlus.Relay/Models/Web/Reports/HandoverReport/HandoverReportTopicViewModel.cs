﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class HandoverReportTopicViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsNullReport { get; set; }
        public string TopicGroupName { get; set; }
        public string Tag { get; set; }
        public Guid? FromSourceId { get; set; }
        public Guid? RelationId { get; set; }
        public bool? IsPinned { get; set; }
        public bool HasTask { get; set; }
    }
}
