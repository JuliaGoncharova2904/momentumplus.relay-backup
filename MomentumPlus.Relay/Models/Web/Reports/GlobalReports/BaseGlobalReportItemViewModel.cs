﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class BaseGlobalReportItemViewModel
    {
        public DateTime CreatedTime { get; set; }

        public string DateString { get; set; }
        public string PositionName { get; set; }
        public RotationType RotationType { get; set; }
        public int ItemsNumber { get; set; }
        public int TasksNumber { get; set; }
        public Guid SourceId { get; set; }
        public bool IsQME { get; set; }
    }
}
