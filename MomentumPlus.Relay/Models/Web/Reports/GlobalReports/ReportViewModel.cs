﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class ReportViewModel
    {
        public Guid ReportId { get; set; }

        public bool IsReadMode { get; set; }

        public bool ShareReportBtnIsActive { get; set; }

        public RotationType ReportType { get; set; }

        public ReportType Type { get; set; }

        public Guid? FromSourceId { get; set; }

        public string HandoverCreatorOrRecipientName { get; set; }

        public string ReportPeriod { get; set; }
    }
}
