﻿namespace MomentumPlus.Relay.Models
{
    public class FooterViewModel
    {
        public int Page { get; set; }

        public int Sitepage { get; set; }

        public int Frompage { get; set; }

        public int Topage { get; set; }
    }
}
