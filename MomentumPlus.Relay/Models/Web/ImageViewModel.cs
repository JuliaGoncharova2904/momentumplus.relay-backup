﻿namespace MomentumPlus.Relay.Models
{
    public class FileViewModel
    {
        public byte[] BinaryData { get; set; }
        public string ContentType { get; set; }
    }
}
