﻿using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class ForgottenPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email*")]
        public string Email { get; set; }
    }
}