﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class TemplatesViewModel
    {
        public TemplateViewModel Template { get; set; }

        public IEnumerable<SelectListItem> Templates { get; set; }

        [Display(Name = "Template")]
        public string SelectedTemplate { get; set; }

        public ModuleType? SelectedModule { get; set; }

        public rModuleViewModel Module { get; set; }
    }
}
