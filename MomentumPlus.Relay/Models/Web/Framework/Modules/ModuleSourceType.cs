﻿namespace MomentumPlus.Relay.Models
{
    public enum ModuleSourceType
    {
        Received = 0,
        Draft = 1
    }
}
