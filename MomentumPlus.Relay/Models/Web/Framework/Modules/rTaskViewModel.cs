﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class rTaskViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Name*")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Priority*")]
        public TaskPriority Priority { get; set; }


        [Display(Name = "Notes")]
        public string Description { get; set; }

        public Guid? ParentTaskId { get; set; }

        public Guid TopicId { get; set; }

        public bool Enabled { get; set; }
    }
}
