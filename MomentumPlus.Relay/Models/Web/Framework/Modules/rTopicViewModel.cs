﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class rTopicViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Name*")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        [Display(Name = "Notes")]
        public string Description { get; set; }

        public Guid TopicGroupId { get; set; }

        public Guid? ParentTopicId { get; set; }

        public ICollection<rTaskViewModel> Tasks { get; set; }

        public bool Enabled { get; set; }
    }
}
