﻿using System;
using System.Collections.Generic;
using MvcPaging;

namespace MomentumPlus.Relay.Models
{
    public class rModuleViewModel
    {
        public Guid Id { get; set; }

        public ModuleType Type { get; set; }

        public Guid? ParentModuleId { get; set; }

        public Guid? TemplateId { get; set; }

        public bool Enabled { get; set; }

        public IPagedList<rTopicGroupViewModel> TopicGroupsList { get; set; }

        public IPagedList<rTopicViewModel> TopicsList { get; set; }

        public IPagedList<rTaskViewModel> TasksList { get; set; }

        public Guid? ActiveTopicGroupId { get; set; }

        public Guid? ActiveTopicId { get; set; }

        public int EnabledTopicGroupsCounter { get; set; }

        public virtual ICollection<rTopicGroupViewModel> TopicGroups { get; set; }

    }
}
