﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class TeamRotationViewModel
    {
        public Guid ID { get; set; }
        public Guid OwnerId { get; set; }
        public string OwnerFullName { get; set; }
        public string OwnerEmail { get; set; }
        public string OwnerPosition { get; set; }
        public Guid? BackToBackId { get; set; }
        public string BackToBackName { get; set; }
        public bool IsActive { get; set; }
        public string StateMessage { get; set; }
        public bool IsLineManager { get; set; }
        public bool IsAdmin { get; set; }

        public bool IsEnable { get; set; }
    }
}
