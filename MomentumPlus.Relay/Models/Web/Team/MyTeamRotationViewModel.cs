﻿using MvcPaging;

namespace MomentumPlus.Relay.Models
{
    public class MyTeamRotationViewModel
    {
        public bool IsViewMode { get; set; }
        public string OwnerName { get; set; }
        public IPagedList<TeamRotationViewModel> TeamRotations { get; set; }
    }
}
