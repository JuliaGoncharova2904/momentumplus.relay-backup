﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models
{
    public class LineManagerForTeamViewModel
    {
        public Guid UserId { get; set; }

        public string UserName { get; set; }

        public string UserEmail { get; set; }

        public string RotarionPatternName { get; set; }

        public string PositionName { get; set; }

        public int CountReceivedTask { get; set; }

    }
}
