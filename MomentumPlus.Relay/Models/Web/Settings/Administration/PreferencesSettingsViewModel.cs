﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class PreferencesSettingsViewModel
    {

        [Required]
        [Display(Name = "Handover Trigger*")]
        public HandoverTriggerType HandoverTriggerType { get; set; }

        [Display(Name = "Time")]
        public string HandoverTriggerTime { get; set; }

        [Required]
        [Display(Name = "Handover Preview*")]
        public HandoverPreviewType HandoverPreviewType { get; set; }

        [Display(Name = "Time")]
        public string HandoverPreviewTime { get; set; }


        public Guid? LogoImageId { get; set; }

        [Display(Name = "Allowed File Types")]
        public IEnumerable<string> AllowedFileTypes { get; set; }


        public string AllowedFileTypesString
        {
            get
            {
                string allowedFileTypes = string.Empty;

                if (AllowedFileTypes != null)
                {
                    foreach (string allowedFileType in AllowedFileTypes)
                    {
                        allowedFileTypes = string.Format("{0} \"{1}\",", allowedFileTypes, allowedFileType);
                    }
                }

                return allowedFileTypes;
            }
        }

    }
}
