﻿using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class ThirdPartySettingsViewModel
    {
        [Display(Name = "Twilio Account")]
        public string TwilioAccount { get; set; }

        [Display(Name = "Twilio End Point")]
        [Url(ErrorMessage = "Please enter valid Url")]
        public string TwilioEndPoint { get; set; }

        [Display(Name = "Twilio Phone Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression("^(?!0+$)(\\+\\d{1,3}[- ]?)?(?!0+$)\\d{10,15}$", ErrorMessage = "Please enter valid phone number")]
        public string TwilioPhoneNumber { get; set; }

        [Display(Name = "SMS Gateway")]
        public string SMSGateway { get; set; }

        [Display(Name = "SMS/User/Day")]
        public int SMSPerUserPerDay { get; set; }
    }
}
