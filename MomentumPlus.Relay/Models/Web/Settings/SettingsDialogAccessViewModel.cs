﻿namespace MomentumPlus.Relay.Models
{
    public class SettingsDialogAccessViewModel
    {
        //------------------------
        public struct _Profile
        {
            public bool PresonalTab;
            public bool SecurityTab;
            public bool PreferencesTab;
        };

        public struct _Admin
        {
            public bool AccountTab;
            public bool VersionDetailsTab;
            public bool ThirdPartyTab;
            public bool PreferencesTab;
            public bool UsersTab;
            public bool UpgradeTab;
        }
        //------------------------
        public bool ProfileTab;
        public bool AdminTab;
        public _Profile Profile;
        public _Admin Admin;
        //------------------------
    }
}
