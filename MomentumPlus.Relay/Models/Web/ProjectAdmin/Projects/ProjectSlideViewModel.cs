﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class ProjectSlideViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ProjectStatus { get; set; }
    }
}
