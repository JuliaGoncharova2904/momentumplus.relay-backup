﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class ProjectFullSlideViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ActivePeriod { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string ProjectManager { get; set; }
        public string ProjectStatus { get; set; }
    }
}
