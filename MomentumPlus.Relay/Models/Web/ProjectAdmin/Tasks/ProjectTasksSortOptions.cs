﻿namespace MomentumPlus.Relay.Models
{
    public enum ProjectTasksSortOptions
    {
        Default = 0,
        PriorityHighToLow = 1,
        PriorityLowToHigh = 2,
        DueDateHighToLow = 3,
        DueDateLowToHigh = 4
    }
}
