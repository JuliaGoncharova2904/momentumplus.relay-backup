﻿namespace MomentumPlus.Relay.Models
{
    public class ProjectAdminEditTaskDialogViewModel
    {
        public ProjectAdminProjectDetailsTabViewModel ProjectDetailsTab { get; set; }
        public ProjectAdminEditTaskDetailTabViewModel TaskDetailTab { get; set; }
        public ProjectAdminOutcomesTabViewModel OutcomesTab { get; set; }
    }
}
