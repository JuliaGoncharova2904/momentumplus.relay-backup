﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class ProjectAdminOutcomesTabViewModel
    {
        public Guid TaskId { get; set; }

        [Display(Name = "Handback")]
        public bool IsFeedbackRequired { get; set; }

        [Display(Name = "Handback Notes")]
        [StringLength(500)]
        public string Feedback { get; set; }
    }
}
