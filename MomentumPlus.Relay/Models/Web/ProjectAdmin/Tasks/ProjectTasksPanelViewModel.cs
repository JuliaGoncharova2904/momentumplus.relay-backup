﻿using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class ProjectTasksPanelViewModel
    {
        public int TotalTasksCounter { get; set; }
        public IEnumerable<ProjectTaskTopicViewModel> Tasks { get; set; }
    }
}
