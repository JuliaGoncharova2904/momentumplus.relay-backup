﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class ChecklistRotationFilterViewModel
    {
        public Guid? RotationId { get; set; }
        public Guid? CurrentRotationId { get; set; }
        public string Date { get; set; }
        public bool IsCurrentRotation { get; set; }
        public Guid? NextRotationId { get; set; }
        public Guid? PrevRotationId { get; set; }

    }
}
