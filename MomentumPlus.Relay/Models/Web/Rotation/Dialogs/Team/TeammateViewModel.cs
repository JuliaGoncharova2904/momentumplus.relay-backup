﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models
{
    public class TeammateViewModel
    {
        [Required]
        public Guid Id { get; set; }

        public string FullName { get; set; }

    }
}
