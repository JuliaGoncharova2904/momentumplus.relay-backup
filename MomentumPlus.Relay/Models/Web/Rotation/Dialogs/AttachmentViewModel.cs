﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace MomentumPlus.Relay.Models
{
    public class AttachmentViewModel
    {
        public Guid? Id { get; set; }

        public Guid? TargetId { get; set; }

        public Guid? FileId { get; set; }
        public string FileName { get; set; }

        [Required]
        [Display(Name = "Name*")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        public string AddedTime { get; set; }

        [Required]
        [Display(Name = "Upload from Computer")]
        //[FileExtensions(Extensions = "txt,doc,docx,pdf,png", ErrorMessage = "Specify a CSV file. (Comma-separated values)")]
        public HttpPostedFileBase File { get; set; }
    }
}
