﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class BaseTopicDialogViewModel
    {
        public Guid? Id { get; set; }

        [Required]
        [Display(Name = "Topic*")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        [Display(Name = "Notes")]
        [StringLength(1000)]
        public string Notes { get; set; }

        [Display(Name = "Manager Comments")]
        [StringLength(300)]
        public string ManagerComments { get; set; }

        [Required]
        [Display(Name = "Team Member*")]
        public Guid? AssignedToId { get; set; }


        [Display(Name = "Team Member*")]
        public Guid? RelationId { get; set; }

        [Display(Name = "Tasks")]
        public int TasksCounter { get; set; }

        public bool HasNewTasks { get; set; }

        [Display(Name = "Attacments")]
        public int AttacmentsCounter { get; set; }

        public bool HasNewAttacments { get; set; }

        [Display(Name = "Voice Messages")]

        public int VoiceMessagesCounter { get; set; }

        public bool HasNewVoiceMessages { get; set; }

        [Display(Name = "Handback")]
        public bool IsFeedbackRequired { get; set; }

        public bool IsFromTemplate { get; set; }


        [Display(Name = "Tags")]
        public IEnumerable<string> Tags { get; set; }

        public bool Enabled { get; set; }

        public bool IsManagerCommentsEnabled { get; set; }

        public string TagsString
        {
            get
            {
                string tagsString = string.Empty;

                if (Tags != null)
                {
                    foreach (string tag in Tags)
                    {
                        tagsString = string.Format("{0} \"{1}\",", tagsString, tag);
                    }
                }

                return tagsString;
            }
        }
    }
}
