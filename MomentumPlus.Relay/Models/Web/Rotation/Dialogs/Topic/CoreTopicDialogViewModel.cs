﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class CoreTopicDialogViewModel : BaseTopicDialogViewModel
    {
        [Required]
        [Display(Name = "Process Group*")]
        public string RotationTopicGroupId { get; set; }
        public IEnumerable<SelectListItem> RotationTopicGroups { get; set; }
    }
}
