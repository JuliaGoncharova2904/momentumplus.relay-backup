﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class ManagerCommentsViewModel
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Notes*")]
        [StringLength(300)]
        public string Notes { get; set; }
    }
}
