﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class RotationCompleteTaskOutcomesTabViewModel
    {
        public Guid ID { get; set; }

        [Display(Name = "Handback")]
        public bool IsFeedbackRequired { get; set; }

        [Display(Name = "Handback Notes")]
        [StringLength(500)]
        public string Feedback { get; set; }

        [Display(Name = "Task Name")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        [Display(Name = "Notes")]
        [StringLength(500)]
        public string TaskNotes { get; set; }

        public int AttacmentsCounter { get; set; }

        public int VoiceMessagesCounter { get; set; }
    }
}
