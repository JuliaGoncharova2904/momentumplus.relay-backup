﻿namespace MomentumPlus.Relay.Models
{
    public class RotationTaskCompleteViewModel
    {
        public RotationCompleteTaskTaskDetailsTabViewModel TaskDetailsTab { get; set; }
        public RotationCompleteTaskOutcomesTabViewModel OutcomesTab { get; set; }
        public RotationCompleteTaskTopicTabViewModel TopicDetailsTab { get; set; }
        public int SelectedTabNumber { get; set; }
        public CompleteDialogOption CompleteOption { get; set; }
    }
}
