﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class RotationTaskViewModel
    {
        public Guid? Id { get; set; }

        public Guid? OwnerId { get; set; }

        public Guid? TopicId { get; set; }

        public bool IsSwingMode { get; set; }

        [Display(Name = "Section")]
        public string Section { get; set; }

        [Required]
        [Display(Name = "Name*")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        [Display(Name = "Notes")]
        [StringLength(500)]
        public string Notes { get; set; }

        [Display(Name = "Assignee")]
        public Guid? AssignedTo { get; set; }

        [Required]
        [Display(Name = "Due Date*")]
        public DateTime? Deadline { get; set; }

        public DateTime StartDeadlineDates { get; set; }
        public DateTime EndDeadlineDates { get; set; }

        [Required]
        [Display(Name = "Priority*")]
        public TaskPriority Priority { get; set; }

        [Display(Name = "Tags")]
        public IEnumerable<string> Tags { get; set; }

        public string CompleteStatus { get; set; }

        [Display(Name = "Attacments")]
        public int AttacmentsCounter { get; set; }

        public bool HasNewAttacments { get; set; }

        [Display(Name = "Voice Messages")]
        public int VoiceMessagesCounter { get; set; }

        public bool HasNewVoiceMessages { get; set; }

        [Display(Name = "Handback")]
        public bool IsFeedbackRequired { get; set; }

        public TopicDetailsViewModel TopicDetails { get; set; }

        public TaskSendingOptions SendingOption { get; set; }

        public bool CanChangeAssignee { get; set; }

        public bool IsRotationMode { get; set; }

        public string TagsString
        {
            get
            {
                string tagsString = string.Empty;

                if (Tags != null)
                {
                    foreach(string tag in Tags)
                    {
                        tagsString = string.Format("{0} \"{1}\",", tagsString, tag);
                    }
                }

                return tagsString;
            }
        }
    }
}
