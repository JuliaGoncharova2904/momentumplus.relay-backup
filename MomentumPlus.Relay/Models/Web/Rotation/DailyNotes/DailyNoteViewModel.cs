﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class SafetyMessages
    {
        public string Version;
        public int MessagesNumber;
    };

    public class SafetyMessagesV1 : SafetyMessages
    {
        public Guid ImageId;
    };

    public class SafetyMessagesV2 : SafetyMessages
    {
        public string IconUrl;
    };

    public class DailyNoteViewModel
    {
        public Guid? Id { get; set; }

        public string Date { get; set; }

        public Guid RotationId { get; set; }

        [StringLength(1000)]
        [Display(Name = "Notes*")]
        public string Notes { get; set; }

        public bool HasContent { get; set; }

        public int TasksNumber { get; set; }

        public int AttachmentsNummer { get; set; }

        public bool ShowInReport { get; set; }

        public bool CanBeEdited { get; set; }

        public bool IsFuture { get; set; }

        public bool IsActiveInPanel { get; set; }

        public DateTime NormalDate { get; set; }

        public SafetyMessages SafetyMessages { get; set; }
    }
}
