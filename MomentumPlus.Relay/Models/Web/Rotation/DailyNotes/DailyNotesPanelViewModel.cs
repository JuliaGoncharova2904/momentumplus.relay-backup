﻿using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class DailyNotesPanelViewModel
    {
        public bool ShowEndSlide { get; set; }
        public IEnumerable<DailyNoteViewModel> DailyNotes { get; set; }
    }
}
