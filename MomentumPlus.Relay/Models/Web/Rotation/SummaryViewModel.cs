﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class SummaryViewModel
    {
        public Guid RotationId { get; set; }

        public Guid? CurrentShiftId { get; set; }

        public Guid RotarionOwnerId { get; set; }
        public string RotationOwnerFullName { get; set; }
        public bool IsViewMode { get; set; }
        public bool IsSwingExpired { get; set; }
        public bool IsRotationExpiredOrNotStarted { get; set; } 
    }
}
