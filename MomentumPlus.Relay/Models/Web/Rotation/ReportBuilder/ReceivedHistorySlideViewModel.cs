﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class ReceivedHistorySlideViewModel
    {
        public Guid Id { get; set; }
        public string Date { get; set; }
        public string ReceivedFrom { get; set; }
        public int ReceivedTopicCounter { get; set; }
        public int ReceivedTaskCounter { get; set; }
    }
}
