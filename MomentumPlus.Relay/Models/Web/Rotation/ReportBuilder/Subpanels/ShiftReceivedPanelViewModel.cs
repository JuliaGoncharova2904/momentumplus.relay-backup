﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class ShiftReceivedPanelViewModel
    {
        public Guid ShiftId { get; set; }
        public Guid? RecivedShiftId { get; set; }
        public IEnumerable<ShiftReceivedSlideViewModel> RecivedShifts { get; set; }
    }
}
