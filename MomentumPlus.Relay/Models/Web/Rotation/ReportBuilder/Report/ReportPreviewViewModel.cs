﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class ReportPreviewViewModel
    {
        public bool IsReceived { get; set; }

        public bool IsRotationFinished { get; set; }

        public Guid? CompanyLogoImageId { get; set; }

        public Guid RotationId { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public string Dates { get; set; }

        public Guid? SelectedRotationId { get; set; }

        public ReportModuleViewModel HSEModule { get; set; }

        public ReportModuleViewModel CoreModule { get; set; }

        public ReportModuleViewModel TeamModule { get; set; }

        public ReportModuleViewModel ProjectModule { get; set; }

        public ReportModuleViewModel DailyNotesModule { get; set; }
    }
}
