﻿namespace MomentumPlus.Relay.Models
{
    public class ReportTaskViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool? IsPinned { get; set; }
        public bool IsComplete { get; set; }
        public string Priority { get; set; }
        public bool IsNullReport { get; set; }
        public bool IsFinalized { get; set; }
        public bool IsFeedbackRequired { get; set; }

        public bool HasAttachments { get; set; }
        public bool HasVoiceMessages { get; set; }
        public string UserName { get; set; }
    }
}
