﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class ShiftReportPreviewViewModel
    {
        public bool IsReceived { get; set; }

        public bool IsShiftFinished { get; set; }

        public Guid? CompanyLogoImageId { get; set; }

        public Guid ShiftId { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public string Dates { get; set; }

        public string ReceivedFrom { get; set; }

        public Guid? SelectedShiftId { get; set; }

        public ReportShiftModuleViewModel HSEModule { get; set; }

        public ReportShiftModuleViewModel CoreModule { get; set; }

        public ReportShiftModuleViewModel TeamModule { get; set; }

        public ReportShiftModuleViewModel ProjectModule { get; set; }

    }
}
