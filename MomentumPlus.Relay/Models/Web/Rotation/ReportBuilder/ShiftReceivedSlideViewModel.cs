﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class ShiftReceivedSlideViewModel
    {
        public Guid Id { get; set; }
        public string Date { get; set; }
        public string HandoverFromName { get; set; }
        public Guid HandoverFromID { get; set; }
        public IEnumerable<ShiftContributorForReceivedPanel> Contributors { get; set; }
        public int TopicCounter { get; set; }
        public int TaskCounter { get; set; }
    }
}
