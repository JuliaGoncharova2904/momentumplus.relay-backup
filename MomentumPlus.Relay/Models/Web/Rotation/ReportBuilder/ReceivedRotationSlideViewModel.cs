﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class ReceivedRotationSlideViewModel
    {
        public Guid Id { get; set; }
        public string Date { get; set; }
        public string HandoverFrom { get; set; }
        public string Contributors { get; set; }
        public int TopicCounter { get; set; }
        public int TaskCounter { get; set; }
    }
}
