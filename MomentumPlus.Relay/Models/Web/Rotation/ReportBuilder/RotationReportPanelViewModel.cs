﻿using MvcPaging;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class RotationReportPanelViewModel
    {
        public Guid UserId { get; set; }

        public HandoverReportFilterType FilterType { get; set; }

        public bool IsQME { get; set; }
    }
}
