﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class FromToBlockViewModel
    {
        public Guid? RotationOwnerId { get; set; }
        public string RotationOwnerFullName { get; set; }
        public Guid? RotationBackToBackId { get; set; }
        public string RotationBackToBackFullName { get; set; }
    }
}
