﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models
{
    public class ShiftContributorForReceivedPanel
    {
        public string ContributorName { get; set; }
        public Guid ContributorID { get; set; }
    }
}
