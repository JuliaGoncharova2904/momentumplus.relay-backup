﻿using MvcPaging;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class ShiftReportPanelViewModel
    {
        public Guid? ShiftId { get; set; }

        public HandoverReportFilterType FilterType { get; set; }

        public bool IsQME { get; set; } 
    }
}
