﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class ChecklistViewModel
    {
        public Guid RotationId { get; set; }
        public Guid CurrentRotationId { get; set; }
        public bool IsViewMode { get; set; }
        public string RotationOwnerFullName { get; set; }
    }
}
