﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class ViewModeViewModel
    {
        public string RotationOwnerFullName { get; set; }
        public bool IsViewMode { get; set; }
    }
}
