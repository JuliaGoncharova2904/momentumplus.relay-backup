﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class TimelineRotationInfo
    {
        public DateTime StartDate;
        public int DayOn;
        public bool HasPrevRotations;
    }
}
