﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class DailyReportsPanelViewModel
    {
        public bool ShowFirstSlide { get; set; }

        public int UnreadSafetyMessagesCounter { get; set; }

        public IEnumerable<DailyReportViewModel> DailyReports { get; set; }
    }
}
