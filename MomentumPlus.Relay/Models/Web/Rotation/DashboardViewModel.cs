﻿using MvcPaging;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class DashboardViewModel
    {
        public Guid UserId { get; set; }

        public Guid RotationId { get; set; }

        public Guid ShiftId { get; set; }

        public bool IsViewMode { get; set; }

        public bool IsRotationExpiredOrNotStarted { get; set; }

        public string RotationOwnerFullName { get; set; }

        public int CoreCounter { get; set; }

        public int SafetyCounter { get; set; }

        public int TaskCounter { get; set; }

        public bool AllowEditRotation { get; set; }

    }
}
