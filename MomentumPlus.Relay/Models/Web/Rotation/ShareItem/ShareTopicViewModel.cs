﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class ShareTopicViewModel
    {
        public Guid TopicId { get; set; }

        [Display(Name = "Recipients")]
        public IEnumerable<Guid> RecipientsIds { get; set; }

        public IEnumerable<Guid> DeleteTopicRelationsIds { get; set; }

        public IEnumerable<SelectListItem> Recipients { get; set; }

        public IEnumerable<TopicRelationViewModel> TopicRelations { get; set; }
    }
}
