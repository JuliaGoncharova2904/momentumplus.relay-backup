﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class TopicRelationViewModel
    {
        public Guid TopicRelationId { get; set; }

        public Guid? RecipientId { get; set; }

        public string RecipientTitle { get; set; }

        public string RecipientPosition { get; set; }

        public Guid SourceTopicId { get; set; }

        public Guid? DestinationTopicId { get; set; }
    }
}
