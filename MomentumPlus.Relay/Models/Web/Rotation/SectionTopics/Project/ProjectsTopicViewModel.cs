﻿namespace MomentumPlus.Relay.Models
{
    public class ProjectsTopicViewModel : BaseTopicViewModel
    {
        public string Project { get; set; }

        public string Reference { get; set; }
    }
}
