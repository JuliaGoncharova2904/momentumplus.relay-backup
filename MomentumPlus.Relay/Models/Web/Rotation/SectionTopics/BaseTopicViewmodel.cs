﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class BaseTopicViewModel
    {
        public Guid? Id { get; set; }

        public Guid OwnerId { get; set; }

        public bool IsNullReport { get; set; }

        public bool IsCustom { get; set; }

        public int CarryforwardCounter { get; set; } 

        public bool IsFeedbackRequired { get; set; }

        public bool? IsPinned { get; set; }

        public bool HasComments { get; set; }

        public string Notes { get; set; }

        public Guid? RelationId { get; set; }

        public Guid? TeammateId { get; set; }

        public Guid? FromTeammateId { get; set; }

        public string FromTeammateFullName { get; set; }

        public Guid? FromRotationId { get; set; }

        public Guid? FromShiftId { get; set; }

        public bool HasLocation { get; set; }

        public bool HasTasks { get; set; }

        public bool HasAttachments { get; set; }

        public bool HasVoiceMessages { get; set; }

        public bool IsFinalized { get; set; }

        public bool IsSharingTopic { get; set; }

        public int SharedCounter { get; set; }
    }
}
