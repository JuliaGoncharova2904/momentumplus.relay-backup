﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class TaskBoardCompleteOutcomesTabViewModel
    {
        public Guid ID { get; set; }

        [Display(Name = "Handback")]
        public bool IsFeedbackRequired { get; set; }

        [Display(Name = "Handback Notes")]
        [StringLength(500)]
        public string Feedback { get; set; }

        public int AttacmentsCounter { get; set; }

        public int VoiceMessagesCounter { get; set; }

    }
}
