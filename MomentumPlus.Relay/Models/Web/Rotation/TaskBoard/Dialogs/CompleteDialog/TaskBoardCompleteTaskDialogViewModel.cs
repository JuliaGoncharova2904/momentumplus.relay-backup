﻿namespace MomentumPlus.Relay.Models
{
    public class TaskBoardCompleteTaskDialogViewModel
    {
        public TaskBoardCompleteTaskDetailTabViewModel TaskDetailTab { get; set; }
        public TaskBoardCompleteOutcomesTabViewModel TaskOutcomesTab { get; set; }
        public ProjectAdminProjectDetailsTabViewModel ProjectDetailsTab { get; set; }

        public bool HasTopic { get; set; }
        public int SelectedTabNumber { get; set; }
        public CompleteDialogOption CompleteOption { get; set; }
    }
}
