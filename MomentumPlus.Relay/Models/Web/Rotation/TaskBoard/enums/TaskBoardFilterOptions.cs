﻿using System.ComponentModel;

namespace MomentumPlus.Relay.Models
{
    public enum TaskBoardFilterOptions
    {
        [Description("All Tasks")]
        AllTasks = 0,

        [Description("Active Tasks")]
        ActiveTasks = 1,

        [Description("Incompleted Tasks")]
        IncompletedTasks = 2,

        [Description("Completed Tasks")]
        CompletedTasks = 3,

        [Description("Pending Tasks")]
        PendingTasks = 4,

        [Description("Archived Tasks")]
        ArchivedTasks = 5
    }
}
