﻿namespace MomentumPlus.Relay.Models
{
    public enum TaskBoardSortOptions
    {
        NotSort = 0,
        PriorityDirect = 1,
        PriorityReverse = 2,
        DueDateDirect = 3,
        DueDateReverse = 4,
    }
}
