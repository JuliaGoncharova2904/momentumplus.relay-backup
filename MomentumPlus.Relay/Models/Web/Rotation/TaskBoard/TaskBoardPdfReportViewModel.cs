﻿using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class TaskBoardPdfReportViewModel
    {
        public string ReportDate { get; set; }

        public IEnumerable<TaskBoardPdfReportItemViewModel> Tasks { get; set; }
    }
}
