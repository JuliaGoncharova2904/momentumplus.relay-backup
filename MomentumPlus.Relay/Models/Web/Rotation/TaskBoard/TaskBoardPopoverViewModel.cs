﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models
{
    public class TaskBoardPopoverViewModel
    {
        public TaskBoardSection Section { get; set; }

        public TaskBoardFilterOptions FilterOption { get; set; }
    }
}
