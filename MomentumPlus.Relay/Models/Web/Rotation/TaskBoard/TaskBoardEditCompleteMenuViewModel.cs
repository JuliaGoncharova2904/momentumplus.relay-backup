﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class TaskBoardEditCompleteMenuViewModel
    {
        public Guid UserId { get; set; }
        public Guid TaskId { get; set; }
    }
}
