﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class RegistrationViewModel
    {
        [Required]
        public BillingPeriodType BillingPeriodType { get; set; }

        [Required]
        public BillingCurrencyType BillingCurrencyType { get; set; }

        [Required]
        public UserSubscriptionType UserSubscriptionType { get; set; }

        [Required]
        public SubscriptionLevelType SubscriptionLevelType { get; set; }

        public RegistrationDetailsViewModel RegistrationDetails { get; set; }

        public PaymentDetailsViewModel PaymentDetailsViewModel { get; set; }

        public int ManagersUsersCount { get; set; }

        public int SimpleUsersCount { get; set; }

    }
}