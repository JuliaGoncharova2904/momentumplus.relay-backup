﻿namespace MomentumPlus.Relay.Models
{
    public enum BillingPeriodType
    {
        Monthly = 0,
        Annually = 1
    }
}
