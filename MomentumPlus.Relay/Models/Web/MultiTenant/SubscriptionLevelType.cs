﻿namespace MomentumPlus.Relay.Models
{
    public enum SubscriptionLevelType
    {
        Basic = 0,
        Premium = 1,
        Ultimate =2
    }
}
