﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class SafetyMessageV2ViewModel
    {
        public Guid Id { get; set; }
        public string Message { get; set; }
        public string MajorHazardName { get; set; }
        public string CriticalControlName { get; set; }
        public string IconUrl { get; set; }
    }
}
