﻿using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class SafetyMessagesForDateV2ViewModel
    {
        public string DateString { get; set; }
        public bool IsExpired { get; set; }
        public IEnumerable<SafetyMessageV2ViewModel> SafetyMessages { get; set; }
    }
}
