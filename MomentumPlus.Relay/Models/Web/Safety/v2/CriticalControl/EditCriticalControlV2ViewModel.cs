﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class EditCriticalControlV2ViewModel
    {
        public Guid Id { get; set; }
        public Guid MajorHazardId { get; set; }

        [Required]
        [Display(Name = "Critical Control*")]
        [StringLength(70, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "CCO*")]
        public Guid OwnerId { get; set; }

        [Required]
        [Display(Name = "CCC(s)*")]
        public IList<Guid> ChampionsIds { get; set; }

        public IEnumerable<SelectListItem> AllUsers { get; set; }
    }
}
