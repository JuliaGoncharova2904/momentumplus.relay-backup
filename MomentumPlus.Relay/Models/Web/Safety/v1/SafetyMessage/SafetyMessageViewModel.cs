﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class SafetyMessageViewModel
    {
        public Guid Id { get; set; }
        public string Message { get; set; }
        public string MajorHazardName { get; set; }
        public string CriticalControlName { get; set; }
        public Guid IconId { get; set; }
    }
}
