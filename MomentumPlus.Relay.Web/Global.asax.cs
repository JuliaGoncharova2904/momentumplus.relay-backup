﻿using System;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using JavaScriptEngineSwitcher.Core;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Web.Extensions;
using NLog;

namespace MomentumPlus.Relay.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static Logger _logger = LogManager.GetLogger("Application.Error");

        private static IServicesUnitOfWork _servieceServicesUnitOfWork = DependencyResolver.Current.GetService<IServicesUnitOfWork>();

        private static bool _startupPerformed = false;

        private static readonly object _startupLock = new object();


        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            JsEngineSwitcherConfig.Configure(JsEngineSwitcher.Instance);
        }

        private void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();

            if (ex is HttpAntiForgeryException)
            {
                Response.Clear();

                Response.Redirect("/");
            }

            _logger.Error(ex, "Application Error.");

        }


        public void Application_BeginRequest()
        {
            RunStartupTasks();
        }

        private void RunStartupTasks()
        {
            if (!_startupPerformed)
            {
                lock (_startupLock)
                {
                    if (!_startupPerformed)
                    {
                        _startupPerformed = true;

                        InitializeAdminSettings();

                    }
                }
            }
        }


        private void InitializeAdminSettings()
        {
            _servieceServicesUnitOfWork.AdministrationService.InitializeAdminSettings();
        }
    }
}
