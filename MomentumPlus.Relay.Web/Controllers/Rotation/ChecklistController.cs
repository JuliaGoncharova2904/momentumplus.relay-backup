﻿using System;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System.Web.Mvc;
using System.Collections.Generic;
using MomentumPlus.Relay.Web.Extensions;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ChecklistController : BaseController
    {
        public ChecklistController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork) { }

        public ActionResult Index(Guid? userId, Guid? filterId)
        {
            Guid currentUserId = userId ?? Guid.Parse(User.Identity.GetUserId());

            Guid? viewedRotationId = Services.RotationService.GetCurrentRotationIdForUser(currentUserId);

            //IsMyRotationViewMode = !userId.HasValue;

            if (viewedRotationId == null || !Services.RotationService.RotationIsConfirmed(viewedRotationId.Value) && !Services.RotationService.IsRotationHavePrevRotation(viewedRotationId.Value))
            {
                ViewModeViewModel viewModeModel = new ViewModeViewModel
                {
                    IsViewMode = IsViewMode,
                    RotationOwnerFullName = !IsViewMode ? string.Empty : Services.UserService.GetEmployeeById(userId.Value).Name
                };

                return View("~/Views/Rotation/Shared/NoRotation.cshtml", viewModeModel);
            }

            ChecklistViewModel model = Services.ChecklistService.GetChecklistForRotation(viewedRotationId.Value, currentUserId);
            model.IsViewMode = userId.HasValue;

            if (filterId.HasValue)
            {
                model.RotationOwnerFullName = Services.RotationService.GetRotationOwnerFullName(filterId.Value);
                model.RotationId = filterId.Value;
            }

            return View("~/Views/Rotation/Checklist/Index.cshtml", model);
        }

        public ActionResult RorationFilter(Guid filterId)
        {
            ChecklistRotationFilterViewModel model = Services.ChecklistService.GetRotationsForFilter(filterId);

            if (model != null)
            {
                model.RotationId = filterId;

                return PartialView("~/Views/Rotation/Checklist/_ChecklistRotationFilter.cshtml", model);
            }

            return new HttpNotFoundResult();
        }

        public ActionResult TeamMember(Guid? rotationId)
        {
       rotationId = rotationId ?? Services.RotationService.GetCurrentRotationIdForUser(Guid.Parse(User.Identity.GetUserId()));


            if (rotationId.HasValue)
            {
                TeamCheckListViewModel model = Services.ChecklistService.GetTeamForCheckList(rotationId.Value);

                if (model != null)
                    return PartialView("~/Views/Rotation/Checklist/_TeamMemberChecklist.cshtml", model);
            }

            return new HttpNotFoundResult();
        }

        public ActionResult ReceivedTasksPanel(Guid RotationId)
        {
            if (RotationId != Guid.Empty)
            {
                if (Services.RotationService.IsShiftRotation(RotationId))
                {
                    IEnumerable<TasksTopicViewModel> viewModel = Services.RotationTaskService.GetShiftReceivedTasks(RotationId);

                    return PartialView("~/Views/Rotation/Checklist/_ReceivedTasksPanel.cshtml", viewModel);
                }
                else
                {
                    IEnumerable<TasksTopicViewModel> viewModel = Services.RotationTaskService.GetReceivedTasks(RotationId);
                    return PartialView("~/Views/Rotation/Checklist/_ReceivedTasksPanel.cshtml", viewModel);
                }
            }

            return new HttpNotFoundResult();
        }

        [RequireRequestValue("UserIds")]
        public ActionResult ReceivedTasksPanel(Guid RotationId, IEnumerable<Guid> UserIds)
        {
            if (RotationId != Guid.Empty && UserIds != null)
            {
                if (Services.RotationService.IsShiftRotation(RotationId))
                {
                    List<TasksTopicViewModel> viewModel = new List<TasksTopicViewModel>();
                    foreach (var item in UserIds)
                    {
                        viewModel.AddRange(Services.RotationTaskService.GetShiftReceivedTasks(RotationId, item));
                    }
                    return PartialView("~/Views/Rotation/Checklist/_ReceivedTasksPanel.cshtml", viewModel);
                }
                else
                {
                    List<TasksTopicViewModel> viewModel = new List<TasksTopicViewModel>();
                    foreach (var item in UserIds)
                    {
                        viewModel.AddRange(Services.RotationTaskService.GetReceivedTasks(RotationId, item));
                    }
                    return PartialView("~/Views/Rotation/Checklist/_ReceivedTasksPanel.cshtml", viewModel);
                }  
            }

            return new HttpNotFoundResult();
        }

        public ActionResult ReloadTopic(Guid topicid)
        {
            if (topicid != Guid.Empty)
            {
                TasksTopicViewModel model = Services.RotationTaskService.GetTaskTopic(topicid);
                if (model != null)
                {
                    return Json(model);
                }
            }

            return new HttpNotFoundResult();
        }
    }
}