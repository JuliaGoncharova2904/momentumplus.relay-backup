﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class TeamController : BaseController
    {
        // IOC
        public TeamController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult ChooseTeammateDialog(Guid userId, bool? showMe, bool? showOnSite, bool? showOffSite, bool? showWithoutRotation)
        {
            TeammatesDialogViewModel model = Services.TeamService.PopulateTeammatesDialogViewModel(userId,
                                                                                                    showMe ?? false,
                                                                                                    showOnSite ?? false,
                                                                                                    showOffSite ?? false,
                                                                                                    showWithoutRotation ?? false);

            return PartialView("~/Views/Rotation/Team/_ChooseTeammateDialog.cshtml", model);
        }

    }
}