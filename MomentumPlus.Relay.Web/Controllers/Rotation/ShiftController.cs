﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ShiftController : BaseController
    {
        //IOC
        public ShiftController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        public ActionResult Index(Guid? rotationId)
        {
            return RedirectToAction("Index", "Summary", new { rotationId });
        }


        #region Confirm Shift

        [HttpGet]
        public ActionResult ConfirmShiftDialog()
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            ConfirmShiftViewModel model = Services.ShiftService.PopulateConfirmShiftViewModel(userId);

            return PartialView("~/Views/Rotation/Confirmation/_ConfirmShiftDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmShiftDialog(ConfirmShiftViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.StartShiftDate > model.EndShiftDate)
                {
                    ModelState.AddModelError("StartShiftDate", "Start shift date сan not be less than End shift date");
                }
                else if (model.EndShiftDate > model.NextShiftDate)
                {
                    ModelState.AddModelError("EndShiftDate", "End shift date сan not be less than end Next shift date");
                }
                else if (model.EndShiftDate < DateTime.Now)
                {
                    ModelState.AddModelError("EndShiftDate", "End shift date сan not be in the past");
                }
                else if (model.NextShiftDate < DateTime.Now)
                {
                    ModelState.AddModelError("NextShiftDate", "Next shift date сan not be in the past");
                }
                else
                {
                    Guid userId = Guid.Parse(User.Identity.GetUserId());
                    Services.ShiftService.ConfirmShift(userId, model);

                    return Content("ok");
                }
            }

            return PartialView("~/Views/Rotation/Confirmation/_ConfirmShiftDialog.cshtml", model);
        }

        #endregion

        #region Shift Recipient

        [HttpGet]
        public ActionResult ChooseShiftRecipient(Guid shiftId)
        {
            ChooseShiftRecipientModel model = Services.ShiftService.PopulateChooseShiftRecipientViewModel(shiftId);

            return PartialView("~/Views/Rotation/ShiftReportBuilder/_HandoverRecipientsDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChooseShiftRecipient(ChooseShiftRecipientModel model)
        {
            if (ModelState.IsValid)
            {
                Services.ShiftService.UpdateShiftRecipient(model);

                return Content("ok");
            }

            model.TeamMembers = Services.ShiftService.PopulateChooseShiftRecipientViewModel(model.ShiftId).TeamMembers;

            return PartialView("~/Views/Rotation/ShiftReportBuilder/_HandoverRecipientsDialog.cshtml", model);
        }


        #endregion

        #region Edit Shift

        [HttpGet]
        public ActionResult EditShiftForm(Guid shiftId)
        {
            EditShiftDatesViewModel model = Services.ShiftService.GetShiftDates(shiftId);

            return PartialView("~/Views/Rotation/Confirmation/Edit/_EditShiftForm.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditShiftForm(EditShiftDatesViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!model.EndShiftDateReadOnly && model.EndShiftDate < DateTime.Now)
                {
                    ModelState.AddModelError("EndShiftDate", "End Shift Time has to be great or equal current Time.");
                }
                else if (model.NextShiftDate <= DateTime.Now)
                {
                    ModelState.AddModelError("NextShiftDate", "Next Shift Time has to be great then current Time.");
                }
                else
                {
                    Services.ShiftService.UpdateRotationDates(model);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Rotation/Confirmation/Edit/_EditShiftForm.cshtml", model);
        }

        #endregion

        #region Finish Shift

        [HttpPost]
        public ActionResult FinishShift(Guid shiftId)
        {
            Services.ShiftService.ExpireShift(shiftId);

            return Content("ok");
        }


        #endregion
    }
}