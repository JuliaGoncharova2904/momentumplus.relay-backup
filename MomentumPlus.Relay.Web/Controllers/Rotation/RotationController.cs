﻿using System;
using System.Web.Mvc;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System.Net;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class RotationController : BaseController
    {
        //IOC
        public RotationController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        public ActionResult Index(Guid? userId)
        {
            return RedirectToAction("Index", "Summary", new { userId });
        }

        #region Test functions

        public ActionResult ExpireRotation(Guid? userId, Guid? rotationId)
        {
            if (rotationId.HasValue)
            {
                var expired = Services.RotationService.ExpireRotation((Guid)rotationId);

                if (expired)
                {
                    return RedirectToAction("Index", "Summary", new { userId });
                }
            }

            return RedirectToAction("Index", "Summary", new { userId });
        }

        public ActionResult EndSwingOfRotation(Guid? userId, Guid? rotationId)
        {
            if (rotationId.HasValue)
            {
                var ended = Services.RotationService.EndSwingOfRotation(rotationId.Value);

                if (ended)
                {
                    return RedirectToAction("Index", "Summary", new { userId });
                }
            }

            return RedirectToAction("Index", "Summary", new { userId });
        }

        #endregion


        #region Check current period state

        public ActionResult ConfirmShiftRotation()
        {
            if (!OffSiteUserAsked)
            {
                Guid employeeId = Guid.Parse(User.Identity.GetUserId());
                Guid? rotationId = Services.RotationService.GetCurrentRotationIdForUser(employeeId);

                if (rotationId.HasValue)
                {
                    if (!Services.RotationService.RotationIsConfirmed(rotationId.Value) || Services.RotationService.IsRotationSwingEnd(rotationId.Value))
                    {
                        return PartialView("~/Views/Rotation/Confirmation/_ConfirmRotation.cshtml");
                    }
                    else if (Services.RotationService.IsShiftRotation(rotationId.Value))
                    {
                        if (!Services.ShiftService.RotationHasWorkingShift(rotationId.Value))
                        {
                            return PartialView("~/Views/Rotation/Confirmation/_ConfirmShift.cshtml");
                        }

                        Guid? shiftId = Services.ShiftService.GetCurrentShiftIdForRotation(rotationId.Value);
                        if (!Services.ShiftService.ShiftHasRecipient(shiftId.Value))
                        {
                            return PartialView("~/Views/Rotation/Confirmation/_ChooseShiftRecipient.cshtml", shiftId.Value);
                        }
                    }
                }
            }

            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult NotBackOnSite()
        {
            OffSiteUserAsked = true;
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        #endregion

        #region Confirm Rotation

        [HttpGet]
        public ActionResult ConfirmRotationDialog()
        {
            Guid employeeId = Guid.Parse(User.Identity.GetUserId());
            ConfirmRotationViewModel model = Services.RotationService
                                                     .PopulateConfirmModelWithCurrentRotation(employeeId)
                                                     .FillRepeatRotationRange();

            return PartialView("~/Views/Rotation/Confirmation/_ConfirmRotationDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmRotationDialog(ConfirmRotationViewModel model)
        {
            if (ModelState.IsValid)
            {
                Guid employeeId = Guid.Parse(User.Identity.GetUserId());

                if (model.ConfirmState != ConfirmRotationState.AcceptCustomPatern && !Services.RotationService.CheckRotationPatternCompatibility(employeeId, model))
                {
                    ModelState.Remove("ConfirmState");
                    model.ConfirmState = ConfirmRotationState.RequestCustomPatern;
                }
                else
                {
                    if (Services.RotationService.ConfirmRotation(employeeId, model))
                    {
                        return Content("ok");
                    }
                    else
                    {
                        ModelState.Remove("ConfirmState");
                        model.ConfirmState = ConfirmRotationState.NoOperation;
                        ModelState.AddModelError("Confirm", "Rotation was not confirmed.");
                    }
                }
            }

            return PartialView("~/Views/Rotation/Confirmation/_ConfirmRotationDialog.cshtml", model.FillRepeatRotationRange());
        }

        #endregion

        #region Edit Rotation

        [HttpGet]
        public ActionResult EditSwingShiftDialog(Guid userId)
        {
            Guid? rotationId = Services.RotationService.GetCurrentRotationIdForUser(userId);

            if (rotationId.HasValue && !Services.RotationService.IsRotationExpiredOrNotStarted(rotationId.Value))
            {
                if (Services.RotationService.IsShiftRotation(rotationId.Value))
                {
                    Guid? shiftId = Services.ShiftService.GetCurrentShiftIdForRotation(rotationId.Value);

                    if (shiftId.HasValue)
                    {
                        EditShiftRotationViewModel model = new EditShiftRotationViewModel
                        {
                            RotationId = rotationId.Value,
                            ShiftId = shiftId.Value
                        };

                        return PartialView("~/Views/Rotation/Confirmation/Edit/_EditSwingShiftDialog.cshtml", model);
                    }
                }

                return PartialView("~/Views/Rotation/Confirmation/Edit/_EditRotationDatesDialog.cshtml", rotationId.Value);
            }

            return PartialView("~/Views/Rotation/Confirmation/_WarningEditDatesDialog.cshtml");
        }




        [HttpGet]
        public ActionResult EditSwingForm(Guid rotationId)
        {
            EditRotationDatesViewModel model = Services.RotationService.GetRotationDates(rotationId);

            return PartialView("~/Views/Rotation/Confirmation/Edit/_EditSwingForm.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditSwingForm(EditRotationDatesViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!model.EndSwingDateReadOnly && model.EndSwingDate.Value.Date < DateTime.Now.Date)
                {
                    ModelState.AddModelError("EndSwingDate", "End Swing Date has to be great or equal current date.");
                }
                else if (model.BackOnSiteDate.Value.Date <= DateTime.Now.Date)
                {
                    ModelState.AddModelError("BackOnSiteDate", "Back On Site Date has to be great then current date.");
                }
                else if (Services.RotationService.UpdateRotationDates(model))
                {
                    return Content("ok");
                }
                else
                {
                    ModelState.AddModelError("", "Rotation was not confirmed.");
                }
            }

            return PartialView("~/Views/Rotation/Confirmation/Edit/_EditSwingForm.cshtml", model.FillRepeatRotationRange());
        }

        #endregion
    }
}