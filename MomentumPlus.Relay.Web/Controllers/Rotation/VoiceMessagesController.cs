﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class VoiceMessagesController : BaseController
    {
        // IOC
        public VoiceMessagesController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        #region Topic

        [HttpGet]
        public ActionResult VoiceMessagesListForTopicDialog(Guid Id)
        {
            IEnumerable<VoiceMessageViewModel> model = Services.VoiceMessageService.GetVoiceMessagesForTopic(Id);

            if(model != null)
                return PartialView("~/Views/Rotation/VoiceMessages/_VoiceMessagesListDialog.cshtml", model);

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult AddVoiceMessageForTopic(Guid Id)
        {
            if (Id != Guid.Empty)
            {
                VoiceMessageViewModel voiceMessage = new VoiceMessageViewModel { TargetId = Id };
                return PartialView("~/Views/Rotation/VoiceMessages/_AddVoiceMessageForm.cshtml", voiceMessage);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddVoiceMessageForTopic(VoiceMessageViewModel voiceMessage)
        {
            if (voiceMessage.TargetId.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.VoiceMessageService.AddVoiceMessageToTopic(voiceMessage))
                    {
                        return Content("ok");
                    }
                }
                else
                {
                    return PartialView("~/Views/Rotation/VoiceMessages/_AddVoiceMessageForm.cshtml", voiceMessage);
                }
            }

            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult RemoveVoiceMessageFromTopic(Guid Id, Guid voiceMessageId)
        {
            if (Id != Guid.Empty && voiceMessageId != Guid.Empty)
            {
                if (Services.VoiceMessageService.RemoveVoiceMessageFromTopic(Id, voiceMessageId))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.OK);
                }
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        #endregion

        #region Task

        [HttpGet]
        public ActionResult VoiceMessagesListForTaskDialog(Guid Id)
        {
            IEnumerable<VoiceMessageViewModel> model = Services.VoiceMessageService.GetVoiceMessagesForTask(Id);

            if(model != null)
                return PartialView("~/Views/Rotation/VoiceMessages/_VoiceMessagesListDialog.cshtml", model);

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult AddVoiceMessageForTask(Guid Id)
        {
            if (Id != Guid.Empty)
            {
                VoiceMessageViewModel voiceMessage = new VoiceMessageViewModel { TargetId = Id };
                return PartialView("~/Views/Rotation/VoiceMessages/_AddVoiceMessageForm.cshtml", voiceMessage);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddVoiceMessageForTask(VoiceMessageViewModel voiceMessage)
        {
            if (voiceMessage.TargetId.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.VoiceMessageService.AddVoiceMessageToTask(voiceMessage))
                    {
                        return Content("ok");
                    }
                }
                else
                {
                    return PartialView("~/Views/Rotation/VoiceMessages/_AddVoiceMessageForm.cshtml", voiceMessage);
                }
            }

            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult RemoveVoiceMessageFromTask(Guid Id, Guid voiceMessageId)
        {
            if (Id != Guid.Empty && voiceMessageId != Guid.Empty)
            {
                if (Services.VoiceMessageService.RemoveVoiceMessageFromTask(Id, voiceMessageId))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.OK);
                }
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        #endregion
    }
}