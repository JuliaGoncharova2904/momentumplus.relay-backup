﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Web.Mvc;
using MomentumPlus.Core.Authorization;
using System.Net;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class TopicController : BaseController
    {
        // IOC
        public TopicController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        public ActionResult EditTopicDialog(Guid topicid)
        {
            ModuleType section = Services.RotationTopicService.GetTopicModuleType(topicid);

            switch (section)
            {
                case ModuleType.HSE:
                    return RedirectToAction("EditTopicDialog", "HSESection", new { topic = topicid });
                case ModuleType.Core:
                    return RedirectToAction("EditTopicDialog", "CoreSection", new { topic = topicid });
                case ModuleType.Project:
                    return RedirectToAction("EditTopicDialog", "ProjectsSection", new { topic = topicid });
                case ModuleType.Team:
                    return RedirectToAction("EditTopicDialog", "TeamSection", new { topic = topicid });

                case ModuleType.DailyNote:
                default:
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public ActionResult ManagerCommentButton(ManagerCommentButtonViewModel model)
        {
            var currentUserId = Guid.Parse(User.Identity.GetUserId());

            if (model.ItemId.HasValue)
            {
                var isMyTopic = Services.RotationTopicService.IsMyTopic(currentUserId, model.ItemId.Value);

                if (User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) && isMyTopic == false ||
                    User.IsInRole(iHandoverRoles.Relay.ExecutiveUser) && isMyTopic == false ||
                    User.IsInRole(iHandoverRoles.Relay.Administrator) && isMyTopic == false ||
                    User.IsInRole(iHandoverRoles.Relay.HeadLineManager) && isMyTopic == false ||
                    User.IsInRole(iHandoverRoles.Relay.LineManager) && isMyTopic == false)
                {
                    model.IsActive = true;
                }

            }

            return PartialView("~/Views/Rotation/Shared/ManagerComments/_ManagerCommentButton.cshtml", model);
        }


        [HttpGet]
        public ActionResult ManagerCommentsDialog(Guid? Id)
        {
            if (Id != null)
            {

                var isMyTopic = Services.RotationTopicService.IsMyTopic(Guid.Parse(User.Identity.GetUserId()), (Guid)Id);

                var editEnable = User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) && !isMyTopic ||
                                 User.IsInRole(iHandoverRoles.Relay.ExecutiveUser) && !isMyTopic ||
                                 User.IsInRole(iHandoverRoles.Relay.Administrator) && !isMyTopic ||
                                 User.IsInRole(iHandoverRoles.Relay.HeadLineManager) && !isMyTopic ||
                                 User.IsInRole(iHandoverRoles.Relay.LineManager) && !isMyTopic;

                var model = Services.RotationTopicService.PopulateManagerCommentViewModel(Id.Value);

                if (editEnable)
                {
                    return PartialView("~/Views/Rotation/Shared/ManagerComments/_ManagerCommentsFormDialog.cshtml", model);
                }

                return PartialView("~/Views/Rotation/Shared/ManagerComments/_ManagerCommentsDetailsDialog.cshtml", model);

            }
            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ManagerCommentsDialog(ManagerCommentsViewModel model)
        {
            if (ModelState.IsValid)
            {
                Services.RotationTopicService.UpdateTopicManagerComment(model);
                return Content("ok");
            }

            return PartialView("~/Views/Rotation/Shared/ManagerComments/_ManagerCommentsFormDialog.cshtml", model);
        }

        [HttpPost]
        public ActionResult CarryforwardTopic(Guid topicId)
        {
            if (topicId != Guid.Empty)
            {
                Services.RotationTopicService.CarryforwardTopic(topicId);

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        public ActionResult RemoveTopic(Guid topicId)
        {
            if (topicId != Guid.Empty && Services.RotationTopicService.RemoveTopic(topicId))
            {
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        #region Location

        [HttpGet]
        public ActionResult DetailsLocationFormDialog(Guid topicId)
        {
            LocationViewModel model = Services.LocationService.GetLocationForTopic(topicId);
            if (model != null)
            {
                return PartialView("~/Views/Rotation/Topic/_LocationDetailsFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult CreateLocationFormDialog(Guid topicId)
        {
            if (topicId != Guid.Empty)
            {
                TempData["CreateLocation_TopicId"] = topicId;
                ViewBag.Title = "Create Location";
                return PartialView("~/Views/Rotation/Topic/_LocationFormDialog.cshtml");
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateLocationFormDialog(LocationViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (TempData["CreateLocation_TopicId"] != null)
                {
                    Guid topicId = (Guid)TempData["CreateLocation_TopicId"];
                    if (Services.LocationService.CreateLocationForTopic(topicId, model))
                    {
                        return Content("ok");
                    }
                }

                return HttpNotFound();
            }
            else
            {
                ViewBag.Title = "Edit Location";
                return PartialView("~/Views/Rotation/Topic/_LocationFormDialog.cshtml", model);
            }
        }

        [HttpGet]
        public ActionResult EditLocationFormDialog(Guid topicId)
        {
            LocationViewModel model = Services.LocationService.GetLocationForTopic(topicId);
            if (model != null)
            {
                TempData["EditLocation_TopicId"] = topicId;
                ViewBag.Title = "Edit Location";
                return PartialView("~/Views/Rotation/Topic/_LocationFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditLocationFormDialog(LocationViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (TempData["EditLocation_TopicId"] != null)
                {
                    Guid topicId = (Guid)TempData["EditLocation_TopicId"];
                    if (Services.LocationService.UpdateLocation(model))
                    {
                        return Content("ok");
                    }
                }

                return HttpNotFound();
            }
            else
            {
                ViewBag.Title = "Edit Location";
                return PartialView("~/Views/Rotation/Topic/_LocationFormDialog.cshtml", model);
            }
        }

        #endregion
    }
}