﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class AttachmentsController : BaseController
    {
        // IOC
        public AttachmentsController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        #region Topic

        [HttpGet]
        public ActionResult AttachmentsListForTopicDialog(Guid Id)
        {
            IEnumerable<AttachmentViewModel> model = Services.AttachmentService.GetAttachmentForTopic(Id);

            if (model != null)
                return PartialView("~/Views/Rotation/Attachments/_AttachmentsListDialog.cshtml", model);

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult AddAttachmentForTopic(Guid Id)
        {
            if (Id != Guid.Empty)
            {
                AttachmentViewModel attachment = new AttachmentViewModel { TargetId = Id };
                return PartialView("~/Views/Rotation/Attachments/_AddAttachmentForm.cshtml", attachment);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddAttachmentForTopic(AttachmentViewModel attachment)
        {
            if (attachment.TargetId.HasValue)
            {
                if (ModelState.IsValid)
                {
                    Services.AttachmentService.AddAttachmentToTopic(attachment);
                    return Content("ok");
                }
                else
                {
                    return PartialView("~/Views/Rotation/Attachments/_AddAttachmentForm.cshtml", attachment);
                }
            }

            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult RemoveAttachmentFromTopic(Guid Id, Guid attachmentId)
        {
            if (Id != Guid.Empty && attachmentId != Guid.Empty)
            {
                Services.AttachmentService.RemoveAttachmentFromTopic(Id, attachmentId);
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        #endregion

        #region Task

        [HttpGet]
        public ActionResult AttachmentsListForTaskDialog(Guid Id)
        {
            IEnumerable<AttachmentViewModel> model = Services.AttachmentService.GetAttachmentForTask(Id);

            if (model != null)
                return PartialView("~/Views/Rotation/Attachments/_AttachmentsListDialog.cshtml", model);

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult AddAttachmentForTask(Guid Id)
        {
            if (Id != Guid.Empty)
            {
                AttachmentViewModel attachment = new AttachmentViewModel { TargetId = Id };
                return PartialView("~/Views/Rotation/Attachments/_AddAttachmentForm.cshtml", attachment);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddAttachmentForTask(AttachmentViewModel attachment)
        {
            if (attachment.TargetId.HasValue)
            {
                if (ModelState.IsValid)
                {
                    Services.AttachmentService.AddAttachmentToTask(attachment);
                    return Content("ok");
                }
                else
                {
                    return PartialView("~/Views/Rotation/Attachments/_AddAttachmentForm.cshtml", attachment);
                }
            }

            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult RemoveAttachmentFromTask(Guid Id, Guid attachmentId)
        {
            if (Id != Guid.Empty && attachmentId != Guid.Empty)
            {
                Services.AttachmentService.RemoveAttachmentFromTask(Id, attachmentId);
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        #endregion

    }
}