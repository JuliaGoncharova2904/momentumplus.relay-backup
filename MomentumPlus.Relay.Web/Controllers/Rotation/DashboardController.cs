﻿using System;
using MomentumPlus.Relay.Interfaces;
using System.Web.Mvc;
using MomentumPlus.Relay.Models;
using MvcPaging;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class DashboardController : BaseController
    {
        // IOC
        public DashboardController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        // GET: ReportBuilder/ReportBuilder
        public ActionResult Index(Guid? userId)
        {
            var currentUserId = userId ?? Guid.Parse(User.Identity.GetUserId());

            var viewedRotationId = Services.RotationService.GetCurrentRotationIdForUser(currentUserId);

            if (viewedRotationId == null  || !Services.RotationService.RotationIsConfirmed(viewedRotationId.Value) && !Services.RotationService.IsRotationHavePrevRotation(viewedRotationId.Value))
            {
                ViewModeViewModel viewModeModel = new ViewModeViewModel
                {
                    IsViewMode = IsViewMode,
                    RotationOwnerFullName = !IsViewMode ? string.Empty : Services.UserService.GetEmployeeById(userId.Value).Name
                }; 

                return View("~/Views/Rotation/Shared/NoRotation.cshtml", viewModeModel);
            }

            DashboardViewModel model = Services.RotationService.GetDashboardForRotation(viewedRotationId.Value, currentUserId);
            model.IsViewMode = userId.HasValue;

            return View("~/Views/Rotation/Dashboard/Index.cshtml", model);
            //----------------------------------------------
        }

        public ActionResult MyTeamWidget(Guid? rotationId, int? page)
        {
            if (rotationId.HasValue)
            {
                var rotation = Services.RotationService.GetRotation(rotationId.Value);
                var viewModel = Services.UserService.GetTeamForWidjet(rotationId.Value, Guid.Parse(rotation.RotationOwnerId)).ToPagedList(page - 1 ?? 0, 1);
                return PartialView("~/Views/Rotation/Dashboard/_MyTeamWidget.cshtml", viewModel);
            }
            return null;
        }

        public ActionResult TimeLinePanel(Guid? rotationId)
        {
            if (rotationId.HasValue)
            {
                var model = Services.TimelineService.GetLastTwoYearsTimeline(rotationId.Value);
                return PartialView("~/Views/Rotation/Dashboard/_TimelinePanel.cshtml", model);
            }
            return null;
        }
    }
}