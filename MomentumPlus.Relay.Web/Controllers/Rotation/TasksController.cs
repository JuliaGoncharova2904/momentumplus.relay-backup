﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using MomentumPlus.Core.Authorization;
using System.Net;
using Microsoft.AspNet.Identity;
using Rotativa;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class TasksController : BaseController
    {
        // IOC
        public TasksController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult DraftTasksListDialog(Guid TopicId)
        {
            if (TopicId != Guid.Empty)
            {
                IEnumerable<RotationTaskViewModel> model = Services.RotationTaskService.GetTopicTasks(TopicId);

                return PartialView("~/Views/Rotation/Tasks/_DraftTasksListDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult ReceivedTasksListDialog(Guid TopicId)
        {
            if (TopicId != Guid.Empty)
            {
                IEnumerable<RotationTaskViewModel> model = Services.RotationTaskService.GetTopicTasks(TopicId);

                return PartialView("~/Views/Rotation/Tasks/_ReceivedTasksListDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        #region Add Task

        [HttpGet]
        public ActionResult AddTaskFormDialog(Guid TopicId)
        {
            if (TopicId != Guid.Empty && Services.RotationTopicService.IsTopicExist(TopicId))
            {
                RotationTaskViewModel model = new RotationTaskViewModel
                {
                    TopicId = TopicId,
                    SendingOption = TaskSendingOptions.EndOfSwing
                };

                model = Services.RotationTaskService.PopulateTaskModel(model);
                return PartialView("~/Views/Rotation/Tasks/_AddTaskFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddTaskFormDialog(RotationTaskViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.TopicId.HasValue)
                {
                    if (Services.RotationTaskService.IsTopicTaskExist(model.TopicId.Value, model.Name))
                    {
                        ModelState.AddModelError("Name", "Task with the same name already exist.");
                    }
                    else
                    {
                        Services.RotationTaskService.AddTopicTask(model.TopicId.Value, model);
                        return Content("ok");
                    }
                }
            }

            Services.RotationTaskService.PopulateTaskModel(model);

            return PartialView("~/Views/Rotation/Tasks/_AddTaskFormDialog.cshtml", model);
        }

        #endregion

        #region Edit Task

        [HttpGet]
        public ActionResult EditTaskFormDialog(Guid TaskId)
        {
            if (TaskId != Guid.Empty)
            {
                if (Services.RotationTaskService.IsChildTaskComplited(TaskId))
                {
                    return RedirectToAction("TaskDetailsDialog", new { TaskId = TaskId });
                }
                else
                {
                    RotationTaskViewModel model = Services.RotationTaskService.IsShiftTask(TaskId) ?
                    Services.RotationTaskService.GetShiftTask(TaskId) : Services.RotationTaskService.GetTask(TaskId);
                    if (model != null)
                    {
                        return PartialView("~/Views/Rotation/Tasks/_EditTaskFormDialog.cshtml", model);
                    }
                }
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTaskFormDialog(RotationTaskViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Id.HasValue)
                {
                    if (Services.RotationTaskService.IsTaskNameExist(model.Id.Value, model.Name))
                    {
                        ModelState.AddModelError("Name", "Task with the same name already exist.");
                    }
                    else
                    {
                        Services.RotationTaskService.UpdateTopicTask(model);
                        return Content("ok");
                    }
                }
                else
                {
                    ModelState.AddModelError("Id", "Task Id is not exist.");
                }
            }

            Services.RotationTaskService.PopulateTaskModel(model);

            return PartialView("~/Views/Rotation/Tasks/_EditTaskFormDialog.cshtml", model);
        }

        #endregion

        #region Edit Received Task

        [HttpGet]
        public ActionResult EditReceivedTaskDialog(Guid taskId)
        {
            RotationTaskViewModel model = Services.RotationTaskService.IsShiftTask(taskId) ?
                             Services.RotationTaskService.GetShiftTask(taskId) : Services.RotationTaskService.GetTask(taskId);

            return PartialView("~/Views/Rotation/Tasks/_EditReceivedTaskFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditReceivedTaskDialog(RotationTaskViewModel model)
        {
            if (ModelState.IsValid)
            {

                var currentUserId = Guid.Parse(User.Identity.GetUserId());

                Services.RotationTaskService.EditReceivedTask(currentUserId, model);
                return Content("ok");
            }

            Services.RotationTaskService.PopulateTaskModel(model);
            return PartialView("~/Views/Rotation/Tasks/_EditReceivedTaskFormDialog.cshtml", model);
        }


        #endregion

        #region Complete Task

        [HttpGet]
        public ActionResult CompleteTaskDialog(Guid TaskId)
        {
            if (TaskId != Guid.Empty)
            {
                var model = Services.RotationTaskService.PopulateCompleteTaskViewModel(TaskId);
                if (model != null)
                {
                    return PartialView("~/Views/Rotation/Tasks/CompleteTaskDialog/_CompleteTaskFormDialog.cshtml", model);
                }
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CompleteTaskDialog(RotationTaskCompleteViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.OutcomesTab.IsFeedbackRequired && model.OutcomesTab.Feedback.IsNullOrWhiteSpace())
                {
                    ModelState.AddModelError("Feedback", "Handback Notes is required");

                    model.SelectedTabNumber = 2;
                }
                else
                {
                    Services.RotationTaskService.SaveCompleteTask(model);

                    return Content("ok");
                }
            }

            return PartialView("~/Views/Rotation/Tasks/CompleteTaskDialog/_CompleteTaskFormDialog.cshtml", model);
        }

        #endregion

        #region Task details

        [HttpGet]
        public ActionResult TaskDetailsDialog(Guid TaskId)
        {
            if (TaskId != Guid.Empty)
            {
                RotationTaskDetailsViewModel model = Services.RotationTaskService.GetTaskDetails(TaskId);

                if (model.IsFeedbackRequired)
                {
                    model.SelectedTabNumber = 2;
                }


                return PartialView("~/Views/Rotation/Tasks/_TaskDetailsDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        #endregion

        #region Manager comments

        [HttpGet]
        public ActionResult ManagerCommentButton(ManagerCommentButtonViewModel model)
        {
            var currentUserId = Guid.Parse(User.Identity.GetUserId());

            if (model.ItemId.HasValue)
            {
                var isMyTopic = Services.RotationTaskService.IsMyTask(currentUserId, model.ItemId.Value);

                if (User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) && isMyTopic == false ||
                    User.IsInRole(iHandoverRoles.Relay.ExecutiveUser) && isMyTopic == false ||
                    User.IsInRole(iHandoverRoles.Relay.Administrator) && isMyTopic == false ||
                    User.IsInRole(iHandoverRoles.Relay.HeadLineManager) && isMyTopic == false ||
                    User.IsInRole(iHandoverRoles.Relay.LineManager) && isMyTopic == false)
                {
                    model.IsActive = true;
                }

            }

            return PartialView("~/Views/Rotation/Shared/ManagerComments/_ManagerCommentButton.cshtml", model);
        }


        [HttpGet]
        public ActionResult ManagerCommentsDialog(Guid? Id)
        {
            if (Id != null)
            {

                var isMyTask = Services.RotationTaskService.IsMyTask(Guid.Parse(User.Identity.GetUserId()), (Guid)Id);

                var editEnable = User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) && !isMyTask ||
                                 User.IsInRole(iHandoverRoles.Relay.ExecutiveUser) && !isMyTask ||
                                 User.IsInRole(iHandoverRoles.Relay.Administrator) && !isMyTask ||
                                 User.IsInRole(iHandoverRoles.Relay.HeadLineManager) && !isMyTask ||
                                 User.IsInRole(iHandoverRoles.Relay.LineManager) && !isMyTask;

                var model = Services.RotationTaskService.PopulateManagerCommentViewModel(Id.Value);

                if (editEnable)
                {
                    return PartialView("~/Views/Rotation/Shared/ManagerComments/_ManagerCommentsFormDialog.cshtml", model);
                }

                return PartialView("~/Views/Rotation/Shared/ManagerComments/_ManagerCommentsDetailsDialog.cshtml", model);

            }
            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ManagerCommentsDialog(ManagerCommentsViewModel model)
        {
            if (ModelState.IsValid)
            {
                Services.RotationTaskService.UpdateTaskManagerComment(model);
                return Content("ok");
            }

            return PartialView("~/Views/Rotation/Shared/ManagerComments/_ManagerCommentsFormDialog.cshtml", model);
        }

        #endregion

        [HttpPost]
        public ActionResult GetTaskRecipientId(Guid taskId)
        {
            if (taskId != Guid.Empty)
            {
                Guid RecipientId = Services.RotationTaskService.GetTaskRecipientId(taskId);

                return Json(new { recipientId = RecipientId });
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        public ActionResult CarryforwardTask(Guid taskId)
        {
            if (taskId != Guid.Empty)
            {
                Services.RotationTaskService.CarryforwardTask(taskId);

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }


        [HttpPost]
        public ActionResult RemoveTask(Guid taskId)
        {
            if (taskId != Guid.Empty && Services.RotationTaskService.RemoveTask(taskId))
            {
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        //[HttpGet]
        public ActionResult GetTaskLogs(Guid taskId)
        {
            IEnumerable<string> model = Services.TaskLogService.GetTaskLogs(taskId);

            return PartialView("~/Views/Rotation/Tasks/_TaskLogTab.cshtml", model);
        }
    }
}