﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.Controllers.Rotation
{
    public class DailyNotesPanelController : BaseController
    {
        // IOC
        public DailyNotesPanelController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult DailyNotesForRotation(Guid rotationId)
        {
            if (rotationId != Guid.Empty)
            {
                string safetyVersion = ConfigurationManager.AppSettings["safetySectionVersion"];

                DailyNotesPanelViewModel model = Services.DailyNoteService
                                                        .DailyNotesForPanel(rotationId, Guid.Parse(User.Identity.GetUserId()), safetyVersion);

                if (model.DailyNotes.Any())
                    return PartialView("~/Views/Rotation/DailyNotesPanel/_DailyNotesPanel.cshtml", model);
                else
                    return new EmptyResult();
            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult DailyNoteSlide(Guid dailyNoteId)
        {
            if (dailyNoteId != Guid.Empty)
            {
                string safetyVersion = ConfigurationManager.AppSettings["safetySectionVersion"];

                DailyNoteViewModel model = Services.DailyNoteService
                    .DailyNoteSlideForPanel(dailyNoteId, Guid.Parse(User.Identity.GetUserId()), safetyVersion);

                return PartialView("~/Views/Rotation/DailyNotesPanel/_DailyNoteSlide.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult DetailsDailyNoteDialog(Guid? Id)
        {
            if (Id.HasValue)
            {
                DailyNoteViewModel model = Services.DailyNoteService.GetDailyNoteById(Id.Value);
                if (model != null)
                {
                    return PartialView("~/Views/Rotation/DailyNotesPanel/_DailyNoteDetailsDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult EditDailyNoteDialog(Guid? Id)
        {
            if (Id.HasValue)
            {
                DailyNoteViewModel model = Services.DailyNoteService.GetDailyNoteById(Id.Value);
                if (model != null)
                {
                    return PartialView("~/Views/Rotation/DailyNotesPanel/_DailyNoteFormDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDailyNoteDialog(DailyNoteViewModel model)
        {
            if (ModelState.IsValid)
            {
                Services.DailyNoteService.UpdateDailyNote(model);
                return Content("ok");
            }

            return PartialView("~/Views/Rotation/DailyNotesPanel/_DailyNoteFormDialog.cshtml", model);
        }

    }
}