﻿using System;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class SummaryController : BaseController
    {
        // IOC
        public SummaryController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        public ActionResult Index(Guid? userId)
        {
            var currentUserId = userId ?? Guid.Parse(User.Identity.GetUserId());

            Guid? viewedRotationId = Services.RotationService.GetCurrentRotationIdForUser(currentUserId);

            if (viewedRotationId == null || !Services.RotationService.RotationIsConfirmed(viewedRotationId.Value) && !Services.RotationService.IsRotationHavePrevRotation(viewedRotationId.Value))
            {
                ViewModeViewModel viewModeModel = new ViewModeViewModel
                {
                    IsViewMode = IsViewMode,
                    RotationOwnerFullName = !IsViewMode ? string.Empty : Services.UserService.GetEmployeeById(userId.Value).Name
                };
                return View("~/Views/Rotation/Shared/NoRotation.cshtml", viewModeModel);
            }

            SummaryViewModel model = Services.RotationService.GetSummaryForRotation(viewedRotationId.Value, currentUserId, IsViewMode);

            if(Services.RotationService.IsShiftRotation(viewedRotationId.Value))
            {
                return View("~/Views/Rotation/Summary/ShiftIndex.cshtml", model);
            }
            else
            {
                return View("~/Views/Rotation/Summary/SwingIndex.cshtml", model);
            }
        }
    }
}