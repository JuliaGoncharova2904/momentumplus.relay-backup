﻿using System;
using MomentumPlus.Relay.Interfaces;
using System.Web.Mvc;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ShareItemController : BaseController
    {
        public ShareItemController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork) {}

        #region Share topic

        [HttpGet]
        public ActionResult ShareTopicFormDialog(Guid? topicId)
        {
            if (topicId.HasValue)
            {
                ShareTopicViewModel model = Services.SharingTopicService.PopulateShareTopicViewModel((Guid)topicId);

                return PartialView("~/Views/Rotation/ShareItem/ShareTopicDialog.cshtml", model);
            }

            return new HttpNotFoundResult();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ShareTopicFormDialog(ShareTopicViewModel model)
        {
            if (ModelState.IsValid)
            {
                Services.SharingTopicService.ChangeShareTopicRelations(model);
                return Content("ok");
            }

            model = Services.SharingTopicService.PopulateShareTopicViewModel(model.TopicId);

            return PartialView("~/Views/Rotation/ShareItem/ShareTopicDialog.cshtml", model);
        }

        #endregion

        #region Share Report

        [HttpPost]
        public ActionResult ShareReportIcon(Guid sourceId)
        {
            int model = Services.SharingReportService.CountRecipientsBySourceId(sourceId);
            return PartialView("~/Views/Rotation/ShareItem/_ShareReportItemIcon.cshtml", model);
        }

        [HttpGet]
        public ActionResult ShareReportFormDialog(Guid sourceId, RotationType sourceType)
        {
            var model = Services.SharingReportService.PopulateShareReportViewModel(sourceId, sourceType);   
            return PartialView("~/Views/Rotation/ShareItem/ShareReportFormDialog.cshtml", model);
        }

        [HttpPost]
        public ActionResult ShareReportFormDialog(ShareReportViewModel model)
        {
            if (ModelState.IsValid)
            {
                Services.SharingReportService.ChangeShareReportRelations(model);
                return Content("ok");
            }

            model = Services.SharingReportService.PopulateShareReportViewModel(model.ReportId, model.ReportType);
            return PartialView("~/Views/Rotation/ShareItem/ShareReportFormDialog.cshtml", model);
        }

        #endregion
    }
}