﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Core.Authorization;
using System.Net;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Web.Extensions;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ShiftHSESectionController : BaseController
    {
        // IOC
        public ShiftHSESectionController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Draft(Guid shiftId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            IEnumerable<HSETopicViewModel> model = Services.RotationModuleService
                                                           .GetHseModuleTopics(shiftId, ModuleSourceType.Draft, RotationType.Shift, filter)
                                                           .OrderBy(h => h.Type)
                                                           .ThenBy(h => h.Reference);

            if (filter == HandoverReportFilterType.NrItems)
            {
                return PartialView("~/Views/Rotation/ShiftReportBuilder/HSESection/_HSE_NR_DraftSection.cshtml", model);
            }


            return PartialView("~/Views/Rotation/ShiftReportBuilder/HSESection/_HSEDraftSection.cshtml", model);
        }

        [HttpGet]
        public ActionResult Received(Guid shiftId, Guid? receivedShiftId)
        {
            IEnumerable<HSETopicViewModel> model = Services.RotationModuleService
                                                           .GetHseModuleTopics(shiftId, ModuleSourceType.Received, RotationType.Shift)
                                                           .OrderBy(h => h.Type)
                                                           .ThenBy(h => h.Reference);
            if (receivedShiftId.HasValue)
            {
                model = model.Where(m => m.FromShiftId == receivedShiftId.Value);
            }

            return PartialView("~/Views/Rotation/ShiftReportBuilder/HSESection/_HSEReceivedSection.cshtml", model);
        }

        [HttpPost]
        public ActionResult UpdateTopic(HSETopicViewModel model)
        {
            model = Services.RotationTopicService.UpdateHSETopic(model);

            return Json(model);
        }

        [HttpPost]
        public ActionResult ReloadTopic(Guid topicId)
        {
            if (topicId != Guid.Empty)
            {
                HSETopicViewModel model = Services.RotationTopicService.GetHSETopic(topicId);
                if (model != null)
                {
                    return Json(model);
                }
            }

            return HttpNotFound();
        }

        #region Edit Topic Dialog

        [HttpGet]
        public ActionResult EditTopicDialog(Guid? topic)
        {
            if (topic != null)
            {
                var model = Services.RotationTopicService.PopulateHSETopicDialogModel((Guid)topic);

                var isMyTopic = Services.RotationTopicService.IsMyShiftTopic(Guid.Parse(User.Identity.GetUserId()), (Guid)topic);

                model.IsManagerCommentsEnabled = User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.ExecutiveUser) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.Administrator) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.HeadLineManager) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.LineManager) && !isMyTopic;

                return PartialView("~/Views/Rotation/ShiftReportBuilder/HSESection/_HSETopicFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicDialog(HSETopicDialogViewModel model)
        {
            if (model.Id.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicExist(Guid.Parse(model.RotationTopicGroupId), (Guid)model.Id, model.Name))
                    {
                        ModelState.AddModelError("Name", "Safety Item with this name already exist.");
                    }

                    else
                    {
                        Services.RotationTopicService.UpdateHSETopic(model);
                        return Content("ok");
                    }
                }

                model = Services.RotationTopicService.PopulateHSETopicDialogModel((Guid)model.Id);

                return PartialView("~/Views/Rotation/ShiftReportBuilder/HSESection/_HSETopicFormDialog.cshtml", model);

            }


            return HttpNotFound();
        }

        #endregion

        #region Add Topic

        [HttpGet]
        public ActionResult AddInlineTopic(Guid destId)
        {

            ShiftHSEAddInlineTopicViewModel model = Services.RotationTopicService.PopulateShiftHSETopicInlineModel(destId);

            return PartialView("~/Views/Rotation/ShiftReportBuilder/HSESection/_HSEAddInlineTopic.cshtml", model);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddInlineTopic(ShiftHSEAddInlineTopicViewModel model)
        {
            if (model.ShiftId.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicExist((Guid)model.Type, Guid.NewGuid(), model.Reference))
                    {
                        ModelState.AddModelError("Reference", "An item with this category and reference already exists");
                    }

                    else
                    {
                        Services.RotationTopicService.AddShiftHSETopic(model);

                        return new HttpStatusCodeResult(HttpStatusCode.OK);
                    }
                }

                var errorMessages = ModelState.GetValidErrorFromState();

                return Content(errorMessages);
            }

            return HttpNotFound();
        }

        #endregion
    }
}