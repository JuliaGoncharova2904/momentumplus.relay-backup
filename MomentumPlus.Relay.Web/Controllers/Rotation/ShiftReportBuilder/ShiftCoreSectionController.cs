﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Core.Authorization;
using System.Net;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Web.Extensions;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ShiftCoreSectionController : BaseController
    {
        // IOC
        public ShiftCoreSectionController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Draft(Guid shiftId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            IEnumerable<CoreTopicViewModel> model = Services.RotationModuleService
                                                            .GetCoreModuleTopics(shiftId, ModuleSourceType.Draft, RotationType.Shift, filter)
                                                            .OrderBy(c => c.Category)
                                                            .ThenBy(c => c.Reference);

            if (filter == HandoverReportFilterType.NrItems)
            {
                return PartialView("~/Views/Rotation/ShiftReportBuilder/CoreSection/_Core_NR_DraftSection.cshtml", model);
            }

            return PartialView("~/Views/Rotation/ShiftReportBuilder/CoreSection/_CoreDraftSection.cshtml", model);
        }

        [HttpGet]
        public ActionResult Received(Guid shiftId, Guid? receivedShiftId)
        {
            IEnumerable<CoreTopicViewModel> model = Services.RotationModuleService
                                                            .GetCoreModuleTopics(shiftId, ModuleSourceType.Received, RotationType.Shift)
                                                            .OrderBy(c => c.Category)
                                                            .ThenBy(c => c.Reference);
            if (receivedShiftId.HasValue)
            {
                model = model.Where(m => m.FromShiftId == receivedShiftId.Value);
            }

            return PartialView("~/Views/Rotation/ShiftReportBuilder/CoreSection/_CoreReceivedSection.cshtml", model);
        }

        [HttpPost]
        public ActionResult UpdateTopic(CoreTopicViewModel model)
        {
            model = Services.RotationTopicService.UpdateCoreTopic(model);

            return Json(model);
        }

        [HttpPost]
        public ActionResult ReloadTopic(Guid topicId)
        {
            if (topicId != Guid.Empty)
            {
                CoreTopicViewModel model = Services.RotationTopicService.GetCoreTopic(topicId);
                if (model != null)
                {
                    return Json(model);
                }
            }

            return HttpNotFound();
        }

        #region Edit Topic

        [HttpGet]
        public ActionResult EditTopicDialog(Guid? topic)
        {
            if (topic != null)
            {
                var model = Services.RotationTopicService.PopulateCoreTopicDialogModel((Guid)topic);

                var isMyTopic = Services.RotationTopicService.IsMyShiftTopic(Guid.Parse(User.Identity.GetUserId()), (Guid)topic);

                model.IsManagerCommentsEnabled = User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.ExecutiveUser) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.Administrator) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.HeadLineManager) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.LineManager) && !isMyTopic;

                return PartialView("~/Views/Rotation/ShiftReportBuilder/CoreSection/_CoreTopicFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicDialog(CoreTopicDialogViewModel model)
        {
            if (model.Id.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicExist(Guid.Parse(model.RotationTopicGroupId), (Guid)model.Id, model.Name))
                    {
                        ModelState.AddModelError("Name", "Core Topic with this name already exist.");
                    }

                    else
                    {
                        Services.RotationTopicService.UpdateCoreTopic(model);
                        return Content("ok");
                    }
                }

                model = Services.RotationTopicService.PopulateCoreTopicDialogModel((Guid)model.Id);

                return PartialView("~/Views/Rotation/ShiftReportBuilder/CoreSection/_CoreTopicFormDialog.cshtml", model);

            }


            return HttpNotFound();
        }

        #endregion

        #region Add Topic

        [HttpGet]
        public ActionResult AddInlineTopic(Guid destId)
        {
            ShiftCoreAddInlineTopicViewModel model = Services.RotationTopicService.PopulateShiftCoreTopicInlineModel(destId);

            return PartialView("~/Views/Rotation/ShiftReportBuilder/CoreSection/_CoreAddInlineTopic.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddInlineTopic(ShiftCoreAddInlineTopicViewModel model)
        {
            if (model.ShiftId.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicExist((Guid)model.ProcessGroup, Guid.NewGuid(), model.ProcessLocation))
                    {
                        ModelState.AddModelError("ProcessLocation", "An item with this category and reference already exists");
                    }

                    else
                    {
                        Services.RotationTopicService.AddShiftCoreTopic(model);

                        return new HttpStatusCodeResult(HttpStatusCode.OK);
                    }
                }

                var errorMessages = ModelState.GetValidErrorFromState();

                return Content(errorMessages);
            }

            return HttpNotFound();
        }

        #endregion
    }
}