﻿using System;
using MomentumPlus.Relay.Interfaces;
using System.Web.Mvc;
using MomentumPlus.Relay.Models;
using Rotativa;
using System.Net;
using System.Web.Configuration;

namespace MomentumPlus.Relay.Web.Controllers.Rotation.ShiftReportBuilder
{
    public class ShiftReportBuilderController : BaseController
    {
        public ShiftReportBuilderController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork) { }


        public ActionResult Index(Guid? shiftId, HandoverReportFilterType? filter)
        {
            if (shiftId.HasValue)
            {
                var qmeReport = WebConfigurationManager.AppSettings["qmeReport"];

                ShiftReportPanelViewModel model = new ShiftReportPanelViewModel
                {
                    FilterType = filter ?? HandoverReportFilterType.All,
                    ShiftId = shiftId,
                    IsQME = Boolean.Parse(qmeReport)
                };

                if (!Services.ShiftService.ShiftHasRecipient(shiftId.Value))
                {
                    return PartialView("~/Views/Rotation/Shared/_EmptyReportBuilder.cshtml", "Handover recipient has not been selected.");
                }

                if (Services.ShiftService.IsShiftEnd(shiftId.Value) || Services.ShiftService.IsShiftWorkTimeEnd(shiftId.Value))
                {
                    return PartialView("~/Views/Rotation/ShiftReportBuilder/_ReadModeShiftReportBuilderPanel.cshtml", model);
                }

                return PartialView("~/Views/Rotation/ShiftReportBuilder/_ShiftReportBuilderPanel.cshtml", model);
            }

            return PartialView("~/Views/Rotation/Shared/_EmptyReportBuilder.cshtml", "Please choose a report from above to view the handover report.");
        }

        #region Draft Panel

        public ActionResult DraftPanel(Guid shiftId, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems)
        {
            ShiftDraftPanelViewModel model = new ShiftDraftPanelViewModel
            {
                RotationId = Services.ShiftService.ShiftRotationId(shiftId),
                Filter = filter,
                ShiftId = shiftId,
                ShiftPeriod = Services.ShiftService.GetShiftPeriod(shiftId),
                HandoverRecipientName = Services.ShiftService.GetShiftHandoverRecipientName(shiftId)
            };
            return PartialView("~/Views/Rotation/ShiftReportBuilder/_DraftPanel.cshtml", model);
        }

        public ActionResult ReportFilterPopover(Guid shiftId, HandoverReportFilterType filter)
        {
            ShiftReportBuilderFilterViewModel model = new ShiftReportBuilderFilterViewModel
            {
                Id = shiftId,
                Filter = filter
            };

            return PartialView("~/Views/Rotation/ShiftReportBuilder/_ShiftReportFilterPopover.cshtml", model);
        }


        #endregion

        #region Received Panel

        [HttpGet]
        public ActionResult ReceivedPanel(Guid shiftId, Guid? recivedShiftId)
        {
            ShiftReceivedPanelViewModel model = new ShiftReceivedPanelViewModel
            {
                ShiftId = shiftId,
                RecivedShiftId = recivedShiftId,
                RecivedShifts = Services.ShiftService.PopulateReceivedShiftsPanel(shiftId)
            };

            return PartialView("~/Views/Rotation/ShiftReportBuilder/_ReceivedPanel.cshtml", model);
        }

        [HttpGet]
        public ActionResult ReceivedPanelSections(Guid shiftId, Guid? recivedShiftId)
        {
            ShiftReceivedPanelViewModel model = new ShiftReceivedPanelViewModel
            {
                ShiftId = shiftId,
                RecivedShiftId = recivedShiftId,
                RecivedShifts = Services.ShiftService.PopulateReceivedShiftsPanel(shiftId)
            };

            return PartialView("~/Views/Rotation/ShiftReportBuilder/_ReceivedPanelSections.cshtml", model);
        }

        #endregion

        #region Report Preview

        public ActionResult ReportPreview(Guid shiftId, bool isReceived = false, Guid? selectedShiftId = null)
        {
            ShiftReportPreviewViewModel viewModel = Services.ReportService.PopulateShiftReportViewModel(shiftId,
                                                                                    isReceived ? ModuleSourceType.Received : ModuleSourceType.Draft,
                                                                                    selectedShiftId);

            return PartialView("~/Views/Rotation/ShiftReportBuilder/ReportPreview/_ReportPreview.cshtml", viewModel);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ReportPrint(Guid shiftId, bool isReceived = false, Guid? selectedShiftId = null)
        {
            string footerHtml = string.Format("--footer-html {0} --enable-local-file-access --enable-javascript --footer-spacing -10", Url.Action("PdfFooter", "ShiftReportBuilder", new { area = "" }, Request.Url.Scheme));

            var pdfResult = new ActionAsPdf("ShiftReport", new { shiftId, isReceived, selectedShiftId })
            {
                FileName = "Shift-Report.pdf",
                PageOrientation = Rotativa.Options.Orientation.Portrait,
                IsGrayScale = false,
                IsJavaScriptDisabled = false,
                CustomSwitches = footerHtml,
                PageMargins = new Rotativa.Options.Margins(0, 0, 0, 0)
            };

            var binary = pdfResult.BuildPdf(this.ControllerContext);

            return File(binary, "application/pdf");
        }

        [AllowAnonymous]
        public ActionResult ShiftReport(Guid shiftId, bool isReceived = false, Guid? selectedShiftId = null)
        {
            ShiftReportPreviewViewModel viewModel = Services.ReportService.PopulateShiftReportViewModel(shiftId, isReceived ? ModuleSourceType.Received : ModuleSourceType.Draft,
                                                                                                        selectedShiftId);

            return View("~/Views/Rotation/ShiftReportBuilder/ReportPreview/_ReportPrint.cshtml", viewModel);
        }


        [AllowAnonymous]
        public ActionResult PdfFooter()
        {
            return PartialView("~/Views/Shared/Layouts/Base/_PrintFooter.cshtml");
        }

        #endregion

        #region Finalize All

        [HttpPost]
        public ActionResult FinalizeAll(Guid shiftId)
        {
            Services.ShiftService.SetFinalizeStatusForDraftItems(shiftId, FinalizeStatus.Finalized);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        #endregion
    }
}