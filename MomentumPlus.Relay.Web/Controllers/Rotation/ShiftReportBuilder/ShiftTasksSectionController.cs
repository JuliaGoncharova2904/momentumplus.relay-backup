﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ShiftTasksSectionController : BaseController
    {
        // IOC
        public ShiftTasksSectionController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Draft(Guid shiftId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            IEnumerable<TasksTopicViewModel> model = Services.RotationTaskService
                                                                .GetAllShiftTasks(shiftId, ModuleSourceType.Draft)
                                                                .OrderBy(t => t.Name);

            return PartialView("~/Views/Rotation/ShiftReportBuilder/TasksSection/_TasksDraftSection.cshtml", model);
        }

        [HttpGet]
        public ActionResult Received(Guid shiftId, Guid? receivedShiftId)
        {
            IEnumerable<TasksTopicViewModel> model = Services.RotationTaskService
                                                                .GetAllShiftTasks(shiftId, ModuleSourceType.Received)
                                                                .OrderBy(t => t.Name);
            if (receivedShiftId.HasValue)
            {
                model = model.Where(m => m.FromShiftId == receivedShiftId.Value);
            }

            return PartialView("~/Views/Rotation/ShiftReportBuilder/TasksSection/_TasksReceivedSection.cshtml", model);
        }

        [HttpPost]
        public ActionResult UpdateTopic(TasksTopicViewModel model)
        {
            Guid currentUserId = Guid.Parse(User.Identity.GetUserId());
            model = Services.RotationTaskService.UpdateTaskTopic(model, currentUserId);

            return Json(model);
        }

        [HttpPost]
        public ActionResult ReloadTopic(Guid topicid)
        {
            if (topicid != Guid.Empty)
            {
                TasksTopicViewModel model = Services.RotationTaskService.GetTaskTopic(topicid);
                if (model != null)
                {
                    return Json(model);
                }
            }

            return HttpNotFound();
        }

        public ActionResult GetEditCompleteMenuPopover(Guid taskId)
        {
            ReceivedEditCompleteViewModel model = new ReceivedEditCompleteViewModel
            {
                TaskId = taskId
            };
            return PartialView("~/Views/Rotation/ShiftReportBuilder/_ShiftEditCompleteForReceivedMenuPopover.cshtml", model);
        }
    }
}