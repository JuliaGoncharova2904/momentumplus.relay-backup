﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Core.Authorization;
using System.Net;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Web.Extensions;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ShiftProjectsSectionController : BaseController
    {
        // IOC
        public ShiftProjectsSectionController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Draft(Guid shiftId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            if (Services.RotationModuleService.IsModuleEnabled(ModuleType.Project, shiftId, ModuleSourceType.Draft, RotationType.Shift))
            {
                IEnumerable<ProjectsTopicViewModel> model = Services.RotationModuleService
                                                                    .GetProjectModuleTopics(shiftId, ModuleSourceType.Draft, RotationType.Shift, filter)
                                                                    .OrderBy(p => p.Project)
                                                                    .ThenBy(p => p.Reference);

                return PartialView("~/Views/Rotation/ShiftReportBuilder/ProjectsSection/_ProjectsDraftSection.cshtml", model);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult Received(Guid shiftId, Guid? receivedShiftId)
        {
            IEnumerable<ProjectsTopicViewModel> model = Services.RotationModuleService
                                                                .GetProjectModuleTopics(shiftId, ModuleSourceType.Received, RotationType.Shift)
                                                                .OrderBy(p => p.Project)
                                                                .ThenBy(p => p.Reference);

            if (receivedShiftId.HasValue)
            {
                model = model.Where(m => m.FromShiftId == receivedShiftId.Value);
            }

            return PartialView("~/Views/Rotation/ShiftReportBuilder/ProjectsSection/_ProjectsReceivedSection.cshtml", model);
        }

        [HttpPost]
        public ActionResult UpdateTopic(ProjectsTopicViewModel model)
        {
            model = Services.RotationTopicService.UpdateProjectTopic(model);

            return Json(model);
        }

        [HttpPost]
        public ActionResult ReloadTopic(Guid topicId)
        {
            if (topicId != Guid.Empty)
            {
                ProjectsTopicViewModel model = Services.RotationTopicService.GetProjectTopic(topicId);
                if (model != null)
                {
                    return Json(model);
                }
            }

            return HttpNotFound();
        }

        #region Edit Topic

        [HttpGet]
        public ActionResult EditTopicDialog(Guid? topic)
        {
            if (topic != null)
            {
                ProjectTopicDialogViewModel model = Services.RotationTopicService.PopulateProjectTopicDialogModel((Guid)topic);

                bool isMyTopic = Services.RotationTopicService.IsMyShiftTopic(Guid.Parse(User.Identity.GetUserId()), (Guid)topic);

                model.IsManagerCommentsEnabled = User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.ExecutiveUser) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.Administrator) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.HeadLineManager) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.LineManager) && !isMyTopic;

                return PartialView("~/Views/Rotation/ShiftReportBuilder/ProjectsSection/_ProjectTopicFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicDialog(ProjectTopicDialogViewModel model)
        {
            if (model.Id.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicExist(Guid.Parse(model.RotationTopicGroupId), (Guid)model.Id, model.Name))
                    {
                        ModelState.AddModelError("Name", "Project Item with this name already exist.");
                    }

                    else
                    {
                        Services.RotationTopicService.UpdateProjectTopic(model);
                        return Content("ok");
                    }
                }

                model = Services.RotationTopicService.PopulateProjectTopicDialogModel((Guid)model.Id);

                return PartialView("~/Views/Rotation/ShiftReportBuilder/ProjectsSection/_ProjectTopicFormDialog.cshtml", model);

            }


            return HttpNotFound();
        }

        #endregion

        #region Add Topic

        [HttpGet]
        public ActionResult AddInlineTopic(Guid destId)
        {
            ShiftProjectAddInlineTopicViewModel model = Services.RotationTopicService.PopulateShiftProjectTopicInlineModel(destId);

            return PartialView("~/Views/Rotation/ShiftReportBuilder/ProjectsSection/_ProjectAddInlineTopic.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddInlineTopic(ShiftProjectAddInlineTopicViewModel model)
        {
            if (model.ShiftId.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicExist((Guid)model.Project, Guid.NewGuid(), model.Reference))
                    {
                        ModelState.AddModelError("Reference", "An item with this category and reference already exists");
                    }

                    else
                    {
                        Services.RotationTopicService.AddShiftProjectTopic(model);

                        return new HttpStatusCodeResult(HttpStatusCode.OK);
                    }
                }

                var errorMessages = ModelState.GetValidErrorFromState();

                return Content(errorMessages);
            }

            return HttpNotFound();
        }

        #endregion
    }
}