﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Core.Authorization;
using System.Net;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Web.Extensions;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ShiftTeamSectionController : BaseController
    {
        // IOC
        public ShiftTeamSectionController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Draft(Guid shiftId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            if (Services.RotationModuleService.IsModuleEnabled(ModuleType.Team, shiftId, ModuleSourceType.Draft, RotationType.Shift))
            {

                IEnumerable<TeamTopicViewModel> model = Services.RotationModuleService
                                                                .GetTeamModuleTopics(shiftId, ModuleSourceType.Draft, RotationType.Shift, filter)
                                                                .OrderBy(t => t.TeamMember)
                                                                .ThenBy(t => t.Reference);

                return PartialView("~/Views/Rotation/ShiftReportBuilder/TeamSection/_TeamDraftSection.cshtml", model);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult Received(Guid shiftId, Guid? receivedShiftId)
        {
            IEnumerable<TeamTopicViewModel> model = Services.RotationModuleService
                                                            .GetTeamModuleTopics(shiftId, ModuleSourceType.Received, RotationType.Shift)
                                                            .OrderBy(t => t.TeamMember)
                                                            .ThenBy(t => t.Reference);
            if (receivedShiftId.HasValue)
            {
                model = model.Where(m => m.FromShiftId == receivedShiftId.Value);
            }

            return PartialView("~/Views/Rotation/ShiftReportBuilder/TeamSection/_TeamReceivedSection.cshtml", model);
        }

        [HttpPost]
        public ActionResult UpdateTopic(TeamTopicViewModel model)
        {
            model = Services.RotationTopicService.UpdateTeamTopic(model);

            return Json(model);
        }

        [HttpPost]
        public ActionResult ReloadTopic(Guid topicId)
        {
            if (topicId != Guid.Empty)
            {
                TeamTopicViewModel model = Services.RotationTopicService.GetTeamTopic(topicId);
                if (model != null)
                {
                    return Json(model);
                }
            }

            return HttpNotFound();
        }

        #region Edit Topic

        [HttpGet]
        public ActionResult EditTopicDialog(Guid? topic)
        {
            if (topic != null)
            {
                bool isMyTopic = Services.RotationTopicService.IsMyShiftTopic(Guid.Parse(User.Identity.GetUserId()), (Guid)topic);

                TeamTopicDialogViewModel model = Services.RotationTopicService.PopulateShiftTeamTopicDialogModel((Guid)topic);

                model.IsManagerCommentsEnabled = User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.ExecutiveUser) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.Administrator) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.HeadLineManager) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.LineManager) && !isMyTopic;


                return PartialView("~/Views/Rotation/ShiftReportBuilder/TeamSection/_TeamTopicFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicDialog(TeamTopicDialogViewModel model)
        {
            if (model.Id.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicAssignedWithSameNameExistByTopic((Guid)model.Id, (Guid)model.RelationId, model.Name))
                    {
                        ModelState.AddModelError("Name", "Topic Item with this name already exist.");
                    }

                    else
                    {
                        Services.RotationTopicService.UpdateTeamTopic(model);
                        return Content("ok");
                    }
                }

                model = Services.RotationTopicService.PopulateShiftTeamTopicDialogModel((Guid)model.Id);

                return PartialView("~/Views/Rotation/ShiftReportBuilder/TeamSection/_TeamTopicFormDialog.cshtml", model);

            }


            return HttpNotFound();
        }

        #endregion

        #region Add Topic

        [HttpGet]
        public ActionResult AddInlineTopic(Guid destId)
        {
            var model = Services.RotationTopicService.PopulateShiftTeamTopicInlineModel(destId);

            return PartialView("~/Views/Rotation/ShiftReportBuilder/TeamSection/_TeamAddInlineTopic.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddInlineTopic(ShiftTeamAddInlineTopicViewModel model)
        {
            if (model.ShiftId.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicAssignedWithSameNameExist(model.ShiftId.Value, (Guid)model.TeamMember, model.Reference, RotationType.Shift))
                    {
                        ModelState.AddModelError("Reference", "An item with this category and reference already exists");
                    }
                    else
                    {
                        Services.RotationTopicService.AddShiftTeamTopic(model);

                        return new HttpStatusCodeResult(HttpStatusCode.OK);
                    }
                }

                var errorMessages = ModelState.GetValidErrorFromState();

                return Content(errorMessages);
            }

            return HttpNotFound();
        }

        #endregion
    }
}