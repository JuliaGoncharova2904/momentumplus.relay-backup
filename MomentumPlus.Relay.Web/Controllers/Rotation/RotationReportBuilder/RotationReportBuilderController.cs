﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Net;
using System.Web.Mvc;
using Rotativa;
using System.Web.Configuration;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class RotationReportBuilderController : BaseController
    {
        // IOC
        public RotationReportBuilderController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }


        public ActionResult Index(Guid userId, HandoverReportFilterType? filter)
        {
            var qmeReport = WebConfigurationManager.AppSettings["qmeReport"];

            RotationReportPanelViewModel model = new RotationReportPanelViewModel
            {
                UserId = userId,
                FilterType = filter ?? HandoverReportFilterType.All,
                IsQME = Boolean.Parse(qmeReport)
            };
            return PartialView("~/Views/Rotation/RotationReportBuilder/_RotationReportBuilderPanel.cshtml", model);
        }

        #region Draft panel

        /// <summary>
        /// Return draft panel for rotation.
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="filter">Fiter Type</param>
        /// <returns></returns>
        public ActionResult DraftPanel(Guid userId, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems)
        {
            if (userId != Guid.Empty)
            {
                var curentRotationId = Services.RotationService.GetCurrentRotationIdForUser(userId).Value;

                DraftPanelViewModel model = new DraftPanelViewModel
                {
                    RotationId = curentRotationId,
                    Filter = filter,
                    RotationPeriod = Services.RotationService.GetRotationPeriod(curentRotationId)
                };

                if (Services.RotationService.IsRotationExpiredOrNotStarted(curentRotationId))
                {
                    model.EmptyPanelMessage = "Your rotation has ended, please confirm your next rotation to begin drafting your report.";
                    return PartialView("~/Views/Rotation/RotationReportBuilder/_EmptyDraftPanel.cshtml", model);
                }
                else if (Services.RotationService.IsRotationSwingEnd(curentRotationId))
                {
                    model.EmptyPanelMessage = "Your report has been sent to your back-to-back. You can now view it in the History section.";
                    return PartialView("~/Views/Rotation/RotationReportBuilder/_EmptyDraftPanel.cshtml", model);
                }
                else
                {
                    return PartialView("~/Views/Rotation/RotationReportBuilder/_DraftPanel.cshtml", model);
                }
            }

            return HttpNotFound();
        }


        public ActionResult ReportFilterPopover(Guid userId, HandoverReportFilterType filter)
        {
            SwingReportBuilderFilterViewModel model = new SwingReportBuilderFilterViewModel
            {
                Id = userId,
                Filter = filter
            };

            return PartialView("~/Views/Rotation/RotationReportBuilder/_RotationReportFilterPopover.cshtml", model);
        }


        /// <summary>
        /// Return From -> To block for darft section.
        /// </summary>
        /// <param name="rotationId">Rotation Id</param>
        /// <returns></returns>
        public ActionResult FromToBlock(Guid rotationId)
        {
            if (rotationId != Guid.Empty)
            {
                FromToBlockViewModel model = Services.RotationService.GetFromToBlock(rotationId);

                return PartialView("~/Views/Rotation/RotationReportBuilder/Shared/_DraftFromToBlock.cshtml", model);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        #endregion

        #region Received panel

        /// <summary>
        /// Return received panel for user.
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="selectedRotationId">Selected Rotation Id</param>
        /// <param name="receivedRotationId">Selected received Rotation Id</param>
        /// <returns></returns>
        public ActionResult ReceivedPanel(Guid userId, Guid? selectedRotationId, Guid? receivedRotationId)
        {
            if (userId != Guid.Empty)
            {
                ReceivedPanelViewModel model = new ReceivedPanelViewModel
                {
                    SelectedRotationId = selectedRotationId,
                    ReceivedRotationId = receivedRotationId,
                    RotationPeriod = Services.RotationService.GetReceivedRotationPeriod(selectedRotationId, null),
                    Rotations = Services.RotationService.GetRotationsReceivedHistorySlidesByUser(userId),
                    ReceivedRotations = selectedRotationId.HasValue ? Services.RotationService.GetRotationsWhoPopulateMyReceivedSection(selectedRotationId.Value) : null
                };

                return PartialView("~/Views/Rotation/RotationReportBuilder/_ReceivedPanel.cshtml", model);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectedRotationId"></param>
        /// <returns></returns>
        public ActionResult ReceivedPanelForSelectedRotation(Guid? selectedRotationId)
        {
            if (selectedRotationId.HasValue)
            {
                ReceivedPanelViewModel model = new ReceivedPanelViewModel
                {
                    SelectedRotationId = selectedRotationId,
                    RotationPeriod = Services.RotationService.GetReceivedRotationPeriod(selectedRotationId, null),
                    ReceivedRotations = Services.RotationService.GetRotationsWhoPopulateMyReceivedSection(selectedRotationId.Value)
                };

                return PartialView("~/Views/Rotation/RotationReportBuilder/_ReceivedPanelForSelectedRotation.cshtml", model);
            }

            return new EmptyResult();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectedRotationId"></param>
        /// <param name="receivedRotationId"></param>
        /// <returns></returns>
        public ActionResult ReceivedPanelForReceivedRotation(Guid selectedRotationId, Guid? receivedRotationId)
        {
            if (selectedRotationId != Guid.Empty)
            {
                ReceivedPanelViewModel model = new ReceivedPanelViewModel
                {
                    SelectedRotationId = selectedRotationId,
                    ReceivedRotationId = receivedRotationId,
                    RotationPeriod = Services.RotationService.GetReceivedRotationPeriod(selectedRotationId, receivedRotationId)
                };

                return PartialView("~/Views/Rotation/RotationReportBuilder/_ReceivedPanelForReceivedRotation.cshtml", model);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        #endregion

        #region History panel

        public ActionResult HistoryPanel(Guid userId, Guid? selectedRotationId)
        {
            if (userId != Guid.Empty)
            {
                HistoryPanelViewModel model = new HistoryPanelViewModel
                {
                    SelectedRotationId = selectedRotationId,
                    RotationPeriod = selectedRotationId.HasValue ? Services.RotationService.GetRotationPeriod(selectedRotationId.Value) : "...",
                    Rotations = Services.RotationService.GetRotationsHistorySlidesByUser(userId)
                };
                return PartialView("~/Views/Rotation/RotationReportBuilder/_HistoryPanel.cshtml", model);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        public ActionResult HistoryPanelSelectedRotation(Guid? selectedRotationId)
        {
            if (selectedRotationId.HasValue)
            {
                HistoryPanelViewModel model = new HistoryPanelViewModel
                {
                    SelectedRotationId = selectedRotationId,
                    RotationPeriod = Services.RotationService.GetRotationPeriod(selectedRotationId.Value),
                };

                return PartialView("~/Views/Rotation/RotationReportBuilder/_HistoryPanelSelectedRotation.cshtml", model);
            }

            return new EmptyResult();
        }

        #endregion

        public ActionResult ReportPreview(Guid rotationId, bool isReceived = false, Guid? selectedRotationId = null)
        {
            var viewModel = Services.ReportService.PopulateRotationReportViewModel(rotationId,
                                                                                    isReceived ? ModuleSourceType.Received : ModuleSourceType.Draft,
                                                                                    selectedRotationId);

            return PartialView("~/Views/Rotation/RotationReportBuilder/ReportPreview/_ReportPreview.cshtml", viewModel);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ReportPrint(Guid rotationId, bool isReceived = false, Guid? selectedRotationId = null)
        {
            string footerHtml = string.Format("--footer-html {0} --enable-local-file-access --enable-javascript --footer-spacing -5", Url.Action("PdfFooter", "RotationReportBuilder", new { area = "" }, Request.Url.Scheme));

            var pdfResult = new ActionAsPdf("RotationReport", new { rotationId, isReceived, selectedRotationId })
            {
                FileName = "Rotation-Report.pdf",
                PageOrientation = Rotativa.Options.Orientation.Portrait,
                IsGrayScale = false,
                IsJavaScriptDisabled = false,
                CustomSwitches = footerHtml,
                PageMargins = new Rotativa.Options.Margins(0, 0, 5, 0)
            };

            var binary = pdfResult.BuildPdf(this.ControllerContext);

            return File(binary, "application/pdf");
        }

        [AllowAnonymous]
        public ActionResult RotationReport(Guid rotationId, bool isReceived = false, Guid? selectedRotationId = null)
        {
            ReportPreviewViewModel viewModel = Services.ReportService.PopulateRotationReportViewModel(rotationId,
                                                                                                        isReceived ? ModuleSourceType.Received :
                                                                                                                        ModuleSourceType.Draft,
                                                                                                        selectedRotationId);

            return View("~/Views/Rotation/RotationReportBuilder/ReportPreview/_ReportPrint.cshtml", viewModel);
        }


        [AllowAnonymous]
        public ActionResult PdfFooter()
        {
            return PartialView("~/Views/Shared/Layouts/Base/_PrintFooter.cshtml");
        }

        [HttpPost]
        public ActionResult FinalizeAll(Guid? rotationId)
        {
            if (rotationId.HasValue)
            {
                try
                {
                    Services.RotationService.ChangeFinalizeStatusForDraftTopicsAndTasks((Guid) rotationId, FinalizeStatus.Finalized);

                    return new HttpStatusCodeResult(HttpStatusCode.OK);
                }
                catch (Exception)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }

            return  new EmptyResult();

        }
    }
}