﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class DailyNotesSectionController : BaseController
    {
        // IOC
        public DailyNotesSectionController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Draft(Guid rotationId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            if (rotationId != Guid.Empty)
            {
                IEnumerable<DailyNotesTopicViewModel> model = Services.DailyNoteService
                                                                        .DailyNoteTopicsForRotation(rotationId, ModuleSourceType.Draft).Where(t => t.IsSharingTopic == false)
                                                                        .OrderBy(d => d.OriginalDate);

                return PartialView("~/Views/Rotation/RotationReportBuilder/DailyNotesSection/_DailyNotesDraftSection.cshtml", model);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult Received(Guid rotationId, Guid? receivedRotationId)
        {
            if (rotationId != Guid.Empty)
            {
                IEnumerable<DailyNotesTopicViewModel> model = Services.DailyNoteService
                                                                        .DailyNoteTopicsForRotation(rotationId, ModuleSourceType.Received)
                                                                        .OrderBy(d => d.OriginalDate);

                if(receivedRotationId.HasValue)
                    model = model.Where(m => m.FromRotationId == receivedRotationId.Value);

                return PartialView("~/Views/Rotation/RotationReportBuilder/DailyNotesSection/_DailyNotesReceivedSection.cshtml", model);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult History(Guid rotationId)
        {
            if (rotationId != Guid.Empty)
            {
                IEnumerable<DailyNotesTopicViewModel> model = Services.DailyNoteService
                                                                        .DailyNoteTopicsForRotation(rotationId, ModuleSourceType.Draft)
                                                                        .OrderBy(d => d.OriginalDate);

                return PartialView("~/Views/Rotation/RotationReportBuilder/DailyNotesSection/_DailyNotesHistorySection.cshtml", model);
            }

            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult UpdateTopic(DailyNotesTopicViewModel model)
        {
            DailyNotesTopicViewModel resultModel = Services.DailyNoteService.UpdateDailyNoteTopic(model);
            return Json(resultModel);
        }

        [HttpPost]
        public ActionResult ReloadTopic(Guid topicId)
        {
            if(topicId != Guid.Empty)
            {
                DailyNotesTopicViewModel resultModel = Services.DailyNoteService.GetDailyNoteTopic(topicId);
                if(resultModel != null)
                {
                    return Json(resultModel);
                }
            }

            return HttpNotFound();
        }

    }
}