﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Core.Authorization;
using System.Net;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Web.Extensions;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class TeamSectionController : BaseController
    {
        // IOC
        public TeamSectionController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Draft(Guid rotationId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            if (Services.RotationModuleService.IsModuleEnabled(ModuleType.Team, rotationId, ModuleSourceType.Draft))
            {

                IEnumerable<TeamTopicViewModel> model = Services.RotationModuleService
                                                                .GetTeamModuleTopics(rotationId, ModuleSourceType.Draft, RotationType.Swing, filter)
                                                                .OrderBy(t => t.TeamMember)
                                                                .ThenBy(t => t.Reference);

                return PartialView("~/Views/Rotation/RotationReportBuilder/TeamSection/_TeamDraftSection.cshtml", model);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult Received(Guid rotationId, Guid? receivedRotationId)
        {
            IEnumerable<TeamTopicViewModel> model = Services.RotationModuleService
                                                            .GetTeamModuleTopics(rotationId, ModuleSourceType.Received).Where(t => t.IsSharingTopic == false)
                                                            .OrderBy(t => t.TeamMember)
                                                            .ThenBy(t => t.Reference);

            if (receivedRotationId.HasValue)
                model = model.Where(m => m.FromRotationId == receivedRotationId.Value);

            return PartialView("~/Views/Rotation/RotationReportBuilder/TeamSection/_TeamReceivedSection.cshtml", model);
        }

        [HttpGet]
        public ActionResult History(Guid rotationId)
        {
            IEnumerable<TeamTopicViewModel> model = Services.RotationModuleService
                                                            .GetTeamModuleTopics(rotationId, ModuleSourceType.Draft)
                                                            .OrderBy(t => t.TeamMember)
                                                            .ThenBy(t => t.Reference);

            return PartialView("~/Views/Rotation/RotationReportBuilder/TeamSection/_TeamHistorySection.cshtml", model);
        }

        [HttpPost]
        public ActionResult UpdateTopic(TeamTopicViewModel model)
        {
            model = Services.RotationTopicService.UpdateTeamTopic(model);

            return Json(model);
        }

        [HttpPost]
        public ActionResult ReloadTopic(Guid topicId)
        {
            if(topicId != Guid.Empty)
            {
                TeamTopicViewModel model = Services.RotationTopicService.GetTeamTopic(topicId);
                if(model != null)
                {
                    return Json(model);
                }
            }

            return HttpNotFound();
        }

        #region Edit Topic

        [HttpGet]
        public ActionResult EditTopicDialog(Guid? topic)
        {
            if (topic != null)
            {
                var isMyTopic = Services.RotationTopicService.IsMyTopic(Guid.Parse(User.Identity.GetUserId()), (Guid)topic);

                var model = Services.RotationTopicService.PopulateTeamTopicDialogModel((Guid)topic);

                model.IsManagerCommentsEnabled = User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.ExecutiveUser) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.Administrator) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.HeadLineManager) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.LineManager) && !isMyTopic;


                return PartialView("~/Views/Rotation/RotationReportBuilder/TeamSection/_TeamTopicFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicDialog(TeamTopicDialogViewModel model)
        {
            if (model.Id.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicAssignedWithSameNameExistByTopic((Guid)model.Id, (Guid)model.RelationId, model.Name))
                    {
                        ModelState.AddModelError("Name", "Topic Item with this name already exist.");
                    }

                    else
                    {
                        Services.RotationTopicService.UpdateTeamTopic(model);
                        return Content("ok");
                    }
                }

                model = Services.RotationTopicService.PopulateTeamTopicDialogModel((Guid)model.Id);

                return PartialView("~/Views/Rotation/RotationReportBuilder/TeamSection/_TeamTopicFormDialog.cshtml", model);

            }


            return HttpNotFound();
        }

        #endregion

        #region Add Topic

        [HttpGet]
        public ActionResult AddInlineTopic(Guid destId)
        {
            var model = Services.RotationTopicService.PopulateTeamTopicInlineModel(destId);

            return PartialView("~/Views/Rotation/RotationReportBuilder/TeamSection/_TeamAddInlineTopic.cshtml", model);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddInlineTopic(TeamAddInlineTopicViewModel model)
        {
            if (model.RotationId.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicAssignedWithSameNameExist(model.RotationId.Value, (Guid)model.TeamMember, model.Reference))
                    {
                        ModelState.AddModelError("Reference", "An item with this category and reference already exists");
                    }
                    else
                    {
                        Services.RotationTopicService.AddTeamTopic(model);

                        return new HttpStatusCodeResult(HttpStatusCode.OK);
                    }
                }

                var errorMessages = ModelState.GetValidErrorFromState();

                return Content(errorMessages);
            }

            return HttpNotFound();
        }

        #endregion
    }
}