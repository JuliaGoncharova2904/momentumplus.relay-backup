﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Core.Authorization;
using System.Net;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Web.Extensions;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class CoreSectionController : BaseController
    {
        // IOC
        public CoreSectionController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Draft(Guid rotationId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            IEnumerable<CoreTopicViewModel> model = Services.RotationModuleService
                                                            .GetCoreModuleTopics(rotationId, ModuleSourceType.Draft, RotationType.Swing, filter)
                                                            .OrderBy(c => c.Category)
                                                            .ThenBy(c => c.Reference);
            if (filter == HandoverReportFilterType.NrItems)
            {
                return PartialView("~/Views/Rotation/RotationReportBuilder/CoreSection/_Core_NR_DraftSection.cshtml", model);
            }

            return PartialView("~/Views/Rotation/RotationReportBuilder/CoreSection/_CoreDraftSection.cshtml", model);
        }

        [HttpGet]
        public ActionResult Received(Guid rotationId, Guid? receivedRotationId)
        {
            IEnumerable<CoreTopicViewModel> model = Services.RotationModuleService
                                                            .GetCoreModuleTopics(rotationId, ModuleSourceType.Received).Where(t => t.IsSharingTopic == false)
                                                            .OrderBy(c => c.Category)
                                                            .ThenBy(c => c.Reference);

            if (receivedRotationId.HasValue)
                model = model.Where(m => m.FromRotationId == receivedRotationId.Value);

            return PartialView("~/Views/Rotation/RotationReportBuilder/CoreSection/_CoreReceivedSection.cshtml", model);
        }

        [HttpGet]
        public ActionResult History(Guid rotationId)
        {
            IEnumerable<CoreTopicViewModel> model = Services.RotationModuleService
                                                            .GetCoreModuleTopics(rotationId, ModuleSourceType.Draft)
                                                            .OrderBy(c => c.Category)
                                                            .ThenBy(c => c.Reference);

            return PartialView("~/Views/Rotation/RotationReportBuilder/CoreSection/_CoreHistorySection.cshtml", model);
        }

        [HttpPost]
        public ActionResult UpdateTopic(CoreTopicViewModel model)
        {
            model = Services.RotationTopicService.UpdateCoreTopic(model);

            return Json(model);
        }

        [HttpPost]
        public ActionResult ReloadTopic(Guid topicId)
        {
            if(topicId != Guid.Empty)
            {
                CoreTopicViewModel model = Services.RotationTopicService.GetCoreTopic(topicId);
                if(model != null)
                {
                    return Json(model);
                }
            }

            return HttpNotFound();
        }

        #region Edit Topic

        [HttpGet]
        public ActionResult EditTopicDialog(Guid? topic)
        {
            if (topic != null)
            {
                var model = Services.RotationTopicService.PopulateCoreTopicDialogModel((Guid)topic);

                var isMyTopic = Services.RotationTopicService.IsMyTopic(Guid.Parse(User.Identity.GetUserId()), (Guid)topic);
                
                model.IsManagerCommentsEnabled = User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.ExecutiveUser) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.Administrator) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.HeadLineManager) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.LineManager) && !isMyTopic;

                return PartialView("~/Views/Rotation/RotationReportBuilder/CoreSection/_CoreTopicFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicDialog(CoreTopicDialogViewModel model)
        {
            if (model.Id.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicExist(Guid.Parse(model.RotationTopicGroupId), (Guid)model.Id, model.Name))
                    {
                        ModelState.AddModelError("Name", "Core Topic with this name already exist.");
                    }

                    else
                    {
                        Services.RotationTopicService.UpdateCoreTopic(model);
                        return Content("ok");
                    }
                }

                model = Services.RotationTopicService.PopulateCoreTopicDialogModel((Guid)model.Id);

                return PartialView("~/Views/Rotation/RotationReportBuilder/CoreSection/_CoreTopicFormDialog.cshtml", model);

            }


            return HttpNotFound();
        }

        #endregion

        #region Add Topic


        [HttpGet]
        public ActionResult AddInlineTopic(Guid destId)
        {

            var model = Services.RotationTopicService.PopulateCoreTopicInlineModel(destId);

            return PartialView("~/Views/Rotation/RotationReportBuilder/CoreSection/_CoreAddInlineTopic.cshtml", model);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddInlineTopic(RotationCoreAddInlineTopicViewModel model)
        {
            if (model.RotationId.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicExist((Guid)model.ProcessGroup, Guid.NewGuid(), model.ProcessLocation))
                    {
                        ModelState.AddModelError("ProcessLocation", "An item with this category and reference already exists");
                    }

                    else
                    {
                        Services.RotationTopicService.AddRotationCoreTopic(model);

                        return new HttpStatusCodeResult(HttpStatusCode.OK);
                    }
                }

                var errorMessages = ModelState.GetValidErrorFromState();

                return Content(errorMessages);
            }

            return HttpNotFound();
        }






        #endregion
    }
}