﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Core.Authorization;
using System.Net;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Web.Extensions;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class HSESectionController : BaseController
    {
        // IOC
        public HSESectionController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Draft(Guid rotationId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            IEnumerable<HSETopicViewModel> model = Services.RotationModuleService
                                                            .GetHseModuleTopics(rotationId, ModuleSourceType.Draft, RotationType.Swing, filter)
                                                            .OrderBy(h => h.Type)
                                                            .ThenBy(h => h.Reference);

            if (filter == HandoverReportFilterType.NrItems)
            {
                return PartialView("~/Views/Rotation/RotationReportBuilder/HSESection/_HSE_NR_DraftSection.cshtml", model);
            }

            return PartialView("~/Views/Rotation/RotationReportBuilder/HSESection/_HSEDraftSection.cshtml", model);
        }

        [HttpGet]
        public ActionResult Received(Guid rotationId, Guid? receivedRotationId)
        {
            IEnumerable<HSETopicViewModel> model = Services.RotationModuleService
                                                            .GetHseModuleTopics(rotationId, ModuleSourceType.Received).Where(t => t.IsSharingTopic == false)
                                                            .OrderBy(h => h.Type)
                                                            .ThenBy(h => h.Reference);

            if (receivedRotationId.HasValue)
                model = model.Where(m => m.FromRotationId == receivedRotationId.Value);

            return PartialView("~/Views/Rotation/RotationReportBuilder/HSESection/_HSEReceivedSection.cshtml", model);
        }

        [HttpGet]
        public ActionResult History(Guid rotationId)
        {
            IEnumerable<HSETopicViewModel> model = Services.RotationModuleService
                                                            .GetHseModuleTopics(rotationId, ModuleSourceType.Draft)
                                                            .OrderBy(h => h.Type)
                                                            .ThenBy(h => h.Reference);

            return PartialView("~/Views/Rotation/RotationReportBuilder/HSESection/_HSEHistorySection.cshtml", model);
        }

        [HttpPost]
        public ActionResult UpdateTopic(HSETopicViewModel model)
        {
            model = Services.RotationTopicService.UpdateHSETopic(model);

            return Json(model);
        }

        [HttpPost]
        public ActionResult ReloadTopic(Guid topicId)
        {
            if(topicId != Guid.Empty)
            {
                HSETopicViewModel model = Services.RotationTopicService.GetHSETopic(topicId);
                if(model != null)
                {
                    return Json(model);
                }
            }

            return HttpNotFound();
        }

        #region Edit Topic Dialog

        [HttpGet]
        public ActionResult EditTopicDialog(Guid? topic)
        {
            if (topic != null)
            {
                var model = Services.RotationTopicService.PopulateHSETopicDialogModel((Guid)topic);

                var isMyTopic = Services.RotationTopicService.IsMyTopic(Guid.Parse(User.Identity.GetUserId()), (Guid)topic);

                model.IsManagerCommentsEnabled = User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.ExecutiveUser) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.Administrator) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.HeadLineManager) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.LineManager) && !isMyTopic;

                return PartialView("~/Views/Rotation/RotationReportBuilder/HSESection/_HSETopicFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicDialog(HSETopicDialogViewModel model)
        {
            if (model.Id.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicExist(Guid.Parse(model.RotationTopicGroupId), (Guid)model.Id, model.Name))
                    {
                        ModelState.AddModelError("Name", "Safety Item with this name already exist.");
                    }

                    else
                    {
                        Services.RotationTopicService.UpdateHSETopic(model);
                        return Content("ok");
                    }
                }

                model = Services.RotationTopicService.PopulateHSETopicDialogModel((Guid)model.Id);

                return PartialView("~/Views/Rotation/RotationReportBuilder/HSESection/_HSETopicFormDialog.cshtml", model);

            }


            return HttpNotFound();
        }

        #endregion

        #region Add Topic

        [HttpGet]
        public ActionResult AddInlineTopic(Guid destId)
        {

            var model = Services.RotationTopicService.PopulateHSETopicInlineModel(destId);

            return PartialView("~/Views/Rotation/RotationReportBuilder/HSESection/_HSEAddInlineTopic.cshtml", model);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddInlineTopic(HSEAddInlineTopicViewModel model)
        {
            if (model.RotationId.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicExist((Guid)model.Type, Guid.NewGuid(), model.Reference))
                    {
                        ModelState.AddModelError("Reference", "An item with this category and reference already exists");
                    }

                    else
                    {
                        Services.RotationTopicService.AddHSETopic(model);

                        return new HttpStatusCodeResult(HttpStatusCode.OK);
                    }
                }

                var errorMessages = ModelState.GetValidErrorFromState();

                return Content(errorMessages);
            }

            return HttpNotFound();
        }

        #endregion

    }
}