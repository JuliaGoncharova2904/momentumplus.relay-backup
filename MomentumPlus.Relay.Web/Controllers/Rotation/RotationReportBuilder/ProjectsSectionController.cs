﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Core.Authorization;
using System.Net;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Web.Extensions;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ProjectsSectionController : BaseController
    {
        // IOC
        public ProjectsSectionController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Draft(Guid rotationId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            if (Services.RotationModuleService.IsModuleEnabled(ModuleType.Project, rotationId, ModuleSourceType.Draft))
            {
                IEnumerable<ProjectsTopicViewModel> model = Services.RotationModuleService
                                                                        .GetProjectModuleTopics(rotationId, ModuleSourceType.Draft, RotationType.Swing, filter)
                                                                        .OrderBy(p => p.Project)
                                                                        .ThenBy(p => p.Reference);

                return PartialView("~/Views/Rotation/RotationReportBuilder/ProjectsSection/_ProjectsDraftSection.cshtml", model);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult Received(Guid rotationId, Guid? receivedRotationId)
        {
            IEnumerable<ProjectsTopicViewModel> model = Services.RotationModuleService
                                                                    .GetProjectModuleTopics(rotationId, ModuleSourceType.Received).Where(t => t.IsSharingTopic == false)
                                                                    .OrderBy(p => p.Project)
                                                                    .ThenBy(p => p.Reference);

            if (receivedRotationId.HasValue)
                model = model.Where(m => m.FromRotationId == receivedRotationId.Value);

            return PartialView("~/Views/Rotation/RotationReportBuilder/ProjectsSection/_ProjectsReceivedSection.cshtml", model);
        }

        [HttpGet]
        public ActionResult History(Guid rotationId)
        {
            IEnumerable<ProjectsTopicViewModel> model = Services.RotationModuleService
                                                                .GetProjectModuleTopics(rotationId, ModuleSourceType.Draft)
                                                                .OrderBy(p => p.Project)
                                                                .ThenBy(p => p.Reference);

            return PartialView("~/Views/Rotation/RotationReportBuilder/ProjectsSection/_ProjectsHistorySection.cshtml", model);
        }

        [HttpPost]
        public ActionResult UpdateTopic(ProjectsTopicViewModel model)
        {
            model = Services.RotationTopicService.UpdateProjectTopic(model);

            return Json(model);
        }

        [HttpPost]
        public ActionResult ReloadTopic(Guid topicId)
        {
            if(topicId != Guid.Empty)
            {
                ProjectsTopicViewModel model = Services.RotationTopicService.GetProjectTopic(topicId);
                if(model != null)
                {
                    return Json(model);
                }
            }

            return HttpNotFound();
        }

        #region Edit Topic

        [HttpGet]
        public ActionResult EditTopicDialog(Guid? topic)
        {
            if (topic != null)
            {
                var model = Services.RotationTopicService.PopulateProjectTopicDialogModel((Guid)topic);

                var isMyTopic = Services.RotationTopicService.IsMyTopic(Guid.Parse(User.Identity.GetUserId()), (Guid)topic);

                model.IsManagerCommentsEnabled = User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.ExecutiveUser) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.Administrator) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.HeadLineManager) && !isMyTopic ||
                                                 User.IsInRole(iHandoverRoles.Relay.LineManager) && !isMyTopic;

                return PartialView("~/Views/Rotation/RotationReportBuilder/ProjectsSection/_ProjectTopicFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicDialog(ProjectTopicDialogViewModel model)
        {
            if (model.Id.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicExist(Guid.Parse(model.RotationTopicGroupId), (Guid)model.Id, model.Name))
                    {
                        ModelState.AddModelError("Name", "Project Item with this name already exist.");
                    }

                    else
                    {
                        Services.RotationTopicService.UpdateProjectTopic(model);
                        return Content("ok");
                    }
                }

                model = Services.RotationTopicService.PopulateProjectTopicDialogModel((Guid)model.Id);

                return PartialView("~/Views/Rotation/RotationReportBuilder/ProjectsSection/_ProjectTopicFormDialog.cshtml", model);

            }


            return HttpNotFound();
        }

        #endregion

        #region Add Topic

        [HttpGet]
        public ActionResult AddInlineTopic(Guid destId)
        {

            var model = Services.RotationTopicService.PopulateProjectTopicInlineModel(destId);

            return PartialView("~/Views/Rotation/RotationReportBuilder/ProjectsSection/_ProjectAddInlineTopic.cshtml", model);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddInlineTopic(ProjectAddInlineTopicViewModel model)
        {
            if (model.RotationId.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicExist((Guid)model.Project, Guid.NewGuid(), model.Reference))
                    {
                        ModelState.AddModelError("Reference", "An item with this category and reference already exists");
                    }

                    else
                    {
                        Services.RotationTopicService.AddProjectTopic(model);

                        return new HttpStatusCodeResult(HttpStatusCode.OK);
                    }
                }

                var errorMessages = ModelState.GetValidErrorFromState();

                return Content(errorMessages);
            }

            return HttpNotFound();
        }

        #endregion
    }
}