﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.Controllers.Rotation
{
    public class DailyReportsPanelController : BaseController
    {
        // IOC
        public DailyReportsPanelController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult DailyReportsPanel(Guid userId)
        {
            Guid currentUserId = Guid.Parse(User.Identity.GetUserId());

            string safetyVersion = ConfigurationManager.AppSettings["safetySectionVersion"];

            DailyReportsPanelViewModel model = Services.DailyReportService.PopulateDailyReportsPanelViewModel(userId, userId == currentUserId, safetyVersion);

            if (model.DailyReports.Any())
            {
                return PartialView("~/Views/Rotation/DailyReportsPanel/_DailyReportsPanel.cshtml", model);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult DailyReportSlide(Guid shiftId)
        {
            string safetyVersion = ConfigurationManager.AppSettings["safetySectionVersion"];

            DailyReportViewModel model = Services.DailyReportService
                .PopulateDailyReportViewModel(shiftId, Guid.Parse(User.Identity.GetUserId()), safetyVersion);

            return PartialView("~/Views/Rotation/DailyReportsPanel/_DailyReportSlide.cshtml", model);
        }

        [HttpGet]
        public ActionResult CommonReportSlide(Guid userId)
        {
            string safetyVersion = ConfigurationManager.AppSettings["safetySectionVersion"];
            int safetyMessagesCounter = Services.DailyReportService.GetUnreadSafetyMessages(userId, safetyVersion);

            return PartialView("~/Views/Rotation/DailyReportsPanel/_CommonReportSlide.cshtml", safetyMessagesCounter);
        }
    }
}