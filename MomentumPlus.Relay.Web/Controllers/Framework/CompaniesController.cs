﻿using System;
using System.Web.Mvc;
using MomentumPlus.Core.Authorization;
using MomentumPlus.Relay.Constants;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using MvcPaging;

namespace MomentumPlus.Relay.Web.Controllers.Framework
{
    [Authorize(Roles = iHandoverRoles.Relay.Administrator + "," + iHandoverRoles.Relay.iHandoverAdmin + "," + iHandoverRoles.Relay.ExecutiveUser)]
    public class CompaniesController : BaseController
    {
        //IOC
        public CompaniesController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        public ActionResult Index(int? page)
        {
            var model = Services.CompanyService.GetCompanies().ToPagedList(page - 1 ?? 0, NumericConstants.Paginator.PaginatorPageSize);

            return View("~/Views/Framework/MasterLists/Companies/Index.cshtml", model);
        }

        [HttpGet]
        public ActionResult CreateCompanyDialog()
        {
            ViewBag.Title = "Create Company";
            return PartialView("~/Views/Framework/MasterLists/Companies/_CompanyFormDialog.cshtml");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCompanyDialog(CompanyViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.CompanyService.CompanyExist(model))
                {
                    ModelState.AddModelError("Name", "Company width this name, already exist.");
                }

                if (Services.CompanyService.IsDefaultCompanyExist(model) && model.IsDefaultCompany)
                {
                    ModelState.AddModelError("IsDefaultCompany", "The default company is already installed.");
                }

                else
                {
                    Services.CompanyService.AddCompany(model);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/MasterLists/Companies/_CompanyFormDialog.cshtml", model);
        }



        [HttpGet]
        public ActionResult EditCompanyDialog(Guid? Id)
        {
            if (Id != null)
            {
                var model = Services.CompanyService.GetCompany((Guid)Id);
                ViewBag.Title = "Edit Company";

                if (model != null)
                {
                    return PartialView("~/Views/Framework/MasterLists/Companies/_CompanyFormDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCompanyDialog(CompanyViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.CompanyService.CompanyExist(model))
                {
                    ModelState.AddModelError("Name", "Company width this name, already exist.");
                }
                if (Services.CompanyService.IsDefaultCompanyExist(model) && model.IsDefaultCompany)
                {
                    ModelState.AddModelError("IsDefaultCompany", "The default company is already installed.");
                }

                else
                {
                    Services.CompanyService.UpdateCompany(model);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/MasterLists/Companies/_CompanyFormDialog.cshtml", model);
        }

    }
}