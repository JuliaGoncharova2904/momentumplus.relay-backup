﻿using System.Web.Mvc;
using MomentumPlus.Relay.Interfaces;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ModulesController : BaseController
    {
        //IOC
        public ModulesController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }
    }
}