﻿using System;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ProjectModuleController : BaseController
    {
        //IOC
        public ProjectModuleController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }
        public ActionResult Index(
                  int? topicGroupsPageNo,
                  int? topicsPageNo,
                  int? tasksPageNo,
                  Guid? topic = null,
                  Guid? topicGroup = null)
        {
            var model = Services.ModuleService.PopulateBaseModuleViewModel(ModuleType.Project, topicGroup, topic,
                topicGroupsPageNo, topicsPageNo, tasksPageNo);

            return View("~/Views/Framework/Modules/Project/Index.cshtml", model);
        }

        #region TopicGroup

        [HttpGet]
        public ActionResult CreateTopicGroupDialog(Guid? module)
        {
            if (module != null)
            {
                var model = new ProjectTopicGroupViewModel
                {
                    ModuleId = module.Value,
                    Projects = Services.ProjectService.GetProjectList(),
                    Name = module.ToString(),
                    Enabled = true

                };


                return PartialView("~/Views/Framework/Modules/Project/_TopicGroupFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTopicGroupDialog(ProjectTopicGroupViewModel model)
        {
            model = Services.TopicGroupService.PopulateProjectTopicGroupViewModel(model);

            if (ModelState.IsValid)
            {
                if (Services.TopicGroupService.TopicGroupRelationExist(model))
                {
                    ModelState.AddModelError("SelectedProject", "Relation for this Project already exist.");
                }

                else
                {
                    Services.TopicGroupService.AddTopicGroup(model);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/Modules/Project/_TopicGroupFormDialog.cshtml", model);
        }

        [HttpGet]
        public ActionResult EditTopicGroupDialog(Guid? Id)
        {
            if (Id != null)
            {
                var model = Services.TopicGroupService.GetTopicGroupViewModel((Guid)Id);

                var projectModel = new ProjectTopicGroupViewModel
                {
                    RelationId = model.RelationId,
                    ModuleId = model.ModuleId,
                    Enabled = model.Enabled,
                    Description = model.Description,
                    SelectedProject = model.RelationId.ToString(),
                    Id = model.Id
                };

                projectModel = Services.TopicGroupService.PopulateProjectTopicGroupViewModel(projectModel);

                return PartialView("~/Views/Framework/Modules/Project/_TopicGroupFormDialog.cshtml", projectModel);
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicGroupDialog(ProjectTopicGroupViewModel model)
        {
            if (ModelState.IsValid)
            {
                model = Services.TopicGroupService.PopulateProjectTopicGroupViewModel(model);

                if (Services.TopicGroupService.TopicGroupRelationExist(model))
                {
                    ModelState.AddModelError("SelectedProject", "Relation for this Project already exist.");
                }

                else
                {
                    Services.TopicGroupService.UpdateTopicGroup(model);
                    return Content("ok");
                }
            }
            return PartialView("~/Views/Framework/Modules/Project/_TopicGroupFormDialog.cshtml", model);
        }

        #endregion


        #region Topic

        [HttpGet]
        public ActionResult CreateTopicDialog(Guid? group)
        {
            if (group != null)
            {
                var model = new rTopicViewModel { TopicGroupId = group.Value, Enabled = true };

                return PartialView("~/Views/Framework/Modules/Shared/_TopicFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTopicDialog(rTopicViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.TopicService.TopicExist(model))
                {
                    ModelState.AddModelError("Name", "Project Item with this name already exist.");
                }

                else
                {
                    Services.TopicService.AddTopic(model);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/Modules/Shared/_TopicFormDialog.cshtml", model);
        }

        [HttpGet]
        public ActionResult EditTopicDialog(Guid? Id)
        {
            if (Id != null)
            {
                var model = Services.TopicService.GetTopicViewModel((Guid)Id);

                if (model != null)
                {
                    return PartialView("~/Views/Framework/Modules/Shared/_TopicFormDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicDialog(rTopicViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.TopicService.TopicExist(model))
                {
                    ModelState.AddModelError("Name", "Project Item with this name already exist.");
                }

                else
                {
                    Services.TopicService.UpdateTopic(model);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/Modules/Shared/_TopicFormDialog.cshtml", model);
        }

        #endregion


        #region Task

        [HttpGet]
        public ActionResult CreateTaskDialog(Guid? topic)
        {
            if (topic != null)
            {
                var model = new rTaskViewModel { TopicId = topic.Value, Enabled = true };

                return PartialView("~/Views/Framework/Modules/Shared/_TaskFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTaskDialog(rTaskViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.TaskService.TaskExist(model))
                {
                    ModelState.AddModelError("Name", "Task with this name already exist.");
                }

                else
                {
                    Services.TaskService.AddTask(model);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/Modules/Shared/_TaskFormDialog.cshtml", model);
        }

        [HttpGet]
        public ActionResult EditTaskDialog(Guid? Id)
        {
            if (Id != null)
            {
                var model = Services.TaskService.GetTaskViewModel((Guid)Id);

                if (model != null)
                {
                    return PartialView("~/Views/Framework/Modules/Shared/_TaskFormDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTaskDialog(rTaskViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.TaskService.TaskExist(model))
                {
                    ModelState.AddModelError("Name", "Task with this name already exist.");
                }

                else
                {
                    Services.TaskService.UpdateTask(model);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/Modules/Shared/_TaskFormDialog.cshtml", model);
        }

        #endregion
    }
}