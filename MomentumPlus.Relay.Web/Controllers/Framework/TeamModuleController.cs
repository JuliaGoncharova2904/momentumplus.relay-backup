﻿using System;
using System.Web.Mvc;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class TeamModuleController : BaseController
    {
        //IOC
        public TeamModuleController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }
        public ActionResult Index(
                  int? topicGroupsPageNo,
                  int? topicsPageNo,
                  int? tasksPageNo,
                  Guid? topic = null,
                  Guid? topicGroup = null)
        {
            var model = Services.ModuleService.PopulateBaseModuleViewModel(ModuleType.Team, topicGroup, topic,
                topicGroupsPageNo, topicsPageNo, tasksPageNo);

            return View("~/Views/Framework/Modules/Team/Index.cshtml", model);
        }

        #region TopicGroup

        [HttpGet]
        public ActionResult CreateTopicGroupDialog(Guid? module)
        {
            if (module != null)
            {
                var model = new TeamTopicGroupViewModel
                {
                    ModuleId = module.Value,
                    Teams = Services.TeamService.GetTeamList(),
                    Name = module.ToString(),
                    Enabled = true

                };


                return PartialView("~/Views/Framework/Modules/Team/_TopicGroupFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTopicGroupDialog(TeamTopicGroupViewModel model)
        {
            model = Services.TopicGroupService.PopulateTeamTopicGroupViewModel(model);

            if (ModelState.IsValid)
            {
                if (Services.TopicGroupService.TopicGroupRelationExist(model))
                {
                    ModelState.AddModelError("SelectedTeam", "Relation for this Team already exist.");
                }

                else
                {
                    Services.TopicGroupService.AddTopicGroup(model);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/Modules/Team/_TopicGroupFormDialog.cshtml", model);
        }

        [HttpGet]
        public ActionResult EditTopicGroupDialog(Guid? Id)
        {
            if (Id != null)
            {
                var model = Services.TopicGroupService.GetTopicGroupViewModel((Guid)Id);

                var teamModel = new TeamTopicGroupViewModel
                {
                    RelationId = model.RelationId,
                    ModuleId = model.ModuleId,
                    Description = model.Description,
                    SelectedTeam = model.RelationId.ToString(),
                    Id = model.Id,
                    Enabled = model.Enabled
                };

                teamModel = Services.TopicGroupService.PopulateTeamTopicGroupViewModel(teamModel);

                return PartialView("~/Views/Framework/Modules/Team/_TopicGroupFormDialog.cshtml", teamModel);
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicGroupDialog(TeamTopicGroupViewModel model)
        {
            if (ModelState.IsValid)
            {
                model = Services.TopicGroupService.PopulateTeamTopicGroupViewModel(model);

                if (Services.TopicGroupService.TopicGroupRelationExist(model))
                {
                    ModelState.AddModelError("SelectedTeam", "Relation for this Project already exist.");
                }

                else
                {
                    Services.TopicGroupService.UpdateTopicGroup(model);
                    return Content("ok");
                }
            }
            return PartialView("~/Views/Framework/Modules/Team/_TopicGroupFormDialog.cshtml", model);
        }

        #endregion


        #region Topic

        [HttpGet]
        public ActionResult CreateTopicDialog(Guid? group)
        {
            if (group != null)
            {
                var model = new rTopicViewModel { TopicGroupId = group.Value, Enabled = true };

                return PartialView("~/Views/Framework/Modules/Shared/_TopicFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTopicDialog(rTopicViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.TopicService.TopicExist(model))
                {
                    ModelState.AddModelError("Name", "Team Item with this name already exist.");
                }

                else
                {
                    Services.TopicService.AddTopic(model);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/Modules/Shared/_TopicFormDialog.cshtml", model);
        }

        [HttpGet]
        public ActionResult EditTopicDialog(Guid? Id)
        {
            if (Id != null)
            {
                var model = Services.TopicService.GetTopicViewModel((Guid)Id);

                if (model != null)
                {
                    return PartialView("~/Views/Framework/Modules/Shared/_TopicFormDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicDialog(rTopicViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.TopicService.TopicExist(model))
                {
                    ModelState.AddModelError("Name", "Team Item with this name already exist.");
                }

                else
                {
                    Services.TopicService.UpdateTopic(model);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/Modules/Shared/_TopicFormDialog.cshtml", model);
        }

        #endregion


        #region Task

        [HttpGet]
        public ActionResult CreateTaskDialog(Guid? topic)
        {
            if (topic != null)
            {
                var model = new rTaskViewModel { TopicId = topic.Value, Enabled = true };

                return PartialView("~/Views/Framework/Modules/Shared/_TaskFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTaskDialog(rTaskViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.TaskService.TaskExist(model))
                {
                    ModelState.AddModelError("Name", "Task with this name already exist.");
                }

                else
                {
                    Services.TaskService.AddTask(model);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/Modules/Shared/_TaskFormDialog.cshtml", model);
        }

        [HttpGet]
        public ActionResult EditTaskDialog(Guid? Id)
        {
            if (Id != null)
            {
                var model = Services.TaskService.GetTaskViewModel((Guid)Id);

                if (model != null)
                {
                    return PartialView("~/Views/Framework/Modules/Shared/_TaskFormDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTaskDialog(rTaskViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.TaskService.TaskExist(model))
                {
                    ModelState.AddModelError("Name", "Task with this name already exist.");
                }

                else
                {
                    Services.TaskService.UpdateTask(model);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/Modules/Shared/_TaskFormDialog.cshtml", model);
        }

        #endregion

    }
}