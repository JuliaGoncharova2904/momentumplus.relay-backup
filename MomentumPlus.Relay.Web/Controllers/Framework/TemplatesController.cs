﻿using System;
using System.Web.Mvc;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class TemplatesController : BaseController
    {
        //IOC
        public TemplatesController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Index(
            string selectedTemplate,
            ModuleType? selectedModule,
            int? topicGroupsPageNo,
            int? topicsPageNo,
            int? tasksPageNo,
            Guid? topic = null,
            Guid? topicGroup = null)
        {

            ModelState.Clear();

            var model = Services.TemplateService.PopulateTemplatesViewModel(selectedTemplate, selectedModule, topicGroupsPageNo, topicsPageNo, tasksPageNo, topic, topicGroup);

            return View("~/Views/Framework/Templates/Index.cshtml", model);
        }


        [HttpPost]
        public ActionResult UpdateTemplateTask(Guid baseTaskId, Guid templateId, bool status)
        {
            try
            {
                Services.TaskService.UpdateChildTaskStatus(baseTaskId, templateId, status);
                return Content("OK");

            }
            catch (Exception error)
            {
                return Content(error.Message);
            }
        }

        [HttpPost]
        public ActionResult UpdateTemplateTopicGroup(Guid baseTopicGroupId, Guid templateId, bool status)
        {
            try
            {
                Services.TopicGroupService.UpdateChildTopicGroupStatus(baseTopicGroupId, templateId, status);
                return Content("OK");

            }
            catch (Exception error)
            {
                return Content(error.Message);
            }
        }

        [HttpPost]
        public ActionResult UpdateTemplateTopic(Guid baseTopicId, Guid templateId, bool status)
        {
            try
            {
                Services.TopicService.UpdateChildTopicStatus(baseTopicId, templateId, status);
                return Content("OK");

            }
            catch (Exception error)
            {
                return Content(error.Message);
            }
        }

        [HttpPost]
        public ActionResult UpdateTemplateModule(Guid baseModuleId, Guid templateId, bool status)
        {
            try
            {
                Services.ModuleService.ChangeTemplateModuleStatus(baseModuleId, templateId, status);
                return Content("OK");

            }
            catch (Exception error)
            {
                return Content(error.Message);
            }
        }



        [HttpGet]
        public ActionResult CreateTemplateDialog()
        {
            var model = new TemplateViewModel
            {
                Id = Guid.NewGuid()
            };

            return PartialView("~/Views/Framework/Templates/_TemplateFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTemplateDialog(TemplateViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.TemplateService.TemplateExist(model))
                {
                    ModelState.AddModelError("Name", "Template with this name already exist.");
                }

                else
                {
                    Services.TemplateService.AddTemplate(model);
                    return Content("ok");
                }
            }
            return PartialView("~/Views/Framework/Templates/_TemplateFormDialog.cshtml", model);
        }

        [HttpGet]
        public ActionResult EditTemplateDialog(Guid? Id)
        {
            if (Id != null)
            {
                var model = Services.TemplateService.GetTemplateViewModel((Guid)Id);

                if (model != null)
                {
                    return PartialView("~/Views/Framework/Templates/_TemplateFormDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTemplateDialog(TemplateViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.TemplateService.TemplateExist(model))
                {
                    ModelState.AddModelError("Name", "Template with this name already exist.");
                }
                else
                {
                    Services.TemplateService.UpdateTemplate(model);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/Templates/_TemplateFormDialog.cshtml", model);
        }

    }
}