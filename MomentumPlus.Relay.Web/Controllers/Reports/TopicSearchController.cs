﻿using System;
using System.Collections.Generic;
using System.Linq;using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using Rotativa;
using Rotativa.Options;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class TopicSearchController : BaseController
    {
        public TopicSearchController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        {
        }

        public ActionResult Index(TopicSearchViewModel model)
        {
            model.SelectedTopicGroupsIds = model.SelectedTopicGroupsIds ?? new Guid?[] {};

            return View("~/Views/Reports/TopicSearch/Index.cshtml", model);
        }


        public ActionResult TopicGroupFilterBarItems()
        {
            var topicGroups = Services.TopicSearchService.GetBaseTopicGroups();

            return Json(topicGroups, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DateRangeFilterBarItems()
        {
            List<SearchBarItem> items = new List<SearchBarItem>();

            items.Add(new SearchBarItem {Id = Guid.NewGuid(), Name = TopicSearchDateRangeType.AllTime.ToString()});
            items.Add(new SearchBarItem { Id = Guid.NewGuid(), Name = TopicSearchDateRangeType.Last7Days.ToString() });
            items.Add(new SearchBarItem { Id = Guid.NewGuid(), Name = TopicSearchDateRangeType.LastDay.ToString() });
            items.Add(new SearchBarItem { Id = Guid.NewGuid(), Name = TopicSearchDateRangeType.LastMonth.ToString() });
            items.Add(new SearchBarItem { Id = Guid.NewGuid(), Name = TopicSearchDateRangeType.LastYear.ToString() });

            return Json(items, JsonRequestBehavior.AllowGet);
        }




        public ActionResult SearchTopics(TopicSearchViewModel model)
        {
            var userId = Guid.Parse(User.Identity.GetUserId());

            var searchResult = Services.TopicSearchService.SearchTopics(userId, model);

            return PartialView("~/Views/Reports/TopicSearch/_TopicSearchResult.cshtml", searchResult);
        }

        public ActionResult TopicSearchReportPdf(TopicSearchViewModel model)
        {
            var selectedTopicGroupsIds = model.SelectedTopicGroupsIds != null && model.SelectedTopicGroupsIds.Any() ? string.Join(",", model.SelectedTopicGroupsIds) : string.Empty;
            var selectedUserIds = model.SelectedUserIds != null && model.SelectedUserIds.Any() ? string.Join(",", model.SelectedUserIds) : string.Empty;

            var pdfResult = new ActionAsPdf("PrintPdf", new { model.PrintType, model.DateRangeType, selectedTopicGroupsIds, selectedUserIds })
            {
                FileName = "Topic-Search-Report.pdf",
                PageOrientation = model.PrintType == TopicSearchPrintType.portrait ? Orientation.Portrait : Orientation.Landscape,
                IsGrayScale = false,
                IsJavaScriptDisabled = false,
                PageMargins = new Margins(10, 10, 10, 10)
            };

            var binary = pdfResult.BuildFile(ControllerContext);

            return File(binary, "application/pdf");
        }

        [AllowAnonymous]
        public ActionResult PrintPdf(TopicSearchPrintType printType, TopicSearchDateRangeType? dateRangeType, string selectedTopicGroupsIds = "", string selectedUserIds = "")
        {
            var SelectedTopicGroupsIds = string.IsNullOrEmpty(selectedTopicGroupsIds) ? new List<Guid?>() : selectedTopicGroupsIds.Split(',').Select(tg => (Guid?) Guid.Parse(tg));
            var SelectedUserIds = string.IsNullOrEmpty(selectedUserIds) ? new List<Guid?>() : selectedUserIds.Split(',').Select(tg => (Guid?)Guid.Parse(tg));

            var viewModel = Services.TopicSearchService.PopulateTopicSearchReportViewModel(SelectedTopicGroupsIds, SelectedUserIds, dateRangeType);

            if (printType == TopicSearchPrintType.portrait)
            {
                return View("~/Views/Reports/TopicSearch/TopicPdfReport/PortraitViewReport.cshtml", viewModel);
            }
            else
            {
                return View("~/Views/Reports/TopicSearch/TopicPdfReport/LandscapeViewReport.cshtml", viewModel);
            }
        }
    }
}
