﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ProjectTasksController : BaseController
    {
        //IOC
        public ProjectTasksController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        public ActionResult PlannedTasksList(Guid projectId, int? page, int? pageSize, ProjectTasksFilterOptions? filterOptions, ProjectTasksSortOptions? sortOptions)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());
            Services.AccessService.UserHasAccessToProject(userId, projectId, throwException: true);

            ProjectTasksPanelViewModel model = Services.ProjectService.GetPlannedTasksForProject(userId, projectId,
                                                                                                    page - 1 ?? 0,
                                                                                                    pageSize ?? 5,
                                                                                                    filterOptions ?? ProjectTasksFilterOptions.AllTasks,
                                                                                                    sortOptions ?? ProjectTasksSortOptions.Default);

            return PartialView("~/Views/ProjectAdmin/FollowerTasksPanel/_PlannedTasks.cshtml", model);
        }

        public ActionResult AdHocTasksList(Guid projectId, int? page, int? pageSize, ProjectTasksFilterOptions? filterOptions, ProjectTasksSortOptions? sortOptions)
        {
            Services.AccessService.UserHasAccessToProject(Guid.Parse(User.Identity.GetUserId()), projectId, throwException: true);

            ProjectTasksPanelViewModel model = Services.ProjectService.GetAdHocTasksForProject( projectId, 
                                                                                                page - 1 ?? 0, 
                                                                                                pageSize ?? 5, 
                                                                                                filterOptions ?? ProjectTasksFilterOptions.AllTasks,
                                                                                                sortOptions ?? ProjectTasksSortOptions.Default);

            return PartialView("~/Views/ProjectAdmin/FollowerTasksPanel/_AdHocTasks.cshtml", model);
        }

        public ActionResult TaskFilter(ProjectTasksFilterOptions filter = ProjectTasksFilterOptions.AllTasks)
        {
            return PartialView("~/Views/ProjectAdmin/_TaskFilterPopover.cshtml", filter);
        }

        [HttpPost]
        public ActionResult UpdateTask(ProjectTaskTopicViewModel model)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());
            ProjectTaskTopicViewModel result = Services.ProjectService.UpdateProjectTask(userId, model);

            return Json(result);
        }

        [HttpPost]
        public ActionResult ReloadTask(Guid topicId)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());
            ProjectTaskTopicViewModel result = Services.ProjectService.GetProjectTask(userId, topicId);

            return Json(result);
        }

        #region Add task dialog

        [HttpGet]
        public ActionResult AddTaskDialog(Guid projectId)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());
            Services.AccessService.UserHasAccessToProject(userId, projectId, throwException: true);
            ProjectAdminAddTaskDialogViewModel model = Services.ProjectService.PopulateAddTaskModel(userId, projectId);

            return PartialView("~/Views/ProjectAdmin/FollowerTasksPanel/Dialogs/_AddTaskFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddTaskDialog(ProjectAdminAddTaskDialogViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.SendingOption != TaskSendingOptions.Pending || model.SendDate.HasValue)
                {
                    Guid userId = Guid.Parse(User.Identity.GetUserId());
                    Services.AccessService.UserHasAccessToProject(userId, model.ProjectId, throwException: true);
                    Services.ProjectService.AddTask(model);
                    return Content("ok");
                }

                ModelState.AddModelError("SendDate", "SendDate is required field");
            }

            return PartialView("~/Views/ProjectAdmin/FollowerTasksPanel/Dialogs/_AddTaskFormDialog.cshtml", model);
        }

        #endregion

        #region Edit task dialog

        [HttpGet]
        public ActionResult EditTaskDialog(Guid taskId)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());
            ProjectAdminEditTaskDialogViewModel model = Services.ProjectService.PopulateEditTaskModel(userId, taskId);

            return PartialView("~/Views/ProjectAdmin/FollowerTasksPanel/Dialogs/EditDialog/_EditTaskFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTaskDialog(ProjectAdminEditTaskDialogViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.TaskDetailTab.SendingOption != TaskSendingOptions.Pending || model.TaskDetailTab.SendDate.HasValue)
                {
                    Services.ProjectService.UpdateTask(model);
                    return Content("ok");
                }

                ModelState.AddModelError("SendDate", "SendDate is required field");
            }

            return PartialView("~/Views/ProjectAdmin/FollowerTasksPanel/Dialogs/EditDialog/_EditTaskFormDialog.cshtml", model);
        }

        #endregion

        #region Choose Teammate Dialog

        [HttpGet]
        public ActionResult GetChooseTeammateDialog(Guid projectId)
        {
            IEnumerable<SelectListItem> model = Services.ProjectService.GetProjectFollowerForAssigning(projectId);

            return PartialView("~/Views/ProjectAdmin/FollowerTasksPanel/Dialogs/_ChooseTeammateDialog.cshtml", model);
        }

        #endregion
    }
}