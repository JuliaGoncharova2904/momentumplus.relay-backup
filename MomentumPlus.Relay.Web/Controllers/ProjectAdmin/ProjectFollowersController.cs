﻿using MomentumPlus.Core.Authorization;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ProjectFollowersController : BaseController
    {
        //IOC
        public ProjectFollowersController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        #region Followers Dialog

        [HttpGet]
        public ActionResult FollowersDialog(Guid projectId)
        {
            return PartialView("~/Views/ProjectAdmin/ProjectFollowersPanel/ProjectFollowersDialog/_ProjectFollowersDialog.cshtml", projectId);
        }

        [HttpGet]
        public ActionResult TeamsTab(Guid projectId)
        {
            Services.AccessService.UserHasAccessToProject(Guid.Parse(User.Identity.GetUserId()), projectId, throwException: true);

            ProjectsTeamsTabViewModel model = Services.ProjectService.GetTeamsForProject(projectId);

            return PartialView("~/Views/ProjectAdmin/ProjectFollowersPanel/ProjectFollowersDialog/_TeamsTab.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TeamsTab(ProjectsTeamsTabViewModel model)
        {
            Services.AccessService.UserHasAccessToProject(Guid.Parse(User.Identity.GetUserId()), model.ProjectId, throwException: true);

            Services.ProjectService.UpdateTeamsForProject(model);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpGet]
        public ActionResult FollowersTab(Guid projectId, string searchQuery)
        {
            Services.AccessService.UserHasAccessToProject(Guid.Parse(User.Identity.GetUserId()), projectId, throwException: true);

            ProjectFollowersTabViewModel model = Services.ProjectService.GetTeamsMembersForProject(projectId, searchQuery);
            return PartialView("~/Views/ProjectAdmin/ProjectFollowersPanel/ProjectFollowersDialog/_TeamMembersTab.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FollowersTab(ProjectFollowersTabViewModel model)
        {
            Services.AccessService.UserHasAccessToProject(Guid.Parse(User.Identity.GetUserId()), model.ProjectId, throwException: true);

            Services.ProjectService.UpdateFollowersOfProject(model);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        #endregion

        public ActionResult FollowersPanel(Guid? projectId, int? page, int? pageSize)
        {
            if (projectId.HasValue)
            {
                Services.AccessService.UserHasAccessToProject(Guid.Parse(User.Identity.GetUserId()), projectId.Value, throwException: true);

                ProjectTeamPanelViewModel model = Services.ProjectService.GetProjectFollowers(projectId.Value, page - 1 ?? 0, pageSize ?? 6);
                return PartialView("~/Views/ProjectAdmin/ProjectFollowersPanel/_ProjectFollowers.cshtml", model);
            }

            return PartialView("~/Views/ProjectAdmin/ProjectFollowersPanel/_EmptyProjectFollowers.cshtml");
        }

    }
}