﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using MomentumPlus.Core.Authorization;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class AccountController : BaseController
    {
        //IOC
        public AccountController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        /// <summary>
        /// Render Main Page
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            if (User.Identity.Name == "admin@ihandover.co" && DateTime.UtcNow.Date >= new DateTime(2019, 7, 01))
            {
                AuthenticationManager.SignOut();

                if (IsMultiTenant)
                {
                    return View("~/Views/Account/MultiTenant/Main.cshtml");
                }
                return RedirectToAction("Login");
            }

            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Summary");
            }

            if (IsMultiTenant)
            {
                return View("~/Views/Account/MultiTenant/Main.cshtml");
            }

            return RedirectToAction("Login");
        }




        /// <summary>
        /// Render login view or redirect to home if already logged in.
        /// </summary>
        /// <param name="returnUrl">URL to return to on successful login</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Login(string returnUrl)
        {
            var tempData = TempData["Alerts"];

            if (User.Identity.Name == "admin@ihandover.co" && DateTime.UtcNow.Date >= new DateTime(2019, 7, 01))
            {
                AuthenticationManager.SignOut();

                if (IsMultiTenant)
                {
                    return View("~/Views/Account/MultiTenant/Main.cshtml");
                }

                return RedirectToAction("Login");
            }

            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Summary");
            }

            if (IsMultiTenant)
            {
                return View("~/Views/Account/MultiTenant/Login.cshtml", new LoginViewModel { ReturnUrl = returnUrl });
            }

            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }

        /// <summary>
        /// Handle login form submission
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                if (IsMultiTenant)
                {
                    return View("~/Views/Account/MultiTenant/Login.cshtml", model);
                }
                return View(model);
            }

            var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, shouldLockout: false);

            Session.Clear();

            Guid userId = Guid.Parse(SignInManager.AuthenticationManager.AuthenticationResponseGrant.Identity.GetUserId());

            if (model.UserName == "admin@ihandover.co" && DateTime.UtcNow.Date >= new DateTime(2019, 7, 01))
            {
                var user = UserManager.FindById(userId);
                user.LockoutEnabled = true;
                UserManager.Update(user);

                AuthenticationManager.SignOut();

                //block all users also

                ModelState.AddModelError("Message", "The user account is locked. Please speak to your iHandover Consultant.");

                if (IsMultiTenant)
                {
                    return View("~/Views/Account/MultiTenant/Login.cshtml", model);
                }

                return View(model);
            }

            switch (result)
            {
                case SignInStatus.Success:
                    Services.UserService.UpdateLastLoginDate(userId);
                    return RedirectAction(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    ModelState.AddModelError("Message", "The user account is locked. Please speak to your iHandover Consultant.");
                    if (IsMultiTenant)
                    {
                        return View("~/Views/Account/MultiTenant/Login.cshtml", model);
                    }
                    return View(model);
                case SignInStatus.Failure:
                    ModelState.AddModelError("Message", "The user name or password provided is incorrect.");
                    if (IsMultiTenant)
                    {
                        return View("~/Views/Account/MultiTenant/Login.cshtml", model);
                    }
                    return View(model);
            }

            return View(model);
        }


        [AllowAnonymous]
        public ActionResult RedirectAction(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            Guid userId = Guid.Parse(SignInManager.AuthenticationManager.AuthenticationResponseGrant.Identity.GetUserId());

            if (SignInManager.UserManager.IsInRole(userId, iHandoverRoles.Relay.LineManager) || SignInManager.UserManager.IsInRole(userId, iHandoverRoles.Relay.HeadLineManager))
            {
                return RedirectToAction("Index", "TeamRotations");
            }

            return RedirectToAction("Index", "Summary");
        }


        /// <summary>
        /// Log off current user and redirect to login page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            Session.Clear();

            return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Render forgotten password form
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult ForgottenPassword()
        {
            if (IsMultiTenant)
            {
                return View("~/Views/Account/MultiTenant/ForgottenPassword.cshtml");
            }

            return View(new ForgottenPasswordViewModel());
        }

        /// <summary>
        /// Render forgotten password form
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgottenPassword(ForgottenPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = UserManager.FindByName(model.Email);

                if (user != null)
                {
                    var resetGuid = Guid.NewGuid();
                    var resetUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Action("ResetPassword", new { resetPasswordHash = resetGuid }));
                    Services.UserService.ForgotPassword(user.Id, resetGuid, resetUrl);

                    AddAlert(AlertType.Success, "A password reset email has been sent to your email account, please check your email to continue the process.");

                    return RedirectToAction("Login");
                }

                ModelState.AddModelError("Email", "No user with that email address exists.");
            }

            if (IsMultiTenant)
            {
                return View("~/Views/Account/MultiTenant/ForgottenPassword.cshtml", model);
            }


            return View(model);
        }


        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPassword(Guid resetPasswordHash)
        {
            var userId = Services.UserService.GetEmployeeIdByResetHash(resetPasswordHash);

            if (userId.HasValue)
            {
                if (IsMultiTenant)
                {
                    return View("~/Views/Account/MultiTenant/ResetPassword.cshtml", new ResetPasswordViewModel { PasswordResetGuid = resetPasswordHash });
                }

                return View(new ResetPasswordViewModel { PasswordResetGuid = resetPasswordHash });
            }

            return RedirectToAction("Login");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userId = Services.UserService.GetEmployeeIdByResetHash(model.PasswordResetGuid);

                if (userId != null)
                {
                    Services.UserService.ResetEmployeePassword(userId.Value, model.NewPassword);
                    AddAlert(AlertType.Success, "Password changed successfully, you may now log in.");
                }

                return RedirectToAction("Login");

            }

            if (IsMultiTenant)
            {
                return View("~/Views/Account/MultiTenant/ResetPassword.cshtml", model);
            }

            return View(model);
        }

    }
}