﻿using System;
using MomentumPlus.Relay.Interfaces;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class SchedulerController : BaseController
    {
        public SchedulerController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork) {}

        public ActionResult Index()
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            SchedulerPanelViewModel schedulerPanel = Services.SchedulerService.GetTimelinesPanel(new SchedulerFilterOptionsViewModel { allTeams = true }, userId);

            if(schedulerPanel.IsSimpleUserMpode)
            {
                return View("~/Views/Scheduler/IndexSimple.cshtml", schedulerPanel);
            }
            else
            {
                SchedulerPageViewModel model = new SchedulerPageViewModel
                {
                    FilterPanel = Services.SchedulerService.GetFilterPanel(userId),
                    SchedulerPanel = schedulerPanel
                };

                return View("~/Views/Scheduler/IndexFull.cshtml", model);
            }
        }

        [HttpPost]
        public ActionResult SchedulerPanels(SchedulerFilterOptionsViewModel filterModel)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            SchedulerPanelViewModel schedulerPanel = Services.SchedulerService.GetTimelinesPanel(filterModel, userId);

            return PartialView("~/Views/Scheduler/_SchedulerPanel.cshtml", schedulerPanel);
        }

        [HttpPost]
        public ActionResult TimelinePanel(Guid userId)
        {
            var currentUserId = Guid.Parse(User.Identity.GetUserId());

            SchedulerTimelinePanelViewModel timelinePanel = Services.SchedulerService.GetTimelineForUser(userId, userId == currentUserId);

            return PartialView("~/Views/Scheduler/_TimelinePanel.cshtml", timelinePanel);
        }
    }
}