﻿using System;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Interfaces;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class MenuController : BaseController
    {
        public MenuController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        {
        }

        public ActionResult GetNavifationBar()
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            return PartialView("~/Views/Shared/Layouts/Main/_MainNavigationBar.cshtml", IsViewMode);
        }

    }
}