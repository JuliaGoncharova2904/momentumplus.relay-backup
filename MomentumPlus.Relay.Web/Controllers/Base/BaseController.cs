﻿using System;
using System.Collections.Generic;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System.Web.Mvc;
using System.Web;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using MomentumPlus.Core.Authorization;
using MomentumPlus.Relay.Web.Attributes;

namespace MomentumPlus.Relay.Web.Controllers
{
    [LogFilter]
    [LoadUserLayout]
    public abstract class BaseController : Controller
    {
        #region IOC

        private IServicesUnitOfWork _services;

        private ApplicationSignInManager _signInManager;

        private ApplicationUserManager _userManager;


        protected IServicesUnitOfWork Services
        {
            get
            {
                return _services;
            }
        }

        protected ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set { _signInManager = value; }
        }


        protected ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        protected IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }


        protected BaseController(IServicesUnitOfWork servicesUnitOfWork)
        {
            _services = servicesUnitOfWork;
            IsMultiTenant = bool.Parse(WebConfigurationManager.AppSettings["IsMultiTenant"]);
        }

        #endregion


        #region ViewMode Swing Owner ID

        private Guid? _swingOwnerId;

        public Guid SwingOwnerId
        {
            get
            {
                if (!this._swingOwnerId.HasValue)
                {
                    Guid userId;
                    string userIdString = (string)this.RouteData.Values["userId"];

                    if (!string.IsNullOrEmpty(userIdString) && Guid.TryParse(userIdString, out userId))
                    {
                        this._swingOwnerId = userId;
                    }
                    else
                    {
                        this._swingOwnerId = Guid.Parse(User.Identity.GetUserId());
                    }
                }

                return this._swingOwnerId.Value;
            }
        }

        public bool IsViewMode
        {
            get
            {
                return !string.IsNullOrEmpty((string)this.RouteData.Values["userId"]);
            }
        }

        #endregion

        public bool IsMultiTenant { get; }

        public bool OffSiteUserAsked
        {
            get
            {
                return (bool)(Session["OffSiteUserAsked"] ?? false);
            }
            set
            {
                Session["OffSiteUserAsked"] = value;
            }
        }

        #region Alerts

        /// <summary>
        /// Add an alert to be shown on the next page loaded.
        /// </summary>
        /// <param name="type">The type of the alert.</param>
        /// <param name="message">Alert message.</param>
        protected void AddAlert(AlertType type, string message)
        {
            var alert = new AlertViewModel
            {
                Type = type,
                Message = message
            };

            List<AlertViewModel> alerts = TempData.ContainsKey("Alerts")
                ? (List<AlertViewModel>)TempData["Alerts"]
                : new List<AlertViewModel>();

            alerts.Insert(0, alert);

            TempData["Alerts"] = alerts;
        }

        #endregion


    }
}