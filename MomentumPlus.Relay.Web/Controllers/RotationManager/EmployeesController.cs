﻿using MomentumPlus.Relay.Constants;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using MvcPaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MomentumPlus.Core.Authorization;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class EmployeesController : BaseController
    {
        //IOC
        public EmployeesController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Index()
        {
            return View("~/Views/RotationManager/Employees/Index.cshtml");
        }

        [HttpPost]
        public ActionResult GetEmployeesForTeam(int? page, Guid? teamId)
        {
            if (teamId.HasValue)
            {
                int currentPageIndex = page.HasValue ? page.Value - 1 : 0;
                var employees = Services.UserService
                                    .GetEmployeesForTeam(teamId.Value)
                                    .ToPagedList(currentPageIndex, NumericConstants.Paginator.PaginatorPageSize);

                return Json(new
                {
                    Employees = employees.Select(m => new { Id = m.Id, Name = m.Name + " " + m.Surname }).OrderBy( em => em.Name),
                    IsFirstPage = employees.IsFirstPage,
                    IsLastPage = employees.IsLastPage,
                    PageNumber = employees.PageNumber,
                    PageCount = employees.PageCount
                });
            }

            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult GetEmployeesForPosition(Guid positionId)
        {
            IEnumerable<EmployeeViewModel> employees = Services.UserService.GetEmployeesForPosition(positionId);

            return Json(employees.Select(m => new { Id = m.Id, Name = m.Name + " " + m.Surname }).OrderBy(em => em.Name));
        }

        [HttpPost]
        public ActionResult GetAllEmployees(Guid positionId, Guid? rotationOwnerId)
        {
            IEnumerable<EmployeeViewModel> employeesList = Services.UserService.GetAllEmployees();

            var employees = rotationOwnerId.HasValue
                ? employeesList.Where(e => e.Id != rotationOwnerId).OrderBy(e => e.Name)
                : employeesList.OrderBy(e => e.Name);
            return Json(employees.Select(m => new { Id = m.Id, Name = m.Name + " " + m.Surname }));
        }

        #region Create Dialog

        [HttpGet]
        public ActionResult CreateEmployeeDialog(CreateEmployeeViewModel model)
        {
            ModelState.Clear();
            model = (CreateEmployeeViewModel)Services.UserService.PopulateEmployeeModel(model);

            return PartialView("~/Views/RotationManager/Employees/_CreateEmployeeFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateEmployeeDialog(CreateEmployeeViewModel model, HttpPostedFileBase profileImage)
        {
            if(ModelState.IsValid)
            {
                if(!Services.UserService.IsEmployeeExist(model.Email))
                {
                    if(profileImage != null)
                    {
                        model.ImageId = Services.MediaService.AddImage("avatar", profileImage);
                    }
                    Services.UserService.AddEmployee(model);
                    return Content("ok");
                }
                else
                {
                    ModelState.AddModelError("Email", "User with the same login, already exist.");
                }
            }

            model = (CreateEmployeeViewModel)Services.UserService.PopulateEmployeeModel(model);
            return PartialView("~/Views/RotationManager/Employees/_CreateEmployeeFormDialog.cshtml", model);
        }

        #endregion

        #region Edit Dialog

        [HttpGet]
        public ActionResult EditEmployeeDialog(Guid EmployeeId)
        {
            if(EmployeeId != Guid.Empty)
            {
                EmployeeViewModel model = Services.UserService.GetEmployeeById(EmployeeId);
                TempData["OldAvatarId"] = model.ImageId;
                return PartialView("~/Views/RotationManager/Employees/_EditEmployeeFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditEmployeeDialog(EmployeeViewModel model, HttpPostedFileBase profileImage)
        {
            if (ModelState.IsValid)
            {
                if (model.Id.HasValue && !Services.UserService.IsEmployeeExist(model.Email, model.Id.Value))
                {
                    if (profileImage != null)
                    {
                        model.ImageId = Services.MediaService.AddImage("avatar", profileImage);
                    }

                    if (Services.UserService.UpdateEmployee(model))
                    {
                        Guid? oldAvatarId = (Guid?)TempData["OldAvatarId"];
                        if (profileImage != null && oldAvatarId.HasValue)
                        {
                            Services.MediaService.RemoveImage(oldAvatarId.Value);
                        }

                        return Content("ok");
                    }
                    else if (model.ImageId.HasValue)
                    {
                        Services.MediaService.RemoveImage(model.ImageId.Value);
                    }
                }
                else
                {
                    ModelState.AddModelError("Email", "Employee with the same login already exist.");
                }

            }

            model = Services.UserService.PopulateEmployeeModel(model);
            return PartialView("~/Views/RotationManager/Employees/_EditEmployeeFormDialog.cshtml", model);
        }

        #endregion


        [HttpGet]
        [Authorize(Roles = iHandoverRoles.Relay.Administrator + "," + iHandoverRoles.Relay.iHandoverAdmin + "," + iHandoverRoles.Relay.ExecutiveUser)]
        public ActionResult ChangeEmployeePasswordDialog(Guid employee)
        {
            var model = new ChangeEmployeePasswordViewModel
            {
                EmployeeId = employee
            };

            return PartialView("~/Views/RotationManager/Employees/_ChangeEmployeePasswordFormDialog.cshtml", model);
        }

        [HttpPost]
        [Authorize(Roles = iHandoverRoles.Relay.Administrator + "," + iHandoverRoles.Relay.iHandoverAdmin + "," + iHandoverRoles.Relay.ExecutiveUser)]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeEmployeePasswordDialog(ChangeEmployeePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Services.UserService.ResetEmployeePassword(model.EmployeeId, model.NewPassword);
                    return Content("ok");
                }
                catch (Exception)
                {
                    return HttpNotFound();
                }
            }
            return PartialView("~/Views/RotationManager/Employees/_ChangeEmployeePasswordFormDialog.cshtml", model);
        }


    }
}