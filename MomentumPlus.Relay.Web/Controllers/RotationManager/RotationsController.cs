﻿using MomentumPlus.Relay.Constants;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using MvcPaging;
using System;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Core.Authorization;
using MomentumPlus.Relay.Web.Helpers;

namespace MomentumPlus.Relay.Web.Controllers
{
    [Authorize(Roles = iHandoverRoles.Relay.Administrator + "," + iHandoverRoles.Relay.iHandoverAdmin + "," + iHandoverRoles.Relay.ExecutiveUser)]
    public class RotationsController : BaseController
    {
        //IOC
        public RotationsController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        // GET: Rotations
        public ActionResult Index()
        {
            AppSession.Current.ExitRotationViewModeUrl = Request.RawUrl;

            return View("~/Views/RotationManager/Rotations/Index.cshtml");
        }

        [HttpPost]
        public ActionResult GetRotationsForPosition(int? page, Guid positionId)
        {

            int currentPageIndex = page.HasValue ? page.Value - 1 : 0;
            var rotations = Services.RotationService.GetRotationsForPosition(positionId)
                                .ToPagedList(currentPageIndex, NumericConstants.Paginator.PaginatorPageSize);
            return Json(new
            {
                Rotations = rotations.Select(m => new { Id = m.Id, Name = m.RotationOwner, userId = m.RotationOwnerId }),
                IsFirstPage = rotations.IsFirstPage,
                IsLastPage = rotations.IsLastPage,
                PageNumber = rotations.PageNumber,
                PageCount = rotations.PageCount
            });
        }

        #region Create Dialog

        [HttpGet]
        public ActionResult CreateRotationDialog(Guid? SiteId, Guid? PositionId)
        {
            ModelState.Clear();

            var model = Services.RotationService.PopulateEditRotationModel(new CreateRotationViewModel()
            {
                PositionId = PositionId,
                SiteId = SiteId
            });

            return PartialView("~/Views/RotationManager/Rotations/_CreateRotationFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateRotationDialog(CreateRotationViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.RotationService.CreateFirstRotation(model))
                {
                    return Content("ok");
                }
                else
                {
                    ModelState.AddModelError("RotationOwnerId", "This employee already have rotation.");
                }
            }

            model = Services.RotationService.PopulateEditRotationModel(model);

            return PartialView("~/Views/RotationManager/Rotations/_CreateRotationFormDialog.cshtml", model);
        }

        #endregion

        #region Edit Dialog


        [HttpGet]
        public ActionResult EditRotationDialog(Guid RotationId)
        {
            if (RotationId != Guid.Empty)
            {
                EditRotationViewModel model = Services.RotationService.GetRotation(RotationId);
                if (model != null)
                {

                    model.CanChangeLineManager = User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) ||
                                                 User.IsInRole(iHandoverRoles.Relay.Administrator) ||
                                                 User.IsInRole(iHandoverRoles.Relay.ExecutiveUser) ||
                                                 User.IsInRole(iHandoverRoles.Relay.HeadLineManager);

                    return PartialView("~/Views/RotationManager/Rotations/_EditRotationFormDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditRotationDialog(EditRotationViewModel model)
        {
            if (ModelState.IsValid)
            {
                if(Services.RotationService.UpdateRotation(model))
                {
                    return Content("ok");
                }

                return HttpNotFound();
            }

            model = Services.RotationService.PopulateEditRotationModel(model);

            return PartialView("~/Views/RotationManager/Rotations/_EditRotationFormDialog.cshtml", model);
        }

        #endregion
    }
}