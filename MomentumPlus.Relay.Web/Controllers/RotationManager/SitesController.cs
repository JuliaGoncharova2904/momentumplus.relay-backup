﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System.Web.Mvc;
using System.Linq;
using System;
using MomentumPlus.Relay.Constants;
using MvcPaging;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class SitesController : BaseController
    {
        //IOC
        public SitesController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpPost]
        public ActionResult GetAllSites(int? page)
        {
            int currentPageIndex = page.HasValue ? page.Value - 1 : 0;
            var sites = Services.SiteService.GetAllSites()
                                .ToPagedList(currentPageIndex, NumericConstants.Paginator.PaginatorPageSize);
            return Json(new
            {
                Sites = sites,
                IsFirstPage = sites.IsFirstPage,
                IsLastPage = sites.IsLastPage,
                PageNumber = sites.PageNumber,
                PageCount = sites.PageCount
            });
        }

        [HttpPost]
        public ActionResult GetSiteForPosition(Guid positionId)
        {
            return Json(Services.SiteService.GetSiteForPosition(positionId));
        }

        #region Create Dialog

        [HttpGet]
        public ActionResult CreateSiteDialog()
        {
            ViewBag.Title = "Create Site";
            return PartialView("~/Views/RotationManager/Sites/_SiteFormDialog.cshtml");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateSiteDialog(SiteViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.SiteService.AddSite(model))
                {
                    return Content("ok");
                }
                else
                {
                    ModelState.AddModelError("Name", "Site with the same Name, already exist.");
                }
            }

            return PartialView("~/Views/RotationManager/Sites/_SiteFormDialog.cshtml", model);
        }

        #endregion

        #region Edit Dialog

        [HttpGet]
        public ActionResult EditSiteDialog(Guid SiteId)
        {
            if (SiteId != Guid.Empty)
            {
                SiteViewModel model = Services.SiteService.GetSite(SiteId);
                ViewBag.Title = "Edit Site";
                if (model != null)
                {
                    return PartialView("~/Views/RotationManager/Sites/_SiteFormDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditSiteDialog(SiteViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.SiteService.UpdateSite(model))
                {
                    return Content("ok");
                }

                return HttpNotFound();
            }

            return PartialView("~/Views/RotationManager/Sites/_SiteFormDialog.cshtml", model);
        }

        #endregion
    }
}