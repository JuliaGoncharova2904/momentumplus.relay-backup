﻿using MomentumPlus.Relay.Constants;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using MvcPaging;
using System;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class TeamsController : BaseController
    {
        //IOC
        public TeamsController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }


        [HttpPost]
        public ActionResult GetAllTeams(int? page)
        {
            int currentPageIndex = page.HasValue ? page.Value - 1 : 0;
            var teams = Services.TeamService.GetAllTeams()
                                .ToPagedList(currentPageIndex, NumericConstants.Paginator.PaginatorPageSize);
            return Json(new
            {
                Teams = teams,
                IsFirstPage = teams.IsFirstPage,
                IsLastPage = teams.IsLastPage,
                PageNumber = teams.PageNumber,
                PageCount = teams.PageCount
            });
        }

        #region Create Dialog

        [HttpGet]
        public ActionResult CreateTeamDialog()
        {
            ViewBag.Title = "Create Team";

            return PartialView("~/Views/RotationManager/Teams/_TeamFormDialog.cshtml");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTeamDialog(TeamViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.TeamService.AddTeam(model))
                {
                    return Content("ok");
                }
                else
                {
                    ModelState.AddModelError("Name", "Team with the same Name, already exist.");
                }
            }

            return PartialView("~/Views/RotationManager/Teams/_TeamFormDialog.cshtml", model);

        }

        #endregion

        #region Edit Dialog

        [HttpGet]
        public ActionResult EditTeamDialog(Guid TeamId)
        {
            if(TeamId != Guid.Empty)
            {
                var model = Services.TeamService.GetTeam(TeamId);
                ViewBag.Title = "Edit Team";
                if (model != null)
                {
                    return PartialView("~/Views/RotationManager/Teams/_TeamFormDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTeamDialog(TeamViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.TeamService.UpdateTeam(model))
                {
                    return Content("ok");
                }

                return HttpNotFound();
            }

            return PartialView("~/Views/RotationManager/Teams/_TeamFormDialog.cshtml", model);
        }

        #endregion

    }
}