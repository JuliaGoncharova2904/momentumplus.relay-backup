﻿using System.Web.Mvc;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.Web.Helpers;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class TeamRotationsController : BaseController
    {
        //IOC
        public TeamRotationsController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork) { }
   
        public ActionResult Index(int? page, int? pageSize)
        {
            if(!this.IsViewMode)
                AppSession.Current.ExitRotationViewModeUrl = Request.RawUrl;

            MyTeamRotationViewModel model = Services.TeamRotationsService
                                                    .GetMyTeamRotations(this.SwingOwnerId,
                                                                        this.IsViewMode,
                                                                        page - 1 ?? 0,
                                                                        pageSize ?? 9);

            return View("~/Views/MyTeam/Index.cshtml", model);
        }
    }
}