﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class MediaController : BaseController
    {
        //IOC
        public MediaController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        public ActionResult GetFile(Guid Id)
        {
            if (Id == Guid.Empty)
                return HttpNotFound();

            FileViewModel file = Services.MediaService.GetFile(Id);

            if (file == null)
                return HttpNotFound();

            return File(file.BinaryData, file.ContentType);
        }
        [AllowAnonymous]
        public ActionResult GetImage(Guid imageId, int? width = null, int? height = null)
        {
            if (imageId == Guid.Empty)
                return HttpNotFound();

            FileViewModel img = Services.MediaService.GetImage(imageId, width, height);

            if (img == null)
                return HttpNotFound();

            return File(img.BinaryData, img.ContentType);
        }

        public ActionResult GetAvatarForEmployee(Guid employeeId, int? width, int? height)
        {
            if(employeeId != Guid.Empty)
            {
                FileViewModel file = Services.MediaService.GetEmployeeAvatarImage(employeeId, width, height);

                if (file == null)
                    return HttpNotFound();

                return File(file.BinaryData, file.ContentType);
            }

            return HttpNotFound();
        }

        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult GetMyAvatar(int? width, int? height)
        {
            var userId = Guid.Parse(User.Identity.GetUserId());

            FileViewModel file = Services.MediaService.GetEmployeeAvatarImage(userId, width, height);

            if (file == null)
                return HttpNotFound();

            return File(file.BinaryData, file.ContentType);
        }

        public ActionResult GetProjectAvatar(Guid projectId, int? width, int? height)
        {
            if (projectId != Guid.Empty)
            {
                FileViewModel file = Services.MediaService.GetProjectAvatarImage(projectId, width, height);

                if (file == null)
                    return HttpNotFound();

                return File(file.BinaryData, file.ContentType);
            }

            return HttpNotFound();
        }

    }
}