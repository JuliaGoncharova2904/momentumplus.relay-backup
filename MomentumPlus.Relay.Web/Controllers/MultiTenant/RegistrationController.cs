﻿using System.Web.Mvc;
using MomentumPlus.Relay.Interfaces;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class RegistrationController : BaseController
    {
        //IOC
        public RegistrationController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        /// <summary>
        /// Render Sign Up Page
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Summary");
            }

            return View("~/Views/Account/MultiTenant/Registration/Registration.cshtml");
        }

        ///// <summary>
        ///// Currency popover
        ///// </summary>
        //[HttpGet]
        //[AllowAnonymous]
        //[OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        //public ActionResult Currency()
        //{
        //    return PartialView("~/Views/Account/MultiTenant/Registration/_Currency.cshtml");
        //}


        /// <summary>
        /// Sign Up Step 1
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Step1()
        {
            return PartialView("~/Views/Account/MultiTenant/Registration/_Step_1.cshtml");
        }

        /// <summary>
        /// Sign Up Step 2
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Step2()
        {
            return PartialView("~/Views/Account/MultiTenant/Registration/_Step_2.cshtml");
        }


        /// <summary>
        /// Sign Up Step 3
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Step3()
        {
            return PartialView("~/Views/Account/MultiTenant/Registration/_Step_3.cshtml");
        }


        /// <summary>
        /// Sign Up Step 1
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Step4()
        {
            return PartialView("~/Views/Account/MultiTenant/Registration/_Step_4.cshtml");
        }

    }
}