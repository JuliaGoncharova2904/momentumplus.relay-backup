﻿using System;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.Interfaces;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Web.Controllers.Safety
{
    public class CriticalControlController : BaseController
    {
        //IOC
        public CriticalControlController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork){}

        #region Create Critical Control

        [HttpGet]
        public ActionResult CreateCriticalControl(Guid majorHazardId)
        {
            if (majorHazardId != Guid.Empty)
            {
                EditCriticalControlViewModel viewModel = Services.CriticalControlService.PopulateCriticalControlModel();
                viewModel.MajorHazardId = majorHazardId;

                ViewBag.Title = "New";
                return PartialView("~/Views/Safety/v1/CriticalControl/_EditCriticalControlForm.cshtml", viewModel);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCriticalControl(EditCriticalControlViewModel model)
        {
            if(ModelState.IsValid)
            {
                Services.CriticalControlService.AddCriticalControl(model);
                return Content("ok");
            }

            ViewBag.Title = "New";
            Services.CriticalControlService.PopulateCriticalControlModel(model);
            return PartialView("~/Views/Safety/v1/CriticalControl/_EditCriticalControlForm.cshtml", model);
        }

        #endregion

        #region Edit Critical Control

        [HttpGet]
        public ActionResult EditCriticalControl(Guid criticalControlId)
        {
            if (criticalControlId != Guid.Empty)
            {
                EditCriticalControlViewModel viewModel = Services.CriticalControlService.GetCriticalControl(criticalControlId);
                ViewBag.Title = "Edit";
                return PartialView("~/Views/Safety/v1/CriticalControl/_EditCriticalControlForm.cshtml", viewModel);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCriticalControl(EditCriticalControlViewModel model)
        {
            if (ModelState.IsValid)
            {
                Services.CriticalControlService.UpdateCriticalControl(model);
                return Content("ok");
            }

            ViewBag.Title = "Edit";
            Services.CriticalControlService.PopulateCriticalControlModel(model);
            return PartialView("~/Views/Safety/v1/CriticalControl/_EditCriticalControlForm.cshtml", model);
        }

        #endregion
    }
}