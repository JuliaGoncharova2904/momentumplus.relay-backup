﻿using System;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.Interfaces;
using System.Web.Mvc;
using System.Net;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Web.Controllers.Safety
{
    public class SafetyCriticalControlsV2Controller : BaseController
    {
        //IOC
        public SafetyCriticalControlsV2Controller(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork){}

        [HttpPost]
        public ActionResult GetCriticalControlsForMajorHazard(Guid majorHazardId)
        {
            if (majorHazardId != Guid.Empty)
            {
                IEnumerable<SelectListItem> criticalContrils = Services.CriticalControlV2Service.GetCriticalControlsList(majorHazardId);
                return Json(criticalContrils);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        public ActionResult GetCriticalControlInfo(Guid criticalControlId)
        {
            if (criticalControlId != Guid.Empty)
            {
                string ownerName = Services.CriticalControlV2Service.GetCriticalControlOwnerName(criticalControlId);
                IEnumerable<string> champions = Services.CriticalControlV2Service.GetCriticalControlChampions(criticalControlId);
                return Json(new { ownerName, champions });
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        #region Create Critical Control

        [HttpGet]
        public ActionResult CreateCriticalControl(Guid majorHazardId)
        {
            if (majorHazardId != Guid.Empty)
            {
                EditCriticalControlV2ViewModel viewModel = Services.CriticalControlV2Service
                                                                    .PopulateCriticalControlEditModel(majorHazardId);

                ViewBag.Title = "New";
                return PartialView("~/Views/Safety/v2/CriticalControl/_EditCriticalControlV2Form.cshtml", viewModel);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCriticalControl(EditCriticalControlV2ViewModel model)
        {
            if (ModelState.IsValid)
            {
                Services.CriticalControlV2Service.AddCriticalControl(model);
                return Content("ok");
            }

            ViewBag.Title = "New";
            Services.CriticalControlV2Service.PopulateCriticalControlEditModel(model);
            return PartialView("~/Views/Safety/v2/CriticalControl/_EditCriticalControlV2Form.cshtml", model);
        }

        #endregion

        #region Edit Critical Control

        [HttpGet]
        public ActionResult EditCriticalControl(Guid criticalControlId)
        {
            if (criticalControlId != Guid.Empty)
            {
                EditCriticalControlV2ViewModel viewModel = Services.CriticalControlV2Service.GetCriticalControlForEdit(criticalControlId);
                ViewBag.Title = "Edit";
                return PartialView("~/Views/Safety/v2/CriticalControl/_EditCriticalControlV2Form.cshtml", viewModel);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCriticalControl(EditCriticalControlV2ViewModel model)
        {
            if (ModelState.IsValid)
            {
                Services.CriticalControlV2Service.UpdateCriticalControl(model);
                return Content("ok");
            }

            ViewBag.Title = "Edit";
            Services.CriticalControlV2Service.PopulateCriticalControlEditModel(model);
            return PartialView("~/Views/Safety/v2/CriticalControl/_EditCriticalControlV2Form.cshtml", model);
        }

        #endregion


    }
}