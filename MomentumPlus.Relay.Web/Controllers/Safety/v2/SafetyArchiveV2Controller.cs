﻿using MomentumPlus.Relay.Interfaces;
using System.Web.Mvc;
using MomentumPlus.Relay.Constants;
using MvcPaging;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class SafetyArchiveV2Controller : BaseController
    {
        //IOC
        public SafetyArchiveV2Controller(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork) { }


        public ActionResult Index(int? page, int? pageSize)
        {
            var model = Services.SafetyStatV2Service.PopulateArchivedSafetyMessagesViewModel().ToPagedList(page - 1 ?? 0, pageSize ?? NumericConstants.Paginator.PaginatorPageSize);

            return View("~/Views/Safety/v2/Archive/Index.cshtml", model);
        }


    }
}