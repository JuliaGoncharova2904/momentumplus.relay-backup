﻿using System;
using MomentumPlus.Relay.Interfaces;
using System.Web.Mvc;
using MomentumPlus.Relay.Models;
using System.Collections.Generic;
using System.Net;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class SafetyMajorHazardsV2Controller : BaseController
    {
        public SafetyMajorHazardsV2Controller(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork) { }


        public ActionResult Index()
        {
            IEnumerable<MajorHazardV2ViewModel> majorHazardsViewModel = Services.MajorHazardV2Service.GetAllMajorHazards();
            return View("~/Views/Safety/v2/MajorHazards/Index.cshtml", majorHazardsViewModel);
        }

        [HttpPost]
        public ActionResult GetMajorHazardInfo(Guid majorHazardId)
        {
            if (majorHazardId == Guid.Empty)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            MajorHazardV2ViewModel model = Services.MajorHazardV2Service.GetMajorHazard(majorHazardId);
            return Json(model);
        }

        [HttpGet]
        public ActionResult EditMajorHazardDialog(Guid majorHazardId)
        {
            if (majorHazardId == Guid.Empty)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            EditMajorHazardV2ViewModel model = Services.MajorHazardV2Service.GetMajorHazardForEdit(majorHazardId);
            return PartialView("~/Views/Safety/v2/MajorHazards/_EditMajorHazardV2Dialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditMajorHazardDialog(EditMajorHazardV2ViewModel model)
        {
            if(model.Id == Guid.Empty)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Services.MajorHazardV2Service.UpdateMajorHazard(model);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

    }
}