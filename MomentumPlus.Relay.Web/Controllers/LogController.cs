﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using NLog;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class LogController : BaseController
    {
        private static readonly Logger Logger = LogManager.GetLogger("front-logger");

        //IOC
        public LogController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

    
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Info(string message)
        {
            if (User.Identity.IsAuthenticated)
            {
                Guid userId = Guid.Parse(User.Identity.GetUserId());


                Logger.Info("User id: {0} . Message - {1}", userId, message);
            }

            return new HttpStatusCodeResult(200);
        }


        [AllowAnonymous]
        [HttpPost]
        public ActionResult Error(string message)
        {
            if (User.Identity.IsAuthenticated)
            {
                Logger.Error("Error: {0}", message);
            }

            return new HttpStatusCodeResult(200);
        }

    }
}