﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ProfileController : BaseController
    {
        //IOC
        public ProfileController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpPost]
        public ActionResult GetEmployeeInfo()
        {
            EmployeeViewModel model = Services.UserService.GetEmployeeById(Guid.Parse(User.Identity.GetUserId()));
            return Json(new { Name = model.Name, Position = model.Position });
        }

        [HttpGet]
        public ActionResult PersonalDetailsTab()
        {
            PersonalDetailsViewModel model = Services.UserService.GetPersonDetails(Guid.Parse(User.Identity.GetUserId()));
            if (model != null)
            {
                return PartialView("~/Views/Settings/Profile/Tabs/_PersonalDetailsTab.cshtml", model);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PersonalDetailsTab(PersonalDetailsViewModel model, HttpPostedFileBase profileImage)
        {
            if (ModelState.IsValid)
            {
                if (Services.UserService.UpdatePersonDetails(Guid.Parse(User.Identity.GetUserId()), model, profileImage))
                {
                    return Content("ok");
                }
            }

            model.FillTitles();

            return PartialView("~/Views/Settings/Profile/Tabs/_PersonalDetailsTab.cshtml", model);
        }

        [HttpGet]
        public ActionResult PreferencesTab()
        {
            return PartialView("~/Views/Settings/Profile/Tabs/_PreferencesTab.cshtml");
        }


        [HttpGet]
        public ActionResult SecurityTab()
        {
            SecurityViewModel model = Services.UserService.GetSecurityDetails(Guid.Parse(User.Identity.GetUserId()));
            if (model != null)
            {
                return PartialView("~/Views/Settings/Profile/Tabs/_SecurityTab.cshtml", model);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SecurityTab(SecurityViewModel model)
        {
            if (ModelState.IsValid)
            {
                var currentUser = UserManager.FindById(Guid.Parse(User.Identity.GetUserId()));

                if ((!string.IsNullOrEmpty(model.Password) && !string.IsNullOrEmpty(model.NewPassword)) && !UserManager.ChangePassword(currentUser.Id,model.Password, model.NewPassword).Succeeded)
                {
                    ModelState.AddModelError("Password", "The user password provided is incorrect.");
                } else if (currentUser.UserName != model.RelayEmail)
                {
                    if (!Services.UserService.UpdateSecurityDetails(Guid.Parse(User.Identity.GetUserId()), model))
                    {
                        ModelState.AddModelError("User", "The user with the same name already exists.");
                    }
                    else
                    {   
                        //WebSecurity.Logout();
                        Session.Clear();

                        return Content("ok");
                    } 
                }
                else
                {
                    return Content("ok");
                }

            }

            model.Password = string.Empty;
            model.NewPassword = string.Empty;
            model.ConfirmNewPassword = string.Empty;

            return PartialView("~/Views/Settings/Profile/Tabs/_SecurityTab.cshtml", model);
        }

    }
}