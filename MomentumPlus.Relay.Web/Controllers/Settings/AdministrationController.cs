﻿using System;
using System.Web.Mvc;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System.Collections.Generic;
using System.Net;
using System.Web;
using MomentumPlus.Core.Authorization;

namespace MomentumPlus.Relay.Web.Controllers
{
    [Authorize(Roles = iHandoverRoles.Relay.Administrator + "," + iHandoverRoles.Relay.iHandoverAdmin + "," + iHandoverRoles.Relay.ExecutiveUser)]
    public class AdministrationController : BaseController
    {
        //IOC
        public AdministrationController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }


        [HttpGet]
        public ActionResult DetailsTab()
        {

            var model = Services.AdministrationService.PopulateAccountDetailsViewModel();

            return PartialView("~/Views/Settings/Administration/Tabs/_DetailsTab.cshtml", model);
        }

        [HttpGet]
        [Authorize(Roles = iHandoverRoles.Relay.iHandoverAdmin)]
        public ActionResult VersionDetailsTab()
        {
            var model = Services.AdministrationService.PopulateVersionDetailsViewModel();

            return PartialView("~/Views/Settings/Administration/Tabs/_VersionDetailsTab.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VersionDetailsTab(VersionDetailsViewModel model)
        {
            if (ModelState.IsValid)
            {
                Services.AdministrationService.UpdateVersionDetails(model);
                return Content("ok");
            }

            return PartialView("~/Views/Settings/Administration/Tabs/_VersionDetailsTab.cshtml", model);
        }


        [HttpGet]
        public ActionResult ThirdPartyTab()
        {
            var model = Services.AdministrationService.PopulateThirdPartySettingsViewModel();

            return PartialView("~/Views/Settings/Administration/Tabs/_ThirdPartyTab.cshtml", model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ThirdPartyTab(ThirdPartySettingsViewModel model)
        {
            if (ModelState.IsValid)
            {
                Services.AdministrationService.UpdateThirdPartySettings(model);
                return Content("ok");
            }

            return PartialView("~/Views/Settings/Administration/Tabs/_ThirdPartyTab.cshtml", model);
        }


        [HttpGet]
        public ActionResult PreferencesTab()
        {
            var model = Services.AdministrationService.PopulatePreferencesSettingsViewModel();

            return PartialView("~/Views/Settings/Administration/Tabs/_PreferencesTab.cshtml", model);
        }


        [HttpGet]
        public ActionResult UsersTab()
        {
            IEnumerable<UserDetailsViewModel> model = Services.UserService.GetUsersDetails();
            return PartialView("~/Views/Settings/Administration/Tabs/_UsersTab.cshtml", model);
        }

        [HttpPost]
        public ActionResult RemoveUser(Guid userId)
        {
            if (Services.UserService.RemoveUser(userId))
            {
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        public ActionResult UpgradeTab()
        {
            return PartialView("~/Views/Settings/Administration/Tabs/_UpgradeTab.cshtml");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PreferencesTab(PreferencesSettingsViewModel model, HttpPostedFileBase logoImage)
        {

            if (ModelState.IsValid)
            {
                Services.AdministrationService.UpdatePreferencesSettings(model, logoImage);
                return Content("ok");
            }

            return PartialView("~/Views/Settings/Administration/Tabs/_PreferencesTab.cshtml", model);
        }

    }
}