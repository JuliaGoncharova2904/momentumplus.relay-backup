﻿using System;
using System.Web.Mvc;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using MomentumPlus.Core.Authorization;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class SettingsController : BaseController
    {
        //IOC
        public SettingsController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Index()
        {
            //================= Init access rights ==================
            Guid userId = Guid.Parse(User.Identity.GetUserId());
            bool all = true;
            bool admin = UserManager.IsInRole(userId, iHandoverRoles.Relay.Administrator);
            bool ihandoverAdmin = UserManager.IsInRole(userId, iHandoverRoles.Relay.iHandoverAdmin);
            bool executiveUser = UserManager.IsInRole(userId, iHandoverRoles.Relay.ExecutiveUser);
            //----------------------------
            SettingsDialogAccessViewModel accessModel = new SettingsDialogAccessViewModel();
            //----------------------------
            accessModel.ProfileTab = all;
            accessModel.AdminTab = admin || ihandoverAdmin || executiveUser;
            //----------------------------
            accessModel.Profile.PresonalTab = all;
            accessModel.Profile.SecurityTab = all;
            accessModel.Profile.PreferencesTab = all;
            //----------------------------
            accessModel.Admin.AccountTab = admin || ihandoverAdmin || executiveUser;
            accessModel.Admin.VersionDetailsTab = ihandoverAdmin;
            accessModel.Admin.ThirdPartyTab = admin || ihandoverAdmin || executiveUser;
            accessModel.Admin.PreferencesTab = admin || ihandoverAdmin || executiveUser;
            accessModel.Admin.UsersTab = admin || ihandoverAdmin || executiveUser;
            accessModel.Admin.UpgradeTab = admin || ihandoverAdmin || executiveUser;
            //=======================================================
            return PartialView("~/Views/Settings/_SettingsDialog.cshtml", accessModel);
        }

    }
}