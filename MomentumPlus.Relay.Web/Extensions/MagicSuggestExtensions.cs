﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Web.Extensions
{
    public static class MagicSuggestExtensions
    {
        public static string GetValues(this IEnumerable<Guid?> source)
        {
            string valueString = string.Empty;

            if (source != null)
            {
                foreach (Guid id in source)
                {
                    valueString = $"{valueString} \"{id.ToString()}\",";
                }
            }

            return valueString;
        }
    }
}