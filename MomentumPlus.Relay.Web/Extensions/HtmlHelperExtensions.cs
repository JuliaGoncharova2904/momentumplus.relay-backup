﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Relay.Models;
using System.Web.Mvc.Ajax;
using System.Text;
using System.Web.Routing;
using Microsoft.Ajax.Utilities;
using MomentumPlus.Core.Authorization;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;

namespace MomentumPlus.Relay.Web.Extensions
{
    /// <summary>
    /// <c>HtmlHelper</c> extension methods for use in views across the application.
    /// </summary>
    public static class HtmlHelperExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public static MvcHtmlString ConvertTextToHtml(this HtmlHelper htmlHelper, string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return MvcHtmlString.Create(String.Empty);
            }

            bool continueList = false;
            string resultHtml = "";
            string[] lines = text.Split(new[] { "\n" }, StringSplitOptions.None);

            foreach (string line in lines)
            {
                string[] listElements = line.Split(new[] { "*" }, StringSplitOptions.None);

                if (listElements[0].Length > 0)
                {
                    if (continueList)
                    {
                        continueList = false;
                        resultHtml += "</ul>";
                    }

                    resultHtml = string.Format("{0}<div>{1}</div>", resultHtml, listElements[0]);
                }

                if (listElements.Length > 1)
                {
                    if (!continueList)
                    {
                        continueList = true;
                        resultHtml += "<ul>";
                    }

                    for (var i = 1; i < listElements.Length; ++i)
                    {
                        resultHtml = string.Format("{0}<li>{1}</li>", resultHtml, listElements[i]);
                    }
                }
            }

            return MvcHtmlString.Create(resultHtml);
        }

        /// <summary>
        /// Generate alias fo role
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        public static MvcHtmlString RoleAlias(this HtmlHelper htmlHelper, string role)
        {
            string alias = string.Empty;

            switch (role)
            {
                case iHandoverRoles.Relay.Administrator:
                    alias = "A";
                    break;
                case iHandoverRoles.Relay.LineManager:
                    alias = "LM";
                    break;
                case iHandoverRoles.Relay.SafetyManager:
                    alias = "SM";
                    break;
            }

            return MvcHtmlString.Create(alias);
        }

        /// <summary>
        /// Get description of enum
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="htmlHelper">The <c>HtmlHelper</c> object.</param>
        /// <param name="enumVal">Enum value</param>
        /// <param name="template">Template string</param>
        /// <returns>An HTML-encoded string.</returns>
        public static MvcHtmlString EnumDescription<TEnum>(this HtmlHelper htmlHelper, TEnum enumVal, string template = "{0}") where TEnum : struct, IConvertible
        {
            return new MvcHtmlString(string.Format(template, enumVal.GetEnumDescription<TEnum>()));
        }

        /// <summary>
        /// Create a title for the Topic Groups panel.
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="moduleType">Type of module</param>
        /// <returns>An HTML-encoded string containing the label.</returns>
        public static MvcHtmlString TopicGroupsPanelTitle(this HtmlHelper htmlHelper, ModuleType moduleType)
        {
            var tagBuilder = new TagBuilder("span");

            switch (moduleType)
            {
                case ModuleType.HSE:
                    tagBuilder.InnerHtml = "HSE Types";
                    break;
                case ModuleType.Core:
                    tagBuilder.InnerHtml = "Core Categories";
                    break;
                case ModuleType.Project:
                    tagBuilder.InnerHtml = "Projects";
                    break;
                case ModuleType.Team:
                    tagBuilder.InnerHtml = "Teams";
                    break;
                default:
                    tagBuilder.InnerHtml = "Topic Groups";
                    break;
            }

            return MvcHtmlString.Create(tagBuilder.ToString());
        }

        /// <summary>
        /// Create a title for the Topics panel.
        /// </summary>
        /// <param name="htmlHelper">The <c>HtmlHelper</c> object.</param>
        /// <param name="moduleType">Type of module</param>
        /// <returns>An HTML-encoded string containing the label.</returns>
        public static MvcHtmlString TopicsPanelTitle(this HtmlHelper htmlHelper, ModuleType moduleType)
        {
            var tagBuilder = new TagBuilder("span");

            switch (moduleType)
            {
                case ModuleType.HSE:
                    tagBuilder.InnerHtml = "References";
                    break;
                case ModuleType.Core:
                    tagBuilder.InnerHtml = "References";
                    break;
                case ModuleType.Project:
                    tagBuilder.InnerHtml = "Project Items";
                    break;
                case ModuleType.Team:
                    tagBuilder.InnerHtml = "Team Items";
                    break;
                default:
                    tagBuilder.InnerHtml = "Topics";
                    break;
            }

            return MvcHtmlString.Create(tagBuilder.ToString());
        }

        /// <summary>
        /// Create a Task Priority button.
        /// </summary>
        /// <param name="htmlHelper">The <c>HtmlHelper</c> object.</param>
        /// <param name="moduleType">Task priority</param>
        /// <returns>An HTML-encoded string containing the label.</returns>
        public static MvcHtmlString TaskPriorityBtn(this HtmlHelper htmlHelper, TaskPriority taskPriority)
        {
            TagBuilder tagBuilder = new TagBuilder("div");

            tagBuilder.InnerHtml = taskPriority.ToString();
            tagBuilder.AddCssClass("btn");
            tagBuilder.AddCssClass("btn-priority");

            switch (taskPriority)
            {
                case TaskPriority.Low:
                    tagBuilder.AddCssClass("btn-low");
                    break;
                case TaskPriority.Normal:
                    tagBuilder.AddCssClass("btn-success");
                    break;
                case TaskPriority.Important:
                    tagBuilder.AddCssClass("btn-info");
                    break;
                case TaskPriority.Critical:
                    tagBuilder.AddCssClass("btn-warning");
                    break;
                case TaskPriority.Urgent:
                    tagBuilder.AddCssClass("btn-danger");
                    break;
            }

            return MvcHtmlString.Create(tagBuilder.ToString());
        }

        public static MvcHtmlString RawActionLink(this AjaxHelper ajaxHelper, string linkText, string actionName, string controllerName, object routeValues, AjaxOptions ajaxOptions, object htmlAttributes)
        {
            var replaceId = Guid.NewGuid().ToString();
            var lnk = ajaxHelper.ActionLink(replaceId, actionName, controllerName, routeValues, ajaxOptions, htmlAttributes);
            return MvcHtmlString.Create(lnk.ToString().Replace(replaceId, linkText));
        }

        public static MvcHtmlString TooltipAttritutes(this HtmlHelper htmlHelper, string message, int showTime = 1000, bool isContainerBody = false)
        {
            //var str = "data-html=true data-trigger=\"hover focus\"  data-toggle=tooltip data-delay='{\"show\":\"" + showTime + "\", \"hide\":\"1000\"}' " + (isContainerBody ? "data-container=body" : "") + " data-original-title=" + message.Replace(" ", "&nbsp;") + " data-placement=top tabindex=-1";
            //var str = string.Empty;
            var str = "trigger=\"hover focus\" title=\"" + message + "\" side=\"top\" delay=\"" + showTime + "\" ";
            return MvcHtmlString.Create(str);
        }


        public static MvcHtmlString Popover(this HtmlHelper htmlHelper, string message)
        {
            var str = "data-content=\"<p>" + message + "</p>\"";
            return MvcHtmlString.Create(str);
        }

        public static MvcHtmlString PopoverFromList(this HtmlHelper htmlHelper, IEnumerable<DropDownListItemModel> items)
        {
            if (items.Any())
            {
                var content = new StringBuilder();

                items.ForEach(item =>
                {
                    content.Append("<div class='popover-item'>" + item.Text + "</div>");
                });


                var str = "data-content=\"<div class='popover-content'>" + content + "</div>\" data-placement=\"bottom-right\"";
                return MvcHtmlString.Create(str);
            }

            return MvcHtmlString.Create("data-content=\"\"");
        }


        public static string ActionLinkWithModel(this HtmlHelper helper, string action, string controller, object routeData)
        {
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);


            string href = urlHelper.Action(action, controller);

            if (routeData != null)
            {
                RouteValueDictionary rv = new RouteValueDictionary(routeData);
                List<string> urlParameters = new List<string>();
                foreach (var key in rv.Keys)
                {
                    object value = rv[key];
                    if (value is IEnumerable && !(value is string))
                    {
                        int i = 0;
                        foreach (object val in (IEnumerable)value)
                        {
                            urlParameters.Add(string.Format("{0}[{2}]={1}", key, val, i));
                            ++i;
                        }
                    }
                    else if (value != null)
                    {
                        urlParameters.Add(string.Format("{0}={1}", key, value));
                    }
                }
                string paramString = string.Join("&", urlParameters.ToArray()); // ToArray not needed in 4.0
                if (!string.IsNullOrEmpty(paramString))
                {
                    href += "?" + paramString;
                }
            }
            return href;
        }





        public static string ActionLinkWithList(this HtmlHelper helper, string text, string action, string controller, object routeData, object htmlAttributes)
        {
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);


            string href = urlHelper.Action(action, controller);

            if (routeData != null)
            {
                RouteValueDictionary rv = new RouteValueDictionary(routeData);
                List<string> urlParameters = new List<string>();
                foreach (var key in rv.Keys)
                {
                    object value = rv[key];
                    if (value is IEnumerable && !(value is string))
                    {
                        int i = 0;
                        foreach (object val in (IEnumerable)value)
                        {
                            urlParameters.Add(string.Format("{0}[{2}]={1}", key, val, i));
                            ++i;
                        }
                    }
                    else if (value != null)
                    {
                        urlParameters.Add(string.Format("{0}={1}", key, value));
                    }
                }
                string paramString = string.Join("&", urlParameters.ToArray()); // ToArray not needed in 4.0
                if (!string.IsNullOrEmpty(paramString))
                {
                    href += "?" + paramString;
                }
            }

            TagBuilder builder = new TagBuilder("a");
            builder.Attributes.Add("href", href);
            builder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            builder.SetInnerText(text);
            return builder.ToString(TagRenderMode.Normal);
        }
    }
}