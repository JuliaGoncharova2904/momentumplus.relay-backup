﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace MomentumPlus.Relay.Web.Extensions
{
    public static class DateTimeExtensions
    {
        public static string CalendarFormat(this DateTime dt)
        {
            return string.Format("{0:dddd} {1} {0:MMMM}", dt, dt.Day.ToString("00"));
        }

        public static string CalendarFormat(this DateTime? dt)
        {
            if (dt.HasValue)
                return string.Format("{0:dddd} {1} {0:MMMM}", dt, dt.Value.Day.ToString("00"));

            return string.Empty;
        }

        public static string CalendarFormatWidthYear(this DateTime? dt)
        {
            if (dt.HasValue)
                return string.Format("{0:dd MMM yyyy}", dt);

            return string.Empty;
        }

        public static string CalendarFormatWidthYear(this DateTime dt)
        {
            return string.Format("{0:dd MMM yyyy}", dt);
        }

        public static string ToISOString(this DateTime dt, bool utc = false)
        {
            if (utc)
            {
                return dt.ToString("yyyy-MM-ddTHH\\:mm\\:ss.fffffff+00:00");
            }

            return dt.ToString("yyyy-MM-ddTHH\\:mm\\:ss.fffffffzzz");
        }

        public static string ToISOString(this DateTime? dt, bool utc = false)
        {
            return dt.HasValue ? dt.Value.ToISOString(utc) : string.Empty;
        }
    }
}