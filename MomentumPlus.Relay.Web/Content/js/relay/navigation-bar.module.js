﻿$(document).ready(function () {
    "use strict";
    //================== Navigation Bar module ===============================
    var navigationBarDiv = $("#tabs");
    //-----------------------------------------
    var navigationBar = {
        disable: NavigationBar_DisableAllEvents,
        enable: NavigationBar_EnableAllEvents
    };
    //-----------------------------------------
    function NavigationBar_DisableAllEvents() {
        navigationBarDiv.attr("style", "pointer-events:none;");
    }
    function NavigationBar_EnableAllEvents() {
        navigationBarDiv.removeAttr("style");
    }
    //========================================================================
    window.Relay = window.Relay || {};
    window.Relay.NavigationBar = navigationBar;
    //========================================================================
});