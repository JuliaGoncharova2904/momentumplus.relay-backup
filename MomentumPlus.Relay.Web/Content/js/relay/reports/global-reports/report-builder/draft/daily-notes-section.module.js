﻿; (function (window) {
    //===============================================================================================
    //  sectionElement: (section div)
    //  sourceId: (shift or rotation id)
    //  sourceType: (source type: Shift or Swing)
    //  readOnly: (readonly mode flag)
    //==============================  Draft Daily Notes Section =====================================
    function GlobalDraftDailyNotesSection(sectionElement, sourceId, sourceType, readOnly) {
        //----------------------
        if (sourceType === "Shift")
            console.error("DailyNotesSection is not exist for shift!");
        //----------------------
        this.type = Relay.SectionFactory.Types.DailyNote;
        var controller = "/DailyNotesSection";
        this.sectionUrl = controller + "/Draft";
        this.updateTopicUrl = controller + "/UpdateTopic";
        this.reloadTopicUrl = controller + "/ReloadTopic";
        //----------------------
        this.topics;
        this.isExpanded = false;
        this.rotationId = sourceId;
        this.sectionElement = sectionElement;
        //----------------------
        GlobalDraftDailyNotesSection_Initialise.call(this);
    }
    GlobalDraftDailyNotesSection.prototype.reload = function () {
        var _this = this;

        $.ajax({
            url: this.sectionUrl,
            method: "GET",
            data: {
                rotationId: this.rotationId
            }
        })
        .done(function (newContent) {
            //--- update content -----
            _this.sectionElement.empty();
            $(newContent).children().each(function () {
                _this.sectionElement.append(this);
            });
            //---- Init section ------
            GlobalDraftDailyNotesSection_Initialise.call(_this)
            //------------------------
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    GlobalDraftDailyNotesSection.prototype.expand = function () {
        this.isExpanded = true;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].autoCollapseOff();
            this.topics[i].expand();
        }
    };
    GlobalDraftDailyNotesSection.prototype.collapse = function () {
        this.isExpanded = false;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].collapse();
            this.topics[i].autoCollapseOn();
        }
    };
    GlobalDraftDailyNotesSection.prototype.disableTopics = function () {
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].disable();
        }
    }
    GlobalDraftDailyNotesSection.prototype.enableTopics = function () {
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].enable();
        }
    }
    function GlobalDraftDailyNotesSection_Initialise() {
        var _this = this;
        this.topics = [];
        //-------
        $("tbody tr", this.sectionElement).each(function () {
            var topic = GlobalDraftDailyNotesSection_BuildTopic.call(_this, this);

            if (_this.isExpanded) {
                topic.autoCollapseOff();
                topic.expand();
            }

            _this.topics.push(topic);
        })
    }
    function GlobalDraftDailyNotesSection_BuildTopic(trElement) {
        var _this = this;

        var topic = Relay.Topic(trElement, {
            updateUrl: this.updateTopicUrl,
            reloadUrl: this.reloadTopicUrl
        });

        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Date",
            elementClass: ".second-td"
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NotesField({
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.FinaliseBtn({
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.ManagerCommentsBtn({
            readOnly: this.readOnly,
            forTopic: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtn());

        topic.AddControl(Relay.Topic.ControlsFactory.TasksBtn({
            dialogType: this.readOnly ? Relay.TasksListDialog.Types.ReadOnly : Relay.TasksListDialog.Types.Draft,
            changeHandler: GlobalDraftDailyNotesSection_TasksChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
            dialogType: this.readOnly ? Relay.AttachmentsDialog.Types.Received : Relay.AttachmentsDialog.Types.Draft,
            container: Relay.AttachmentsDialog.Containers.Topic
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
            dialogType: Relay.VoiceMessagesDialog.Types.Draft,
            container: Relay.VoiceMessagesDialog.Containers.Topic
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.ShareBtn({
            readOnly: this.readOnly
        }));

        topic.updateView();

        return topic;
    }
    function GlobalDraftDailyNotesSection_TasksChangeHandler(model) {
        if (this.reloadExternalSection)
            this.reloadExternalSection(Relay.SectionFactory.Types.Tasks);
    }
    //===============================================================================================
    if (window.Relay.SectionFactory) {
        window.Relay.SectionFactory.GlobalDraftDailyNotesSection = function (sectionElement, sourceId, sourceType, readOnly) {
            return new GlobalDraftDailyNotesSection(sectionElement, sourceId, sourceType, readOnly);
        };
    }
    else {
        console.error("Draft Daily Notes Section cannot work without SectionFactory.");
    }
    //===============================================================================================
})(window);