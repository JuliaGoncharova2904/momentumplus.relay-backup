﻿; (function (window) {
    //===============================================================================================
    //  sectionElement: (section div)
    //  sourceId: (shift or rotation id)
    //  sourceType: (source type: Shift or Swing)
    //  readOnly: (readonly mode flag)
    //==============================  Draft Projects Section ========================================
    //------------- Reference Column click handler ------------------------
    function ReferenceColumnClickHandler(model, reload, update) {
        if (!model.IsNullReport) {
            var editProjectTopicDialog = Relay.FormDialog({
                url: (this.sourceType === "Swing" ? "/ProjectsSection" : "/ShiftProjectsSection") + "/EditTopicDialog",
                data: {
                    topic: model.Id
                }
            });
            editProjectTopicDialog.setSubmitEvent(reload);
            editProjectTopicDialog.open();
        }
    }
    //---------------------------------------------------------------------
    function GlobalDraftProjectsSection(sectionElement, sourceId, sourceType, readOnly) {
        //----------------------
        this.type = Relay.SectionFactory.Types.Project;
        var controller = sourceType === "Swing" ? "/ProjectsSection" : "/ShiftProjectsSection";
        this.sectionUrl = controller + "/Draft";
        this.updateTopicUrl = controller + "/UpdateTopic";
        this.reloadTopicUrl = controller + "/ReloadTopic";
        this.addInlineTopicUrl = controller + "/AddInlineTopic";
        //----------------------
        this.topics;
        this.isExpanded = false;
        this.sourceId = sourceId;
        this.sourceType = sourceType;
        this.readOnly = readOnly;
        this.sectionElement = sectionElement;
        this.reloadExternalSection = null;
        this.openInlineTopic = null;
        this.closeInlineTopic = null;
        //----------------------
        GlobalDraftProjectsSection_Initialise.call(this);
    }
    GlobalDraftProjectsSection.prototype.reload = function () {
        var _this = this;
        var isSwingReport = this.sourceType === "Swing";

        $.ajax({
            url: this.sectionUrl,
            method: "GET",
            data: {
                shiftId: isSwingReport ? undefined : this.sourceId,
                rotationId: isSwingReport ? this.sourceId : undefined,
            }
        })
        .done(function (newContent) {
            //--- update content -----
            _this.sectionElement.html($(newContent).children());
            //---- Init section ------
            GlobalDraftProjectsSection_Initialise.call(_this)
            //------------------------
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    GlobalDraftProjectsSection.prototype.expand = function () {
        this.isExpanded = true;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].autoCollapseOff();
            this.topics[i].expand();
        }
    };
    GlobalDraftProjectsSection.prototype.collapse = function () {
        this.isExpanded = false;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].collapse();
            this.topics[i].autoCollapseOn();
        }
    };
    GlobalDraftProjectsSection.prototype.disableTopics = function () {
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].disable();
        }
    }
    GlobalDraftProjectsSection.prototype.enableTopics = function () {
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].enable();
        }
    }
    function GlobalDraftProjectsSection_Initialise() {
        var _this = this;
        this.topics = [];
        //-------
        if (!$("tbody tr.no-items-message", this.sectionElement).length) {
            $("tbody tr", this.sectionElement).each(function () {
                var topic = GlobalDraftProjectsSection_BuildTopic.call(_this, this);

                if (_this.isExpanded) {
                    topic.autoCollapseOff();
                    topic.expand();
                }

                _this.topics.push(topic);
            });
        }
        //-------
        if (this.readOnly) {
            $(".add-item-btn", this.sectionElement).parent().hide();

            if ($(".no-items-message", this.sectionElement).length) {
                $(".table", this.sectionElement).hide();
                $(".no-items-message", this.sectionElement).show();
            }
        }
        else {
            var addInlineTopic = Relay.AddInlineTopic(this.sectionElement, {
                url: this.addInlineTopicUrl,
                destId: this.sourceId,
                topicName: this.type,
                warningMessage: "Please choose a Project and enter a Reference. Exit?"
            });
            addInlineTopic.setSaveEvent(this.reload.bind(this));
            addInlineTopic.setOpenEvent(function () {
                if (_this.openInlineTopic)
                    _this.openInlineTopic();
            });
            addInlineTopic.setCloseEvent(function () {
                if (_this.closeInlineTopic)
                    _this.closeInlineTopic();
            });
        }
    }
    function GlobalDraftProjectsSection_BuildTopic(trElement) {

        var topic = Relay.Topic(trElement, {
            updateUrl: this.updateTopicUrl,
            reloadUrl: this.reloadTopicUrl
        });

        topic.AddControl(Relay.Topic.ControlsFactory.RemoveTopicBtn({
            readOnly: this.readOnly,
            type: Relay.Topic.ControlsFactory.RemoveTopicBtn.Type.topic,
            changeHandler: GlobalDraftProjectsSection_RmoveTopicHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NullReportBtn({
            readOnly: this.readOnly,
            changeHandler: GlobalDraftProjectsSection_TasksAndActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TransferredBtn({
            readOnly: this.readOnly,
            changeHandler: GlobalDraftProjectsSection_TasksAndActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.PinnedBtn({
            readOnly: this.readOnly,
            changeHandler: GlobalDraftProjectsSection_TasksAndActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Project",
            elementClass: this.sourceType === "Swing" ? ".second-td" : ".type-body"
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Reference",
            elementClass: this.sourceType === "Swing" ? ".third-td" : ".reference-body",
            clickHandler: this.readOnly ? null : ReferenceColumnClickHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NotesField({
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.FinaliseBtn({
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.ManagerCommentsBtn({
            readOnly: this.readOnly,
            forTopic: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.LocationBtn({
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TasksBtn({
            dialogType: this.readOnly ? Relay.TasksListDialog.Types.ReadOnly : Relay.TasksListDialog.Types.Draft,
            changeHandler: GlobalDraftProjectsSection_TasksAndActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
            dialogType: this.readOnly ? Relay.AttachmentsDialog.Types.Received : Relay.AttachmentsDialog.Types.Draft,
            container: Relay.AttachmentsDialog.Containers.Topic
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
            dialogType: Relay.VoiceMessagesDialog.Types.Draft,
            container: Relay.VoiceMessagesDialog.Containers.Topic
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.ShareBtn({
            readOnly: this.readOnly
        }));

        topic.updateView();

        return topic;
    }
    function GlobalDraftProjectsSection_TasksAndActionsChangeHandler(model) {
        if (this.reloadExternalSection)
            this.reloadExternalSection(Relay.SectionFactory.Types.Tasks);
    }
    function GlobalDraftProjectsSection_RmoveTopicHandler(model) {
        this.reload();
        if (this.reloadExternalSection)
            this.reloadExternalSection(Relay.SectionFactory.Types.Tasks);
    }
    //===============================================================================================
    if (window.Relay.SectionFactory) {
        window.Relay.SectionFactory.GlobalDraftProjectsSection = function (sectionElement, sourceId, sourceType, readOnly) {
            return new GlobalDraftProjectsSection(sectionElement, sourceId, sourceType, readOnly);
        };
    }
    else {
        console.error("Draft Projects Section cannot work without SectionFactory.");
    }
    //===============================================================================================
})(window);