﻿; (function (window) {
    //===============================================================================================
    //  sectionElement: (section div)
    //  sourceId: (shift or rotation id)
    //  sourceType: (source type: Shift or Swing)
    //  readOnly: (readonly mode flag)
    //==============================  Received Core Section =========================================
    function GlobalReceivedCoreSection(sectionElement, sourceId, sourceType, readOnly) {
        //----------------------
        this.type = Relay.SectionFactory.Types.Core;
        var controller = sourceType === "Swing" ? "/CoreSection" : "/ShiftCoreSection";
        this.sectionUrl = controller + "/Received";
        this.updateTopicUrl = controller + "/UpdateTopic";
        this.reloadTopicUrl = controller + "/ReloadTopic";
        //----------------------
        this.topics;
        this.isExpanded = false;
        this.sourceId = sourceId;
        this.sourceType = sourceType;
        this.readOnly = readOnly;
        this.sectionElement = sectionElement;
        this.reloadExternalSection = null;
        //----------------------
        GlobalReceivedCoreSection_Initialise.call(this);
    }
    GlobalReceivedCoreSection.prototype.reload = function () {
        var _this = this;
        var isSwingReport = this.sourceType === "Swing";

        $.ajax({
            url: this.sectionUrl,
            method: "GET",
            data: {
                shiftId: isSwingReport ? undefined : this.sourceId,
                rotationId: isSwingReport ? this.sourceId : undefined,
            }
        })
        .done(function (newContent) {
            //--- update content -----
            _this.sectionElement.html($(newContent).children());
            //---- Init section ------
            GlobalReceivedCoreSection_Initialise.call(_this)
            //------------------------
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    GlobalReceivedCoreSection.prototype.expand = function () {
        this.isExpanded = true;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].autoCollapseOff();
            this.topics[i].expand();
        }
    };
    GlobalReceivedCoreSection.prototype.collapse = function () {
        this.isExpanded = false;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].collapse();
            this.topics[i].autoCollapseOn();
        }
    };
    function GlobalReceivedCoreSection_Initialise() {
        var _this = this;
        this.topics = [];
        //-------
        $("tbody tr", this.sectionElement).each(function () {
            var topic = GlobalReceivedCoreSection_BuildTopic.call(_this, this);

            if (_this.isExpanded) {
                topic.autoCollapseOff();
                topic.expand();
            }

            _this.topics.push(topic);
        })
    }
    function GlobalReceivedCoreSection_BuildTopic(trElement) {

        var topic = Relay.Topic(trElement, {
            updateUrl: this.updateTopicUrl,
            reloadUrl: this.reloadTopicUrl
        });

        topic.AddControl(Relay.Topic.ControlsFactory.NullReportBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TransferredBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Category",
            elementClass: this.sourceType === "Swing" ? ".second-td" : ".type-body"
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Reference",
            elementClass: this.sourceType === "Swing" ? ".third-td" : ".reference-body",
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NotesField({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.FinaliseBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.ManagerCommentsBtn({
            readOnly: true,
            forTopic: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TeamBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.LocationBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TasksBtn({
            dialogType: this.readOnly ? Relay.TasksListDialog.Types.ReadOnly : Relay.TasksListDialog.Types.Received
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
            dialogType: Relay.AttachmentsDialog.Types.Received,
            container: Relay.AttachmentsDialog.Containers.Topic
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
            dialogType: Relay.VoiceMessagesDialog.Types.Received,
            container: Relay.VoiceMessagesDialog.Containers.Topic
        }));

        topic.updateView();

        return topic;
    }
    //===============================================================================================
    if (window.Relay.SectionFactory) {
        window.Relay.SectionFactory.GlobalReceivedCoreSection = function (sectionElement, sourceId, sourceType, readOnly) {
            return new GlobalReceivedCoreSection(sectionElement, sourceId, sourceType, readOnly);
        };
    }
    else {
        console.error("Received Core Section cannot work without SectionFactory.");
    }
    //===============================================================================================
})(window);