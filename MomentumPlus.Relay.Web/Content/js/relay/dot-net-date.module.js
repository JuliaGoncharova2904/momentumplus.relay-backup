﻿; (function (window) {
    "use strict";
    //=================================================================================
    function dateStringToDate(dateString) {

        if (!dateString)
            return null;

        var dateAndTimeStrings = dateString.split(' ');
        var dateParams = dateAndTimeStrings[0].split('/');
        var timeParams = dateAndTimeStrings[1].split(':');

        var jsDate = new Date(dateParams[2] + '-' + dateParams[1] + '-' + dateParams[0]);

        jsDate.setHours(timeParams[0]);
        jsDate.setMinutes(timeParams[1]);
        jsDate.setSeconds(timeParams[2]);

        return jsDate;
    }

    function dateToDateString(date) {

        if (!date)
            return '';

        return  formatDateNumber(date.getDate()) +
                '/'
                + formatDateNumber(date.getMonth() + 1) +
                '/' + date.getFullYear() +
                ' ' + formatDateNumber(date.getHours()) +
                ':' + formatDateNumber(date.getMinutes()) +
                ':' + formatDateNumber(date.getSeconds());
    }

    function formatDateNumber(dateNumber) {
        if (dateNumber < 10) {
            return "0" + dateNumber;
        }

        return dateNumber.toString();
    }
    //=================================================================================
    window.Relay = window.Relay || {};
    window.Relay.DotNetDate = {
        dateStringToDate: dateStringToDate,
        dateToDateString: dateToDateString
    };
    //=================================================================================
})(window);