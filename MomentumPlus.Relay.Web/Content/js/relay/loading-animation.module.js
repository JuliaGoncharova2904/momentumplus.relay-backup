﻿; (function (window) {
    //========================================================================
    window.Relay = window.Relay || {};
    window.Relay.LoadingAnimation = function (ajaxPromise, delayMs) {

        var loadingAnimationTimeout = setTimeout(function () {
            $('#loading-animation').show();
        }, delayMs || 200);

        ajaxPromise.always(function () {
            clearTimeout(loadingAnimationTimeout);
            $('#loading-animation').hide();
        });
    };
    //========================================================================
})(window);