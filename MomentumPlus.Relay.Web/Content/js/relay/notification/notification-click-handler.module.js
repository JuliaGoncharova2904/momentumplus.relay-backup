﻿; (function () {
    "use strict";
    //================== Notification click handler module =====================
    function AddNotificationClickHandlers(msgContainer) {
        $(".edit-shift-rotation", msgContainer).unbind("click").bind("click", OpenEditShiftRotationDatesDialog);
        $(".load-checklist", msgContainer).unbind("click").bind("click", LoadChecklistPage);
        $(".load-report-builder", msgContainer).unbind("click").bind("click", LoadReportBuilderPage);
        $(".load-report-builder-view-mode", msgContainer).unbind("click").bind("click", LoadReportBuilderPageViewMode);
        $(".edit-task", msgContainer).unbind("click").bind("click", OpenEditTaskFormDialog);
        $(".complete-task", msgContainer).unbind("click").bind("click", OpenCompleteTaskFormDialog);
        $(".view-task", msgContainer).unbind("click").bind("click", OpenTaskDetailsFormDialog);
    }

    function MakeNotificationOpened(notifyElement) {
        $.ajax({
            url: "/Notification/MakeNotificationOpened",
            method: "POST",
            data: { notificationId: $(notifyElement).attr("notification-id") }
        })
        .done(function () {
            $(notifyElement).removeClass("unread")
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        });
    }

    function getRelationId(element) {
        var relationId = $(element).attr("relation-id");

        return (relationId === "00000000-0000-0000-0000-000000000000") ? null : relationId;
    }

    function isNotExistDialog() {
        Relay.InfoDialog("This notification link is no longer available.").open();
    }

    //--------------------------------------------------------------------------

    function OpenEditShiftRotationDatesDialog() {
        var relationId = getRelationId(this);

        $(this).hasClass("unread") && MakeNotificationOpened(this);

        if (relationId) {
            var editDialog = Relay.EditSwingShiftDialog({ userId: relationId });
            editDialog.setCloseEvent(function () { location.reload(); });
            editDialog.open();
        }
        else {
            isNotExistDialog();
        }
    }

    function LoadChecklistPage() {
        $('#loading-animation').show();
        $(this).hasClass("unread") && MakeNotificationOpened(this);
        location.href = "/TaskBoard";
    }

    function LoadReportBuilderPage() {
        $('#loading-animation').show();
        $(this).hasClass("unread") && MakeNotificationOpened(this);
        location.href = "/Summary";
    }

    function LoadReportBuilderPageViewMode() {
        var relationId = getRelationId(this);

        $(this).hasClass("unread") && MakeNotificationOpened(this);

        if (relationId) {
            $('#loading-animation').show();
            location.href = "/Rotation/" + relationId + "/Summary"
        }
        else {
            isNotExistDialog();
        }
    }

    function OpenEditTaskFormDialog() {
        var relationId = getRelationId(this);

        $(this).hasClass("unread") && MakeNotificationOpened(this);

        if(relationId){
            var tasksListDialog = Relay.FormDialog({
                url: "/Notification/EditTaskDialog",
                data: { TaskId: relationId }
            });
            tasksListDialog.setSubmitEvent(function () {
                location.reload();
            });
            tasksListDialog.open();
        }
        else {
            isNotExistDialog();
        }
    }

    function OpenCompleteTaskFormDialog() {
        var relationId = getRelationId(this);

        $(this).hasClass("unread") && MakeNotificationOpened(this);

        if(relationId){
            var tasksListDialog = Relay.FormDialog({
                url: "/Notification/CompleteTaskDialog",
                data: { TaskId: relationId }
            });
            tasksListDialog.setSubmitEvent(function () {
                location.reload();
            });
            tasksListDialog.open();
        }
        else {
            isNotExistDialog();
        }
    }

    function OpenTaskDetailsFormDialog() {
        var relationId = getRelationId(this);

        $(this).hasClass("unread") && MakeNotificationOpened(this);

        if (relationId) {
            var tasksListDialog = Relay.FormDialog({
                url: "/Notification/TaskDetailsDialog",
                data: { TaskId: relationId }
            });
            tasksListDialog.setSubmitEvent(function () {
                location.reload();
            });
            tasksListDialog.open();
        }
        else {
            isNotExistDialog();
        }
    }
    //==========================================================================
    window.Relay = window.Relay || {};
    window.Relay.AddNotificationClickHandlers = AddNotificationClickHandlers;
    //==========================================================================
})();