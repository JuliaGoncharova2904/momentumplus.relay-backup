﻿; (function () {
    var dialog = document.getElementById("editSafetyMessageV2Dialog");
    var majorHazardSelect = document.getElementById("MajorHazardId");
    var criticalControlSelect = document.getElementById("CriticalControlId");

    majorHazardSelect.addEventListener("change", loadCriticalContrlols);
    criticalControlSelect.addEventListener("change", loadCriticalControlInfo);

    setTimeout(initChampionsList, 150);
    //-------------
    function loadCriticalContrlols() {
        var majorHazardId = majorHazardSelect.options[majorHazardSelect.selectedIndex].value;

        if (majorHazardId) {
            $.ajax({
                url: "/SafetyCriticalControlsV2/GetCriticalControlsForMajorHazard",
                method: "POST",
                data: { majorHazardId: majorHazardId }
            })
            .done(function (data) {
                if (data) {
                    updateCriticalControls(data);
                    clearCriticalControlInfo();
                }
            })
            .fail(function () {
                var errorDialog = Relay.ErrorDialog();
                errorDialog.setCloseEvent(function () {
                    location.reload();
                });
                errorDialog.open();
            });
        }
        else {
            updateCriticalControls([]);
            clearCriticalControlInfo();
        }
    }
    //-------------
    function updateCriticalControls(items) {

        while (criticalControlSelect.firstChild) {
            criticalControlSelect.removeChild(criticalControlSelect.firstChild);
        }

        var defOption = document.createElement("option");
        defOption.textContent = "Select Critical Control";
        defOption.setAttribute("value", "");
        criticalControlSelect.appendChild(defOption);

        for (var i = 0; i < items.length; ++i) {
            var option = document.createElement("option");
            option.setAttribute("value", items[i].Value);
            option.textContent = items[i].Text;
            criticalControlSelect.appendChild(option);
        }

        $(criticalControlSelect).selectpicker('refresh');
    }
    //-------------
    function loadCriticalControlInfo() {
        var criticalControlId = criticalControlSelect.options[criticalControlSelect.selectedIndex].value;

        if (criticalControlId) {
            $.ajax({
                url: "/SafetyCriticalControlsV2/GetCriticalControlInfo",
                method: "POST",
                data: { criticalControlId: criticalControlId }
            })
            .done(function (data) {
                updateCriticalControlOwnerName(data.ownerName);
                updateCritecalControlChampions(data.champions)
            })
            .fail(function () {
                var errorDialog = Relay.ErrorDialog();
                errorDialog.setCloseEvent(function () {
                    location.reload();
                });
                errorDialog.open();
            });
        }
        else {
            clearCriticalControlInfo();
        }
    }
    //-------------
    function updateCriticalControlOwnerName(ownerName) {
        $(".critical-control-owner-name, .empty-critical-control-owner-name", dialog).remove();

        $(".critical-control-owner", dialog).append($("<div class='critical-control-owner-name'>" + ownerName + "</div>"));
    }

    function updateCritecalControlChampions(champions) {
        $(".critical-control-champion, .empty-critical-control-champions", dialog).remove();

        for (var i = 0; i < champions.length; ++i) {
            $(".critical-control-champion-list", dialog).append($("<div class='critical-control-champion'>" + champions[i] + "</div>"));
        }

        initChampionsList();
    }

    function clearCriticalControlInfo() {
        $(".critical-control-owner-name, .empty-critical-control-owner-name", dialog).remove();
        $(".critical-control-champion, .empty-critical-control-champions", dialog).remove();

        $(".critical-control-owner", dialog).append($("<div class='empty-critical-control-owner-name'>Critical Control Owner</div>"));
        $(".critical-control-champion-list", dialog).append($("<div class='empty-critical-control-champions'>Critical Control Champion(s)</div>"));
    }
    //-------------
    function initChampionsList() {
        var removedElements = 0;
        var elementsHeight = 0;

        $(".critical-control-champion", dialog).each(function (index, element) {
            var elementHeight = $(element).width() + 10;

            if (elementsHeight + elementHeight < 260) {
                elementsHeight += elementHeight;
            }
            else {
                $(element).remove();
                removedElements++;
            }
        });

        if (removedElements) {
            var numberDiv = document.createElement("div");
            numberDiv.className = "critical-control-champion";
            numberDiv.textContent = '+' + removedElements.toString();

            $(".critical-control-champion-list", dialog)[0].appendChild(numberDiv);
        }
    }
    //-------------
})();