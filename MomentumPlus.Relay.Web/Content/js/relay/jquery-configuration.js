﻿; (function ()
{
    //=================================================================
    $.ajaxSetup({ cache: false });
    $.ajaxSettings.traditional = true;

    $(document).on('hidden.bs.modal', '.modal', function ()
    {
        $('.modal:visible').length && $(document.body).addClass('modal-open');
    });

    $(document).ajaxError(function (e, xhr)
    {
        e.stopPropagation();

        if (xhr.status === 401)
        {
            xhr.abort();
            window.location = "/Account/Login";
        }
        else if (xhr.status === 403)
        {
            xhr.abort();
            Relay.InfoDialog("You have no enough permissions to request this resource.").open();
        }

        var msg = $.parseJSON(xhr.responseText);

        $.ajax({
            url: location.protocol + "//" + location.host + "/Log/Error",
            type: 'post',
            method: "POST",
            data: { message: msg },
            global: false
        });

    });

    $(window).on('show.bs.modal', function (e)
    {
        if (e.namespace === 'bs.modal')
        {
            $("body").css({ "overflow": "hidden" });
        }
    });

    $(window).on('hide.bs.modal', function (e)
    {
        if (e.namespace === 'bs.modal')
        {
            $("body").css("overflow", "");
        }
    });


    $(document).ajaxSend(function (event, request, settings)
    {
        var msg = "Starting request at " + settings.url + "";

        $.ajax({
            url: location.protocol + "//" + location.host + "/Log/Info",
            type: 'post',
            method: "POST",
            data: { message: msg },
            global: false
        });
    });

    //=================================================================
})();