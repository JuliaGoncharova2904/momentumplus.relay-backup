﻿$(document).ready(function () {
    "use strict";
    //================== Header module =======================================
    function MyTasksSection(panel) {
        this.taskBoardPanel = panel;
        this.userId = panel.attr("user-id");
        var controller = "/TaskBoard";
        this.updateTopicUrl = controller + "/UpdateTaskTopic";
        this.reloadTopicUrl = controller + "/ReloadTaskTopic";

        $(".plus-btn", this.taskBoardPanel).show();

        var queryModel = Relay.UrlQuery.getQueryModel();
        
        var filterBtn = $(".filter-btn", panel);

        $(".pdf-report", panel).click(function () {
            var queryModel = Relay.UrlQuery.getQueryModel();

            var pdfUrl = "/TaskBoard/TaskBoardSectionPdfReport?userId=" + panel.attr("user-id") + "&section=MyTasks&filter=" + (queryModel.filter || 'IncompletedTasks');

            window.open(pdfUrl, '_blank');
        });

        function resizeHandler() {
            WebuiPopovers.hide(filterBtn);
        };

        filterBtn.webuiPopover({
            hideEmpty: true,
            placement:'bottom',
            style: 'task-board',
            trigger: 'click',
            multi: false,
            dismissible: true,
            type: "async",
            cache: false,
            width: '118',
            url: "/TaskBoard/GetPopoverFilter?section=MyTasks&filter=" + (queryModel.filter || 'IncompletedTasks'),
            offsetLeft: -3,
            onShow: function () {
                window.addEventListener("resize", resizeHandler, false);
            },
            onHide: function () {
                window.removeEventListener("resize", resizeHandler, false);
            }
        });

        $(".tooltipster", panel).tooltipster({
            debug: false
        });

        MyTasksSection_Initialise.call(this);
        InitSortMechanism.call(this);

        $(".plus-btn", this.taskBoardPanel).click(this.addBtnClick.bind(this));
    }
    MyTasksSection.prototype.addBtnClick = function () {
        var addTaskDialog = Relay.FormDialog({
            url: "/TaskBoard/AddTaskDialog",
            data: {
                userId: this.userId,
                isMyTask: true
            }
        });
        addTaskDialog.setSubmitEvent(function () {
            location.reload();
        });
        addTaskDialog.open();
    }
    function InitSortMechanism() {
        $(".priority-title", this.taskBoardPanel).click(function () {
            var prioritySort = Relay.UrlQuery.getQueryModel();

            if ($(".priority-sort-icon", this).hasClass("high-to-low")) {
                prioritySort.sortStrategy = "PriorityReverse";
            }
            else if ($(".priority-sort-icon", this).hasClass("low-to-high")) {
                prioritySort.sortStrategy = null;
            }
            else {
                prioritySort.sortStrategy = "PriorityDirect";
            }

            Relay.UrlQuery.updateQueryModel(prioritySort);
        });

        $(".due-date-title", this.taskBoardPanel).click(function () {
            var dueDateSort = Relay.UrlQuery.getQueryModel();

            if ($(".due-date-sort-icon", this).hasClass("high-to-low")) {
                dueDateSort.sortStrategy = "DueDateReverse";
            }
            else if ($(".due-date-sort-icon", this).hasClass("low-to-high")) {
                dueDateSort.sortStrategy = null;
            }
            else {
                dueDateSort.sortStrategy = "DueDateDirect";
            }

            Relay.UrlQuery.updateQueryModel(dueDateSort);
        });
    }
    function NameClickTaskBoardCompleteHandler(model, reload, update) {
        var completeTaskDialog = Relay.FormDialog({
            url: "/TaskBoard/CompleteTaskChecklistDialog",
            data: {
                taskId: model.Id
            }
        });
        completeTaskDialog.setErrorEvent(function () {
            location.reload();
        });
        completeTaskDialog.setSubmitEvent(function () {
            location.reload();
        });
        completeTaskDialog.open();
    }

    function MyTasksSection_Initialise() {
        var _this = this;
        this.tasks = [];
        //-------
        $("tbody tr", this.taskBoardPanel).each(function () {
            var task = MyTasksSection_BuildTaskItem.call(_this, this);
            _this.tasks.push(task);
        })
    }

    function MyTasksSection_BuildTaskItem(trElement) {
        var readOnly = $(trElement).attr("read-only") === "True";

        var task = Relay.Topic(trElement, {
            updateUrl: this.updateTopicUrl,
            reloadUrl: this.reloadTopicUrl
        });

        if (!readOnly) {
            var taskName = $(".task-name-body", trElement);

            function resizeEditCompletePopoverHandler() {
                WebuiPopovers.hide(taskName);
            };

            taskName.webuiPopover({
                hideEmpty: true,
                selector: true,
                placement: "bottom-right",
                style: 'task-board-menu-popover',
                trigger: 'click',
                multi: false,
                dismissible: true,
                type: "async",
                cache: false,
                width: '83',
                offsetTop: -10,
                offsetLeft: 0,
                url: "/TaskBoard/GetEditCompleteMenuPopover?userId=" + this.userId + "&taskId=" + task.model.Id,
                onShow: function () {
                    window.addEventListener("resize", resizeEditCompletePopoverHandler, false);
                },
                onHide: function () {
                    window.removeEventListener("resize", resizeEditCompletePopoverHandler, false);
                }
            });

            task.AddControl(Relay.Topic.ControlsFactory.RemoveTopicBtn({
                type: Relay.Topic.ControlsFactory.RemoveTopicBtn.Type.task,
                changeHandler: function () { location.reload(); },
                warningMessage: "Are you sure you want to delete this task?"
            }));
        }

        task.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Name",
            elementClass: ".task-name-body",
            clickHandler: readOnly ? NameClickTaskBoardCompleteHandler : null,
            finalizeOff: true
        }));
        task.AddControl(Relay.Topic.ControlsFactory.PriorityField());
        task.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "CompleteStatus",
            elementClass: ".status-body",
            finalizeOff: true
        }));
        task.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
        task.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
            dialogType: Relay.AttachmentsDialog.Types.Draft,
            container: Relay.AttachmentsDialog.Containers.Task
        }));
        task.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
            dialogType: Relay.VoiceMessagesDialog.Types.Received,
            container: Relay.VoiceMessagesDialog.Containers.Task
        }));
        task.AddControl(Relay.Topic.ControlsFactory.ArchivedBtn({
            completeFieldName: "CompleteStatus"
        }));

        task.updateView();

        return task;
    }

    //========================================================================
    window.Relay = window.Relay || {};
    window.Relay.MyTasksSection = new MyTasksSection($(".task-board-page").first());
    //========================================================================
});