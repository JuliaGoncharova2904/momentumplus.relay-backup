﻿$(document).ready(function () {
    //================================================================================
    function TimelinePanel(timelinePanelElement) {
        this.panel = timelinePanelElement;

        TimelinePanel_Init.call(this);
    }

    function TimelinePanel_Init() {
        //------------- Timeline List ----------------
        var timelineSlider = new Swiper($("#timeline-panel .swiper-container", this.panel.conteiner), {
            spaceBetween: 0,
            slidesPerView: "auto",
            centeredSlides: true,
            freeMode: true,
            nextButton: $(".swiper-next", this.panel.conteiner),
            prevButton: $(".swiper-prev", this.panel.conteiner),
            breakpoints: {
                320: {
                    slidesPerView: "auto",
                    spaceBetween: 0,
                },
                480: {
                    slidesPerView: "auto",
                    spaceBetween: 0,
                },
                640: {
                    slidesPerView: "auto",
                    spaceBetween: 0,
                },
                768: {
                    slidesPerView: "auto",
                    spaceBetween: 0,
                },
                1024: {
                    slidesPerView: "auto",
                    spaceBetween: 0,
                }
            }
        });

        //timelineSlider.on('onSlideChangeEnd', function (swiper) {
        //    //activeSlide = $(swiper.getSlide(swiper.activeIndex));
        //    //if (swipe.activeIndex <= 5) {
        //    //    console.log("----------------");
        //    //}

        //    //console.log(swipe.activeIndex);
        //});
        //-----------------------------------------------
        $(".swiper-slide", this.panel.conteiner).each(function (slideIndex) {
            if ($(this).attr("data-active") === 'True') {
                timelineSlider.slideTo(slideIndex, 1, false);
            }
        });
        //-----------------------------------------------
    };

    //================================================================================
    window.Relay = window.Relay || {};
    window.Relay.TimelinePanel = new TimelinePanel($("#timeline-panel"));
    //================================================================================
});