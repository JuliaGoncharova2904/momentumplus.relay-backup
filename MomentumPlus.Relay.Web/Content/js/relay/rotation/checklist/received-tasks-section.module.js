﻿; (function (window) {
    //===============================================================================================
    //  container: (section div)
    //==============================  Received Tasks Section ========================================
    //------------------- Name Column Handler -----------------------------
    function NameClickHandler(model, reload, update) {
        var _this = this;

        if (!model.IsNullReport) {
            var complitTaskDialog = Relay.FormDialog({
                url: "/Tasks/CompleteTaskDialog?TaskId=" + model.Id,
                method: "GET",
                dialogId: "completeTaskFormDialog"
            });
            complitTaskDialog.setSubmitEvent(reload);
            complitTaskDialog.open();
        }
    }
    //---------------------------------------------------------------------
    function ReceivedTasksPanel(container, rotationId) {
        //----------------------
        var controller = "/Checklist";
        this.sectionUrl = controller + "/ReceivedTasksPanel";
        this.reloadTopicUrl = controller + "/ReloadTopic";
        //----------------------
        this.topics;
        this.container = container;
        //----------------------
        this.userIds = null;
        this.rotationId = rotationId;
        //----------------------
        ReceivedTasksPanel_Initialise.call(this);
    }
    ReceivedTasksPanel.prototype.reload = function (rotationId, userIds) {
        var _this = this;

        this.userIds = userIds;
        this.rotationId = rotationId;

        $.ajax({
            url: this.sectionUrl,
            method: "GET",
            data: {
                RotationId: rotationId,
                UserIds: userIds
            }
        })
        .done(function (newContent) {
            //--- update content -----
            _this.container.html($(newContent));
            //---- Init section ------
            ReceivedTasksPanel_Initialise.call(_this)
            //------------------------
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    function ReceivedTasksPanel_Initialise() {
        var _this = this;
        this.topics = [];
        //-------
        $("tbody tr", this.container).each(function () {
            var topic = ReceivedTasksPanel_BuildTopic.call(_this, this);
            _this.topics.push(topic);
        })
    }
    function ReceivedTasksPanel_BuildTopic(trElement) {
        var _this = this;

        var topic = Relay.Topic(trElement, {
            reloadUrl: this.reloadTopicUrl
        });

        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Location",
            elementClass: ".location-td"
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Name",
            elementClass: ".name-td",
            clickHandler: NameClickHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NotesField({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.FinaliseBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.PriorityField());
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Deadline",
            elementClass: ".deadline-field"
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "CompleteStatus",
            elementClass: ".complite-status"
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TeamBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
            dialogType: Relay.AttachmentsDialog.Types.Received,
            container: Relay.AttachmentsDialog.Containers.Task
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn());

        topic.updateView();

        return topic;
    }
    //===============================================================================================
    window.Relay = window.Relay || {};

    window.Relay.ReceivedTasksPanel = function (container, rotationId) {
        return new ReceivedTasksPanel(container, rotationId);
    };
    //===============================================================================================
})(window);