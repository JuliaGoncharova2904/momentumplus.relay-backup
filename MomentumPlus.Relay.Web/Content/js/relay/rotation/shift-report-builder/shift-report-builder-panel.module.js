﻿$(document).ready(function () {
    "use strict";
    //===========================================================================
    var reportsTypeVarName = "ReportsType";
    //--------------------------------
    var ReportesTypes = {
        Draft: "DraftPanel",
        Received: "ReceivedPanel"
    };
    //===========================================================================
    function ShiftReportBuilderPanel(panelElement) {
        //-------------------------
        this.panelElement = panelElement;

        Relay.UrlHash.addChangeEvent(ShiftReportBuilderPanel_ExecHashParams.bind(this));
        //-------------------------
    }
    function ShiftReportBuilderPanel_Init() {
        //-------------------------
        var _this = this;
        //--------------
        if ($(".shift-info-container", this.panelElement).attr("empty-panel") === "true")
            return;
        //--------------
        this.shiftId = $(".shift-info-container", this.panelElement).attr("shift-id");
        this.readOnly = $(".shift-info-container", this.panelElement).attr("read-mode") === "True";
        this.subpanelController && this.subpanelController.dispose();
        this.subpanelController = null;
        //-----------------------
        this.panelHeading = $(".panel-heading", this.panelElement).first();
        this.panelBody = $(".panel-body", this.panelElement).first();
        //-----------------------
        this.plusBtn = $(".plus-btn", this.panelElement).first();
        this.filterBtn = $(".filter-btn", this.panelElement).first();
        this.finishBtn = $(".finish-shift-btn", this.panelElement).first();
        this.documentBtn = $(".document-btn", this.panelElement).first();
        this.qmePdfReportBtn = $(".qme-pdf-report", this.panelElement).first();
        //--------------
        this.sectionBtns = $("#section-filter a", this.panelElement);
        this.leftControls = $(".left-controls", this.panelElement);
        //-----------------------
        this.controls = {
            documentBtn: this.documentBtn,
            plusBtn: this.plusBtn,
            qmePdfReportBtn: this.qmePdfReportBtn,
            finishBtn: new Relay.ShiftHandoverFinishShiftBtn(this.finishBtn, this.shiftId).build(),
            filterBtn: new Relay.ShiftHandoverFilterBtn(this.filterBtn),
            shareReportBlock: $(".share-report-block", this.panelElement).first(),
            dateBlock: $(".date-block", this.panelElement).first(),
            nameBlock: $(".name-block", this.panelElement).first(),
            pencilBlock: $(".pencil-block", this.panelElement).first(),
            finaliseAllBtn: $(".finalise-all-btn", this.panelElement).first(),
            expandCollapseAllBtn: $(".expand-all-btn", this.panelElement).first(),
            panelContainer: $("#shiftReportBuilderData", this.panelElement),
            shareReportBtnController: new Relay.ShareReportBtn($(".share-report-block", this.panelElement))
        };
        //------- Init Btn --------
        this.qmePdfReportBtn.click(this.QMEReportBtnHandler.bind(this));
        this.documentBtn.click(this.DocumentBtnHandler.bind(this));
        //--------
        this.sectionBtns.click(function () {
            var reportType;

            switch ($(this).attr("id")) {
                case "draftBtn":
                    reportType = ReportesTypes.Draft;
                    break;
                case "receivedBtn":
                    reportType = ReportesTypes.Received;
                    break;
                default:
                    reportType = $(this).text().replace(/ /g, "_");
            }

            var hashModel = Relay.UrlHash.getHashModel();
            hashModel[reportsTypeVarName] = reportType;
            Relay.UrlHash.updateHashModel(hashModel);
        });
        //----- Init tooltips -----
        $(this.documentBtn).tooltipster({
            debug: false
        });
        this.controls.panelContainer.bind("DOMNodeInserted DOMNodeRemoved", function () {
            $(".tooltipster", this).tooltipster({
                debug: false
            });
        });
        //-------------------------
        var hashModel = Relay.UrlHash.getHashModel();
        if (!this.LoadPanel(hashModel[reportsTypeVarName])) {
            hashModel[reportsTypeVarName] = ReportesTypes.Draft;
            Relay.UrlHash.updateHashModel(hashModel);
        }
        //-------------------------
    }
    ShiftReportBuilderPanel.prototype.enable = function () {
        this.panelHeading.removeAttr("style");
        this.panelBody.removeAttr("style");
    };
    ShiftReportBuilderPanel.prototype.disable = function () {
        this.panelHeading.attr("style", "pointer-events:none;");
        this.panelBody.attr("style", "pointer-events:none;");
    };
    ShiftReportBuilderPanel.prototype.loadForShift = function (shiftId) {
        var _this = this;

        var loadingAnimationTimeout = setTimeout(function () {
            $('#loading-animation').show();
        }, 200);

        $.ajax({
            url: "/ShiftReportBuilder/Index",
            method: "GET",
            data: { shiftId: shiftId }
        })
        .done(function (newContent) {
            _this.panelElement.html($(newContent).children());
            ShiftReportBuilderPanel_Init.call(_this);
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        })
        .always(function () {
            clearTimeout(loadingAnimationTimeout);
            $('#loading-animation').hide();
        });
    };
    ShiftReportBuilderPanel.prototype.reload = function () {
        if (this.subpanelController) {
            this.subpanelController.load();
        }
    };
    ShiftReportBuilderPanel.prototype.LoadPanel = function (panelType) {
        if (!panelType)
            return false;

        this.sectionBtns.parent().removeClass("active");

        switch (panelType) {
            case ReportesTypes.Received:
                this.plusBtn.hide();
                this.filterBtn.hide();
                this.documentBtn.show();
                this.qmePdfReportBtn.show();
                this.leftControls.show();
                $("#receivedBtn").parent().addClass("active");
                ShiftReportBuilderPanel_SetSubpanelController.call(this, Relay.ShiftReceivedSubpanel(this.shiftId, this.controls, this.readOnly));
                break;
            case ReportesTypes.Draft:
                this.plusBtn.show();
                this.filterBtn.show();
                this.documentBtn.show();
                this.qmePdfReportBtn.show();
                this.leftControls.show();
                $("#draftBtn").parent().addClass("active");
                ShiftReportBuilderPanel_SetSubpanelController.call(this, Relay.ShiftDraftSubpanel(this.shiftId, this.controls, this.readOnly));
                break;
            default:
                this.plusBtn.hide();
                this.filterBtn.hide();
                this.documentBtn.hide();
                this.qmePdfReportBtn.hide();
                this.leftControls.hide();
                $("#section-filter a:contains('" + panelType.replace(/_/g, " ") + "')").parent().addClass("active");
                this.controls.finaliseAllBtn.hide();
                this.controls.expandCollapseAllBtn.hide();
                ShiftReportBuilderPanel_SetSubpanelController.call(this, null);
        }

        return true;
    };
    function ShiftReportBuilderPanel_SetSubpanelController(controller) {
        if (this.subpanelController) {
            this.subpanelController.dispose();
        }
        this.subpanelController = controller;
    }
    function ShiftReportBuilderPanel_ExecHashParams(model, modelChangesMap) {
        if (modelChangesMap[reportsTypeVarName]) {
            if (!this.LoadPanel(model[reportsTypeVarName])) {
                model[reportsTypeVarName] = ReportesTypes.Draft;
                Relay.UrlHash.updateHashModel(model);
            }
        }
    };

    ShiftReportBuilderPanel.prototype.reloadSection = function (sectionType) {
        if (this.subpanelController) {
            this.subpanelController.reloadSection(sectionType);
        }
    }
    ShiftReportBuilderPanel.prototype.DocumentBtnHandler = function () {
        if (this.subpanelController) {
            var hashModel = Relay.UrlHash.getHashModel();;
            var shiftId = this.subpanelController.getShiftId();
            var isReceived = hashModel[reportsTypeVarName] === ReportesTypes.Received;
            var selectedShiftId = null;

            if (isReceived) {
                selectedShiftId = this.subpanelController.getReceivedShiftId();
            }

            var pdfUrl = "/Reports/HandoverReport?sourceId=" + shiftId
                + "&isShift=true"
                + "&isReceived=" + isReceived
                + "&selectedSourceId=" + selectedShiftId;

            window.open(pdfUrl, '_blank');
            //var hashModel = Relay.UrlHash.getHashModel();;
            //var isReceived = hashModel[reportsTypeVarName] === ReportesTypes.Received;
            //var shiftId = this.subpanelController.getShiftId();
            //var selectedShiftId = null;

            //if (isReceived) {
            //    selectedShiftId = this.subpanelController.getReceivedShiftId();
            //}

            //Relay.FormDialog({
            //    url: "/ShiftReportBuilder/ReportPreview",
            //    method: "GET",
            //    data: {
            //        isReceived: isReceived,
            //        shiftId: shiftId,
            //        selectedShiftId: selectedShiftId
            //    },
            //    dialogId: "popupReportPreview"
            //}).open();
        }
    };

    ShiftReportBuilderPanel.prototype.QMEReportBtnHandler = function () {
        if (this.subpanelController) {
            var hashModel = Relay.UrlHash.getHashModel();;
            var shiftId = this.subpanelController.getShiftId();
            var isReceived = hashModel[reportsTypeVarName] === ReportesTypes.Received;
            var selectedShiftId = null;

            if (isReceived) {
                selectedShiftId = this.subpanelController.getReceivedShiftId();
            }

            var pdfUrl = "/Reports/QMEReport?sourceId=" + shiftId
                + "&isShift=true"
                + "&isReceived=" + isReceived
                + "&selectedSourceId=" + selectedShiftId;

            window.open(pdfUrl, '_blank');
        }
    };
    //===========================================================================
    window.Relay = window.Relay || {};
    window.Relay.ShiftReportBuilderPanel = new ShiftReportBuilderPanel($("#shiftReportBuilderPanel").first());
    //===========================================================================
});