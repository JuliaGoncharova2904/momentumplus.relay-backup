﻿; (function (window) {
    //===============================================================================================
    //  sectionElement: (section div)
    //  shiftId: (shift id)
    //  readOnly: (readonly mode flag)
    //==============================  Draft Core Section ============================================
    //-----------  Process Location Column click handler ------------------
    function ProcessLocationColumnClickHandler(model, reload, update) {
        if (!model.IsNullReport) {
            var editCoreTopicDialog = Relay.FormDialog({
                url: "/ShiftCoreSection/EditTopicDialog?topic=" + model.Id,
                method: "GET",
                dialogId: "CoreTopicFormDialog"
            });
            editCoreTopicDialog.setSubmitEvent(reload);
            editCoreTopicDialog.open();
        }
    }
    //---------------------------------------------------------------------
    function ShiftDraftCoreSection(sectionElement, shiftId, readOnly) {
        //----------------------
        this.type = Relay.SectionFactory.Types.Core;
        var controller = "/ShiftCoreSection";
        this.sectionUrl = controller + "/Draft";
        this.updateTopicUrl = controller + "/UpdateTopic";
        this.reloadTopicUrl = controller + "/ReloadTopic";
        this.addInlineTopicUrl = controller + "/AddInlineTopic";
        //----------------------
        this.topics;
        this.isExpanded = false;
        this.shiftId = shiftId;
        this.readOnly = readOnly;
        this.sectionElement = sectionElement;
        this.reloadExternalSection = null;
        this.openInlineTopic = null;
        this.closeInlineTopic = null;
        //----------------------
        DraftCoreSection_Initialise.call(this);
    }
    ShiftDraftCoreSection.prototype.reload = function () {
        var _this = this;

        $.ajax({
            url: this.sectionUrl,
            method: "GET",
            data: { shiftId: this.shiftId }
        })
        .done(function (newContent) {
            //--- update content -----
            _this.sectionElement.html($(newContent).children());
            //---- Init section ------
            DraftCoreSection_Initialise.call(_this)
            //------------------------
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
        //-----------------------------
        if (Relay.DailyReportsPanel) {
            Relay.DailyReportsPanel.reloadSlideById(this.shiftId);
        }
        //-----------------------------
    };
    ShiftDraftCoreSection.prototype.expand = function () {
        this.isExpanded = true;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].autoCollapseOff();
            this.topics[i].expand();
        }
    };
    ShiftDraftCoreSection.prototype.collapse = function () {
        this.isExpanded = false;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].collapse();
            this.topics[i].autoCollapseOn();
        }
    };
    ShiftDraftCoreSection.prototype.disableTopics = function () {
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].disable();
        }
    }
    ShiftDraftCoreSection.prototype.enableTopics = function () {
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].enable();
        }
    }
    function DraftCoreSection_Initialise() {
        var _this = this;
        this.topics = [];
        //-------
        if (!$("tbody tr.no-items-message", this.sectionElement).length) {
            $("tbody tr", this.sectionElement).each(function () {
                var topic = DraftCoreSection_BuildTopic.call(_this, this);

                if (_this.isExpanded) {
                    topic.autoCollapseOff();
                    topic.expand();
                }

                _this.topics.push(topic);
            });
        }
        //-------
        if (this.readOnly) {
            $(".add-item-btn", this.sectionElement).parent().hide();

            if ($(".no-items-message", this.sectionElement).length) {
                $(".table", this.sectionElement).hide();
                $(".no-items-message", this.sectionElement).show();
            }
        } else {
            var addInlineTopic = Relay.AddInlineTopic(this.sectionElement, {
                url: this.addInlineTopicUrl,
                destId: this.shiftId,
                topicName: this.type,
                warningMessage: "Please choose a Process Group and enter a Process. Exit?"
            });
            addInlineTopic.setSaveEvent(this.reload.bind(this));
            addInlineTopic.setOpenEvent(function () {
                if (_this.openInlineTopic)
                    _this.openInlineTopic();
            });
            addInlineTopic.setCloseEvent(function () {
                if (_this.closeInlineTopic)
                    _this.closeInlineTopic();
            });
        }
    }
    function DraftCoreSection_BuildTopic(trElement) {

        var topic = Relay.Topic(trElement, {
            updateUrl: this.updateTopicUrl,
            reloadUrl: this.reloadTopicUrl
        });

        topic.AddControl(Relay.Topic.ControlsFactory.RemoveTopicBtn({
            readOnly: this.readOnly,
            type: Relay.Topic.ControlsFactory.RemoveTopicBtn.Type.topic,
            changeHandler: DraftCoreSection_RemoveTopicHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NullReportBtn({
            readOnly: this.readOnly,
            changeHandler: DraftCoreSection_TasksAndActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TransferredBtn({
            readOnly: this.readOnly,
            changeHandler: DraftCoreSection_TasksAndActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.PinnedBtn({
            readOnly: this.readOnly,
            changeHandler: DraftCoreSection_TasksAndActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Category",
            elementClass: ".type-body"
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Reference",
            elementClass: ".reference-body",
            clickHandler: this.readOnly ? null : ProcessLocationColumnClickHandler
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NotesField({
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.FinaliseBtn({
            readOnly: this.readOnly,
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.ManagerCommentsBtn({
            readOnly: this.readOnly,
            forTopic: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.LocationBtn({
            readOnly: this.readOnly,
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TasksBtn({
            dialogType: this.readOnly ? Relay.TasksListDialog.Types.ReadOnly : Relay.TasksListDialog.Types.Draft,
            changeHandler: DraftCoreSection_TasksAndActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
            dialogType: this.readOnly ? Relay.AttachmentsDialog.Types.Received : Relay.AttachmentsDialog.Types.Draft,
            container: Relay.AttachmentsDialog.Containers.Topic
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
            dialogType: Relay.VoiceMessagesDialog.Types.Draft,
            container: Relay.VoiceMessagesDialog.Containers.Topic
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.ShareBtn({
            readOnly: this.readOnly
        }));

        topic.updateView();

        return topic;
    }
    function DraftCoreSection_TasksAndActionsChangeHandler(model) {
        if (this.reloadExternalSection)
            this.reloadExternalSection(Relay.SectionFactory.Types.Tasks);
    }
    function DraftCoreSection_RemoveTopicHandler(model) {
        this.reload();
        if (this.reloadExternalSection)
            this.reloadExternalSection(Relay.SectionFactory.Types.Tasks);
    }
    //===============================================================================================
    if (window.Relay.SectionFactory) {
        window.Relay.SectionFactory.ShiftDraftCoreSection = function (sectionElement, shiftId, readOnly) {
            return new ShiftDraftCoreSection(sectionElement, shiftId, readOnly);
        };
    }
    else {
        console.error("Draft Core Section cannot work without SectionFactory.");
    }
    //===============================================================================================
})(window);