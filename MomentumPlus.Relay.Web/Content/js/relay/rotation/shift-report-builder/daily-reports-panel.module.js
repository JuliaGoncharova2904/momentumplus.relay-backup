﻿$(document).ready(function () {
    //================================================================================
    function DailyReport(dailyReportElement, chooseShiftHandler, reloadHandler) {
        this.id = $(dailyReportElement).attr("shift-id");
        this.dailyReportElement = dailyReportElement;
        this.chooseShiftHandler = chooseShiftHandler;
        this.reloadHandler = reloadHandler;

        this.init();
    }
    DailyReport.prototype.init = function () {
        var _this = this;
        var slide = $(this.dailyReportElement).children().first();

        if (slide.hasClass("safety-message")) {
            var safetyVersion = $(slide).attr("safety-version");

            switch (safetyVersion) {
                case "V1":
                    slide.click(this.showSafetyMessageV1.bind(this));
                    break;
                case "V2":
                    slide.click(this.showSafetyMessageV2.bind(this));
                    break;
            }
        }
        else {
            slide.click(function () {
                _this.chooseShiftHandler(_this.id);
            });
        }
    };
    DailyReport.prototype.setActiveById = function (id) {
        if (this.id === id) {
            $(this.dailyReportElement).addClass("active");
        }
        else {
            $(this.dailyReportElement).removeClass("active");
        }
    };
    DailyReport.prototype.reload = function () {
        var _this = this;

        $.ajax({
            url: "/DailyReportsPanel/DailyReportSlide",
            method: "GET",
            data: { shiftId: this.id }
        })
        .done(function (newContent) {
            $(_this.dailyReportElement).html($(newContent).children());
            _this.init();

            if (_this.reloadHandler) {
                _this.reloadHandler();
            }
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    DailyReport.prototype.showSafetyMessageV1 = function (event) {
        var date = $(event.currentTarget).attr('date');

        var formDialog = Relay.FormDialog({
            url: "/ScheduleMessage/GetSafetyMessagesForUser",
            method: "GET",
            dialogId: "safetyMessageDialog",
            data: { date: date }
        });
        formDialog.setCloseEvent(this.reload.bind(this));
        formDialog.open();
    };
    DailyReport.prototype.showSafetyMessageV2 = function (event) {
        var date = $(event.currentTarget).attr('date');

        var formDialog = Relay.FormDialog({
            url: "/SafetyMessageV2/GetSafetyMessagesForUser",
            method: "GET",
            dialogId: "safetyMessageV2Dialog",
            data: { date: date }
        });
        formDialog.setCloseEvent(this.reload.bind(this));
        formDialog.open();
    };
    //================================================================================
    var selectesShiftIdParam = "DailyReportShiftId";

    function DailyReportsPanel(panelElement, userId) {
        this.userId = userId;
        this.panel = panelElement;
        this.commonSlide = $(".daily-container", panelElement);
        this.slides = [];

        this.init();
    }
    DailyReportsPanel.prototype.init = function () {
        var _this = this;
        //-----------------------------------------------
        $(".panel-body", this.panel).removeAttr("style");
        //------------- Daily Notes List ----------------
        var dailyReportsSlider = new Swiper($(".daily-report-swiper", this.panel), {
            slidesPerView: "auto",
            spaceBetween: 15,
            centeredSlides: false,
            hashnav: false,
            // Navigation arrows
            nextButton: $(".daily-report-swiper-next", this.panel),
            prevButton: $(".daily-report-swiper-prev", this.panel),
            breakpoints: {
                320: {
                    slidesPerView: "auto",
                    spaceBetween: 15
                },
                480: {
                    slidesPerView: "auto",
                    spaceBetween: 15
                },
                640: {
                    slidesPerView: "auto",
                    spaceBetween: 15
                },
                768: {
                    slidesPerView: "auto",
                    spaceBetween: 15
                },
                1024: {
                    slidesPerView: "auto",
                    spaceBetween: 15
                }
            }
        });
        //-----------------------------------------------
        $(".swiper-slide", this.panel).each(function () {
            if (!$(this).hasClass("long-slide")) {
                _this.slides.push(new DailyReport(this, changeReportShiftId, _this.reloadCommonReport.bind(_this)));
            }
        });
        //-----------------------------------------------
        var hashModel = Relay.UrlHash.getHashModel();

        if (hashModel[selectesShiftIdParam]) {
            $(".swiper-slide", this.panel).removeClass("active");
            $(".swiper-slide[shift-id='" + hashModel[selectesShiftIdParam] + "']", this.panel).addClass("active");;
        }

        setTimeout(function () {
            updateContentForShift.call(_this, hashModel);
        }, 50);

        //-----------------------------------------------
        if ($(".swiper-slide.active", this.panel).length) {
            $(".swiper-slide", this.panel).each(function (slideNumber) {
                if ($(this).hasClass("active")) {
                    dailyReportsSlider.slideTo(slideNumber, 1, false);

                    hashModel[selectesShiftIdParam] = $(this).attr("shift-id");
                    Relay.UrlHash.updateHashModel(hashModel);
                }
            });
        }
        else {
            dailyReportsSlider.slideTo(dailyReportsSlider.slides.length, 1, false);
        }
        //-----------------------------------------------
        Relay.UrlHash.addParamListener(selectesShiftIdParam, updateContentForShift.bind(this));
        //-----------------------------------------------
        return this;
        //-----------------------------------------------
    };
    DailyReportsPanel.prototype.enable = function () {
        this.panel.removeAttr("style");
    };
    DailyReportsPanel.prototype.disable = function () {
        this.panel.attr("style", "pointer-events:none;");
    };
    DailyReportsPanel.prototype.reloadPanel = function () {
        var _this = this;

        $.ajax({
            url: "/DailyReportsPanel/DailyReportsPanel",
            method: "GET",
            data: { userId: this.userId }
        })
        .done(function (newContent) {
            _this.panel.empty();
            $(newContent).children().each(function () {
                _this.panel.append(this);
            });
            _this.init();
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    DailyReportsPanel.prototype.reloadSlideById = function (id) {
        for (var i = 0; i < this.slides.length; ++i) {
            if (this.slides[i].id === id) {
                this.slides[i].reload();
            }
        }
    };
    DailyReportsPanel.prototype.reloadCommonReport = function () {
        var _this = this;

        $.ajax({
            url: "/DailyReportsPanel/CommonReportSlide",
            method: "GET",
            data: { userId: this.userId }
        })
        .done(function (newContent) {
            _this.commonSlide.html($(newContent));
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };

    function changeReportShiftId(shiftId) {
        var hashModel = Relay.UrlHash.getHashModel();
        hashModel[selectesShiftIdParam] = shiftId;
        Relay.UrlHash.updateHashModel(hashModel);
    }
    function updateContentForShift(model) {
        for (var i = 0; i < this.slides.length; ++i) {
            this.slides[i].setActiveById(model[selectesShiftIdParam]);
        }

        if (Relay.ShiftReportBuilderPanel) {
            Relay.ShiftReportBuilderPanel.loadForShift(model[selectesShiftIdParam]);
        }
    }
    //================================================================================
    window.Relay = window.Relay || {};
    window.Relay.DailyReportsPanel = new DailyReportsPanel($("#dailyReportPanel"), $("#RotarionOwnerId").val());
    //================================================================================
});