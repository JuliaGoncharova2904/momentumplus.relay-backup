﻿; (function (window) {
    //===============================================================================================
    //  sectionElement: (section div)
    //  shiftId: (shift id)
    //  readOnly: (readonly mode flag)
    //==============================  Received Tasks Section ========================================
    //------------------- Name Column Handler -----------------------------
    function NameCompleteClickHandler(model, reload, update) {
        if (!model.IsNullReport) {
            var tasksListDialog = Relay.FormDialog({
                url: "/Tasks/CompleteTaskDialog",
                method: "GET",
                data: { TaskId: model.Id }
            });
            tasksListDialog.setCloseEvent(reload);
            tasksListDialog.open();
        }
    }
    function NameDetailsClickHandler(model, reload, update) {
        var tasksDetailsDialog = Relay.FormDialog({
            url: "/Tasks/TaskDetailsDialog",
            method: "GET",
            data: { TaskId: model.Id }
        });
        tasksDetailsDialog.setErrorEvent(function () {
            location.reload();
        });
        tasksDetailsDialog.open();
    }
    //---------------------------------------------------------------------
    function ShiftReceivedTasksSection(sectionElement, shiftId, readOnly) {
        //----------------------
        this.type = Relay.SectionFactory.Types.Tasks;
        var controller = "/ShiftTasksSection";
        this.sectionUrl = controller + "/Received?shiftId=" + shiftId;
        this.updateTopicUrl = controller + "/UpdateTopic";
        this.reloadTopicUrl = controller + "/ReloadTopic";
        //----------------------
        this.topics;
        this.isExpanded = false;
        this.shiftId = shiftId;
        this.readOnly = readOnly;
        this.sectionElement = sectionElement;
        this.reloadExternalSection = null;
        //----------------------
        ReceivedTasksSection_Initialise.call(this);
    }
    ShiftReceivedTasksSection.prototype.reload = function () {
        var _this = this;

        $.ajax({
            url: _this.sectionUrl,
            method: "GET"
        })
        .done(function (newContent) {
            //--- update content -----
            _this.sectionElement.html($(newContent).children());
            //---- Init section ------
            ReceivedTasksSection_Initialise.call(_this)
            //------------------------
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    ShiftReceivedTasksSection.prototype.expand = function () {
        this.isExpanded = true;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].autoCollapseOff();
            this.topics[i].expand();
        }
    };
    ShiftReceivedTasksSection.prototype.collapse = function () {
        this.isExpanded = false;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].collapse();
            this.topics[i].autoCollapseOn();
        }
    };
    function ReceivedTasksSection_Initialise() {
        var _this = this;
        this.topics = [];
        //-------
        $("tbody tr", this.sectionElement).each(function () {
            var topic = ReceivedTasksSection_BuildTopic.call(_this, this);

            if (_this.isExpanded) {
                topic.autoCollapseOff();
                topic.expand();
            }

            _this.topics.push(topic);
        });
    }
    function ReceivedTasksSection_BuildTopic(trElement) {
        var topic = Relay.Topic(trElement, {
            updateUrl: this.updateTopicUrl,
            reloadUrl: this.reloadTopicUrl
        });

        var isComplete = $(trElement).attr("is-complete") === "True";

        if (!isComplete && !this.readOnly) {
            $(".reference-body", trElement).webuiPopover({
                hideEmpty: true,
                placement: "bottom-right",
                style: 'shift-menu-popover',
                trigger: 'click',
                multi: false,
                dismissible: true,
                type: "async",
                cache: false,
                width: '83',
                offsetTop: -1,
                offsetLeft: -15,
                url: "/ShiftTasksSection/GetEditCompleteMenuPopover?taskId=" + $("#Id", trElement).val()
            });
        };

        topic.AddControl(Relay.Topic.ControlsFactory.TransferredBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Location",
            elementClass: ".type-body"
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Name",
            elementClass: ".reference-body",
            clickHandler: this.readOnly
                                    ? NameDetailsClickHandler
                                    : isComplete ? NameCompleteClickHandler : null
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NotesField({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.PriorityField());
        topic.AddControl(Relay.Topic.ControlsFactory.FinaliseBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "CompleteStatus",
            elementClass: ".complite-status"
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.ManagerCommentsBtn({
            readOnly: true,
            forTask: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TeamBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
            dialogType: Relay.AttachmentsDialog.Types.Received,
            container: Relay.AttachmentsDialog.Containers.Task
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.LocationBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
            dialogType: Relay.VoiceMessagesDialog.Types.Received,
            container: Relay.VoiceMessagesDialog.Containers.Task
        }));

        topic.updateView();

        return topic;
    }
    //===============================================================================================
    if (window.Relay.SectionFactory) {
        window.Relay.SectionFactory.ShiftReceivedTasksSection = function (sectionElement, shiftId, readOnly) {
            return new ShiftReceivedTasksSection(sectionElement, shiftId, readOnly);
        };
    }
    else {
        console.error("Received Tasks Section cannot work without SectionFactory.");
    }
    //===============================================================================================
})(window);