﻿; (function (window) {
    //============================== Null Report Button =============================================
    var dataAttributeName = "topic-val";
    //--------------------------------------
    var TaskPriorityClass = {
        Low: "btn-low",
        Normal: "btn-success",
        Important: "btn-info",
        Critical: "btn-warning",
        Urgent: "btn-danger"
    };
    var TaskPriorityName = {
        '0': "Low",
        '1': "Normal",
        '2': "Important",
        '3': "Critical",
        '4': "Urgent",
        Low: "Low",
        Normal: "Normal",
        Important: "Important",
        Critical: "Critical",
        Urgent: "Urgent"
    };
    //--------------------------------------
    function PriorityField() {
        this.topic = null;
        this.control = null;
    }
    PriorityField.prototype.initialize = function (topic) {
        var _this = this;
        this.topic = topic;
        var control = $('.btn-priority', topic.topicElement).first();
        if (control.length && control[0].hasAttribute("topic-val")) {
            this.control = control;
            //----------
            topic.model.Priority = control.attr("topic-val");
            //----------
            return true;
        }

        return false;
    };
    PriorityField.prototype.update = function () {
        if (this.control) {
            var priority = TaskPriorityName[this.topic.model.Priority];
            for (var item in TaskPriorityClass) {
                if (item !== priority) {
                    this.control.removeClass(TaskPriorityClass[item]);
                }
            }
            this.control.addClass(TaskPriorityClass[priority]);
            this.control.text(priority);
        }
    };
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.PriorityField = function () {
            return new PriorityField();
        };
    }
    else {
        console.error("Priority Field control cannot work without Topic control.");
    }
    //===============================================================================================
})(window);