﻿; (function (window) {
    //===============================================================================================
    //  config {
    //      dialogType: (type),
    //      container: (container)
    //      changeHandler: (handler)
    //  }
    //============================== Attachments Button =============================================
    var dataAttributeName = "topic-val";
    //--------------------------------------
    function AttachmentsBtn(config) {
        this.topic = null;
        this.control = null;
        this.config = config;
    }
    AttachmentsBtn.prototype.initialize = function (topic) {
        var _this = this;
        this.topic = topic;
        var control = $('.attachments-btn', topic.topicElement).first();
        if (control.length !== 0 && control[0].hasAttribute(dataAttributeName)) {
            this.control = control;
            //----------
            topic.model.HasAttachments = control.attr(dataAttributeName) === "true";
            //----------
            control.click(function () {
                if (!topic.model.IsNullReport) {
                    var attachmentsDialog = Relay.AttachmentsDialog(topic.model.Id,
                                                                    _this.config.dialogType,
                                                                    _this.config.container);
                    if (_this.config.dialogType === Relay.AttachmentsDialog.Types.Draft) {
                        attachmentsDialog.setCloseEvent(function () {
                            topic.reloadServerData();
                            if (_this.config.changeHandler)
                                _this.config.changeHandler();
                        });
                    }
                    attachmentsDialog.open();
                }
            });
            //----------
            return true;
        }

        return false;
    };
    AttachmentsBtn.prototype.update = function () {
        if (this.topic.model.HasAttachments) {
            //-- Rotation builder classes ---
            this.control.removeClass('clip-inactive-action-btn');
            this.control.addClass('clip-active-action-btn');
            //-- Shift builder classes ------
            this.control.removeClass('attachment-inactive-btn');
            this.control.addClass('attachment-active-btn');
        }
        else {
            //-- Rotation builder classes ---
            this.control.removeClass('clip-active-action-btn');
            this.control.addClass('clip-inactive-action-btn');
            //-- Shift builder classes ------
            this.control.removeClass('attachment-active-btn');
            this.control.addClass('attachment-inactive-btn');
        }
    };
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.AttachmentsBtn = function (clickHandler) {
            return new AttachmentsBtn(clickHandler);
        };
    }
    else {
        console.error("Attachments Button control cannot work without Topic control.");
    }
    //===============================================================================================
})(window);