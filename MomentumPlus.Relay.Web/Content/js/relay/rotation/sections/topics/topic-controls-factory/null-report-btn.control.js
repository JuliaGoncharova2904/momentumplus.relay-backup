﻿; (function (window)
{
    //===============================================================================================
    //  config {
    //      readOnly: false
    //      changeHandler: (handler)
    //  }
    //============================== Null Report Button =============================================
    var dataAttributeName = "topic-val";
    //--------------------------------------
    function NullReportBtn(config)
    {
        this.config = config || {};
        this.topic = null;
        this.control = null;
        this.filterType = Relay.UrlHash.getHashParam('filter') || 'All';
    }
    NullReportBtn.prototype.initialize = function (topic)
    {
        var _this = this;
        this.topic = topic;
        var control = $('.nr-btn', topic.topicElement).first();

        if (control.length !== 0 && control[0].hasAttribute(dataAttributeName))
        {
            //----------
            this.control = control;
            topic.model.IsNullReport = control.attr(dataAttributeName) === "true";
            //----------
            if (!this.config.readOnly)
            {
                control.click(function ()
                {
                    var hasItem = topic.model.HasAttachments || topic.model.HasComments || topic.model.HasTasks || topic.model.HasLocation;
                    if (topic.model.IsNullReport)
                    {
                        //----------
                        topic.model.IsNullReport = false;
                        _this.update();
                        topic.addChangeEventLisetner(_this.config.changeHandler);
                        topic.updateServerData();
                        //----------
                    }
                    else
                    {
                        if (hasItem)
                        {
                            var confirmDialog = Relay.ConfirmDialog(
                                "All content will be deleted.",
                                "Are you sure you want to continue?",
                                "Yes",
                                "No"
                            );
                            confirmDialog.setOkEvent(function ()
                            {
                                //----------
                                topic.model.IsNullReport = true;
                                topic.addChangeEventLisetner(_this.config.changeHandler);
                                topic.updateServerData();
                                _this.update();
                                //----------
                            }.bind(this));
                            confirmDialog.open();
                        } else
                        {
                            topic.model.IsNullReport = true;
                            _this.update();
                            topic.addChangeEventLisetner(_this.config.changeHandler);
                            topic.updateServerData();
                        }
                    }
                });
            }
            //----------
            return true;
        }

        return false;
    };
    NullReportBtn.prototype.update = function ()
    {

        if (this.control)
        {
            if (this.topic.model.IsNullReport)
            {
                if (this.filterType === 'HandoverItems')
                {
                    $(this.topic.topicElement).remove();
                }
                else
                {
                    this.control.removeClass('nr-inactive-btn');
                    this.control.addClass('nr-active-btn');
                    $(this.topic.topicElement).addClass('tr-grey-text');
                }
            }
            else
            {
                if (this.filterType === 'NrItems')
                {
                    $(this.topic.topicElement).remove();
                }
                else
                {
                    this.control.removeClass('nr-active-btn');
                    this.control.addClass('nr-inactive-btn');
                    $(this.topic.topicElement).removeClass('tr-grey-text');
                }
            }
        }
    };
    //===============================================================================================
    if (window.Relay.Topic)
    {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.NullReportBtn = function (config)
        {
            return new NullReportBtn(config);
        };
    }
    else
    {
        console.error("Null Report Button control cannot work without Topic control.");
    }
    //===============================================================================================
})(window);