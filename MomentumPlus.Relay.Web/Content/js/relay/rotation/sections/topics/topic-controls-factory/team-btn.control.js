﻿; (function (window) {
    //===============================================================================================
    //  config {
    //      readOnly: false, 
    //      forDraftTask: (true)
    //  }
    //================================== Team Button ================================================
    var dataAttributeName = "topic-val";
    //--------------------------------------
    function TeamBtn(config) {
        this.config = config || { forDraftTask: false };
        this.topic = null;
        this.control = null;
    }
    TeamBtn.prototype.initialize = function (topic) {
        var _this = this;
        this.topic = topic;
        var control = $('.team-btn', topic.topicElement).first();
        if (control.length && control[0].hasAttribute(dataAttributeName)) {
            this.control = control;
            //----------
            topic.model.TeammateId = control.attr(dataAttributeName);
            topic.model.OwnerId = control.attr("owner-id");
            topic.model.Status = control.attr("status");
            var rotationMode = control.attr("data-rotation-mode") === "False";
            //----------
            if (!this.config.readOnly && !rotationMode) {
                if (this.config.forDraftTask) {
                    control.click(function () {
                        if (!topic.model.IsNullReport) {
                            var chooseTeammateDialog = Relay.ChooseTeammateDialog({
                                userId: topic.model.OwnerId,
                                selectedUserId: topic.model.TeammateId,
                                showOnSite: true,
                                showOffSite: topic.model.Status !== "Now"
                            });
                            chooseTeammateDialog.setChooseEvent(function (teammateId) {
                                topic.model.TeammateId = teammateId;
                                topic.updateServerData();
                            });
                            chooseTeammateDialog.open();
                        }
                    });
                }
            }
            //----------
            return true;
        }

        return false;
    };
    TeamBtn.prototype.update = function ()
    {
        if (this.config.forDraftTask)
        {
            if (this.control.length)
            {
                $("img", this.control).attr("src", "/Media/GetAvatarForEmployee?employeeId=" + this.topic.model.TeammateId + "&width=100&height=100");
            }
        } 
    };
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.TeamBtn = function (config) {
            return new TeamBtn(config);
        };
    }
    else {
        console.error("Team Button control cannot work without Topic control.");
    }
    //===============================================================================================
})(window);