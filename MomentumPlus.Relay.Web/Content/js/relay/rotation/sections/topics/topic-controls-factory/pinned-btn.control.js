﻿; (function (window) {
    //===============================================================================================
    //  config {
    //      readOnly: false
    //      changeHandler: (handler)
    //  }
    //=================================== Pinned Button =============================================
    var dataAttributeName = "topic-val";
    //--------------------------------------
    function PinnedBtn(config) {
        this.config = config || {};
        this.topic = null;
        this.control = null;
    }
    PinnedBtn.prototype.initialize = function (topic) {
        var _this = this;
        this.topic = topic;
        var control = $('.note-btn', topic.topicElement).first();

        if (control.length !== 0 && control[0].hasAttribute(dataAttributeName)) {
            //----------
            this.control = control;
            topic.model.IsPinned = control.attr(dataAttributeName) === "true";
            //----------
            if (!this.config.readOnly) {
                control.click(function () {
                    if (!topic.model.IsNullReport) {

                        topic.model.IsPinned = !topic.model.IsPinned;
                        _this.update();

                        topic.addChangeEventLisetner(_this.config.changeHandler);
                        topic.updateServerData();
                    }
                });
            }
            //----------
            return true;
        }

        return false;
    };
    PinnedBtn.prototype.update = function () {
        if (this.control) {
            if (this.topic.model.IsPinned) {
                //-- Rotation builder classes ---
                this.control.removeClass('note-grey-btn');
                this.control.addClass('note-active-btn');
                //-- Shift builder classes ------
                this.control.removeClass('pin-grey-btn');
                this.control.addClass('pin-active-btn');
            }
            else {
                //-- Rotation builder classes ---
                this.control.removeClass('note-active-btn');
                this.control.addClass('note-grey-btn');
                //-- Shift builder classes ------
                this.control.removeClass('pin-active-btn');
                this.control.addClass('pin-grey-btn');
            }
        }
    };
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.PinnedBtn = function (config) {
            return new PinnedBtn(config);
        };
    }
    else {
        console.error("Pinned Button control cannot work without Topic control.");
    }
    //===============================================================================================
})(window);