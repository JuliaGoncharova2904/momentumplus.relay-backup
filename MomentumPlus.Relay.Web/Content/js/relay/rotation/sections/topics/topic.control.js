﻿; (function () {
    //===============================================================================================
    //  config {
    //      updateUrl: (update url)
    //      reloadUrl: (reload Utl)
    //  }
    //=================================== Topic Control =============================================
    function Topic(topicElement, config) {
        this.config = config || {};
        this.topicElement = topicElement;
        this.IsExpanded = false;
        this.controls = [];
        this.model = {
            Id: $('#Id', topicElement).val()
        };

        this.autoCollapse = true;
        this.changeEvents = [];
        this.reloadEvents = [];
    }
    Topic.prototype.autoCollapseOn = function () {
        this.autoCollapse = true;
    };
    Topic.prototype.autoCollapseOff = function () {
        this.autoCollapse = false;
        Topic_RemoveAutoCollapseEvent.call(this);
    };
    Topic.prototype.AddControl = function (control) {
        if (control.initialize(this)) {
            this.controls.push(control);
        }
    }
    Topic.prototype.updateView = function () {
        for (var i = 0; i < this.controls.length; ++i) {
            this.controls[i].update();
        }
    }
    Topic.prototype.expand = function () {
        if (!this.model.IsNullReport) {
            $(this.topicElement).addClass('edit-tr');
            if (this.autoCollapse) Topic_SetAutoCollapseEvent.call(this);
            this.IsExpanded = true;
        }
    };
    Topic.prototype.collapse = function () {
        if (this.autoCollapse) Topic_RemoveAutoCollapseEvent.call(this);
        $(this.topicElement).removeClass('edit-tr');
        this.IsExpanded = false;
    };
    Topic.prototype.enable = function () {
        $(this.topicElement).removeAttr("style");
    };
    Topic.prototype.disable = function () {
        $(this.topicElement).attr("style", "pointer-events:none;");
    };
    Topic.prototype.updateServerData =  function (e) {
        var _this = this;

        if (!this.config.updateUrl) {
            console.error("Update URL was not initialized for topic: " + this.model.Id);
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function ()
            {
                location.reload();
            });
            errorDialog.open();
            return;
        }

        $.ajax({
            url: _this.config.updateUrl,
            method: "POST",
            data: _this.model
        })
        .done(function (data) {
            _this.model = data;
            _this.updateView();
            Topic_ExecChangeHandlers.call(_this);
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    Topic.prototype.reloadServerData = $.debounce(300, function () {
        var _this = this;

        if (!this.config.reloadUrl) {
            console.error("Reload URL was not initialized for topic: " + this.model.Id);
            return;
        }

        $.ajax({
            url: _this.config.reloadUrl,
            method: "POST",
            data: { topicId: _this.model.Id }
        })
        .done(function (data) {
            _this.model = data;
            _this.updateView();
            Topic_ExecReloadHandlers.call(_this);
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    });
    Topic.prototype.addChangeEventLisetner = function (changeHandler) {
        if (this.changeEvents.indexOf(changeHandler) === -1)
            this.changeEvents.push(changeHandler);
    };
    Topic.prototype.addReloadEventLisetner = function (reloadHandler)
    {
        if (this.reloadEvents.indexOf(reloadHandler) === -1)
            this.reloadEvents.push(reloadHandler);
    };
    function Topic_ExecChangeHandlers() {
        for (var i = 0; i < this.changeEvents.length; ++i) {
            if (this.changeEvents[i])
                this.changeEvents[i](this.model);
        }

        this.changeEvents = [];
    }
    function Topic_ExecReloadHandlers() {
        for (var i = 0; i < this.reloadEvents.length; ++i) {
            if (this.reloadEvents[i])
                this.reloadEvents[i](this.model);
        }

        this.reloadEvents = [];
    }
    function Topic_SetAutoCollapseEvent() {
        var _this = this;

        if (this.collapseHandler)
            $("#rootContent").unbind("click", this.collapseHandler);

        this.collapseHandler = function (event) {
            if (!$.contains(_this.topicElement, event.target)) {
                _this.collapse();
            }
        };

        setTimeout(function () {
            $("#rootContent").bind("click", _this.collapseHandler);
        }, 10);
    }
    function Topic_RemoveAutoCollapseEvent() {
        $("#rootContent").unbind("click", this.collapseHandler);
    }
    //===============================================================================================
        window.Relay = window.Relay || {};

        window.Relay.Topic = function (topicElement, config) {
            return new Topic(topicElement, config);
        };
    //===============================================================================================
})();