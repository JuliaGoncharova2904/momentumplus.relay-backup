﻿; (function (window) {
    "use strict";
    //===============================================================================================
    // config {
    //   url: (action url),
    //   destId: (topic destenation id),
    //   topicName: (name of topic),
    //   warningMessage: ( warning )
    // }
    //=============================== Add inline topic module =======================================
    function AddInlineTopic(section, config) {
        this.config = config;
        this.section = section;

        this.addBtn = null;
        this.tbody = null;
        this.inlineElement = null;

        this.openHandler = null;
        this.saveHandler = null;
        this.cancelHandler = null;
        this.closeHandler = null;

        this.init();
    }
    AddInlineTopic.prototype.isOpenedInlineForm = false;
    AddInlineTopic.prototype.init = function () {
        this.addBtn = $(".add-item-btn", this.section).first();
        this.tbody = $("tbody", this.section).first();

        this.addBtn.click(this.openForm.bind(this));
    };
    AddInlineTopic.prototype.openForm = function () {
        if (AddInlineTopic.prototype.isOpenedInlineForm)
            return;

        AddInlineTopic.prototype.isOpenedInlineForm = true;

        var _this = this;
        this.addBtn.attr("disabled", "");
        if (this.openHandler) this.openHandler();
        var animationTimeout = setTimeout(function () {
            $('#loading-animation').show();
        }, 200);

        $.ajax({
            url: _this.config.url,
            method: "GET",
            data: { destId: _this.config.destId }
        })
        .done(function (data) {
            _this.inlineElement = $(data);
            _this.initView();
            _this.addBtn.parent().hide();
            _this.tbody.append(_this.inlineElement);
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
            AddInlineTopic.prototype.isOpenedInlineForm = false;
            if (_this.closeHandler) _this.closeHandler();
        })
        .always(function() {
            _this.addBtn.removeAttr("disabled");
            clearTimeout(animationTimeout);
            $('#loading-animation').hide();
        });
    };
    AddInlineTopic.prototype.initView = function () {
        $(".select-parameter", this.inlineElement)
            .children()
            .first()
            .selectpicker({
                style: 'select-parametr',
                size: 4
            });

        $("select", this.inlineElement).bind("change", AddInlineTopic_UnlocInputFields.bind(this));
        AddInlineTopic_LocInputFields.call(this);

        this.loseFocusEvent = this.loseFocus.bind(this);
        $(window).bind("click", this.loseFocusEvent);
        $(".inline-add-btn", this.inlineElement).click(this.saveBtnHandler.bind(this));
        $(".remove-btn", this.inlineElement).click(this.removeBtnHandler.bind(this));
    };
    AddInlineTopic.prototype.saveBtnHandler = function () {
        var data = this.getDataFromFields();
        if (data) {
            $(window).unbind("click", this.loseFocusEvent);
            this.saveToServer(data);
        }
    };
    AddInlineTopic.prototype.removeBtnHandler = function () {
        $(window).unbind("click", this.loseFocusEvent);
        this.closeInlineElement();
    };
    AddInlineTopic.prototype.loseFocus = function (event) {
        if (!$.contains(this.inlineElement[0], event.target)) {
            //---------------------------------------------------
            $(window).unbind("click", this.loseFocusEvent);
            //---------------------------------------------------
            var _this = this;
            var data = this.getDataFromFields();
            //---------------------------------------------------
            if (!data) {
                var confirmDialog = Relay.ConfirmDialog(this.config.warningMessage, "", "Yes", "No");
                confirmDialog.setCancelEvent(function () {
                    setTimeout(function () {
                        $(window).bind("click", _this.loseFocusEvent);
                    }, 100);

                    if (_this.cancelHandler) _this.cancelHandler();

                    $("select.select-type", _this.inlineElement).first().focus();
                });
                confirmDialog.setOkEvent(this.closeInlineElement.bind(this));
                confirmDialog.open();
            }
            else {
                this.saveToServer(data);
            }
            //---------------------------------------------------
        }
    };
    AddInlineTopic.prototype.getDataFromFields = function () {
        //---------------------------------------------------
        var data = {};
        //---------------------------------------------------
        var selectTypeElement = $("select.select-type", this.inlineElement).first();
        var selectTypeKey = selectTypeElement.prop("name");
        data[selectTypeKey] = selectTypeElement.val();
        //---------------------------------------------------
        var addReferenceElement = $("input.add-reference", this.inlineElement).first();
        var addReferenceKey = addReferenceElement.prop("name");
        data[addReferenceKey] = addReferenceElement.val();
        //---------------------------------------------------
        var addNotesElement = $(".add-note", this.inlineElement).first();
        var addNotesKey = addNotesElement.prop("name");
        data[addNotesKey] = addNotesElement.val();
        //---------------------------------------------------
        $("input[type='hidden']", this.inlineElement).each(function () {
            data[$(this).attr("name")] = $(this).val();
        });
        //---------------------------------------------------
        return (data[selectTypeKey] && data[addReferenceKey]) ? data : null;
        //---------------------------------------------------
    };
    AddInlineTopic.prototype.closeInlineElement = function () {
        this.inlineElement.remove();
        this.addBtn.parent().show();
        AddInlineTopic.prototype.isOpenedInlineForm = false;
        if (this.closeHandler) this.closeHandler();
    };
    AddInlineTopic.prototype.validationErrorDialog = function (errorMessage) {
        var _this = this;
        var infoDialog = Relay.InfoDialog(errorMessage);
        infoDialog.setCloseEvent(function () {
            setTimeout(function () {
                $(window).bind("click", _this.loseFocusEvent);
            }, 100);
            $("input.add-reference", _this.inlineElement).first().focus();
        });
        infoDialog.open();
    };
    AddInlineTopic.prototype.saveToServer = function (data) {
        var _this = this;

        $.ajax({
            url: _this.config.url,
            method: "POST",
            data: data
        })
        .done(function (data) {
            if (!data) {
                _this.closeInlineElement();
                if (_this.saveHandler)
                    _this.saveHandler();
            }
            else {
                $(window).unbind("click", this.loseFocusEvent);
                _this.validationErrorDialog(data);
            }
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    AddInlineTopic.prototype.setOpenEvent = function (handler) {
        this.openHandler = handler;
    };
    AddInlineTopic.prototype.setSaveEvent = function (handler) {
        this.saveHandler = handler;
    };
    AddInlineTopic.prototype.setCancelEvent = function (handler) {
        this.cancelHandler = handler;
    };
    AddInlineTopic.prototype.setCloseEvent = function (handler) {
        this.closeHandler = handler;
    };
    function AddInlineTopic_UnlocInputFields() {
        $("input", this.inlineElement).removeAttr("disabled").removeAttr("style");
        $("textarea", this.inlineElement).removeAttr("disabled").removeAttr("style");
        $(".select-parameter .filter-option", this.inlineElement).addClass("text-blue");
    }
    function AddInlineTopic_LocInputFields() {
        $("input", this.inlineElement).attr("style", "background: #FFFFFF;").attr("disabled", "");
        $("textarea", this.inlineElement).attr("style", "background: #FFFFFF;").attr("disabled", "");
    }
    //===============================================================================================
    window.Relay = window.Relay || {};
    window.Relay.AddInlineTopic = function (section, config) {
        return new AddInlineTopic(section, config);
    }
    //===============================================================================================
})(window);