﻿; (function (window) {
    //===============================================================================================
    window.Relay = window.Relay || {};
    window.Relay.SectionFactory = window.Relay.SectionFactory || {};
    window.Relay.SectionFactory.Types = {
        Core: "Core",
        DailyNote: "Daily Notes",
        Project: "Projects",
        HSE: "Safety",
        Tasks: "Tasks",
        Team: "Team"
    };
    //===============================================================================================
})(window);