﻿(function () {
    "use strict";
    //===========================================================================
    var historySelectedRotationVarName = "HistorySelectedRotationId";
    //===========================================================================
    function HistorySubpanel(userId, controls) {
        this.userId = userId;
        this.rotationId = null;
        this.controls = controls;

        this.sections = [];

        this.reportPanel = null;
        this.historyPanelSelectedRotation = null;

        controls.documentBtn.prop('disabled', true);
        controls.qmePdfReportBtn.prop('disabled', true);
        controls.finaliseAllBtn.hide();
        controls.draftFromToBlock.hide();
        controls.expandCollapseAllBtn.hide();

        this.execHashParamsHandler = HistorySubpanel_ExecHashParams.bind(this);
        Relay.UrlHash.addChangeEvent(this.execHashParamsHandler);

        this.load();
    }
    HistorySubpanel.prototype.dispose = function () {
        Relay.UrlHash.removeChangeEvent(this.execHashParamsHandler);
        this.controls = null;
        this.sections = null;
    };
    HistorySubpanel.prototype.getRotationId = function () {
        var hashModel = Relay.UrlHash.getHashModel();

        return hashModel[historySelectedRotationVarName];
    };
    HistorySubpanel.prototype.load = function () {
        var _this = this;

        var hashModel = Relay.UrlHash.getHashModel();

        if (hashModel[historySelectedRotationVarName]) {
            this.controls.documentBtn.prop('disabled', false);
            this.controls.qmePdfReportBtn.prop('disabled', false);
        }
        //-----------------------------------
        $('#loading-animation').show();
        //-----------------------------------
        $.ajax({
            url: "/RotationReportBuilder/HistoryPanel",
            method: "GET",
            data: {
                userId: _this.userId,
                selectedRotationId: hashModel[historySelectedRotationVarName]
            }
        })
        .done(function (result) {
            //--------------
            var div = document.createElement(div);
            div.innerHTML = result;
            //--------------
            _this.reportPanel = $(".report-panel", div);
            _this.controls.panelContainer.empty();
            _this.controls.panelContainer.html(_this.reportPanel);

            _this.historyPanelSelectedRotation = $(".history-panel-selected-rotation", _this.reportPanel).first();
            HistorySubpanel_InitRotationsSwiper($(".history-container", _this.reportPanel));

            if (_this.historyPanelSelectedRotation.length) {
                var rotationId = _this.historyPanelSelectedRotation.attr("rotation-id");
                _this.controls.periodBtn.html(_this.historyPanelSelectedRotation.attr("rotation-period"));
                HistorySubpanel_Init.call(_this, rotationId);
                _this.controls.shareReportBtnController.build(rotationId, "Swing");
            }
            else {
                _this.controls.periodBtn.html("...");
                _this.controls.shareReportBtnController.destroy();
            }
            //--------------
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
        .always(function () {
            $('#loading-animation').hide();
            //$.scrollTo('#safetySection', 800);
        });
        //-----------------------------------
    }
    HistorySubpanel.prototype.reloadSection = function (sectionType) {
        HistorySubpanel_ReloadHandler.call(this, sectionType);
    };

    function HistorySubpanel_ExecHashParams(model, modelChangesMap) {
        if (modelChangesMap[historySelectedRotationVarName]) {

            var rotationsSwiperContainer = $(".history-container", this.reportPanel)
            $(".swiper-slide", rotationsSwiperContainer).removeClass("active-line");
            $(".swiper-slide[rotation-id='" + model[historySelectedRotationVarName] + "']", rotationsSwiperContainer).addClass("active-line");

            HistorySubpanel_LoadForSelectedRotation.call(this, model);

            if (model[historySelectedRotationVarName]) {
                this.controls.documentBtn.prop('disabled', false);
                this.controls.qmePdfReportBtn.prop('disabled', false);
            }
        }
    }

    function HistorySubpanel_LoadForSelectedRotation(hashModel) {
        var _this = this;

        //-----------------------------------
        $('#loading-animation').show();
        //-----------------------------------
        $.ajax({
            url: "/RotationReportBuilder/HistoryPanelSelectedRotation",
            method: "GET",
            data: { selectedRotationId: hashModel[historySelectedRotationVarName] }
        })
        .done(function (result) {
            //--------------
            var div = document.createElement(div);
            div.innerHTML = result;
            //--------------
            _this.historyPanelSelectedRotation.remove();
            _this.historyPanelSelectedRotation = $(".history-panel-selected-rotation", div);
            _this.reportPanel.append(_this.historyPanelSelectedRotation);
            if (_this.historyPanelSelectedRotation.length) {
                var rotationId = _this.historyPanelSelectedRotation.attr("rotation-id");
                _this.controls.periodBtn.html(_this.historyPanelSelectedRotation.attr("rotation-period"));
                HistorySubpanel_Init.call(_this, rotationId);
                _this.controls.shareReportBtnController.build(rotationId, "Swing");
            }
            else {
                _this.controls.periodBtn.html("...");
                _this.controls.shareReportBtnController.destroy();
            }
            //--------------
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
        .always(function () {
            $('#loading-animation').hide();
        });
        //-----------------------------------
    }

    function HistorySubpanel_InitRotationsSwiper(rotationsSwiperContainer) {

        var swiper = new Swiper($(".history-swiper", rotationsSwiperContainer), {
            direction: 'vertical',
            height: 40,
            nextButton: $(".history-swiper-next", rotationsSwiperContainer),
            prevButton: $(".history-swiper-prev", rotationsSwiperContainer)
        });

        var hashModel = Relay.UrlHash.getHashModel();

        $(".swiper-slide", rotationsSwiperContainer).each(function () {
            if (hashModel[historySelectedRotationVarName] == $(this).attr("rotation-id")) {
                $(this).addClass("active-line");
                swiper.slideTo($(this).index(), 0, true);
            }
        });

        $(".swiper-slide", rotationsSwiperContainer).click(function () {
            var hashModel = Relay.UrlHash.getHashModel();
            hashModel[historySelectedRotationVarName] = $(this).attr("rotation-id");
            Relay.UrlHash.updateHashModel(hashModel);
        });
    }

    function HistorySubpanel_Init(rotationId) {
        this.sections = [];

        this.sections.push(Relay.SectionFactory.HistoryCoreSection($("#coreSection"), rotationId));
        this.sections.push(Relay.SectionFactory.HistoryDailyNotesSection($("#dailyNotesSection"), rotationId));
        this.sections.push(Relay.SectionFactory.HistoryProjectsSection($("#projectsSection"), rotationId));
        this.sections.push(Relay.SectionFactory.HistorySafetySection($("#safetySection"), rotationId));
        this.sections.push(Relay.SectionFactory.HistoryTeamSection($("#teamSection"), rotationId));

        var taskSection = Relay.SectionFactory.HistoryTasksSection($("#tasksSection"), rotationId);
        taskSection.reloadExternalSection = HistorySubpanel_ReloadHandler.bind(this);

        this.sections.push(taskSection);
    }
    function HistorySubpanel_ReloadHandler(sectionType) {
        for (var i = 0; i < this.sections.length; ++i) {
            if (this.sections[i].type === sectionType)
                this.sections[i].reload();
        }
    }
    //===========================================================================
    window.Relay = window.Relay || {};
    window.Relay.HistorySubpanel = function (userId, controls) {
        return new HistorySubpanel(userId, controls);
    };
    //===========================================================================
})();