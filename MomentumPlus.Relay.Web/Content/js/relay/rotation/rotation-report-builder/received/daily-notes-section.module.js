﻿; (function (window) {
    //===============================================================================================
    //  sectionElement: (section div)
    //  rotationId: (rotation id)
    //==============================  Received Daily Notes Section ==================================
    function ReceivedDailyNotesSection(sectionElement, rotationId) {
        //----------------------
        this.type = Relay.SectionFactory.Types.DailyNote;
        var controller = "/DailyNotesSection";
        this.sectionUrl = controller + "/Received?rotationId=" + rotationId;
        this.updateTopicUrl = controller + "/UpdateTopic";
        this.reloadTopicUrl = controller + "/ReloadTopic";
        //----------------------
        this.topics;
        this.isExpanded = false;
        this.rotationId = rotationId;
        this.sectionElement = sectionElement;
        //----------------------
        ReceivedDailyNotesSection_Initialise.call(this);
    }
    ReceivedDailyNotesSection.prototype.reload = function () {
        var _this = this;

        $.ajax({
            url: _this.sectionUrl,
            method: "GET"
        })
        .done(function (newContent) {
            //--- update content -----
            _this.sectionElement.empty();
            $(newContent).children().each(function () {
                _this.sectionElement.append(this);
            });
            //---- Init section ------
            ReceivedDailyNotesSection_Initialise.call(_this)
            //------------------------
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    ReceivedDailyNotesSection.prototype.expand = function () {
        this.isExpanded = true;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].autoCollapseOff();
            this.topics[i].expand();
        }
    };
    ReceivedDailyNotesSection.prototype.collapse = function () {
        this.isExpanded = false;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].collapse();
            this.topics[i].autoCollapseOn();
        }
    };
    function ReceivedDailyNotesSection_Initialise() {
        var _this = this;
        this.topics = [];
        //-------
        $("tbody tr", this.sectionElement).each(function () {
            var topic = ReceivedDailyNotesSection_BuildTopic.call(_this, this);

            if (_this.isExpanded) {
                topic.autoCollapseOff();
                topic.expand();
            }

            _this.topics.push(topic);
        })
    }
    function ReceivedDailyNotesSection_BuildTopic(trElement) {
        var _this = this;

        var topic = Relay.Topic(trElement, {
            updateUrl: this.updateTopicUrl,
            reloadUrl: this.reloadTopicUrl
        });

        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Date",
            elementClass: ".second-td"
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NotesField({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.FinaliseBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.ManagerCommentsBtn({
            forTopic: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TeamBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.TasksBtn({
            dialogType: Relay.TasksListDialog.Types.Received
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
            dialogType: Relay.AttachmentsDialog.Types.Received,
            container: Relay.AttachmentsDialog.Containers.Topic
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
            dialogType: Relay.VoiceMessagesDialog.Types.Received,
            container: Relay.VoiceMessagesDialog.Containers.Topic
        }));

        topic.updateView();

        return topic;
    }
    //===============================================================================================
    if (window.Relay.SectionFactory) {
        window.Relay.SectionFactory.ReceivedDailyNotesSection = function (sectionElement, rotationId) {
            return new ReceivedDailyNotesSection(sectionElement, rotationId);
        };
    }
    else {
        console.error("Received Daily Notes Section cannot work without SectionFactory.");
    }
    //===============================================================================================
})(window);