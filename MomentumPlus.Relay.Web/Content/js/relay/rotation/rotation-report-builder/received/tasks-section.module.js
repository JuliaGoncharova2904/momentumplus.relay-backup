﻿; (function (window) {
    //===============================================================================================
    //  sectionElement: (section div)
    //  rotationId: (rotation id)
    //==============================  Received Tasks Section ========================================
    //------------------- Name Column Handler -----------------------------
    function NameCompleteClickHandler(model, reload, update) {
        if (!model.IsNullReport) {
            var tasksListDialog = Relay.FormDialog({
                url: "/Tasks/CompleteTaskDialog",
                method: "GET",
                data: { TaskId: model.Id }
            });
            tasksListDialog.setCloseEvent(reload);
            tasksListDialog.open();
        }
    }
    //---------------------------------------------------------------------
    function ReceivedTasksSection(sectionElement, rotationId) {
        //----------------------
        this.type = Relay.SectionFactory.Types.Tasks;
        var controller = "/TasksSection";
        this.sectionUrl = controller + "/Received?rotationId=" + rotationId;
        this.updateTopicUrl = controller + "/UpdateTopic";
        this.reloadTopicUrl = controller + "/ReloadTopic";
        //----------------------
        this.topics;
        this.isExpanded = false;
        this.rotationId = rotationId;
        this.sectionElement = sectionElement;
        this.reloadExternalSection = null;
        //----------------------
        ReceivedTasksSection_Initialise.call(this);
    }
    ReceivedTasksSection.prototype.reload = function () {
        var _this = this;

        $.ajax({
            url: _this.sectionUrl,
            method: "GET"
        })
        .done(function (newContent) {
            //--- update content -----
            _this.sectionElement.empty();
            $(newContent).children().each(function () {
                _this.sectionElement.append(this);
            });
            //---- Init section ------
                ReceivedTasksSection_Initialise.call(_this);
                //------------------------
            })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    ReceivedTasksSection.prototype.expand = function () {
        this.isExpanded = true;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].autoCollapseOff();
            this.topics[i].expand();
        }
    };
    ReceivedTasksSection.prototype.collapse = function () {
        this.isExpanded = false;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].collapse();
            this.topics[i].autoCollapseOn();
        }
    };
    function ReceivedTasksSection_Initialise() {
        var _this = this;
        this.topics = [];
        //-------
        $("tbody tr", this.sectionElement).each(function() {
            var topic = ReceivedTasksSection_BuildTopic.call(_this, this);

            if (_this.isExpanded) {
                topic.autoCollapseOff();
                topic.expand();
            }

            _this.topics.push(topic);
        });
    }
    function ReceivedTasksSection_BuildTopic(trElement) {
        var topic = Relay.Topic(trElement, {
            updateUrl: this.updateTopicUrl,
            reloadUrl: this.reloadTopicUrl
        });

        var isComplete = $(trElement).attr("is-complete") === "True";

        if (!isComplete)
        {
            $(".third-td", trElement).webuiPopover({
                hideEmpty: true,
                placement: "bottom-right",
                style: 'rotation-menu-popover',
                trigger: 'click',
                multi: false,
                dismissible: true,
                type: "async",
                cache: false,
                width: '83',
                offsetTop: -1,
                offsetLeft: -15,
                url: "/TasksSection/GetEditCompleteMenuPopover?taskId=" + $("#Id", trElement).val()
            });
        }

        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Location",
            elementClass: ".second-td"
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Name",
            elementClass: ".third-td",
            clickHandler: isComplete ? NameCompleteClickHandler : null
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NotesField({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.PriorityField());
        topic.AddControl(Relay.Topic.ControlsFactory.FinaliseBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "CompleteStatus",
            elementClass: ".complite-status"
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.ManagerCommentsBtn({
            forTask: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TeamBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
            dialogType: Relay.AttachmentsDialog.Types.Received,
            container: Relay.AttachmentsDialog.Containers.Task
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
            dialogType: Relay.VoiceMessagesDialog.Types.Received,
            container: Relay.VoiceMessagesDialog.Containers.Task
        }));

        topic.updateView();

        return topic;
    }
    //===============================================================================================
    if (window.Relay.SectionFactory) {
        window.Relay.SectionFactory.ReceivedTasksSection = function (sectionElement, rotationId) {
            return new ReceivedTasksSection(sectionElement, rotationId);
        };
    }
    else {
        console.error("Received Tasks Section cannot work without SectionFactory.");
    }
    //===============================================================================================
})(window);