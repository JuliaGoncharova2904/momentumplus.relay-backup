﻿(function () {
    "use strict";
    //===========================================================================
    var receivedReceivedRotationVarName = "ReceivedReceivedRotationId";
    var receivedSelectedRotationVarName = "ReceivedSelectedRotationId";
    //===========================================================================
    function ReceivedSubpanel(userId, controls) {
        this.userId = userId;
        this.rotationId = null;
        this.controls = controls;

        this.sections = [];
        this.IsExpanded = false;

        this.reportPanel = null;
        this.receivedPanelSelectedRotation = null;
        this.receivedPanelReceivedRotation = null;

        controls.documentBtn.prop('disabled', true);
        controls.qmePdfReportBtn.prop('disabled', false);
        controls.finaliseAllBtn.hide();
        controls.draftFromToBlock.hide();
        controls.shareReportBtnController.destroy();

        controls.expandCollapseAllBtn.html("Expand All");
        controls.expandCollapseAllBtn.show();
        this.expandCollapseHandler = ReceivedSubpanel_ExpandCollapseAllBtnHandler.bind(this);
        controls.expandCollapseAllBtn[0].addEventListener("click", this.expandCollapseHandler, false);

        this.execHashParamsHandler = ReceivedSubpanel_ExecHashParams.bind(this);
        Relay.UrlHash.addChangeEvent(this.execHashParamsHandler);

        this.load();
    }
    ReceivedSubpanel.prototype.dispose = function () {
        Relay.UrlHash.removeChangeEvent(this.execHashParamsHandler);
        this.controls.expandCollapseAllBtn[0].removeEventListener('click', this.expandCollapseHandler, false);
        this.controls = null;
        this.sections = null;
    };
    ReceivedSubpanel.prototype.getRotationId = function () {
        var hashModel = Relay.UrlHash.getHashModel();
        return hashModel[receivedSelectedRotationVarName];
    };
    ReceivedSubpanel.prototype.getReceivedRotationId = function () {
        var hashModel = Relay.UrlHash.getHashModel();
        return hashModel[receivedReceivedRotationVarName];
    };
    ReceivedSubpanel.prototype.load = function () {
        var _this = this;

        var hashModel = Relay.UrlHash.getHashModel();
        if (hashModel[receivedSelectedRotationVarName]) {
            this.controls.documentBtn.prop('disabled', false);
            this.controls.qmePdfReportBtn.prop('disabled', false);
        }
        //-----------------------------------
        $('#loading-animation').show();
        //-----------------------------------
        $.ajax({
            url: "/RotationReportBuilder/ReceivedPanel",
            method: "GET",
            data: {
                userId: _this.userId,
                selectedRotationId: hashModel[receivedSelectedRotationVarName],
                receivedRotationId: hashModel[receivedReceivedRotationVarName]
            }
        })
        .done(function (result) {
            //--------------
            var div = document.createElement(div);
            div.innerHTML = result;
            //--------------
            _this.reportPanel = $(".report-panel", div);
            _this.controls.panelContainer.empty();
            _this.controls.panelContainer.html(_this.reportPanel);
            _this.receivedPanelSelectedRotation = $(".received-panel-selected-rotation", _this.reportPanel).first();
            _this.receivedPanelReceivedRotation = $(".received-panel-received-rotation", _this.reportPanel).first();
            ReceivedSubpanel_InitRotationsSwiper($(".received-history-container", _this.reportPanel));
            ReceivedSubpanel_InitReceivedRotationsSwiper($(".received-container", _this.reportPanel));
            if (_this.receivedPanelReceivedRotation.length) {
                _this.controls.periodBtn.html(_this.receivedPanelReceivedRotation.attr("rotation-period"));
                ReceivedSubpanel_Init.call(_this, _this.receivedPanelReceivedRotation.attr("rotation-id"));
            }
            else {
                _this.controls.periodBtn.html("...");
            }
            //--------------
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
        .always(function () {
            $('#loading-animation').hide();
            //$.scrollTo('#safetySection', 800);
        });
        //-----------------------------------
    }
    ReceivedSubpanel.prototype.reloadSection = function (sectionType) { };

    function ReceivedSubpanel_ExecHashParams(model, modelChangesMap) {
        if (modelChangesMap[receivedSelectedRotationVarName]) {

            var rotationsSwiperContainer = $(".received-history-container", this.reportPanel)
            $(".swiper-slide", rotationsSwiperContainer).removeClass("active-line");
            $(".swiper-slide[rotation-id='" + model[receivedSelectedRotationVarName] + "']", rotationsSwiperContainer).addClass("active-line");

            ReceivedSubpanel_LoadForSelectedRotation.call(this, model);

            this.IsExpanded = false;
            this.controls.expandCollapseAllBtn.html("Expand All");

            if (model[receivedSelectedRotationVarName]) {
                this.controls.documentBtn.prop('disabled', false);
                this.controls.qmePdfReportBtn.prop('disabled', false);
            }
        }
        if (modelChangesMap[receivedReceivedRotationVarName]) {

            var receivedRotationsSwiperContainer = $(".received-container", this.reportPanel);
            $(".swiper-slide", receivedRotationsSwiperContainer).removeClass("active-line");
            $(".swiper-slide[rotation-id='" + model[receivedReceivedRotationVarName] + "']", receivedRotationsSwiperContainer).addClass("active-line");

            ReceivedSubpanel_LoadForReceivedRotation.call(this, model);

            this.IsExpanded = false;
            this.controls.expandCollapseAllBtn.html("Expand All");
        }
    }

    function ReceivedSubpanel_LoadForSelectedRotation(hashModel) {
        var _this = this;

        //-----------------------------------
        $('#loading-animation').show();
        //-----------------------------------
        $.ajax({
            url: "/RotationReportBuilder/ReceivedPanelForSelectedRotation",
            method: "GET",
            data: { selectedRotationId: hashModel[receivedSelectedRotationVarName] }
        })
        .done(function (result) {
            //--------------
            var div = document.createElement(div);
            div.innerHTML = result;
            //--------------
            _this.receivedPanelSelectedRotation.remove();
            _this.receivedPanelSelectedRotation = $(".received-panel-selected-rotation", div);
            _this.reportPanel.append(_this.receivedPanelSelectedRotation);
            _this.receivedPanelReceivedRotation = $(".received-panel-received-rotation", _this.reportPanel).first();
            ReceivedSubpanel_InitReceivedRotationsSwiper($(".received-container", _this.reportPanel));
            if (_this.receivedPanelReceivedRotation.length) {
                _this.controls.periodBtn.html(_this.receivedPanelReceivedRotation.attr("rotation-period"));
                ReceivedSubpanel_Init.call(_this, _this.receivedPanelReceivedRotation.attr("rotation-id"));
            }
            else {
                _this.controls.periodBtn.html("...");
            }
            //--------------
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
        .always(function () {
            $('#loading-animation').hide();
            //$.scrollTo('#safetySection', 800);
        });
        //-----------------------------------
    }

    function ReceivedSubpanel_LoadForReceivedRotation(hashModel) {
        var _this = this;

        //-----------------------------------
        $('#loading-animation').show();
        //-----------------------------------
        $.ajax({
            url: "/RotationReportBuilder/ReceivedPanelForReceivedRotation",
            method: "GET",
            data: {
                selectedRotationId: hashModel[receivedSelectedRotationVarName],
                receivedRotationId: hashModel[receivedReceivedRotationVarName]
            }
        })
        .done(function (result) {
            //--------------
            var div = document.createElement(div);
            div.innerHTML = result;
            //--------------
            _this.receivedPanelReceivedRotation.remove();
            _this.receivedPanelReceivedRotation = $(".received-panel-received-rotation", div);
            _this.receivedPanelSelectedRotation.append(_this.receivedPanelReceivedRotation);
            if (_this.receivedPanelReceivedRotation.length) {
                _this.controls.periodBtn.html(_this.receivedPanelReceivedRotation.attr("rotation-period"));
                ReceivedSubpanel_Init.call(_this, _this.receivedPanelReceivedRotation.attr("rotation-id"));
            }
            else {
                _this.controls.periodBtn.html("...");
            }
            //--------------
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
        .always(function () {
            $('#loading-animation').hide();
            //$.scrollTo('#safetySection', 800);
        });
        //-----------------------------------
    }

    function ReceivedSubpanel_InitRotationsSwiper(rotationsSwiperContainer) {

        var swiper = new Swiper($(".swiper", rotationsSwiperContainer), {
            direction: 'vertical',
            height: 40,
            nextButton: $(".swiper-next", rotationsSwiperContainer),
            prevButton: $(".swiper-prev", rotationsSwiperContainer)
        });

        var hashModel = Relay.UrlHash.getHashModel();
        $(".swiper-slide", rotationsSwiperContainer).each(function () {
            if (hashModel[receivedSelectedRotationVarName] == $(this).attr("rotation-id")) {
                $(this).addClass("active-line");
                swiper.slideTo($(this).index(), 0, true);
            }
        });

        $(".swiper-slide", rotationsSwiperContainer).click(function () {
            var hashModel = Relay.UrlHash.getHashModel();
            hashModel[receivedSelectedRotationVarName] = $(this).attr("rotation-id");
            hashModel[receivedReceivedRotationVarName] = null;
            Relay.UrlHash.updateHashModel(hashModel);
        });
    }

    function ReceivedSubpanel_InitReceivedRotationsSwiper(receivedRotationsSwiperContainer) {

        var swiper = new Swiper($(".receivedSwiper", receivedRotationsSwiperContainer), {
            direction: 'vertical',
            height: 40,
            nextButton: $(".receivedSwiper-next", receivedRotationsSwiperContainer),
            prevButton: $(".receivedSwiper-prev", receivedRotationsSwiperContainer)
        });

        var hashModel = Relay.UrlHash.getHashModel();
        $(".swiper-slide", receivedRotationsSwiperContainer).each(function () {
            if (hashModel[receivedReceivedRotationVarName] == $(this).attr("rotation-id")) {
                $(this).addClass("active-line");
                swiper.slideTo($(this).index(), 0, true);
            }
        });

        $(".swiper-slide", receivedRotationsSwiperContainer).click(function () {
            var hashModel = Relay.UrlHash.getHashModel();
            var rotationId = $(this).attr("rotation-id");
            if (hashModel[receivedReceivedRotationVarName] == rotationId) {
                hashModel[receivedReceivedRotationVarName] = null;
            }
            else {
                hashModel[receivedReceivedRotationVarName] = rotationId;
            }
            Relay.UrlHash.updateHashModel(hashModel);
        });
    }

    function ReceivedSubpanel_ExpandCollapseAllBtnHandler() {
        if (!this.IsExpanded) {
            for (var i = 0; i < this.sections.length; ++i) {
                this.sections[i].expand();
            }

            this.IsExpanded = true;
            this.controls.expandCollapseAllBtn.html("Collapse All");
        }
        else {
            for (var i = 0; i < this.sections.length; ++i) {
                this.sections[i].collapse();
            }

            this.IsExpanded = false;
            this.controls.expandCollapseAllBtn.html("Expand All");
        }
    };

    function ReceivedSubpanel_Init(rotationId) {
        this.sections = [];

        this.sections.push(Relay.SectionFactory.ReceivedCoreSection($("#coreSection"), rotationId));
        this.sections.push(Relay.SectionFactory.ReceivedDailyNotesSection($("#dailyNotesSection"), rotationId));
        this.sections.push(Relay.SectionFactory.ReceivedProjectsSection($("#projectsSection"), rotationId));
        this.sections.push(Relay.SectionFactory.ReceivedSafetySection($("#safetySection"), rotationId));
        this.sections.push(Relay.SectionFactory.ReceivedTasksSection($("#tasksSection"), rotationId));
        this.sections.push(Relay.SectionFactory.ReceivedTeamSection($("#teamSection"), rotationId));
    }
    //===========================================================================
    window.Relay = window.Relay || {};
    window.Relay.ReceivedSubpanel = function (userId, controls) {
        return new ReceivedSubpanel(userId, controls);
    };
    //===========================================================================
})();