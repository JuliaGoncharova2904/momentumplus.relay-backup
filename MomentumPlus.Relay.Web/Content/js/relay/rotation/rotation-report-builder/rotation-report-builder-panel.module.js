﻿$(document).ready(function () {
    "use strict";
    //===========================================================================
    var reportsTypeVarName = "ReportsType";
    //--------------------------------
    var ReportesTypes = {
        Draft: "DraftPanel",
        Received: "ReceivedPanel",
        History: "HistoryPanel"
    };
    //===========================================================================
    function ReportBuilderPanel(panelElement) {
        //-------------------------
        var _this = this;
        //--------------
        this.panelElement = panelElement;
        this.userId = panelElement.attr("user-id");
        this.subpanelController = null;
        //-----------------------
        this.panelHeading = $(".panel-heading", panelElement).first();
        this.panelBody = $(".panel-body", panelElement).first();
        //-----------------------
        this.plusBtn = $(".plus-btn", panelElement).first();
        this.filterBtn = $(".filter-btn", this.panelElement).first();
        this.documentBtn = $(".document-btn", panelElement).first();
        this.qmePdfReportBtn = $(".qme-pdf-report", panelElement).first();
        //--------------
        this.draftBtn = $(".draft-btn", panelElement).first();
        this.receivedBtn = $(".received-btn", panelElement).first();
        this.historyBtn = $(".history-btn", panelElement).first();
        //-----------------------
        this.controls = {
            documentBtn: this.documentBtn,
            qmePdfReportBtn: this.qmePdfReportBtn,
            filterBtn: new Relay.RotationHandoverFilterBtn(this.filterBtn[0]),
            periodBtn: $(".period-btn", panelElement).first(),
            draftFromToBlock: $("#fromToBlock", panelElement).first(),
            finaliseAllBtn: $(".finalise-all-btn", panelElement).first(),
            expandCollapseAllBtn: $(".expand-all-btn", panelElement).first(),
            panelContainer: $("#reportBuilderData", panelElement),
            shareReportBtnController: new Relay.ShareReportBtn($(".share-report-block", panelElement))
        };
        //------- Init Btn --------
        this.qmePdfReportBtn.click(this.QMEReportBtnHandler.bind(this));
        this.documentBtn.click(this.DocumentBtnHandler.bind(this));
        //--------
        this.draftBtn.click(function () {
            var hashModel = Relay.UrlHash.getHashModel();
            hashModel[reportsTypeVarName] = ReportesTypes.Draft;
            Relay.UrlHash.updateHashModel(hashModel);
        });
        this.receivedBtn.click(function () {
            var hashModel = Relay.UrlHash.getHashModel();
            hashModel[reportsTypeVarName] = ReportesTypes.Received;
            Relay.UrlHash.updateHashModel(hashModel);
        });
        this.historyBtn.click(function () {
            var hashModel = Relay.UrlHash.getHashModel();
            hashModel[reportsTypeVarName] = ReportesTypes.History;
            Relay.UrlHash.updateHashModel(hashModel);
        });
        //----- Init tooltips -----
        $(this.documentBtn).tooltipster({
            debug: false
        });
        this.controls.draftFromToBlock.bind("DOMNodeInserted DOMNodeRemoved", function () {
            $(".tooltipster", this).tooltipster({
                debug: false
            });
        });
        this.controls.panelContainer.bind("DOMNodeInserted DOMNodeRemoved", function () {
            $(".tooltipster", this).tooltipster({
                debug: false
            });
        });
        //-------------------------
        Relay.UrlHash.addChangeEvent(ReportBuilderPanel_ExecHashParams.bind(this));
        //-------------------------
        var hashModel = Relay.UrlHash.getHashModel();
        if (!this.LoadPanel(hashModel[reportsTypeVarName])) {
            hashModel[reportsTypeVarName] = ReportesTypes.Draft;
            Relay.UrlHash.updateHashModel(hashModel);
        }
        //-------------------------
    }
    ReportBuilderPanel.prototype.enable = function () {
        this.panelHeading.removeAttr("style");
        this.panelBody.removeAttr("style");
    };
    ReportBuilderPanel.prototype.disable = function () {
        this.panelHeading.attr("style", "pointer-events:none;");
        this.panelBody.attr("style", "pointer-events:none;");
    }
    ReportBuilderPanel.prototype.reload = function () {
        if (this.subpanelController) {
            this.subpanelController.load();
        }
    }
    ReportBuilderPanel.prototype.LoadPanel = function (panelType) {
        switch (panelType) {
            case ReportesTypes.History:
                this.plusBtn.hide();
                this.filterBtn.hide();
                this.documentBtn.show();
                this.qmePdfReportBtn.show();
                this.historyBtn.parent().addClass('active');
                this.draftBtn.parent().removeClass("active");
                this.receivedBtn.parent().removeClass("active");
                ReportBuilderPanel_SetSubpanelController.call(this, Relay.HistorySubpanel(this.userId, this.controls));
                break;
            case ReportesTypes.Received:
                this.plusBtn.hide();
                this.filterBtn.hide();
                this.documentBtn.show();
                this.qmePdfReportBtn.show();
                this.receivedBtn.parent().addClass('active');
                this.draftBtn.parent().removeClass("active");
                this.historyBtn.parent().removeClass("active");
                ReportBuilderPanel_SetSubpanelController.call(this, Relay.ReceivedSubpanel(this.userId, this.controls));
                break;
            case ReportesTypes.Draft:
                this.plusBtn.show();
                this.filterBtn.show();
                this.qmePdfReportBtn.show();
                this.documentBtn.show();
                this.draftBtn.parent().addClass("active");
                this.receivedBtn.parent().removeClass('active');
                this.historyBtn.parent().removeClass("active");
                ReportBuilderPanel_SetSubpanelController.call(this, Relay.DraftSubpanel(this.userId, this.controls));
                break;
            default:
                return false;
        }

        return true;
    }
    function ReportBuilderPanel_SetSubpanelController(controller) {
        if (this.subpanelController) {
            this.subpanelController.dispose();
        }
        this.subpanelController = controller;
    }
    function ReportBuilderPanel_ExecHashParams(model, modelChangesMap) {
        if (modelChangesMap[reportsTypeVarName]) {
            if (!this.LoadPanel(model[reportsTypeVarName])) {
                model[reportsTypeVarName] = ReportesTypes.Draft;
                Relay.UrlHash.updateHashModel(model);
            }
        }
    };
    ReportBuilderPanel.prototype.reloadSection = function (sectionType) {
        if (this.subpanelController) {
            this.subpanelController.reloadSection(sectionType);
        }
    }
    ReportBuilderPanel.prototype.DocumentBtnHandler = function () {
        if (this.subpanelController) {
            var hashModel = Relay.UrlHash.getHashModel();;
            var rotationId = this.subpanelController.getRotationId();
            var selectedRotationId = null;
            var isReceived = hashModel[reportsTypeVarName] === ReportesTypes.Received;

            if (isReceived) {
                selectedRotationId = this.subpanelController.getReceivedRotationId();
            }

            var pdfUrl = "/Reports/HandoverReport?sourceId=" + rotationId
                + "&isShift=false"
                + "&isReceived=" + isReceived
                + "&selectedSourceId=" + selectedRotationId;

            window.open(pdfUrl, '_blank');
            //var hashModel = Relay.UrlHash.getHashModel();
            //var isReceived = hashModel[reportsTypeVarName] === ReportesTypes.Received;
            //var rotationId = this.subpanelController.getRotationId();
            //var selectedRotationId = null;

            //if (isReceived) {
            //    selectedRotationId = this.subpanelController.getReceivedRotationId();
            //}

            //Relay.FormDialog({
            //    url: "/RotationReportBuilder/ReportPreview",
            //    method: "GET",
            //    data: {
            //        isReceived: isReceived,
            //        rotationId: rotationId,
            //        selectedRotationId: selectedRotationId
            //    },
            //    dialogId: "popupReportPreview"
            //}).open();
        }
    };

    ReportBuilderPanel.prototype.QMEReportBtnHandler = function () {
        if (this.subpanelController) {
            var hashModel = Relay.UrlHash.getHashModel();;
            var rotationId = this.subpanelController.getRotationId();
            var selectedRotationId = null;
            var isReceived = hashModel[reportsTypeVarName] === ReportesTypes.Received;

            if (isReceived) {
                selectedRotationId = this.subpanelController.getReceivedRotationId();
            }

            var pdfUrl = "/Reports/QMEReport?sourceId=" + rotationId
                + "&isShift=false"
                + "&isReceived=" + isReceived
                + "&selectedSourceId=" + selectedRotationId;

            window.open(pdfUrl, '_blank');
        }
    };

    //===========================================================================
    window.Relay = window.Relay || {};
    window.Relay.ReportBuilderPanel = new ReportBuilderPanel($("#reportBuilderPanel").first());
    //===========================================================================
});