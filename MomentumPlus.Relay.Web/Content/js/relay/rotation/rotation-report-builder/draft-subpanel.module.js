﻿(function () {
    "use strict";
    //===========================================================================
    function DraftSubpanel(userId, controls) {
        this.userId = userId;
        this.rotationId = null;
        this.controls = controls;

        this.sections = [];
        this.IsExpanded = false;

        controls.filterBtn.build(userId, this);

        controls.documentBtn.prop('disabled', true);
        controls.qmePdfReportBtn.prop('disabled', true);
        controls.finaliseAllBtn.show();
        controls.expandCollapseAllBtn.html("Expand All");
        controls.expandCollapseAllBtn.show();
        controls.draftFromToBlock.show();

        this.finaliseHandler = DraftSubpanel_FinaliseAllBtnHandler.bind(this);
        controls.finaliseAllBtn[0].addEventListener("click", this.finaliseHandler, false);
        this.expandCollapseHandler = DraftSubpanel_ExpandCollapseAllBtnHandler.bind(this);
        controls.expandCollapseAllBtn[0].addEventListener("click", this.expandCollapseHandler, false);
        this.load();
    }
    DraftSubpanel.prototype.dispose = function () {

        this.controls.finaliseAllBtn[0].removeEventListener('click', this.finaliseHandler, false);
        this.controls.expandCollapseAllBtn[0].removeEventListener('click', this.expandCollapseHandler, false);
        this.controls.filterBtn.destroy();
        this.controls = null;
        this.sections = null;
    };
    DraftSubpanel.prototype.getRotationId = function () {
        return this.rotationId;
    };
    DraftSubpanel.prototype.load = function () {
        var _this = this;

        var filterType = Relay.UrlHash.getHashParam('filter') || 'HandoverItems';

        if (filterType === 'NrItems') {
            _this.controls.finaliseAllBtn.hide();
            _this.controls.expandCollapseAllBtn.hide();
        } else {
            _this.controls.finaliseAllBtn.show();
            _this.controls.expandCollapseAllBtn.show();
        }

        //-----------------------------------
        $('#loading-animation').show();
        //-----------------------------------
        $.ajax({
            url: "/RotationReportBuilder/DraftPanel",
            method: "GET",
            data: { userId: _this.userId, filter: filterType }
        })
        .done(function (result) {
            //--------------
            var div = document.createElement(div);
            div.innerHTML = result;
            //--------------
            var reportPanel = $(".report-panel", div);
            var rotationId = reportPanel.attr("rotation-id");
            _this.controls.panelContainer.empty();
            _this.controls.panelContainer.html(reportPanel);
            _this.controls.periodBtn.html(reportPanel.attr("rotation-period"));
            //--------------
            if (rotationId) {
                DraftSubpanel_Init.call(_this, rotationId);
                DraftSubpanel_LoadFromToBlock.call(_this, rotationId);
                _this.rotationId = rotationId;
                _this.controls.documentBtn.prop('disabled', false);
                _this.controls.qmePdfReportBtn.prop('disabled', false);
                _this.controls.shareReportBtnController.build(rotationId, "Swing");
            }
            else {
                _this.controls.documentBtn.prop('disabled', true);
                _this.controls.qmePdfReportBtn.prop('disabled', true);
                _this.controls.shareReportBtnController.destroy();
            }
            //--------------
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
        .always(function () {
            $('#loading-animation').hide();
        });
        //-----------------------------------
    }
    DraftSubpanel.prototype.reloadSection = function (sectionType) {
        DraftSubpanel_ReloadHandler.call(this, sectionType);
    }

    function DraftSubpanel_LoadFromToBlock(rotationId) {
        var _this = this;
        //-----------------------------------
        $.ajax({
            url: "/RotationReportBuilder/FromToBlock",
            method: "GET",
            data: { rotationId: rotationId }
        })
        .done(function (result) {
            //--------------
            var div = document.createElement(div);
            div.innerHTML = result;
            //--------------
            _this.controls.draftFromToBlock.html($(div).html());
            //--------------
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        });
        //-----------------------------------
    }

    function DraftSubpanel_ExpandCollapseAllBtnHandler() {
        if (!this.IsExpanded) {
            for (var i = 0; i < this.sections.length; ++i) {
                this.sections[i].expand();
            }

            this.IsExpanded = true;
            this.controls.expandCollapseAllBtn.html("Collapse All");
        }
        else {
            for (var i = 0; i < this.sections.length; ++i) {
                this.sections[i].collapse();
            }

            this.IsExpanded = false;
            this.controls.expandCollapseAllBtn.html("Expand All");
        }
    };

    function DraftSubpanel_FinaliseAllBtnHandler() {
        var _this = this;

        var confirmDialog = Relay.ConfirmDialog(
                                                    "Are you sure you want to finalise every topic and task?",
                                                    "",
                                                    "Yes",
                                                    "No"
                                                );

        confirmDialog.setOkEvent(function () {
            $.ajax({
                    url: "/RotationReportBuilder/FinalizeAll",
                    method: "POST",
                    data: { rotationId: _this.rotationId }
                })
                .done(function(result) {
                    _this.load();
                })
                .fail(function() {
                    Relay.ErrorDialog().open();
                });
        });

        confirmDialog.open();
    }

    function DraftSubpanel_Init(rotationId) {
        this.sections = [];

        this.sections.push(Relay.SectionFactory.DraftCoreSection($("#coreSection"), rotationId));
        this.sections.push(Relay.SectionFactory.DraftDailyNotesSection($("#dailyNotesSection"), rotationId));
        this.sections.push(Relay.SectionFactory.DraftProjectsSection($("#projectsSection"), rotationId));
        this.sections.push(Relay.SectionFactory.DraftSafetySection($("#safetySection"), rotationId));
        this.sections.push(Relay.SectionFactory.DraftTasksSection($("#tasksSection"), rotationId));
        this.sections.push(Relay.SectionFactory.DraftTeamSection($("#teamSection"), rotationId));

        for (var i = 0; i < this.sections.length; ++i) {
            this.sections[i].reloadExternalSection = DraftSubpanel_ReloadHandler.bind(this);
            this.sections[i].openInlineTopic = DraftSubpanel_OpenInlineTopicHandler.bind(this);
            this.sections[i].closeInlineTopic = DraftSubpanel_CloseInlineTopicHandler.bind(this);
        }
    }
    function DraftSubpanel_ReloadHandler(sectionType) {
        for (var i = 0; i < this.sections.length; ++i) {
            if (this.sections[i].type === sectionType)
                this.sections[i].reload();
        }
    }
    function DraftSubpanel_OpenInlineTopicHandler() {
        Relay.ReportBuilderPanel.disable();
        Relay.Header.disable();
        Relay.NavigationBar.disable();
        Relay.DailyNotesPanel.disable();

        for (var i = 0; i < this.sections.length; ++i) {
            this.sections[i].disableTopics();
        }
    }
    function DraftSubpanel_CloseInlineTopicHandler() {
        Relay.ReportBuilderPanel.enable();
        Relay.Header.enable();
        Relay.NavigationBar.enable();
        Relay.DailyNotesPanel.enable();

        for (var i = 0; i < this.sections.length; ++i) {
            this.sections[i].enableTopics();
        }
    }
    //===========================================================================
    window.Relay = window.Relay || {};
    window.Relay.DraftSubpanel = function (userId, controls) {
        return new DraftSubpanel(userId, controls);
    };
    //===========================================================================
})();