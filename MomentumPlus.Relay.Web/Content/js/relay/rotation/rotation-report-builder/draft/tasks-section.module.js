﻿; (function (window) {
    //===============================================================================================
    //  sectionElement: (section div)
    //  rotationId: (rotation id)
    //==============================  Draft Tasks Section ===========================================
    //------------------- Name Column Handler -----------------------------
    function NameClickHandler(model, reload, update) {
        if (!model.IsNullReport) {
            var tasksListDialog = Relay.FormDialog({
                url: "/Tasks/EditTaskFormDialog",
                method: "GET",
                data: { TaskId: model.Id }
            });
            tasksListDialog.setCloseEvent(reload);
            tasksListDialog.open();
        }
    }
    //---------------------------------------------------------------------
    function DraftTasksSection(sectionElement, rotationId) {
        //----------------------
        this.type = Relay.SectionFactory.Types.Tasks;
        var controller = "/TasksSection";
        this.sectionUrl = controller + "/Draft?rotationId=" + rotationId;
        this.updateTopicUrl = controller + "/UpdateTopic";
        this.reloadTopicUrl = controller + "/ReloadTopic";
        //----------------------
        this.topics;
        this.isExpanded = false;
        this.rotationId = rotationId;
        this.sectionElement = sectionElement;
        this.reloadExternalSection = null;
        //----------------------
        DraftTasksSection_Initialise.call(this);
    }
    DraftTasksSection.prototype.reload = function () {
        var _this = this;

        $.ajax({
            url: _this.sectionUrl,
            method: "GET"
        })
        .done(function (newContent) {
            //--- update content -----
            _this.sectionElement.empty();
            $(newContent).children().each(function () {
                _this.sectionElement.append(this);
            });
            //---- Init section ------
            DraftTasksSection_Initialise.call(_this)
            //------------------------
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    DraftTasksSection.prototype.expand = function () {
        this.isExpanded = true;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].autoCollapseOff();
            this.topics[i].expand();
        }
    };
    DraftTasksSection.prototype.collapse = function () {
        this.isExpanded = false;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].collapse();
            this.topics[i].autoCollapseOn();
        }
    };
    DraftTasksSection.prototype.disableTopics = function () {
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].disable();
        }
    }
    DraftTasksSection.prototype.enableTopics = function () {
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].enable();
        }
    }
    function DraftTasksSection_Initialise() {
        var _this = this;
        this.topics = [];
        //-------
        $("tbody tr", this.sectionElement).each(function () {
            var topic = DraftTasksSection_BuildTopic.call(_this, this);

            if (_this.isExpanded) {
                topic.autoCollapseOff();
                topic.expand();
            }

            _this.topics.push(topic);
        })
    }
    function DraftTasksSection_BuildTopic(trElement) {
        var _this = this;

        var topic = Relay.Topic(trElement, {
            updateUrl: this.updateTopicUrl,
            reloadUrl: this.reloadTopicUrl
        });

        topic.AddControl(Relay.Topic.ControlsFactory.RemoveTopicBtn({
            type: Relay.Topic.ControlsFactory.RemoveTopicBtn.Type.task,
            changeHandler: DraftTasksSection_ReloadAll
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TransferredBtn({
            changeHandler: DraftTasksSection_TopicActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.PinnedBtn({
            changeHandler: DraftTasksSection_TopicActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Location",
            elementClass: ".second-td"
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Name",
            elementClass: ".third-td",
            clickHandler: NameClickHandler
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NotesField());
        topic.AddControl(Relay.Topic.ControlsFactory.NowTaskStatusField());
        topic.AddControl(Relay.Topic.ControlsFactory.PriorityField());
        topic.AddControl(Relay.Topic.ControlsFactory.FinaliseBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.ManagerCommentsBtn({
            forTask: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TeamBtn({
            forDraftTask: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
            dialogType: Relay.AttachmentsDialog.Types.Draft,
            container: Relay.AttachmentsDialog.Containers.Task
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
            dialogType: Relay.VoiceMessagesDialog.Types.Draft,
            container: Relay.VoiceMessagesDialog.Containers.Task
        }));

        topic.updateView();

        return topic;
    }
    function DraftTasksSection_TopicActionsChangeHandler(model) {
        if (this.reloadExternalSection)
            this.reloadExternalSection(Relay.SectionFactory.Types[model.SectionType]);
    }
    function DraftTasksSection_ReloadAll() {
        Relay.ReportBuilderPanel.reload();
        Relay.DailyNotesPanel.reloadPanel();
    }
    //===============================================================================================
    if (window.Relay.SectionFactory) {
        window.Relay.SectionFactory.DraftTasksSection = function (sectionElement, rotationId) {
            return new DraftTasksSection(sectionElement, rotationId);
        };
    }
    else {
        console.error("Draft Tasks Section cannot work without SectionFactory.");
    }
    //===============================================================================================
})(window);