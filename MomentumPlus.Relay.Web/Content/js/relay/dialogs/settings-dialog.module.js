﻿; (function () {
    //=============================================================
    function SettingsDialog() {
        this.config = {
            url: "/Settings",
            dialogId: "settingsDialog"
        };

        this.dialog = null;
        this.openHandler = null;
        this.closeHandler = null;

        this.saveBtn = null;
        this.closeBtn = null;

        this.closeEventBind = this.closeEvent.bind(this);
    }
    SettingsDialog.prototype.IsOpened = false;
    SettingsDialog.prototype.setOpenEvent = function (handler) {
        if (SettingsDialog.prototype.IsOpened)
            return;

        this.openHandler = handler;
    };
    SettingsDialog.prototype.setCloseEvent = function (handler) {
        if (SettingsDialog.prototype.IsOpened)
            return;

        this.closeHandler = handler;
    };
    SettingsDialog.prototype.open = function () {
        if (SettingsDialog.prototype.IsOpened)
            return;
        SettingsDialog.prototype.IsOpened = true;
        //--------------------------
        $('#loading-animation').show();
        if (this.openHandler) {
            this.openHandler();
        }
        //--------------------------
        $.ajax({
            url: this.config.url,
            method: "GET"
        })
        .done(SettingsDialog_BuildDialog.bind(this))
        .fail(function () {
            Relay.ErrorDialog().open();
            SettingsDialog.prototype.IsOpened = false;
        })
        .always(function () {
            $('#loading-animation').hide();
        });
        //--------------------------
    };
    SettingsDialog.prototype.tabClickHandler = function (event) {
        var ref = $(event.target).attr("href");
        $(ref, this.dialog).find("li[class='active'] a").click();
    };
    SettingsDialog.prototype.saveBtnClickHandler = function (event) {
        var _this = this;
        if (Tab.activeTab) {
            Tab.activeTab.submitForm().then(function () {
                _this.dialog.modal("hide");
            });
        }
        else {
            this.dialog.modal("hide");
        }
    };
    SettingsDialog.prototype.formChangeHandler = function (isChanged) {
        if (isChanged) {
            //------- Add confirm close event --------
            this.closeBtn.removeAttr("data-dismiss");
            this.closeBtn.bind("click", this.closeEventBind);
            //--------- Active save button -----------
            this.saveBtn.addClass("active");
        }
        else {
            //----- Remove confirm close event -------
            this.closeBtn.unbind("click", this.closeEventBind);
            this.closeBtn.attr("data-dismiss", "modal");
            //------- Inactive save button -----------
            this.saveBtn.removeClass("active");
        }
    };
    SettingsDialog.prototype.closeEvent = function () {
        var confirmDialog = Relay.ConfirmDialog(
            "Are you sure?",
            "",
            "Yes",
            "No"
        );
        confirmDialog.setOkEvent(function () {
            this.dialog.modal("hide");
        }.bind(this));
        confirmDialog.open();
    };
    function SettingsDialog_BuildDialog(dialogHtml) {
        var _this = this;

        var container = document.createElement("div");
        container.innerHTML = dialogHtml;
        document.body.appendChild(container);

        this.dialog = $('#' + this.config.dialogId, container);

        this.dialog.on('hidden.bs.modal', function () {
            $(container).remove();
            SettingsDialog.prototype.IsOpened = false;
            if (_this.closeHandler)
                _this.closeHandler();
        });

        this.saveBtn = $("button.btn-save", this.dialog);
        this.closeBtn = $("button.close", this.dialog);

        this.saveBtn.click(this.saveBtnClickHandler.bind(this));
        $(".navigation-block li a", this.dialog).click(this.tabClickHandler.bind(this));

        SettingsDialog_InitSubtabs.call(this);

        this.dialog.modal('show');
    }
    function SettingsDialog_InitSubtabs() {
        //-----------------------------------
        var contentStateHandler = this.formChangeHandler.bind(this);
        //------- Init admin section --------
        var parentTab = $("a[href='#Administration']", this.dialog);
        var divTabContainer = $("#Administration .tab-content", this.dialog);
        $("#Administration .nav.nav-tabs a", this.dialog).each(function (i, tab) {
            new Tab(tab, parentTab, divTabContainer, contentStateHandler);
        });
        //------ Init profile section -------
        parentTab = $("a[href='#Profile']", this.dialog);
        divTabContainer = $("#Profile .tab-content", this.dialog);
        $("#Profile .nav.nav-tabs a", this.dialog).each(function (i, tab) {
            new Tab(tab, parentTab, divTabContainer, contentStateHandler);
        });
        //-----------------------------------
        Relay.InitInputFields(this.dialog);
        Tab.activeTab.initChangeDataEvents();
        Tab.dialog = this.dialog;
        //-----------------------------------
    }
    //=============================================================
    function Tab(tab, parentTab, container, contentStateHandler) {
        this.tab = tab;
        this.parentTab = parentTab;
        this.container = container;
        this.contentChanged = contentStateHandler;
        this.isContentChanged = false;

        $(tab).click(this.clickTab.bind(this));

        if ($(tab).parent().attr("class") === "active") {
            Tab.activeTab = this;
        }
    }
    Tab.activeTab = null;
    Tab.dialog = null;
    Tab.prototype.clickTab = function (event) {
        var _this = this;
        var url = $(event.target).attr("tab-url");

        if (Tab.activeTab !== this && url) {

            Relay.DialogLoadingAnimation.show(Tab.dialog, 200);

            if (Tab.activeTab && Tab.activeTab.isContentChanged) {
                var confirmDialog = Relay.ConfirmDialog(
                    "Do you want to save changes?",
                    "",
                    "Yes",
                    "No"
                );
                confirmDialog.setOkEvent(function () {
                    Tab.activeTab.submitForm()
                       .done(function () {
                           Tab_LoadContent.call(_this, url);
                       })
                       .fail(function () {
                           Relay.DialogLoadingAnimation.hide(Tab.dialog);
                           $(Tab.activeTab.parentTab).tab('show');
                           $(Tab.activeTab.tab).tab('show');
                       });
                });
                confirmDialog.setCancelEvent(function () {
                    Tab.activeTab.isContentChanged = false;
                    Tab_LoadContent.call(_this, url);
                });
                confirmDialog.open();
            }
            else {
                Tab_LoadContent.call(this, url);
            }
        }
    };
    Tab.prototype.submitForm = function () {
        var _this = this;
        var form = $("form", this.container).first();
        //----------------------------------------------
        if (form.length) {
            //------------------------
            return $.Deferred(function (defer) {
                $.validator.unobtrusive.parse(form);
                if (form.valid()) {
                    $.ajax({
                        url: form[0].action,
                        type: form[0].method,
                        data: new FormData(form[0]),
                        processData: false,
                        contentType: false
                    })
                    .done(function (result) {
                        if (result === "ok") {
                            _this.isContentChanged = false;
                            defer.resolve();
                        } else {
                            $(_this.container).html($(result));
                            _this.initChangeDataEvents();
                            Relay.InitInputFields(Tab.dialog);
                            defer.reject();
                        }
                    })
                    .fail(function () {
                        defer.reject();
                        Relay.ErrorDialog().open();
                    });
                }
                else {
                    defer.reject();
                }
            }).promise();
            //------------------------
        }
        else {
            //------------------------
            return $.Deferred(function (defer) { defer.resolve(); }).promise();
            //------------------------
        }
        //----------------------------------------------
    };
    Tab.prototype.initChangeDataEvents = function() {
        var _this = this;

        var handler = function () {
            //------ Unbind change events --------
            $('select', _this.container).unbind("change", handler);
            $('.simple-input', _this.container).unbind("input", handler);
            $('.simple-textarea', _this.container).unbind("input", handler);
            $('input[type=file]', _this.container).bind("change", handler);
            //------------------------------------
            _this.isContentChanged = true;

            if (_this.contentChanged)
                _this.contentChanged(true);
        };

        $('select', this.container).bind("change", handler);
        $('.simple-input', this.container).bind("input", handler);
        $('.simple-textarea', this.container).bind("input", handler);
        $('input[type=file]', this.container).bind("change", handler);
    };
    function Tab_LoadContent(url) {
        var _this = this;

        $.ajax({
            url: url,
            method: "GET"
        })
         .done(function (data) {
             $(_this.container).html($(data));
             _this.initChangeDataEvents();
             Relay.InitInputFields(Tab.dialog);
             Relay.DialogLoadingAnimation.hide(Tab.dialog);
             Tab.activeTab = _this;
             if (_this.contentChanged)
                 _this.contentChanged(false);
         })
         .fail(function () {
             Relay.ErrorDialog().open();
         });
    }
    //=============================================================
    window.Relay = window.Relay || {};
    window.Relay.SettingsDialog = function () {
        return new SettingsDialog();
    };
    //=============================================================
})();