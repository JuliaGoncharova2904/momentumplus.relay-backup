﻿; (function (window) {
    "use strict";
    //===================================================================
    // config: {
    //      userId: userId,
    //      selectedUserId: SelectedUserId,
    //      showMe: false,
    //      showOnSite: false,
    //      showOffSite: false,
    //      showWithoutRotation: false
    // }
    //=================== Choose Teammate Dialog ========================
    function ChooseTeammateDialog(config) {
        this.config = config;
        this.openHandler = null;
        this.chooseHandler = null;
        this.closeHandler = null;
        //-----------------------
        this.chosenId = null;
        //-----------------------
        this.dialogId = "chooseTeammateDialog";
        this.conteiner = null;
        this.dialog = null;
        //-----------------------
    }
    ChooseTeammateDialog.prototype.setOpenEvent = function (eventHandler) {
        this.openHandler = eventHandler;
    };
    ChooseTeammateDialog.prototype.setChooseEvent = function (eventHandler) {
        this.chooseHandler = eventHandler;
    };
    ChooseTeammateDialog.prototype.setCloseEvent = function (eventHandler) {
        this.closeHandler = eventHandler;
    };
    ChooseTeammateDialog.prototype.open = function () {

        var _this = this;

        $('#loading-animation').show();

        if (_this.openHandler) {
            _this.openHandler();
        }

        $.ajax({
            url: "/Team/ChooseTeammateDialog",
            method: "GET",
            data: {
                userId: _this.config.userId,
                showMe: _this.config.showMe,
                showOnSite: _this.config.showOnSite,
                showOffSite: _this.config.showOffSite,
                showWithoutRotation: _this.config.showWithoutRotation,
            }
        })
        .done(function (data) {
            ChooseTeammateDialog_CreateDialog.call(_this, data);
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(_this.closeHandler);
            errorDialog.open();
        })
        .always(function () {
            $('#loading-animation').hide();
        });
    };

    function ChooseTeammateDialog_CreateDialog(dialogHtml) {
        this.conteiner = document.createElement("div");
        this.conteiner.innerHTML = dialogHtml;
        document.body.appendChild(this.conteiner);

        var _this = this;
        this.dialog = $('#' + this.dialogId, this.conteiner)[0];
        $(this.dialog).on('hidden.bs.modal', function () {
            $(_this.conteiner).remove();
            if (_this.closeHandler) {
                _this.closeHandler();
            }
        });

        this.chosenId = this.config.selectedUserId;

        $(".user[id='" + this.chosenId + "']", this.dialog).addClass("selected-user");

        $(".user", this.dialog).tooltipster({
            debug: false
        });

        $(".user", this.dialog).on('click', function () {
            $(".user", _this.dialog).removeClass("selected-user");
            $(this).addClass("selected-user");

            _this.chosenId = $(this).prop('id');

            $(".btn-save", _this.dialog).addClass("active");
        });

        $(".btn-save", this.dialog).click(function () {
            if (_this.chooseHandler)
                _this.chooseHandler(_this.chosenId);

            $(_this.dialog).modal("hide");
        });

        $(this.dialog).modal('show');
    }
    //===================================================================
    window.Relay = window.Relay || {};
    window.Relay.ChooseTeammateDialog = function (config) {
        return new ChooseTeammateDialog(config);
    };
    //===================================================================
})(window);