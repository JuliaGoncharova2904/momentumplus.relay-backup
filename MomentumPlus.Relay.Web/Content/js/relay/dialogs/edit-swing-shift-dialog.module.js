﻿; (function () {
    //===========================================================================
    //  config = {
    //              userId: (User ID)
    //          }
    //===========================================================================
    function EditSwingShiftDialog(config) {
        this.config = config || {}
        this.config.url = "/Rotation/EditSwingShiftDialog";

        this.dialog = null;
        this.openHandler = null;
        this.closeHandler = null;

        this.saveBtn = null;
        this.closeBtn = null;

        this.swingTab = null;
        this.shiftTab = null;

        this.changeStatusFlag = false;

        this.closeEventBind = EditSwingShiftDialog_CloseEvent.bind(this);
    }
    EditSwingShiftDialog.prototype.setOpenEvent = function (handler) {
        this.openHandler = handler;
    };
    EditSwingShiftDialog.prototype.setCloseEvent = function (handler) {
        this.closeHandler = handler;
    };
    EditSwingShiftDialog.prototype.open = function () {
        //--------------------------
        $('#loading-animation').show();
        if (this.openHandler) {
            this.openHandler();
        }
        //--------------------------
        $.ajax({
                url: this.config.url,
                method: "GET",
                data: { userId: this.config.userId }
            })
            .done(EditSwingShiftDialog_BuildDialog.bind(this))
            .fail(function () {
                Relay.ErrorDialog().open();
            })
            .always(function () {
                $('#loading-animation').hide();
            });
        //--------------------------
    };
    EditSwingShiftDialog.prototype.ChangeStatus = function (isChanged) {
        if (isChanged) {
            if (!this.saveBtn.hasClass("active")) {
                //------- Add confirm close event --------
                this.closeBtn.removeAttr("data-dismiss");
                this.closeBtn.bind("click", this.closeEventBind);
                //--------- Active save button -----------
                this.saveBtn.addClass("active");
            }
        }
        else {
            //----- Remove confirm close event -------
            this.closeBtn.unbind("click", this.closeEventBind);
            this.closeBtn.attr("data-dismiss", "modal");
            //------- Inactive save button -----------
            this.saveBtn.removeClass("active");
        }

        this.changeStatusFlag = isChanged;
    };
    EditSwingShiftDialog.prototype.Save = function (event) {
        var _this = this;

        if (!this.changeStatusFlag)
            return;

        if (this.swingTab && this.swingTab.isActive()) {
            this.swingTab.submit()
                .then(function () {
                    if (_this.shiftTab) {
                        _this.swingTab.reload();
                    }
                    else {
                        _this.dialog.modal("hide");
                    }

                    _this.ChangeStatus(false);
                })
                .fail(function () {
                    _this.ChangeStatus(false);
                });
        }
        else if (this.shiftTab && this.shiftTab.isActive()) {
            this.shiftTab.submit()
                .then(function () {
                    _this.shiftTab.reload();
                    _this.ChangeStatus(false);
                })
                .fail(function () {
                    _this.ChangeStatus(false);
                });
        }
    };
    function EditSwingShiftDialog_CloseEvent() {
        var confirmDialog = Relay.ConfirmDialog(
            "All changes on tab will be lost. Are you sure?",
            "",
            "Yes",
            "No"
        );
        confirmDialog.setOkEvent(function () {
            this.dialog.modal("hide");
        }.bind(this));
        confirmDialog.open();
    };
    function EditSwingShiftDialog_BuildDialog(dialogHtml) {
        var _this = this;
        //-----------------------------------
        var container = document.createElement("div");
        container.innerHTML = dialogHtml;
        document.body.appendChild(container);
        //-----------------------------------
        this.dialog = $(container).children().first();

        this.dialog.on('hidden.bs.modal', function () {
            $(container).remove();
            if (_this.closeHandler)
                _this.closeHandler();
        });
        //-----------------------------------
        this.saveBtn = $("button.btn-save", this.dialog);
        this.closeBtn = $("button.btn-close", this.dialog);

        this.saveBtn.click(this.Save.bind(this));
        //-----------------------------------
        var swingElement = $("#swing", container);
        if (swingElement.length) {
            this.swingTab = new SwingTab(swingElement, this);
            this.swingTab.isActive(true);
        }

        var shiftElement = $("#shift", container);
        if (shiftElement.length) {
            this.shiftTab = new ShiftTab(shiftElement, this);
        }
        //-----------------------------------
        var retToPrevTab = false;
        $("a[data-toggle='tab']", container).on('shown.bs.tab', function (event) {

            if (retToPrevTab) {
                retToPrevTab = false;
                return;
            }

            var target = $(event.target).attr("href")

            switch (target) {
            case "#swing":
                if (_this.changeStatusFlag) {
                    var confirmDialog = Relay.ConfirmDialog(
                        "All changes on tab will be lost. Are you sure?",
                        "",
                        "Yes",
                        "No"
                    );
                    confirmDialog.setOkEvent(function () {
                        _this.shiftTab.isActive(false);
                        _this.swingTab.isActive(true);
                        _this.swingTab.reload();
                    });
                    confirmDialog.setCancelEvent(function () {
                        $('a[href="#shift"]', container).tab('show');
                        retToPrevTab = true;
                    });
                    confirmDialog.open();
                }
                else {
                    _this.shiftTab.isActive(false);
                    _this.swingTab.isActive(true);
                    _this.swingTab.reload();
                }
                break;
            case "#shift":
                if (_this.changeStatusFlag) {
                    var confirmDialog = Relay.ConfirmDialog(
                        "All changes on tab will be lost. Are you sure?",
                        "",
                        "Yes",
                        "No"
                    );
                    confirmDialog.setOkEvent(function () {
                        _this.swingTab.isActive(false);
                        _this.shiftTab.isActive(true);
                        _this.shiftTab.reload();
                    });
                    confirmDialog.setCancelEvent(function () {
                        $('a[href="#swing"]', container).tab('show');
                        retToPrevTab = true;
                    });
                    confirmDialog.open();
                }
                else {
                    _this.swingTab.isActive(false);
                    _this.shiftTab.isActive(true);
                    _this.shiftTab.reload();
                }
                break;
            }
        });
        //-----------------------------------
        this.dialog.modal('show');
    }
    //===========================================================================
    function SwingTab(tabElement, dialog) {
        this.dialog = dialog;
        this.tabElement = tabElement;
        this.rotationId = null;
        this.calendarContainer = null;
        this.isActiveFlag = false;

        SwingTab_Init.call(this);
    }
    SwingTab.prototype.isActive = function (state) {
        if (state !== undefined)
            this.isActiveFlag = state;

        return this.isActiveFlag;
    };
    SwingTab.prototype.reload = function () {
        var _this = this;

        $.ajax({
                url: "/Rotation/EditSwingForm",
                method: "GET",
                data: { rotationId: this.rotationId }
            })
            .done(function (result) {
                _this.tabElement.html($(result));
                SwingTab_Init.call(_this);
                _this.dialog.ChangeStatus(false);
            })
            .fail(function () {
                Relay.ErrorDialog().open();
            })
    };
    SwingTab.prototype.submit = function () {
        var _this = this;
        var form = $("form", _this.tabElement);

        if (form.length) {
            return $.Deferred(function (defer) {

                $.validator.unobtrusive.parse(form);
                if (form.valid()) {
                    $.ajax({
                            url: "/Rotation/EditSwingForm",
                            method: "POST",
                            data: form.serialize()
                        })
                        .done(function (result) {
                            if (result === "ok") {
                                defer.resolve();
                            } else {
                                _this.tabElement.html($(result));
                                SwingTab_Init.call(_this);
                                defer.reject();
                            }
                        })
                        .fail(function () {
                            defer.reject();
                            Relay.ErrorDialog().open();
                        })
                }
                else {
                    defer.reject();
                }
            }).promise();
        }

        return $.Deferred(function (defer) { defer.resolve(); }).promise();
    };

    function SwingTab_Init() {
        this.rotationId = $("#RotationId", this.tabElement).val();
        this.calendarContainer = $("#miniCalendar", this.tabElement);

        var _this = this;
        var chooseHandlerId = null;
        var currentDate = Relay.DotNetDate.dateStringToDate($("#CurrentDate", this.tabElement).val());
        var endSwingDate = Relay.DotNetDate.dateStringToDate($("#EndSwingDate", this.tabElement).val());
        var backOnSiteDateReadOnly = $("#BackOnSiteDateReadOnly", this.tabElement).val();
        var endSwingDateReadOnly = $("#EndSwingDateReadOnly", this.tabElement).val();

        var calendar = $(this.calendarContainer).datepicker({ todayBtn: false });

        calendar.on('changeDate', function () {
            if (chooseHandlerId) {
                switch (chooseHandlerId) {
                case "EndSwingDate":
                    $("#BackOnSiteDate", this.tabElement).val('');
                    SwingTab_UpdateDateField.call(_this, "BackOnSiteDate");
                }

                $('#' + chooseHandlerId, this.tabElement).val(Relay.DotNetDate.dateToDateString($(this).datepicker('getDate')));
                SwingTab_UpdateDateField.call(_this, chooseHandlerId);

                _this.dialog.ChangeStatus(true);
            }
        });

        $(".prev", calendar).text("");
        $(".next", calendar).text("");

        SwingTab_UpdateDateField.call(_this, "StartSwingDate");
        SwingTab_UpdateDateField.call(_this, "EndSwingDate");
        SwingTab_UpdateDateField.call(_this, "BackOnSiteDate");

        if (backOnSiteDateReadOnly === "False" || endSwingDateReadOnly === "False") {
            this.tabElement.bind("click", function () {
                SwingTab_OffCalendar.call(_this);
                $(".field-container", this.tabElement).removeClass("active");
            });

            if (endSwingDateReadOnly === "False") {
                $(".end-date-btn", this.tabElement).bind("click", function (event) {

                    if (currentDate) {
                        var startEndDate = new Date(currentDate);
                        startEndDate.setDate(startEndDate.getDate() + 1);
                        calendar.datepicker("setStartDate", startEndDate);

                        SwingTab_OnCalendar.call(_this);
                        $(".field-container", this.tabElement).removeClass("active");
                        $(this).parent().addClass("active");
                        chooseHandlerId = "EndSwingDate";
                    }

                    event.stopPropagation();
                });
            }

            if (backOnSiteDateReadOnly === "False") {
                $(".back-date-btn", this.tabElement).bind("click", function (event) {
                    var endSwingDate = Relay.DotNetDate.dateStringToDate($("#EndSwingDate", this.tabElement).val());

                    if (endSwingDate) {
                        var startBackDate = new Date(endSwingDate);
                        startBackDate.setDate(startBackDate.getDate() + 1);
                        calendar.datepicker("setStartDate", startBackDate);

                        SwingTab_OnCalendar.call(_this);
                        $(".field-container", this.tabElement).removeClass("active");
                        $(this).parent().addClass("active");
                        chooseHandlerId = "BackOnSiteDate";
                    }

                    event.stopPropagation();
                });
            }
        }

        Relay.InitInputFields(this.tabElement);

        $("select", this.tabElement).on("change", function () {
            _this.dialog.ChangeStatus(true);
        });
    }
    function SwingTab_UpdateDateField(id) {
        var date = Relay.DotNetDate.dateStringToDate($('#' + id, this.tabElement).val());

        if (date) {
            $('#' + id + "Day", this.tabElement).text(moment(date).format("DD"));
            $('#' + id + "Month", this.tabElement).text(moment(date).format("MMM"));
            $('#' + id + "Year", this.tabElement).text(moment(date).format("YYYY"));
        }
        else {
            $('#' + id + "Day", this.tabElement).text("DD");
            $('#' + id + "Month", this.tabElement).text("MMM");
            $('#' + id + "Year", this.tabElement).text("YYYY");
        }
    }
    function SwingTab_OnCalendar() {
        this.calendarContainer.removeAttr("style");
    }
    function SwingTab_OffCalendar() {
        this.calendarContainer.attr("style", "pointer-events: none;");
    }
    //===========================================================================
    var TimeSelectType = {
        Hour: 0,
        Minute: 1
    };
    //---------------------------------------------
    function ShiftTab(tabElement, dialog) {
        this.dialog = dialog;
        this.tabElement = tabElement;
        this.shiftId = null;
        this.calendarContainer = null;
        this.isActiveFlag = false;

        ShiftTab_Init.call(this);
    }
    ShiftTab.prototype.isActive = function (state) {
        if (state !== undefined)
            this.isActiveFlag = state;

        return this.isActiveFlag;
    };
    ShiftTab.prototype.reload = function () {
        var _this = this;

        $.ajax({
                url: "/Shift/EditShiftForm",
                method: "GET",
                data: { shiftId: this.shiftId }
            })
            .done(function (result) {
                _this.tabElement.empty();
                _this.tabElement.html($(result));
                ShiftTab_Init.call(_this);
                _this.dialog.ChangeStatus(false);
            })
            .fail(function () {
                Relay.ErrorDialog().open();
            })
    };
    ShiftTab.prototype.submit = function () {
        var _this = this;
        var form = $("form", _this.tabElement);

        if (form.length) {
            return $.Deferred(function (defer) {

                $.validator.unobtrusive.parse(form);
                if (form.valid()) {
                    $.ajax({
                            url: "/Shift/EditShiftForm",
                            method: "POST",
                            data: form.serialize()
                        })
                        .done(function (result) {
                            if (result === "ok") {
                                defer.resolve();
                            } else {
                                _this.tabElement.html($(result));
                                ShiftTab_Init.call(_this);
                                defer.reject();
                            }
                        })
                        .fail(function () {
                            defer.reject();
                            Relay.ErrorDialog().open();
                        })
                }
                else {
                    defer.reject();
                }
            }).promise();
        }

        return $.Deferred(function (defer) { defer.resolve(); }).promise();
    };

    function ShiftTab_Init() {
        var _this = this;
        this.chooseHandlerId = null;
        this.shiftId = $("#ShiftId", this.tabElement).val();
        this.calendarContainer = $("#miniCalendar", this.tabElement);
        var calendar = $(this.calendarContainer).datepicker({ todayBtn: false });
        //------------------ Limit Dates -------------------------------------
        var currentDate = Relay.DotNetDate.dateStringToDate($("#CurrentDate", this.tabElement).val());
        var endLimitDate = Relay.DotNetDate.dateStringToDate($("#EndLimitDate", this.tabElement).val());
        var endShiftDateReadOnly = $("#EndShiftDateReadOnly", this.tabElement).val() === "True";
        var nextShiftDateReadOnly = $("#NextShiftDateReadOnly", this.tabElement).val() === "True";
        //------------------ Functions -------------------------------------
        calendar.on('changeDate', function () {
            if (_this.chooseHandlerId) {
                $('#' + _this.chooseHandlerId, _this.tabElement).val(Relay.DotNetDate.dateToDateString($(this).datepicker('getDate')));
                ShiftTab_ClearDependentDates.call(_this, _this.chooseHandlerId);
                ShiftTab_UpdateDateField.call(_this, _this.chooseHandlerId);

                _this.dialog.ChangeStatus(true);
            }
        });

        $(".prev", calendar).text("");
        $(".next", calendar).text("");

        $('.select-list', this.tabElement).selectpicker({
            style: 'select-class',
            size: 7,
            dropupAuto: false
        });

        $("input[name=RepeatTimes]:radio", this.tabElement).bind("change", function () {
            _this.dialog.ChangeStatus(true);
        });

        //-------------------- Select Hours handlers -------------------------
        $("select.select-list.hours-list", this.tabElement).on("show.bs.select", ShiftTab_ShowTimeSelectHandler.bind(this));
        $("select.select-list.minutes-list", this.tabElement).on("show.bs.select", ShiftTab_ShowTimeSelectHandler.bind(this));

        $("select.select-list.hours-list", this.tabElement).on('change', function () {
            if (_this.chooseHandlerId) {
                var dateString = $('#' + _this.chooseHandlerId, _this.tabElement).val();
                if (dateString) {
                    var date = Relay.DotNetDate.dateStringToDate(dateString);
                    date.setHours(Number($(this).val()));
                    $('#' + _this.chooseHandlerId, _this.tabElement).val(Relay.DotNetDate.dateToDateString(date));

                    ShiftTab_ClearDependentDates.call(_this, _this.chooseHandlerId);

                    _this.dialog.ChangeStatus(true);
                }
            }
        });

        $("select.select-list.minutes-list", this.tabElement).on('change', function () {
            if (_this.chooseHandlerId) {
                var dateString = $('#' + _this.chooseHandlerId, this.tabElement).val();
                if (dateString) {
                    var date = Relay.DotNetDate.dateStringToDate(dateString);
                    date.setMinutes(Number($(this).val()));
                    $('#' + _this.chooseHandlerId, this.tabElement).val(Relay.DotNetDate.dateToDateString(date));

                    ShiftTab_ClearDependentDates.call(_this, _this.chooseHandlerId);

                    _this.dialog.ChangeStatus(true);
                }
            }
        });

        $(".bootstrap-select ul.dropdown-menu", this.tabElement).each(function () {
            $("li", this).first().attr("style", "display: none;");
        });

        ShiftTab_UpdateDateField.call(this, "StartShiftDate");
        ShiftTab_UpdateDateField.call(this, "EndShiftDate");
        ShiftTab_UpdateDateField.call(this, "NextShiftDate");

        if ($("#RepeatShiftMultiple", this.tabElement).is(":checked")) {
            $(".repeat-count", this.tabElement).show();
            ShiftTab_CalcAllowedRepeatTimes.call(this, endLimitDate);
        }

        $('input[name=RepeatShift]:radio', this.tabElement).change(function () {
            if ($("#RepeatShiftMultiple", _this.tabElement).is(":checked")) {
                $(".repeat-count", _this.tabElement).show();
                ShiftTab_CalcAllowedRepeatTimes.call(_this, endLimitDate);
            }
            else {
                $(".repeat-count", _this.tabElement).hide();
                $("#RepeatTimes0", _this.tabElement).prop("checked", true);
            }

            ShiftTab_OffCalendar.call(_this);
            $(".field-container", _this.tabElement).removeClass("active");
            _this.dialog.ChangeStatus(true);
        });

        if (!endShiftDateReadOnly) {
            $(".end-date-btn", this.tabElement).bind("click", function (event) {
                var startEndDateString = $("#StartShiftDate", _this.tabElement).val();

                if (startEndDateString) {
                    var startEndDate = Relay.DotNetDate.dateStringToDate(startEndDateString);

                    calendar.datepicker("setStartDate", currentDate.getTime() < startEndDate.getTime() ? startEndDate : currentDate);

                    if (endLimitDate) {
                        calendar.datepicker("setEndDate", endLimitDate);
                    }

                    ShiftTab_OnCalendar.call(_this);
                    $(".field-container", _this.tabElement).removeClass("active");
                    $(this).parent().addClass("active");
                    _this.chooseHandlerId = "EndShiftDate";
                }

                event.stopPropagation();
            });
        }

        if (!nextShiftDateReadOnly) {
            $(".next-date-btn", this.tabElement).bind("click", function (event) {
                var endShiftDateString = $("#EndShiftDate", _this.tabElement).val();

                if (endShiftDateString) {
                    var endShiftDate = Relay.DotNetDate.dateStringToDate(endShiftDateString);

                    calendar.datepicker("setStartDate", currentDate.getTime() < endShiftDate.getTime() ? endShiftDate : currentDate);

                    var beginningOfNextShiftLimitDate = Relay.DotNetDate.dateStringToDate($("#BeginningOfNextShiftLimitDate", _this.tabElement).val());

                    if (beginningOfNextShiftLimitDate) {
                        calendar.datepicker("setEndDate", beginningOfNextShiftLimitDate);
                    }

                    ShiftTab_OnCalendar.call(_this);
                    $(".field-container", _this.tabElement).removeClass("active");
                    $(this).parent().addClass("active");
                    _this.chooseHandlerId = "NextShiftDate";
                }

                event.stopPropagation();
            });
        }
    }
    //------------------------------------------------------
    function ShiftTab_CalcHideFirstItems(lastEndTime, newDate, type) {
        var _newDate = new Date(newDate);

        type === TimeSelectType.Hour && _newDate.setHours(0);
        type === TimeSelectType.Minute && _newDate.setMinutes(0);

        var difTime = lastEndTime.getTime() - _newDate.getTime();
        var hours = difTime > 0 ? Math.floor(difTime / (1000 * 3600)) : 0;
        var minutes = difTime > 0 ? Math.floor(difTime / (1000 * 60)) : 0;

        switch (type) {
        case TimeSelectType.Hour:
            return hours;
        case TimeSelectType.Minute:
            return (minutes - hours * 60) / 5;
        }
    }

    function ShiftTab_CalcHideLastMinutesItems(endLimitTime, newDate) {
        var _newDate = new Date(newDate);
        _newDate.setMinutes(0);

        var difTime = endLimitTime.getTime() - _newDate.getTime();
        var minutes = difTime > 0 ? Math.floor(difTime / (1000 * 60)) : 0;

        return (minutes < 60) ? (60 - minutes) / 5 : 0;
    }

    function ShiftTab_ShowAllItems(items) {
        items.slice(1).removeAttr("style");
    }

    function ShiftTab_HideFirstItems(items, number) {
        number && items.slice(1, number + 1).attr("style", "display: none");
    }

    function ShiftTab_HideLastItems(items, number) {
        number && items.slice(-number).attr("style", "display: none");
    }

    function ShiftTab_ShowTimeSelectHandler(event) {
        var type = $(event.currentTarget).hasClass("hours-list") ? TimeSelectType.Hour : TimeSelectType.Minute;
        var allSelectItems = $(event.currentTarget).parent().find("ul.dropdown-menu li");
        var endShiftDate = Relay.DotNetDate.dateStringToDate($("#EndShiftDate", this.tabElement).val());
        var nextShiftDate = Relay.DotNetDate.dateStringToDate($("#NextShiftDate", this.tabElement).val());
        var currentDate = Relay.DotNetDate.dateStringToDate($("#CurrentDate", this.tabElement).val());
        var endLimitDate = Relay.DotNetDate.dateStringToDate($("#EndLimitDate", this.tabElement).val());

        var beginningOfNextShiftLimitDate = Relay.DotNetDate.dateStringToDate($("#BeginningOfNextShiftLimitDate", this.tabElement).val());

        ShiftTab_ShowAllItems(allSelectItems);

        switch (this.chooseHandlerId) {
        case "EndShiftDate":
            ShiftTab_HideFirstItems(allSelectItems, ShiftTab_CalcHideFirstItems(currentDate, endShiftDate, type));
            (type === TimeSelectType.Minute) && ShiftTab_HideLastItems(allSelectItems, ShiftTab_CalcHideLastMinutesItems(endLimitDate, endShiftDate));
            break;
        case "NextShiftDate":
            ShiftTab_HideFirstItems(allSelectItems, ShiftTab_CalcHideFirstItems(endShiftDate, nextShiftDate, type));
            (type === TimeSelectType.Minute) && ShiftTab_HideLastItems(allSelectItems, ShiftTab_CalcHideLastMinutesItems(beginningOfNextShiftLimitDate, nextShiftDate));
            break;
        }
    }
    //------------------------------------------------------
    function ShiftTab_ClearDependentDates(id) {
        switch (id) {
        case "StartShiftDate":
            $("#EndShiftDate", this.tabElement).val('');
            ShiftTab_UpdateDateField.call(this, "EndShiftDate");
            break;
        case "EndShiftDate":
            $("#NextShiftDate", this.tabElement).val('');
            ShiftTab_UpdateDateField.call(this, "NextShiftDate");
            break;
        default:
            $("#RepeatShiftNever", this.tabElement).click();
            break;
        }
    }
    function ShiftTab_UpdateDateField(id) {
        var date = Relay.DotNetDate.dateStringToDate($('#' + id, this.tabElement).val());

        if (date) {
            $('#' + id + "Day", this.tabElement).text(moment(date).format("DD"));
            $('#' + id + "Month", this.tabElement).text(moment(date).format("MMM"));
            $('#' + id + "Year", this.tabElement).text(moment(date).format("YYYY"));

            $('#' + id + "Hours", this.tabElement).selectpicker('val', [date.getHours()]);
            $('#' + id + "Minutes", this.tabElement).selectpicker('val', [date.getMinutes()]);
        }
        else {
            $('#' + id + "Day", this.tabElement).text("DD");
            $('#' + id + "Month", this.tabElement).text("MMM");
            $('#' + id + "Year", this.tabElement).text("YYYY");

            $('#' + id + "Hours", this.tabElement).selectpicker('val', ['']);
            $('#' + id + "Minutes", this.tabElement).selectpicker('val', ['']);
        }
    }
    function ShiftTab_OnCalendar() {
        this.calendarContainer.removeAttr("style");
    }
    function ShiftTab_OffCalendar() {
        this.calendarContainer.attr("style", "pointer-events: none;");
    }
    function ShiftTab_CalcAllowedRepeatTimes(endLimitDate) {

        var startShiftDateString = $("#StartShiftDate", this.tabElement).val();
        var endShiftDateString = $("#EndShiftDate", this.tabElement).val();
        var nextShiftDateString = $("#NextShiftDate", this.tabElement).val();

        if (startShiftDateString && endShiftDateString && nextShiftDateString) {
            var startShiftDate = Relay.DotNetDate.dateStringToDate(startShiftDateString);
            var endShiftDate = Relay.DotNetDate.dateStringToDate(endShiftDateString);
            var nextShiftDate = Relay.DotNetDate.dateStringToDate(nextShiftDateString);
            var _endLimitDate = new Date(endLimitDate);
            _endLimitDate.setDate(_endLimitDate.getDate() + 1);

            var fullShiftPeriod = nextShiftDate.getTime() - startShiftDate.getTime();
            var workShiftPeriod = endShiftDate.getTime() - startShiftDate.getTime();
            var leftSwingPeriod = _endLimitDate.getTime() - startShiftDate.getTime();

            var repeatTimes = Math.floor(leftSwingPeriod / fullShiftPeriod) + (leftSwingPeriod % fullShiftPeriod > workShiftPeriod ? 1 : 0);

            $("input[name='RepeatTimes']", this.tabElement).each(function (index) {
                if (Number($(this).val()) < repeatTimes) {
                    $(this).removeAttr("disabled");
                }
                else {
                    $(this).attr("disabled", '');
                }
            });
        }
        else {
            $("input[name='RepeatTimes']", this.tabElement).attr("disabled", '');
        }
    }
    //===========================================================================
    window.Relay = window.Relay || {};
    window.Relay.EditSwingShiftDialog = function (config) {
        return new EditSwingShiftDialog(config);
    };
    //===========================================================================
})();