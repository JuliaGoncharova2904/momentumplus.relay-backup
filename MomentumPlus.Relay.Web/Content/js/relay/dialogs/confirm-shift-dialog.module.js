﻿$(document).ready(function () {
    //--------------------- Init -----------------------------------------
    var dialog = $("#confirmShiftDialog");
    var calendarContainer = $("#miniCalendar", dialog);
    var calendar = $(calendarContainer).datepicker({ todayBtn: false });
    var chooseHandlerId = null;
    //------------------ Limit Dates -------------------------------------
    var startLimitDate = Relay.DotNetDate.dateStringToDate($("#StartLimitDate", dialog).val());
    var endLimitDate = Relay.DotNetDate.dateStringToDate($("#EndLimitDate", dialog).val());

    var beginningOfNextShiftLimitDate = Relay.DotNetDate.dateStringToDate($("#BeginningOfNextShiftLimitDate", dialog).val());
    //-------------------------------------------------------------------
    setTimeout(function () {
        $(".bootstrap-select ul.dropdown-menu", dialog).each(function () {
            $("li", this).first().attr("style", "display: none;");
        });

        updateDateField("StartShiftDate");
        updateDateField("EndShiftDate");
        updateDateField("NextShiftDate");

        if ($("#RepeatShiftMultiple", dialog).is(":checked")) {
            $(".repeat-count", dialog).show();
            calcAllowedRepeatTimes();
        }
    }, 100);
    //------------------- Functions --------------------------------------
    calendar.on('changeDate', function () {
        if (chooseHandlerId) {
            $('#' + chooseHandlerId, dialog).val(Relay.DotNetDate.dateToDateString($(this).datepicker('getDate')));
            clearDependentDates(chooseHandlerId);
            updateDateField(chooseHandlerId);

            $("button[type=submit]", dialog).trigger("activate");
        }
    });

    $(".prev", calendar).text("");
    $(".next", calendar).text("");
    //-------------------- Select Hours handlers -------------------------
    var timeSelectType = { Hour: 0, Minute: 1 }

    function calcHideFirstItems(lastEndTime, newDate, type) {
        var _newDate = new Date(newDate);

        type == timeSelectType.Hour && _newDate.setHours(0);
        type == timeSelectType.Minute && _newDate.setMinutes(0);

        var difTime = lastEndTime.getTime() - _newDate.getTime();
        var hours = difTime > 0 ? Math.floor(difTime / (1000 * 3600)) : 0;
        var minutes = difTime > 0 ? Math.floor(difTime / (1000 * 60)) : 0;

        switch (type) {
            case timeSelectType.Hour:
                return hours;
            case timeSelectType.Minute:
                return (minutes - hours * 60) / 5;
        }
    }

    function calcHideLastMinutesItems(endLimitTime, newDate) {
        var _newDate = new Date(newDate);
        _newDate.setMinutes(0);

        var difTime = endLimitTime.getTime() - _newDate.getTime();
        var minutes = difTime > 0 ? Math.floor(difTime / (1000 * 60)) : 0;

        return (minutes < 60) ? (60 - minutes) / 5 : 0;
    }

    function showAllItems(items) {
        items.slice(1).removeAttr("style");
    }

    function hideFirstItems(items, number) {
        number && items.slice(1, number + 1).attr("style", "display: none");
    }

    function hideLastItems(items, number) {
        number && items.slice(-number).attr("style", "display: none");
    }

    function showTimeSelectHandler() {
        var type = $(this).hasClass("hours-list") ? timeSelectType.Hour : timeSelectType.Minute;
        var allSelectItems = $(this).parent().find("ul.dropdown-menu li");
        var startShiftDate = Relay.DotNetDate.dateStringToDate($("#StartShiftDate", dialog).val());
        var endShiftDate = Relay.DotNetDate.dateStringToDate($("#EndShiftDate", dialog).val());
        var nextShiftDate = Relay.DotNetDate.dateStringToDate($("#NextShiftDate", dialog).val());

        showAllItems(allSelectItems);

        switch (chooseHandlerId) {
            case "StartShiftDate":
                hideFirstItems(allSelectItems, calcHideFirstItems(startLimitDate, startShiftDate, type));
                (type == timeSelectType.Minute) && hideLastItems(allSelectItems, calcHideLastMinutesItems(endLimitDate, startShiftDate));
                break;
            case "EndShiftDate":
                hideFirstItems(allSelectItems, calcHideFirstItems(startShiftDate, endShiftDate, type));
                (type == timeSelectType.Minute) && hideLastItems(allSelectItems, calcHideLastMinutesItems(endLimitDate, endShiftDate));
                break;
            case "NextShiftDate":
                hideFirstItems(allSelectItems, calcHideFirstItems(endShiftDate, nextShiftDate, type));
                (type == timeSelectType.Minute) && hideLastItems(allSelectItems, calcHideLastMinutesItems(endLimitDate, nextShiftDate));
                break;
        }
    }

    $("select.select-list.hours-list", dialog).on("show.bs.select", showTimeSelectHandler);
    $("select.select-list.minutes-list", dialog).on("show.bs.select", showTimeSelectHandler);

    $("select.select-list.hours-list", dialog).on('change', function () {
        if (chooseHandlerId) {
            var dateString = $('#' + chooseHandlerId, dialog).val();
            if (dateString) {
                var date = Relay.DotNetDate.dateStringToDate(dateString);
                date.setHours(Number($(this).val()));
                $('#' + chooseHandlerId, dialog).val(Relay.DotNetDate.dateToDateString(date));

                clearDependentDates(chooseHandlerId);
                updateDateField(chooseHandlerId);

                $("button[type=submit]", dialog).trigger("activate");
            }
        }
    });

    $("select.select-list.minutes-list", dialog).on('change', function () {
        if (chooseHandlerId) {
            var dateString = $('#' + chooseHandlerId, dialog).val();
            if (dateString) {
                var date = Relay.DotNetDate.dateStringToDate(dateString);
                date.setMinutes(Number($(this).val()));
                $('#' + chooseHandlerId, dialog).val(Relay.DotNetDate.dateToDateString(date));

                clearDependentDates(chooseHandlerId);
                updateDateField(chooseHandlerId);

                $("button[type=submit]", dialog).trigger("activate");
            }
        }
    });
    //--------------------------------------------------------------------
    $('input[name=RepeatShift]:radio', dialog).change(function () {
        if ($("#RepeatShiftMultiple", dialog).is(":checked")) {
            $(".repeat-count", dialog).show();
            calcAllowedRepeatTimes();
        }
        else {
            $(".repeat-count", dialog).hide();
            $("#RepeatTimes0", dialog).prop("checked", true);
        }

        offCalendar();
        $(".field-container", dialog).removeClass("active");
    });

    $('input[name=RepeatTimes]:radio', dialog).change(function () {
        $("button[type=submit]", dialog).trigger("activate");
    });

    $(".start-date-btn", dialog).bind("click", function (event) {
        if (startLimitDate) {
            calendar.datepicker("setStartDate", startLimitDate);
        }

        if (endLimitDate) {
            calendar.datepicker("setEndDate", endLimitDate);
        }

        onCalendar();
        $(".field-container", dialog).removeClass("active");
        $(this).parent().addClass("active");
        chooseHandlerId = "StartShiftDate";

        event.stopPropagation();
    });

    $(".end-date-btn", dialog).bind("click", function (event) {
        var startEndDateString = $("#StartShiftDate", dialog).val();

        if (startEndDateString) {
            calendar.datepicker("setStartDate", Relay.DotNetDate.dateStringToDate(startEndDateString));

            if (endLimitDate) {
                calendar.datepicker("setEndDate", endLimitDate);
            }

            onCalendar();
            $(".field-container", dialog).removeClass("active");
            $(this).parent().addClass("active");
            chooseHandlerId = "EndShiftDate";
        }

        event.stopPropagation();
    });

    $(".next-date-btn", dialog).bind("click", function (event) {
        var endShiftDateString = $("#EndShiftDate", dialog).val();

        if (endShiftDateString) {
            calendar.datepicker("setStartDate", Relay.DotNetDate.dateStringToDate(endShiftDateString));

            if (beginningOfNextShiftLimitDate)
            {
                calendar.datepicker("setEndDate", beginningOfNextShiftLimitDate);
            }

            onCalendar();
            $(".field-container", dialog).removeClass("active");
            $(this).parent().addClass("active");
            chooseHandlerId = "NextShiftDate";
        }

        event.stopPropagation();
    });
    //--------------------------------------------------------------------
    function clearDependentDates(id) {
        switch (id) {
            case "StartShiftDate":
                $("#EndShiftDate", dialog).val('');
                updateDateField("EndShiftDate");

            case "EndShiftDate":
                $("#NextShiftDate", dialog).val('');
                updateDateField("NextShiftDate");

            default:
                $("#RepeatShiftNever", dialog).click();
        }
    }

    function updateDateField(id) {
        var date = Relay.DotNetDate.dateStringToDate($('#' + id, dialog).val());

        if (date) {
            $('#' + id + "Day", dialog).text(moment(date).format("DD"));
            $('#' + id + "Month", dialog).text(moment(date).format("MMM"));
            $('#' + id + "Year", dialog).text(moment(date).format("YYYY"));

            $('#' + id + "Hours", dialog).selectpicker('val', [date.getHours()]);
            $('#' + id + "Minutes", dialog).selectpicker('val', [date.getMinutes()]);
        }
        else {
            $('#' + id + "Day", dialog).text("DD");
            $('#' + id + "Month", dialog).text("MMM");
            $('#' + id + "Year", dialog).text("YYYY");

            $('#' + id + "Hours", dialog).selectpicker('val', ['']);
            $('#' + id + "Minutes", dialog).selectpicker('val', ['']);
        }
    }

    function onCalendar() {
        calendarContainer.removeAttr("style");
    }
    function offCalendar() {
        calendarContainer.attr("style", "pointer-events: none;");
    }

    function calcAllowedRepeatTimes() {
        var startShiftDateString = $("#StartShiftDate", dialog).val();
        var endShiftDateString = $("#EndShiftDate", dialog).val();
        var nextShiftDateString = $("#NextShiftDate", dialog).val();

        if (startShiftDateString && endShiftDateString && nextShiftDateString) {

            var startShiftDate = Relay.DotNetDate.dateStringToDate(startShiftDateString);
            var endShiftDate = Relay.DotNetDate.dateStringToDate(endShiftDateString);
            var nextShiftDate = Relay.DotNetDate.dateStringToDate(nextShiftDateString);
            var _endLimitDate = new Date(endLimitDate);

            _endLimitDate.setDate(_endLimitDate.getDate() + 1);

            var fullShiftPeriod = nextShiftDate.getTime() - startShiftDate.getTime();
            var workShiftPeriod = endShiftDate.getTime() - startShiftDate.getTime();
            var leftSwingPeriod = _endLimitDate.getTime() - startShiftDate.getTime();

            var repeatTimes = Math.floor(leftSwingPeriod / fullShiftPeriod) + (leftSwingPeriod % fullShiftPeriod > workShiftPeriod ? 1 : 0);

            $("input[name='RepeatTimes']", dialog).each(function (index) {
                if (Number($(this).val()) < repeatTimes) {
                    $(this).removeAttr("disabled");
                }
                else {
                    $(this).attr("disabled", '');
                }
            });
        }
        else {
            $("input[name='RepeatTimes']", dialog).attr("disabled", '');
        }
    }
    //--------------------------------------------------------------------
});