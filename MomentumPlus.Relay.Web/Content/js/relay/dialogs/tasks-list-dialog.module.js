﻿(function () {
    //============================================================================================
    // config {
    //     topicId: (topic id)
    // }
    //============================================================================================
    var Types = {
        Draft: 1,
        Received: 2,
        ReadOnly: 3
    }
    //=============================== Draft Tasks Dialog =========================================
    function DraftTasksListDialog(topicId, readOnly) {
        this.topicId = topicId;
        this.readOnly = readOnly;
        this.openHandler = null;
        this.closeHandler = null;
    }
    DraftTasksListDialog.prototype.open = function () {
        var _this = this;
        //--------------------------
        var configDialog = {
            topicId: this.topicId,
            readOnly: this.readOnly,
            //---------
            dialogId: "draftTasksListDialog",
            dialogUrl: "/Tasks/DraftTasksListDialog",
            //---------
            addFormUrl: "/Tasks/AddTaskFormDialog",
            //---------
            editFormUrl: "/Tasks/EditTaskFormDialog",
            //---------
            detailsFormUrl: "/Tasks/TaskDetailsDialog"
            //---------
        };
        //--------------------------
        $('#loading-animation').show();
        if (_this.openHandler) {
            _this.openHandler();
        }
        //--------------------------
        $.ajax({
            url: configDialog.dialogUrl + '?TopicId=' + configDialog.topicId,
            method: "GET"
        })
        .done(function (result) {
            var dialog = new _DraftDialog(configDialog);
            dialog.buildDialog(result);
            dialog.closeHandler = _this.closeHandler;
            dialog.open();
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
        .always(function () {
            $('#loading-animation').hide();
        });
        //--------------------------
    };
    DraftTasksListDialog.prototype.setOpenEvent = function (eventHandler) {
        this.openHandler = eventHandler;
    };
    DraftTasksListDialog.prototype.setCloseEvent = function (eventHandler) {
        this.closeHandler = eventHandler;
    };
    //====================================================
    function _DraftDialog(config) {
        this.config = config;
        this.conteiner = null;
        this.slidesContainer = null;

        this.closeHandler = null;
    }
    _DraftDialog.prototype.open = function () {
        //----------- Tasks Messages List --------------
        new Swiper($(".swiper-container", this.conteiner),
            {
                // slidesPerView: 2,
                direction: 'vertical',
                height: 30,
                nextButton: $(".swiper-next", this.conteiner),
                prevButton: $(".swiper-prev", this.conteiner)
            });
        //----------------------------------------------
        $('#' + this.config.dialogId, this.conteiner).modal('show');
    };
    _DraftDialog.prototype.buildDialog = function (windowHtml) {

        var _this = this;

        this.conteiner = document.createElement("div");
        this.conteiner.innerHTML = windowHtml;
        document.body.appendChild(this.conteiner);

        this.slidesContainer = $(".swiper-wrapper", this.conteiner).first();

        if (this.config.readOnly) {
            $(".add-task-btn", this.conteiner).hide();
        }
        else {
            $(".add-task-btn", this.conteiner).click(function () {

                var addBtn = this;
                $(addBtn).prop("disabled", true);;

                var tasksListDialog = Relay.FormDialog({
                    url: _this.config.addFormUrl,
                    method: "GET",
                    data: { TopicId: _this.config.topicId }
                });
                tasksListDialog.setSubmitEvent(function () {
                    _this.reloadTasksList();
                });
                tasksListDialog.setCloseEvent(function () {
                    $(addBtn).prop("disabled", false);;
                });
                tasksListDialog.open();
            });
        }

        this.initEditClick();

        $('#' + this.config.dialogId, this.conteiner).on('hidden.bs.modal', function () {
            $(_this.conteiner).remove();
            if (_this.closeHandler) {
                _this.closeHandler();
            }
        });
    };
    _DraftDialog.prototype.reloadTasksList = function () {
        var _this = this;

        $.ajax({
            url: this.config.dialogUrl + '?TopicId=' + this.config.topicId,
            method: "GET"
        })
        .done(function (result) {
            var slides = $(result).find(".swiper-wrapper");
            _this.slidesContainer.html(slides.html());
            _this.initEditClick();
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        });
    };
    _DraftDialog.prototype.initEditClick = function () {
        var _this = this;
        if (this.config.readOnly) {
            $(".task-name", _this.conteiner).click(function () {
                Relay.FormDialog({
                    url: _this.config.detailsFormUrl,
                    method: "GET",
                    data: { TaskId: $(this).attr("task") }
                }).open();
            });
        }
        else {
            $(".task-name", _this.conteiner).click(function () {
                var tasksListDialog = Relay.FormDialog({
                    url: _this.config.editFormUrl,
                    method: "GET",
                    data: { TaskId: $(this).attr("task") }
                });
                tasksListDialog.setSubmitEvent(_this.reloadTasksList.bind(_this));
                tasksListDialog.open();
            });
        }
    };
    //============================== Received Tasks Dialog =======================================
    function ReceivedTasksListDialog(topicId) {
        this.topicId = topicId;
        this.openHandler = null;
        this.closeHandler = null;
    }
    ReceivedTasksListDialog.prototype.open = function () {
        var _this = this;
        //--------------------------
        var configDialog = {
            topicId: this.topicId,
            //---------
            dialogId: "receivedTasksListDialog",
            dialogUrl: "/Tasks/ReceivedTasksListDialog",
            //---------
            completeFormUrl: "/Tasks/CompleteTaskDialog"
            //---------
        };
        //--------------------------
        $('#loading-animation').show();
        if (_this.openHandler) {
            _this.openHandler();
        }
        //--------------------------
        $.ajax({
            url: configDialog.dialogUrl + '?TopicId=' + configDialog.topicId,
            method: "GET"
        })
        .done(function (result) {
            var dialog = new _ReceivedDialog(configDialog);
            dialog.buildDialog(result);
            dialog.closeHandler = _this.closeHandler;
            dialog.open();
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
        .always(function () {
            $('#loading-animation').hide();
        });
        //--------------------------
    };
    ReceivedTasksListDialog.prototype.setOpenEvent = function (eventHandler) {
        this.openHandler = eventHandler;
    };
    ReceivedTasksListDialog.prototype.setCloseEvent = function (eventHandler) {
        this.closeHandler = eventHandler;
    };
    //====================================================
    function _ReceivedDialog(config) {
        this.config = config;
        this.conteiner = null;
        this.slidesContainer = null;

        this.closeHandler = null;
    }
    _ReceivedDialog.prototype.open = function () {
        //----------- Tasks Messages List --------------
        new Swiper($(".swiper-container", this.conteiner),
            {
                // slidesPerView: 2,
                direction: 'vertical',
                height: 30,
                nextButton: $(".swiper-next", this.conteiner),
                prevButton: $(".swiper-prev", this.conteiner)
            });
        //----------------------------------------------
        $('#' + this.config.dialogId, this.conteiner).modal('show');
    };
    _ReceivedDialog.prototype.buildDialog = function (windowHtml) {

        var _this = this;

        this.conteiner = document.createElement("div");
        this.conteiner.innerHTML = windowHtml;
        document.body.appendChild(this.conteiner);

        this.slidesContainer = $(".swiper-wrapper", this.conteiner).first();

        this.initCompleteClick();

        $('#' + this.config.dialogId, this.conteiner).on('hidden.bs.modal', function () {
            $(_this.conteiner).remove();
            if (_this.closeHandler) {
                _this.closeHandler();
            }
        });
    };
    _ReceivedDialog.prototype.reloadTasksList = function () {
        var _this = this;

        $.ajax({
            url: this.config.dialogUrl + '?TopicId=' + this.config.topicId,
            method: "GET"
        })
        .done(function (result) {
            var slides = $(result).find(".swiper-wrapper");
            _this.slidesContainer.html(slides.html());
            _this.initCompleteClick();
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        });
    };
    _ReceivedDialog.prototype.initCompleteClick = function () {
        var _this = this;

        $(".task-name", _this.conteiner).click(function () {
            var complitTaskDialog = Relay.FormDialog({
                url: _this.config.completeFormUrl,
                method: "GET",
                data: { TaskId: $(this).attr("task") }
            });
            complitTaskDialog.setSubmitEvent(_this.reloadTasksList.bind(_this));
            complitTaskDialog.open();
        });
    };
    //============================================================================================
    window.Relay = window.Relay || {};
    window.Relay.TasksListDialog = function (type, topicId) {
        switch (type) {
            case Types.Draft:
                return new DraftTasksListDialog(topicId, false);
            case Types.Received:
                return new ReceivedTasksListDialog(topicId);
            case Types.ReadOnly:
                return new DraftTasksListDialog(topicId, true);
        }
    };
    //---------------------
    window.Relay.TasksListDialog.Types = Types;
    //============================================================================================
})();