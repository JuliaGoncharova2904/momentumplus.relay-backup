﻿(function ()
{
    'use strict';

    angular
          .module('RelayMultiTenantApp')
          .controller('SignUpCtrl', SignUpCtrl);

    SignUpCtrl.$inject = ['$scope'];

    function SignUpCtrl($scope)
    {
        /* jshint validthis:true */
        var ctrl = this;

        ///////////////////////////////////////////////////////////////////////////

        var priceConstants = {
            Manager: {
                BasicFirst: 15,
                PremiumFirst: 25,
                UltimateFirst: 25,
                AdditionalManager: 70
            },
            User: {
                Basic: 25,
                Premium: 40,
                Ultimate: 65
            }
        }

        ///////////////////////////////////////////////////////////////////////////

        ctrl.SignUpModel = {
            BillingPeriodType: 1,
            SimpleUsersCount: 5,
            ManagersUsersCount: 1,
            UserSubscriptionType: null,
            UserSubscriptionPrices: {
                SinglePrice: priceConstants.User.Basic + priceConstants.Manager.BasicFirst,
                B2bPrice: priceConstants.User.Basic * 2 + priceConstants.Manager.BasicFirst,
                TeamPrice: priceConstants.User.Basic * 5 + priceConstants.Manager.BasicFirst
            },
            SubscriptionLevelPrices: {
                Basic: priceConstants.User.Basic + priceConstants.Manager.BasicFirst,
                Premium: priceConstants.User.Premium + priceConstants.Manager.PremiumFirst,
                Ultimate: priceConstants.User.Ultimate + priceConstants.Manager.UltimateFirst
            },
            SubscriptionLevelType: null
        };

        ctrl.CurrentStepNumber = 0;

        ///////////////////////////////////////////////////////////////////////////

        ctrl.signUp = signUp;

        ctrl.changeStep = changeStep;

        ctrl.goToSecondStep = goToSecondStep;

        ctrl.changeUserSubscriptionType = changeUserSubscriptionType;

        ctrl.changeSubscriptionLevelType = changeSubscriptionLevelType;


        ///////////////////////////////////////////////////////////////////////////

        activate();

        ///////////////////////////////////////////////////////////////////////////


        function activate()
        {
        }

        function signUp()
        {
        }

        function changeUserSubscriptionType(subscriptionType)
        {
            ctrl.SignUpModel.UserSubscriptionType = subscriptionType;
            //if (ctrl.SignUpModel.UserSubscriptionType !== subscriptionType)
            //{
            //    ctrl.SignUpModel.UserSubscriptionType = subscriptionType;
            //}
            //else
            //{
            //    ctrl.SignUpModel.UserSubscriptionType = null;
            //}
        }

        function changeSubscriptionLevelType(subscriptionLevelType)
        {

            ctrl.SignUpModel.SubscriptionLevelType = subscriptionLevelType;
            //if (ctrl.SignUpModel.SubscriptionLevelType !== subscriptionLevelType)
            //{
            //    ctrl.SignUpModel.SubscriptionLevelType = subscriptionLevelType;
            //}
            //else
            //{
            //    ctrl.SignUpModel.SubscriptionLevelType = null;
            //}
        }


        function changeStep(stepNumber)
        {
            ctrl.CurrentStepNumber = stepNumber;
        }

        function goToSecondStep()
        {
            ctrl.CurrentStepNumber = 1;

            if (ctrl.SignUpModel.UserSubscriptionType === 0)
            {
                ctrl.SignUpModel.SubscriptionLevelPrices.Basic = priceConstants.User.Basic + priceConstants.Manager.BasicFirst;
                ctrl.SignUpModel.SubscriptionLevelPrices.Premium = priceConstants.User.Premium + priceConstants.Manager.PremiumFirst;
                ctrl.SignUpModel.SubscriptionLevelPrices.Ultimate = priceConstants.User.Ultimate + priceConstants.Manager.UltimateFirst;
            }

            if (ctrl.SignUpModel.UserSubscriptionType === 1)
            {
                ctrl.SignUpModel.SubscriptionLevelPrices.Basic = priceConstants.User.Basic * 2 + priceConstants.Manager.BasicFirst;
                ctrl.SignUpModel.SubscriptionLevelPrices.Premium = priceConstants.User.Premium * 2 + priceConstants.Manager.PremiumFirst;
                ctrl.SignUpModel.SubscriptionLevelPrices.Ultimate = priceConstants.User.Ultimate * 2 + priceConstants.Manager.UltimateFirst;
            }
        }


        ///////////////////////////////////////////////////////////////////////////


        $scope.$watch('ctrl.SignUpModel.ManagersUsersCount', function ()
        {
            if (ctrl.SignUpModel.ManagersUsersCount >= 2)
            {
                ctrl.SignUpModel.UserSubscriptionPrices.TeamPrice = priceConstants.User.Basic * ctrl.SignUpModel.SimpleUsersCount + priceConstants.Manager.BasicFirst + (ctrl.SignUpModel.ManagersUsersCount - 1) * priceConstants.Manager.AdditionalManager;

                ctrl.SignUpModel.SubscriptionLevelPrices.Basic = priceConstants.User.Basic * ctrl.SignUpModel.SimpleUsersCount + priceConstants.Manager.BasicFirst + (ctrl.SignUpModel.ManagersUsersCount - 1) * priceConstants.Manager.AdditionalManager;
                ctrl.SignUpModel.SubscriptionLevelPrices.Premium = priceConstants.User.Premium * ctrl.SignUpModel.SimpleUsersCount + priceConstants.Manager.PremiumFirst + (ctrl.SignUpModel.ManagersUsersCount - 1) * priceConstants.Manager.AdditionalManager;
                ctrl.SignUpModel.SubscriptionLevelPrices.Ultimate = priceConstants.User.Ultimate * ctrl.SignUpModel.SimpleUsersCount + priceConstants.Manager.UltimateFirst + (ctrl.SignUpModel.ManagersUsersCount - 1) * priceConstants.Manager.AdditionalManager;
            }
            else
            {
                ctrl.SignUpModel.UserSubscriptionPrices.TeamPrice = priceConstants.User.Basic * ctrl.SignUpModel.SimpleUsersCount + priceConstants.Manager.BasicFirst;

                ctrl.SignUpModel.SubscriptionLevelPrices.Basic = priceConstants.User.Basic * ctrl.SignUpModel.SimpleUsersCount + priceConstants.Manager.BasicFirst;
                ctrl.SignUpModel.SubscriptionLevelPrices.Premium = priceConstants.User.Premium * ctrl.SignUpModel.SimpleUsersCount + priceConstants.Manager.PremiumFirst;
                ctrl.SignUpModel.SubscriptionLevelPrices.Ultimate = priceConstants.User.Ultimate * ctrl.SignUpModel.SimpleUsersCount + priceConstants.Manager.UltimateFirst;
            }
        });


        $scope.$watch('ctrl.SignUpModel.SimpleUsersCount', function ()
        {
            if (ctrl.SignUpModel.ManagersUsersCount < 2)
            {
                ctrl.SignUpModel.UserSubscriptionPrices.TeamPrice = priceConstants.User.Basic * ctrl.SignUpModel.SimpleUsersCount + priceConstants.Manager.BasicFirst;

                ctrl.SignUpModel.SubscriptionLevelPrices.Basic = priceConstants.User.Basic * ctrl.SignUpModel.SimpleUsersCount + priceConstants.Manager.BasicFirst;
                ctrl.SignUpModel.SubscriptionLevelPrices.Premium = priceConstants.User.Premium * ctrl.SignUpModel.SimpleUsersCount + priceConstants.Manager.PremiumFirst;
                ctrl.SignUpModel.SubscriptionLevelPrices.Ultimate = priceConstants.User.Ultimate * ctrl.SignUpModel.SimpleUsersCount + priceConstants.Manager.UltimateFirst;
            }
            else
            {
                ctrl.SignUpModel.UserSubscriptionPrices.TeamPrice = priceConstants.User.Basic * ctrl.SignUpModel.SimpleUsersCount + priceConstants.Manager.BasicFirst + (ctrl.SignUpModel.ManagersUsersCount - 1) * priceConstants.Manager.AdditionalManager;

                ctrl.SignUpModel.SubscriptionLevelPrices.Basic = priceConstants.User.Basic * ctrl.SignUpModel.SimpleUsersCount + priceConstants.Manager.BasicFirst + (ctrl.SignUpModel.ManagersUsersCount - 1) * priceConstants.Manager.AdditionalManager;
                ctrl.SignUpModel.SubscriptionLevelPrices.Premium = priceConstants.User.Premium * ctrl.SignUpModel.SimpleUsersCount + priceConstants.Manager.PremiumFirst + (ctrl.SignUpModel.ManagersUsersCount - 1) * priceConstants.Manager.AdditionalManager;
                ctrl.SignUpModel.SubscriptionLevelPrices.Ultimate = priceConstants.User.Ultimate * ctrl.SignUpModel.SimpleUsersCount + priceConstants.Manager.UltimateFirst + (ctrl.SignUpModel.ManagersUsersCount - 1) * priceConstants.Manager.AdditionalManager;
            }
        });
    }

})();
