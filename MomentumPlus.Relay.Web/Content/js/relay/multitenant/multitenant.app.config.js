﻿(function ()
{
    'use strict';

    var appModule = angular.module('RelayMultiTenantApp');

    appModule.config(configure);

    appModule.run(run);

    /* @ngInject */
    function configure($stateProvider, $urlRouterProvider, $locationProvider)
    {
        $urlRouterProvider.otherwise('/step1');

        $stateProvider
            .state('app', {
                abstract: true,
                template: '<ui-view/>'
            })
            .state('app.step1', {
                url: '/step1',
                templateUrl: '/Registration/Step1'
            })
            .state('app.step2', {
                url: "/step2",
                templateUrl: '/Registration/Step2'
            })
            .state('app.step3', {
                url: "/step3",
                templateUrl: '/Registration/Step3'
            })
            .state('app.step4', {
                url: "/step4",
                templateUrl: '/Registration/Step4'
            });


        //$locationProvider.html5Mode(true);

    }

    ///////////////////////////////////////////////////////////////////////////

    function run($rootScope, $state, $stateParams)
    {

    }

})();