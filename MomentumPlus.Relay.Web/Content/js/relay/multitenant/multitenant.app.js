﻿(function ()
{
    'use strict';

    angular.module('RelayMultiTenantApp', [
        'ui.router',
        'rzModule'
    ]);

})();

