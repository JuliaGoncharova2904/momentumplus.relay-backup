﻿$(document).ready(function () {
    //=================================================================================
    var params = {
        ProjectId: "ProjectId",
    };

    var projectPanel = Relay.ProjectsPanel($("#projectsPanel"), params.ProjectId);
    var teamPanel = Relay.TeamPanel($("#projectFollowersPanel"), params.ProjectId);
    var tasksPanel = Relay.TasksPanel($("#projectTasksPanel"), params.ProjectId);
    //=================================================================================
});