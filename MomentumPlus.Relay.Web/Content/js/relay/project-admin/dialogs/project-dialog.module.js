﻿; (function () {
    //=============================================================
    var dialog = $("#addProjectFormDialog , #editProjectFormDialog");
    var statusInput = $('#ProjectStatus', dialog);
    var startDateInput = $('#StartDate', dialog);
    var endDateInput = $('#EndDate', dialog);
    //------------------------------------------------------
    $('#StartDate , #EndDate', dialog).datepicker({
        format: "dd M yyyy",
        container: "#addProjectFormDialog , #editProjectFormDialog",
        todayBtn: "linked",
        todayHighlight: true
    })
    .on('changeDate', function () {
        $(this).datepicker('hide');
        $("button[type=submit]", dialog).trigger("activate");
        updateProjectState();
    });
    //------------------------------------------------------
    function updateProjectState() {
        var startDate = startDateInput.datepicker("getDate");
        var endDate = endDateInput.datepicker("getDate");

        if (startDate && endDate && startDate.getTime() <= endDate.getTime()) {
            var curretnDate = new Date();

            if (startDate.getTime() > curretnDate.getTime()) {
                statusInput.val("Not Started");
            }
            else if (startDate.getTime() <= curretnDate.getTime() && endDate.getTime() >= curretnDate.getTime()) {
                statusInput.val("Active");
            }
            else if (endDate.getTime() < curretnDate.getTime()) {
                statusInput.val("Complete");
            }
            else {
                statusInput.val('ERROR');
            }

        }
        else {
            statusInput.val('');
        }
    }
    //=============================================================
})();