﻿; (function (window) {
    //=================================================================================
    function errorHandler() {
        var errorDialog = Relay.ErrorDialog();
        errorDialog.setCloseEvent(function () {
            location.reload();
        });
        errorDialog.open();
    }
    //=================================================================================
    function ProjectSlide(slide, projectIdKey) {
        this.slide = slide;
        this.isFullMode = false;
        this.projectId = $(slide).attr("projectId");
        this.projectIdKey = projectIdKey;

        $(slide).children().click(ProjectSlide_SelectHandler.bind(this));

        var hashModel = Relay.UrlHash.getHashModel();
        ProjectSlide_UpdateProjectState.call(this, hashModel[this.projectIdKey]);

        Relay.UrlHash.addChangeEvent(function (model, modelChangesMap) {
            if (modelChangesMap[this.projectIdKey]) {
                ProjectSlide_UpdateProjectState.call(this, model[this.projectIdKey]);
            }
        }.bind(this));
    }
    function ProjectSlide_SelectHandler() {
        var hashModel = Relay.UrlHash.getHashModel();
        hashModel[this.projectIdKey] = this.projectId;
        Relay.UrlHash.updateHashModel(hashModel);
        this.loadFullSlide();
    }
    function ProjectSlide_UnSelectHandler() {
        var hashModel = Relay.UrlHash.getHashModel();
        hashModel[this.projectIdKey] = null;
        Relay.UrlHash.updateHashModel(hashModel);
        this.loadSlide();
    }
    function ProjectSlide_UpdateProjectState(projectId) {
        if (this.projectId === projectId) {
            this.isFullMode = true;
            this.loadFullSlide();
        }
        else if (this.isFullMode && this.projectId !== projectId) {
            this.isFullMode = false;
            this.loadSlide();
        }
    }
    ProjectSlide.prototype.loadSlide = function () {
        var _this = this;

        $(this.slide).empty();

        Relay.LoadingAnimation($.ajax({
            url: "/ProjectAdmin/ProjectSlide",
            method: "GET",
            data: { projectId: this.projectId }
        })
        .done(function (newContent) {
            var projectSlide = $(newContent);
            $(_this.slide).html(projectSlide);
            projectSlide.click(ProjectSlide_SelectHandler.bind(_this));
        })
        .fail(errorHandler), 200);
    };
    ProjectSlide.prototype.loadFullSlide = function () {
        var _this = this;

        $(this.slide).empty();

        Relay.LoadingAnimation($.ajax({
            url: "/ProjectAdmin/FullProjectSlide",
            method: "GET",
            data: { projectId: this.projectId }
        })
        .done(function (newContent) {
            var projectFullSlide = $(newContent);
            $(_this.slide).html(projectFullSlide);
            projectFullSlide.find(".project-image-block").click(_this.openEditDialog.bind(_this));
            projectFullSlide.find(".close-full-slide").click(ProjectSlide_UnSelectHandler.bind(_this));
        })
        .fail(errorHandler), 200);
    };
    ProjectSlide.prototype.openEditDialog = function () {
        var _this = this;

        var formDialog = Relay.FormDialog({
            url: "/ProjectAdmin/EditProjectDialog",
            method: "GET",
            dialogId: "editProjectFormDialog",
            data: { projectId: this.projectId }
        });
        formDialog.setSubmitEvent(function () {
            _this.loadFullSlide();
        });
        formDialog.open();
    };
    //=================================================================================
    function ProjectsPanel(panel, projectIdKey) {
        this.panel = panel;
        this.projects = [];
        this.projectIdKey = projectIdKey;
        this.projectsSlider = null;

        this.swiperBreakpoints = {
            320: {
                slidesPerView: 1,
                spaceBetweenSlides: 10
            },
            480: {
                slidesPerView: 2,
                spaceBetweenSlides: 20
            },
            640: {
                slidesPerView: 3,
                spaceBetweenSlides: 30
            },
            991: {
                slidesPerView: 4,
                spaceBetweenSlides: 30
            },
            1199: {
                slidesPerView: 5,
                spaceBetweenSlides: 30
            }
        };

        ProjectsPanel_Initialization.call(this);

        var hashModel = Relay.UrlHash.getHashModel();
        ProjectsPanel_UpdateProjectsState.call(this, hashModel[projectIdKey]);

        Relay.UrlHash.addChangeEvent(function (model, modelChangesMap) {
            if (modelChangesMap[projectIdKey]) {
                ProjectsPanel_UpdateProjectsState.call(this, model[projectIdKey]);
            }
        }.bind(this));
    }
    ProjectsPanel.prototype.turnOnFullSlideMode = function (projectId) {
        //----------------------------------------------------
        this.projectsSlider.params.slidesPerView = 1;
        this.projectsSlider.params.breakpoints = null;
        this.projectsSlider.update(true);
        //----------------------------------------------------
        for (var i = 0; i < this.projects.length; ++i) {
            if (this.projects[i].projectId === projectId) {
                this.projectsSlider.slideTo(i, 1, false);
            }
        }
        //----------------------------------------------------
        this.projectsSlider.lockSwipes();
        //----------------------------------------------------
        ProjectsPanel_HideSwiperButtons.call(this, true);
        //----------------------------------------------------
    };
    ProjectsPanel.prototype.turnOffFullSlideMode = function () {
        //----------------------------------------------------
        this.projectsSlider.params.slidesPerView = 6;
        this.projectsSlider.params.breakpoints = this.swiperBreakpoints;
        this.projectsSlider.update(true);
        this.projectsSlider.unlockSwipes();
        //----------------------------------------------------
        ProjectsPanel_HideSwiperButtons.call(this, false);
        //----------------------------------------------------
    };
    ProjectsPanel.prototype.addProject = function () {
        var _this = this;

        var formDialog = Relay.FormDialog({
            url: "/ProjectAdmin/AddProjectDialog",
            method: "GET",
            dialogId: "addProjectFormDialog"
        });
        formDialog.setSubmitEvent(function () {
            _this.reload();
        });
        formDialog.open();
    };
    ProjectsPanel.prototype.reload = function () {
        var _this = this;

        $.ajax({
            url: "/ProjectAdmin/ProjectsPanel",
            method: "GET"
        })
        .done(function (newContent) {
            var projectPanelContent = $(newContent).children();
            $(_this.panel).html(projectPanelContent);
            ProjectsPanel_Initialization.call(_this);
        })
        .fail(errorHandler);
    };
    function ProjectsPanel_UpdateProjectsState(projectId) {
        this.projectsSlider.container.length && this.turnOffFullSlideMode();
        projectId && this.turnOnFullSlideMode(projectId);
    }
    function ProjectsPanel_Initialization() {
        var _this = this;

        $(".panel-body", _this.panel).removeAttr("style");
        //------- Init Swiper --------------
        this.projectsSlider = new Swiper($(".projects-swiper", this.panel), {
            slidesPerView: 6,
            spaceBetween: 5,
            centeredSlides: false,
            hashnav: false,
            prevButton: $(".projects-swiper-prev", this.panel),
            nextButton: $(".projects-swiper-next", this.panel),
            breakpoints: this.swiperBreakpoints
        });
        //----------------------------------
        this.projects = [];

        $(".swiper-slide", this.panel).each(function (index, item) {
            _this.projects.push(new ProjectSlide(item, _this.projectIdKey));
        });
        //----------------------------------
        $(".project-create-btn", this.panel).click(this.addProject.bind(this));
        //----------------------------------
    }
    function ProjectsPanel_HideSwiperButtons(state) {
        if (state) {
            $(".projects-swiper-prev", this.panel).attr("style", "display: none;");
            $(".projects-swiper-next", this.panel).attr("style", "display: none;");
        }
        else {
            $(".projects-swiper-prev", this.panel).removeAttr("style");
            $(".projects-swiper-next", this.panel).removeAttr("style");
        }
    }
    //=================================================================================
    window.Relay = window.Relay || {};
    window.Relay.ProjectsPanel = function (panel, projectIdKey) {
        return new ProjectsPanel(panel, projectIdKey);
    }
    //=================================================================================
})(window);