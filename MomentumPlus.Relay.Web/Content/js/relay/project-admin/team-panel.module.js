﻿; (function (window) {
    //=================================================================================
    function TeamPanel(panel, projectIdKey) {
        this.panel = panel;
        this.followersContainer = $(".panel-body", panel);

        this.hashParams = {
            ProjectId: projectIdKey,
            Page: "TeamPage",
            PageSize: "TeamPageSize",
        };

        this.pagination = new Relay.Pagination($(".followers-footer", panel), this.hashParams.Page, this.hashParams.PageSize);

        this.addFollowerBtn = $(".follower-add-btn", this.panel);
        this.addFollowerBtn.on("click", this.addFollower.bind(this));

        Relay.UrlHash.addChangeEvent(function (model, modelChangesMap) {
            if (modelChangesMap[this.hashParams.ProjectId]
                || modelChangesMap[this.hashParams.Page]
                || modelChangesMap[this.hashParams.PageSize]) {

                this.reload(model[this.hashParams.ProjectId],
                            model[this.hashParams.Page],
                            model[this.hashParams.PageSize])
                    .done();
            }
        }.bind(this));

        TeamPanel_Initialization.call(this);

        var hashModel = Relay.UrlHash.getHashModel();
        this.reload(hashModel[this.hashParams.ProjectId],
                    hashModel[this.hashParams.Page],
                    hashModel[this.hashParams.PageSize])
            .done();
    }
    TeamPanel.prototype.addFollower = function () {
        var _this = this;
        var hashModel = Relay.UrlHash.getHashModel();
        var projectId = hashModel[this.hashParams.ProjectId];

        if (projectId) {
            var followersDialog = Relay.FollowersDialog(projectId);
            followersDialog.setCloseEvent(function () {
                _this.pagination.close();

                _this.reload(projectId,
                                hashModel[_this.hashParams.Page], 
                                hashModel[_this.hashParams.PageSize])
                    .done();
            });
            followersDialog.open();
        }
    };
    TeamPanel.prototype.reload = function (projectId, page, pageSize) {
        var _this = this;

        if (!projectId)
            this.addFollowerBtn.attr("style", "display: none;");

        return $.Deferred(function (defer) {
            Relay.LoadingAnimation($.ajax({
                url: "/ProjectFollowers/FollowersPanel",
                method: "GET",
                data: {
                    projectId: projectId,
                    page: page,
                    pageSize: pageSize
                }
            })
            .done(function (newContent) {
                var folovers = $(newContent);
                _this.followersContainer.html(folovers);
                TeamPanel_Initialization.call(_this);
                (!projectId)
                    ? _this.pagination.close()
                    : _this.pagination.open(folovers.attr("counter"));
                defer.resolve();
            })
            .fail(function () {
                var errorDialog = Relay.ErrorDialog();
                errorDialog.setCloseEvent(function () {
                    location.reload();
                });
                errorDialog.open();
            }), 300)
        }).promise();
    };
    function TeamPanel_Initialization() {
        var _this = this;

        var hashModel = Relay.UrlHash.getHashModel();
        //----------------------------------
        if (hashModel[this.hashParams.ProjectId]) {
            this.addFollowerBtn.removeAttr("style");
        }
        else {
            this.addFollowerBtn.attr("style", "display: none;");
        }
        //----------------------------------
    } 
    //=================================================================================
    window.Relay = window.Relay || {};
    window.Relay.TeamPanel = function (panel, projectIdKey) {
        return new TeamPanel(panel, projectIdKey);
    }
    //=================================================================================
})(window);