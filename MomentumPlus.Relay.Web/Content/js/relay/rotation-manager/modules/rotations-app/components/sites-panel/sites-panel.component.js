﻿; (function () {
    "use strict";

    angular
      .module("RotationsApp")
      .component("sitesPanel", {
          templateUrl: "/Content/js/relay/rotation-manager/modules/rotations-app/components/sites-panel/sites-panel.template.html",
          controller: SitesPanelController
      });

    SitesPanelController.$inject = ["$scope", "$http", "$location"];
    function SitesPanelController($scope, $http, $location) {

        var ctrl = this;
        ctrl.http = $http;
        ctrl.scope = $scope;
        ctrl.location = $location;
        ctrl.currentPage = $location.search().sitePage;

        ctrl.vm = {
            chosenSiteId: ctrl.location.search().siteId,
            page: null,
            errorMsg: ''
        };

        if (ctrl.currentPage === undefined)
            $location.search('sitePage', 1);

        ctrl.chooseSite = function (siteId) {
            if (ctrl.vm.chosenSiteId != siteId) {
                $location.search('siteId', siteId);
            }
        };

        ctrl.showPrevPage = function () {
            if (!ctrl.vm.page.IsFirstPage) {
                var params = ctrl.location.search();
                $location.search('sitePage', +params.sitePage - 1);
            }
        };

        ctrl.showNextpage = function () {
            if (!ctrl.vm.page.IsLastPage) {
                var params = ctrl.location.search();
                $location.search('sitePage', +params.sitePage + 1);
            }
        }

        $scope.$watch(function () { return $location.search() }, function () {
            UpdateSitesPanel(ctrl);
        }, true);

        //--- Open modal dialog for site creating --
        ctrl.createSiteDialog = function () {
            var formDialog = Relay.FormDialog({
                url: "/Sites/CreateSiteDialog",
                method: "GET",
                dialogId: "siteFormDialog"
            });
            formDialog.setSubmitEvent(function () {
                UpdateSitesPanel(ctrl, true);
            });
            formDialog.open();
        };

        ctrl.editSiteDialog = function (siteId) {
            var formDialog = Relay.FormDialog({
                url: "/Sites/EditSiteDialog?SiteId=" + siteId,
                method: "GET",
                dialogId: "siteFormDialog"
            });
            formDialog.setSubmitEvent(function () {
                UpdateSitesPanel(ctrl, true);
            });
            formDialog.open();
        };

        UpdateSitesPanel(ctrl, true);

    }

    function UpdateSitesPanel(ctrl, reload) {

        var params = ctrl.location.search();

        if (reload || ctrl.currentPage !== params.sitePage) {
            ctrl.http.post("/Sites/GetAllSites", { page: params.sitePage })
                .then(function (response) {
                    ctrl.vm.errorMsg = null;
                    ctrl.vm.page = response.data;
                },
                function (response) {
                    ctrl.vm.page = null;
                    ctrl.vm.errorMsg = "Server error: " + response.status;
                });
        }

        ctrl.currentPage = params.sitePage;
        ctrl.vm.chosenSiteId = params.siteId;
    }

})();