﻿; (function () {
    "use strict";

    angular.
      module("RotationsApp").
      component("rotationsPanel", {
          templateUrl: "/Content/js/relay/rotation-manager/modules/rotations-app/components/rotations-panel/rotations-panel.template.html",
          controller: RotationsPanelController
      });

    RotationsPanelController.$inject = ["$scope", "$http", "$location"];
    function RotationsPanelController($scope, $http, $location) {

        var ctrl = this;
        ctrl.http = $http;
        ctrl.scope = $scope;
        ctrl.location = $location;
        ctrl.currentPage = $location.search().rotationPage;
        ctrl.currentPositionId = $location.search().positionId;

        ctrl.vm = {
            page: null,
            errorMsg: null
        };

        if (ctrl.currentPage === undefined)
            $location.search('rotationPage', 1);

        ctrl.showPrevPage = function () {
            if (!ctrl.vm.page.IsFirstPage) {
                var params = ctrl.location.search();
                $location.search('rotationPage', +params.rotationPage - 1);
            }
        };

        ctrl.showNextpage = function () {
            if (!ctrl.vm.page.IsLastPage) {
                var params = ctrl.location.search();
                $location.search('rotationPage', +params.rotationPage + 1);
            }
        }

        $scope.$watch(function () { return $location.search() }, function () {
            UpdateRotationsPanel(ctrl);
        }, true);

        ctrl.createRotationDialog = function () {
            var formDialog = Relay.FormDialog({
                url: "/Rotations/CreateRotationDialog",
                method: "GET",
                dialogId: "createRotationFormDialog",
                data: { PositionId: ctrl.currentPositionId }
            });
            formDialog.setSubmitEvent(function () {
                UpdateRotationsPanel(ctrl, true);
            });
            formDialog.open();
        };

        ctrl.editRotationDialog = function (rotationId) {
            var formDialog = Relay.FormDialog({
                url: "/Rotations/EditRotationDialog?RotationId=" + rotationId,
                method: "GET",
                dialogId: "editRotationFormDialog"
            });
            formDialog.setSubmitEvent(function () {
                UpdateRotationsPanel(ctrl, true);
            });
            formDialog.open();
        };

        UpdateRotationsPanel(ctrl, true);
    }

    function UpdateRotationsPanel(ctrl, reload) {

        var params = ctrl.location.search();

        if (params.positionId && (reload || ctrl.currentPage != params.rotationPage || ctrl.currentPositionId != params.positionId)) {

            if (ctrl.currentPositionId != params.positionId && params.rotationPage != 1) {
                ctrl.location.search('rotationPage', 1);
                return;
            }

            ctrl.http.post("/Rotations/GetRotationsForPosition", { page: params.rotationPage, positionId: params.positionId })
                .then(function (response) {
                    ctrl.vm.errorMsg = null;
                    ctrl.vm.page = response.data;
                },
                function (response) {
                    ctrl.vm.page = null;
                    ctrl.vm.errorMsg = "Server error: " + response.status;
                });
        }

        ctrl.currentPage = params.rotationPage;
        ctrl.currentPositionId = params.positionId;
    }

})();