﻿angular.module('RotationsApp', [])

    .component("rotationsApp", {

        template:   '<div class ="row column-row">' +
                        '<div class ="col-md-4">' +
                            '<sites-panel>Loading...</sites-panel>' +
                        '</div>' +
                        '<div class ="col-md-4">' +
                            '<positions-panel>Loading...</positions-panel>' +
                        '</div>' +
                        '<div class ="col-md-4">' +
                            '<rotations-panel>Loading...</rotations-panel>' +
                        '</div>' +
                    '</div>'
    });
