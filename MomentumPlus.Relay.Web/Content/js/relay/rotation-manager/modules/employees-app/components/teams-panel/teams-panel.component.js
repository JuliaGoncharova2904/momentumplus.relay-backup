﻿; (function () {
    "use strict";

    angular
      .module("employeesMenu")

      .component("teamsPanel", {
          templateUrl: "/Content/js/relay/rotation-manager/modules/employees-app/components/teams-panel/teams-panel.template.html",
          controller: TeamsPanelController
      });

    TeamsPanelController.$inject = ["$scope", "$http", "$location"];
    function TeamsPanelController($scope, $http, $location) {

        var ctrl = this;
        ctrl.http = $http;
        ctrl.scope = $scope;
        ctrl.location = $location;
        ctrl.currentPage = $location.search().teamPage;

        ctrl.vm = {
            chosenTeamId: ctrl.location.search().teamId,
            page: null,
            errorMsg: ''
        };

        if (ctrl.currentPage == undefined)
            $location.search('teamPage', 1);

        ctrl.chooseTeam = function (teamId) {
            if (ctrl.vm.chosenTeamId != teamId) {
                $location.search('teamId', teamId);
            }
        };

        ctrl.showPrevPage = function () {
            if (!ctrl.vm.page.IsFirstPage) {
                var params = ctrl.location.search();
                $location.search('teamPage', +params.teamPage - 1);
            }
        };

        ctrl.showNextpage = function () {
            if (!ctrl.vm.page.IsLastPage) {
                var params = ctrl.location.search();
                $location.search('teamPage', +params.teamPage + 1);
            }
        }

        $scope.$watch(function () { return $location.search() }, function () {
            update(ctrl);
        }, true);

        //--- Open modal dialog for team creating --
        ctrl.createTeamDialog = function () {
            var formDialog = Relay.FormDialog({
                url: "/Teams/CreateTeamDialog",
                method: "GET",
                dialogId: "teamFormDialog"
            });
            formDialog.setSubmitEvent(function () {
                update(ctrl, true);
            });
            formDialog.open();
        };

        ctrl.editTeamDialog = function (teamId) {
            var formDialog = Relay.FormDialog({
                url: "/Teams/EditTeamDialog?TeamId=" + teamId,
                method: "GET",
                dialogId: "teamFormDialog"
            });
            formDialog.setSubmitEvent(function () {
                update(ctrl, true);
            });
            formDialog.open();
        };

        update(ctrl, true);
    }

    function update(ctrl, reload) {

        var params = ctrl.location.search();

        if (reload || ctrl.currentPage !== params.teamPage) {
            ctrl.http.post("/Teams/GetAllTeams", { page: params.teamPage })
                    .then(function (response) {
                        ctrl.vm.errorMsg = null;
                        ctrl.vm.page = response.data;
                    },
                    function (response) {
                        reject("Server error: " + response.status);
                        ctrl.vm.page = null;
                        ctrl.vm.errorMsg = "Server error: " + response.status;
                    });
        }

        ctrl.currentPage = params.teamPage;
        ctrl.vm.chosenTeamId = params.teamId;
    }

})();