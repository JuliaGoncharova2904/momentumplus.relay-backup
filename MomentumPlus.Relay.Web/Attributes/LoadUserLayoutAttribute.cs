﻿using System.Web.Mvc;

namespace MomentumPlus.Relay.Web.Attributes
{
    public class LoadUserLayoutAttribute : ActionFilterAttribute
    {
        private readonly string _styleThemeName;
        public LoadUserLayoutAttribute()
        {
            _styleThemeName = "white-theme";
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            var result = filterContext.Result as ViewResult;
            if (result != null)
            {
                result.ViewBag.styleTheme = _styleThemeName;
            }
        }
    }
}