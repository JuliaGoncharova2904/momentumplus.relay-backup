﻿using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Web.Attributes
{
    public class AuthAuthorizeAttribute : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var httpContext = filterContext.HttpContext;
            var request = httpContext.Request;
            var response = httpContext.Response;
            var user = httpContext.User;

            if (request.IsAjaxRequest())
            {
                if (user.Identity.IsAuthenticated == false)
                {
                    response.StatusCode = (int)HttpStatusCode.Unauthorized;


                    var alert = new AlertViewModel
                    {
                        Type = AlertType.Warning,
                        Message = "Your session has expired. Please log in again."
                    };

                    List<AlertViewModel> alerts = filterContext.Controller.TempData.ContainsKey("Alerts")
                        ? (List<AlertViewModel>)filterContext.Controller.TempData["Alerts"]
                        : new List<AlertViewModel>();

                    alerts.Insert(0, alert);

                    filterContext.Controller.TempData.Add("Alerts", alerts);

                }
                else
                {
                    if (!response.IsRequestBeingRedirected)
                    {
                        response.StatusCode = (int)HttpStatusCode.Forbidden;

                    }
                }

                response.SuppressFormsAuthenticationRedirect = true;
                response.End();
            }

            base.HandleUnauthorizedRequest(filterContext);
        }
    }
}