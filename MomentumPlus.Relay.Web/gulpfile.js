var gulp = require('gulp');
var less = require('gulp-less');
var concat = require('gulp-concat');
var jshint = require('gulp-jshint');




var paths = {
    mainLess: ['./Content/styles/main.less'],
    watchLess: ['./Content/styles/**/*.less']
};

/*
 | --- DEFAULT -----------------------------------------------
 */

gulp.task('default', ['less', 'lint']);


/*
 | --- Styles -----------------------------------------------
 */


gulp.task('less', function (done)
{
    gulp.src(paths.mainLess)
    .pipe(concat('main.css'))
    .pipe(less())
    .pipe(gulp.dest('./Content/styles'))
    .on('end', done);
});


/*
 | --- JSHint ------------------------------------------
 */

gulp.task('lint', function ()
{
    return gulp.src('./Content/js/**/*.js')
      .pipe(jshint())
      .pipe(jshint.reporter('jshint-stylish'));
});


/*
 | --- WATCH ------------------------------------------
 */

gulp.task('watch', function ()
{
    gulp.watch(paths.watchLess, ['less']);
});


/*
 * --- Black-theme-dialogs ---------------------------
 */

gulp.task('black-theme', function(done) {
    gulp.src('./Content/styles/black-theme.less')
        .pipe(concat('black-theme.css'))
        .pipe(less())
        .pipe(gulp.dest('./Content/styles'))
        .on('end', done);
});

/*
 * --- White-theme-dialogs ---------------------------
 */

gulp.task('white-theme', function (done) {
    gulp.src('./Content/styles/white-theme.less')
        .pipe(concat('white-theme.css'))
        .pipe(less())
        .pipe(gulp.dest('./Content/styles'))
        .on('end', done);
});