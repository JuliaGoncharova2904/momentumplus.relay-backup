﻿using System;
using System.Linq;
using System.Reflection;
using System.Web.Configuration;
using MomentumPlus.Relay.Interfaces;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Web
{
    public static class CustomReportConfig
    {
        private static ICustomReport[] _customReports;

        static CustomReportConfig()
        {
            List<ICustomReport> customReports = new List<ICustomReport>();
            string assembliesForScan = WebConfigurationManager.AppSettings["MvcSiteMapProvider_IncludeAssembliesForScan"];

            if(assembliesForScan != null)
            {
                foreach(string assemblyName in assembliesForScan.Split(new[] { ',' }))
                {
                    try
                    {
                        Assembly assembly = Assembly.Load(new AssemblyName(assemblyName));

                        Type type = assembly.GetTypes().Where(t => t.GetInterfaces().Contains(typeof(ICustomReport))
                                                                        && t.GetConstructor(Type.EmptyTypes) != null)
                                                                        .FirstOrDefault();
                        if(type != null)
                        {
                            customReports.Add(Activator.CreateInstance(type) as ICustomReport);
                        }
                    }
                    catch (Exception) { }
                }
            }

            _customReports = customReports.ToArray();
        }

        public static ICustomReport[] Reports()
        {
            return _customReports != null ? _customReports : new ICustomReport[0];
        }

    }
}