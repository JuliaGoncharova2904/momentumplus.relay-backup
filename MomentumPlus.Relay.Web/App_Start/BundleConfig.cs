﻿using System.Web;
using System.Web.Optimization;
using BundleTransformer.Core.Bundles;
using BundleTransformer.Core.Orderers;

namespace MomentumPlus.Relay.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            RegisterLayoutStyles(bundles);
            RegisterLayoutScripts(bundles);

            RegisterPageStyles(bundles);
            RegisterPageScripts(bundles);

            RegisterSharedStyles(bundles);
            RegisterSharedScripts(bundles);

            // это обязательно, чтобы файлы подгружались в том же порядке, в котором 
            //добавляются в бандл
            var nullOrderer = new NullOrderer();
            foreach (var b in bundles)
            {
                b.Orderer = nullOrderer;
            }

            BundleTable.EnableOptimizations = true;

        }

        #region Bundles for Layouts

        /// <summary>Регистрирует бандлы для стилей шаблонов</summary>
        private static void RegisterLayoutStyles(BundleCollection bundles)
        {

            bundles.Add(new CustomStyleBundle("~/bundles/styles/layouts/not-auth-main")
            //.Include("~/Content/styles/main.less")
              .Include("~/Content/lib/webui-popover/dist/jquery.webui-popover.min.css")
            );

            bundles.Add(new CustomStyleBundle("~/bundles/styles/layouts/auth-main")
                .Include("~/Content/lib/bootstrap-select/dist/css/bootstrap-select.min.css")
                .Include("~/Content/lib/magicsuggest/magicsuggest-min.css")
                .Include("~/Content/styles/loading-animation.css")
                .Include("~/Content/lib/jasny-bootstrap/dist/css/jasny-bootstrap.min.css")
                .Include("~/Content/lib/webui-popover/dist/jquery.webui-popover.min.css")
                .Include("~/Content/lib/tooltipster/dist/css/tooltipster.bundle.min.css")
                //.Include("~/Content/styles/main.less")
                .Include("~/Content/lib/Swiper/dist/css/swiper.min.css")
                .Include("~/Content/lib/font-awesome/css/font-awesome.min.css")
            );
        }

        /// <summary>Регистрирует бандлы для скриптов шаблонов</summary>
        private static void RegisterLayoutScripts(BundleCollection bundles)
        {

            bundles.Add(new CustomScriptBundle("~/bundles/scripts/layouts/not-auth-main")
                .Include("~/Content/js/relay/sign-in/sign_in.js")
                .Include("~/Content/lib/bootstrap/dist/js/bootstrap.min.js")
            );

            bundles.Add(new CustomScriptBundle("~/bundles/scripts/layouts/auth-main")
                .Include("~/Content/lib/modernizr/modernizr.js")
                .Include("~/Content/lib/bootstrap/dist/js/bootstrap.min.js")
                .Include("~/Content/lib/magicsuggest/magicsuggest-min.js")
                .Include("~/Content/lib/bootstrap-select/dist/js/bootstrap-select.min.js")
                .Include("~/Content/lib/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js")
                .Include("~/Content/lib/jasny-bootstrap/dist/js/jasny-bootstrap.min.js")
                .Include("~/Content/lib/Swiper/dist/js/swiper.min.js")
                .Include("~/Content/lib/autosize/dist/autosize.min.js")
                .Include("~/Content/lib/purl/purl.js")
                .Include("~/Content/lib/lodash/lodash.js")
                .Include("~/Content/js/relay/dot-net-date.module.js")
                .Include("~/Content/js/relay/url-hash.module.js")
                .Include("~/Content/js/relay/url-query.module.js")
                .Include("~/Content/js/relay/header.module.js")
                .Include("~/Content/js/relay/loading-animation.module.js")
                .Include("~/Content/lib/js-cookie/src/js.cookie.js")
                .Include("~/Content/js/relay/navigation-bar.module.js")
                .Include("~/Content/js/relay/notification/notification.module.js")
                .Include("~/Content/js/relay/notification/notification-popover.module.js")
                .Include("~/Content/js/relay/notification/notification-click-handler.module.js")
            );

        }

        #endregion

        #region Bundles for Pages

        /// <summary>Регистрирует стили для конкретных страниц</summary>
        private static void RegisterPageStyles(BundleCollection bundles)
        {
            bundles.Add(new CustomStyleBundle("~/bundles/styles/sign-in")
                .Include("~/Content/styles/base/pages/sign_in.less")
            );

            bundles.Add(new CustomStyleBundle("~/bundles/styles/sign-in-welcom")
                .Include("~/Content/styles/base/pages/sign_in_welcome.less")
            );
        }

        /// <summary>Регистрирует стили для конкретных страниц</summary>
        private static void RegisterPageScripts(BundleCollection bundles)
        {
            bundles.Add(new CustomScriptBundle("~/bundles/scripts/sign-in")
                //.Include("~/Content/lib/jquery/dist/jquery.min.js")
                .Include("~/Content/js/relay/sign-in/sign_in.js")
            );

            bundles.Add(new CustomScriptBundle("~/bundles/scripts/daily-notes-panel")
                .Include("~/Content/js/relay/rotation/rotation-report-builder/daily-notes-panel.module.js")
            );

            bundles.Add(new CustomScriptBundle("~/bundles/scripts/timeline-panel")
                .Include("~/Content/js/relay/rotation/dashboard/timeline-panel.module.js")
            );

            bundles.Add(new CustomScriptBundle("~/bundles/scripts/edit-safety-message-dialog-v1")
                .Include("~/Content/js/relay/safety/v1/dialogs/major-hazard-dialog-v1.module.js")
                .Include("~/Content/js/relay/safety/v1/dialogs/safety-message-dialog-v1.module.js")
                .Include("~/Content/js/relay/safety/v1/dialogs/calendar-mini-v1.module.js")
            );

            bundles.Add(new CustomScriptBundle("~/bundles/scripts/edit-safety-message-dialog-v2")
                .Include("~/Content/js/relay/safety/v2/dialogs/safety-message-dialog-v2.module.js")
                .Include("~/Content/js/relay/safety/v2/dialogs/calendar-mini-v2.module.js")
            );

            bundles.Add(new CustomScriptBundle("~/bundles/scripts/shared/notification-page")
                .Include("~/Content/js/relay/notification/notification-click-handler.module.js")
                .Include("~/Content/js/relay/notification/notification-page.module.js")
            );
        }
        #endregion

        #region Shared Bundles

        /// <summary>Регистрирует бандлы для общих стилей</summary>
        private static void RegisterSharedStyles(BundleCollection bundles)
        {

        }

        /// <summary>Регистрирует бандлы для общих скриптов</summary>
        private static void RegisterSharedScripts(BundleCollection bundles)
        {
            bundles.Add(new CustomScriptBundle("~/bundles/scripts/shared/jquery").Include(
                "~/Content/lib/jquery/dist/jquery.min.js",
                "~/Content/lib/jquery-validation/dist/jquery.validate.min.js",
                "~/Content/lib/jquery-validation-unobtrusive/jquery.validate.unobtrusive.min.js",
                "~/Content/lib/jquery-ajax-unobtrusive/jquery.unobtrusive-ajax.min.js",
                "~/Content/lib/jquery-debounce/jquery.ba-throttle-debounce.min.js",
                "~/Content/lib/signalr/jquery.signalR.min.js",
                "~/Content/lib/jquery-clickoutside/jquery.ba-outside-events.min.js",
                "~/Content/lib/jquery.scrollTo/jquery.scrollTo.min.js",
                "~/Content/js/relay/jquery-configuration.js",
                "~/Content/lib/jQuery.dotdotdot/src/jquery.dotdotdot.min.js",
                "~/Content/js/relay/polifills.module.js",
                "~/Content/lib/moment/min/moment.min.js",
                "~/Content/lib/webui-popover/dist/jquery.webui-popover.min.js",
                "~/Content/lib/jquery-mask-plugin/dist/jquery.mask.min.js",
                "~/Content/lib/tooltipster/dist/js/tooltipster.bundle.js",
                "~/Content/lib/tooltipster-discovery/tooltipster-discovery.min.js"
                )
            );

            bundles.Add(new CustomScriptBundle("~/bundles/scripts/shared/dialogs")
                .Include("~/Content/js/relay/dialogs/validation.module.js")
                .Include("~/Content/js/relay/dialogs/dialog-loading-animation.module.js")
                .Include("~/Content/js/relay/dialogs/init-input-fields.module.js")
                .Include("~/Content/js/relay/dialogs/simple-dialogs.module.js")
                .Include("~/Content/js/relay/dialogs/form-dialog.module.js")
                .Include("~/Content/js/relay/dialogs/settings-dialog.module.js")
                .Include("~/Content/js/relay/dialogs/tasks-list-dialog.module.js")
                .Include("~/Content/js/relay/dialogs/choose-teammate-dialog.module.js")
                .Include("~/Content/js/relay/dialogs/attachments-dialog.module.js")
                .Include("~/Content/js/relay/dialogs/voice-messages-list-dialog.module.js")
                .Include("~/Content/js/relay/dialogs/edit-swing-shift-dialog.module.js")
            );

            bundles.Add(new CustomScriptBundle("~/bundles/scripts/shared/rotation-report-builder")
                //- Share report btn -
                .Include("~/Content/js/relay/reports/share-report.module.js")
                //---- Handover Filter Btn -----
                //.Include("~/Content/js/relay/rotation/rotation-report-builder/rotation-report-filter.module.js")
                //-- Report builder --
                .Include("~/Content/js/relay/rotation/rotation-report-builder/draft-subpanel.module.js")
                .Include("~/Content/js/relay/rotation/rotation-report-builder/received-subpanel.module.js")
                .Include("~/Content/js/relay/rotation/rotation-report-builder/history-subpanel.module.js")
                .Include("~/Content/js/relay/rotation/rotation-report-builder/rotation-report-builder-panel.module.js")
                //---- Inline --------
                .Include("~/Content/js/relay/rotation/sections/add-inline-section-topic.module.js")
                //----- Topic --------
                .Include("~/Content/js/relay/rotation/sections/topics/topic.control.js")
                //---- controls-------
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/attachments-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/finalise-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/location-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/manager-comments-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/more-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/notes-field.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/null-report-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/carryforward-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/pinned-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/now-task-status.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/priority-field.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/remove-topic-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/tasks-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/team-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/text-field.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/transferred-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/voice-messages-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/share-btn.control.js")
                //-----
                .Include("~/Content/js/relay/rotation/sections/section-factory.module.js")
                //----- Draft sections ---
                .Include("~/Content/js/relay/rotation/rotation-report-builder/draft/core-section.module.js")
                .Include("~/Content/js/relay/rotation/rotation-report-builder/draft/daily-notes-section.module.js")
                .Include("~/Content/js/relay/rotation/rotation-report-builder/draft/projects-section.module.js")
                .Include("~/Content/js/relay/rotation/rotation-report-builder/draft/safety-section.module.js")
                .Include("~/Content/js/relay/rotation/rotation-report-builder/draft/tasks-section.module.js")
                .Include("~/Content/js/relay/rotation/rotation-report-builder/draft/team-section.module.js")
                //----- Received sections -----
                .Include("~/Content/js/relay/rotation/rotation-report-builder/received/core-section.module.js")
                .Include("~/Content/js/relay/rotation/rotation-report-builder/received/daily-notes-section.module.js")
                .Include("~/Content/js/relay/rotation/rotation-report-builder/received/projects-section.module.js")
                .Include("~/Content/js/relay/rotation/rotation-report-builder/received/safety-section.module.js")
                .Include("~/Content/js/relay/rotation/rotation-report-builder/received/tasks-section.module.js")
                .Include("~/Content/js/relay/rotation/rotation-report-builder/received/team-section.module.js")
                //----- History section ---
                .Include("~/Content/js/relay/rotation/rotation-report-builder/history/core-section.module.js")
                .Include("~/Content/js/relay/rotation/rotation-report-builder/history/daily-notes-section.module.js")
                .Include("~/Content/js/relay/rotation/rotation-report-builder/history/projects-section.module.js")
                .Include("~/Content/js/relay/rotation/rotation-report-builder/history/safety-section.module.js")
                .Include("~/Content/js/relay/rotation/rotation-report-builder/history/tasks-section.module.js")
                .Include("~/Content/js/relay/rotation/rotation-report-builder/history/team-section.module.js")
                //-------------------------
            );

            bundles.Add(new CustomScriptBundle("~/bundles/scripts/shared/global-report-builder")
                //- Share report btn -
                .Include("~/Content/js/relay/reports/share-report.module.js")
                //---- Handover Filter Btn -----
                //.Include("~/Content/js/relay/rotation/rotation-report-builder/rotation-report-filter.module.js")
                //-- Report builder --
                .Include("~/Content/js/relay/reports/global-reports/report-builder/global-report-builder.module.js")
                //---- Inline --------
                .Include("~/Content/js/relay/rotation/sections/add-inline-section-topic.module.js")
                //----- Topic --------
                .Include("~/Content/js/relay/rotation/sections/topics/topic.control.js")
                //---- controls-------
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/attachments-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/finalise-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/location-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/manager-comments-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/more-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/notes-field.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/null-report-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/carryforward-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/pinned-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/now-task-status.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/priority-field.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/remove-topic-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/tasks-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/team-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/text-field.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/transferred-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/voice-messages-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/share-btn.control.js")
                //-----
                .Include("~/Content/js/relay/rotation/sections/section-factory.module.js")
                //----- Draft sections ---
                .Include("~/Content/js/relay/reports/global-reports/report-builder/draft/core-section.module.js")
                .Include("~/Content/js/relay/reports/global-reports/report-builder/draft/daily-notes-section.module.js")
                .Include("~/Content/js/relay/reports/global-reports/report-builder/draft/projects-section.module.js")
                .Include("~/Content/js/relay/reports/global-reports/report-builder/draft/safety-section.module.js")
                .Include("~/Content/js/relay/reports/global-reports/report-builder/draft/tasks-section.module.js")
                .Include("~/Content/js/relay/reports/global-reports/report-builder/draft/team-section.module.js")
                //----- Received sections -----
                .Include("~/Content/js/relay/reports/global-reports/report-builder/received/core-section.module.js")
                .Include("~/Content/js/relay/reports/global-reports/report-builder/received/daily-notes-section.module.js")
                .Include("~/Content/js/relay/reports/global-reports/report-builder/received/projects-section.module.js")
                .Include("~/Content/js/relay/reports/global-reports/report-builder/received/safety-section.module.js")
                .Include("~/Content/js/relay/reports/global-reports/report-builder/received/tasks-section.module.js")
                .Include("~/Content/js/relay/reports/global-reports/report-builder/received/team-section.module.js")
            //-------------------------
            );

            bundles.Add(new CustomScriptBundle("~/bundles/scripts/shared/shift-report-builder")
                //- Share report btn -
                .Include("~/Content/js/relay/reports/share-report.module.js")
                //---- Handover Filter Btn -----
                //.Include("~/Content/js/relay/rotation/shift-report-builder/shift-report-filter.module.js")
                //-- Report builder --
                .Include("~/Content/js/relay/rotation/shift-report-builder/draft-subpanel.module.js")
                .Include("~/Content/js/relay/rotation/shift-report-builder/received-subpanel.module.js")
                .Include("~/Content/js/relay/rotation/shift-report-builder/shift-report-builder-panel.module.js")
                //---- Inline --------
                .Include("~/Content/js/relay/rotation/sections/add-inline-section-topic.module.js")
                //----- Topic --------
                .Include("~/Content/js/relay/rotation/sections/topics/topic.control.js")
                //---- controls-------
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/attachments-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/finalise-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/location-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/manager-comments-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/more-btn.control.js")
                //.Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/notes-field.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/null-report-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/carryforward-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/pinned-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/now-task-status.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/priority-field.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/remove-topic-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/tasks-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/team-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/text-field.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/transferred-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/voice-messages-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/share-btn.control.js")
                //-----
                .Include("~/Content/js/relay/rotation/sections/section-factory.module.js")
                //----- Draft sections ---
                .Include("~/Content/js/relay/rotation/shift-report-builder/draft/core-section.module.js")
                .Include("~/Content/js/relay/rotation/shift-report-builder/draft/daily-notes-section.module.js")
                .Include("~/Content/js/relay/rotation/shift-report-builder/draft/projects-section.module.js")
                .Include("~/Content/js/relay/rotation/shift-report-builder/draft/safety-section.module.js")
                .Include("~/Content/js/relay/rotation/shift-report-builder/draft/tasks-section.module.js")
                .Include("~/Content/js/relay/rotation/shift-report-builder/draft/team-section.module.js")
                //----- Received sections -----
                .Include("~/Content/js/relay/rotation/shift-report-builder/received/core-section.module.js")
                .Include("~/Content/js/relay/rotation/shift-report-builder/received/daily-notes-section.module.js")
                .Include("~/Content/js/relay/rotation/shift-report-builder/received/projects-section.module.js")
                .Include("~/Content/js/relay/rotation/shift-report-builder/received/safety-section.module.js")
                .Include("~/Content/js/relay/rotation/shift-report-builder/received/tasks-section.module.js")
                .Include("~/Content/js/relay/rotation/shift-report-builder/received/team-section.module.js")
            //-------------------------
            );

            bundles.Add(new CustomScriptBundle("~/bundles/scripts/shared/checklist-tasks-panel")
                //----- Topic --------
                .Include("~/Content/js/relay/rotation/sections/topics/topic.control.js")
                //---- controls-------
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/attachments-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/finalise-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/more-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/notes-field.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/priority-field.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/team-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/text-field.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/transferred-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/voice-messages-btn.control.js")
                //-----
                .Include("~/Content/js/relay/rotation/checklist/received-tasks-section.module.js")
            );


            bundles.Add(new CustomScriptBundle("~/bundles/scripts/shared/project-admin")
                //----- Topic --------
                .Include("~/Content/js/relay/rotation/sections/topics/topic.control.js")
                //---- controls-------
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/attachments-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/more-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/priority-field.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/team-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/text-field.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/remove-topic-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/transferred-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/archived-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/voice-messages-btn.control.js")
                //-----
                .Include("~/Content/js/relay/project-admin/sort-tasks.module.js")
                .Include("~/Content/js/relay/project-admin/planned-tasks.module.js")
                .Include("~/Content/js/relay/project-admin/adhoc-tasks.module.js")
                .Include("~/Content/js/relay/project-admin/tasks-panel.module.js")
                //--------------------
                .Include("~/Content/js/relay/project-admin/pagination-panel.module.js")
                .Include("~/Content/js/relay/project-admin/projects-panel.module.js")
                .Include("~/Content/js/relay/project-admin/team-panel.module.js")
                .Include("~/Content/js/relay/project-admin/project-admin.module.js")
                //--------------------
                .Include("~/Content/js/relay/project-admin/dialogs/followers-dialog.module.js")
                .Include("~/Content/js/relay/project-admin/dialogs/project-choose-teammate-dialog.module.js")
            //--------------------
            );

            bundles.Add(new CustomScriptBundle("~/bundles/scripts/shared/task-board-received-section")
                //----- Topic --------
                .Include("~/Content/js/relay/rotation/sections/topics/topic.control.js")
                //---- controls-------
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/archived-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/attachments-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/more-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/priority-field.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/team-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/text-field.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/transferred-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/voice-messages-btn.control.js")
                //-----
                .Include("~/Content/js/relay/rotation/task-board/received-section.module.js")
            );

            bundles.Add(new CustomScriptBundle("~/bundles/scripts/shared/task-board-my-task-section")
                //----- Topic --------
                .Include("~/Content/js/relay/rotation/sections/topics/topic.control.js")
                //---- controls-------
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/archived-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/remove-topic-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/attachments-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/more-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/priority-field.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/text-field.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/transferred-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/voice-messages-btn.control.js")
                //-----
                .Include("~/Content/js/relay/rotation/task-board/my-tasks-section.module.js")
            );

            bundles.Add(new CustomScriptBundle("~/bundles/scripts/shared/task-board-assigned-section")
                //----- Topic --------
                .Include("~/Content/js/relay/rotation/sections/topics/topic.control.js")
                //---- controls-------
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/archived-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/remove-topic-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/carryforward-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/attachments-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/more-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/priority-field.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/team-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/text-field.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/transferred-btn.control.js")
                .Include("~/Content/js/relay/rotation/sections/topics/topic-controls-factory/voice-messages-btn.control.js")
                //-----
                .Include("~/Content/js/relay/rotation/task-board/assigned-section.module.js")
            );

            bundles.Add(new CustomScriptBundle("~/bundles/scripts/shared/indordrill-list")
                .Include("~/Content/js/relay/indodrill-report/indodrill-reports.module.js")
            );
        }

        #endregion
    }
}
