﻿using Microsoft.Owin;
using MomentumPlus.Relay.Api;
using MomentumPlus.Relay.CompositionRoot.HangFire;
using Owin;

[assembly: OwinStartup(typeof(MomentumPlus.Relay.Web.Startup))]
namespace MomentumPlus.Relay.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            JobsConfiguration.Execute(app);

            AuthorizationConfiguration.Configure(app);

            HangFireConfiguration.Configure(app);

            SignalRConfiguration.Configure(app);

            ApiConfiguration.Configure(app);
        }
    }
}