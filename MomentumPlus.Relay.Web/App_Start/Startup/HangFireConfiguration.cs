﻿using Hangfire;
using MomentumPlus.Relay.CompositionRoot.HangFire;
using Owin;

namespace MomentumPlus.Relay.Web
{
    public static class HangFireConfiguration
    {
        public static void Configure(IAppBuilder app)
        {
            app.UseHangfireDashboard("/jobs", new DashboardOptions()
            {
                Authorization = new[] { new HangFireAuthorizationFilter() }
            });

        }
    }
}