﻿using Microsoft.AspNet.SignalR;
using MomentumPlus.Relay.Web.SignalRHubs.Configuration;
using Owin;

namespace MomentumPlus.Relay.Web
{
    public static class SignalRConfiguration
    {
        public static void Configure(IAppBuilder app)
        {
            SignalRUserIdProvider idProvider = new SignalRUserIdProvider();
            GlobalHost.DependencyResolver.Register(typeof(IUserIdProvider), () => idProvider);

            app.MapSignalR();

            GlobalHost.HubPipeline.RequireAuthentication();
        }
    }
}