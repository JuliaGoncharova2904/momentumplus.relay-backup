//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Relay.Migrator.NewDB
{
    using System;
    using System.Collections.Generic;
    
    public partial class SafetyV2Messages
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SafetyV2Messages()
        {
            this.SafetyV2Stats = new HashSet<SafetyV2Stats>();
        }
    
        public System.Guid Id { get; set; }
        public System.DateTime Date { get; set; }
        public string Message { get; set; }
        public System.Guid CriticalControlId { get; set; }
        public Nullable<System.DateTime> CreatedUtc { get; set; }
        public Nullable<System.DateTime> ModifiedUtc { get; set; }
        public Nullable<System.DateTime> DeletedUtc { get; set; }
    
        public virtual SafetyV2CriticalControls SafetyV2CriticalControls { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SafetyV2Stats> SafetyV2Stats { get; set; }
    }
}
