//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Relay.Migrator.NewDB
{
    using System;
    using System.Collections.Generic;
    
    public partial class Shifts
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Shifts()
        {
            this.RotationModules = new HashSet<RotationModules>();
            this.Shifts1 = new HashSet<Shifts>();
            this.Shifts2 = new HashSet<Shifts>();
        }
    
        public System.Guid Id { get; set; }
        public Nullable<System.DateTime> StartDateTime { get; set; }
        public int WorkMinutes { get; set; }
        public int BreakMinutes { get; set; }
        public int RepeatTimes { get; set; }
        public Nullable<System.Guid> ShiftRecipientId { get; set; }
        public System.Guid RotationId { get; set; }
        public int State { get; set; }
        public Nullable<System.DateTime> CreatedUtc { get; set; }
        public Nullable<System.DateTime> ModifiedUtc { get; set; }
        public Nullable<System.DateTime> DeletedUtc { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RotationModules> RotationModules { get; set; }
        public virtual Rotations Rotations { get; set; }
        public virtual UserProfiles UserProfiles { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Shifts> Shifts1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Shifts> Shifts2 { get; set; }
    }
}
