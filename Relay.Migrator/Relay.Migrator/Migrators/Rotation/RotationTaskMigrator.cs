﻿using System;
using System.Data.Entity.Migrations;
using System.Linq;
using NLog;
using Relay.Migrator.NewDB;

namespace Relay.Migrator.Migrators
{
    public static class RotationTaskMigrator
    {
        private static readonly Logger Logger = LogManager.GetLogger("RotationTaskMigrator");

        public static void Migrate(NewContextEntities newContext)
        {
            try
            {
                foreach (RotationTasks task in newContext.RotationTasks.Where(t => !t.ParentTaskId.HasValue))
                {
                    var childTasks = task.RotationTasks12;

                    if (task.TaskBoardTaskType != 0 && task.TaskBoardId.HasValue)
                    {
                        // Migrate Received Task Board
                        var receivedTask = childTasks?.FirstOrDefault(t => t.TaskBoardTaskType == 2);

                        if (receivedTask != null)
                        {
                            receivedTask.AncestorTaskId = task.Id;

                            receivedTask.ParentTaskId = task.Id;


                            task.SuccessorTaskId = receivedTask.Id;

                            newContext.RotationTasks.AddOrUpdate(c => c.Id, receivedTask);
                            newContext.RotationTasks.AddOrUpdate(c => c.Id, task);
                        }
                    }
                    else
                    {
                        // Migrate Received Task
                        var receivedTask = childTasks?.FirstOrDefault(t => t.RotationTopics.RotationTopicGroups.RotationModules.SourceType == 0);

                        if (receivedTask != null)
                        {
                            receivedTask.AncestorTaskId = task.Id;

                            receivedTask.ParentTaskId = task.Id;

                            task.SuccessorTaskId = receivedTask.Id;

                            newContext.RotationTasks.AddOrUpdate(c => c.Id, receivedTask);
                            newContext.RotationTasks.AddOrUpdate(c => c.Id, task);

                        }
                    }

                    // Migrate Carry Forward

                    var draftsTasks = childTasks?.Where(t => t?.RotationTopics?.RotationTopicGroups?.RotationModules?.SourceType == 1);

                    if (draftsTasks != null)
                        foreach (var draftsTask in draftsTasks.OrderBy(t => t.CreatedUtc))
                        {
                            draftsTask.ForkParentTaskId = task.Id;

                            task.ForkCounter = task.ForkCounter + 1;

                            draftsTask.ParentTaskId = task.Id;

                            newContext.RotationTasks.AddOrUpdate(c => c.Id, task);

                            newContext.RotationTasks.AddOrUpdate(c => c.Id, draftsTask);
                        }

                    Logger.Info("Migrate Rotation Task {0}, Id - {1}.", task.Name, task.Id);

                }

                newContext.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "RotationTaskMigrator Error.");
                throw;
            }
        }
    }
}
