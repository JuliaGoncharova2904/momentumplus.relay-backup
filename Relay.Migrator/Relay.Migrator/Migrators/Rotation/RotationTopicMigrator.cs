﻿using System;
using System.Data.Entity.Migrations;
using System.Linq;
using NLog;
using Relay.Migrator.NewDB;

namespace Relay.Migrator.Migrators
{
    public static class RotationTopicMigrator
    {
        private static readonly Logger Logger = LogManager.GetLogger("RotationTopicMigrator");

        public static void Migrate(NewContextEntities newContext)
        {
            try
            {
                foreach (RotationTopics topic in newContext.RotationTopics.Where(t => !t.ParentRotationTopicId.HasValue))
                {
                    var childTopics = topic.RotationTopics12;

                    // Migrate Received Topics
                    var receivedTopic = childTopics?.FirstOrDefault(t => t.RotationTopicGroups.RotationModules.SourceType == 0);

                    if (receivedTopic != null)
                    {
                        receivedTopic.AncestorTopicId = topic.Id;

                        topic.SuccessorTopicId = receivedTopic.Id;

                        receivedTopic.ParentTopicId = topic.Id;

                        newContext.RotationTopics.AddOrUpdate(c => c.Id, receivedTopic);
                        newContext.RotationTopics.AddOrUpdate(c => c.Id, topic);

                    }


                    // Migrate Carry Forward

                    var draftsTopics = childTopics?.Where(t => t.RotationTopicGroups.RotationModules.SourceType == 1);

                    foreach (var draftsTopic in draftsTopics.OrderBy(t => t.CreatedUtc))
                    {
                        draftsTopic.ForkParentTopicId = topic.Id;

                        draftsTopic.ParentTopicId = topic.Id;

                        topic.ForkCounter = topic.ForkCounter + 1;

                        newContext.RotationTopics.AddOrUpdate(c => c.Id, topic);

                        newContext.RotationTopics.AddOrUpdate(c => c.Id, draftsTopic);
                    }

                    Logger.Info("Migrate Rotation Topic {0}, Id - {1}.", topic.Name, topic.Id);
                }

                newContext.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "RotationTopicMigrator Error.");
                throw;
            }
        }
    }
}
