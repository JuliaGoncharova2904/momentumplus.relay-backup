﻿namespace Relay.Migrator
{
    partial class MigratorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MigratorForm));
            this.btn_StartMigrate = new System.Windows.Forms.Button();
            this.migratorWorker = new System.ComponentModel.BackgroundWorker();
            this.MigrationProgressBar = new System.Windows.Forms.ProgressBar();
            this.lblOperation = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_StartMigrate
            // 
            this.btn_StartMigrate.Location = new System.Drawing.Point(132, 239);
            this.btn_StartMigrate.Name = "btn_StartMigrate";
            this.btn_StartMigrate.Size = new System.Drawing.Size(177, 41);
            this.btn_StartMigrate.TabIndex = 0;
            this.btn_StartMigrate.Text = "Start Migration";
            this.btn_StartMigrate.UseVisualStyleBackColor = true;
            this.btn_StartMigrate.Click += new System.EventHandler(this.btn_StartMigrate_Click);
            // 
            // migratorWorker
            // 
            this.migratorWorker.WorkerReportsProgress = true;
            this.migratorWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.migratorWorker_DoWork);
            this.migratorWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.migratorWorker_ProgressChanged);
            this.migratorWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.migratorWorker_RunWorkerCompleted);
            // 
            // MigrationProgressBar
            // 
            this.MigrationProgressBar.Location = new System.Drawing.Point(12, 93);
            this.MigrationProgressBar.Maximum = 11;
            this.MigrationProgressBar.Name = "MigrationProgressBar";
            this.MigrationProgressBar.Size = new System.Drawing.Size(418, 29);
            this.MigrationProgressBar.TabIndex = 1;
            // 
            // lblOperation
            // 
            this.lblOperation.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblOperation.Location = new System.Drawing.Point(12, 154);
            this.lblOperation.Name = "lblOperation";
            this.lblOperation.Size = new System.Drawing.Size(418, 50);
            this.lblOperation.TabIndex = 2;
            this.lblOperation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MigratorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 315);
            this.Controls.Add(this.lblOperation);
            this.Controls.Add(this.MigrationProgressBar);
            this.Controls.Add(this.btn_StartMigrate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MigratorForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Relay Migrator";
            this.Load += new System.EventHandler(this.MigratorForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_StartMigrate;
        private System.ComponentModel.BackgroundWorker migratorWorker;
        private System.Windows.Forms.ProgressBar MigrationProgressBar;
        private System.Windows.Forms.Label lblOperation;
    }
}

