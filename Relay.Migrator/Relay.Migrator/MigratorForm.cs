﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using Relay.Migrator.Migrators;
using Relay.Migrator.NewDB;

namespace Relay.Migrator
{
    public partial class MigratorForm : Form
    { 

        private NewContextEntities _newContext;

        public MigratorForm()
        {
            InitializeComponent();
        }

        private void MigratorForm_Load(object sender, EventArgs e)
        {
            _newContext = new NewContextEntities();

            MigrationProgressBar.Maximum = 1;

        }

        private void migratorWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {

                var progress = 0;

                ////Migrate Topic
                //ChangeOperationLabel(@"Migrate Topic");
                //RotationTopicMigrator.Migrate(_newContext);
                //progress = progress + 1;
                //migratorWorker.ReportProgress(progress);

                //Migrate Task
                ChangeOperationLabel(@"Migrate Task");
                RotationTaskMigrator.Migrate(_newContext);
                progress = progress + 1;
                migratorWorker.ReportProgress(progress);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"Something Wrong", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ChangeOperationLabel(string text)
        {
            lblOperation.Invoke((MethodInvoker)delegate { lblOperation.Text = text; });
        }

        private void migratorWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            MigrationProgressBar.Value = e.ProgressPercentage;
        }

        private void btn_StartMigrate_Click(object sender, EventArgs e)
        {
            btn_StartMigrate.Visible = false;
            migratorWorker.RunWorkerAsync();
        }

        private void migratorWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btn_StartMigrate.Visible = true;
            MigrationProgressBar.Maximum = 41;
            MigrationProgressBar.Value = 0;
            lblOperation.Text = null;
        }
    }
}
