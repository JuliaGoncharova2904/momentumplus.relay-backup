﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Hosting;
using System.Web.Mvc;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.Mailer.Models;
using NLog;
using Postal;
using Attachment = System.Net.Mail.Attachment;

namespace MomentumPlus.Relay.Mailer
{
    public class MailerCore
    {
        private readonly EmailService _emailService;

        private readonly IRepositoriesUnitOfWork _repositoriesUnitOfWork;

        private static readonly Logger Logger = LogManager.GetLogger("mail-logger");

        public MailerCore()
        {
            var viewsPath = Path.GetFullPath(HostingEnvironment.MapPath(@"~/bin/MailTemplates/"));
            var engines = new ViewEngineCollection { new FileSystemRazorViewEngine(viewsPath) };
            var emailService = new EmailService(engines);
            _emailService = emailService;
            _repositoriesUnitOfWork = new RepositoriesUnitOfWork();
        }

        [DisplayName(@"Send Forgot Password Email to {0}")]
        public void ForgotPasswordEmail(string to, string toName, string resetUrl)
        {
            var email = new ForgotPasswordEmail
            {
                To = to,
                ToName = toName,
                ResetUrl = resetUrl
            };

            Logger.Info("System send Forgot Password Email to {0}, email: {1} . Reset Url - {2} ", email.ToName, email.To, email.ResetUrl);

            _emailService.Send(email);
        }

        [DisplayName(@"Send Handover Shift Preview Email to Shift Recipient. ShiftId:{0}")]
        public void HandoverShiftPreviewEmail(Guid shiftId)
        {
            Shift shift = _repositoriesUnitOfWork.ShiftRepository.Get(shiftId);

            var siteSettings = _repositoriesUnitOfWork.AdminSettingsRepository.GetAll().FirstOrDefault();

            var shiftRecipient = shift.ShiftRecipient;

            if (shiftRecipient != null && shiftRecipient.CurrentRotation != null)
            {
                var shiftRecipientPreviousShift = shiftRecipient.CurrentRotation.RotationShifts.OrderBy(s => s.CreatedUtc.Value).LastOrDefault(s => s.StartDateTime.HasValue
                                                                                    && s.State == ShiftState.Finished
                                                                                    && s.CreatedUtc.Value < DateTime.Now);
                var email = new ShiftPreviewEmail
                {
                    To = shiftRecipient.Email,
                    ShiftOwnerName = shift.Rotation.RotationOwner.FirstName,
                    ShiftRecipientName = shiftRecipient.FirstName,
                    ShiftId = shift.Id,
                    SiteUrl = siteSettings != null ? siteSettings.DomainUrl : null,
                    ShiftEndTime = shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes).ToString("HH:mm"),
                    IsRecipientHasPreviousShift = shiftRecipientPreviousShift != null
                };

                var reportUrl = $"{email.SiteUrl}/Reports/HandoverReport?sourceId={email.ShiftId}&isShift=true";

                if (shiftRecipientPreviousShift != null)
                {
                    var shiftRecipientPreviousShiftUrl = string.Format("{0}/Summary#?DailyReportShiftId={1}", email.SiteUrl, shiftRecipientPreviousShift.Id);

                    email.RecipientPreviousShiftUrl = shiftRecipientPreviousShiftUrl;
                }

                byte[] pdfBytes;
                using (var webClient = new WebClient())
                {
                    pdfBytes = webClient.DownloadData(reportUrl);
                }

                Logger.Info("System send Handover Shift Preview Email to {0}, email: {1} . Creator is {2}, shiftId: {3} . Url for report - {4}",
                    email.ShiftRecipientName,
                    email.To,
                    email.ShiftOwnerName,
                    email.ShiftId,
                    reportUrl);


                email.Attach(new Attachment(new MemoryStream(pdfBytes), shift.Rotation.RotationOwner.FirstName + " Shift Report.pdf"));

                _emailService.Send(email);
            }
        }

        [DisplayName(@"Send Handover Preview Email to Back To Back. RotationId:{0}")]
        public void HandoverPreviewEmail(Guid rotationId)
        {
            Rotation rotation = _repositoriesUnitOfWork.RotationRepository.Get(rotationId);

            Rotation backRotation = rotation.DefaultBackToBack.CurrentRotation;

            var siteSettings = _repositoriesUnitOfWork.AdminSettingsRepository.GetAll().FirstOrDefault();


            string backToBackReturnDate = null;

            if (backRotation != null)
            {
                backToBackReturnDate = RotationExpiredDate(backRotation).FormatWithDayAndMonth();
            }

            var email = new HandoverPreviewEmail
            {
                To = rotation.DefaultBackToBack.Email,
                RotationOwnerName = rotation.RotationOwner.FirstName,
                RotationBackToBackName = rotation.DefaultBackToBack.FirstName,
                ReturnDate = backToBackReturnDate,
                SiteUrl = siteSettings != null ? siteSettings.DomainUrl : null,
                RotationId = rotationId,
               // HistorySectionUrl = siteSettings != null ? siteSettings.DomainUrl + "/Summary#?ReportsType=HistoryPanel" : null,
                HistorySectionUrl = siteSettings != null ? siteSettings.DomainUrl + $"/Reports/GlobalReports/Report/{rotationId}/Draft?reportType=Swing" : null,
                BackHaveRotation = backRotation != null
            };

            var reportUrl = $"{email.SiteUrl}/Reports/HandoverReport?sourceId={email.RotationId}";

            byte[] pdfBytes;
            using (var webClient = new WebClient())
            {
                pdfBytes = webClient.DownloadData(reportUrl);
            }

            Logger.Info("System send Handover Preview Email to {0}, email: {1} . Creator is {2}, rotationId: {3} . Url for report - {4}",
                email.RotationBackToBackName,
                email.To,
                email.RotationOwnerName,
                email.RotationId,
                reportUrl);


            email.Attach(new Attachment(new MemoryStream(pdfBytes), rotation.RotationOwner.FirstName + " Draft Handover Report.pdf"));

            _emailService.Send(email);
        }

        [DisplayName(@"Send Rotation Share Report Email to Back To Back. RotationId:{0}")]
        public void RotationShareReportEmail(Guid rotationId)
        {
            Rotation rotation = _repositoriesUnitOfWork.RotationRepository.Get(rotationId);

            UserProfile rotationOwner = rotation.RotationOwner;

            UserProfile rotationOwnerLineManager = rotation.LineManager;

            var reportSharingRecipientsIds = rotationOwner.SendedReports.Where(report => report.ReportId == rotationId 
                                             && report.RecipientId != rotationOwnerLineManager.Id
                                             && report.RecipientId != rotation.DefaultBackToBackId).Select(user => user.RecipientId);

            var reportCCs = _repositoriesUnitOfWork.UserProfileRepository.Find(u => reportSharingRecipientsIds.Contains(u.Id)).ToList();

            reportCCs.Add(rotationOwnerLineManager);

            reportCCs.Add(rotationOwner);

            string reportCCsEmails = string.Join(",", reportCCs.Select(x => x.Email));

            var siteSettings = _repositoriesUnitOfWork.AdminSettingsRepository.GetAll().FirstOrDefault();

            var email = new RotationShareReport
            {
                To = rotation.DefaultBackToBack.Email,
                Subject = $"Finalised Relay Report - {DateTime.UtcNow.FormatWithDayAndMonth()} - {rotation.RotationOwner.FirstName}",
                NameOfDrafter = rotation.RotationOwner.FirstName,
                ReportRecipientName = rotation.DefaultBackToBack.FirstName,
                HandoverStartDate = rotation.StartDate.FormatWithDayAndMonth(),
                HandoverEndDate = RotationSwingEndDate(rotation).FormatWithDayAndMonth(),
                DateOfHandover = DateTime.UtcNow.FormatWithDayAndMonth(),
                CC = reportCCsEmails,
                ReportUrl = siteSettings != null ? siteSettings.DomainUrl + $"/Reports/GlobalReports/Report/{rotation.Id}/Draft?reportType=Swing" : null

            };

            Logger.Info("System send Rotation Share Report Email to {0}, email: {1} . Creator is {2}, rotationId: {3} . Url for share report - {4} . CC`s - {5}",
              email.ReportRecipientName,
              email.To,
              email.NameOfDrafter,
              rotationId,
              email.ReportUrl,
              email.CC);

            var reportPdfUrl = $"{siteSettings.DomainUrl}/Reports/HandoverReport?sourceId={rotation.Id}";

            byte[] pdfBytes;
            using (var webClient = new WebClient())
            {
                pdfBytes = webClient.DownloadData(reportPdfUrl);
            }

            email.Attach(new Attachment(new MemoryStream(pdfBytes), rotation.RotationOwner.FirstName + " Draft Handover Report.pdf"));

            _emailService.Send(email);
        }

        public DateTime RotationExpiredDate(Rotation rotation)
        {
            if (rotation.StartDate.HasValue)
            {
                DateTime expireDate = rotation.StartDate.Value.AddDays(rotation.DayOn + rotation.DayOff);

                return expireDate;
            }

            return rotation.CreatedUtc.Value.AddDays(rotation.DayOn + rotation.DayOff);
        }

        public DateTime RotationSwingEndDate(Rotation rotation)
        {
            if (rotation.StartDate.HasValue)
            {
                DateTime expireDate = rotation.StartDate.Value.AddDays(rotation.DayOn - 1);

                return expireDate;
            }

            return rotation.CreatedUtc.Value.AddDays(rotation.DayOn - 1);
        }
    }
}
