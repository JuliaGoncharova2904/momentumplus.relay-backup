﻿using Postal;

namespace MomentumPlus.Relay.Mailer.Models
{
    public class ForgotPasswordEmail : Email
    {
        public string To { get; set; }

        public string ToName { get; set; }

        public string ResetUrl { get; set; }

  
    }
}
