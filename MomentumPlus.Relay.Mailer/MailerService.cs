﻿using System;
using Hangfire;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Mailer
{
    public class MailerService : IMailerService
    {
        public void SendHandoverPreviewEmail(Guid rotationId)
        {
            BackgroundJob.Enqueue<MailerCore>(x => x.HandoverPreviewEmail(rotationId));
        }

        public void SendShiftHandoverPreviewEmail(Guid shiftId)
        {
            BackgroundJob.Enqueue<MailerCore>(x => x.HandoverShiftPreviewEmail(shiftId));
        }

        public void SendForgotPasswordEmail(string to, string toName, string resetUrl)
        {
            BackgroundJob.Enqueue<MailerCore>(x => x.ForgotPasswordEmail(to, toName, resetUrl));
        }

        public void SendRotationShareReportEmail(Guid rotationId)
        {
            BackgroundJob.Enqueue<MailerCore>(x => x.RotationShareReportEmail(rotationId));
        }

    }
}