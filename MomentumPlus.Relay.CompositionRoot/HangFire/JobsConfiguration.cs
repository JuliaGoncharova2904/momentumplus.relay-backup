﻿using Hangfire;
using Hangfire.SqlServer;
using Owin;
using System;
using Hangfire.Logging;
using Hangfire.Logging.LogProviders;

namespace MomentumPlus.Relay.CompositionRoot.HangFire
{
    public static class JobsConfiguration
    {
        public static void Execute(IAppBuilder app)
        {
            var options = new SqlServerStorageOptions
            {
                QueuePollInterval = TimeSpan.FromSeconds(15),
                JobExpirationCheckInterval = TimeSpan.FromDays(5),
                DashboardJobListLimit = 50000,
                TransactionTimeout = TimeSpan.FromMinutes(1)
            };

            GlobalConfiguration.Configuration.UseSqlServerStorage("MomentumContext", options);

            LogProvider.SetCurrentLogProvider(new NLogLogProvider());

            app.UseHangfireServer();

            JobsRunner.InitializeJobs();
        }
    }
}
