﻿using Hangfire;
using Hangfire.Storage;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.CompositionRoot.IOC;
using MomentumPlus.Relay.Configuration;
using MomentumPlus.Relay.Configuration.Configurations.Jobs;
using MomentumPlus.Relay.Interfaces.Logic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using NLog;

namespace MomentumPlus.Relay.CompositionRoot.HangFire
{
    /// <summary>
    /// Jobs Manager
    /// </summary>
    public static class JobsRunner
    {

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initialize all jobs in site.
        /// </summary>
        public static void InitializeJobs()
        {
            JobsConfig jobsConfig = RelayConfig.GetConfig().JobsConfig;

            if (jobsConfig != null)
            {
                List<RecurringJobDto> recurringJobs = JobStorage.Current.GetConnection().GetRecurringJobs();

                IEnumerable<string> removedJobsIs = recurringJobs.Where(rj => !jobsConfig.Jobs.Select(j => j.Id).Contains(rj.Id)).Select(rj => rj.Id);

                foreach (string jobId in removedJobsIs)
                {
                    RecurringJob.RemoveIfExists(jobId);
                }

                foreach (Job job in jobsConfig.Jobs)
                {
                    CreateJob(job);
                }
            }

            jobsConfig.ReloadHandler += InitializeJobs;
        }

        /// <summary>
        /// Reload jobs that is depend on specific Cron Provider.
        /// </summary>
        /// <param name="providerType">Cron Provider Type</param>
        public static void ReloadJobsWithProvider(string providerType)
        {
            JobsConfig jobsConfig = RelayConfig.GetConfig().JobsConfig;

            if (jobsConfig != null)
            {
                IEnumerable<Job> jobs = jobsConfig.Jobs.Where(j => j.CronProvider != null && j.CronProvider.Type == providerType);

                foreach (Job job in jobs)
                {
                    CreateJob(job);
                }
            }
        }

        /// <summary>
        /// Run job immediately
        /// </summary>
        /// <param name="id">Job Id</param>
        public static void RunJobById(string id)
        {
            JobsConfig jobsConfig = RelayConfig.GetConfig().JobsConfig;

            if (jobsConfig != null)
            {
                if(jobsConfig.Jobs.Select(j => j.Id).Contains(id))
                {
                    RecurringJob.Trigger(id);
                }
                else
                {
                    Logger.Error("Jobs Config is null!");
                }
            }
        }

        /// <summary>
        /// Create job method.
        /// </summary>
        /// <param name="job">Configuration object</param>
        private static void CreateJob(Job job)
        {
            try
            {
                string cron = job.Cron;

                if (string.IsNullOrEmpty(cron) && job.CronProvider != null)
                {
                    Type providerType = Assembly.Load(job.CronProvider.Assembly).GetType(job.CronProvider.Class);

                    ConstructorInfo providerConstructor = providerType.GetConstructor(new[] { typeof(LogicCoreUnitOfWork) });
                    object providerObject = providerConstructor.Invoke(new object[] { GetLogicCore() });

                    MethodInfo providerMethod = providerType.GetMethod(job.CronProvider.Method);
                    cron = (string)providerMethod.Invoke(providerObject, new object[] { });
                }

                RecurringJob.RemoveIfExists(job.Id);
                if (!string.IsNullOrEmpty(cron))
                {
                    RecurringJob.AddOrUpdate(job.Id, () => Job(job.Assembly, job.Class, job.Method), cron, TimeZoneInfo.Local);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Job Exception.");
            }
        }

        /// <summary>
        /// Job executer.
        /// </summary>
        /// <param name="assembly">Assembly name</param>
        /// <param name="className">Class name</param>
        /// <param name="method">Method name (Job method)</param>
        [DisplayName("{2}")]
        public static void Job(string assembly, string className, string method)
        {
            try
            {
                Type jobType = Assembly.Load(assembly).GetType(className);

                ConstructorInfo jobConstructor = jobType.GetConstructor(new[] { typeof(LogicCoreUnitOfWork) });
                object jobObject = jobConstructor.Invoke(new object[] { GetLogicCore() });

                MethodInfo jobMethod = jobType.GetMethod(method);
                jobMethod.Invoke(jobObject, new object[] { });
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Job Exception.");
            }
        }

        /// <summary>
        /// Create LogicCoreUnitOfWork object
        /// </summary>
        /// <returns>LogicCoreUnitOfWork object</returns>
        private static LogicCoreUnitOfWork GetLogicCore()
        {
            LogicCoreUnitOfWork instance = null;

            using (RelayLightInject.JobContainer.BeginScope())
            {
                instance = (LogicCoreUnitOfWork)RelayLightInject.JobContainer.GetInstance(typeof(ILogicCoreUnitOfWork));
            }

            return instance;
        }

    }
}
