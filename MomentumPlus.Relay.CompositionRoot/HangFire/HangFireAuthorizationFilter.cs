﻿using System.Web;
using Hangfire.Annotations;
using Hangfire.Dashboard;
using MomentumPlus.Core.Authorization;

namespace MomentumPlus.Relay.CompositionRoot.HangFire
{
    public class HangFireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize([NotNull] DashboardContext context)
        {
            if (HttpContext.Current.GetOwinContext().Authentication.User != null
                && HttpContext.Current.GetOwinContext().Authentication.User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin))
            {
                return true;
            }

            return false;
        }

    }
}
