﻿using System;
using LightInject;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Relay.Interfaces;
using System.Reflection;
using MomentumPlus.Relay.Mailer;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.Services;
using MomentumPlus.Relay.Interfaces.Logic;

namespace MomentumPlus.Relay.CompositionRoot.IOC
{
    public static class RelayLightInject
    {
        private static IServiceContainer _jobContainer;
        private const string WebAssemblyName = "MomentumPlus.Relay.Web";

        public static IServiceContainer JobContainer
        {
            get
            {
                return _jobContainer;
            }
        }

        public static void RegisterContainer()
        {
            //-----------------------------------------------------------------------------
            IServiceContainer webContainer = CreateContainer();

            webContainer.RegisterControllers(Assembly.Load(WebAssemblyName));
            webContainer.EnableMvc();

            //-----------------------------------------------------------------------------
            IServiceContainer jobsContainer = CreateContainer();

            jobsContainer.ScopeManagerProvider = new PerThreadScopeManagerProvider();

            _jobContainer = jobsContainer;
            //-----------------------------------------------------------------------------
        }

        public static IServiceContainer CreateContainer()
        {
            IServiceContainer container = new ServiceContainer();

            container.Register(typeof(ServicesUnitOfWork), new PerRequestLifeTime());
            container.Register(typeof(LogicCoreUnitOfWork), new PerRequestLifeTime());

            container.Register<IServicesUnitOfWork>(factory => factory.GetInstance<ServicesUnitOfWork>(), new PerScopeLifetime());
            container.Register<ILogicCoreUnitOfWork>(factory => factory.GetInstance<LogicCoreUnitOfWork>(), new PerScopeLifetime());

            container.Register<IRepositoriesUnitOfWork>(factory => new RepositoriesUnitOfWork());
            container.Register<IMailerService>(factory => new MailerService());

            container.Register<INotificationAdapter>(factory =>
            {
                Type adapterType = Assembly.Load(WebAssemblyName).GetType("MomentumPlus.Relay.Web.SignalRHubs.Adapters.NotificationAdapter");
                ConstructorInfo providerConstructor = adapterType.GetConstructor(new Type[] { });
                object adapterObject = providerConstructor.Invoke(new object[] { });

                return (adapterObject as INotificationAdapter);
            });

            return container;
        }

    }
}