﻿using System.Web.Mvc;

namespace IndodrillReport
{
    public class IndodrillReportArea : AreaRegistration
    {
        public override string AreaName
        {
            get { return "Indodrill"; }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Indodrill_default",
                "Indodrill/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }, 
                namespaces: new[] { "IndodrillReport.Controllers" }
            );
        }

    }
}