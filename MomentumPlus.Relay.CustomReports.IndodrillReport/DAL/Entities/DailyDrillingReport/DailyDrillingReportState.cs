﻿namespace IndodrillReport.DAL.Entities
{
    public enum DailyDrillingReportState
    {
        Created = 0,
        Completed = 1
    }
}