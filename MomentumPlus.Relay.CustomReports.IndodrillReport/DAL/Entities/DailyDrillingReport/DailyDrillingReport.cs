﻿using System;
using System.Collections.Generic;

namespace IndodrillReport.DAL.Entities
{
    public class DailyDrillingReport : BaseModel
    {
        public int? ReportNumber { get; set; }

        public Guid? CompanyInfoId { get; set; }
        public virtual CompanyInfo CompanyInfo { get; set; }

        public DailyDrillingReportState State { get; set; }

        public DateTime Date { get; set; }

        public Guid? ClientId { get; set; }
        public string ClientName { get; set; }

        public Guid? DrillerId { get; set; }
        public string DrillerName { get; set; }

        public Guid CreatorId { get; set; }
        public string CreatorName { get; set; }

        public Guid? ShiftId { get; set; }

        public string ShiftType { get; set; }

        public string Hole_I_D { get; set; }

        public int? LastBitChange { get; set; }

        public string Helpers { get; set; }

        public Guid? LocationId { get; set; }
        public string Location { get; set; }

        public Guid? DrillRigId { get; set; }
        public string DrillRig { get; set; }

        public string Angle { get; set; }

        public int? BitMTD { get; set; }

        public int? WeekToDateDS { get; set; }

        public int? WeekToDateNS { get; set; }

        public int? WeekToDateCombined { get; set; }

        public string DrillersAssistant { get; set; }

        public string DrillerComments { get; set; }

        public string ClientComments { get; set; }

        public virtual ICollection<Bit> Bits { get; set; }

        public virtual ICollection<Approval> Approvals { get; set; }

        public virtual ICollection<Activity> Activities { get; set; }

        public virtual ICollection<LeftInHole> LeftInHole { get; set; }

        public virtual ICollection<UsedMaterial> UsedMaterials { get; set; }

        public Guid ModuleId { get; set; }
        public virtual IndodrillReportsModule Module { get; set; }

    }
}