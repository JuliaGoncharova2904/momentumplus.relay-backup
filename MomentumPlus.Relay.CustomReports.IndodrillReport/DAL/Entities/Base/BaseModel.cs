﻿using System;

namespace IndodrillReport.DAL.Entities
{
    public class BaseModel
    {
        public Guid Id { get; set; }
        public DateTime? CreatedUtc { get; set; }
        public DateTime? ModifiedUtc { get; set; }
    }
}