﻿using System;
using System.Collections.Generic;

namespace IndodrillReport.DAL.Entities
{
    public class IndodrillReportsModule : BaseModel
    {
        public int LastReportId { get; set; }
        public Guid? CurrentCompanyInfoId { get; set; }
        public virtual ICollection<CompanyInfo> CompanyInfoStory { get; set; }
        public virtual ICollection<Client> Clients { get; set; }
        public virtual ICollection<ActivityTemplate> Activities { get; set; }
        public virtual ICollection<MaterialTemplate> Materials { get; set; }
        public virtual ICollection<LostInItemTemplate> LostInItems { get; set; }
        public virtual ICollection<RigTemplate> Rigs { get; set; }
        public virtual ICollection<DailyDrillingReport> Reports { get; set; }
    }
}