﻿using System;

namespace IndodrillReport.DAL.Entities
{
    public class Location : BaseModel
    {
        public string Name { get; set; }

        public Guid ClientId { get; set; }
        public virtual Client Client { get; set; }
    }
}