﻿using System;

namespace IndodrillReport.DAL.Entities
{
    public class ActivityTemplate : BaseModel
    {
        public string Name { get; set; }
        public string CostCode { get; set; }

        public Guid ModuleId { get; set; }
        public virtual IndodrillReportsModule Module { get; set; }
    }
}