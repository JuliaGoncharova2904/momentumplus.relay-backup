﻿using System;

namespace IndodrillReport.DAL.Entities
{
    public class LostInItemTemplate : BaseModel
    {
        public string Name { get; set; }
        public string Units { get; set; }

        public Guid ModuleId { get; set; }
        public virtual IndodrillReportsModule Module { get; set; }
    }
}