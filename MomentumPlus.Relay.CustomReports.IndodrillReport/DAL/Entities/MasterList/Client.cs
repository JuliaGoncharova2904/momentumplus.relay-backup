﻿using System;
using System.Collections.Generic;

namespace IndodrillReport.DAL.Entities
{
    public class Client : BaseModel
    {
        public string Name { get; set; }
        public virtual ICollection<Location> Locations { get; set; }

        public Guid ModuleId { get; set; }
        public virtual IndodrillReportsModule Module { get; set; }
    }
}