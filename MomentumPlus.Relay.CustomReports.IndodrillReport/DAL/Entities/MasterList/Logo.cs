﻿using System;
using System.Collections.Generic;

namespace IndodrillReport.DAL.Entities
{
    public class Logo : BaseModel
    {
        public Guid RelayLogoId { get; set; }
        public byte[] BinaryData { get; set; }
        public string ContentType { get; set; }

        public virtual ICollection<CompanyInfo> CompanyInfos { get; set; }
    }
}