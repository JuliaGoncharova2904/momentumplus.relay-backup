﻿using System;
using System.Collections.Generic;

namespace IndodrillReport.DAL.Entities
{
    public class CompanyInfo : BaseModel
    {
        public string Name { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string Site { get; set; }

        public Guid? LogoId { get; set; }
        public virtual Logo Logo { get; set; }

        public virtual ICollection<DailyDrillingReport> Reports { get; set; }

        public Guid ModuleId { get; set; }
        public virtual IndodrillReportsModule Module { get; set; }
    }
}