﻿using System;

namespace IndodrillReport.DAL.Entities
{
    public class RigTemplate : BaseModel
    {
        public string Name_Number { get; set; }

        public Guid ModuleId { get; set; }
        public virtual IndodrillReportsModule Module { get; set; }
    }
}