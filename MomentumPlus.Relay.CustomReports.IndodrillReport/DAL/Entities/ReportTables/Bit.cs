﻿using System;

namespace IndodrillReport.DAL.Entities
{
    public class Bit : BaseModel
    {
        public Guid? TemplateBitId { get; set; }

        public string Type { get; set; }

        public string Size { get; set; }

        public string SerialNo { get; set; }

        public float? On { get; set; }

        public float? Off { get; set; }

        public Guid ReportId { get; set; }
        public virtual DailyDrillingReport Report { get; set; }
    }
}