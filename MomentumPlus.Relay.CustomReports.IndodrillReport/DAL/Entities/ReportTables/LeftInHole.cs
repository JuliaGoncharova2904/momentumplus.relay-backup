﻿using System;

namespace IndodrillReport.DAL.Entities
{
    public class LeftInHole : BaseModel
    {
        public Guid? TemplateLeftInItemId { get; set; }

        public string Name { get; set; }

        public string Units { get; set; }

        public int? Quantity { get; set; }

        public Guid ReportId { get; set; }
        public virtual DailyDrillingReport Report { get; set; }
    }
}