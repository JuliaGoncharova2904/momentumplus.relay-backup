﻿using System;

namespace IndodrillReport.DAL.Entities
{
    public class Activity : BaseModel
    {
        public Guid? TemplateActivityId { get; set; }

        public string Name { get; set; }

        public string CostCode { get; set; }

        public int? StartTime { get; set; }

        public int? EndTime { get; set; }

        public float? From { get; set; }

        public float? To { get; set; }

        public float? Recovered { get; set; }

        public string CoreSize { get; set; }

        public string Comments { get; set; }

        public Guid ReportId { get; set; }
        public virtual DailyDrillingReport Report { get; set; }
    }
}