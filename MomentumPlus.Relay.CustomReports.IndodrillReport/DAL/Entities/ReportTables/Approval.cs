﻿using System;

namespace IndodrillReport.DAL.Entities
{
    public class Approval : BaseModel
    {
        public string Name { get; set; }

        public bool IsApproved { get; set; }

        public string Signature { get; set; }

        public Guid ReportId { get; set; }
        public virtual DailyDrillingReport Report { get; set; }
    }
}