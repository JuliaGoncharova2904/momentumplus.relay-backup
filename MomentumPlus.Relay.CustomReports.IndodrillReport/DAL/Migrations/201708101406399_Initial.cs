namespace IndodrillReport.DAL.DbInit
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "indodrill.Activities",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TemplateActivityId = c.Guid(),
                        Name = c.String(),
                        CostCode = c.String(),
                        StartTime = c.Int(),
                        EndTime = c.Int(),
                        From = c.Single(),
                        To = c.Single(),
                        Recovered = c.Single(),
                        CoreSize = c.String(),
                        Comments = c.String(),
                        ReportId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("indodrill.DailyDrillingReports", t => t.ReportId, cascadeDelete: true)
                .Index(t => t.ReportId);
            
            CreateTable(
                "indodrill.DailyDrillingReports",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ReportNumber = c.Int(),
                        CompanyInfoId = c.Guid(),
                        State = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        ClientId = c.Guid(),
                        ClientName = c.String(),
                        DrillerId = c.Guid(),
                        DrillerName = c.String(),
                        CreatorId = c.Guid(nullable: false),
                        CreatorName = c.String(),
                        ShiftId = c.Guid(),
                        ShiftType = c.String(),
                        Hole_I_D = c.String(),
                        LastBitChange = c.Int(),
                        Helpers = c.String(),
                        LocationId = c.Guid(),
                        Location = c.String(),
                        DrillRigId = c.Guid(),
                        DrillRig = c.String(),
                        Angle = c.String(),
                        BitMTD = c.Int(),
                        WeekToDateDS = c.Int(),
                        WeekToDateNS = c.Int(),
                        WeekToDateCombined = c.Int(),
                        DrillersAssistant = c.String(),
                        DrillerComments = c.String(),
                        ClientComments = c.String(),
                        ModuleId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("indodrill.CompanyInfo", t => t.CompanyInfoId)
                .ForeignKey("indodrill.IndodrillReportsModules", t => t.ModuleId)
                .Index(t => t.CompanyInfoId)
                .Index(t => t.ModuleId);
            
            CreateTable(
                "indodrill.Approvals",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        IsApproved = c.Boolean(nullable: false),
                        Signature = c.String(),
                        ReportId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("indodrill.DailyDrillingReports", t => t.ReportId, cascadeDelete: true)
                .Index(t => t.ReportId);
            
            CreateTable(
                "indodrill.Bits",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TemplateBitId = c.Guid(),
                        Type = c.String(),
                        Size = c.String(),
                        SerialNo = c.String(),
                        On = c.Single(),
                        Off = c.Single(),
                        ReportId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("indodrill.DailyDrillingReports", t => t.ReportId, cascadeDelete: true)
                .Index(t => t.ReportId);
            
            CreateTable(
                "indodrill.CompanyInfo",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Address = c.String(),
                        Phone = c.String(),
                        Email = c.String(),
                        Site = c.String(),
                        LogoId = c.Guid(),
                        ModuleId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("indodrill.Logotips", t => t.LogoId)
                .ForeignKey("indodrill.IndodrillReportsModules", t => t.ModuleId)
                .Index(t => t.LogoId)
                .Index(t => t.ModuleId);
            
            CreateTable(
                "indodrill.Logotips",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        RelayLogoId = c.Guid(nullable: false),
                        BinaryData = c.Binary(),
                        ContentType = c.String(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "indodrill.IndodrillReportsModules",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        LastReportId = c.Int(nullable: false),
                        CurrentCompanyInfoId = c.Guid(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "indodrill.ActivityTemplates",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        CostCode = c.String(),
                        ModuleId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("indodrill.IndodrillReportsModules", t => t.ModuleId)
                .Index(t => t.ModuleId);
            
            CreateTable(
                "indodrill.Clients",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        ModuleId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("indodrill.IndodrillReportsModules", t => t.ModuleId)
                .Index(t => t.ModuleId);
            
            CreateTable(
                "indodrill.Locations",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        ClientId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("indodrill.Clients", t => t.ClientId, cascadeDelete: true)
                .Index(t => t.ClientId);
            
            CreateTable(
                "indodrill.LostInItemTemplates",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Units = c.String(),
                        ModuleId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("indodrill.IndodrillReportsModules", t => t.ModuleId)
                .Index(t => t.ModuleId);
            
            CreateTable(
                "indodrill.MaterialTemplates",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Units = c.String(),
                        ModuleId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("indodrill.IndodrillReportsModules", t => t.ModuleId)
                .Index(t => t.ModuleId);
            
            CreateTable(
                "indodrill.RigTemplates",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name_Number = c.String(),
                        ModuleId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("indodrill.IndodrillReportsModules", t => t.ModuleId)
                .Index(t => t.ModuleId);
            
            CreateTable(
                "indodrill.LeftInHoles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TemplateLeftInItemId = c.Guid(),
                        Name = c.String(),
                        Units = c.String(),
                        Quantity = c.Int(),
                        ReportId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("indodrill.DailyDrillingReports", t => t.ReportId, cascadeDelete: true)
                .Index(t => t.ReportId);
            
            CreateTable(
                "indodrill.UsedMaterials",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TemplateActivityId = c.Guid(),
                        Name = c.String(),
                        Units = c.String(),
                        Quantity = c.Int(),
                        ReportId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("indodrill.DailyDrillingReports", t => t.ReportId, cascadeDelete: true)
                .Index(t => t.ReportId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("indodrill.Activities", "ReportId", "indodrill.DailyDrillingReports");
            DropForeignKey("indodrill.UsedMaterials", "ReportId", "indodrill.DailyDrillingReports");
            DropForeignKey("indodrill.DailyDrillingReports", "ModuleId", "indodrill.IndodrillReportsModules");
            DropForeignKey("indodrill.LeftInHoles", "ReportId", "indodrill.DailyDrillingReports");
            DropForeignKey("indodrill.DailyDrillingReports", "CompanyInfoId", "indodrill.CompanyInfo");
            DropForeignKey("indodrill.CompanyInfo", "ModuleId", "indodrill.IndodrillReportsModules");
            DropForeignKey("indodrill.RigTemplates", "ModuleId", "indodrill.IndodrillReportsModules");
            DropForeignKey("indodrill.MaterialTemplates", "ModuleId", "indodrill.IndodrillReportsModules");
            DropForeignKey("indodrill.LostInItemTemplates", "ModuleId", "indodrill.IndodrillReportsModules");
            DropForeignKey("indodrill.Clients", "ModuleId", "indodrill.IndodrillReportsModules");
            DropForeignKey("indodrill.Locations", "ClientId", "indodrill.Clients");
            DropForeignKey("indodrill.ActivityTemplates", "ModuleId", "indodrill.IndodrillReportsModules");
            DropForeignKey("indodrill.CompanyInfo", "LogoId", "indodrill.Logotips");
            DropForeignKey("indodrill.Bits", "ReportId", "indodrill.DailyDrillingReports");
            DropForeignKey("indodrill.Approvals", "ReportId", "indodrill.DailyDrillingReports");
            DropIndex("indodrill.UsedMaterials", new[] { "ReportId" });
            DropIndex("indodrill.LeftInHoles", new[] { "ReportId" });
            DropIndex("indodrill.RigTemplates", new[] { "ModuleId" });
            DropIndex("indodrill.MaterialTemplates", new[] { "ModuleId" });
            DropIndex("indodrill.LostInItemTemplates", new[] { "ModuleId" });
            DropIndex("indodrill.Locations", new[] { "ClientId" });
            DropIndex("indodrill.Clients", new[] { "ModuleId" });
            DropIndex("indodrill.ActivityTemplates", new[] { "ModuleId" });
            DropIndex("indodrill.CompanyInfo", new[] { "ModuleId" });
            DropIndex("indodrill.CompanyInfo", new[] { "LogoId" });
            DropIndex("indodrill.Bits", new[] { "ReportId" });
            DropIndex("indodrill.Approvals", new[] { "ReportId" });
            DropIndex("indodrill.DailyDrillingReports", new[] { "ModuleId" });
            DropIndex("indodrill.DailyDrillingReports", new[] { "CompanyInfoId" });
            DropIndex("indodrill.Activities", new[] { "ReportId" });
            DropTable("indodrill.UsedMaterials");
            DropTable("indodrill.LeftInHoles");
            DropTable("indodrill.RigTemplates");
            DropTable("indodrill.MaterialTemplates");
            DropTable("indodrill.LostInItemTemplates");
            DropTable("indodrill.Locations");
            DropTable("indodrill.Clients");
            DropTable("indodrill.ActivityTemplates");
            DropTable("indodrill.IndodrillReportsModules");
            DropTable("indodrill.Logotips");
            DropTable("indodrill.CompanyInfo");
            DropTable("indodrill.Bits");
            DropTable("indodrill.Approvals");
            DropTable("indodrill.DailyDrillingReports");
            DropTable("indodrill.Activities");
        }
    }
}
