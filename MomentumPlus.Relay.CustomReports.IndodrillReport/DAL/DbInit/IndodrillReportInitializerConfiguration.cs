namespace IndodrillReport.DAL.DbInit
{
    using System.Data.Entity.Migrations;

    public sealed class IndodrillReportInitializerConfiguration : DbMigrationsConfiguration<IndodrillReportsContext>
    {
        public IndodrillReportInitializerConfiguration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;
            MigrationsDirectory = "DAL\\Migrations";
        }

        protected override void Seed(IndodrillReportsContext context)
        {
            IndodrillReportInitializer.InitializeModuleAndCompanyInfo(context);
        }

    }
}
