﻿using MomentumPlus.Core.Models;
using System;

namespace IndodrillReport.DAL.DbInit
{
    public static class IndodrillReportInitializerConstants
    {
        public static class IndodrillModule
        {
            public static readonly Guid Id = new Guid("D93E8B72-1756-6CC1-BBED-855153269077");
        }

        public static class CompanyInfo
        {
            public static readonly Guid Id = new Guid("D93E8B72-1756-6CC1-7777-855153269077");
        }
    }
}
