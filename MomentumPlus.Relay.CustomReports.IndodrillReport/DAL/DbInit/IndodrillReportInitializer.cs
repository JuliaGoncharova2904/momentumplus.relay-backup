﻿using System.Data.Entity;
using IndodrillReport.DAL.Entities;

namespace IndodrillReport.DAL.DbInit
{

    public class IndodrillReportInitializer : MigrateDatabaseToLatestVersion<IndodrillReportsContext, IndodrillReportInitializerConfiguration>
    {
        public override void InitializeDatabase(IndodrillReportsContext context)
        {
            if (!context.Database.Exists())
            {
                context.Database.Create();
            }
        }

        public static void InitializeModuleAndCompanyInfo(IndodrillReportsContext context)
        {
            IndodrillReportsModule module = context.Module.Find(IndodrillReportInitializerConstants.IndodrillModule.Id);
            CompanyInfo companyInfo = context.CompanyInfo.Find(IndodrillReportInitializerConstants.CompanyInfo.Id);

            if(companyInfo == null)
            {
                if(module == null)
                {
                    context.Module.Add(new IndodrillReportsModule
                    {
                        Id = IndodrillReportInitializerConstants.IndodrillModule.Id,
                        CurrentCompanyInfoId = IndodrillReportInitializerConstants.CompanyInfo.Id
                    });
                }

                context.CompanyInfo.Add(new CompanyInfo
                {
                    Id = IndodrillReportInitializerConstants.CompanyInfo.Id,
                    Email = "general@indodrill.com",
                    Phone = "(045) 499-1159",
                    Site = "www.indodrill.com",
                    Name = "INDODRILL PHILLIPINES, INC",
                    Address = @"Bldg. 1 & 2 Berthaphil II South </br> Bayanihan St.Calrk Freeport Zone </ br > 2023 Pampanga, Philippines </ br > ",
                    ModuleId = IndodrillReportInitializerConstants.IndodrillModule.Id
                });
            }
        }
    }

}
