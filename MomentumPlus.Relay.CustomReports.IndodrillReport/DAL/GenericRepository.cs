﻿using System;
using System.Collections.Generic;
using System.Linq;
using IndodrillReport.DAL.Interfaces;
using System.Data.Entity;
using IndodrillReport.DAL.Entities;

namespace IndodrillReport.DAL
{
    class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : BaseModel
    {
        private readonly IndodrillReportsContext _context = null;
        readonly DbSet<TEntity> _dbSet;

        public GenericRepository(IndodrillReportsContext context)
        {
            this._context = context;
            _dbSet = this._context.Set<TEntity>();
        }

        public IQueryable<TEntity> GetAll()
        {
            return _dbSet;
        }

        public TEntity Get(Guid id)
        {
            TEntity entity = _dbSet.Find(id);

            if(entity == null)
            {
                throw new Exception(string.Format("{0} with ID:{1} was not found.", typeof(TEntity).Name, id));
            }

            return entity;
        }

        public void Add(TEntity entity)
        {
            _dbSet.Add(entity);
        }

        public void Update(TEntity entity)
        {
            _dbSet.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(Guid id)
        {
            TEntity entity = _dbSet.Find(id);
            if (entity != null)
            {
                _dbSet.Remove(entity);
            }
        }

        public void Delete(TEntity entity)
        {
            if (_context.Entry(entity).State == EntityState.Detached)
            {
                _dbSet.Attach(entity);
            }
            _dbSet.Remove(entity);
        }
    }
}