﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using IndodrillReport.DAL.DbInit;
using IndodrillReport.DAL.Entities;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System;

namespace IndodrillReport.DAL
{
    public class IndodrillReportsContext : DbContext
    {
        public IndodrillReportsContext()
            : base("IndodrillReport")
        {
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;

            //Database.SetInitializer(new IndodrillReportInitializer());
        }

        #region Database sets

        public DbSet<IndodrillReportsModule> Module { get; set; }

        public DbSet<DailyDrillingReport> DailyDrillingReports { get; set; }
        //--- report tables -------
        public DbSet<Activity> Activities { get; set; }
        public DbSet<UsedMaterial> MaterialsUsed { get; set; }
        public DbSet<LeftInHole> LeftInHole { get; set; }
        public DbSet<Bit> Bits { get; set; }
        public DbSet<Approval> Approvals { get; set; }

        //--- master list ---------
        public DbSet<Logo> Logotips { get; set; }
        public DbSet<CompanyInfo> CompanyInfo { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<ActivityTemplate> ActivityTemplates { get; set; }
        public DbSet<MaterialTemplate> MaterialTemplates { get; set; }
        public DbSet<LostInItemTemplate> LostInItemsTemplates { get; set; }
        public DbSet<RigTemplate> RigTemplates { get; set; }

        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.HasDefaultSchema("indodrill");

            DailyDrillingReport_EntitiesConfiguration(modelBuilder);
            MasterList_EntitiesConfiguration(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

        #region Fluent API configuration

        
        private void DailyDrillingReport_EntitiesConfiguration(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DailyDrillingReport>()
                        .HasOptional(dr => dr.CompanyInfo)
                        .WithMany(ci => ci.Reports)
                        .HasForeignKey(dr => dr.CompanyInfoId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<DailyDrillingReport>()
                        .HasRequired(dr => dr.Module)
                        .WithMany(m => m.Reports)
                        .HasForeignKey(dr => dr.ModuleId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Activity>()
                        .HasRequired(activity => activity.Report)
                        .WithMany(report => report.Activities)
                        .HasForeignKey(activity => activity.ReportId)
                        .WillCascadeOnDelete(true);

            modelBuilder.Entity<Bit>()
                        .HasRequired(bit => bit.Report)
                        .WithMany(report => report.Bits)
                        .HasForeignKey(bit => bit.ReportId)
                        .WillCascadeOnDelete(true);

            modelBuilder.Entity<UsedMaterial>()
                        .HasRequired(material => material.Report)
                        .WithMany(report => report.UsedMaterials)
                        .HasForeignKey(material => material.ReportId)
                        .WillCascadeOnDelete(true);

            modelBuilder.Entity<LeftInHole>()
                        .HasRequired(material => material.Report)
                        .WithMany(report => report.LeftInHole)
                        .HasForeignKey(material => material.ReportId)
                        .WillCascadeOnDelete(true);

            modelBuilder.Entity<Approval>()
                        .HasRequired(approval => approval.Report)
                        .WithMany(report => report.Approvals)
                        .HasForeignKey(approval => approval.ReportId)
                        .WillCascadeOnDelete(true);


        }

        private void MasterList_EntitiesConfiguration(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Logo>().ToTable("Logotips");
            modelBuilder.Entity<CompanyInfo>().ToTable("CompanyInfo");

            modelBuilder.Entity<CompanyInfo>()
                        .HasOptional(ci => ci.Logo)
                        .WithMany(l => l.CompanyInfos)
                        .HasForeignKey(ci => ci.LogoId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<CompanyInfo>()
                        .HasRequired(ci => ci.Module)
                        .WithMany(m => m.CompanyInfoStory)
                        .HasForeignKey(ci => ci.ModuleId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Location>()
                        .HasRequired(l => l.Client)
                        .WithMany(c => c.Locations)
                        .HasForeignKey(l => l.ClientId)
                        .WillCascadeOnDelete(true);

            modelBuilder.Entity<Client>()
                        .HasRequired(c => c.Module)
                        .WithMany(m => m.Clients)
                        .HasForeignKey(c => c.ModuleId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<ActivityTemplate>()
                        .HasRequired(at => at.Module)
                        .WithMany(m => m.Activities)
                        .HasForeignKey(at => at.ModuleId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<MaterialTemplate>()
                        .HasRequired(mt => mt.Module)
                        .WithMany(m => m.Materials)
                        .HasForeignKey(mt => mt.ModuleId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<LostInItemTemplate>()
                        .HasRequired(lt => lt.Module)
                        .WithMany(m => m.LostInItems)
                        .HasForeignKey(lt => lt.ModuleId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<RigTemplate>()
                        .HasRequired(rt => rt.Module)
                        .WithMany(m => m.Rigs)
                        .HasForeignKey(rt => rt.ModuleId)
                        .WillCascadeOnDelete(false);
        }

        #endregion

        public override int SaveChanges()
        {
            foreach (DbEntityEntry entity in ChangeTracker.Entries().Where(e => e.Entity is BaseModel))
            {
                if (entity.State == EntityState.Added)
                    (entity.Entity as BaseModel).CreatedUtc = DateTime.UtcNow;

                if (entity.State == EntityState.Modified)
                    (entity.Entity as BaseModel).ModifiedUtc = DateTime.UtcNow;
            }

            return base.SaveChanges();
        }

    }
}

