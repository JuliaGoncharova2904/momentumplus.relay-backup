﻿using System;
using System.Collections.Generic;
using System.Linq;
using IndodrillReport.DAL.Entities;
using IndodrillReport.DAL.Interfaces;
using System.Data.Entity;

namespace IndodrillReport.DAL
{
    public class GenericUnitOfWork : IDisposable
    {
        private readonly IndodrillReportsContext _entities = null;

        public GenericUnitOfWork()
        {
            _entities = new IndodrillReportsContext();
        }

        public DbContextTransaction BeginTransaction()
        {
            return _entities.Database.BeginTransaction();
        }

        public Dictionary<Type, object> Repositories = new Dictionary<Type, object>();

        public IRepository<T> Repository<T>() where T : BaseModel
        {
            if (Repositories.Keys.Contains(typeof(T)) == true)
            {
                return Repositories[typeof (T)] as IRepository<T>;
            }
            IRepository<T> repo = new GenericRepository<T>(_entities);
            Repositories.Add(typeof(T), repo);
            return repo;
        }

        public void SaveChanges()
        {
            _entities.SaveChanges();
        }

        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _entities.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}