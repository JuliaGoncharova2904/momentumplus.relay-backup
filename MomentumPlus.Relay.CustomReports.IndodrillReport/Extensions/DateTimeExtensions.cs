﻿using System;

namespace IndodrillReport.Extensions
{
    public static class DateTimeExtensions
    {
        private static string ConvertDay(int day)
        {
            string suffix = string.Empty;

            switch (day)
            {
                case 1:
                case 21:
                case 31:
                    suffix = "st";
                    break;
                case 2:
                case 22:
                    suffix = "nd";
                    break;
                case 3:
                case 23:
                    suffix = "rd";
                    break;
                default:
                    suffix = "th";
                    break;
            }

            return string.Format("{0}{1}", day, suffix);
        }

        public static string DayFormat(this DateTime dt)
        {
            return ConvertDay(dt.Day);
        }

        public static string FormatWithMonth(this DateTime dt)
        {
            return string.Format("{1} {0:MMM}", dt, ConvertDay(dt.Day));
        }

        public static string FormatWithMonth(this DateTime? dt)
        {
            if (dt.HasValue)
                return string.Format("{1} {0:MMM}", dt, ConvertDay(dt.Value.Day));

            return string.Empty;
        }

        public static string FormatWithDayMonthYear(this DateTime dt)
        {
            return string.Format("{1} {0:MMM} {0:yy}", dt, ConvertDay(dt.Day));
        }

    }
}
