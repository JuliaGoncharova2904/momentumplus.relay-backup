﻿"use strict";

var gulp = require("gulp");
var uglify = require("gulp-uglify");
var less = require("gulp-less");
var cleanCSS = require("gulp-clean-css");
var jshint = require('gulp-jshint');

gulp.task("build-dev", function (callback) {
    gulp.src('./Frontend/src/js/*.js')
        .pipe(gulp.dest('./Frontend/dist/'));

    gulp.src('./Frontend/src/style/indodrill-reports.less')
        .pipe(less())
        .pipe(gulp.dest('./Frontend/dist/'));

    callback();
});

gulp.task("build-prod", function (callback) {

    gulp.src('./Frontend/src/js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('./Frontend/dist/'));

    gulp.src('./Frontend/src/style/indodrill-reports.less')
        .pipe(less())
        .pipe(cleanCSS())
        .pipe(gulp.dest('./Frontend/dist/'));

    callback();
});

gulp.task("watch-prod-js", function (callback) {

    gulp.src('./Frontend/src/js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('./Frontend/dist/'));

    callback();
});

gulp.task("watch-prod-less", function (callback) {

    gulp.src('./Frontend/src/style/indodrill-reports.less')
        .pipe(less())
        .pipe(cleanCSS())
        .pipe(gulp.dest('./Frontend/dist/'));

    callback();
});

gulp.task("watch-prod", function () {

    gulp.watch('./Frontend/src/**', ['build-prod']);
    
});
