﻿(function () {
    //===========================================================================
    //  clientId
    //===================== Indodrill Client Dialog =============================
    function IndodrillClientDialog(clientId) {

        this.clientId = clientId;
        this.container = null;

        this.openHandler = null;
        this.closeHandler = null;

        this.saveBtn = null;
        this.closeBtn = null;

        this.locationsSwiper = null;
        this.addEditLocation = null;

        this.closeEvent = IndodrillClientDialog_CloseEvent.bind(this);
        this.saveEvent = IndodrillClientDialog_SaveEvent.bind(this);

        this.config = {
            addClientUrl: "/Indodrill/ClientTemplate/AddClientFormDialog",
            editClientUrl: "/Indodrill/ClientTemplate/EditClientFormDialog",
            addLocationUrl: "/Indodrill/ClientTemplate/AddLocation",
            editLocationlUrl: "/Indodrill/ClientTemplate/EditLocation",
            removeLocationUrl: "/Indodrill/ClientTemplate/RemoveLocation",
            getLocationsUrl: "/Indodrill/ClientTemplate/LocationsForClient"
        };
    }
    IndodrillClientDialog.prototype.open = function () {
        var _this = this;
        //--------------------------
        $('#loading-animation').show();
        if (_this.openHandler) {
            _this.openHandler();
        }
        //--------------------------
        $.ajax({
            url: this.clientId ? this.config.editClientUrl : this.config.addClientUrl,
            method: "GET",
            data: { clientId: this.clientId }
        })
        .done(function (result) {
            IndodrillClientDialog_BuildDialog.call(_this, result);
            IndodrillClientDialog_OpenDialog.call(_this);
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
        .always(function () {
            $('#loading-animation').hide();
        });
        //--------------------------
    };
    IndodrillClientDialog.prototype.setOpenEvent = function (eventHandler) {
        this.openHandler = eventHandler;
    };
    IndodrillClientDialog.prototype.setCloseEvent = function (eventHandler) {
        this.closeHandler = eventHandler;
    };

    function IndodrillClientDialog_CloseEvent() {
        var confirmDialog = Relay.ConfirmDialog(
            "All changes will be lost. Are you sure you want to exit?",
            "",
            "Yes",
            "No"
        );
        confirmDialog.setOkEvent(function () {
            $(this.container).children().first().modal('hide');
        }.bind(this));
        confirmDialog.open();
    };

    function IndodrillClientDialog_SaveEvent() {
        $.when(IndodrillClientDialog_SubmitForm.call(this), this.addEditLocation.save())
         .done(IndodrillClientDialog_UnactivateSaveBtn.bind(this));
    }

    function IndodrillClientDialog_ActivateSaveBtn() {
        //------- Confirm close event --------
        this.closeBtn.removeAttr("data-dismiss");
        this.closeBtn.off("click");
        this.closeBtn.on("click", this.closeEvent);
        //------- Active save button ---------
        this.saveBtn.addClass("active");
        this.saveBtn.off("click");
        this.saveBtn.on("click", this.saveEvent);
        //------------------------------------
    }
    function IndodrillClientDialog_UnactivateSaveBtn() {
        //------- Confirm close event --------
        this.closeBtn.attr("data-dismiss", "modal");
        this.closeBtn.off("click", this.closeEvent);
        //------- Active save button ---------
        this.saveBtn.removeClass("active")
        this.saveBtn.off("click", this.saveEvent);
        //------------------------------------
    }

    function IndodrillClientDialog_SubmitForm() {
        var _this = this;
        var form = $(".client-form form", this.conteiner).first();

        return $.Deferred(function (defer) {

            $.validator.unobtrusive.parse(form);
            if (form.valid()) {
                $.ajax({
                    url: form[0].action,
                    method: form[0].method,
                    data: form.serialize()
                })
                .done(function (result) {
                    if (result.clientTemplateId) {
                        IndodrillClientDialog_LoadEditDialog.call(_this, result.clientTemplateId);
                        defer.resolve();
                    }
                    else if (result == "ok") {
                        defer.resolve();
                    }
                    else {
                        form.html($(result).children());
                        $('#Name', _this.container).on("input", $.debounce(500, IndodrillClientDialog_ActivateSaveBtn.bind(_this)));
                    }
                })
                .fail(function () {
                    defer.reject();
                    Relay.ErrorDialog().open();
                })
            }
            else {
                defer.reject();
            }
        }).promise();
    }

    function IndodrillClientDialog_LoadEditDialog(clientId) {
        var _this = this;
        this.clientId = clientId;

        $.ajax({
            url: this.config.editClientUrl,
            method: "GET",
            data: { clientId: clientId }
        })
        .done(function (result) {
            $(_this.container).children().children().html($(result).children());
            $(_this.container).children().removeClass("add-client-form-dialog").addClass("client-form-dialog");
            IndodrillClientDialog_InitControls.call(_this);
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        });
    }

    function IndodrillClientDialog_BuildDialog(dialogHtml) {
        var _this = this;

        this.container = document.createElement("div");
        this.container.innerHTML = dialogHtml;
        document.body.appendChild(this.container);

        $(this.container).children().first().on('hidden.bs.modal', function () {
            $(_this.container).remove();
            if (_this.closeHandler) {
                _this.closeHandler();
            }
        });

        IndodrillClientDialog_InitControls.call(this);
    }
    function IndodrillClientDialog_InitControls() {
        Relay.InitInputFields(this.container);

        this.saveBtn = $(".btn-save", this.container).first();
        this.closeBtn = $(".btn-close", this.container).first();

        $('#Name', this.container).on("input", $.debounce(500, IndodrillClientDialog_ActivateSaveBtn.bind(this)));

        this.locationsSwiper = new LocationsSwiper(this.config, this.container, this.clientId);
        this.addEditLocation = new AddEditLocation(this.config, this.container, this.clientId, this.locationsSwiper);

        this.addEditLocation.setChangeHandler(IndodrillClientDialog_ActivateSaveBtn.bind(this));
    }
    function IndodrillClientDialog_OpenDialog() {
        $(this.container).children().first().modal('show');
    }
    //===============================================================================
    function LocationsSwiper(config, container, clientId) {
        var _this = this;

        this.url = config.getLocationsUrl;
        this.clientId = clientId;
        this.swiper = $(".swiper-container", container);
        this.clickHandler = null;
        this.removeHandler = null;

        this.locationsSwiper = new Swiper(this.swiper, {
            direction: 'vertical',
            height: 40,
            nextButton: $(".swiper-next", container),
            prevButton: $(".swiper-prev", container)
        });

        $(".location-name", this.swiper).on("click", function () {
            var locationId = $(this).parent().attr("location-id");
            _this.clickHandler && _this.clickHandler(locationId);
        });

        $(".remove-location", this.swiper).on("click", function () {
            var locationId = $(this).parent().attr("location-id");
            _this.removeHandler && _this.removeHandler(locationId);
        });
    }
    LocationsSwiper.prototype.setClickHandler = function (handler) {
        this.clickHandler = handler;
    };
    LocationsSwiper.prototype.setRemoveHandler = function (handler) {
        this.removeHandler = handler;
    };
    LocationsSwiper.prototype.update = function () {
        var _this = this;

        $.ajax({
            url: this.url,
            method: "POST",
            data: { clientId: this.clientId }
        })
        .done(function (result) {
            _this.swiper.children().html($(result));
            _this.locationsSwiper.update(false);
 
            $(".location-name", _this.swiper).on("click", function () {
                var locationId = $(this).parent().attr("location-id");
                _this.clickHandler && _this.clickHandler(locationId);
            });

            $(".remove-location", _this.swiper).on("click", function () {
                var locationId = $(this).parent().attr("location-id");
                _this.removeHandler && _this.removeHandler(locationId);
            });
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
    }
    //===============================================================================
    function AddEditLocation(config, container, clientId, locationObj) {
        this.config = config;
        this.clientId = clientId;
        this.addBtn = $(".btn-plus-block", container);
        this.slideContainer = $("#new-location-for-client", container);
        this.locationObj = locationObj;

        this.changeHandler = null;

        this.slideContainer.slideUp();

        $(this.addBtn).on("click", this.add.bind(this));
        locationObj.setClickHandler(this.edit.bind(this));
        locationObj.setRemoveHandler(this.remove.bind(this));
    }
    AddEditLocation.prototype.setChangeHandler = function (handler) {
        this.changeHandler = handler;
    };
    AddEditLocation.prototype.remove = function (id) {
        var _this = this;

        var confirmDialog = Relay.ConfirmDialog(
            "Remove location",
            "Do toy want to remove this location?",
            "Yes",
            "No"
        );
        confirmDialog.setOkEvent(function () {
            $.ajax({
                url: _this.config.removeLocationUrl,
                method: "POST",
                data: { locationId: id }
            })
            .done(function (result) {
                _this.locationObj.update();
            })
            .fail(function () {
                Relay.ErrorDialog().open();
            })
        });
        confirmDialog.open();
    };
    AddEditLocation.prototype.add = function () {
        var _this = this;

        var loadingAnimationTimeout = setTimeout(function () {
            $('#loading-animation').show();
        }, 200);

        $.ajax({
            url: this.config.addLocationUrl,
            method: "GET",
            data: { clientId: this.clientId }
        })
        .done(function (result) {
            _this.slideContainer.html(result);
            Relay.InitInputFields(_this.slideContainer);
            _this.slideContainer.slideDown();
            _this.addBtn.css('display', 'none');
            $(".add-new-item", _this.slideContainer).on("click", function () {
                _this.save().then();
            });
            _this.changeHandler && _this.changeHandler();
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
        .always(function () {
            clearTimeout(loadingAnimationTimeout);
            $('#loading-animation').hide();
        });
    };
    AddEditLocation.prototype.edit = function (id) {
        var _this = this;

        var loadingAnimationTimeout = setTimeout(function () {
            $('#loading-animation').show();
        }, 200);

        $.ajax({
            url: this.config.editLocationlUrl,
            method: "GET",
            data: { locationId: id }
        })
        .done(function (result) {
            _this.slideContainer.html(result);
            Relay.InitInputFields(_this.slideContainer);
            _this.slideContainer.slideDown();
            _this.addBtn.css('display', 'block');
            $(".add-new-item", _this.slideContainer).on("click", function () {
                _this.save().then();
            });
            _this.changeHandler && _this.changeHandler();
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
        .always(function () {
            clearTimeout(loadingAnimationTimeout);
            $('#loading-animation').hide();
        });
    };
    AddEditLocation.prototype.save = function () {
        var _this = this;
        var form = $("form", this.slideContainer);

        return $.Deferred(function (defer) {

            if (form.length === 0) {
                defer.resolve();
                return;
            }

            $.validator.unobtrusive.parse(form);
            if (form.valid()) {
                $.ajax({
                    url: form[0].action,
                    method: form[0].method,
                    data: form.serialize()
                })
                .done(function (result) {
                    if (result === "ok") {
                        _this.locationObj.update();
                        _this.slideContainer.slideUp();
                        _this.slideContainer.empty();
                        _this.addBtn.css('display', 'block');
                        defer.resolve();
                    } else {
                        _this.slideContainer.html(result);
                        Relay.InitInputFields(_this.slideContainer);
                        $(".add-new-item", _this.slideContainer).on("click", function () {
                            _this.save().then();
                        });
                        defer.reject();
                    }
                })
                .fail(function () {
                    defer.reject();
                    Relay.ErrorDialog().open();
                })
            }
            else {
                defer.reject();
            }
        }).promise();
    }
    //===========================================================================
    window.Relay = window.Relay || {};
    window.Relay.IndodrillClientDialog = function (clientId) {
        return new IndodrillClientDialog(clientId);
    };
    //===========================================================================
})();