﻿$(document).ready(function () {

    var indodrillReportsTable = $(".indodrill-reports-table");

    indodrillReportsTable.show();

    $("#Filters.Driller").selectpicker({
        liveSearch: false
    });

    $(".select-list", indodrillReportsTable).selectpicker({
        style: 'select-class',
        size: 7,
        dropupAuto: false,
        container: "body"
    });

    $("select.select-list", "td").on("change", function () {
        var filter = Relay.UrlQuery.getQueryModel();

        switch ($(this).attr("name")) {
            case "Filters.DrillRig":
                filter.DrillRig = $(this).val();
                break;
            case "Filters.Client":
                filter.Client = $(this).val();
                break;
            case "Filters.Location":
                filter.Location = $(this).val();
                break;
            case "Filters.Driller":
                filter.Driller = $(this).val();
                break;
        }

        Relay.UrlQuery.updateQueryModel(filter);
    });

    $(".report-row .remove-btn", indodrillReportsTable).on("click", function () {

        var reportId = $(this).attr("report-id");

        var confirm = Relay.ConfirmDialog("Remove Report", "Do you want to remove Report?", "Yes", "No");
        confirm.setOkEvent(function () {

            var loadingAnimationTimeout = setTimeout(function () {
                $('#loading-animation').show();
            }, 200);

            $.ajax({
                url: "/Indodrill/Report/RemoveReport",
                method: "POST",
                data: { reportId: reportId }
            })
            .done(function (data) {
                location.reload();
            })
            .fail(function () {
                var errorDialog = Relay.ErrorDialog();
                errorDialog.setCloseEvent(function () {
                    location.reload();
                });
                errorDialog.open();
            })
            .always(function () {
                clearTimeout(loadingAnimationTimeout);
                $('#loading-animation').hide();
            });

        });
        confirm.open();

    });
});