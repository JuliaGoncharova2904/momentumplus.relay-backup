﻿//===================================================================================
(function (window) {
    //-----------------------------------------------------------------
    var debounceDelay = 300;
    //-----------------------------------------------------------------
    function LoadingAnimation(ajaxPromise) {
        var loadingAnimationTimeout = setTimeout(function () {
            $('#loading-animation').show();
        }, 200);

        ajaxPromise.always(function () {
            clearTimeout(loadingAnimationTimeout);
            $('#loading-animation').hide();
        });
    }
    //-----------------------------------------------------------------
    function Report(reportElement) {
        this.reportId = $(reportElement).attr("report-id");
        this.validateFields = $(reportElement).attr("valid-fields") === "True";

        this.reportFields = new ReportFields($("#reportFields", reportElement), $("#reportComments", reportElement), this.reportId);
        this.summaryTable = new SummaryTable($("#summaryTable", reportElement));
        this.activityTable = new ActivityTable($("#activityTable", reportElement), this.reportId, this.summaryTable.reloadContent.bind(this.summaryTable));
        this.materialsTable = new MaterialsTable($("#materialsTable", reportElement), this.reportId);
        this.bitsTable = new BitsTable($("#bitsTable", reportElement), this.reportId);
        this.leftInHoleTable = new LeftInHoleTable($("#leftInHoleTable", reportElement), this.reportId);
        this.approvalsTable = new ApprovalsTable($("#approvalsTable", reportElement));

        reportElement.show();

        this.validateFields && this.validate();
    }
    Report.prototype.validate = function () {
        var res = true;

        if (!this.reportFields.validate()) res = false;
        if (!this.activityTable.validate()) res = false;
        if (!this.materialsTable.validate()) res = false;
        if (!this.leftInHoleTable.validate()) res = false;

        return res;
    };
    //-----------------------------------------------------------------
    function ReportFields(reportFieldsElement, reportCommentsElement, reportId) {
        var _this = this;
        this.fieldsElement = reportFieldsElement;
        this.reportFieldsModel = { ReportId: reportId };

        this.isValidated = false;

        this.clientField = $("#ClientId", reportFieldsElement);
        this.locationField = $("#LocationId", reportFieldsElement);
        this.weekToDateDsField = $("#WeekToDateDS", reportFieldsElement);
        this.weekToDateNsField = $("#WeekToDateNS", reportFieldsElement);
        this.drillerField = $("#DrillerId", reportFieldsElement);
        this.drillRigField = $("#DrillRigId", reportFieldsElement);
        this.shift = $("#Shift", reportFieldsElement);
        this.weekToDateCombinedField = $("#WeekToDateCombined", reportFieldsElement);
        this.holeIdField = $("#Hole_I_D", reportFieldsElement);
        this.angleField = $("#Angle", reportFieldsElement);
        this.drillersAssistantField = $("#DrillersAssistant", reportFieldsElement);
        this.lastBitChangeField = $("#LastBitChange", reportFieldsElement);
        this.bitMTDField = $("#BitMTD", reportFieldsElement);

        var helpersString = $("#Helpers", reportFieldsElement).val();
        this.helpersField = $(".helpers-input", reportFieldsElement).magicSuggest({
            placeholder: "No Helpers",
            maxSelection: 10,
            hideTrigger: true,
            toggleOnClick: true,
            name: "Helpers",
            value: helpersString ? helpersString.split(",") : null
        });

        this.drillerCommentField = $("#DrillerComment", reportCommentsElement);
        this.clientCommentField = $("#ClientComment", reportCommentsElement);
        //-------------
        var updateServer = $.debounce(debounceDelay, this.updateServer.bind(this));
        //-------------
        this.clientField.on("change", function () {
            var i;
            var option;

            $.ajax({
                url: "/Indodrill/Report/GetLocationsForClient",
                method: "POST",
                data: { clientId: _this.clientField.val() }
            })
            .done(function (data) {
                _this.locationField.empty();
                for (i = 0; i < data.length; ++i) {
                    option = document.createElement("option");
                    option.value = data[i].Value;
                    option.text = data[i].Text;
                    _this.locationField.append(option);
                }
                _this.locationField.selectpicker('refresh');

                _this.updateServer.call(_this);
            })
            .fail(function () {
                var errorDialog = Relay.ErrorDialog();
                errorDialog.setCloseEvent(function () {
                    location.reload();
                });
                errorDialog.open();
            });
        });
        this.locationField.on("change", this.updateServer.bind(this));
        this.weekToDateDsField.on("input", updateServer);
        this.weekToDateNsField.on("input", updateServer);
        this.drillerField.on("change", this.updateServer.bind(this));
        this.drillRigField.on("change", this.updateServer.bind(this));
        this.shift.on("change", this.updateServer.bind(this));
        this.weekToDateCombinedField.on("input", updateServer);
        this.holeIdField.on("input", updateServer);
        this.angleField.on("input", updateServer);
        this.drillersAssistantField.on("input", updateServer);
        this.lastBitChangeField.on("input", updateServer);
        this.bitMTDField.on("input", updateServer);
        $(this.helpersField).on("selectionchange", updateServer);

        this.drillerCommentField.on("input", updateServer);
        this.clientCommentField.on("input", updateServer);
        //----
        this.lastBitChangeField.mask("000000", { 'translation': { 0: { pattern: /^[0-9]+$/ } } });
        this.bitMTDField.mask("000000", { 'translation': { 0: { pattern: /^[0-9]+$/ } } });
        //--------------
        $("#BitMTD, #LastBitChange, #WeekToDateDS, #WeekToDateNS , #WeekToDateCombined", reportFieldsElement).mask("000000");

        $(".select-list", reportFieldsElement).selectpicker({
            style: 'select-class',
            size: 7,
            dropupAuto: false
        });
        //--------------
        this.datePicker = $(".date-block .simple-input", reportFieldsElement);

        $(this.datePicker).datepicker({
            format: "dd M yyyy",
            container: ".report-fields"
        }).on('changeDate', function () {
            $(this).val(moment($(this).val()).format("DD MMM YYYY"));
            $(this).datepicker('hide');
            _this.updateServer();
        });
    }
    ReportFields.prototype.validate = function () {
        var res = true;
        var validInput = function (inputElement, state) {
            if (state) {
                inputElement.parent().removeClass("validation-error");
            }
            else {
                inputElement.parent().addClass("validation-error");
                res = false;
            }
        };
        var validSelect = function (selectElement, state) {
            if (state) {
                selectElement.parent().parent().removeClass("validation-error");
            }
            else {
                selectElement.parent().parent().addClass("validation-error");
                res = false;
            }
        };

        this.isValidated = true;
        this.updateModel();

        validSelect(this.clientField, this.reportFieldsModel.ClientId);
        validSelect(this.locationField, this.reportFieldsModel.LocationId);
        validSelect(this.shift, this.reportFieldsModel.Shift);
        validSelect(this.drillerField, this.reportFieldsModel.DrillerId);
        validSelect(this.drillRigField, this.reportFieldsModel.DrillRigId);

        validInput(this.drillersAssistantField, this.reportFieldsModel.DrillersAssistant);
        validInput($("#Helpers", this.fieldsElement), this.reportFieldsModel.Helpers);

        return res;
    };
    ReportFields.prototype.updateModel = function () {
        this.reportFieldsModel.ClientId = this.clientField.val();
        this.reportFieldsModel.LocationId = this.locationField.val();
        this.reportFieldsModel.WeekToDateDS = this.weekToDateDsField.val();
        this.reportFieldsModel.Date = Relay.DotNetDate.dateToDateString($(this.datePicker).datepicker('getDate'));
        this.reportFieldsModel.WeekToDateNS = this.weekToDateNsField.val();
        this.reportFieldsModel.DrillerId = this.drillerField.val();
        this.reportFieldsModel.DrillRigId = this.drillRigField.val();
        this.reportFieldsModel.Shift = this.shift.val();
        this.reportFieldsModel.WeekToDateCombined = this.weekToDateCombinedField.val();
        this.reportFieldsModel.Hole_I_D = this.holeIdField.val();
        this.reportFieldsModel.Angle = this.angleField.val();
        this.reportFieldsModel.DrillersAssistant = this.drillersAssistantField.val();
        this.reportFieldsModel.LastBitChange = this.lastBitChangeField.val();
        this.reportFieldsModel.BitMTD = this.bitMTDField.val();
        this.reportFieldsModel.Helpers = this.helpersField.getValue().join();
        this.reportFieldsModel.DrillerComment = this.drillerCommentField.val();
        this.reportFieldsModel.ClientComment = this.clientCommentField.val();
    };
    ReportFields.prototype.updateServer = function () {

        this.isValidated ? this.validate() : this.updateModel();

        $.ajax({
            url: "/Indodrill/Report/UpdateReportFields",
            method: "POST",
            data: this.reportFieldsModel
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    //-----------------------------------------------------------------
    function ActivityTable(activityTableElement, reportId, changeHandler) {
        this.reportId = reportId;
        this.changeHandler = changeHandler;
        this.element = activityTableElement;
        this.rows = [];

        var _this = this;

        $("tbody tr", activityTableElement).each(function (index, element) {
            _this.rows.push(new ActivityRow(element, _this.rows, changeHandler));
        });

        $(".add-item-btn", activityTableElement).on("click", this.addRow.bind(this));

        if (changeHandler)
            changeHandler(this.rows);
    }
    ActivityTable.prototype.addRow = function () {
        var _this = this;

        LoadingAnimation($.ajax({
            url: "/Indodrill/Report/AddActivityRow",
            method: "POST",
            data: {
                reportId: this.reportId
            }
        })
        .done(function (data) {
            var row = $(data);
            _this.rows.push(new ActivityRow(row, _this.rows, _this.changeHandler));
            $("tbody", _this.element).append(row);

            if (_this.changeHandler)
                _this.changeHandler(_this.rows);
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        }));
    };
    ActivityTable.prototype.validate = function () {
        var i;
        var res = true;

        for (i = 0; i < this.rows.length; ++i) {
            if (!this.rows[i].validate()) {
                res = false;
            }
        }

        return res;
    };
    function ActivityRow(activityRowElement, rows, changeHandler) {
        var _this = this;

        this.isValidated = false;

        this.rows = rows;
        this.changeHandler = changeHandler;
        this.element = activityRowElement;
        this.activityRowModel = { Id: $(this.element).attr("row-id") };
        //-----
        $(".select-list", activityRowElement).selectpicker({
            style: 'select-class',
            size: 7,
            dropupAuto: false
        });
        //-----
        $(".remove-icon", activityRowElement).on("click", function () {
            var confirm = Relay.ConfirmDialog("Activity Table", "Do you want to remove row?", "Yes", "No");
            confirm.setOkEvent(_this.removeRow.bind(_this));
            confirm.open();
        });
        //-----
        this.templateField = $("#TemplateId", activityRowElement);
        this.costCodeBody = $(".cost-code-body", activityRowElement);
        this.startTimeField = $("#StartTime", activityRowElement);
        this.endTimeField = $("#EndTime", activityRowElement);
        this.fromField = $("#From", activityRowElement);
        this.toField = $("#To", activityRowElement);
        this.recoveredField = $("#Recovered", activityRowElement);
        this.coreSizeField = $("#CoreSize", activityRowElement);
        this.drillingDetailsField = $("#DrillingDetails", activityRowElement);
        //-----
        var updateServer = $.debounce(debounceDelay, this.updateServer.bind(this));
        //-----
        this.fromField.mask("000000", { 'translation': { 0: { pattern: /^[0-9.]+$/ } } });
        this.toField.mask("000000", { 'translation': { 0: { pattern: /^[0-9.]+$/ } } });
        this.recoveredField.mask("000000", { 'translation': { 0: { pattern: /^[0-9.]+$/ } } });
        //-----
        this.templateField.on("change", function () {

            $.ajax({
                url: "/Indodrill/Report/GetCostCode",
                method: "POST",
                data: { activityId: _this.templateField.val() }
            })
            .done(function (data) {
                _this.costCodeBody.html(data.costCode);
                _this.updateServer.call(_this);
            })
            .fail(function () {
                var errorDialog = Relay.ErrorDialog();
                errorDialog.setCloseEvent(function () {
                    location.reload();
                });
                errorDialog.open();
            });
        });
        //-----
        var scrollToTime = ActivityRow_ScrollToTime.bind(this);

        if (this.startTimeField.val() === "" && rows.length !== 0) {
            this.startTimeField.on("shown.bs.select", scrollToTime);
        }
        //-----
        this.startTimeField.on("change", function () {
            var min = $(this).val();
            _this.endTimeField.selectpicker('val', min);
            updateServer();
            _this.startTimeField.off("shown.bs.select", scrollToTime);
        });
        this.endTimeField.on("change", updateServer);
        this.fromField.on("input", updateServer);
        this.toField.on("input", updateServer);
        this.recoveredField.on("input", updateServer);
        this.coreSizeField.on("input", updateServer);
        this.drillingDetailsField.on("input", updateServer);
        //-----
        this.startTimeField.on("change", this.fillAutoCalcFields.bind(this));
        this.endTimeField.on("change", this.fillAutoCalcFields.bind(this));
        this.fromField.on("input", this.fillAutoCalcFields.bind(this));
        this.toField.on("input", this.fillAutoCalcFields.bind(this));
        this.fillAutoCalcFields();
        this.updateModel();
    }
    function ActivityRow_ScrollToTime() {
        var thisIndex = this.rows.indexOf(this);

        if (thisIndex !== -1) {
            var endPrevActivity = this.rows[thisIndex - 1].endTimeField.val();

            if (endPrevActivity !== "") {
                var time = Math.trunc(endPrevActivity / 60) + ':' + endPrevActivity % 60;

                this.startTimeField
                    .parent()
                    .find(".dropdown-menu.open ul")
                    .scrollTo($("li>a:contains('" + time + "')", this.startTimeField.parent()));
            }
        }
    };
    ActivityRow.prototype.fillAutoCalcFields = function () {
        var hours = (Number(this.endTimeField.val()) - Number(this.startTimeField.val())) / 60;
        var cored = Math.round10((Number(this.toField.val()) - Number(this.fromField.val())), -5);

        if (hours < 0)
        {
            hours = hours + 24;
        }

        $(".hours-body", this.element).text(hours);
        $(".cored-body", this.element).text(cored);
    };
    ActivityRow.prototype.updateModel = function () {
        this.activityRowModel.TemplateId = this.templateField.val();
        this.activityRowModel.Name = $("option[value='" + this.activityRowModel.TemplateId + "']", this.templateField).html();
        this.activityRowModel.StartTime = this.startTimeField.val();
        this.activityRowModel.EndTime = this.endTimeField.val();
        this.activityRowModel.From = this.fromField.val();
        this.activityRowModel.To = this.toField.val();
        this.activityRowModel.Recovered = this.recoveredField.val();
        this.activityRowModel.CoreSize = this.coreSizeField.val();
        this.activityRowModel.DrillingDetails = this.drillingDetailsField.val();
    };
    ActivityRow.prototype.validate = function () {
        var res = true;
        var validCell = function (cell, set) {
            if (set) {
                cell.parent().parent().removeClass("validation-error");
            }
            else {
                cell.parent().parent().addClass("validation-error");
                res = false;
            }
        }

        this.isValidated = true;
        this.updateModel();

        validCell(this.templateField, this.activityRowModel.TemplateId);
        validCell(this.startTimeField, this.activityRowModel.StartTime);
        validCell(this.endTimeField, this.activityRowModel.EndTime);

        return res;
    };
    ActivityRow.prototype.updateServer = function () {
        var _this = this;

        this.isValidated ? this.validate() : this.updateModel();

        $.ajax({
            url: "/Indodrill/Report/UpdateActivityRow",
            method: "POST",
            data: this.activityRowModel
        })
        .done(function (data) {
            if (_this.changeHandler)
                _this.changeHandler(_this.rows);
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    ActivityRow.prototype.removeRow = function () {
        var _this = this;

        LoadingAnimation($.ajax({
            url: "/Indodrill/Report/RemoveActivityRow",
            method: "POST",
            data: { activityId: this.activityRowModel.Id }
        })
        .done(function (data) {
            _this.element.remove();
            var index = _this.rows.indexOf(_this);
            if (index >= 0) {
                _this.rows.splice(index, 1);
            }

            if (_this.changeHandler)
                _this.changeHandler(_this.rows);
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        }));
    };
    //-----------------------------------------------------------------
    function MaterialsTable(materialsTableElement, reportId) {
        var _this = this;

        this.reportId = reportId;
        this.element = materialsTableElement;
        this.rows = [];

        $("tbody tr", materialsTableElement).each(function (index, element) {
            _this.rows.push(new MaterialsRow(element, _this.rows));
        });

        $(".add-item-btn", materialsTableElement).on("click", this.addRow.bind(this));
    }
    MaterialsTable.prototype.addRow = function () {
        var _this = this;

        LoadingAnimation($.ajax({
            url: "/Indodrill/Report/AddUsedMaterialRow",
            method: "POST",
            data: {
                reportId: this.reportId
            }
        })
        .done(function (data) {
            var row = $(data);
            _this.rows.push(new MaterialsRow(row, _this.rows));
            $("tbody", _this.element).append(row);
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        }));
    };
    MaterialsTable.prototype.validate = function () {
        var i;
        var res = true;

        for (i = 0; i < this.rows.length; ++i) {
            if (!this.rows[i].validate()) {
                res = false;
            }
        }

        return res;
    };
    function MaterialsRow(materialsRowElement, rows) {
        var _this = this;
        this.element = materialsRowElement;
        this.rows = rows;
        this.isValidated = false;

        this.materialsRowModel = { Id: $(materialsRowElement).attr("row-id") };
        //----
        this.nameField = $("#Name", materialsRowElement);
        this.unitsField = $("#Units", materialsRowElement);
        this.quantityField = $("#Quantity", materialsRowElement);
        //-----
        this.quantityField.mask("000000", { 'translation': { 0: { pattern: /^[0-9.]+$/ } } });
        //----
        var updateServer = $.debounce(debounceDelay, this.updateServer.bind(this));
        //----
        this.nameField.on("input", updateServer);
        this.unitsField.on("input", updateServer);
        this.quantityField.on("input", updateServer);
        //-----
        $(".remove-icon", materialsRowElement).on("click", function () {
            var confirm = Relay.ConfirmDialog("Used Materials Table", "Do you want to remove row?", "Yes", "No");
            confirm.setOkEvent(_this.removeRow.bind(_this));
            confirm.open();
        });
    }
    MaterialsRow.prototype.updateModel = function () {
        this.materialsRowModel.Name = this.nameField.val();
        this.materialsRowModel.Units = this.unitsField.val();
        this.materialsRowModel.Quantity = this.quantityField.val();
    };
    MaterialsRow.prototype.validate = function () {
        var res = true;
        var validCell = function (cell, set) {
            if (set) {
                cell.parent().removeClass("validation-error");
            }
            else {
                cell.parent().addClass("validation-error");
                res = false;
            }
        }

        this.isValidated = true;
        this.updateModel();

        validCell(this.nameField, this.materialsRowModel.Name);
        validCell(this.unitsField, this.materialsRowModel.Units);
        validCell(this.quantityField, this.materialsRowModel.Quantity);

        return res;
    };
    MaterialsRow.prototype.updateServer = function () {

        this.isValidated ? this.validate() : this.updateModel();

        $.ajax({
            url: "/Indodrill/Report/UpdateUsedMaterialRow",
            method: "POST",
            data: this.materialsRowModel
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    MaterialsRow.prototype.removeRow = function () {
        var _this = this;

        LoadingAnimation($.ajax({
            url: "/Indodrill/Report/RemoveUsedMaterialRow",
            method: "POST",
            data: { materialId: this.materialsRowModel.Id }
        })
        .done(function (data) {
            var index = _this.rows.indexOf(_this);
            if (index >= 0) {
                _this.rows.splice(index, 1);
            }

            _this.element.remove();
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        }));
    };
    //-----------------------------------------------------------------
    function BitsTable(bitsTableElement, reportId) {
        this.reportId = reportId;
        this.element = bitsTableElement;

        $("tbody tr", bitsTableElement).each(function (index, element) {
            new BitsRow(element);
        });

        $(".add-item-btn", bitsTableElement).on("click", this.addRow.bind(this));
    }
    BitsTable.prototype.addRow = function () {
        var _this = this;

        LoadingAnimation($.ajax({
            url: "/Indodrill/Report/AddBitRow",
            method: "POST",
            data: {
                reportId: this.reportId
            }
        })
        .done(function (data) {
            var row = $(data);
            new BitsRow(row);
            $("tbody", _this.element).append(row);
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        }));
    };
    function BitsRow(bitsRowElement) {
        var _this = this;
        this.element = bitsRowElement;

        this.bitsRowModel = { Id: $(bitsRowElement).attr("row-id") };
        //----
        this.typeField = $("#Type", bitsRowElement);
        this.sizeField = $("#Size", bitsRowElement);
        this.serialNumberField = $("#SerialNumber", bitsRowElement);
        this.onField = $("#On", bitsRowElement);
        this.offField = $("#Off", bitsRowElement);
        //----
        var updateServer = $.debounce(debounceDelay, this.updateServer.bind(this));
        //----
        this.typeField.on("input", updateServer);
        this.sizeField.on("input", updateServer);
        this.serialNumberField.on("input", updateServer);
        this.onField.on("input", updateServer);
        this.offField.on("input", updateServer);
        //-----
        this.onField.mask("000000", { 'translation': { 0: { pattern: /^[0-9.]+$/ } } });
        this.offField.mask("000000", { 'translation': { 0: { pattern: /^[0-9.]+$/ } } });
        //-----
        $(".remove-icon", bitsRowElement).on("click", function () {
            var confirm = Relay.ConfirmDialog("Bits Table", "Do you want to remove row?", "Yes", "No");
            confirm.setOkEvent(_this.removeRow.bind(_this));
            confirm.open();
        });
        //-----
        this.onField.on("input", this.fillAutoCalcFields.bind(this));
        this.offField.on("input", this.fillAutoCalcFields.bind(this));
        this.fillAutoCalcFields();
    }
    BitsRow.prototype.fillAutoCalcFields = function () {
        var drilled = Math.round10((Number(this.offField.val()) - Number(this.onField.val())), -5);

        $(".drilled-body", this.element).text(drilled);
    };
    BitsRow.prototype.updateModel = function () {
        this.bitsRowModel.Type = this.typeField.val();
        this.bitsRowModel.Size = this.sizeField.val();
        this.bitsRowModel.SerialNumber = this.serialNumberField.val();
        this.bitsRowModel.On = this.onField.val();
        this.bitsRowModel.Off = this.offField.val();
    };
    BitsRow.prototype.updateServer = function () {
        this.updateModel();

        $.ajax({
            url: "/Indodrill/Report/UpdateBitRow",
            method: "POST",
            data: this.bitsRowModel
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    BitsRow.prototype.removeRow = function () {
        var _this = this;

        LoadingAnimation($.ajax({
            url: "/Indodrill/Report/RemoveBitRow",
            method: "POST",
            data: { bitId: this.bitsRowModel.Id }
        })
        .done(function (data) {
            _this.element.remove();
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        }));
    };
    //-----------------------------------------------------------------
    function LeftInHoleTable(leftInHoleTableElement, reportId) {
        var _this = this;

        this.reportId = reportId;
        this.element = leftInHoleTableElement;
        this.rows = [];

        $("tbody tr", leftInHoleTableElement).each(function (index, element) {
            _this.rows.push(new LeftInHoleRow(element, _this.rows));
        });

        $(".add-item-btn", leftInHoleTableElement).on("click", this.addRow.bind(this));
    }
    LeftInHoleTable.prototype.addRow = function () {
        var _this = this;

        LoadingAnimation($.ajax({
            url: "/Indodrill/Report/AddLeftInHoleRow",
            method: "POST",
            data: {
                reportId: this.reportId
            }
        })
        .done(function (data) {
            var row = $(data);
            _this.rows.push(new LeftInHoleRow(row, _this.rows));
            $("tbody", _this.element).append(row);
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        }));
    };
    LeftInHoleTable.prototype.validate = function () {
        var i;
        var res = true;

        for (i = 0; i < this.rows.length; ++i) {
            if (!this.rows[i].validate()) {
                res = false;
            }
        }

        return res;
    };
    function LeftInHoleRow(leftInHoleRowElement, rows) {
        var _this = this;
        this.element = leftInHoleRowElement;
        this.rows = rows;
        this.isValidated = false;

        this.leftInHoleRowModel = { Id: $(leftInHoleRowElement).attr("row-id") };
        //----
        this.nameField = $("#Name", leftInHoleRowElement);
        this.unitsField = $("#Units", leftInHoleRowElement);
        this.quantityField = $("#Quantity", leftInHoleRowElement);
        //-----
        this.quantityField.mask("000000", { 'translation': { 0: { pattern: /^[0-9.]+$/ } } });
        //----
        var updateServer = $.debounce(debounceDelay, this.updateServer.bind(this));
        //----
        this.nameField.on("input", updateServer);
        this.unitsField.on("input", updateServer);
        this.quantityField.on("input", updateServer);
        //-----
        $(".remove-icon", leftInHoleRowElement).on("click", function () {
            var confirm = Relay.ConfirmDialog("Left In Hole Table", "Do you want to remove row?", "Yes", "No");
            confirm.setOkEvent(_this.removeRow.bind(_this));
            confirm.open();
        });
    }
    LeftInHoleRow.prototype.updateModel = function () {
        this.leftInHoleRowModel.Name = this.nameField.val();
        this.leftInHoleRowModel.Units = this.unitsField.val();
        this.leftInHoleRowModel.Quantity = this.quantityField.val();
    };
    LeftInHoleRow.prototype.validate = function () {
        var res = true;
        var validCell = function (cell, set) {
            if (set) {
                cell.parent().removeClass("validation-error");
            }
            else {
                cell.parent().addClass("validation-error");
                res = false;
            }
        }

        this.isValidated = true;
        this.updateModel();

        validCell(this.nameField, this.leftInHoleRowModel.Name);
        validCell(this.unitsField, this.leftInHoleRowModel.Units);
        validCell(this.quantityField, this.leftInHoleRowModel.Quantity);

        return res;
    };
    LeftInHoleRow.prototype.updateServer = function () {

        this.isValidated ? this.validate() : this.updateModel();

        $.ajax({
            url: "/Indodrill/Report/UpdateLeftInHoleRow",
            method: "POST",
            data: this.leftInHoleRowModel
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    LeftInHoleRow.prototype.removeRow = function () {
        var _this = this;

        LoadingAnimation($.ajax({
            url: "/Indodrill/Report/RemoveLeftInHoleRow",
            method: "POST",
            data: { materialId: this.leftInHoleRowModel.Id }
        })
        .done(function (data) {
            var index = _this.rows.indexOf(_this);
            if (index >= 0) {
                _this.rows.splice(index, 1);
            }

            _this.element.remove();
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        }));
    };
    //-----------------------------------------------------------------
    function ApprovalsTable(approvalsTableElement) {
        this.element = approvalsTableElement;

        $("tbody tr", approvalsTableElement).each(function (index, element) {
            new ApprovalsRow(element);
        });
    }
    function ApprovalsRow(approvalsRowElement) {
        this.element = approvalsRowElement;

        this.approvalsRowModel = { Id: $(approvalsRowElement).attr("row-id") };
        //----
        this.isApprovedField = $("#IsApproved", approvalsRowElement);
        this.signatureField = $("#Signature", approvalsRowElement);
        //----
        var updateServer = $.debounce(debounceDelay, this.updateServer.bind(this));
        //----
        this.isApprovedField.on("change", updateServer);
        this.signatureField.on("input", updateServer);
    }
    ApprovalsRow.prototype.updateModel = function () {
        this.approvalsRowModel.IsApproved = this.isApprovedField.prop("checked");
        this.approvalsRowModel.Signature = this.signatureField.val();
    };
    ApprovalsRow.prototype.updateServer = function () {
        this.updateModel();

        $.ajax({
            url: "/Indodrill/Report/UpdateApprovalRow",
            method: "POST",
            data: this.approvalsRowModel
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    //-----------------------------------------------------------------
    function SummaryTable(summaryTableElement) {
        this.element = summaryTableElement;

        this.tbody = $("tbody", summaryTableElement);
    }
    SummaryTable.prototype.reloadContent = function (activityRows) {
        var i;
        var rowsNames = [];

        this.tbody.empty();

        for (i = 0; i < activityRows.length ; ++i) {
            if (rowsNames.indexOf(activityRows[i].activityRowModel.Name) === -1) {
                rowsNames.push(activityRows[i].activityRowModel.Name);
                this.tbody.append(this.createRow(activityRows[i].activityRowModel.Name));
            }
        }
    };
    SummaryTable.prototype.createRow = function (name) {
        var td;
        var tr = document.createElement("tr");

        td = document.createElement("td");
        td.className = "activity-body";
        td.textContent = name;
        tr.appendChild(td);

        td = document.createElement("td");
        td.className = "cost-code-body";
        tr.appendChild(td);

        td = document.createElement("td");
        td.className = "hours-body";
        tr.appendChild(td);

        td = document.createElement("td");
        td.className = "metres-body";
        tr.appendChild(td);

        return tr;
    };
    //-----------------------------------------------------------------
    function CompleteBlock(completeBlockElement, report) {
        var _this = this;
        this.reportId = $(completeBlockElement).attr("report-id");

        $(".remove-btn", completeBlockElement).on("click", function () {
            var confirm = Relay.ConfirmDialog("", "Do you want to discard the report?", "Yes", "No");
            confirm.setOkEvent(_this.discard.bind(_this));
            confirm.open();
        });

        $(".complete-btn", completeBlockElement).on("click", function () {
            if (report.validate()) {
                var confirm = Relay.ConfirmDialog("", "Do you want to complete the report?", "Yes", "No");
                confirm.setOkEvent(_this.complete.bind(_this));
                confirm.open();
            }
            else {
                Relay.InfoDialog("Required fields cannot be empty!").open();
            }
        });
    }
    CompleteBlock.prototype.discard = function () {

        LoadingAnimation($.ajax({
            url: "/Indodrill/Report/RemoveReport",
            method: "POST",
            data: { reportId: this.reportId }
        })
        .done(function (data) {
            location = "/Indodrill/Reports";
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        }));
    };
    CompleteBlock.prototype.complete = function () {
        var reportsUrl = "/Indodrill/Reports";
        var reportURL = "/Indodrill/Report/ReportPage?reportId=" + this.reportId + "&validateFields=true";

        LoadingAnimation($.ajax({
            url: "/Indodrill/Report/CompleteReport",
            method: "POST",
            data: { reportId: this.reportId }
        })
        .done(function (data) {
            location = data.isCompleted ? reportsUrl : reportURL;
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        }));
    };
    //-----------------------------------------------------------------
    $(document).ready(function () {
        var report = new Report($("#indodrillReport"));

        window.Relay = window.Relay || {};
        window.Relay.IndodrillReport = report
        window.Relay.CompleteBlock = new CompleteBlock("#completeBlock", report);
    });
    //-----------------------------------------------------------------
})(window);
//===================================================================================