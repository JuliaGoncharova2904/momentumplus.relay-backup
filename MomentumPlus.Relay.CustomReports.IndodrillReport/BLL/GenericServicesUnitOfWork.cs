﻿using IndodrillReport.BLL.Logic;
using IndodrillReport.BLL.Services;
using IndodrillReport.DAL;
using MomentumPlus.Core.DataSource;

namespace IndodrillReport.BLL
{
    public class GenericServicesUnitOfWork
    {
        private GenericUnitOfWork _repositories;
        private GenericUnitOfWork Repositories => _repositories ?? (_repositories = new GenericUnitOfWork());

        private RelayAccess _relayAccess;
        private RelayAccess RelayAccess => _relayAccess ?? (_relayAccess = new RelayAccess(new MomentumContext()));


        private ReportService _reportService;
        private ReportsListService _reportsListService;
        private ActivityTemplateService _activityTemplateService;
        private ClientTemplateService _clientTemplateService;
        private RigTemplateService _rigTemplateService;
        private PdfReportService _pdfReportService;
        private MediaService _mediaService;

        public ReportService ReportService => _reportService ?? (_reportService = new ReportService(this.Repositories, this.RelayAccess));
        public ReportsListService ReportsListService => _reportsListService ?? (_reportsListService = new ReportsListService(this.Repositories, this.RelayAccess));
        public ActivityTemplateService ActivityTemplateService => _activityTemplateService ?? (_activityTemplateService = new ActivityTemplateService(this.Repositories));
        public ClientTemplateService ClientTemplateService => _clientTemplateService ?? (_clientTemplateService = new ClientTemplateService(this.Repositories));
        public RigTemplateService RigTemplateService => _rigTemplateService ?? (_rigTemplateService = new RigTemplateService(this.Repositories));
        public MediaService MediaService => _mediaService ?? (_mediaService = new MediaService(this.Repositories));
        public PdfReportService PdfReportService => _pdfReportService ?? (_pdfReportService = new PdfReportService(this.Repositories));
    }
}