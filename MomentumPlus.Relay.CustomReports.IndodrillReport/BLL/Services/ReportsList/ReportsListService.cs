﻿using IndodrillReport.BLL.Logic;
using IndodrillReport.BLL.ViewModels;
using IndodrillReport.DAL;
using IndodrillReport.DAL.Entities;
using IndodrillReport.Extensions;
using MvcPaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace IndodrillReport.BLL.Services
{
    public class ReportsListService
    {
        private GenericUnitOfWork _repo;
        private RelayAccess _relayAccess;

        public ReportsListService(GenericUnitOfWork repo, RelayAccess relayAccess)
        {
            this._repo = repo;
            this._relayAccess = relayAccess;
        }

        /// <summary>
        /// Get drilling reports list using pagination params and filter params
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filter"></param>
        /// <returns>ReportsListViewModel</returns>
        public ReportsListViewModel GetReportsList(int page, int pageSize, ReportFilterViewModel filter)
        {
            IQueryable<DailyDrillingReport> reportEntitiesRaw = _repo.Repository<DailyDrillingReport>().GetAll();
            IQueryable<DailyDrillingReport> reportEntities = ReportsFilter.Apply(reportEntitiesRaw, filter);

            List<DailyDrillingReport> reportEntitiesList = reportEntities.OrderByDescending(r => r.Date).Skip(page * pageSize).Take(pageSize).ToList();

            ReportsListViewModel model = new ReportsListViewModel
            {
                Filters = this.FillReportsListFilter(reportEntitiesRaw, filter),
                ReportsItems = this.FillReportsList(reportEntitiesList).ToPagedList(page, pageSize, reportEntities.Count())
            };

            return model;
        }

        /// <summary>
        /// Fill reports list view model
        /// </summary>
        /// <param name="reportEntitiesList">Reports list entities</param>
        /// <returns></returns>
        private IEnumerable<ReportItemViewModel> FillReportsList(IEnumerable<DailyDrillingReport> reportEntitiesList)
        {
            List<ReportItemViewModel> reportListView = new List<ReportItemViewModel>();

            foreach (DailyDrillingReport report in reportEntitiesList)
            {
                reportListView.Add(new ReportItemViewModel
                {
                    Id = report.Id,
                    ClientName = report.ClientName,
                    CreatorId = report.CreatorId,
                    CreatorName = report.CreatorName,
                    DrillRig = report.DrillRig,
                    Location = report.Location,
                    CreatedDate = report.Date.FormatWithDayMonthYear(),
                    DrillerId = report.DrillerId,
                    DrillerName = report.DrillerName,
                    IsComplete = report.State == DailyDrillingReportState.Completed
                });
            }

            return reportListView;
        }

        /// <summary>
        /// Fill reports list filter view model
        /// </summary>
        /// <param name="reportEntitiesList">All reports entities</param>
        /// <param name="filter">Filter view model</param>
        /// <returns></returns>
        private ReportFilterViewModel FillReportsListFilter(IQueryable<DailyDrillingReport> reportEntitiesList, ReportFilterViewModel filter)
        {
            List<string> locations = reportEntitiesList.Select(r => r.Location).Distinct().ToList();
            List<string> clients = reportEntitiesList.Select(r => r.ClientName).Distinct().ToList();
            List<string> drillRigs = reportEntitiesList.Select(r => r.DrillRig).Distinct().ToList();
            List<Guid?> drillersIds = reportEntitiesList.Select(r => r.DrillerId).Distinct().ToList();

            ReportFilterViewModel filters = new ReportFilterViewModel
            {
                Locations = new List<SelectListItem>(new[] { new SelectListItem { Text = "All", Value = "" } }),
                Clients = new List<SelectListItem>(new[] { new SelectListItem { Text = "All", Value = "" } }),
                Drillers = new List<SelectListItem>(new[] { new SelectListItem { Text = "All", Value = "" } }),
                DrillRigs = new List<SelectListItem>(new[] { new SelectListItem { Text = "All", Value = "" } })
            };

            filters.Locations.AddRange(locations.Where(l => !string.IsNullOrEmpty(l))
                                                .Select(l => new SelectListItem { Text = l, Value = l, Selected = l == filter.Location })
                                                .OrderBy(l => l.Text));

            filters.Clients.AddRange(clients.Where(c => !string.IsNullOrEmpty(c))
                                            .Select(c => new SelectListItem { Text = c, Value = c, Selected = c == filter.Client })
                                            .OrderBy(с => с.Text));

            filters.Drillers.AddRange(this._relayAccess.GetRelayUsersListByIds(drillersIds));

            filters.DrillRigs.AddRange(drillRigs.Where(dr => !string.IsNullOrEmpty(dr))
                                                .Select(dr => new SelectListItem { Text = dr, Value = dr, Selected = dr == filter.DrillRig })
                                                .OrderBy(dr => dr.Text));

            return filters;
        }
    }
}