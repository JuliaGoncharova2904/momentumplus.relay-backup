﻿using System.Linq;
using IndodrillReport.DAL.Entities;
using System;
using IndodrillReport.BLL.ViewModels;

namespace IndodrillReport.BLL.Services
{
    public static class ReportsFilter
    {
        public static IQueryable<DailyDrillingReport> Apply(IQueryable<DailyDrillingReport> reports, ReportFilterViewModel filter)
        {
            reports = ClientFilter(reports, filter.Client);
            reports = DrillRigFilter(reports, filter.DrillRig);
            reports = LocationFilter(reports, filter.Location);
            reports = DrillerFilter(reports, filter.Driller);
            return reports;
        }

        private static IQueryable<DailyDrillingReport> ClientFilter(IQueryable<DailyDrillingReport> reports, string client)
        {
            if (!string.IsNullOrWhiteSpace(client))
            {
                return reports.Where(dr => dr.ClientName == client);
            }
            return reports;
        }

        private static IQueryable<DailyDrillingReport> LocationFilter(IQueryable<DailyDrillingReport> reports, string location)
        {
            if (!string.IsNullOrWhiteSpace(location))
            {
                return reports.Where(dr => dr.Location == location);
            }
            return reports;
        }

        private static IQueryable<DailyDrillingReport> DrillRigFilter(IQueryable<DailyDrillingReport> reports, string drillRig)
        {
            if (!string.IsNullOrWhiteSpace(drillRig))
            {
                return reports.Where(dr => dr.DrillRig == drillRig);
            }
            return reports;
        }

        private static IQueryable<DailyDrillingReport> DrillerFilter(IQueryable<DailyDrillingReport> reports, Guid? drillerId)
        {
            if (drillerId.HasValue)
            {
                return reports.Where(dr => dr.DrillerId == drillerId.Value);
            }
            return reports;
        }
    }
}