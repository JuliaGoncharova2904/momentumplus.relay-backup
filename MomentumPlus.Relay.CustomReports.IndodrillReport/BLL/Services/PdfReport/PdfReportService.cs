﻿using IndodrillReport.BLL.ViewModels;
using IndodrillReport.DAL;
using IndodrillReport.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace IndodrillReport.BLL.Services
{
    public class PdfReportService
    {
        private GenericUnitOfWork _repo;

        public PdfReportService(GenericUnitOfWork repo)
        {
            this._repo = repo;
        }

        public PdfReportViewModel GetReportForPrint(Guid reportId)
        {
            DailyDrillingReport drillingEntity =  _repo.Repository<DailyDrillingReport>().Get(reportId);

            PdfReportViewModel model = new PdfReportViewModel
            {
                ReportFields = this.FillReportFields(drillingEntity),
                CompanyInfo = this.FillCompanyInfo(drillingEntity),
                Activities = this.FillActivitiesTable(drillingEntity),
                Bits = this.FillBitsTable(drillingEntity),
                Approvals = this.FillApprovalsTable(drillingEntity),
                UsedMaterials = this.FillUsedMaterialsTable(drillingEntity),
                LeftMaterials = this.FillLeftInHoleTable(drillingEntity),
                Summary = this.FillSummaryTable(drillingEntity)
            };

            return model;
        }

        private PdfReportFieldsViewModel FillReportFields(DailyDrillingReport reportEntity)
        {

            PdfReportFieldsViewModel reportViewModel = new PdfReportFieldsViewModel
            {
                ReportId = reportEntity.Id,
                ReportNumber = string.Format("{0:D6}", reportEntity.ReportNumber),
                Date = reportEntity.Date,
                ClientName = reportEntity.ClientName,
                LocationName = reportEntity.Location,
                Shift = reportEntity.ShiftType,
                DrillerName = reportEntity.DrillerName,
                DrillRigName = reportEntity.DrillRig,
                Angle = reportEntity.Angle,
                BitMTD = reportEntity.BitMTD,
                DrillersAssistant = reportEntity.DrillersAssistant,
                Hole_I_D = reportEntity.Hole_I_D,
                Helpers = string.IsNullOrWhiteSpace(reportEntity.Helpers) ? new []{string.Empty} : reportEntity.Helpers.Split(','),
                LastBitChange = reportEntity.LastBitChange,
                WeekToDateNS = reportEntity.WeekToDateNS,
                WeekToDateDS = reportEntity.WeekToDateDS,
                WeekToDateCombined = reportEntity.WeekToDateCombined,
                DrillerComment = reportEntity.DrillerComments,
                ClientComment = reportEntity.ClientComments
            };

            return reportViewModel;
        }

        private PdfReportCompanyInformation FillCompanyInfo(DailyDrillingReport reportEntity)
        {
            CompanyInfo companyInfo = reportEntity.CompanyInfo;
            PdfReportCompanyInformation companyInfoView = new PdfReportCompanyInformation();

            if (companyInfo == null)
            {
                Guid? companyInfoId = _repo.Repository<IndodrillReportsModule>().GetAll().Select(m => m.CurrentCompanyInfoId).First();

                if (companyInfoId.HasValue)
                {
                    companyInfo = _repo.Repository<CompanyInfo>().Get(companyInfoId.Value);
                }
            }

            if (companyInfo != null)
            {
                companyInfoView.Name = companyInfo.Name;
                companyInfoView.Address = companyInfo.Address;
                companyInfoView.Phone = companyInfo.Phone;
                companyInfoView.Email = companyInfo.Email;
                companyInfoView.Site = companyInfo.Site;
                companyInfoView.LogoId = companyInfo.LogoId;
            }

            return companyInfoView;
        }

        private IEnumerable<PdfReportActivityViewModel> FillActivitiesTable(DailyDrillingReport reportEntity)
        {
            List<Activity> activities = reportEntity.Activities.OrderBy(a => a.CreatedUtc).ToList();
            List<PdfReportActivityViewModel> activityTable = new List<PdfReportActivityViewModel>();

            foreach (Activity activity in activities)
            {
                PdfReportActivityViewModel pdfActivity = new PdfReportActivityViewModel
                {
                    Id = activity.Id,
                    TemplateName = activity.Name,
                    EndTime = activity.EndTime.HasValue
                                                    ? string.Format("{0:D2}:{1:D2}", activity.EndTime / 60, activity.EndTime % 60)
                                                    : string.Empty,
                    Hours = string.Empty,
                    CoreSize = activity.CoreSize,
                    Cored = Math.Round(
                        ((activity.To.HasValue ? activity.To.Value : 0) - (activity.From.HasValue ? activity.From.Value : 0)), 5),
                    CostCode = activity.CostCode,
                    DrillingDetails = activity.Comments,
                    From = activity.From,
                    Recovered = activity.Recovered,
                    StartTime = activity.StartTime.HasValue
                                                    ? string.Format("{0:D2}:{1:D2}", activity.StartTime / 60, activity.StartTime % 60)
                                                    : string.Empty,
                    To = activity.To
                };

                if (activity.EndTime.HasValue && activity.StartTime.HasValue)
                {
                    float hours = (float)(activity.EndTime - activity.StartTime) / 60;
                    pdfActivity.Hours = string.Format("{0}", hours < 0 ? hours + 24 : hours);
                }

                activityTable.Add(pdfActivity);
            }

            return activityTable;
        }

        private IEnumerable<PdfReportBitViewModel> FillBitsTable(DailyDrillingReport reportEntity)
        {
            IEnumerable<PdfReportBitViewModel> bitsTable = reportEntity.Bits
                                                                    .OrderBy(a => a.CreatedUtc)
                                                                    .Select(b => new PdfReportBitViewModel
                                                                    {
                                                                        Id = b.Id,
                                                                        Type = b.Type,
                                                                        SerialNumber = b.SerialNo,
                                                                        Size = b.Size,
                                                                        Off = b.Off,
                                                                        On = b.On,
                                                                        Drilled = Math.Round(
                                                                            ((b.Off.HasValue ? b.Off.Value : 0)  - (b.On.HasValue ? b.On.Value : 0)), 5)
                                                                    });

            return bitsTable;
        }

        private IEnumerable<PdfReportApprovalViewModel> FillApprovalsTable(DailyDrillingReport reportEntity)
        {
            IEnumerable<PdfReportApprovalViewModel> approvalsTable = reportEntity.Approvals
                                                                            .OrderBy(a => a.CreatedUtc)
                                                                            .Select(ap => new PdfReportApprovalViewModel
                                                                            {
                                                                                Id = ap.Id,
                                                                                Name = ap.Name,
                                                                                IsApproved = ap.IsApproved,
                                                                                Signature = ap.Signature
                                                                            });

            return approvalsTable;
        }

        private IEnumerable<PdfReportMaterialViewModel> FillUsedMaterialsTable(DailyDrillingReport reportEntity)
        {
            IEnumerable<PdfReportMaterialViewModel> usedMaterialsTable = reportEntity.UsedMaterials
                                                                                .OrderBy(a => a.CreatedUtc)
                                                                                .Select(m => new PdfReportMaterialViewModel
                                                                                {
                                                                                    Id = m.Id,
                                                                                    Name = m.Name,
                                                                                    Quantity = m.Quantity,
                                                                                    Units = m.Units
                                                                                });

            return usedMaterialsTable;
        }

        private IEnumerable<PdfReportMaterialViewModel> FillLeftInHoleTable(DailyDrillingReport reportEntity)
        {
            IEnumerable<PdfReportMaterialViewModel> leftInHoleTable = reportEntity.LeftInHole
                                                                            .OrderBy(a => a.CreatedUtc)
                                                                            .Select(m => new PdfReportMaterialViewModel
                                                                            {
                                                                                Id = m.Id,
                                                                                Name = m.Name,
                                                                                Quantity = m.Quantity,
                                                                                Units = m.Units
                                                                            });

            return leftInHoleTable;
        }

        private IEnumerable<PdfReportSummaryViewModel> FillSummaryTable(DailyDrillingReport reportEntity)
        {
            IEnumerable<PdfReportSummaryViewModel> model = reportEntity.Activities.GroupBy(a => a.Name).OrderBy(a => a.FirstOrDefault().Name).Select(a => new PdfReportSummaryViewModel
            {
                ActivityName = a.FirstOrDefault().Name
            });
            return model;
        } 
    }
}