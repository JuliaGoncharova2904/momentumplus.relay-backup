﻿using IndodrillReport.BLL.ViewModels;
using IndodrillReport.DAL;
using IndodrillReport.DAL.Entities;
using System;

namespace IndodrillReport.BLL.Services
{
    public class MediaService
    {
        private GenericUnitOfWork _repo;

        public MediaService(GenericUnitOfWork repo)
        {
            this._repo = repo;
        }

        public LogoViewModel GetLogo(Guid logoId)
        {
            Logo logo = _repo.Repository<Logo>().Get(logoId);

            return new LogoViewModel
            {
                BinaryData = logo.BinaryData,
                ContentType = logo.ContentType
            };
        }
    }
}