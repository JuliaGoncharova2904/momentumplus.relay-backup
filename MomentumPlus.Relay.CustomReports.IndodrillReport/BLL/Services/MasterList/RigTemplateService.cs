﻿using IndodrillReport.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using IndodrillReport.BLL.ViewModels;
using IndodrillReport.DAL.Entities;
using MvcPaging;

namespace IndodrillReport.BLL.Services
{
    public class RigTemplateService
    {
        private GenericUnitOfWork _repo;

        public RigTemplateService(GenericUnitOfWork repo)
        {
            this._repo = repo;
        }

        /// <summary>
        /// Get all rig template for master list section
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public IPagedList<RigTemplateItemViewModel> GetRigsTemplate(int page, int pageSize)
        {
            IQueryable<RigTemplate> rigTemplateEntities = _repo.Repository<RigTemplate>().GetAll();
            IEnumerable<RigTemplate> rigTemplateEntitiesList = rigTemplateEntities.OrderByDescending(at => at.CreatedUtc.Value).Skip(page * pageSize).Take(pageSize).ToList();

            IEnumerable<RigTemplateItemViewModel> rigTemplateList =
                rigTemplateEntitiesList.Select(rt => new RigTemplateItemViewModel
                {
                    RigName = rt.Name_Number,
                    RigId = rt.Id
                });

            return rigTemplateList.ToPagedList(page, pageSize, rigTemplateEntities.Count());
        }

        /// <summary>
        /// Get rig template for edit dialog
        /// </summary>
        /// <param name="rigId"></param>
        /// <returns></returns>
        public RigTemplateFormDialogViewModel GetRigForDialog(Guid rigId)
        {
            RigTemplate tigEntity = _repo.Repository<RigTemplate>().Get(rigId);

            RigTemplateFormDialogViewModel model = new RigTemplateFormDialogViewModel
            {
                Id = tigEntity.Id,
                Name = tigEntity.Name_Number
            };

            return model;
        }

        /// <summary>
        /// Checking is exist rig template with same name
        /// </summary>
        /// <param name="rigName">Rig name</param>
        /// <returns></returns>
        public bool IsRigExist(string rigName)
        {
            return _repo.Repository<RigTemplate>().GetAll().Where(r => r.Name_Number == rigName).Any();
        }

        /// <summary>
        /// Checking is exist rig template with same name
        /// </summary>
        /// <param name="rigName">Rig name</param>
        /// <param name="excludeId">Exclude rig Id</param>
        /// <returns></returns>
        public bool IsRigExist(string rigName, Guid excludeId)
        {
            return _repo.Repository<RigTemplate>().GetAll().Where(r => r.Name_Number == rigName && r.Id != excludeId).Any();
        }

        /// <summary>
        /// Add new rig template entity
        /// </summary>
        /// <param name="model"></param>
        public void AddRigTemplate(RigTemplateFormDialogViewModel model)
        {
            IndodrillReportsModule reportModule = _repo.Repository<IndodrillReportsModule>().GetAll().FirstOrDefault();

            RigTemplate rigTemplateEntity = new RigTemplate
            {
                Id = Guid.NewGuid(),
                Name_Number = model.Name,
                ModuleId = reportModule.Id
            };

            _repo.Repository<RigTemplate>().Add(rigTemplateEntity);
            _repo.SaveChanges();
        }

        /// <summary>
        /// Update rig template entity
        /// </summary>
        /// <param name="model"></param>
        public void UpdateRig(RigTemplateFormDialogViewModel model)
        {
            RigTemplate rigTemplateEntity = _repo.Repository<RigTemplate>().Get(model.Id);

            rigTemplateEntity.Name_Number = model.Name;

            _repo.Repository<RigTemplate>().Update(rigTemplateEntity);
            _repo.SaveChanges();
        }

        /// <summary>
        /// Remove rig template entity by rigId
        /// </summary>
        /// <param name="rigId">Guid Rig Id</param>
        public void RemoveRig(Guid rigId)
        {
            _repo.Repository<RigTemplate>().Delete(rigId);
            _repo.SaveChanges();
        }
    }
}