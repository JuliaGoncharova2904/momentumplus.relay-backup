﻿using IndodrillReport.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using IndodrillReport.BLL.ViewModels;
using IndodrillReport.DAL.Entities;
using MvcPaging;
using System.Web.Mvc;

namespace IndodrillReport.BLL.Services
{
    public class ClientTemplateService
    {
        private GenericUnitOfWork _repo;

        public ClientTemplateService(GenericUnitOfWork repo)
        {
            this._repo = repo;
        }

        /// <summary>
        /// Get all clients for master list section
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public IPagedList<ClientItemViewModel> GetClientTemplateList(int page, int pageSize)
        {
            IQueryable<Client> clientTemplateEntities = _repo.Repository<Client>().GetAll();
            IEnumerable<Client> clinetTemplateEntitiesList = clientTemplateEntities.OrderBy(at => at.Name).Skip(page * pageSize).Take(pageSize).ToList();

            IEnumerable<ClientItemViewModel> clientTemplateList =
                clinetTemplateEntitiesList.Select(at => new ClientItemViewModel
                {
                    ClientName = at.Name,
                    ClientId = at.Id
                });

            return clientTemplateList.ToPagedList(page, pageSize, clientTemplateEntities.Count());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Guid AddClientTemplate(ClientFormDialogViewModel model)
        {
            IndodrillReportsModule reportModule = _repo.Repository<IndodrillReportsModule>().GetAll().FirstOrDefault();

            Client clientTemplateEntity = new Client
            {
                Id = Guid.NewGuid(),
                Name = model.Name,
                ModuleId = reportModule.Id
            };

            _repo.Repository<Client>().Add(clientTemplateEntity);
            _repo.SaveChanges();

            return clientTemplateEntity.Id;
        }

        /// <summary>
        /// Add location for client
        /// </summary>
        /// <param name="model"></param>
        public void AddLocation(LocationViewModel model)
        {
            Location locationEntity = new Location
            {
                Name = model.LocationName,
                Id = Guid.NewGuid(),
                ClientId = model.ClientId
            };

            _repo.Repository<Location>().Add(locationEntity);
            _repo.SaveChanges();
        }

        /// <summary>
        /// Checking is exist client with same name
        /// </summary>
        /// <param name="clientName">Client name</param>
        /// <returns></returns>
        public bool IsClientExist(string clientName)
        {
            return _repo.Repository<Client>().GetAll().Where(at => at.Name == clientName).Any();
        }

        /// <summary>
        /// Checking is exist client with same name exclide Id
        /// </summary>
        /// <param name="clientName">Client name</param>
        /// <param name="excludeId">Exclude Id</param>
        /// <returns></returns>
        public bool IsClientExist(string clientName, Guid excludeId)
        {
            return _repo.Repository<Client>().GetAll().Where(c => c.Name == clientName && c.Id != excludeId).Any();
        }

        /// <summary>
        /// Checking is exist location for client with same name
        /// </summary>
        /// <param name="clientId"></param>
        /// /// <param name="locationName"></param>
        /// <returns></returns>
        public bool IsClientLocationExist(Guid clientId, string locationName)
        {
            return _repo.Repository<Location>().GetAll().Where(l => l.ClientId == clientId && l.Name == locationName).Any();
        }

        /// <summary>
        /// Checking is exist location for client with same name exclide Id
        /// </summary>
        /// <param name="clientId">Client Id</param>
        /// <param name="locationName">Location name</param>
        /// <param name="excludeId">Exclue Id</param>
        /// <returns></returns>
        public bool IsClientLocationExist(Guid clientId, string locationName, Guid excludeId)
        {
            return _repo.Repository<Location>().GetAll().Where(l => l.ClientId == clientId && l.Name == locationName && l.Id != excludeId).Any();
        }

        /// <summary>
        /// Get client entity for edit dialog
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public ClientFormDialogViewModel GetClientForDialog(Guid clientId)
        {
            Client clientEntity = _repo.Repository<Client>().Get(clientId);

            ClientFormDialogViewModel model = new ClientFormDialogViewModel
            {
                Name = clientEntity.Name,
                ClientId = clientEntity.Id,
                Locations = clientEntity.Locations.OrderBy(l => l.Name).Select(l => new SelectListItem { Text = l.Name, Value = l.Id.ToString() })
            };

            return model;
        }

        /// <summary>
        /// Return client locations list
        /// </summary>
        /// <param name="clientId">Client Id</param>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetClientLocations(Guid clientId)
        {
            Client clientEntity = _repo.Repository<Client>().Get(clientId);

            return clientEntity.Locations.OrderBy(l => l.Name).Select(l => new SelectListItem { Text = l.Name, Value = l.Id.ToString() });
        }

        /// <summary>
        /// Update client entity
        /// </summary>
        /// <param name="model"></param>
        public void UpdateClient(ClientFormDialogViewModel model)
        {
            Client clientEntity = _repo.Repository<Client>().Get(model.ClientId.Value);
            clientEntity.Name = model.Name;
            _repo.Repository<Client>().Update(clientEntity);
            _repo.SaveChanges();
        }

        /// <summary>
        /// Return View model of Location
        /// </summary>
        /// <param name="locationId">Location Id</param>
        /// <returns></returns>
        public LocationViewModel GetLocation(Guid locationId)
        {
            Location location = _repo.Repository<Location>().Get(locationId);

            return new LocationViewModel
            {
                ClientId = location.ClientId,
                LocationId = location.Id,
                LocationName = location.Name
            };
        }

        /// <summary>
        /// Update location entity
        /// </summary>
        /// <param name="model"></param>
        public void UpdateLocation(LocationViewModel model)
        {
            Location locationEntity = _repo.Repository<Location>().Get(model.LocationId);
            locationEntity.Name = model.LocationName;
            _repo.Repository<Location>().Update(locationEntity);
            _repo.SaveChanges();
        }

        /// <summary>
        /// Remove location by Id
        /// </summary>
        /// <param name="locationId">Location Id</param>
        public void RemoveLocation(Guid locationId)
        {
            _repo.Repository<Location>().Delete(locationId);
            _repo.SaveChanges();
        }

        /// <summary>
        /// Remove client entity by clientId
        /// </summary>
        /// <param name="clientId">Guid Client Id</param>
        public void RemoveClient(Guid clientId)
        {
            _repo.Repository<Client>().Delete(clientId);
            _repo.SaveChanges();
        }
    }
}