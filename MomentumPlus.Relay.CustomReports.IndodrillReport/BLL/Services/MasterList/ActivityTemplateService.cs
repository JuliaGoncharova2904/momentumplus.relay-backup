﻿using IndodrillReport.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IndodrillReport.BLL.ViewModels;
using IndodrillReport.DAL.Entities;
using MvcPaging;

namespace IndodrillReport.BLL.Services
{
    public class ActivityTemplateService
    {
        private GenericUnitOfWork _repo;

        public ActivityTemplateService(GenericUnitOfWork repo)
        {
            this._repo = repo;
        }

        /// <summary>
        /// Get activity template for dialog
        /// </summary>
        /// <param name="activityId"></param>
        /// <returns></returns>
        public ActivityFormDialogViewModel GetActivityForDialog(Guid activityId)
        {
            ActivityTemplate activityEntity = _repo.Repository<ActivityTemplate>().Get(activityId);

            ActivityFormDialogViewModel model = new ActivityFormDialogViewModel
            {
                Id = activityEntity.Id,
                Name = activityEntity.Name,
                CostCode = activityEntity.CostCode
            };

            return model;
        }


        /// <summary>
        /// Get all activity template for master list section
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public IPagedList<ActivityItemViewModel> GetActivityTemplateList(int page, int pageSize)
        {
            IQueryable<ActivityTemplate> activityTemplateEntities = _repo.Repository<ActivityTemplate>().GetAll();
            IEnumerable<ActivityTemplate> activityTemplateEntitiesList = activityTemplateEntities.OrderByDescending(at => at.CreatedUtc.Value).Skip(page * pageSize).Take(pageSize).ToList();

            IEnumerable<ActivityItemViewModel> activityTemplateList =
                activityTemplateEntitiesList.Select(at => new ActivityItemViewModel
                {
                    ActivityName = at.Name,
                    ActivityId = at.Id
                });

            return activityTemplateList.ToPagedList(page, pageSize, activityTemplateEntities.Count());
        }

        /// <summary>
        /// Add new activity template entity
        /// </summary>
        /// <param name="model"></param>
        public void AddActivityTemplate(ActivityFormDialogViewModel model)
        {
            IndodrillReportsModule reportModule = _repo.Repository<IndodrillReportsModule>().GetAll().FirstOrDefault();

            ActivityTemplate activityTemplateEntity = new ActivityTemplate
            {
                Id = Guid.NewGuid(),
                Name = model.Name,
                CostCode = model.CostCode,
                ModuleId = reportModule.Id
            };

            _repo.Repository<ActivityTemplate>().Add(activityTemplateEntity);
            _repo.SaveChanges();
        }

        /// <summary>
        /// Checking is exist activity template with same name
        /// </summary>
        /// <param name="activityName">Activity name</param>
        /// <returns></returns>
        public bool IsActivityExist(string activityName)
        {
            return _repo.Repository<ActivityTemplate>().GetAll().Where(at => at.Name == activityName).Any();
        }

        /// <summary>
        /// Checking is exist activity template with same name
        /// </summary>
        /// <param name="activityName">Activity name</param>
        /// <param name="excludeId">Exclude activity Id</param>
        /// <returns></returns>
        public bool IsActivityExist(string activityName, Guid excludeId)
        {
            return _repo.Repository<ActivityTemplate>().GetAll().Where(at => at.Name == activityName && at.Id != excludeId).Any();
        }

        /// <summary>
        /// Update activity template entity
        /// </summary>
        /// <param name="model"></param>
        public void UpdateActivity(ActivityFormDialogViewModel model)
        {
            ActivityTemplate activityTemplateEntity = _repo.Repository<ActivityTemplate>().Get(model.Id);

            activityTemplateEntity.CostCode = model.CostCode;
            activityTemplateEntity.Name = model.Name;

            _repo.Repository<ActivityTemplate>().Update(activityTemplateEntity);
            _repo.SaveChanges();
        }

        /// <summary>
        /// Remove activity template entity by activityId
        /// </summary>
        /// <param name="activityId">Guid activity id</param>
        public void RemoveActivity(Guid activityId)
        {
            _repo.Repository<ActivityTemplate>().Delete(activityId);
            _repo.SaveChanges();
        }
    }
}