﻿using System;
using System.Collections.Generic;
using System.Linq;
using IndodrillReport.BLL.ViewModels;
using IndodrillReport.DAL;
using IndodrillReport.DAL.Entities;
using IndodrillReport.BLL.Logic;
using System.Web.Mvc;
using System.Data.Entity;

namespace IndodrillReport.BLL.Services
{
    public class ReportService
    {
        private GenericUnitOfWork _repo;
        private RelayAccess _relayAccess;

        public ReportService(GenericUnitOfWork repo, RelayAccess relayAccess)
        {
            this._repo = repo;
            this._relayAccess = relayAccess;
        }

        #region Reports methods

        /// <summary>
        /// Get drilling report by reportId
        /// </summary>
        /// <param name="reportId"></param>
        /// <returns>DrillingReportViewModel</returns>
        public DrillingReportViewModel GetReport(Guid reportId)
        {
            new CompanyInfoLogic(_repo, _relayAccess).UpdateLogo();

            DrillingReportViewModel model = ReportFillProcessor.CreateReport(_repo, _relayAccess, reportId);

            return model;
        }
        
        /// <summary>
        /// Return locations list for client
        /// </summary>
        /// <param name="clientId">Client Id</param>
        /// <returns></returns>
        public IEnumerable<SelectListItem> LocationsForClient(Guid clientId)
        {
            IEnumerable<SelectListItem> locations = _repo.Repository<Location>()
                                                        .GetAll()
                                                        .Where(l => l.ClientId == clientId)
                                                        .OrderBy(l => l.Name)
                                                        .Select(l => new SelectListItem { Text = l.Name, Value = l.Id.ToString() })
                                                        .ToList();

            return locations;
        }  

        /// <summary>
        /// Create empty drilling report for user by userId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Guid AddReport(Guid userId, Guid? shiftId = null)
        {
            new CompanyInfoLogic(_repo, _relayAccess).UpdateLogo();

            DailyDrillingReport drillingEntity = new DailyDrillingReport
            {
                Id = Guid.NewGuid(),
                Date = DateTime.Today,
                ShiftId = shiftId,
                CreatorId = userId,
                CreatorName = _relayAccess.GetUserName(userId),
                ModuleId = _repo.Repository<IndodrillReportsModule>().GetAll().Select(m => m.Id).First(),
                Approvals = new HashSet<Approval>()
            };

            _repo.Repository<DailyDrillingReport>().Add(drillingEntity);
            _repo.SaveChanges();

            //----- Add approvals -------
            Approval rigSupervisor = new Approval { Id = Guid.NewGuid(), Name = "Rig Supervisor" };
            Approval contractSupervisor = new Approval { Id = Guid.NewGuid(), Name = "Contract Supervisor" };
            Approval client = new Approval { Id = Guid.NewGuid(), Name = "Client" };

            _repo.Repository<Approval>().Add(rigSupervisor);
            drillingEntity.Approvals.Add(rigSupervisor);
            _repo.SaveChanges();

            _repo.Repository<Approval>().Add(contractSupervisor);
            drillingEntity.Approvals.Add(contractSupervisor);
            _repo.SaveChanges();

            _repo.Repository<Approval>().Add(client);
            drillingEntity.Approvals.Add(client);
            _repo.SaveChanges();
            //--------------------------

            return drillingEntity.Id;
        }

        /// <summary>
        /// Remove report entity by reportId
        /// </summary>
        /// <param name="reportId"></param>
        public void RemoveReport(Guid reportId)
        {
            DailyDrillingReport report = _repo.Repository<DailyDrillingReport>().Get(reportId);

            report.AssertEditability();

            _repo.Repository<DailyDrillingReport>().Delete(reportId);
            _repo.SaveChanges();
        }

        /// <summary>
        /// Update report from View model.
        /// </summary>
        /// <param name="model"></param>
        public void UpdateReportField(ReportFieldsViewModel model)
        {
            DailyDrillingReport drillingReportEntity = _repo.Repository<DailyDrillingReport>().Get(model.ReportId);

            drillingReportEntity.AssertEditability();

            drillingReportEntity.ClientId = model.ClientId;
            drillingReportEntity.ClientName = model.ClientId.HasValue ? _repo.Repository<Client>().Get(model.ClientId.Value).Name : string.Empty;

            drillingReportEntity.LocationId = model.LocationId;
            drillingReportEntity.Location = model.LocationId.HasValue ? _repo.Repository<Location>().Get(model.LocationId.Value).Name : string.Empty;

            drillingReportEntity.DrillerId = model.DrillerId;
            drillingReportEntity.DrillerName = model.DrillerId.HasValue ? _relayAccess.GetUserName(model.DrillerId.Value) : string.Empty;

            drillingReportEntity.DrillRigId = model.DrillRigId;
            drillingReportEntity.DrillRig = model.DrillRigId.HasValue ? _repo.Repository<RigTemplate>().Get(model.DrillRigId.Value).Name_Number : string.Empty;

            drillingReportEntity.WeekToDateDS = model.WeekToDateDS;
            drillingReportEntity.Date = model.Date;
            drillingReportEntity.WeekToDateNS = model.WeekToDateNS;
            drillingReportEntity.ShiftType = model.Shift;
            drillingReportEntity.WeekToDateCombined = model.WeekToDateCombined;
            drillingReportEntity.Hole_I_D = model.Hole_I_D;
            drillingReportEntity.Angle = model.Angle;
            drillingReportEntity.DrillersAssistant = model.DrillersAssistant;
            drillingReportEntity.LastBitChange = model.LastBitChange;
            drillingReportEntity.BitMTD = model.BitMTD;
            drillingReportEntity.Helpers = model.Helpers;

            drillingReportEntity.DrillerComments = model.DrillerComment;
            drillingReportEntity.ClientComments = model.ClientComment;

            _repo.Repository<DailyDrillingReport>().Update(drillingReportEntity);
            _repo.SaveChanges();
        }

        /// <summary>
        /// Completing drilling report
        /// </summary>
        /// <param name="reportId"></param>
        public bool Complete(Guid reportId)
        {
            DailyDrillingReport drillingReportEntity = _repo.Repository<DailyDrillingReport>().Get(reportId);

            if (drillingReportEntity.Validate(_repo))
            {
                drillingReportEntity.AssertEditability();

                using (var scope = _repo.BeginTransaction())
                {
                    try
                    {
                        IndodrillReportsModule module = _repo.Repository<IndodrillReportsModule>().GetAll().FirstOrDefault();

                        drillingReportEntity.ReportNumber = ++module.LastReportId;

                        _repo.Repository<IndodrillReportsModule>().Update(module);
                        _repo.SaveChanges();

                        scope.Commit();
                    }
                    catch (Exception)
                    {
                        scope.Rollback();
                        throw;
                    }
                }

                new CompanyInfoLogic(_repo, _relayAccess).UpdateLogo();

                drillingReportEntity.CompanyInfoId = drillingReportEntity.Module.CurrentCompanyInfoId;

                drillingReportEntity.State = DailyDrillingReportState.Completed;
                _repo.Repository<DailyDrillingReport>().Update(drillingReportEntity);
                _repo.SaveChanges();

                return true;
            }

            return false;
        }

        /// <summary>
        /// Get or create report for shift.
        /// </summary>
        /// <param name="shiftId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public DrillingReportViewModel GetCreateReportForShift(Guid shiftId, Guid userId)
        {
            DailyDrillingReport report = _repo.Repository<DailyDrillingReport>().GetAll()
                                                .Where(dr => dr.ShiftId == shiftId).FirstOrDefault();

            Guid reportId = (report != null) ? report.Id : this.AddReport(userId, shiftId);

            return this.GetReport(reportId);
        }

        #endregion

        #region Activities methods

        /// <summary>
        /// Return cost code for activity
        /// </summary>
        /// <param name="activityId">Activity Id</param>
        /// <returns></returns>
        public string ActivityCostCode(Guid activityId)
        {
            ActivityTemplate activity = _repo.Repository<ActivityTemplate>().Get(activityId);

            return activity.CostCode;
        }

        /// <summary>
        /// Add new activity row for report by reportId
        /// </summary>
        /// <param name="reportId"></param>
        /// <returns></returns>
        public ReportActivityViewModel AddActivityItem(Guid reportId)
        {
            DailyDrillingReport drillingEntity = _repo.Repository<DailyDrillingReport>().Get(reportId);

            drillingEntity.AssertEditability();

            Activity activity = new Activity { Id = Guid.NewGuid() };

            drillingEntity.Activities.Add(activity);
            _repo.Repository<DailyDrillingReport>().Update(drillingEntity);
            _repo.SaveChanges();

            return new ReportActivityViewModel {
                Id = activity.Id,
                Templates = _repo.Repository<ActivityTemplate>()
                                                        .GetAll()
                                                        .OrderBy(at => at.Name)
                                                        .Select(at => new SelectListItem { Text = at.Name, Value = at.Id.ToString() })
                                                        .ToArray()
            };
        }

        /// <summary>
        /// Update activity entity
        /// </summary>
        /// <param name="model"></param>
        public void UpdateActivity(ReportActivityViewModel model)
        {
            Activity activity = _repo.Repository<Activity>().Get(model.Id);
            ActivityTemplate template = model.TemplateId.HasValue ? _repo.Repository<ActivityTemplate>().Get(model.TemplateId.Value) : null;

            activity.Report.AssertEditability();

            activity.TemplateActivityId = model.TemplateId;
            activity.Name = template != null ? template.Name : string.Empty;
            activity.CostCode = template != null ? template.CostCode : string.Empty;

            activity.To = model.To;
            activity.From = model.From;
            activity.Comments = model.DrillingDetails;
            activity.CoreSize = model.CoreSize;
            activity.StartTime = model.StartTime;
            activity.EndTime = model.EndTime;
            activity.Recovered = model.Recovered;

            _repo.Repository<Activity>().Update(activity);
            _repo.SaveChanges();
        }

        /// <summary>
        /// Remove activity entity by activityId
        /// </summary>
        /// <param name="activityId"></param>
        public void RemoveActivityItem(Guid activityId)
        {
            Activity activity = _repo.Repository<Activity>().Get(activityId);

            activity.Report.AssertEditability();

            _repo.Repository<Activity>().Delete(activity);
            _repo.SaveChanges();
        }

        #endregion

        #region Used Materials

        /// <summary>
        /// Add new used material row for report by reportId
        /// </summary>
        /// <param name="reportId"></param>
        /// <returns></returns>
        public ReportMaterialViewModel AddUsedMaterialItem(Guid reportId)
        {
            DailyDrillingReport drillingEntity = _repo.Repository<DailyDrillingReport>().Get(reportId);

            drillingEntity.AssertEditability();

            UsedMaterial material = new UsedMaterial { Id = Guid.NewGuid() };

            drillingEntity.UsedMaterials.Add(material);
            _repo.Repository<DailyDrillingReport>().Update(drillingEntity);
            _repo.SaveChanges();

            return new ReportMaterialViewModel { Id = material.Id };
        }

        /// <summary>
        /// Update used material entity
        /// </summary>
        /// <param name="model"></param>
        public void UpdateUsedMaterial(ReportMaterialViewModel model)
        {
            UsedMaterial material = _repo.Repository<UsedMaterial>().Get(model.Id);

            material.Report.AssertEditability();

            material.Name = model.Name;
            material.Quantity = model.Quantity;
            material.Units = model.Units;

            _repo.Repository<UsedMaterial>().Update(material);
            _repo.SaveChanges();
        }

        /// <summary>
        /// Remove used material by materialId
        /// </summary>
        /// <param name="materialId"></param>
        public void RemoveUsedMaterial(Guid materialId)
        {
            UsedMaterial material = _repo.Repository<UsedMaterial>().Get(materialId);

            material.Report.AssertEditability();

            _repo.Repository<UsedMaterial>().Delete(material);
            _repo.SaveChanges();
        }

        #endregion

        #region Left In Hole

        /// <summary>
        /// Add new left in hole row for report by reportId
        /// </summary>
        /// <param name="reportId"></param>
        /// <returns></returns>
        public ReportMaterialViewModel AddLeftInHoleItem(Guid reportId)
        {
            DailyDrillingReport drillingEntity = _repo.Repository<DailyDrillingReport>().Get(reportId);

            drillingEntity.AssertEditability();

            LeftInHole material = new LeftInHole { Id = Guid.NewGuid() };

            drillingEntity.LeftInHole.Add(material);
            _repo.Repository<DailyDrillingReport>().Update(drillingEntity);
            _repo.SaveChanges();

            return new ReportMaterialViewModel { Id = material.Id };
        }

        /// <summary>
        /// Update left in hole material entity
        /// </summary>
        /// <param name="model"></param>
        public void UpdateLeftInHoleMaterial(ReportMaterialViewModel model)
        {
            LeftInHole material = _repo.Repository<LeftInHole>().Get(model.Id);

            material.Report.AssertEditability();

            material.Name = model.Name;
            material.Quantity = model.Quantity;
            material.Units = model.Units;

            _repo.Repository<LeftInHole>().Update(material);
            _repo.SaveChanges();
        }

        /// <summary>
        /// Remove LeftInHole material by materialId
        /// </summary>
        /// <param name="materialId"></param>
        public void RemoveLeftInHoleMaterial(Guid materialId)
        {
            LeftInHole material = _repo.Repository<LeftInHole>().Get(materialId);

            material.Report.AssertEditability();

            _repo.Repository<LeftInHole>().Delete(material);
            _repo.SaveChanges();
        }

        #endregion

        #region Bits methods

        /// <summary>
        /// Add new bit row for report by reportId
        /// </summary>
        /// <param name="reportId"></param>
        /// <returns></returns>
        public ReportBitViewModel AddBitItem(Guid reportId)
        {
            DailyDrillingReport drillingEntity = _repo.Repository<DailyDrillingReport>().Get(reportId);

            drillingEntity.AssertEditability();

            Bit bit = new Bit { Id = Guid.NewGuid() };

            drillingEntity.Bits.Add(bit);
            _repo.Repository<DailyDrillingReport>().Update(drillingEntity);
            _repo.SaveChanges();

            return new ReportBitViewModel { Id = bit.Id };
        }

        /// <summary>
        /// Remove bit entity by bitId
        /// </summary>
        /// <param name="bitId"></param>
        public void RemoveBitItem(Guid bitId)
        {
            Bit bit = _repo.Repository<Bit>().Get(bitId);

            bit.Report.AssertEditability();

            _repo.Repository<Bit>().Delete(bit);
            _repo.SaveChanges();
        }

        /// <summary>
        /// Update bit entity
        /// </summary>
        /// <param name="model"></param>
        public void UpdateBit(ReportBitViewModel model)
        {
            Bit bit = _repo.Repository<Bit>().Get(model.Id);

            bit.Report.AssertEditability();

            bit.Type = model.Type;
            bit.SerialNo = model.SerialNumber;
            bit.Off = model.Off;
            bit.On = model.On;
            bit.Size = model.Size;

            _repo.Repository<Bit>().Update(bit);
            _repo.SaveChanges();
        }

        #endregion

        #region Approvals methods

        public void UpdateApproval(ReportApprovalViewModel model)
        {
            Approval approval = _repo.Repository<Approval>().Get(model.Id);

            approval.Report.AssertEditability();

            approval.IsApproved = model.IsApproved;
            approval.Signature = model.Signature;

            _repo.Repository<Approval>().Update(approval);
            _repo.SaveChanges();
        }

        #endregion
    }
}