﻿using System;
using System.Collections.Generic;
using System.Linq;
using IndodrillReport.BLL.Logic;
using IndodrillReport.BLL.ViewModels;
using IndodrillReport.DAL;
using IndodrillReport.DAL.Entities;
using System.Web.Mvc;

namespace IndodrillReport.BLL.Services
{
    public abstract partial class ReportFillProcessor
    {
        private class EditableReportFillProcessor : ReportFillProcessor
        {
            public EditableReportFillProcessor(GenericUnitOfWork repo, RelayAccess relayAccess) : base(repo, relayAccess)
            {
            }

            public override DrillingReportViewModel CreateReport(DailyDrillingReport drillingEntity)
            {

                DrillingReportViewModel model = new DrillingReportViewModel
                {
                    ReadOnlyMode = false,
                    ReportFields = this.FillReportFields(drillingEntity),
                    CompanyInfo = this.FillCompanyInfo(drillingEntity),
                    Activities = this.FillActivitiesTable(drillingEntity),
                    Bits = this.FillBitsTable(drillingEntity),
                    Approvals = this.FillApprovalsTable(drillingEntity),
                    UsedMaterials = this.FillUsedMaterialsTable(drillingEntity),
                    LeftMaterials = this.FillLeftInHoleTable(drillingEntity)
                };

                return model;
            }

            private ReportFieldsViewModel FillReportFields(DailyDrillingReport reportEntity)
            {

                ReportFieldsViewModel reportViewModel = new ReportFieldsViewModel
                {
                    ReportId = reportEntity.Id,
                    ReportNumber = string.Format("{0:D6}", reportEntity.ReportNumber),
                    Date = reportEntity.Date,
                    ClientId = reportEntity.ClientId,
                    Clients = _repo.Repository<Client>()
                                                    .GetAll()
                                                    .OrderBy(l => l.Name)
                                                    .Select(l => new SelectListItem { Text = l.Name, Value = l.Id.ToString() })
                                                    .ToList(),
                    LocationId = reportEntity.LocationId,
                    Locations = _repo.Repository<Location>()
                                                    .GetAll()
                                                    .Where(l => l.ClientId == reportEntity.ClientId)
                                                    .OrderBy(l => l.Name)
                                                    .Select(l => new SelectListItem { Text = l.Name, Value = l.Id.ToString() })
                                                    .ToList(),
                    Shift = reportEntity.ShiftType,
                    ShiftTypes = new[] {
                                        new SelectListItem { Text="Day", Value="Day" },
                                        new SelectListItem { Text="Night", Value="Night" },
                                        new SelectListItem { Text="Late", Value="Late" }
                                    },
                    DrillerId = reportEntity.DrillerId,
                    Drillers = _relayAccess.GetRelayUsersList(),
                    DrillRigId = reportEntity.DrillRigId,
                    DrillRigs = _repo.Repository<RigTemplate>()
                                                    .GetAll()
                                                    .OrderBy(r => r.Name_Number)
                                                    .Select(r => new SelectListItem { Text = r.Name_Number, Value = r.Id.ToString() })
                                                    .ToList(),
                    Angle = reportEntity.Angle,
                    BitMTD = reportEntity.BitMTD,
                    DrillersAssistant = reportEntity.DrillersAssistant,
                    Hole_I_D = reportEntity.Hole_I_D,
                    Helpers = reportEntity.Helpers,
                    LastBitChange = reportEntity.LastBitChange,
                    WeekToDateNS = reportEntity.WeekToDateNS,
                    WeekToDateDS = reportEntity.WeekToDateDS,
                    WeekToDateCombined = reportEntity.WeekToDateCombined,
                    DrillerComment = reportEntity.DrillerComments,
                    ClientComment = reportEntity.ClientComments
                };

                return reportViewModel;
            }

            private ReportCompanyInformation FillCompanyInfo(DailyDrillingReport reportEntity)
            {
                ReportCompanyInformation companyInfoView = new ReportCompanyInformation();
                Guid? currentCompanyInfoId = _repo.Repository<IndodrillReportsModule>().GetAll().Select(m => m.CurrentCompanyInfoId).First();

                if (currentCompanyInfoId.HasValue)
                {
                    CompanyInfo companyInfo = _repo.Repository<CompanyInfo>().Get(currentCompanyInfoId.Value);

                    companyInfoView.Name = companyInfo.Name;
                    companyInfoView.Address = companyInfo.Address;
                    companyInfoView.Phone = companyInfo.Phone;
                    companyInfoView.Email = companyInfo.Email;
                    companyInfoView.Site = companyInfo.Site;
                    companyInfoView.LogoId = companyInfo.LogoId;
                }

                return companyInfoView;
            }

            private IEnumerable<ReportActivityViewModel> FillActivitiesTable(DailyDrillingReport reportEntity)
            {
                List<Activity> activities = reportEntity.Activities.OrderBy(a => a.CreatedUtc).ToList();
                List<ReportActivityViewModel> activityTable = new List<ReportActivityViewModel>();

                foreach (Activity activity in activities)
                {
                    ActivityTemplate activityTemplate = _repo.Repository<ActivityTemplate>().GetAll()
                                                                    .Where(at => at.Id == activity.TemplateActivityId)
                                                                    .FirstOrDefault();

                    activityTable.Add(new ReportActivityViewModel
                    {
                        Id = activity.Id,
                        TemplateId = activity.TemplateActivityId,
                        Templates = _repo.Repository<ActivityTemplate>()
                                                        .GetAll()
                                                        .OrderBy(at => at.Name)
                                                        .Select(at => new SelectListItem { Text = at.Name, Value = at.Id.ToString() })
                                                        .ToArray(),
                        EndTime = activity.EndTime,
                        CoreSize = activity.CoreSize,
                        CostCode = activityTemplate?.CostCode ?? string.Empty,
                        DrillingDetails = activity.Comments,
                        From = activity.From,
                        Recovered = activity.Recovered,
                        StartTime = activity.StartTime,
                        To = activity.To
                    });
                }

                return activityTable;
            }

            private IEnumerable<ReportBitViewModel> FillBitsTable(DailyDrillingReport reportEntity)
            {
                IEnumerable<ReportBitViewModel> bitsTable = reportEntity.Bits
                                                                        .OrderBy(b => b.CreatedUtc)
                                                                        .Select(b => new ReportBitViewModel
                {
                    Id = b.Id,
                    Type = b.Type,
                    SerialNumber = b.SerialNo,
                    Size = b.Size,
                    Off = b.Off,
                    On = b.On
                });

                return bitsTable;
            }

            private IEnumerable<ReportApprovalViewModel> FillApprovalsTable(DailyDrillingReport reportEntity)
            {
                IEnumerable<ReportApprovalViewModel> approvalsTable = reportEntity.Approvals
                                                                                .OrderBy(ap => ap.CreatedUtc)
                                                                                .Select(ap => new ReportApprovalViewModel
                {
                    Id = ap.Id,
                    Name = ap.Name,
                    IsApproved = ap.IsApproved,
                    Signature = ap.Signature
                });

                return approvalsTable;
            }

            private IEnumerable<ReportMaterialViewModel> FillUsedMaterialsTable(DailyDrillingReport reportEntity)
            {
                IEnumerable<ReportMaterialViewModel> usedMaterialsTable = reportEntity.UsedMaterials
                                                                                    .OrderBy(um => um.CreatedUtc)
                                                                                    .Select(m => new ReportMaterialViewModel
                {
                    Id = m.Id,
                    Name = m.Name,
                    Quantity = m.Quantity,
                    Units = m.Units
                });

                return usedMaterialsTable;
            }

            private IEnumerable<ReportMaterialViewModel> FillLeftInHoleTable(DailyDrillingReport reportEntity)
            {
                IEnumerable<ReportMaterialViewModel> leftInHoleTable = reportEntity.LeftInHole
                                                                                .OrderBy(lh => lh.CreatedUtc)
                                                                                .Select(m => new ReportMaterialViewModel
                {
                    Id = m.Id,
                    Name = m.Name,
                    Quantity = m.Quantity,
                    Units = m.Units
                });

                return leftInHoleTable;
            }
        }
    }
}