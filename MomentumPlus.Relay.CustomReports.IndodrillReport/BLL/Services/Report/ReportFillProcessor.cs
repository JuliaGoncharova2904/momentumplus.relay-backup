﻿using IndodrillReport.BLL.Logic;
using IndodrillReport.BLL.ViewModels;
using IndodrillReport.DAL;
using IndodrillReport.DAL.Entities;
using System;

namespace IndodrillReport.BLL.Services
{
    public abstract partial class ReportFillProcessor
    {
        protected GenericUnitOfWork _repo;
        protected RelayAccess _relayAccess;

        public static DrillingReportViewModel CreateReport(GenericUnitOfWork repo, RelayAccess relayAccess, Guid reportId)
        {
            DailyDrillingReport drillingEntity = repo.Repository<DailyDrillingReport>().Get(reportId);

            ReportFillProcessor reportFillProcessor = drillingEntity.IsEditable()
                                                        ? new EditableReportFillProcessor(repo, relayAccess) as ReportFillProcessor
                                                        : new ReadOnlyReportFillProcessor(repo, relayAccess) as ReportFillProcessor;

            return reportFillProcessor.CreateReport(drillingEntity);
        }

        public ReportFillProcessor(GenericUnitOfWork repo, RelayAccess relayAccess)
        {
            this._repo = repo;
            this._relayAccess = relayAccess;
        }

        public abstract DrillingReportViewModel CreateReport(DailyDrillingReport drillingEntity);
    }
}