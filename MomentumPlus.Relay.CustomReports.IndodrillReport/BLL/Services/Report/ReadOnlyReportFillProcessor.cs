﻿using System.Collections.Generic;
using System.Linq;
using IndodrillReport.BLL.Logic;
using IndodrillReport.BLL.ViewModels;
using IndodrillReport.DAL;
using IndodrillReport.DAL.Entities;
using System.Web.Mvc;

namespace IndodrillReport.BLL.Services
{
    public abstract partial class ReportFillProcessor
    {
        private class ReadOnlyReportFillProcessor : ReportFillProcessor
        {
            public ReadOnlyReportFillProcessor(GenericUnitOfWork repo, RelayAccess relayAccess) : base(repo, relayAccess)
            {
            }

            public override DrillingReportViewModel CreateReport(DailyDrillingReport drillingEntity)
            {
                DrillingReportViewModel model = new DrillingReportViewModel
                {
                    ReadOnlyMode = true,
                    ReportFields = this.FillReportFields(drillingEntity),
                    CompanyInfo = this.FillCompanyInfo(drillingEntity),
                    Activities = this.FillActivitiesTable(drillingEntity),
                    Bits = this.FillBitsTable(drillingEntity),
                    Approvals = this.FillApprovalsTable(drillingEntity),
                    UsedMaterials = this.FillUsedMaterialsTable(drillingEntity),
                    LeftMaterials = this.FillLeftInHoleTable(drillingEntity)
                };

                return model;
            }

            private ReportFieldsViewModel FillReportFields(DailyDrillingReport reportEntity)
            {

                ReportFieldsViewModel reportViewModel = new ReportFieldsViewModel
                {
                    ReportId = reportEntity.Id,
                    ReportNumber = string.Format("{0:D6}", reportEntity.ReportNumber),
                    Date = reportEntity.Date,
                    ClientId = reportEntity.ClientId,
                    Clients = new[] { new SelectListItem { Text = reportEntity.ClientName, Value = reportEntity.ClientId.ToString() } },
                    LocationId = reportEntity.LocationId,
                    Locations = new[] { new SelectListItem { Text = reportEntity.Location, Value = reportEntity.LocationId.ToString() } },
                    Shift = reportEntity.ShiftType,
                    ShiftTypes = new[] { new SelectListItem { Text = reportEntity.ShiftType, Value = reportEntity.ShiftType } },
                    DrillerId = reportEntity.DrillerId,
                    Drillers = new[] { new SelectListItem { Text = reportEntity.DrillerName, Value = reportEntity.DrillerId.ToString() } },
                    DrillRigId = reportEntity.DrillRigId,
                    DrillRigs = new[] { new SelectListItem { Text = reportEntity.DrillRig, Value = reportEntity.DrillRigId.ToString() } },
                    Angle = reportEntity.Angle,
                    BitMTD = reportEntity.BitMTD,
                    DrillersAssistant = reportEntity.DrillersAssistant,
                    Hole_I_D = reportEntity.Hole_I_D,
                    Helpers = reportEntity.Helpers,
                    LastBitChange = reportEntity.LastBitChange,
                    WeekToDateNS = reportEntity.WeekToDateNS,
                    WeekToDateDS = reportEntity.WeekToDateDS,
                    WeekToDateCombined = reportEntity.WeekToDateCombined,
                    DrillerComment = reportEntity.DrillerComments,
                    ClientComment = reportEntity.ClientComments
                };

                return reportViewModel;
            }

            private ReportCompanyInformation FillCompanyInfo(DailyDrillingReport reportEntity)
            {
                CompanyInfo companyInfo = reportEntity.CompanyInfo;

                ReportCompanyInformation companyInfoView = new ReportCompanyInformation
                {
                    Name = companyInfo.Name,
                    Address = companyInfo.Address,
                    Phone = companyInfo.Phone,
                    Email = companyInfo.Email,
                    Site = companyInfo.Site,
                    LogoId = companyInfo.LogoId
                };

                return companyInfoView;
            }

            private IEnumerable<ReportActivityViewModel> FillActivitiesTable(DailyDrillingReport reportEntity)
            {
                List<Activity> activities = reportEntity.Activities.OrderBy(a => a.CreatedUtc).ToList();
                List<ReportActivityViewModel> activityTable = new List<ReportActivityViewModel>();

                foreach (Activity activity in activities)
                {
                    activityTable.Add(new ReportActivityViewModel
                    {
                        Id = activity.Id,
                        TemplateId = activity.TemplateActivityId,
                        Templates = new[] { new SelectListItem { Text = activity.Name, Value = activity.TemplateActivityId.ToString() } },
                        EndTime = activity.EndTime,
                        CoreSize = activity.CoreSize,
                        CostCode = activity.CostCode,
                        DrillingDetails = activity.Comments,
                        From = activity.From,
                        Recovered = activity.Recovered,
                        StartTime = activity.StartTime,
                        To = activity.To
                    });
                }

                return activityTable;
            }

            private IEnumerable<ReportBitViewModel> FillBitsTable(DailyDrillingReport reportEntity)
            {
                IEnumerable<ReportBitViewModel> bitsTable = reportEntity.Bits
                                                                        .OrderBy(a => a.CreatedUtc)
                                                                        .Select(b => new ReportBitViewModel
                {
                    Id = b.Id,
                    Type = b.Type,
                    SerialNumber = b.SerialNo,
                    Size = b.Size,
                    Off = b.Off,
                    On = b.On
                });

                return bitsTable;
            }

            private IEnumerable<ReportApprovalViewModel> FillApprovalsTable(DailyDrillingReport reportEntity)
            {
                IEnumerable<ReportApprovalViewModel> approvalsTable = reportEntity.Approvals
                                                                                .OrderBy(a => a.CreatedUtc)
                                                                                .Select(ap => new ReportApprovalViewModel
                {
                    Id = ap.Id,
                    Name = ap.Name,
                    IsApproved = ap.IsApproved,
                    Signature = ap.Signature
                });

                return approvalsTable;
            }

            private IEnumerable<ReportMaterialViewModel> FillUsedMaterialsTable(DailyDrillingReport reportEntity)
            {
                IEnumerable<ReportMaterialViewModel> usedMaterialsTable = reportEntity.UsedMaterials
                                                                                    .OrderBy(a => a.CreatedUtc)
                                                                                    .Select(m => new ReportMaterialViewModel
                {
                    Id = m.Id,
                    Name = m.Name,
                    Quantity = m.Quantity,
                    Units = m.Units
                });

                return usedMaterialsTable;
            }

            private IEnumerable<ReportMaterialViewModel> FillLeftInHoleTable(DailyDrillingReport reportEntity)
            {
                IEnumerable<ReportMaterialViewModel> leftInHoleTable = reportEntity.LeftInHole
                                                                                .OrderBy(a => a.CreatedUtc)
                                                                                .Select(m => new ReportMaterialViewModel
                {
                    Id = m.Id,
                    Name = m.Name,
                    Quantity = m.Quantity,
                    Units = m.Units
                });

                return leftInHoleTable;
            }

        }
    }
}