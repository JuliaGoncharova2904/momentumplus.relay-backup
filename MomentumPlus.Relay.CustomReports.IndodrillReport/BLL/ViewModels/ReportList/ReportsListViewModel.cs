﻿using MvcPaging;

namespace IndodrillReport.BLL.ViewModels
{
    public class ReportsListViewModel
    {
        public ReportFilterViewModel Filters { get; set; }
        public IPagedList<ReportItemViewModel> ReportsItems { get; set; }
    }
}