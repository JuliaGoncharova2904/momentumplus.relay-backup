﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace IndodrillReport.BLL.ViewModels
{
    public class ReportFilterViewModel
    {
        public string Location { get; set; }
        public List<SelectListItem> Locations { get; set; }
        public string Client { get; set; }
        public List<SelectListItem> Clients { get; set; }
        public Guid? Driller { get; set; }
        public List<SelectListItem> Drillers { get; set; }
        public string DrillRig { get; set; }
        public List<SelectListItem> DrillRigs { get; set; }
    }
}