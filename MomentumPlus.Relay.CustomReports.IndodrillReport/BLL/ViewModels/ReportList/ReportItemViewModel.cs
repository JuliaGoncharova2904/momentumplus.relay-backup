﻿using System;

namespace IndodrillReport.BLL.ViewModels
{
    public class ReportItemViewModel
    {
        public Guid Id { get; set; }
        public string CreatedDate { get; set; }
        public string DrillerName { get; set; }
        public Guid? DrillerId { get; set; }
        public string CreatorName { get; set; }
        public Guid CreatorId { get; set; }
        public string DrillRig { get; set; }
        public string ClientName { get; set; }
        public string Location { get; set; }
        public bool IsComplete { get; set; }
    }
}