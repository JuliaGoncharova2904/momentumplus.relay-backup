﻿using System;

namespace IndodrillReport.BLL.ViewModels
{
    public class ClientItemViewModel
    {
        public Guid ClientId { get; set; }

        public  string ClientName { get; set; }
    }
}