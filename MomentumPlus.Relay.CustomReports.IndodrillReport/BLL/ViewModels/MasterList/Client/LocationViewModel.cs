﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IndodrillReport.BLL.ViewModels
{
    public class LocationViewModel
    {
        public Guid ClientId { get; set; }

        public Guid LocationId { get; set; }

        [Display(Name = "Location")]
        [Required(ErrorMessage = "This field must not be empty")]
        public  string LocationName { get; set; }
    }
}