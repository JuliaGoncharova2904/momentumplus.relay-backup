﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace IndodrillReport.BLL.ViewModels
{
    public class ClientFormDialogViewModel
    {
        public Guid? ClientId { get; set; }

        [Display(Name= "Client Name")]
        [Required(ErrorMessage = "This field must not be empty")]
        public string Name { get; set; }

        [Display(Name = "Locations")]
        public IEnumerable<SelectListItem> Locations { get; set; }
    }
}