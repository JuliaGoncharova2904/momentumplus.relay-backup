﻿using System;

namespace IndodrillReport.BLL.ViewModels
{
    public class RigTemplateItemViewModel
    {
        public Guid RigId { get; set; }

        public string RigName { get; set; }
    }
}