﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IndodrillReport.BLL.ViewModels
{
    public class RigTemplateFormDialogViewModel
    {
        public Guid Id { get; set; }

        [Display(Name= "Rig Name/Number")]
        [Required(ErrorMessage = "This field must not be empty")]
        public string Name { get; set; }
    }
}