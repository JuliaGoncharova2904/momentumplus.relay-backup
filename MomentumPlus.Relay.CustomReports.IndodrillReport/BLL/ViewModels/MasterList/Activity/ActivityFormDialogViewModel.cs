﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IndodrillReport.BLL.ViewModels
{
    public class ActivityFormDialogViewModel
    {
        public Guid Id { get; set; }

        [Display(Name= "Activity Name")]
        [Required(ErrorMessage = "This field must not be empty")]
        public string Name { get; set; }

        [Display(Name = "Cost Code")]
        public string CostCode { get; set; }
    }
}