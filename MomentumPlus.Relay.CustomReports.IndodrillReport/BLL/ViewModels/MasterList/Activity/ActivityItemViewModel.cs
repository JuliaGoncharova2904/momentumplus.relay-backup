﻿using System;

namespace IndodrillReport.BLL.ViewModels
{
    public class ActivityItemViewModel
    {
        public Guid ActivityId { get; set; }

        public string ActivityName { get; set; }
    }
}