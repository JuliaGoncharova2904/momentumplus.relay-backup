﻿namespace IndodrillReport.BLL.ViewModels
{
    public class LogoViewModel
    {
        public byte[] BinaryData { get; set; }
        public string ContentType { get; set; }
    }
}