﻿namespace IndodrillReport.BLL.ViewModels
{
    public enum IndodrillReportFilterEnum
    {
        NoFilter = 0,
        DrillerFilter = 1,
        CreatedDateFilter = 2,
        DrillRigFilter = 3,
        ClientFilter = 4,
        LocationFilter = 5
    }
}