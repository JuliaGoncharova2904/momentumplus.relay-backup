﻿using System;

namespace IndodrillReport.BLL.ViewModels
{
    public class ReportBitViewModel
    {
        public Guid Id { get; set; }

        public string Type { get; set; }

        public string Size { get; set; }

        public string SerialNumber { get; set; }

        public float? On { get; set; }

        public  float? Off { get; set; }
    }
}