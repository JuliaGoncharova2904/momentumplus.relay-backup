﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace IndodrillReport.BLL.ViewModels
{
    public class ReportActivityViewModel
    {
        public  Guid Id { get; set; }
        public Guid? TemplateId { get; set; }
        public IEnumerable<SelectListItem> Templates { get; set; }
        public string CostCode { get; set; }
        public int? StartTime { get; set; }
        public int? EndTime { get; set; }
        public float? From { get; set; }
        public float? To { get; set; }
        public float? Recovered { get; set; }
        public string CoreSize { get; set; }
        public string DrillingDetails { get; set; }
    }
}