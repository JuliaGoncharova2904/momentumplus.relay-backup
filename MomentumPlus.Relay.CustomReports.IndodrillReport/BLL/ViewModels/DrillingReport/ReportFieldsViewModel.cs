﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace IndodrillReport.BLL.ViewModels
{
    public class ReportFieldsViewModel
    {
        public Guid ReportId { get; set; }

        public string ReportNumber { get; set; }

        [Display(Name = "Client")]
        public Guid? ClientId { get; set; }
        public IEnumerable<SelectListItem> Clients { get; set; }

        [Display(Name = "Location")]
        public Guid? LocationId { get; set; }
        public IEnumerable<SelectListItem> Locations { get; set; }

        [Display(Name = "Shift")]
        public string Shift { get; set; }
        public IEnumerable<SelectListItem> ShiftTypes { get; set; }

        [Display(Name = "Week to Date DS (m)")]
        public int? WeekToDateDS { get; set; }

        [Display(Name = "Week to Date NS (m)")]
        public int? WeekToDateNS { get; set; }

        [Display(Name = "Week to Date Combined (m)")]
        public int? WeekToDateCombined { get; set; }

        [Display(Name = "Date")]
        public DateTime Date { get; set; }

        [Display(Name = "Driller")]
        public Guid? DrillerId { get; set; }
        public IEnumerable<SelectListItem> Drillers { get; set; }

        [Display(Name = "Drill Rig")]
        public Guid? DrillRigId { get; set; }
        public IEnumerable<SelectListItem> DrillRigs { get; set; }

        [Display(Name = "Hole I.D.")]
        public string Hole_I_D { get; set; }

        [Display(Name = "Angle")]
        public string Angle { get; set; }

        [Display(Name = "Driller's Assistant")]
        public string DrillersAssistant { get; set; }

        [Display(Name = "Bit MTD (m)")]
        public int? BitMTD { get; set; }

        [Display(Name = "Last Bit Change (m)")]
        public int? LastBitChange { get; set; }

        [Display(Name = "Helpers")]
        public string Helpers { get; set; }

        public string DrillerComment { get; set; }

        public string ClientComment { get; set; }
    }
}