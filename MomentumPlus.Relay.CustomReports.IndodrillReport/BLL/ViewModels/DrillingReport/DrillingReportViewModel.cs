﻿using System.Collections.Generic;

namespace IndodrillReport.BLL.ViewModels
{
    public class DrillingReportViewModel
    {
        public bool ReadOnlyMode { get; set; }
        public bool ValidateFields { get; set; }
        public ReportCompanyInformation CompanyInfo { get; set; }

        public ReportFieldsViewModel ReportFields { get; set; }

        public IEnumerable<ReportActivityViewModel> Activities { get; set; }

        public IEnumerable<ReportMaterialViewModel> UsedMaterials { get; set; }

        public IEnumerable<ReportMaterialViewModel> LeftMaterials { get; set; }

        public IEnumerable<ReportBitViewModel> Bits { get; set; }

        public IEnumerable<ReportApprovalViewModel> Approvals { get; set; }
        
    }
}