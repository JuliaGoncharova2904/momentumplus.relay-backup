﻿using System;

namespace IndodrillReport.BLL.ViewModels
{
    public class PdfReportCompanyInformation
    {
        public string Name { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string Site { get; set; }

        public Guid? LogoId;
    }
}