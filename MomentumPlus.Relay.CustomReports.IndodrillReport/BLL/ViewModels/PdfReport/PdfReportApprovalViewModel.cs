﻿using System;

namespace IndodrillReport.BLL.ViewModels
{
    public class PdfReportApprovalViewModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public bool IsApproved { get; set; }

        public string Signature { get; set; }
    }
}