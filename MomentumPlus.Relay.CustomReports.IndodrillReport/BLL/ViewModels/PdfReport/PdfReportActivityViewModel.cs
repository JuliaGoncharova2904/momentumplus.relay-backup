﻿using System;

namespace IndodrillReport.BLL.ViewModels
{
    public class PdfReportActivityViewModel
    {
        public  Guid Id { get; set; }
        public string TemplateName { get; set; }
        public string CostCode { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Hours { get; set; }
        public float? From { get; set; }
        public float? To { get; set; }
        public double? Cored { get; set; }
        public float? Recovered { get; set; }
        public string CoreSize { get; set; }
        public string DrillingDetails { get; set; }
    }
}