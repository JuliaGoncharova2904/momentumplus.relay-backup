﻿using System.Collections.Generic;

namespace IndodrillReport.BLL.ViewModels
{
    public class PdfReportViewModel
    {
        public PdfReportCompanyInformation CompanyInfo { get; set; }

        public PdfReportFieldsViewModel ReportFields { get; set; }

        public IEnumerable<PdfReportActivityViewModel> Activities { get; set; }

        public IEnumerable<PdfReportMaterialViewModel> UsedMaterials { get; set; }

        public IEnumerable<PdfReportMaterialViewModel> LeftMaterials { get; set; }

        public IEnumerable<PdfReportBitViewModel> Bits { get; set; }

        public IEnumerable<PdfReportApprovalViewModel> Approvals { get; set; }

        public IEnumerable<PdfReportSummaryViewModel> Summary { get; set; }
        
    }
}