﻿using System;

namespace IndodrillReport.BLL.ViewModels
{
    public class PdfReportMaterialViewModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Units { get; set; }

        public int? Quantity { get; set; }
    }
}