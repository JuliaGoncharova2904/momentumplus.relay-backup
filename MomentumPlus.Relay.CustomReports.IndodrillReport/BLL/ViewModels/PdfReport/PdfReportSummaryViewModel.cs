﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IndodrillReport.BLL.ViewModels
{
    public class PdfReportSummaryViewModel
    {
        public string ActivityName { get; set; }
        public string CostCode { get; set; }
        public string Hours { get; set; }
        public string Meters { get; set; }
    }
}