﻿using IndodrillReport.DAL;
using IndodrillReport.DAL.Entities;
using System.Linq;

namespace IndodrillReport.BLL.Logic
{
    public static class ReportValidationLogic
    {
        public static bool Validate(this DailyDrillingReport report, GenericUnitOfWork _repo)
        {
            bool isValid = true;

            if(!ValidateReportFields(report, _repo))
                isValid = false;

            foreach(Activity activity in report.Activities.ToList())
            {
                if(!activity.Validate(_repo))
                    isValid = false;
            }

            foreach(UsedMaterial material in report.UsedMaterials.ToList())
            {
                if(!material.Validate(_repo))
                    isValid = false;
            }

            foreach(LeftInHole material in report.LeftInHole.ToList())
            {
                if (!material.Validate(_repo))
                    isValid = false;
            }

            return isValid;
        }

        private static bool ValidateReportFields(DailyDrillingReport report, GenericUnitOfWork _repo)
        {
            bool isValid = true;

            if (!report.ClientId.HasValue || !_repo.Repository<Client>().GetAll().Any(c => c.Id == report.ClientId.Value) || string.IsNullOrEmpty(report.ClientName))
            {
                report.ClientId = null;
                report.ClientName = null;
                isValid = false;
            }

            if(!report.LocationId.HasValue || !_repo.Repository<Location>().GetAll().Any(l => l.Id == report.LocationId.Value) || string.IsNullOrEmpty(report.Location))
            {
                report.LocationId = null;
                report.Location = null;
                isValid = false;
            }

            if(string.IsNullOrEmpty(report.ShiftType))
                isValid = false;

            if(!report.DrillerId.HasValue || string.IsNullOrEmpty(report.DrillerName))
            {
                report.DrillerId = null;
                report.DrillerName = null;
                isValid = false;
            }

            if(!report.DrillRigId.HasValue || !_repo.Repository<RigTemplate>().GetAll().Any(rt => rt.Id == report.DrillRigId.Value) || string.IsNullOrEmpty(report.DrillRig))
            {
                report.DrillRigId = null;
                report.DrillRig = null;
                isValid = false;
            }

            if (string.IsNullOrEmpty(report.DrillersAssistant))
                isValid = false;

            if (string.IsNullOrEmpty(report.Helpers))
                isValid = false;

            if (!isValid)
            {
                _repo.Repository<DailyDrillingReport>().Update(report);
                _repo.SaveChanges();
            }

            return isValid;
        }

        public static bool Validate(this Activity activity, GenericUnitOfWork _repo)
        {
            bool isValid = true;

            if(!activity.TemplateActivityId.HasValue || 
                !_repo.Repository<ActivityTemplate>().GetAll().Any(at => at.Id == activity.TemplateActivityId.Value) || 
                string.IsNullOrEmpty(activity.Name))
            {
                activity.TemplateActivityId = null;
                activity.Name = null;
                isValid = false;
            }

            if(!activity.StartTime.HasValue)
                isValid = false;

            if(!activity.EndTime.HasValue)
                isValid = false;

            if(!isValid)
            {
                _repo.Repository<Activity>().Update(activity);
                _repo.SaveChanges();
            }

            return isValid;
        }

        public static bool Validate(this UsedMaterial material, GenericUnitOfWork _repo)
        {
            return (!string.IsNullOrEmpty(material.Name) && !string.IsNullOrEmpty(material.Units) && material.Quantity.HasValue);
        }

        public static bool Validate(this LeftInHole material, GenericUnitOfWork _repo)
        {
            return (!string.IsNullOrEmpty(material.Name) && !string.IsNullOrEmpty(material.Units) && material.Quantity.HasValue);
        }
    }
}