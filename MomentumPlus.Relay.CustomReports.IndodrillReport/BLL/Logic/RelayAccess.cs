﻿using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace IndodrillReport.BLL.Logic
{
    public class RelayAccess
    {
        private MomentumContext _relayContext;

        public RelayAccess(MomentumContext relayContext)
        {
            this._relayContext = relayContext;
        }

        /// <summary>
        /// Return user name by Id
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns></returns>
        public string GetUserName(Guid userId)
        {
            string name = _relayContext.UsersProfiles.Find(userId).FullName;

            return name;
        }

        /// <summary>
        /// Return List of relay users
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetRelayUsersList()
        {
            List<SelectListItem> usersList = _relayContext.UsersProfiles
                                                            .Where(u => !u.DeletedUtc.HasValue)
                                                            .OrderBy(u => u.FirstName)
                                                            .ThenBy(u => u.LastName)
                                                            .Select(u => new SelectListItem { Text = u.FirstName + " " + u.LastName, Value = u.Id.ToString() })
                                                            .ToList();

            return usersList;
        }

        /// <summary>
        /// Return List of relay users by Ids
        /// </summary>
        /// <param name="ids">Users Ids</param>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetRelayUsersListByIds(IEnumerable<Guid?> ids)
        {
            List<SelectListItem> usersList = _relayContext.UsersProfiles
                                                            .Where(up => ids.Contains(up.Id))
                                                            .OrderBy(u => u.FirstName)
                                                            .ThenBy(u => u.LastName)
                                                            .Select(u => new SelectListItem { Text = u.FirstName + " " + u.LastName, Value = u.Id.ToString() })
                                                            .ToList();

            return usersList;
        }

        /// <summary>
        /// Return Logo Id
        /// </summary>
        /// <returns></returns>
        public Guid? GetRelayLogoId()
        {
            AdminSettings settings = _relayContext.AdminSettings.FirstOrDefault();

            return settings.LogoId;
        }

        /// <summary>
        /// Return relay file by Id
        /// </summary>
        /// <param name="fileId">File Id</param>
        /// <returns></returns>
        public File GetRelayFile(Guid fileId)
        {
            File file = _relayContext.Files.Find(fileId);

            if (file == null)
                throw new Exception(string.Format("Relay file with ID:{0} was not found.", fileId));

            return file;
        }
    }
}