﻿using IndodrillReport.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IndodrillReport.BLL.Logic
{
    public static class DailyDrillingReportExtensions
    {
        public static bool IsEditable(this DailyDrillingReport report)
        {
            return report.State != DailyDrillingReportState.Completed;
        }

        public static void AssertEditability(this DailyDrillingReport report)
        {
            if (!report.IsEditable())
                throw new Exception(string.Format("Report with ID:{0} is complited and can not be edited.", report.Id));
        }
    }
}