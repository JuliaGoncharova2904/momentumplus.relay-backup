﻿using IndodrillReport.DAL;
using IndodrillReport.DAL.Entities;
using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IndodrillReport.BLL.Logic
{
    public class CompanyInfoLogic
    {
        private GenericUnitOfWork _repo;
        private RelayAccess _relayAccess;

        public CompanyInfoLogic(GenericUnitOfWork repo, RelayAccess relayAccess)
        {
            this._repo = repo;
            this._relayAccess = relayAccess;
        }

        /// <summary>
        /// Create new company info entity and fill filds from previous
        /// </summary>
        /// <param name="prevInfo">Previous company info entity</param>
        /// <returns></returns>
        private CompanyInfo CreateNewInfo(CompanyInfo prevInfo)
        {
            CompanyInfo newCompanyInfo = new CompanyInfo
            {
                Id = Guid.NewGuid(),
                Name = prevInfo.Name,
                Address = prevInfo.Address,
                Phone = prevInfo.Phone,
                Email = prevInfo.Email,
                Site = prevInfo.Site,
                LogoId = prevInfo.LogoId,
                ModuleId = prevInfo.ModuleId
            };

            _repo.Repository<CompanyInfo>().Add(newCompanyInfo);

            return newCompanyInfo;
        }

        /// <summary>
        /// Create new logo entity
        /// </summary>
        /// <param name="relayLogo">Relay logo entity</param>
        /// <returns></returns>
        private Logo CreateNewLogo(File relayLogo)
        {
            Logo newLogo = new Logo
            {
                Id = Guid.NewGuid(),
                RelayLogoId = relayLogo.Id,
                ContentType = relayLogo.ContentType,
                BinaryData = relayLogo.BinaryData
            };

            _repo.Repository<Logo>().Add(newLogo);

            return newLogo;
        }

        /// <summary>
        /// Update company logo
        /// </summary>
        public void UpdateLogo()
        {
            Guid? relayLogoId = _relayAccess.GetRelayLogoId();

            if(relayLogoId.HasValue)
            {
                IndodrillReportsModule module = _repo.Repository<IndodrillReportsModule>().GetAll().FirstOrDefault();
                CompanyInfo info = _repo.Repository<CompanyInfo>().Get(module.CurrentCompanyInfoId.Value);

                if(!info.LogoId.HasValue || info.Logo.RelayLogoId != relayLogoId.Value)
                {
                    if(info.Reports.Any(r => r.State == DailyDrillingReportState.Completed))
                    {
                        info = this.CreateNewInfo(info);
                    }
                    else
                    {
                        if(info.LogoId.HasValue && info.Logo.CompanyInfos.Count() == 1)
                        {
                            _repo.Repository<Logo>().Delete(info.Logo);
                        }
                    }

                    info.LogoId = this.CreateNewLogo(_relayAccess.GetRelayFile(relayLogoId.Value)).Id;

                    module.CurrentCompanyInfoId = info.Id;

                    _repo.SaveChanges();
                }
            }
        }

    }
}