﻿using System.Web.Mvc;
using IndodrillReport.BLL;
using IndodrillReport.DAL;

namespace IndodrillReport.Controllers
{
    public abstract class BaseController : Controller
    {
        protected GenericUnitOfWork UnitOfWork;
        protected GenericServicesUnitOfWork Services;

        protected BaseController()
        {
            this.Services = new GenericServicesUnitOfWork();
            UnitOfWork = new GenericUnitOfWork();
        }
    }
}