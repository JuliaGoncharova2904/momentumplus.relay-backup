﻿using IndodrillReport.BLL.ViewModels;
using System;
using System.Web.Mvc;

namespace IndodrillReport.Controllers
{
    [AllowAnonymous]
    public class FileController : BaseController
    {

        public ActionResult Image(Guid Id)
        {
            LogoViewModel logo = Services.MediaService.GetLogo(Id);

            return File(logo.BinaryData, logo.ContentType);
        }

    }
}