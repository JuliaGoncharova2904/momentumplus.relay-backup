﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using IndodrillReport.BLL.ViewModels;
using MomentumPlus.Relay.Constants;
using MvcPaging;
using System.Net;
using MvcSiteMapProvider;
using MomentumPlus.Core.Authorization;

namespace IndodrillReport.Controllers
{
    [Authorize(Roles = iHandoverRoles.RelayGroups.Admins)]
    public class ClientTemplateController : BaseController
    {

        [MvcSiteMapNode(Title = "Clients", Area = "Indodrill", ParentKey = "Indodrill_DDR_Lists")]
        public ActionResult Index(int? page)
        {
            IPagedList<ClientItemViewModel> model = Services.ClientTemplateService.GetClientTemplateList(page - 1 ?? 0, NumericConstants.Paginator.PaginatorPageSize);

            return View("~/Views/IndodrillReport/MasterList/Client/Index.cshtml", model);
        }

        [HttpPost]
        public ActionResult RemoveClient(Guid clientId)
        {
            Services.ClientTemplateService.RemoveClient(clientId);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        #region Client

        [HttpGet]
        public ActionResult AddClientFormDialog()
        {
            return PartialView("~/Views/IndodrillReport/MasterList/Client/_AddClientFormDialog.cshtml");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddClientFormDialog(ClientFormDialogViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!Services.ClientTemplateService.IsClientExist(model.Name))
                {
                    Guid templateId = Services.ClientTemplateService.AddClientTemplate(model);
                    return Json(new { clientTemplateId = templateId });
                }
                ModelState.AddModelError("Name", "Client with same name is exists.");
            }

            return PartialView("~/Views/IndodrillReport/MasterList/Client/_ClientForm.cshtml", model);
        }

        [HttpGet]
        public ActionResult EditClientFormDialog(Guid clientId)
        {
            ClientFormDialogViewModel model = Services.ClientTemplateService.GetClientForDialog(clientId);

            return PartialView("~/Views/IndodrillReport/MasterList/Client/_EditClientFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditClientFormDialog(ClientFormDialogViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!Services.ClientTemplateService.IsClientExist(model.Name, model.ClientId.Value))
                {
                    Services.ClientTemplateService.UpdateClient(model);
                    return Content("ok");
                }
                ModelState.AddModelError("Name", "Client with same name is exists.");
            }

            return PartialView("~/Views/IndodrillReport/MasterList/Client/_ClientForm.cshtml", model);
        }

        [HttpPost]
        public ActionResult LocationsForClient(Guid clientId)
        {
            IEnumerable<SelectListItem> model = Services.ClientTemplateService.GetClientLocations(clientId);

            return PartialView("~/Views/IndodrillReport/MasterList/Client/_LocationsSwiper.cshtml", model);
        }

        #endregion

        #region Location

        [HttpGet]
        public ActionResult AddLocation(Guid clientId)
        {
            LocationViewModel model = new LocationViewModel { ClientId = clientId };

            ViewBag.Title = "New";
            return PartialView("~/Views/IndodrillReport/MasterList/Client/_LocationForm.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddLocation(LocationViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!Services.ClientTemplateService.IsClientLocationExist(model.ClientId, model.LocationName))
                {
                    Services.ClientTemplateService.AddLocation(model);
                    
                    return Content("ok");
                }
                ModelState.AddModelError("LocationName", "Location with same name is exists.");
            }

            ViewBag.Title = "New";
            return PartialView("~/Views/IndodrillReport/MasterList/Client/_LocationForm.cshtml", model);
        }

        [HttpGet]
        public ActionResult EditLocation(Guid locationId)
        {
            LocationViewModel model = Services.ClientTemplateService.GetLocation(locationId);

            ViewBag.Title = "Edit";
            return PartialView("~/Views/IndodrillReport/MasterList/Client/_LocationForm.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditLocation(LocationViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!Services.ClientTemplateService.IsClientLocationExist(model.ClientId, model.LocationName, model.LocationId))
                {
                    Services.ClientTemplateService.UpdateLocation(model);
                    return Content("ok");
                }
                ModelState.AddModelError("LocationName", "Location with same name is exists.");
            }
            ViewBag.Title = "Edit";
            return PartialView("~/Views/IndodrillReport/MasterList/Client/_LocationForm.cshtml", model);
        }

        [HttpPost]
        public ActionResult RemoveLocation(Guid locationId)
        {
            Services.ClientTemplateService.RemoveLocation(locationId);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        #endregion
    }
}