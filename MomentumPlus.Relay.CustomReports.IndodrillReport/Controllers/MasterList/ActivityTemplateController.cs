﻿using System;
using System.Web.Mvc;
using IndodrillReport.BLL.ViewModels;
using MvcPaging;
using MomentumPlus.Relay.Constants;
using System.Net;
using MvcSiteMapProvider;
using MomentumPlus.Core.Authorization;

namespace IndodrillReport.Controllers
{
    [Authorize(Roles = iHandoverRoles.RelayGroups.Admins)]
    public class ActivityTemplateController : BaseController
    {
        [MvcSiteMapNode(Key = "Indodrill_DDR_Lists", Title = "DDR Lists", Area = "Indodrill", ParentKey = "Framework")]
        public ActionResult Index()
        {
            return RedirectToAction("ActivitiesList", "ActivityTemplate");
        }

        [MvcSiteMapNode(Title = "Activities", Area = "Indodrill", ParentKey = "Indodrill_DDR_Lists")]
        public ActionResult ActivitiesList(int? page)
        {
            IPagedList<ActivityItemViewModel> model = Services.ActivityTemplateService.GetActivityTemplateList(page - 1 ?? 0, NumericConstants.Paginator.PaginatorPageSize);

            return View("~/Views/IndodrillReport/MasterList/Activity/Index.cshtml", model);
        }

        [HttpGet]
        public ActionResult AddActivityFormDialog()
        {
            ActivityFormDialogViewModel model = new ActivityFormDialogViewModel();
            ViewBag.Title = "Add Activity";
            return PartialView("~/Views/IndodrillReport/MasterList/Activity/_ActivityFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddActivityFormDialog(ActivityFormDialogViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!Services.ActivityTemplateService.IsActivityExist(model.Name))
                {
                    Services.ActivityTemplateService.AddActivityTemplate(model);   
                    return Content("ok");
                }
                ModelState.AddModelError("Name", "Activity with same name is exists.");
            }
            ViewBag.Title = "Add Activity";
            return PartialView("~/Views/IndodrillReport/MasterList/Activity/_ActivityFormDialog.cshtml", model);
        }

        [HttpGet]
        public ActionResult EditActivityFormDialog(Guid activityId)
        {
            ActivityFormDialogViewModel model = Services.ActivityTemplateService.GetActivityForDialog(activityId);

            ViewBag.Title = "Edit Activity";
            
            return PartialView("~/Views/IndodrillReport/MasterList/Activity/_ActivityFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditActivityFormDialog(ActivityFormDialogViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!Services.ActivityTemplateService.IsActivityExist(model.Name, model.Id))
                {
                    Services.ActivityTemplateService.UpdateActivity(model);
                    return Content("ok");
                }
                ModelState.AddModelError("Name", "Activity with same name is exists.");
            }
            ViewBag.Title = "Edit Activity";
            return PartialView("~/Views/IndodrillReport/MasterList/Activity/_ActivityFormDialog.cshtml", model);
        }

        [HttpPost]
        public ActionResult RemoveActivity(Guid activityId)
        {
            Services.ActivityTemplateService.RemoveActivity(activityId);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}