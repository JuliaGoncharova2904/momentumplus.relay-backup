﻿using System;
using System.Web.Mvc;
using IndodrillReport.BLL.ViewModels;
using MvcPaging;
using MomentumPlus.Relay.Constants;
using System.Net;
using MvcSiteMapProvider;
using MomentumPlus.Core.Authorization;

namespace IndodrillReport.Controllers
{
    [Authorize(Roles = iHandoverRoles.RelayGroups.Admins)]
    public class RigTemplateController : BaseController
    {
        [MvcSiteMapNode(Title = "Rigs", Area = "Indodrill", ParentKey = "Indodrill_DDR_Lists")]
        public ActionResult Index(int? page)
        {
            IPagedList<RigTemplateItemViewModel> model = Services.RigTemplateService.GetRigsTemplate(page - 1 ?? 0, NumericConstants.Paginator.PaginatorPageSize);
            return View("~/Views/IndodrillReport/MasterList/DrillRig/Index.cshtml", model);
        }

        [HttpGet]
        public ActionResult AddDrillRigFormDialog()
        {
            RigTemplateFormDialogViewModel model = new RigTemplateFormDialogViewModel();
            ViewBag.Title = "Add Rig";
            return PartialView("~/Views/IndodrillReport/MasterList/DrillRig/_DrillerRigFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddDrillRigFormDialog(RigTemplateFormDialogViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!Services.RigTemplateService.IsRigExist(model.Name))
                {
                    Services.RigTemplateService.AddRigTemplate(model);
                    return Content("ok");
                }
                ModelState.AddModelError("Name", "Rig with same name is exists.");
            }
            ViewBag.Title = "Add Rig";
            return PartialView("~/Views/IndodrillReport/MasterList/DrillRig/_DrillerRigFormDialog.cshtml");
        }

        [HttpGet]
        public ActionResult EditDrillRigFormDialog(Guid rigId)
        {
            RigTemplateFormDialogViewModel model = Services.RigTemplateService.GetRigForDialog(rigId);
            ViewBag.Title = "Add Rig";
            return PartialView("~/Views/IndodrillReport/MasterList/DrillRig/_DrillerRigFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDrillRigFormDialog(RigTemplateFormDialogViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!Services.RigTemplateService.IsRigExist(model.Name, model.Id))
                {
                    Services.RigTemplateService.UpdateRig(model);
                    return Content("ok");
                }
                ModelState.AddModelError("Name", "Rig with same name is exists.");
            }
            ViewBag.Title = "Add Rig";
            return PartialView("~/Views/IndodrillReport/MasterList/DrillRig/_DrillerRigFormDialog.cshtml");
        }

        [HttpPost]
        public ActionResult RemoveRig(Guid rigId)
        {
            Services.RigTemplateService.RemoveRig(rigId);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}