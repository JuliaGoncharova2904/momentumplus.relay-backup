﻿using System;
using System.Web.Mvc;
using IndodrillReport.BLL.ViewModels;
using Rotativa;

namespace IndodrillReport.Controllers
{
    public class PdfReportController : BaseController
    {
        public ActionResult ReportPreview(Guid reportId)
        {
            PdfReportViewModel model = Services.PdfReportService.GetReportForPrint(reportId);
            return PartialView("~/Views/IndodrillReport/PdfReport/_ReportPreview.cshtml", model);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ReportPrint(Guid reportId)
        {
            var pdfResult = new ActionAsPdf("PrintIndodrillReport", new { reportId })
            {
                FileName = "DAILY-DRILLING-REPORT.pdf",
                PageOrientation = Rotativa.Options.Orientation.Portrait,
                IsGrayScale = false,
                IsJavaScriptDisabled = false,
                PageMargins = new Rotativa.Options.Margins(8, 16, 10, 16)
            };

            var binary = pdfResult.BuildPdf(this.ControllerContext);

            return File(binary, "application/pdf");
        }

        [AllowAnonymous]
        public ActionResult PrintIndodrillReport(Guid reportId)
        {
            PdfReportViewModel model = Services.PdfReportService.GetReportForPrint(reportId);

            return View("~/Views/IndodrillReport/PdfReport/_ReportPrint.cshtml", model);
        }
    }
}