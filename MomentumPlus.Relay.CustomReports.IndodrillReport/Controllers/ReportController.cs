﻿using System.Web.Mvc;
using System;
using IndodrillReport.BLL.ViewModels;
using Microsoft.AspNet.Identity;
using System.Net;
using System.Collections.Generic;
using MvcSiteMapProvider;
using MomentumPlus.Core.Authorization;

namespace IndodrillReport.Controllers
{
    [Authorize(Roles = iHandoverRoles.RelayGroups.Admins + "," + iHandoverRoles.RelayGroups.LineManagers)]
    public class ReportController : BaseController
    {

        #region Report methods

        [HttpGet]
        public ActionResult GetOrCreateReportForShift(Guid shiftId)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            DrillingReportViewModel model = Services.ReportService.GetCreateReportForShift(shiftId, userId);

            return PartialView("~/Views/IndodrillReport/DrillingReport/ReportPageContent.cshtml", model);
        }

        [MvcSiteMapNode(Key = "Indodrill_Report", Title = "Drilling Report", Area = "Indodrill", ParentKey = "Indodrill_DDRs")]
        public ActionResult ReportPage(Guid reportId, bool? validateFields)
        {
            DrillingReportViewModel model = Services.ReportService.GetReport(reportId);

            model.ValidateFields = validateFields.HasValue ? validateFields.Value : false;

            return View("~/Views/IndodrillReport/DrillingReport/ReportPage.cshtml", model);
        }
        
        [HttpPost]
        public ActionResult RemoveReport(Guid reportId)
        {
            Services.ReportService.RemoveReport(reportId);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult CompleteReport(Guid reportId)
        {
            bool completed = Services.ReportService.Complete(reportId);

            return Json(new { isCompleted = completed });
        }

        [HttpPost]
        public ActionResult UpdateReportFields(ReportFieldsViewModel model)
        {
            Services.ReportService.UpdateReportField(model);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult GetLocationsForClient(Guid clientId)
        {
            IEnumerable<SelectListItem> locations = Services.ReportService.LocationsForClient(clientId);

            return Json(locations);
        }

        #endregion

        #region Activity row

        [HttpPost]
        public ActionResult AddActivityRow(Guid reportId)
        {
            ReportActivityViewModel model = Services.ReportService.AddActivityItem(reportId);

            return PartialView("~/Views/IndodrillReport/DrillingReport/Tables/ActivityTable/_ActivityTableRow.cshtml", model);
        }

        [HttpPost]
        public ActionResult UpdateActivityRow(ReportActivityViewModel model)
        {
            Services.ReportService.UpdateActivity(model);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult RemoveActivityRow(Guid activityId)
        {
            Services.ReportService.RemoveActivityItem(activityId);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult GetCostCode(Guid activityId)
        {
            string costCode = Services.ReportService.ActivityCostCode(activityId);

            return Json(new { costCode = costCode });
        }

        #endregion

        #region Used Material

        [HttpPost]
        public ActionResult AddUsedMaterialRow(Guid reportId)
        {
            ReportMaterialViewModel model = Services.ReportService.AddUsedMaterialItem(reportId);

            return PartialView("~/Views/IndodrillReport/DrillingReport/Tables/MaterialsTable/_MaterialsTableRow.cshtml", model);
        }

        [HttpPost]
        public ActionResult UpdateUsedMaterialRow(ReportMaterialViewModel model)
        {
            Services.ReportService.UpdateUsedMaterial(model);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult RemoveUsedMaterialRow(Guid materialId)
        {
            Services.ReportService.RemoveUsedMaterial(materialId);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        #endregion

        #region Left In Hole

        [HttpPost]
        public ActionResult AddLeftInHoleRow(Guid reportId)
        {
            ReportMaterialViewModel model = Services.ReportService.AddLeftInHoleItem(reportId);

            return PartialView("~/Views/IndodrillReport/DrillingReport/Tables/LeftInHoleTable/_LeftInHoleTableRow.cshtml", model);
        }

        [HttpPost]
        public ActionResult UpdateLeftInHoleRow(ReportMaterialViewModel model)
        {
            Services.ReportService.UpdateLeftInHoleMaterial(model);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult RemoveLeftInHoleRow(Guid materialId)
        {
            Services.ReportService.RemoveLeftInHoleMaterial(materialId);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        #endregion

        #region Bit row

        [HttpPost]
        public ActionResult AddBitRow(Guid reportId)
        {
            ReportBitViewModel model = Services.ReportService.AddBitItem(reportId);

            return PartialView("~/Views/IndodrillReport/DrillingReport/Tables/BitsTable/_BitsTableRow.cshtml", model);
        }

        [HttpPost]
        public ActionResult RemoveBitRow(Guid bitId)
        {
            Services.ReportService.RemoveBitItem(bitId);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult UpdateBitRow(ReportBitViewModel model)
        {
            Services.ReportService.UpdateBit(model);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        #endregion

        #region Approvals table

        [HttpPost]
        public ActionResult UpdateApprovalRow(ReportApprovalViewModel model)
        {
            Services.ReportService.UpdateApproval(model);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        #endregion

    }
}