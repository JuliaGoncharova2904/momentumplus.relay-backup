﻿using IndodrillReport.BLL.ViewModels;
using Microsoft.AspNet.Identity;
using MomentumPlus.Core.Authorization;
using MvcSiteMapProvider;
using System;
using System.Web.Mvc;

namespace IndodrillReport.Controllers
{
    [Authorize(Roles = iHandoverRoles.RelayGroups.Admins + "," + iHandoverRoles.RelayGroups.LineManagers)]
    public class ReportsController : BaseController
    {
        [MvcSiteMapNode(Key = "Indodrill_DDRs", Title = "DDRs", Area = "Indodrill", ParentKey = "Reports", Roles = new[] { iHandoverRoles.Relay.Administrator, iHandoverRoles.Relay.iHandoverAdmin, iHandoverRoles.Relay.ExecutiveUser, iHandoverRoles.Relay.LineManager, iHandoverRoles.Relay.HeadLineManager })]
        public ActionResult Index(int? page, int? pageSize, ReportFilterViewModel filter)
        {
            ReportsListViewModel model = Services.ReportsListService.GetReportsList(page - 1 ?? 0, pageSize ?? 5, filter);

            return View("~/Views/IndodrillReport/ReportList/Index.cshtml", model);
        }

        public ActionResult AddReport()
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            Guid reportId = Services.ReportService.AddReport(userId);

            return RedirectToAction("ReportPage", "Report", new { reportId = reportId });
        }
    }
}