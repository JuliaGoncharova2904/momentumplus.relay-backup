﻿using System;
using System.Web.Http;
using System.Configuration;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http.Cors;
using System.Web.Http.ExceptionHandling;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using MomentumPlus.Core.Authorization;
using MomentumPlus.Relay.Api.Handlers;
using MomentumPlus.Relay.CompositionRoot;
using Newtonsoft.Json.Serialization;
using Owin;

namespace MomentumPlus.Relay.Api
{
    public static class ApiConfiguration
    {
        public static void Configure(IAppBuilder app)
        {
            ConfigureOAuthBearerAuthentication(app);

            ConfigureOAuthEndpoint(app);

            // Web API configuration
            HttpConfiguration config = new HttpConfiguration();

            config.Services.Replace(typeof(IExceptionHandler), new ApiExceptionHandler());

            ConfigureApi(config);

            // Light Inject configuration
            ApiLightInject.RegisterContainer(config);

            app.UseWebApi(config);
        }

        // Web API configuration and services
        private static void ConfigureApi(HttpConfiguration config)
        {
            //Enable CORS 
            var cors = new EnableCorsAttribute(
                        origins: "*",
                        headers: "*",
                        methods: "*");
            config.EnableCors(cors);

            config.MessageHandlers.Add(new PreflightRequestsHandler());

            // Add Filters
            config.Filters.Add(new AuthorizeAttribute());

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }



        private static void ConfigureOAuthEndpoint(IAppBuilder app)
        {
            var issuer = ConfigurationManager.AppSettings["siteUrl"];

            app.UseOAuthAuthorizationServer(new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/oauth2/login"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(30),
                Provider = new ApiAuthenticationProvider(),
                AccessTokenFormat = new CustomJwtFormat(issuer),
                RefreshTokenProvider = new ApiRefreshTokenProvider(),
                RefreshTokenFormat = new CustomJwtFormat(issuer)
            });
        }


        private static void ConfigureOAuthBearerAuthentication(IAppBuilder app)
        {
            var issuer = ConfigurationManager.AppSettings["siteUrl"];
            var secret = TextEncodings.Base64Url.Decode(ConfigurationManager.AppSettings["secret"]);
            var audienceId = ConfigurationManager.AppSettings["audience"];

            app.UseJwtBearerAuthentication(new JwtBearerAuthenticationOptions
            {
                AuthenticationMode = AuthenticationMode.Active,
                AllowedAudiences = new[] { audienceId },
                IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[] { new SymmetricKeyIssuerSecurityTokenProvider(issuer, secret) }
            });
        }
    }
}