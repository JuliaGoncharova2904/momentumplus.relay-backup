﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MomentumPlus.Relay.Interfaces;

namespace MomentumPlus.Relay.Api.Controllers
{
    [RoutePrefix("profile")]
    public class ProfileController : BaseApiController
    {
        public ProfileController(IApiServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        {
        }
        [HttpGet]
        public IHttpActionResult Get(Guid? userId = null)
        {
            var model = Services.ProfileService.PopulateProfileInfo(userId ?? CurrentUserId);

            return Ok(model);
        }

        [HttpPost]
        public IHttpActionResult UpdateAvatar(HttpRequestMessage request)
        {
            var base64ImageData = request.Content.ReadAsStringAsync().Result;

            var newAvatarUrl = Services.ProfileService.UpdateAvatar(base64ImageData, CurrentUserId);

            return Ok(new { newAvatarUrl });
        }


    }
}