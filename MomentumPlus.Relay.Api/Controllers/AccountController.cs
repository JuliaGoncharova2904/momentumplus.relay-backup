﻿using MomentumPlus.Relay.Interfaces;

namespace MomentumPlus.Relay.Api.Controllers
{
    public class AccountController : BaseApiController
    {
        public AccountController(IApiServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        {
        }
    }
}