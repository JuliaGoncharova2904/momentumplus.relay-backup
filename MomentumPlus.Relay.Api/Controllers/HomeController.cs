﻿using System;
using System.Web.Http;
using MomentumPlus.Relay.Interfaces;

namespace MomentumPlus.Relay.Api.Controllers
{
    public class HomeController : BaseApiController
    {
        public HomeController(IApiServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        {
        }

        public IHttpActionResult Get(Guid? userId = null)
        {
            var model = Services.HomeService.PopulateHomePage(userId ?? CurrentUserId);

            return Ok(model);
        }
    }
}