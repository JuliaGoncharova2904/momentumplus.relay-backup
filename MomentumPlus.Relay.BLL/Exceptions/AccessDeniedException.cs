﻿using System;

namespace MomentumPlus.Relay.BLL.Exceptions
{
    public class AccessDeniedException : Exception
    {
        public AccessDeniedException()
        { }

        public AccessDeniedException(string message) : base(message)
        { }
    }
}
