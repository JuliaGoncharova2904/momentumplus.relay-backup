﻿using System;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.Extensions.EntityExtensions
{
    public static class TaskExtensions
    {
        public static bool HasParent(this RotationTask task)
        {
            if (task.AncestorTaskId.HasValue)
            {
                return true;
            }

            return false;
        }

        public static Guid? GetTaskLastAssignedId(this RotationTask task)
        {
            var assignedId = task.AssignedToId;

            if (task.SuccessorTaskId.HasValue)
            {
                var childTask = task.SuccessorTask;

                while (childTask != null)
                {
                    childTask = childTask.SuccessorTask;

                    if (childTask != null)
                    {
                        assignedId = childTask.AssignedToId;
                    }
                }
            }

            return assignedId;
        }
    }
}
