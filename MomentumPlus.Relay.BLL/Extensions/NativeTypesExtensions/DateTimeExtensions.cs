﻿using System;

namespace MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions
{
    public static class DateTimeExtensions
    {
        private static string ConvertDay(int day)
        {
            string suffix = string.Empty;

            switch (day)
            {
                case 1:
                case 21:
                case 31:
                    suffix = "st";
                    break;
                case 2:
                case 22:
                    suffix = "nd";
                    break;
                case 3:
                case 23:
                    suffix = "rd";
                    break;
                default:
                    suffix = "th";
                    break;
            }

            return string.Format("{0}{1}", day, suffix);
        }

        public static string DayFormat(this DateTime dt)
        {
            return ConvertDay(dt.Day);
        }

        #region Format time

        public static string FormatTo24TimeWithMeridiem(this DateTime? dt)
        {
            if (dt.HasValue)
                return dt.Value.ToString("HH:mm") + dt.Value.ToString("tt").ToLower();

            return string.Empty;
        }

        #endregion

        #region Format with day and month

        public static string FormatWithDayAndMonth(this DateTime dt)
        {
            return string.Format("{0:ddd} {1} {0:MMM}", dt, ConvertDay(dt.Day));
        }

        public static string FormatWithDayAndMonth(this DateTime? dt)
        {
            if (dt.HasValue)
                return string.Format("{0:ddd} {1} {0:MMM}", dt, ConvertDay(dt.Value.Day));

            return string.Empty;
        }

        #endregion

        #region Format with month

        public static string FormatWithMonth(this DateTime dt)
        {
            return string.Format("{1} {0:MMM}", dt, ConvertDay(dt.Day));
        }

        public static string FormatWithMonth(this DateTime? dt)
        {
            if (dt.HasValue)
                return string.Format("{1} {0:MMM}", dt, ConvertDay(dt.Value.Day));

            return string.Empty;
        }

        public static string FormatDayMonth(this DateTime dt)
        {
            return string.Format("{0:dd} {0:MMM}", dt);
        }

        public static string FormatDayMonth(this DateTime? dt)
        {
            if (dt.HasValue)
                return string.Format("{0:dd} {0:MMM}", dt);

            return string.Empty;
        }

        #endregion

        #region Format with day, month and year

        public static string FormatWithDayMonthYear(this DateTime dt)
        {
            return string.Format("{1} {0:MMM} {0:yy}", dt, ConvertDay(dt.Day));
        }

        public static string FormatWithDayMonthYear(this DateTime? dt)
        {
            if (dt.HasValue)
                return string.Format("{1} {0:MMM} {0:yy}", dt, ConvertDay(dt.Value.Day));

            return string.Empty;
        }

        public static string FormatWithDayAndMonthYear(this DateTime dt)
        {
            return string.Format("{0:ddd} {1} {0:MMM} {0:yy}", dt, ConvertDay(dt.Day));
        }

        public static string FormatWithDayAndMonthAndYear(this DateTime? dt)
        {
            if (dt.HasValue)
                return string.Format("{0:ddd} {1} {0:MMM} {0:yy}", dt, ConvertDay(dt.Value.Day));

            return string.Empty;
        }

        #endregion


        #region Format with day name, date and full year

        public static string FormatDayMonthYear(this DateTime dt)
        {
            return string.Format("{1} {0:MMM} {0:yyyy}", dt, ConvertDay(dt.Day));
        }

        public static string FormatDayMonthYear(this DateTime? dt)
        {
            if (dt.HasValue)
                return string.Format("{1} {0:MMM} {0:yyyy}", dt, ConvertDay(dt.Value.Day));

            return string.Empty;
        }

        public static string FormatWithDayNameDateAndFullYear(this DateTime dt)
        {
            return string.Format(" {0:ddd}, {0:dd} {0:MMM} {0:yyyy}", dt);
        }

        public static string FormatWithDayNameDateAndFullYear(this DateTime? dt)
        {
            if (dt.HasValue)
                return string.Format(" {0:ddd}, {0:dd} {0:MMM} {0:yyyy}", dt);

            return string.Empty;
        }


        #endregion


        #region Rotation Dates fo Print

        public static string FormatWithDateMonthAndFullYear(this DateTime? dt)
        {
            if (dt.HasValue)
                return string.Format("{0:dd} {0:MMM} {0:yyyy}", dt);

            return string.Empty;
        }

        public static string FormatTo24Time(this DateTime? dt)
        {
            if (dt.HasValue)
                return dt.Value.ToString("HH:mm");

            return string.Empty;
        }



        public static string FormatWithDateMonthAndFullYear(this DateTime dt)
        {
            return string.Format("{0:dd} {0:MMM} {0:yyyy}", dt);
        }

        public static string FormatWithDateMonthFullYearAndTime(this DateTime? dt)
        {
            if (dt.HasValue)
                return string.Format("{0:dd} {0:MMM} {0:yyyy} {0:HH} : {0:mm}", dt);

            return string.Empty;
        }


        public static string FormatWithDateMonthFullYearAndTime(this DateTime dt)
        {
            return string.Format("{0:dd} {0:MMM} {0:yyyy} {0:HH}:{0:mm}", dt);
        }


        #endregion
    }
}
