﻿using AutoMapper;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class WebAutoMapperConfig
    {
        private static MapperConfiguration _config;

        static WebAutoMapperConfig()
        {
            ConfigureAutomapper();
        }

        private static void ConfigureAutomapper()
        {
            _config = new MapperConfiguration(cfg =>
            {
                SelectListMapping.Configure(cfg);
                DropDownListMapping.Configure(cfg);


                #region Section Topics

                CoreTopicMapping.Configure(cfg);
                HSETopicMapping.Configure(cfg);
                ProjectTopicMapping.Configure(cfg);
                TeamTopicMapping.Configure(cfg);
                TasksTopicMapping.Configure(cfg);

                #endregion

                ReportBuilderMapping.Configure(cfg);
                DailyNotesMapping.Configure(cfg);
                SafetyMapping.Configure(cfg);
                SafetyV2Mapping.Configure(cfg);
                TeamMapping.Configure(cfg);
                CompanyMapping.Configure(cfg);
                SiteMapping.Configure(cfg);
                RotationPatternMapping.Configure(cfg);
                PositionMapping.Configure(cfg);
                ReportMapping.Configure(cfg);
                EmployeeMapping.Configure(cfg);
                RotationMapping.Configure(cfg);
                MediaMapping.Configure(cfg);
                ModulesMapping.Configure(cfg);
                LocationMapping.Configure(cfg);
                RotationTaskMapping.Configure(cfg);
                RotationTopicMapping.Configure(cfg);
                ShiftMapping.Configure(cfg);
                VoiceMessageMapping.Configure(cfg);
                DashboardMapping.Configure(cfg);
                AdminSettingsMapping.Configure(cfg);
                ProjectMapping.Configure(cfg);
                TimelineMapping.Configure(cfg);
                NotificationMapping.Configure(cfg);
                SharingTopicRelationMapping.Configure(cfg);
                SharingReportRelationMapping.Configure(cfg);

                DailyReportsMapping.Configure(cfg);

                #region Checklist mapping

                ChecklistMapping.Configure(cfg);

                #endregion
            });
        }
        public static IMapper GetMapper()
        {
            return _config.CreateMapper();
        }
    }
}