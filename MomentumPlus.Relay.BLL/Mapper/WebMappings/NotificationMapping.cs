﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore.Cores;
using MomentumPlus.Relay.Models;
using System;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class NotificationMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<Notification, NotificationMessageViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.UrlImage, opt => opt.MapFrom(src => src.IconPath))
                .ForMember(dest => dest.Message, opt => opt.MapFrom(src => src.HtmlMessage))
                .ForMember(dest => dest.Date, opt => opt.MapFrom(src => src.Timestamp))
                .ForMember(dest => dest.IsOpened, opt => opt.MapFrom(src => src.IsOpened))
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.Type.ToString()));

            cfg.CreateMap<Notification, NotificationMessageItemViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.UrlImage, opt => opt.MapFrom(src => src.IconPath))
                .ForMember(dest => dest.Message, opt => opt.MapFrom(src => src.HtmlMessage))
                .ForMember(dest => dest.Timestapm, opt => opt.MapFrom(src => src.Timestamp))
                .ForMember(dest => dest.IsOpened, opt => opt.MapFrom(src => src.IsOpened))
                .ForMember(dest => dest.TypeClass, opt => opt.ResolveUsing(src => src.Type.GetTypeClass()))
                .ForMember(dest => dest.RelationId, opt => opt.MapFrom(src => src.RelationId.HasValue ? src.RelationId.Value : Guid.Empty));

        }
    }
}
