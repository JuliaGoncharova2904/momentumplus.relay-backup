﻿using System;
using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Authorization;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class TeamMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {

            cfg.CreateMap<Team, TeamViewModel>();

            cfg.CreateMap<TeamViewModel, Team>()
                .ForMember(dest => dest.Id, opt => opt.Ignore());

        }
    }
}
