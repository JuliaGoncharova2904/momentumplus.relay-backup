﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class ReportBuilderMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<Rotation, FromToBlockViewModel>()
                     .ForMember(dest => dest.RotationOwnerId, opt => opt.MapFrom(src => src.RotationOwnerId))
                     .ForMember(dest => dest.RotationOwnerFullName, opt => opt.MapFrom(src => src.RotationOwner.FullName))
                     .ForMember(dest => dest.RotationBackToBackId, opt => opt.MapFrom(src => src.DefaultBackToBackId))
                     .ForMember(dest => dest.RotationBackToBackFullName, opt => opt.MapFrom(src => src.DefaultBackToBack.FullName));
        }
    }
}
