﻿using System;
using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class ChecklistMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            #region Team member mapping

            cfg.CreateMap<UserProfile, BackToBackForTeamViewModel>()
            .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.FullName))
            .ForMember(dest => dest.UserEmail, opt => opt.MapFrom(src => src.Email))
            .ForMember(dest => dest.RotarionPatternName, opt => opt.MapFrom(src => src.RotationPattern.Name))
            .ForMember(dest => dest.PositionName, opt => opt.MapFrom(src => src.Position.Name));

            cfg.CreateMap<UserProfile, LineManagerForTeamViewModel>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.FullName))
                .ForMember(dest => dest.UserEmail, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.RotarionPatternName, opt => opt.MapFrom(src => src.RotationPattern.Name))
                .ForMember(dest => dest.PositionName, opt => opt.MapFrom(src => src.Position.Name));

            cfg.CreateMap<UserProfile, TeamMemberViewModel>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.FullName));

            #endregion

            #region Filter Model mapping

            cfg.CreateMap<Rotation, ChecklistRotationFilterViewModel>()
                .ForMember(dest => dest.RotationId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Date, opt => opt.MapFrom(src =>
                            (
                                src.StartDate.HasValue ?
                                    (Convert.ToDateTime(src.StartDate).FormatWithMonth() + " - " + Convert.ToDateTime(src.StartDate.Value.AddDays(src.DayOff + src.DayOn - 1)).FormatWithMonth())
                                    : "")))
                .ForMember(dest => dest.PrevRotationId, opt => opt.MapFrom(src => src.PrevRotationId))
                .ForMember(dest => dest.NextRotationId, opt => opt.MapFrom(src => src.NextRotationId));

            #endregion
        }
    }
}
