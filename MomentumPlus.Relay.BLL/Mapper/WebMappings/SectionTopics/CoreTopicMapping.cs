﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;
using System.Linq;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class CoreTopicMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<RotationTopic, CoreTopicViewModel>()
                 .ForMember(dest => dest.OwnerId, opt => opt.MapFrom(src => (src.RotationTopicGroup.RotationModule.ShiftId.HasValue)
                                        ? src.RotationTopicGroup.RotationModule.ShiftId
                                        : src.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId))
                .ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.RotationTopicGroup.Name))
                .ForMember(dest => dest.Reference, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Notes, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.TeammateId, opt => opt.MapFrom(src => src.AssignedToId))
                .ForMember(dest => dest.IsCustom, opt => opt.MapFrom(src => !src.TempateTopicId.HasValue))
                .ForMember(dest => dest.FromTeammateId, opt => opt.MapFrom(src => (src.AncestorTopic.RotationTopicGroup.RotationModule.RotationId.HasValue)
                                        ? src.AncestorTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId
                                        : src.AncestorTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId))
                .ForMember(dest => dest.FromTeammateFullName, opt => opt.MapFrom(src => (src.AncestorTopic.RotationTopicGroup.RotationModule.RotationId.HasValue)
                                        ? src.AncestorTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwner.FullName
                                        : src.AncestorTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwner.FullName))
                .ForMember(dest => dest.FromRotationId, opt => opt.MapFrom(src => src.AncestorTopic.RotationTopicGroup.RotationModule.RotationId))
                .ForMember(dest => dest.FromShiftId, opt => opt.MapFrom(src => src.AncestorTopic.RotationTopicGroup.RotationModule.ShiftId))
                .ForMember(dest => dest.IsFinalized, opt => opt.MapFrom(src => src.FinalizeStatus == StatusOfFinalize.Finalized || src.FinalizeStatus == StatusOfFinalize.AutoFinalized))
                .ForMember(dest => dest.HasComments, opt => opt.MapFrom(src => !string.IsNullOrWhiteSpace(src.ManagerComments)))
                .ForMember(dest => dest.HasTasks, opt => opt.MapFrom(src => src.RotationTasks != null && src.RotationTasks.Any(t => t.Enabled)))
                .ForMember(dest => dest.HasAttachments, opt => opt.MapFrom(src => src.Attachments != null && src.Attachments.Any()))
                .ForMember(dest => dest.HasVoiceMessages, opt => opt.MapFrom(src => src.VoiceMessages != null && src.VoiceMessages.Any()))
                .ForMember(dest => dest.HasLocation, opt => opt.MapFrom(src => src.LocationId.HasValue))
                .ForMember(dest => dest.SharedCounter, opt => opt.MapFrom(src => src.TopicSharingRelations != null && src.TopicSharingRelations.Any() ? src.TopicSharingRelations.Count : 0))
                .ForMember(dest => dest.IsSharingTopic, opt => opt.MapFrom(src => src.ShareSourceTopicId.HasValue))
                .ForMember(dest => dest.CarryforwardCounter, opt => opt.MapFrom(src => src.ForkParentTopicId.HasValue ? src.ForkParentTopic.ForkCounter : src.ForkCounter));

            cfg.CreateMap<CoreTopicViewModel, RotationTopic>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Name, opt => opt.Ignore())
                .ForMember(dest => dest.FinalizeStatus, opt => opt.MapFrom(src => src.IsFinalized ? StatusOfFinalize.Finalized : StatusOfFinalize.NotFinalized))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Notes));
        }
    }
}
