﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;
using System.Linq;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class SafetyMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            #region MajorHazard
            cfg.CreateMap<MajorHazard, MajorHazardViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.IconId, opt => opt.MapFrom(src => src.Icon.ImageId));

            cfg.CreateMap<MajorHazard, EditMajorHazardViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name));

            cfg.CreateMap<EditMajorHazardViewModel, MajorHazard>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name));
            #endregion

            #region CriticalControl

            cfg.CreateMap<CriticalControl, CriticalControlViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name));

            #endregion

            #region Safety Message

            cfg.CreateMap<SafetyMessage, EditSafetyMessageViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Message, opt => opt.MapFrom(src => src.Message))
                .ForMember(dest => dest.Date, opt => opt.MapFrom(src => src.Date))
                .ForMember(dest => dest.MajorHazard, opt => opt.MapFrom(src => src.CriticalControl.MajorHazardId))
                .ForMember(dest => dest.CriticalControl, opt => opt.MapFrom(src => src.CriticalControlId))
                .ForMember(dest => dest.Teams, opt => opt.MapFrom(src => src.Teams.Select(t => t.Id).ToList()));

            cfg.CreateMap<EditSafetyMessageViewModel, SafetyMessage>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Teams, opt => opt.Ignore())
                .ForMember(dest => dest.CriticalControl, opt => opt.Ignore())
                .ForMember(dest => dest.Message, opt => opt.MapFrom(src => src.Message))
                .ForMember(dest => dest.Date, opt => opt.MapFrom(src => src.Date))
                .ForMember(dest => dest.CriticalControlId, opt => opt.MapFrom(src => src.CriticalControl));

            cfg.CreateMap<SafetyMessage, SafetyMessageViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Message, opt => opt.MapFrom(src => src.Message))
                .ForMember(dest => dest.MajorHazardName, opt => opt.MapFrom(src => src.CriticalControl.MajorHazard.Name))
                .ForMember(dest => dest.CriticalControlName, opt => opt.MapFrom(src => src.CriticalControl.Name))
                .ForMember(dest => dest.IconId, opt => opt.MapFrom(src => src.CriticalControl.MajorHazard.Icon.ImageId));

            #endregion

            #region Icon

            //cfg.CreateMap<BankOfIconItem, IconViewModel>()
            //    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            //    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
            //    .ForMember(dest => dest.ImageId, opt => opt.MapFrom(src => src.Image.Id));

            //cfg.CreateMap<IconViewModel, BankOfIconItem>()
            //    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            //    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
            //    .ForMember(dest => dest.Image.Id, opt => opt.MapFrom(src => src.ImageId));

            #endregion
        }
    }
}
