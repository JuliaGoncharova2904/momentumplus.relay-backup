﻿
using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class DropDownListMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<UserProfile, DropDownListItemModel>()
                    .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.FullName))
                    .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id));

        }
    }
}
