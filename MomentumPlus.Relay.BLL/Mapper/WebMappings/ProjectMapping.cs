﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.EntityExtensions;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.Models;
using System;
using System.Linq;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class ProjectMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            //---------------------------------------------------------------------------------
            cfg.CreateMap<Project, ProjectDialogViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.StartDate))
                .ForMember(dest => dest.EndDate, opt => opt.MapFrom(src => src.EndDate))
                .ForMember(dest => dest.ProjectStatus, opt => opt.MapFrom(src => src.ProjectStatus().GetEnumDescription()))
                .ForMember(dest => dest.ProjectManagerId, opt => opt.MapFrom(src => src.ProjectManagerId));

            cfg.CreateMap<ProjectDialogViewModel, Project>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.StartDate))
                .ForMember(dest => dest.EndDate, opt => opt.MapFrom(src => src.EndDate))
                .ForMember(dest => dest.ProjectManagerId, opt => opt.MapFrom(src => src.ProjectManagerId));
            //---------------------------------------------------------------------------------
            cfg.CreateMap<Project, ProjectSlideViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.ProjectStatus, opt => opt.MapFrom(src => src.ProjectStatus().GetEnumDescription()));
            //---------------------------------------------------------------------------------
            cfg.CreateMap<Project, ProjectFullSlideViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.ActivePeriod, opt => opt.MapFrom(src =>
                    string.Format("{0} {1:MMM} {1:yyyy} - {2} {3:MMM} {3:yyyy}", src.StartDate.DayFormat(), src.StartDate, src.EndDate.DayFormat(), src.EndDate)
                ))
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.Creator.FullName))
                .ForMember(dest => dest.CreatedOn, opt => opt.MapFrom(src => string.Format("{1:ddd} {0} {1:MMM} {1:yyyy}", src.CreatedUtc.Value.DayFormat(), src.CreatedUtc.Value)))
                .ForMember(dest => dest.ProjectManager, opt => opt.MapFrom(src => src.ProjectManager.FullName))
                .ForMember(dest => dest.ProjectStatus, opt => opt.MapFrom(src => src.ProjectStatus().GetEnumDescription()));
            //---------------------------------------------------------------------------------
        }
    }
}
