﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class SharingTopicRelationMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<RotationTopicSharingRelation, TopicRelationViewModel>()
            .ForMember(dest => dest.TopicRelationId, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.RecipientId, opt => opt.MapFrom(src => src.RecipientId))
            .ForMember(dest => dest.SourceTopicId, opt => opt.MapFrom(src => src.SourceTopicId))
            .ForMember(dest => dest.DestinationTopicId, opt => opt.MapFrom(src => src.DestinationTopicId))
            .ForMember(dest => dest.RecipientTitle, opt => opt.MapFrom(src => src.RecipientId.HasValue ? src.Recipient.FullName : src.RecipientEmail))
            .ForMember(dest => dest.RecipientPosition, opt => opt.MapFrom(src => src.RecipientId.HasValue ? src.Recipient.Position.Name : "n/a"));
        }
    }
}
