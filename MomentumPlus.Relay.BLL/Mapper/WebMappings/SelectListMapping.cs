﻿using System;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Core.Authorization.Models;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class SelectListMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<Workplace, SelectListItem>()
                  .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name))
                  .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id));

            cfg.CreateMap<Template, SelectListItem>()
                    .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name))
                    .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id));

            cfg.CreateMap<RotationPattern, SelectListItem>()
                    .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name))
                    .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id));

            cfg.CreateMap<Team, SelectListItem>()
                    .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name))
                    .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id));

            cfg.CreateMap<ApplicationRole, SelectListItem>()
                    .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name))
                    .ForMember(dest => dest.Value, opt => opt.MapFrom(src => (Guid)src.Id));

            cfg.CreateMap<Position, SelectListItem>()
                    .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name + " - " + src.Workplace.Name))
                    .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id));

            cfg.CreateMap<Company, SelectListItem>()
                    .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name))
                    .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id));

            cfg.CreateMap<UserProfile, SelectListItem>()
                    .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.FullName))
                    .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id));

            cfg.CreateMap<Project, SelectListItem>()
                    .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name))
                    .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id));

            cfg.CreateMap<Rotation, SelectListItem>()
                    .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.RotationOwner.FullName))
                    .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id));

            cfg.CreateMap<RotationTopicGroup, SelectListItem>()
                    .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name))
                    .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id));

            cfg.CreateMap<Template, SelectListItem>()
                    .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name))
                    .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id));

            cfg.CreateMap<MajorHazard, SelectListItem>()
                    .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name))
                    .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id));

            cfg.CreateMap<CriticalControl, SelectListItem>()
                    .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name))
                    .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id));

            cfg.CreateMap<SafetyMessage, SelectListItem>()
                  .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.CriticalControl.Name))
                  .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id));
        }
    }
}
