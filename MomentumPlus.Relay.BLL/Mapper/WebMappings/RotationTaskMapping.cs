﻿using System;
using System.Linq;
using AutoMapper;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class RotationTaskMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            //-------------------------------------------------------------------------
            cfg.CreateMap<RotationTask, RotationTaskViewModel>()
                .ForMember(dest => dest.Section, opt => opt.MapFrom(src => src.RotationTopic.RotationTopicGroup.RotationModule.Type.GetEnumDescription()))
                .ForMember(dest => dest.Notes, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.SendingOption, opt => opt.MapFrom(src =>
                    (src.Status == StatusOfTask.Now || src.Status == StatusOfTask.NewNow) ? TaskSendingOptions.Now : TaskSendingOptions.EndOfSwing
                ))
                .ForMember(dest => dest.AssignedTo, opt => opt.MapFrom(src => src.AssignedToId))
                .ForMember(dest => dest.Tags, opt => opt.MapFrom(src => src.SearchTags.Split(',')))
                .ForMember(dest => dest.IsFeedbackRequired, opt => opt.MapFrom(src => src.IsFeedbackRequired))
                .ForMember(dest => dest.AttacmentsCounter, opt => opt.MapFrom(src => src.Attachments.Count))
                .ForMember(dest => dest.VoiceMessagesCounter, opt => opt.MapFrom(src => src.VoiceMessages.Count))
                .ForMember(dest => dest.HasNewAttacments, opt => opt.MapFrom(src => src.Attachments.Any(a => a.CreatedUtc > DateTime.UtcNow.AddDays(-2))))
                .ForMember(dest => dest.HasNewVoiceMessages, opt => opt.MapFrom(src => src.VoiceMessages.Any(v => v.CreatedUtc > DateTime.UtcNow.AddDays(-2))))
                .ForMember(dest => dest.CompleteStatus, opt => opt.MapFrom(src => src.IsComplete ? "Complete" : "Incomplete"));

            cfg.CreateMap<RotationTaskViewModel, RotationTask>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.AssignedTo, opt => opt.Ignore())
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => (src.SendingOption == TaskSendingOptions.Now) ? StatusOfTask.Now : StatusOfTask.Default))
                .ForMember(dest => dest.AssignedToId, opt => opt.MapFrom(src => src.AssignedTo))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Notes))
                .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => src.Priority))
                .ForMember(dest => dest.SearchTags, opt => opt.MapFrom(src => string.Join(",", src.Tags)));
            //-------------------------------------------------------------------------
            cfg.CreateMap<RotationTask, RotationTaskDetailsViewModel>()
                .ForMember(dest => dest.Section, opt => opt.MapFrom(src => src.RotationTopic.RotationTopicGroup.RotationModule.Type.GetEnumDescription()))
                .ForMember(dest => dest.TaskNotes, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.SendingOption, opt => opt.MapFrom(src =>
                    (src.Status == StatusOfTask.Now || src.Status == StatusOfTask.NewNow) ? TaskSendingOptions.Now : TaskSendingOptions.EndOfSwing
                ))
                .ForMember(dest => dest.Feedback, opt => opt.MapFrom(src => src.SuccessorTask != null
                                                                            ? src.SuccessorTask.Feedback
                                                                            : src.Feedback))
                .ForMember(dest => dest.TopicId, opt => opt.MapFrom(src => src.RotationTopicId))
                .ForMember(dest => dest.AssignedFrom, opt => opt.ResolveUsing(src => {
                    if (src.AncestorTask != null)
                    {
                        return GetRotationModuleOwnerId(src.AncestorTask.RotationTopic.RotationTopicGroup.RotationModule);
                    }
                    else
                    {
                        if (src.RotationTopic.AncestorTopic != null)
                        {
                            return GetRotationModuleOwnerId(src.RotationTopic.AncestorTopic.RotationTopicGroup.RotationModule);
                        }
                        else
                        {
                            return GetRotationModuleOwnerId(src.RotationTopic.RotationTopicGroup.RotationModule);
                        }
                    }
                }))
                .ForMember(dest => dest.IsFeedbackRequired, opt => opt.MapFrom(src => src.IsFeedbackRequired))
                .ForMember(dest => dest.AttacmentsCounter, opt => opt.MapFrom(src => src.Attachments.Count ))
                .ForMember(dest => dest.VoiceMessagesCounter, opt => opt.MapFrom(src => src.VoiceMessages.Count))
                .ForMember(dest => dest.HasNewAttacments, opt => opt.MapFrom(src => src.Attachments.Any(a => a.CreatedUtc > DateTime.UtcNow.AddDays(-2))))
                .ForMember(dest => dest.HasNewVoiceMessages, opt => opt.MapFrom(src => src.VoiceMessages.Any(v => v.CreatedUtc > DateTime.UtcNow.AddDays(-2))))
                .ForMember(dest => dest.Tags, opt => opt.MapFrom(src => src.SearchTags.Split(',')));
            //-------------------------------------------------------------------------
        }

        public static Guid? GetRotationModuleOwnerId(RotationModule rotationModule)
        {
            Guid? assignedFrom = null;

            if (rotationModule.RotationId.HasValue)
            {
                assignedFrom = rotationModule.Rotation.RotationOwnerId;
            }
            else if (rotationModule.ShiftId.HasValue)
            {
                assignedFrom = rotationModule.Shift.Rotation.RotationOwnerId;
            }

            return assignedFrom;
        }
    }
}
