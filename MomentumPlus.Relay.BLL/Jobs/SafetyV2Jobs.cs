﻿using MomentumPlus.Relay.BLL.Jobs.Base;
using System;
using MomentumPlus.Relay.BLL.LogicCore;

namespace MomentumPlus.Relay.BLL.Jobs
{
    /// <summary>
    /// Jobs for processing information of Projects.
    /// </summary>
    public class SafetyV2Jobs : JobsBase
    {
        public SafetyV2Jobs(LogicCoreUnitOfWork logicCore) : base(logicCore)
        { }

        /// <summary>
        /// Job => is finding safety messages for this day and generate stats.
        /// </summary>
        public void GenerateSafetyStatsJob()
        {
            var safetyMessages = _logicCore.SafetyMessageStatV2Core.GetActiveNotProcessedSafetyMessages();

            foreach (var safetyMessage in safetyMessages)
            {
                _logicCore.SafetyMessageStatV2Core.CreateSafetyStatsForMessage(safetyMessage, false);
            }

            _logicCore.SyncWithDatabase();
        }

      
    }
}
