﻿using MomentumPlus.Relay.BLL.Jobs.Base;
using System;
using System.Linq;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Core.Models;
using System.Collections.Generic;
using System.Globalization;

namespace MomentumPlus.Relay.BLL.Jobs
{
    public class RotationJobs : JobsBase
    {

        public RotationJobs(LogicCoreUnitOfWork logicCore) : base(logicCore)
        { }

        /// <summary>
        /// Job => is send report to back to back when swing is end
        /// </summary>
        public void HandoverTriggerJob()
        {
            var now = DateTime.UtcNow;

            var stringTime = now.ToString("hh:mmtt");

            var resultTime = DateTime.ParseExact(stringTime, "hh:mmtt", CultureInfo.InvariantCulture);

            var resultTimeTimeOfDay = resultTime.TimeOfDay;

            DateTime handoverTriggerTime = _logicCore.AdministrationCore.GetHandoverTriggerTime();

            if (handoverTriggerTime != null)
            {
                var handoverPreviewTimeTimeOfDay = handoverTriggerTime.TimeOfDay;

                if (handoverTriggerTime.TimeOfDay == resultTimeTimeOfDay)
                {
                    NotificationJobs notificationJobs = new NotificationJobs(_logicCore);
                    DateTime currentDate = DateTime.Today;
                    IEnumerable<Rotation> rotationsWithEndedSwing = _logicCore.RotationCore.GetRotationsByState(RotationState.Confirmed).ToList()
                                                                                .Where(r => r.StartDate.Value.AddDays(r.DayOn - 1) <= currentDate);

                    foreach (Rotation rotation in rotationsWithEndedSwing)
                    {
                        _logicCore.RotationCore.AutoFinalizeAll(rotation, false);
                        _logicCore.RotationCore.SetRotationState(rotation, RotationState.SwingEnded, false);
                        _logicCore.SyncWithDatabase();

                        if (rotation.RotationType == RotationType.Swing)
                        {
                            _logicCore.RotationCore.HandoverReportFromRotation(rotation);
                        }

                        if (rotation.RotationType == RotationType.Shift)
                        {
                            _logicCore.RotationCore.EndAllRotationShifts(rotation);
                        }

                        _logicCore._mailerService.SendRotationShareReportEmail(rotation.Id);

                        notificationJobs.EndOfSwingNotificationSubJob(rotation);
                    }
                }
            }
        }

        /// <summary>
        /// Job => expire rotations
        /// </summary>
        public void ExpireRotationsJob()
        {
            DateTime currentDate = DateTime.Today;
            IEnumerable<Rotation> exparedRotations = _logicCore.RotationCore.GetRotationsByState(RotationState.SwingEnded).ToList()
                                                                        .Where(r => r.StartDate.Value.AddDays(r.DayOn + r.DayOff) <= currentDate);

            foreach(Rotation rotation in exparedRotations)
            {
                _logicCore.RotationCore.SetRotationState(rotation, RotationState.Expired);
            }
        }

    }
}
