﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Jobs.Base;
using MomentumPlus.Relay.BLL.LogicCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MomentumPlus.Relay.BLL.Jobs
{
    public class NotificationJobs : JobsBase
    {
        public NotificationJobs(LogicCoreUnitOfWork logicCore) : base(logicCore)
        { }

        /// <summary>
        /// 
        /// </summary>
        public void PenultimateDayOfSwingJob()
        {
            IEnumerable<Rotation> rotations = _logicCore.RotationCore.RotationsWithPenultimateDayOfSwingByDate(DateTime.Today);

            foreach (Rotation rotation in rotations)
            {
                Guid recipientId = rotation.RotationOwnerId;
                //---- 1 ----
                _logicCore.NotificationCore.NotificationTrigger.Send_PossibilityToExtendSwing(recipientId, rotation.RotationOwnerId);
                //---- 2 ----
                int notComplitedHandbackTasks = rotation.RotationModules.Where(rm => rm.SourceType == TypeOfModuleSource.Received)
                                                                        .SelectMany(rm => rm.RotationTopicGroups)
                                                                        .SelectMany(rtg => rtg.RotationTopics)
                                                                        .SelectMany(rt => rt.RotationTasks)
                                                                        .Count(rt => !rt.IsComplete && rt.IsFeedbackRequired);
                if (notComplitedHandbackTasks > 0)
                {
                    _logicCore.NotificationCore.NotificationTrigger.Send_HandbackTasksStillComplete(recipientId, notComplitedHandbackTasks, rotation.Id);
                }
                //---- 3 ----
                int notComplitedTasks = rotation.RotationModules.Where(rm => rm.SourceType == TypeOfModuleSource.Received)
                                                                .SelectMany(rm => rm.RotationTopicGroups)
                                                                .SelectMany(rtg => rtg.RotationTopics)
                                                                .SelectMany(rt => rt.RotationTasks)
                                                                .Count(rt => !rt.IsComplete && !rt.IsFeedbackRequired);
                if (notComplitedTasks > 0)
                {
                    _logicCore.NotificationCore.NotificationTrigger.Send_TasksStillComplete(recipientId, notComplitedTasks, rotation.Id);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void EndOfSwingNotificationSubJob(Rotation rotation)
        {
            IEnumerable<RotationTask> assignedTasks = _logicCore.RotationModuleCore.GetRotationModules(rotation.Id, TypeOfModuleSource.Draft)
                                                                                            .Where(rm => rm.Enabled)
                                                                                            .SelectMany(rm => rm.RotationTopicGroups)
                                                                                            .SelectMany(rtg => rtg.RotationTopics)
                                                                                            .SelectMany(rt => rt.RotationTasks)
                                                                                            .Where(t => t.Enabled && t.AssignedToId.HasValue)
                                                                                            .ToList();

            IEnumerable<Guid> recipients = assignedTasks.Select(t => t.AssignedToId.Value).Distinct();

            foreach (Guid recipientId in recipients)
            {
                //---- 5 ----
                int assignedTasksNumber = assignedTasks.Count(t => t.AssignedToId == recipientId && !t.IsFeedbackRequired);
                if (assignedTasksNumber > 0)
                {
                    _logicCore.NotificationCore.NotificationTrigger.Send_AssignedTasks(recipientId, assignedTasksNumber);
                }
                //---- 6 ----
                int assignedHandbackTasksNumber = assignedTasks.Count(t => t.AssignedToId == recipientId && t.IsFeedbackRequired);
                if (assignedHandbackTasksNumber > 0)
                {
                    _logicCore.NotificationCore.NotificationTrigger.Send_AssignedHandbackTasks(recipientId, assignedHandbackTasksNumber);
                }
            }


            _logicCore.NotificationCore.NotificationTrigger.Send_TeamMemberHandedOver(rotation.LineManagerId,
                                                                                        rotation.RotationOwner.FullName,
                                                                                        rotation.DefaultBackToBack.FullName,
                                                                                        rotation.RotationOwnerId);
        }

        /// <summary>
        /// 
        /// </summary>
        public void TaskBoardTaskIsNotCompletedJob()
        {
            DateTime tomorrow = DateTime.Today.AddDays(1);
            List<RotationTask> tasks = _logicCore.TaskBoardCore.GetTasksByType(TaskBoardTaskType.Received)
                                                                .Where(t => !t.IsComplete && t.Deadline == tomorrow)
                                                                .ToList();
            tasks.AddRange(_logicCore.TaskBoardCore.GetTasksByType(TaskBoardTaskType.MyTask)
                                                                .Where(t => !t.IsComplete && t.Deadline == tomorrow)
                                                                .ToList());

            foreach(RotationTask task in tasks)
            {
                UserProfile recipient = task.TaskBoard?.User;
                if (recipient == null) continue;

                //---- notification 12 ----
                _logicCore.NotificationCore.NotificationTrigger.Send_YouHaveNowTaskToComplete(recipient.Id, task.Id);
                //---- notification 11 ----
                for(RotationTask parentTask = task.AncestorTask; parentTask != null; parentTask = parentTask.AncestorTask)
                {
                    _logicCore.NotificationCore.NotificationTrigger.Send_YourNowTaskHasNotCompleted(parentTask.TaskBoardId.Value, recipient.FullName, parentTask.Id);
                }
                //-------------------------
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public void NowTaskIsNotCompletedJob()
        {
            DateTime penultimateDayOfDedline = DateTime.Today.AddDays(1);
            IEnumerable<RotationTask> tasks = _logicCore.RotationTaskCore.GetActiveReceivedNowTasks()
                                                                        .ToList()
                                                                        .Where(t => !t.IsComplete && t.Deadline == penultimateDayOfDedline);

            foreach (RotationTask task in tasks)
            {
                Guid ownerId = task.AncestorTask.RotationTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId;
                Guid recipientId = task.RotationTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId;
                string recipientName = task.RotationTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwner.FullName;
                //---- notification 11 ----
                _logicCore.NotificationCore.NotificationTrigger.Send_YourNowTaskHasNotCompleted(ownerId, recipientName, task.AncestorTaskId.Value);
                //---- notification 12 ----
                _logicCore.NotificationCore.NotificationTrigger.Send_YouHaveNowTaskToComplete(recipientId, task.Id);
                //-------------------------
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public void ShiftOverSoonJob()
        {
            DateTime currentDateTime = DateTime.Now;

            IEnumerable<Shift> shifts = _logicCore.ShiftCore.GetShiftsByState(ShiftState.Confirmed)
                                                            .ToList()
                                                            .Where(s =>
                                                            {
                                                                DateTime endShiftTime = s.StartDateTime.Value.AddMinutes(s.WorkMinutes);

                                                                return endShiftTime.AddMinutes(120) > currentDateTime && endShiftTime.AddMinutes(90) < currentDateTime;
                                                            });

            foreach (Shift shift in shifts)
            {
                _logicCore.NotificationCore.NotificationTrigger.Send_PossibilityToExtendShift(shift.Rotation.RotationOwnerId);
            }

        }


    }
}
