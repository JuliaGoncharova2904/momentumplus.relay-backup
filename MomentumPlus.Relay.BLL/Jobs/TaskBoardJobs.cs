﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Jobs.Base;
using MomentumPlus.Relay.BLL.LogicCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MomentumPlus.Relay.BLL.Jobs
{
    public class TaskBoardJobs : JobsBase
    {
        public TaskBoardJobs(LogicCoreUnitOfWork logicCore) : base(logicCore)
        { }

        public void HandowerPandingTasksJob()
        {
            IEnumerable<RotationTask> pendingTasksForHandover = _logicCore.TaskBoardCore.GetTasksByType(TaskBoardTaskType.Pending)
                                                                                        .Where(t => t.DeferredHandoverTime <= DateTime.Today)
                                                                                        .ToList();
            foreach(RotationTask task in pendingTasksForHandover)
            {
                _logicCore.TaskBoardCore.HandoverTask(task, false);
            }

            _logicCore.SyncWithDatabase();
        }
    }
}
