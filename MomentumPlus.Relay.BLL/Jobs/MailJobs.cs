﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Jobs.Base;
using MomentumPlus.Relay.BLL.LogicCore;
using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Relay.Interfaces;
using System.Globalization;

namespace MomentumPlus.Relay.BLL.Jobs
{
    public class MailJobs : JobsBase
    {
        public MailJobs(LogicCoreUnitOfWork logicCore) : base(logicCore)
        {
        }

        /// <summary>
        /// Job => is send Handover Preview Mail to Default B2b of Rotation
        /// </summary>
        public void HandoverPreviewMailJob()
        {
            var now = DateTime.UtcNow;

            var stringTime = now.ToString("hh:mmtt");

            var resultTime = DateTime.ParseExact(stringTime, "hh:mmtt", CultureInfo.InvariantCulture);

            var resultTimeTimeOfDay = resultTime.TimeOfDay;

            var handoverPreviewTime = _logicCore.AdministrationCore.GetHandoverPreviewTime();
            
            if (handoverPreviewTime.HasValue)
            {
                if (handoverPreviewTime.Value.TimeOfDay == resultTimeTimeOfDay)
                {
                    IEnumerable<Rotation> rotations = _logicCore.RotationCore.RotationsWithPenultimateDayOfSwingByDate(DateTime.Today);

                    foreach (Rotation rotation in rotations)
                    {
                        _logicCore._mailerService.SendHandoverPreviewEmail(rotation.Id);
                    }
                }
            }
        }

        /// <summary>
        /// Job => is send Shift Handover Preview Mail to Recipient
        /// </summary>
        public void HandoverShiftPreviewMailJob()
        {
            DateTime currentDateTime = DateTime.Now.AddMinutes(60);

            IEnumerable<Shift> breakShifts = _logicCore.ShiftCore.GetShiftsByState(ShiftState.Confirmed).ToList()
                                                        .Where(s => s.StartDateTime.Value.AddMinutes(s.WorkMinutes) <= currentDateTime &&
                                                                    s.StartDateTime.Value.AddMinutes(s.WorkMinutes + 5) > currentDateTime);

            foreach (Shift shift in breakShifts)
            {
                _logicCore._mailerService.SendShiftHandoverPreviewEmail(shift.Id);
            }
        }
    }
}
