﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Jobs.Base;
using MomentumPlus.Relay.BLL.LogicCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MomentumPlus.Relay.BLL.Jobs
{
    public class ShiftJobs : JobsBase
    {
        public ShiftJobs(LogicCoreUnitOfWork logicCore) : base(logicCore)
        { }

        /// <summary>
        /// Job => is send report to recipient when shift is end
        /// </summary>
        public void HandoverTriggerJob()
        {
            DateTime currentDateTime = DateTime.Now;

            IEnumerable<Shift> breakShifts = _logicCore.ShiftCore.GetShiftsByState(ShiftState.Confirmed).ToList()
                                                        .Where(s => s.StartDateTime.Value.AddMinutes(s.WorkMinutes + 60) <= currentDateTime);

            foreach (Shift shift in breakShifts)
            {
                _logicCore.ShiftCore.AutoFinalizeAll(shift, false);
                _logicCore.ShiftCore.SetShiftState(shift, ShiftState.Break, false);
                _logicCore.SyncWithDatabase();

                _logicCore.ShiftCore.HandoverReportFromShift(shift);
            }
        }

        /// <summary>
        /// Job => Expire shifts
        /// </summary>
        public void FinishShiftsJob()
        {
            DateTime currentDateTime = DateTime.Now;

            IEnumerable<Shift> expiredShifts = _logicCore.ShiftCore.GetShiftsByState(ShiftState.Break).ToList()
                                                        .Where(s => s.StartDateTime.Value.AddMinutes(s.WorkMinutes + s.BreakMinutes) <= currentDateTime);

            foreach (Shift shift in expiredShifts)
            {
                _logicCore.ShiftCore.SetShiftState(shift, ShiftState.Finished, false);
            }

            _logicCore.SyncWithDatabase();
        }

    }
}
