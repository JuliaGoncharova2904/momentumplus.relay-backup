﻿using MomentumPlus.Relay.BLL.LogicCore;

namespace MomentumPlus.Relay.BLL.Jobs.Base
{
    public abstract class JobsBase
    {
        protected LogicCoreUnitOfWork _logicCore;

        public JobsBase(LogicCoreUnitOfWork logicCore)
        {
            this._logicCore = logicCore;
        }
    }
}
