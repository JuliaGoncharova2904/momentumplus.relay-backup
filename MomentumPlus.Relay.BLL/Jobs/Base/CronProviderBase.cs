﻿using MomentumPlus.Relay.BLL.LogicCore;

namespace MomentumPlus.Relay.BLL.Jobs.Base
{
    public abstract class CronProviderBase
    {
        protected LogicCoreUnitOfWork _logicCore;

        public CronProviderBase(LogicCoreUnitOfWork logicCore)
        {
            this._logicCore = logicCore;
        }
    }
}
