﻿using MomentumPlus.Relay.BLL.Jobs.Base;
using System;
using MomentumPlus.Relay.BLL.LogicCore;

namespace MomentumPlus.Relay.BLL.Jobs
{
    public class CronProviders : CronProviderBase
    {
        public CronProviders(LogicCoreUnitOfWork logicCore) : base(logicCore)
        { }

        public string GetHandoverTriggerTime()
        {
            DateTime handoverTriggerTime = _logicCore.AdministrationCore.GetHandoverTriggerTime();

            return string.Format("{0} {1} * * *", handoverTriggerTime.Minute, handoverTriggerTime.Hour);
        }


        public string GetHandoverPreviewTime()
        {
            var handoverPreviewTime = _logicCore.AdministrationCore.GetHandoverPreviewTime();

            if (handoverPreviewTime.HasValue)
            {
                return string.Format("{0} {1} * * *", handoverPreviewTime.Value.Minute, handoverPreviewTime.Value.Hour);
            }

            return null;
        }
    }
}
