﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Services.Api;
using MomentumPlus.Relay.Interfaces.ApiServices;
using INotificationService = MomentumPlus.Relay.Interfaces.ApiServices.INotificationService;

namespace MomentumPlus.Relay.BLL.Services
{
    public class ApiServicesUnitOfWork : IApiServicesUnitOfWork
    {
        private readonly IRepositoriesUnitOfWork _repositories;

        private readonly LogicCoreUnitOfWork _logicCore;

        private readonly IMailerService _mailerService;

        private readonly INotificationAdapter _notificationAdapter;

        private IAccountService _accountService;

        private IHomeService _homeService;

        private INotificationService _notificationService;

        private IProfileService _profileService;

        public ApiServicesUnitOfWork()
        { }

        public ApiServicesUnitOfWork(IRepositoriesUnitOfWork repositoriesUnitOfWork, INotificationAdapter notificationAdapter, IMailerService mailerService)
        {
            this._repositories = repositoriesUnitOfWork;
            this._notificationAdapter = notificationAdapter;
            this._logicCore = new LogicCoreUnitOfWork(repositoriesUnitOfWork, notificationAdapter, mailerService);
            this._mailerService = mailerService;
        }

        #region Properties

        public IAccountService AccountService
        {
            get
            {
                return _accountService ?? (_accountService = new AccountService(this._logicCore));
            }
        }

        public IHomeService HomeService
        {
            get
            {
                return _homeService ?? (_homeService = new HomeService(this._logicCore));
            }
        }

        public INotificationService NotificationService
        {
            get
            {
                return _notificationService ?? (_notificationService = new NotificationService(this._logicCore));
            }
        }

        public IProfileService ProfileService
        {
            get
            {
                return _profileService ?? (_profileService = new ProfileService(this._logicCore));
            }
        }

        #endregion
    }
}
