﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.BLL.Services.Web;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.LogicCore;

namespace MomentumPlus.Relay.BLL.Services
{
    public class ServicesUnitOfWork : IServicesUnitOfWork
    {
        private readonly IRepositoriesUnitOfWork _repositories;

        private readonly INotificationAdapter _notificationAdapter;

        private readonly LogicCoreUnitOfWork _logicCore;

        private readonly IMailerService _mailerService;

        private IUserService _userService;

        private ITeamService _teamService;

        private IRotationService _rotationService;

        private ISiteService _siteService;

        private IPositionService _positionService;

        private IMediaService _mediaService;

        private IAttachmentService _attachmentService;

        private IVoiceMessageService _voiceMessageService;

        private IRotationPatternService _rotationPatternService;

        private IDailyNoteService _dailyNoteService;

        private IModuleService _moduleService;

        private ITopicGroupService _topicGroupService;

        private ITopicService _topicService;

        private ITaskService _taskService;

        private IProjectService _projectService;

        private ITemplateService _templateService;

        private IRotationModuleService _rotationModuleService;

        private IRotationTopicGroupService _rotationTopicGroupService;

        private IRotationTopicService _rotationTopicService;

        private IRotationTaskService _rotationTaskService;

        private IReportService _reportService;

        private ILocationService _locationService;
        // safety
        private IMajorHazardService _majorHazardService;

        private ICriticalControlService _criticalControlService;

        private ISafetyMessageService _safetyMessageService;
        // safety v2
        private IMajorHazardV2Service _majorHazardV2Service;

        private ICriticalControlV2Service _criticalControlV2Service;

        private ISafetyMessageV2Service _safetyMessageV2Service;

        private ISafetyStatV2Service _safetyStatV2Service;
        //
        private IAdministrationService _administrationService;

        private IAccessService _accessService;

        private ITimelineService _timelineService;

        private INotificationService _notificationService;

        private IShiftService _shiftService;

        private IDailyReportService _dailyReportService;

        private ISchedulerService _schedulerService;

        private IChecklistService _checklistService;

        private IGlobalReportsService _globalReportService;

        private ISharingReportService _sharingReportService;

        private ISharingTopicService _sharingTopicService;


        private ICompanyService _companyService;

        private ITaskBoardService _taskBoardService;
        private ITeamRotationsService _teamRotationsService;


        private ITaskLogService _taskLogService;

        // Topic Search 
        private ITopicSearchService _topicSearchService;


        public ServicesUnitOfWork(IRepositoriesUnitOfWork repositoriesUnitOfWork, INotificationAdapter notificationAdapter, IMailerService mailerService)
        {
            this._repositories = repositoriesUnitOfWork;
            this._notificationAdapter = notificationAdapter;
            this._logicCore = new LogicCoreUnitOfWork(repositoriesUnitOfWork, notificationAdapter, mailerService);
            this._mailerService = mailerService;
        }

        #region Properties

        public ITopicSearchService TopicSearchService
        {
            get
            {
                return _topicSearchService ?? (_topicSearchService = new TopicSearchService(this._repositories, this, _logicCore));
            }
        }



        public ITaskLogService TaskLogService
        {
            get
            {
                return _taskLogService ?? (_taskLogService = new TaskLogService(this._repositories, _logicCore));
            }
        }



        public ICompanyService CompanyService
        {
            get
            {
                return _companyService ?? (_companyService = new CompanyService(this._repositories, this));
            }
        }

        public IDailyReportService DailyReportService
        {
            get
            {
                return _dailyReportService ?? (_dailyReportService = new DailyReportService(this._repositories, this, _logicCore));
            }
        }

        public ISharingTopicService SharingTopicService
        {
            get
            {
                return _sharingTopicService ?? (_sharingTopicService = new SharingTopicService(this._logicCore));
            }
        }


        public IAdministrationService AdministrationService
        {
            get
            {
                return _administrationService ?? (_administrationService = new AdministrationService(this._repositories, this, _logicCore, _mailerService));
            }
        }


        public IRotationModuleService RotationModuleService
        {
            get
            {
                return _rotationModuleService ?? (_rotationModuleService = new RotationModuleService(this._repositories, this, _logicCore));
            }
        }

        public IRotationTopicGroupService RotationTopicGroupService
        {
            get
            {
                return _rotationTopicGroupService ?? (_rotationTopicGroupService = new RotationTopicGroupService(this._repositories, this, _logicCore));
            }
        }

        public IRotationTopicService RotationTopicService
        {
            get
            {
                return _rotationTopicService ?? (_rotationTopicService = new RotationTopicService(this._repositories, this, _logicCore));
            }
        }

        public IRotationTaskService RotationTaskService
        {
            get
            {
                return _rotationTaskService ?? (_rotationTaskService = new RotationTaskService(this._repositories, this, _logicCore));
            }
        }

        public ITemplateService TemplateService
        {
            get
            {
                return _templateService ?? (_templateService = new TemplateService(this._repositories, this, _logicCore));
            }
        }

        public IProjectService ProjectService
        {
            get
            {
                return _projectService ?? (_projectService = new ProjectService(this._repositories, this, this._logicCore));
            }
        }


        public ITopicGroupService TopicGroupService
        {
            get
            {
                return _topicGroupService ?? (_topicGroupService = new TopicGroupService(this._repositories, this, _logicCore));
            }
        }

        public ITopicService TopicService
        {
            get
            {
                return _topicService ?? (_topicService = new TopicService(this._repositories, this, _logicCore));
            }
        }

        public ITaskService TaskService
        {
            get
            {
                return _taskService ?? (_taskService = new TaskService(this._repositories, this, _logicCore));
            }
        }


        public IUserService UserService
        {
            get
            {
                return _userService ?? (_userService = new UserService(this._repositories, this, _logicCore, _mailerService));
            }
        }


        public ITeamService TeamService
        {
            get
            {
                return _teamService ?? (_teamService = new TeamService(this._repositories, this, _logicCore));
            }
        }

        public IRotationService RotationService
        {
            get
            {
                return _rotationService ?? (_rotationService = new RotationService(this._repositories, this, this._logicCore));
            }
        }

        public ISiteService SiteService
        {
            get
            {
                return _siteService ?? (_siteService = new SiteService(this._repositories));
            }
        }

        public IPositionService PositionService
        {
            get
            {
                return _positionService ?? (_positionService = new PositionService(this._repositories, this, _logicCore));
            }
        }

        public IMediaService MediaService
        {
            get
            {
                return _mediaService ?? (_mediaService = new MediaService(this._repositories, this._logicCore));
            }
        }

        public IRotationPatternService RotationPatternService
        {
            get
            {
                return _rotationPatternService ?? (_rotationPatternService = new RotationPatternService(this._repositories));
            }
        }


        public IDailyNoteService DailyNoteService
        {
            get
            {
                return _dailyNoteService ?? (_dailyNoteService = new DailyNoteService(this._repositories, this, this._logicCore));
            }
        }

        public IModuleService ModuleService
        {
            get
            {
                return _moduleService ?? (_moduleService = new ModuleService(this._repositories, this, _logicCore));
            }
        }

        public ILocationService LocationService
        {
            get
            {
                return _locationService ?? (_locationService = new LocationService(this._repositories));
            }
        }

        public IAttachmentService AttachmentService
        {
            get
            {
                return _attachmentService ?? (_attachmentService = new AttachmentService(this._repositories, this._logicCore));
            }
        }

        public IReportService ReportService
        {
            get
            {
                return _reportService ?? (_reportService = new ReportService(this._repositories, this, this._logicCore));
            }
        }

        public IVoiceMessageService VoiceMessageService
        {
            get
            {
                return _voiceMessageService ?? (_voiceMessageService = new VoiceMessageService(this._repositories, this._logicCore));
            }
        }

        public IMajorHazardService MajorHazardService
        {
            get
            {
                return _majorHazardService ?? (_majorHazardService = new MajorHazardService(this._repositories, this, this._logicCore));
            }
        }

        public ICriticalControlService CriticalControlService
        {
            get
            {
                return _criticalControlService ?? (_criticalControlService = new CriticalControlService(this._repositories, this, this._logicCore));
            }
        }

        public ISafetyMessageService SafetyMessageService
        {
            get
            {
                return _safetyMessageService ?? (_safetyMessageService = new SafetyMessageService(this._repositories, this, this._logicCore));
            }
        }

        public IAccessService AccessService
        {
            get
            {
                return _accessService ?? (_accessService = new AccessService(this._repositories, this._logicCore));
            }
        }

        public ITimelineService TimelineService
        {
            get
            {
                return _timelineService ?? (_timelineService = new TimelineService(_repositories, this, _logicCore));
            }
        }

        public IMajorHazardV2Service MajorHazardV2Service
        {
            get
            {
                return _majorHazardV2Service ?? (_majorHazardV2Service = new MajorHazardV2Service(this._logicCore));
            }
        }

        public ICriticalControlV2Service CriticalControlV2Service
        {
            get
            {
                return _criticalControlV2Service ?? (_criticalControlV2Service = new CriticalControlV2Service(this._logicCore));
            }
        }

        public ISafetyMessageV2Service SafetyMessageV2Service
        {
            get
            {
                return _safetyMessageV2Service ?? (_safetyMessageV2Service = new SafetyMessageV2Service(this._logicCore));
            }
        }

        public ISafetyStatV2Service SafetyStatV2Service
        {
            get
            {
                return _safetyStatV2Service ?? (_safetyStatV2Service = new SafetyStatV2Service(this._logicCore));
            }
        }

        public INotificationService NotificationService
        {
            get
            {
                return _notificationService ?? (_notificationService = new NotificationService(this, _logicCore));
            }
        }



        public IShiftService ShiftService
        {
            get
            {
                return _shiftService ?? (_shiftService = new ShiftService(this._logicCore));
            }
        }

        public IChecklistService ChecklistService
        {
            get
            {
                return _checklistService ?? (_checklistService = new ChecklistService(this, this._logicCore));
            }
        }
        public ISchedulerService SchedulerService
        {
            get
            {
                return _schedulerService ?? (_schedulerService = new SchedulerService(this._logicCore));
            }
        }

        public IGlobalReportsService GlobalReportService
        {
            get
            {
                return _globalReportService ?? (_globalReportService = new GlobalReportsService(this._logicCore));
            }
        }

        public ISharingReportService SharingReportService
        {
            get
            {
                return _sharingReportService ?? (_sharingReportService = new SharingReportService(this, this._logicCore));
            }
        }


        public ITaskBoardService TaskBoardService
        {
            get
            {
                return _taskBoardService ?? (_taskBoardService = new TaskBoardService(this._logicCore));
            }
        }

        public ITeamRotationsService TeamRotationsService
        {
            get
            {
                return _teamRotationsService ?? (_teamRotationsService = new TeamRotationsService(this._logicCore));
            }
        }

        #endregion
    }
}
