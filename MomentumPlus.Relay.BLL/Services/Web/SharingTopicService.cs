﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class SharingTopicService : ISharingTopicService
    {
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;

        public SharingTopicService(LogicCoreUnitOfWork logicCore)
        {
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
        }

        /// <summary>
        /// Populate ShareTopicViewModel for Share topic dialog.
        /// </summary>
        /// <param name="topicId">Topic Id</param>
        /// <returns></returns>
        public ShareTopicViewModel PopulateShareTopicViewModel(Guid topicId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            var topicOwner = topic.RotationTopicGroup.RotationModule.Rotation != null
                                ? topic.RotationTopicGroup.RotationModule.Rotation.RotationOwner
                                : topic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwner;

            IQueryable<UserProfile> users = _logicCore.UserProfileCore.GetRelayEmployees().Where(u => u.CurrentRotationId.HasValue && u.Id != topicOwner.Id);

            var RotationReportSharingRelations = _logicCore.RotationTopicSharingRelationCore.GetRotationTopicSharingRelations(topicId).ToList().OrderBy(r => r.Recipient.FullName);

            var topicRelationsUserIds = RotationReportSharingRelations.Where(r => r.RecipientId.HasValue).Select(r => r.RecipientId.Value);

            var model = new ShareTopicViewModel
            {
                Recipients = _mapper.Map<IEnumerable<SelectListItem>>(users.Where(u => !topicRelationsUserIds.Contains(u.Id)).ToList().OrderBy(u => u.FullName)),
                TopicRelations = _mapper.Map<IEnumerable<TopicRelationViewModel>>(RotationReportSharingRelations)
            };

            return model;
        }

        /// <summary>
        /// Save sharing topic changes.
        /// </summary>
        /// <param name="model"></param>
        public void ChangeShareTopicRelations(ShareTopicViewModel model)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(model.TopicId);

            _logicCore.RotationTopicSharingRelationCore.ChangeShareTopicRelations(topic, model.RecipientsIds, model.DeleteTopicRelationsIds);
        }

        /// <summary>
        /// Remove sharing topic relation.
        /// </summary>
        /// <param name="topicRelationId">Relation Id</param>
        public void DeleteShareTopicRelation(Guid topicRelationId)
        {
            var shareTopicRelation = _logicCore.RotationTopicSharingRelationCore.GetRotationTopicSharingRelation(topicRelationId);

            _logicCore.RotationTopicSharingRelationCore.DeleteShareTopicRelation(shareTopicRelation);
        }

        /// <summary>
        /// Populate list model of topic recipients.
        /// </summary>
        /// <param name="topicId">Topic Id</param>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetShareTopicRecipients(Guid topicId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            var topicOwner = topic.RotationTopicGroup.RotationModule.Rotation.RotationOwner;

            var topicOwnerTeamMembers = topicOwner.Team.Users.Where(u => u.Id != topicOwner.Id && u.CurrentRotationId.HasValue && !u.DeletedUtc.HasValue);

            var RotationReportSharingRelations = _logicCore.RotationTopicSharingRelationCore.GetRotationTopicSharingRelations(topicId);

            var topicRelationsUserIds = RotationReportSharingRelations.Select(r => r.RecipientId).Where(u => u.HasValue);

            return _mapper.Map<IEnumerable<SelectListItem>>(topicOwnerTeamMembers.Where(u => !topicRelationsUserIds.Contains(u.Id)));
        }

    }
}
