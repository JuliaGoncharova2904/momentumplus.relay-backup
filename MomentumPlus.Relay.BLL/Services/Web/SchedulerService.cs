﻿using AutoMapper;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Authorization;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.Models.Scheduler.Filters;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Services.Web.TimeLine;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class SchedulerService : ISchedulerService
    {
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;

        public SchedulerService(LogicCoreUnitOfWork logicCore)
        {
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
        }

        #region Search panel

        public SchedulerFilterPanelViewModel GetFilterPanel(Guid userId)
        {
            UserProfile owner = _logicCore.UserProfileCore.GetUser(userId);
            string userRole = owner.Role;

            bool userIsAdmin = iHandoverRoles.RelayGroups.Admins.Split(new[] { ',' }).Contains(userRole);
            bool userIsManager = iHandoverRoles.RelayGroups.LineManagers.Split(new[] { ',' }).Contains(userRole);

            List<ShedulerFilterTeamViewModel> teamsList = new List<ShedulerFilterTeamViewModel>();

            SchedulerFilterPanelViewModel model = new SchedulerFilterPanelViewModel
            {
                UserId = userId,
                AllTeamsBtn = true,
                Teams = teamsList
            };

            if (userIsAdmin || userIsManager)
            {
                List<Team> teams;

                if (userIsAdmin)
                {
                    teams = _logicCore.TeamCore.GetAllTeams().ToList();
                }
                else
                {
                    teams = _logicCore.UserProfileCore.GetRelayEmployees()
                                        .Where(u => u.CurrentRotationId.HasValue && u.CurrentRotation.LineManagerId == userId)
                                        .Select(u => u.Team)
                                        .Distinct()
                                        .ToList();
                }

                teamsList.AddRange(teams.Select(team => new ShedulerFilterTeamViewModel
                {
                    TeamId = team.Id,
                    TeamName = team.Name
                }).OrderBy(team => team.TeamName));
            }

            return model;
        }

        #endregion

        #region Timelines panel

        public SchedulerTimelinePanelViewModel GetTimelineForUser(Guid userId, bool isMyTimeline = false)
        {
            DateTime currentDate = DateTime.Now;
            int currentYear = currentDate.Year;
            UserProfile user = _logicCore.UserProfileCore.GetUser(userId);

            Rotation firstRotation = _logicCore.RotationCore.GetFirstUserRotation(user.Id, true);

            int firstYear;

            if (firstRotation.ConfirmDate.HasValue)
            {
                firstYear = firstRotation.ConfirmDate.Value.Year;
            }
            else if (firstRotation.CreatedUtc.HasValue)
            {
                firstYear = firstRotation.CreatedUtc.Value.Year;
            }
            else
            {
                firstYear = currentYear;
            }

            int countYear = currentYear - firstYear;

            List<TimelineViewModel> timelines = new List<TimelineViewModel>();

            if (countYear == 0 || countYear == 1)
            {
                countYear++;
            }

            if (currentDate.Month == 10 || currentDate.Month == 11 || currentDate.Month == 12)
            {
                countYear++;
            }

            for (var i = 0; i < countYear; ++i)
            {
                timelines.Add(new TimeLineSinglePanel(user, firstYear + i, _logicCore).CreateTimeline());
            }

            SchedulerTimelinePanelViewModel model = new SchedulerTimelinePanelViewModel
            {
                UserId = user.Id,
                UserName = isMyTimeline ? "Me" : user.FullName,
                LastUpdate = user.LastLoginDate.FormatDayMonthYear(),
                Timeline = TimeLineBuilder.ConcatTimelines(timelines)
            };

            return model;
        }

        public SchedulerPanelViewModel GetTimelinesPanel(SchedulerFilterOptionsViewModel model, Guid userId)
        {
            //-------------------------------------------------------
            UserProfile owner = _logicCore.UserProfileCore.GetUser(userId);
            string userRole = owner.Role;

            bool userIsAdmin = iHandoverRoles.RelayGroups.Admins.Split(new[] { ',' }).Contains(userRole);
            bool userIsManager = iHandoverRoles.RelayGroups.LineManagers.Split(new[] { ',' }).Contains(userRole);

            SchedulerPanelViewModel panel = new SchedulerPanelViewModel
            {
                UserId = userId,
                IsSimpleUserMpode = !(userIsAdmin || userIsManager),
                Timelines = new List<SchedulerTimelinePanelViewModel>()
            };

            DateTime currentDate = DateTime.Now;
            int currentYear = currentDate.Year;
            IQueryable<UserProfile> users = _logicCore.UserProfileCore.GetRelayEmployees()
                                            .Where(user => user.CurrentRotationId.HasValue
                                                       && (user.CurrentRotation.PrevRotationId.HasValue
                                                       || user.CurrentRotation.State != RotationState.Created))
                                                       .OrderByDescending(user => user.Id == owner.Id)
                                                       .ThenByDescending(u => u.LastLoginDate);
            List<UserProfile> userList = new List<UserProfile>();
            //-------------------------------------------------------
            if (userIsAdmin || userIsManager)
            {
                if (userIsManager)
                {
                    users = users.Where(u => u.CurrentRotation.LineManagerId == userId || u.Id == owner.Id);
                }

                users = this.AvailabilityFilter(users, model.availability);
                users = this.RotationTypeFilter(users, model.rotationType);
                users = this.TeamFilter(users, model.allTeams, model.teamsIds);
                userList = users.ToList();
                userList = userList.OrderBy(u => u.FullName).ToList();
            }
            //-------------------------------------------------------
            else
            {
                userList = users.Where(u => u.TeamId == owner.TeamId).ToList();
                userList = userList.OrderBy(u => u.FullName).ToList();
                userList.Remove(owner);
                var temp = new List<UserProfile>();
                temp.Add(owner);
                temp.AddRange(userList);
                userList = temp;
            }
            //-------------------------------------------------------
            foreach (UserProfile user in userList)
            {
                Rotation firstRotation = _logicCore.RotationCore.GetFirstUserRotation(user.Id, true);

                int firstYear;

                if (firstRotation.ConfirmDate.HasValue)
                {
                    firstYear = firstRotation.ConfirmDate.Value.Year;
                }
                else if (firstRotation.CreatedUtc.HasValue)
                {
                    firstYear = firstRotation.CreatedUtc.Value.Year;
                }
                else
                {
                    firstYear = currentYear;
                }

                int countYear = currentYear - firstYear;

                List<TimelineViewModel> timelines = new List<TimelineViewModel>();

                if (countYear == 0 || countYear == 1)
                {
                    countYear++;
                }

                if (currentDate.Month == 10 || currentDate.Month == 11 || currentDate.Month == 12)
                {
                    countYear++;
                }

                for (var i = 0; i < countYear; ++i)
                {
                    timelines.Add(new TimeLineSinglePanel(user, firstYear + i, _logicCore).CreateTimeline());
                }

                panel.Timelines.Add(new SchedulerTimelinePanelViewModel
                {
                    UserId = user.Id,
                    UserName = user.Id == owner.Id ? "Me" : user.FullName,
                    LastUpdate = user.LastLoginDate.FormatWithDayMonthYear(),
                    Timeline = TimeLineBuilder.ConcatTimelines(timelines)
                });
            }
            //-------------------------------------------------------
            return panel;
        }

        private IQueryable<UserProfile> AvailabilityFilter(IQueryable<UserProfile> users, AvailabilityFilterParams availability)
        {
            switch (availability)
            {
                case AvailabilityFilterParams.Available:
                    users = users.Where(u => u.CurrentRotationId.HasValue && u.CurrentRotation.State == RotationState.Confirmed);
                    break;
                case AvailabilityFilterParams.Unavailable:
                    users = users.Where(u => u.CurrentRotationId.HasValue && u.CurrentRotation.State != RotationState.Confirmed);
                    break;
                case AvailabilityFilterParams.Off:
                    break;
            }

            return users;
        }

        private IQueryable<UserProfile> RotationTypeFilter(IQueryable<UserProfile> users, RotationTypeFilterParams rotationType)
        {
            switch (rotationType)
            {
                case RotationTypeFilterParams.Swing:
                    users = users.Where(u => u.CurrentRotationId.HasValue && u.CurrentRotation.RotationType == Core.Models.RotationType.Swing);
                    break;
                case RotationTypeFilterParams.Shift:
                    users = users.Where(u => u.CurrentRotationId.HasValue && u.CurrentRotation.RotationType == Core.Models.RotationType.Shift);
                    break;
                case RotationTypeFilterParams.Off:
                    break;
            }

            return users;
        }

        private IQueryable<UserProfile> TeamFilter(IQueryable<UserProfile> users, bool allTeams, IEnumerable<Guid> teamsIds)
        {
            if (!allTeams)
            {
                users = users.Where(u => teamsIds.Contains(u.TeamId));
            }

            return users;
        }

        #endregion

    }
}
