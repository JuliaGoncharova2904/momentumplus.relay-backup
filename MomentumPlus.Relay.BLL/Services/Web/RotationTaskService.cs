﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using System.Data.Entity.Core;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.EntityExtensions;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.BLL.LogicCore.AccessValidation;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class RotationTaskService : IRotationTaskService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public RotationTaskService(IRepositoriesUnitOfWork repositoriesUnitOfWork, IServicesUnitOfWork services, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            _logicCore = logicCore;
            _services = services;
        }

        public RotationTaskViewModel PopulateTaskModel(RotationTaskViewModel model)
        {
            if (model.TopicId.HasValue)
            {
                RotationTopic topic = _repositories.RotationTopicRepository.Get(model.TopicId.Value);
                if (topic != null)
                {
                    if (_services.RotationTopicService.IsShiftTopic(model.TopicId.Value))
                        this.PopulateShiftDatesRangeForRotationTask(topic, model);
                    else
                        this.PopulateDatesRangeForRotationTask(topic, model);

                    model.OwnerId = topic.RotationTopicGroup.RotationModule.Rotation != null
                        ? topic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId
                        : topic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId;
                    model.Section = topic.RotationTopicGroup.RotationModule.Type.GetEnumDescription();
                    model.AssignedTo = topic.AssignedToId;
                    model.CanChangeAssignee = false;
                    model.IsRotationMode = false;
                    model.TopicDetails = new TopicDetailsViewModel
                    {
                        Section = topic.RotationTopicGroup.RotationModule.Type.GetEnumDescription(),
                        ItemName = topic.Name,
                        Notes = topic.Description,
                        ManagerComments = topic.ManagerComments,
                        Tags = string.IsNullOrEmpty(topic.SearchTags) ? null : topic.SearchTags.Split(','),
                        AttacmentsCounter = topic.Attachments.Count,
                        VoiceMessagesCounter = topic.VoiceMessages.Count,
                        TasksCounter = topic.RotationTasks.Count,
                        IsFeedbackRequired = topic.IsFeedbackRequired
                    };
                    model.IsSwingMode = topic.RotationTopicGroup.RotationModule.Rotation != null;
                }
            }

            return model;
        }

        public RotationTaskViewModel GetTask(Guid taskId)
        {
            RotationTask taskEntity = _repositories.RotationTaskRepository.Get(taskId);
            if (taskEntity != null && taskEntity.Enabled)
            {
                RotationTopic topicEntity = taskEntity.RotationTopic;

                RotationTaskViewModel model = new RotationTaskViewModel
                {
                    Section = taskEntity.RotationTopic.RotationTopicGroup.RotationModule.Type.GetEnumDescription(),
                    Notes = taskEntity.Description,
                    SendingOption =
                        (taskEntity.Status == StatusOfTask.Now || taskEntity.Status == StatusOfTask.NewNow) ? TaskSendingOptions.Now : TaskSendingOptions.EndOfSwing,
                    AssignedTo = taskEntity.AssignedToId,
                    Tags = string.IsNullOrEmpty(taskEntity.SearchTags) ? null : taskEntity.SearchTags.Split(','),
                    IsFeedbackRequired = taskEntity.IsFeedbackRequired,
                    AttacmentsCounter = taskEntity.Attachments.Count,
                    VoiceMessagesCounter = taskEntity.VoiceMessages.Count,
                    HasNewAttacments = taskEntity.Attachments.Any(a => a.CreatedUtc > DateTime.UtcNow.AddDays(-2)),
                    HasNewVoiceMessages = taskEntity.VoiceMessages.Any(v => v.CreatedUtc > DateTime.UtcNow.AddDays(-2)),
                    CompleteStatus = taskEntity.IsComplete ? "Complete" : "Incomplete",
                    OwnerId = topicEntity.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId,
                    TopicId = topicEntity.Id,
                    Id = taskId,
                    Deadline = taskEntity.Deadline,
                    Priority = (TaskPriority)taskEntity.Priority,
                    Name =  topicEntity.Name,
                    IsRotationMode = false,
                    TopicDetails = new TopicDetailsViewModel
                    {
                        Section = topicEntity.RotationTopicGroup.RotationModule.Type.GetEnumDescription(),
                        ItemName = topicEntity.Name,
                        Notes = topicEntity.Description,
                        ManagerComments = topicEntity.ManagerComments,
                        Tags = string.IsNullOrEmpty(topicEntity.SearchTags) ? null : topicEntity.SearchTags.Split(','),
                        AttacmentsCounter = topicEntity.Attachments.Count,
                        VoiceMessagesCounter = topicEntity.VoiceMessages.Count,
                        TasksCounter = topicEntity.RotationTasks.Count,
                        IsFeedbackRequired = topicEntity.IsFeedbackRequired,
                        ItemId = topicEntity.Id
                    },
                    IsSwingMode = taskEntity.RotationTopic.RotationTopicGroup.RotationModule.Rotation != null,
                    CanChangeAssignee = taskEntity.CanChangeAssignee()
                };

                this.PopulateDatesRangeForRotationTask(taskEntity.RotationTopic, model);

                return model;
            }

            return null;
        }

        /// <summary>
        /// Get shift task by taskId
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns>model RotationTaskViewModel</returns>
        public RotationTaskViewModel GetShiftTask(Guid taskId)
        {
            RotationTask taskEntity = _repositories.RotationTaskRepository.Get(taskId);
            if (taskEntity != null && taskEntity.Enabled)
            {
                RotationTopic topicEntity = taskEntity.RotationTopic;

                RotationTaskViewModel model = new RotationTaskViewModel
                {
                    Section = taskEntity.RotationTopic.RotationTopicGroup.RotationModule.Type.GetEnumDescription(),
                    Notes = taskEntity.Description,
                    SendingOption =
                        (taskEntity.Status == StatusOfTask.Now || taskEntity.Status == StatusOfTask.NewNow) ? TaskSendingOptions.Now : TaskSendingOptions.EndOfSwing,
                    AssignedTo = taskEntity.AssignedToId,
                    Tags = string.IsNullOrEmpty(taskEntity.SearchTags) ? null : taskEntity.SearchTags.Split(','),
                    IsFeedbackRequired = taskEntity.IsFeedbackRequired,
                    AttacmentsCounter = taskEntity.Attachments.Count,
                    VoiceMessagesCounter = taskEntity.VoiceMessages.Count,
                    HasNewAttacments = taskEntity.Attachments.Any(a => a.CreatedUtc > DateTime.UtcNow.AddDays(-2)),
                    HasNewVoiceMessages = taskEntity.VoiceMessages.Any(v => v.CreatedUtc > DateTime.UtcNow.AddDays(-2)),
                    CompleteStatus = taskEntity.IsComplete ? "Complete" : "Incomplete",
                    OwnerId = topicEntity.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId,
                    TopicId = topicEntity.Id,
                    Id = taskId,
                    Deadline = taskEntity.Deadline,
                    Priority = (TaskPriority)taskEntity.Priority,
                    Name = taskEntity.Name,
                    IsRotationMode = false,
                    TopicDetails = new TopicDetailsViewModel
                    {
                        Section = topicEntity.RotationTopicGroup.RotationModule.Type.GetEnumDescription(),
                        ItemName = topicEntity.Name,
                        Notes = topicEntity.Description,
                        ManagerComments = topicEntity.ManagerComments,
                        Tags = string.IsNullOrEmpty(topicEntity.SearchTags) ? null : topicEntity.SearchTags.Split(','),
                        AttacmentsCounter = topicEntity.Attachments.Count,
                        VoiceMessagesCounter = topicEntity.VoiceMessages.Count,
                        TasksCounter = topicEntity.RotationTasks.Count,
                        IsFeedbackRequired = topicEntity.IsFeedbackRequired,
                        ItemId = topicEntity.Id
                    },
                    IsSwingMode = taskEntity.RotationTopic.RotationTopicGroup.RotationModule.Rotation != null,
                    CanChangeAssignee = taskEntity.CanChangeAssignee()
                };

                this.PopulateShiftDatesRangeForRotationTask(taskEntity.RotationTopic, model);

                return model;
            }

            return null;
        }

        private void PopulateDatesRangeForRotationTask(RotationTopic topic, RotationTaskViewModel model)
        {
            Rotation rotation = topic.RotationTopicGroup.RotationModule.Rotation;
            if (!rotation.StartDate.HasValue)
                throw new Exception($"Task cann't be created for no confirmed rotation with Id: {rotation.Id}");

            model.StartDeadlineDates = rotation.StartDate.Value.AddDays(rotation.DayOn);
            model.EndDeadlineDates = rotation.StartDate.Value.AddDays(rotation.DayOn + rotation.DayOff - 1);
        }

        private void PopulateShiftDatesRangeForRotationTask(RotationTopic topic, RotationTaskViewModel model)
        {
            Shift shift = topic.RotationTopicGroup.RotationModule.Shift;
            if (!shift.StartDateTime.HasValue)
                throw new Exception($"Task cann't be created for no confirmed shift with Id: {shift.Id}");

            model.StartDeadlineDates = shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes);
            model.EndDeadlineDates = shift.StartDateTime.Value.AddDays(shift.WorkMinutes + shift.BreakMinutes);
        }

        public RotationTaskDetailsViewModel GetTaskDetails(Guid taskId)
        {
            RotationTask taskEntity = _repositories.RotationTaskRepository.Get(taskId);
            if (taskEntity != null && taskEntity.Enabled)
            {
                RotationTaskDetailsViewModel model = new RotationTaskDetailsViewModel
                {
                    Section = taskEntity.RotationTopic.RotationTopicGroup.RotationModule.Type.GetEnumDescription(),
                    TaskNotes = taskEntity.Description,
                    SendingOption =
                        (taskEntity.Status == StatusOfTask.Now || taskEntity.Status == StatusOfTask.NewNow) ? TaskSendingOptions.Now : TaskSendingOptions.EndOfSwing,
                    Feedback = taskEntity.SuccessorTask != null ? taskEntity.SuccessorTask.Feedback : taskEntity.Feedback,
                    TopicId = taskEntity.RotationTopicId,
                    AssignedFrom = taskEntity.AncestorTask != null
                        ? _logicCore.RotationTaskCore.GetRotationModuleOwnerId(taskEntity.AncestorTask.RotationTopic.RotationTopicGroup.RotationModule)
                        : taskEntity.RotationTopic.AncestorTopic != null
                            ? _logicCore.RotationTaskCore.GetRotationModuleOwnerId(taskEntity.RotationTopic.AncestorTopic.RotationTopicGroup.RotationModule)
                            : _logicCore.RotationTaskCore.GetRotationModuleOwnerId(taskEntity.RotationTopic.RotationTopicGroup.RotationModule),
                    IsFeedbackRequired = taskEntity.IsFeedbackRequired,
                    AttacmentsCounter = taskEntity.Attachments.Count,
                    VoiceMessagesCounter = taskEntity.VoiceMessages.Count,
                    HasNewAttacments = taskEntity.Attachments.Any(a => a.CreatedUtc > DateTime.UtcNow.AddDays(-2)),
                    HasNewVoiceMessages = taskEntity.VoiceMessages.Any(v => v.CreatedUtc > DateTime.UtcNow.AddDays(-2)),
                    Tags = string.IsNullOrEmpty(taskEntity.SearchTags) ? null : taskEntity.SearchTags.Split(','),
                    Id = taskEntity.Id,
                    Name = taskEntity.Name,
                    Priority = (TaskPriority) taskEntity.Priority,
                    IsComplete = taskEntity.IsComplete,
                    Deadline = taskEntity.Deadline,
                    IsRotationMode = false
                };
                model.TopicDetails = _mapper.Map<TopicDetailsViewModel>(taskEntity.RotationTopic);
                model.IsSwingMode = taskEntity.RotationTopic.RotationTopicGroup.RotationModule.Rotation != null;

                return model;
            }

            return null;
        }


        public IEnumerable<RotationTaskViewModel> GetTopicTasks(Guid topicId)
        {
            IEnumerable<RotationTask> tasks = _logicCore.RotationTaskCore.GetTopicTasks(topicId)
                                                                        .ToList();
            IEnumerable<RotationTaskViewModel> model = tasks.Select(t => new RotationTaskViewModel
            {
                Id = t.Id,
                Deadline = t.Deadline,
                AssignedTo = t.AssignedToId,
                Name = t.Name,
                Priority = (TaskPriority) t.Priority
            });

            return model;
        }

        public void AddTopicTask(Guid topicId, RotationTaskViewModel task)
        {
            RotationTask taskEntity = new RotationTask();

            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            taskEntity.Id = Guid.NewGuid();
            taskEntity.Enabled = true;
            taskEntity.IsPinned = false;
            taskEntity.IsFeedbackRequired = topic.IsFeedbackRequired;
            taskEntity.FinalizeStatus = StatusOfFinalize.NotFinalized;
            switch (task.SendingOption)
            {
                case TaskSendingOptions.Now:
                    taskEntity.Status = StatusOfTask.Now;
                    break;
                case TaskSendingOptions.EndOfSwing:
                    taskEntity.Status = StatusOfTask.Default;
                    break;
            }
            _mapper.Map(task, taskEntity);
            _repositories.RotationTaskRepository.Add(taskEntity);

            _logicCore.RotationTaskCore.AddTopicTask(topicId, taskEntity, false);

            _repositories.Save();

            _logicCore.RotationTaskLogCore.AddMessageToTaskLog(taskEntity, $"<strong>{_logicCore.RotationTaskCore.GetOwnerForTask(taskEntity).FullName}</strong> created the task.");


        }

        public void UpdateTopicTask(RotationTaskViewModel task)
        {
            RotationTask taskEntity = _logicCore.RotationTaskCore.GetTask(task.Id.Value);
            Guid prevRecipientId = taskEntity.AssignedToId.Value;
            _mapper.Map(task, taskEntity);
            _logicCore.RotationTaskCore.UpdateRotationTask(taskEntity, prevRecipientId);
        }

        /// <summary>
        /// Remove task
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public bool RemoveTask(Guid taskId)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);

            if (!task.IsEditable())
            {
                throw new Exception(string.Format("Task with ID: {0} cannot be romoved.", taskId));
            }

            if (task.IsTaskBoardTask())
            {
                return _logicCore.RotationTaskCore.RemoveTaskBoardTask(task);
            }
            else if (task.IsShiftTask())
            {
                return _logicCore.RotationTaskCore.RemoveShiftTask(task);
            }
            else if (task.IsRotationTask())
            {
                return _logicCore.RotationTaskCore.RemoveRotationTask(task);
            }
            else
            {
                throw new Exception("Unknown task source.");
            }
        }

        public bool IsTopicTaskExist(Guid topicId, string name)
        {
            return _logicCore.RotationTaskCore.IsTopicTaskExist(topicId, name);
        }

        public bool IsTaskNameExist(Guid taskId, string name)
        {
            return _logicCore.RotationTaskCore.IsTaskNameExist(taskId, name);
        }

        public bool IsChildTaskComplited(Guid taskId)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);

            return _logicCore.RotationTaskCore.IsChildTaskComplited(task);
        }

        public void UpdateRotationTopicTasksStatus(Guid rotationTopicId, bool status)
        {
            var rotationTopic = _logicCore.RotationTopicCore.GetTopic(rotationTopicId);

            foreach (var rotationTask in rotationTopic.RotationTasks)
            {
                UpdateRotationTaskStatus(rotationTask.Id, status);
            }
        }

        public void UpdateRotationTaskStatus(Guid rotationTaskId, bool status)
        {
            var rotationTask = _logicCore.RotationTaskCore.GetTask(rotationTaskId);

            rotationTask.Enabled = status;
            _repositories.RotationTaskRepository.Update(rotationTask);

            _repositories.Save();
        }

        public void UpdateTemplateTaskChildTaskStatus(Guid templateTaskId, bool status)
        {
            var templateTask = _logicCore.TaskCore.GetTask(templateTaskId);

            var rotationTopics = _logicCore.RotationTopicCore.GetActiveRotationTopicsByTemplate(templateTask.TopicId).ToList();

            var shiftTopics = _logicCore.RotationTopicCore.GetActiveShiftsTopicsByTemplate(templateTask.TopicId).ToList();

            rotationTopics.AddRange(shiftTopics);

            foreach (var rotationTopic in rotationTopics)
            {
                var rotationTask = _logicCore.RotationTaskCore.GetOrCreateRotationTask(rotationTopic.Id, templateTask);

                if (status == true)
                {
                    _services.RotationModuleService.ChangeRotationModuleStatus(rotationTopic.RotationTopicGroup.RotationModuleId, true);
                    _services.RotationTopicGroupService.UpdateRotationTopicGroupStatus(rotationTopic.RotationTopicGroup.Id, true);
                    _services.RotationTopicService.UpdateRotationTopicStatus(rotationTopic.Id, true);
                }

                UpdateRotationTaskStatus(rotationTask.Id, status);
            }
        }

        public TasksTopicViewModel UpdateTaskTopic(TasksTopicViewModel taskTopic, Guid changedByUserId)
        {
            if (taskTopic.Id != null)
            {
                RotationTask task = _logicCore.RotationTaskCore.GetTask(taskTopic.Id.Value);
                Guid prevRecipientId = task.AssignedToId.Value;

                Guid taskOwnerId = task.RotationTopic.RotationTopicGroup.RotationModule.RotationId.HasValue
                    ? task.RotationTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId
                    : task.RotationTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId;

                //---- notification 9 ----
                if (taskOwnerId != changedByUserId && task.AssignedToId != taskTopic.TeammateId)
                {
                    _logicCore.NotificationCore.NotificationTrigger.Send_ManagerReassignsTask(taskOwnerId, task.Id);
                }
                //------------------------

                if (task.Description != taskTopic.Notes)
                {
                    task.FinalizeStatus = StatusOfFinalize.NotFinalized;
                    taskTopic.IsFinalized = false;
                }

                _mapper.Map(taskTopic, task);

                task.RotationTopic.IsPinned = task.RotationTopic.IsPinned.HasValue && !task.RotationTopic.IsPinned.Value
                    ? task.IsPinned
                    : task.RotationTopic.IsPinned;

                task.RotationTopic.IsFeedbackRequired = !task.RotationTopic.IsFeedbackRequired
                    ? task.IsFeedbackRequired
                    : task.RotationTopic.IsFeedbackRequired;

                _logicCore.RotationTaskCore.UpdateRotationTask(task, prevRecipientId);

                taskTopic = _mapper.Map<TasksTopicViewModel>(task);
            }
            return taskTopic;
        }

        public TasksTopicViewModel GetTaskTopic(Guid taskId)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);
            if (task != null && task.Enabled)
            {
                return _mapper.Map<TasksTopicViewModel>(task);
            }

            return null;
        }

        public IEnumerable<TasksTopicViewModel> GetReceivedTasks(Guid rotationId, Guid userId)
        {
            List<RotationTask> tasks = _logicCore.RotationTaskCore.GetReceivedTasksByCreator(rotationId, userId).ToList();

            IEnumerable<TasksTopicViewModel> model = _mapper.Map<IEnumerable<TasksTopicViewModel>>(tasks.OrderBy(t => t.IsComplete.ToString()).ThenBy(t => t.Priority));

            tasks.ForEach(task => _logicCore.RotationTaskCore.MakeNowTaskOpened(task, false));
            _logicCore.SyncWithDatabase();

            return model;
        }

        public IEnumerable<TasksTopicViewModel> GetReceivedTasks(Guid rotationId)
        {
            List<RotationTask> tasks = _logicCore.RotationTaskCore.GetAllRotationTasks(rotationId, TypeOfModuleSource.Received).ToList();

            IEnumerable<TasksTopicViewModel> model = _mapper.Map<IEnumerable<TasksTopicViewModel>>(tasks.OrderBy(t => t.IsComplete.ToString()).ThenBy(t => t.Priority));

            tasks.ForEach(task => _logicCore.RotationTaskCore.MakeNowTaskOpened(task, false));
            _logicCore.SyncWithDatabase();

            return model;
        }

        #region Complete dialog

        /// <summary>
        /// Get task for complete task dialog
        /// </summary>
        /// <param name="taskId">Task ID</param>
        /// <returns></returns>
        public RotationTaskCompleteViewModel PopulateCompleteTaskViewModel(Guid taskId)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);

            RotationTaskCompleteViewModel model = new RotationTaskCompleteViewModel
            {
                TaskDetailsTab = new RotationCompleteTaskTaskDetailsTabViewModel
                {
                    Name = task.Name,
                    Deadline = task.Deadline,
                    TaskNotes = task.Description,
                    Tags = string.IsNullOrEmpty(task.SearchTags) ? null : task.SearchTags.Split(','),
                    AssignedFrom = _logicCore.RotationTaskCore.GetOwnerForTask(task).Id,
                    Priority = (TaskPriority)task.Priority,
                    IsSwingMode = task.RotationTopic.RotationTopicGroup.RotationModule.Rotation != null,
                    SendingOption = (task.Status == StatusOfTask.Now || task.Status == StatusOfTask.NewNow) ? TaskSendingOptions.Now : TaskSendingOptions.EndOfSwing,
                    AttacmentsCounter = task.Attachments.Count,
                    VoiceMessagesCounter = task.VoiceMessages.Count,
                    IsFeedbackRequired = task.IsFeedbackRequired,
                    CanChangeAssignee = task.CanChangeAssignee(),
                    IsRotationMode = false
                },

                OutcomesTab = new RotationCompleteTaskOutcomesTabViewModel
                {
                    ID = task.Id,
                    Name = task.Name,
                    TaskNotes = task.Description,
                    Feedback = task.Feedback,
                    IsFeedbackRequired = task.IsFeedbackRequired,
                    AttacmentsCounter = task.Attachments.Count,
                    VoiceMessagesCounter = task.VoiceMessages.Count
                },

                TopicDetailsTab = new RotationCompleteTaskTopicTabViewModel
                {
                    Section = task.RotationTopic.RotationTopicGroup.RotationModule.Type.GetEnumDescription(),
                    ItemName = task.RotationTopic.Name,
                    Notes = task.RotationTopic.Description,
                    ManagerComments = task.RotationTopic.ManagerComments,
                    Tags = string.IsNullOrEmpty(task.RotationTopic.SearchTags) ? null : task.RotationTopic.SearchTags.Split(','),
                    AttacmentsCounter = task.RotationTopic.Attachments.Count,
                    VoiceMessagesCounter = task.RotationTopic.VoiceMessages.Count,
                    TasksCounter = task.RotationTopic.RotationTasks.Count,
                    IsFeedbackRequired = task.RotationTopic.IsFeedbackRequired,
                    ItemId = task.RotationTopicId.Value
                },

                SelectedTabNumber = task.IsFeedbackRequired ? 2 : 1
            };

            return model;
        }

        /// <summary>
        /// Save/Complete task for rotation
        /// </summary>
        /// <param name="model">Task complete view model</param>
        public void SaveCompleteTask(RotationTaskCompleteViewModel model)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(model.OutcomesTab.ID);

            switch (model.CompleteOption)
            {
                case CompleteDialogOption.Save:
                    _logicCore.RotationTaskCore.SaveTaskFeedback(task, model.OutcomesTab.Feedback);
                    break;
                case CompleteDialogOption.Complete:
                    _logicCore.RotationTaskCore.CompleteTask(task, model.OutcomesTab.Feedback);
                    break;
                default:
                    throw new Exception("Bad complete status info");
            }
        }

        #endregion

        public ManagerCommentsViewModel PopulateManagerCommentViewModel(Guid taskId)
        {
            var model = new ManagerCommentsViewModel
            {
                Id = taskId,
                Notes = _logicCore.RotationTaskCore.GetTaskManagerComments(taskId)
            };

            return model;

        }

        public void UpdateTaskManagerComment(ManagerCommentsViewModel model)
        {
            var task = _logicCore.RotationTaskCore.GetTask(model.Id);

            task.ManagerComments = model.Notes;

            _repositories.RotationTaskRepository.Update(task);
            _repositories.Save();

            var notifyUserId = task.RotationTopic.RotationTopicGroup.RotationModule.Rotation != null ?
                             task.RotationTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwner.Id :
                             task.RotationTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwner.Id;

            _logicCore.NotificationCore.NotificationTrigger
                                        .Send_ManagerAddedComment(notifyUserId, task.Id);
        }

        public void CarryforwardTask(Guid taskId)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);
            if (task == null)
            {
                throw new ObjectNotFoundException(string.Format("RotationTask with Id: {0} was not found.", taskId));
            }
            if (task.IsComplete)
            {
                throw new Exception(string.Format("RotationTask with Id: {0} already Completed, Carry Forward not available", taskId));
            }

            if (!task.TemplateTaskId.HasValue)
            {
                _logicCore.RotationTaskCore.CarryforwardTask(task);
            }

        }

        public bool IsMyTask(Guid userId, Guid taskId)
        {
            var task = _logicCore.RotationTaskCore.GetTask(taskId);

            if (task.RotationTopic.RotationTopicGroup.RotationModule.Rotation != null)
            {
                if (task.RotationTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId == userId)
                {
                    return true;
                }
            }
            else
            {
                if (task.RotationTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId == userId)
                {
                    return true;
                }

            }
            return false;
        }

        public Guid GetTaskRecipientId(Guid taskId)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);

            if (!task.AssignedToId.HasValue)
                throw new Exception(string.Format("RotationTask with Id: {0} don't have recipient", taskId));

            return task.AssignedToId.Value;
        }

        public IEnumerable<TasksTopicViewModel> GetAllShiftTasks(Guid shiftId, ModuleSourceType sourceType)
        {
            IEnumerable<RotationTask> tasks = _logicCore.RotationTaskCore.GetAllShiftTasks(shiftId, (TypeOfModuleSource)sourceType).ToList();

            if (sourceType == ModuleSourceType.Received)
            {
                tasks = tasks.Where(t => t.Status == StatusOfTask.Default && t.Name != "For information");
            }

            return _mapper.Map<IEnumerable<TasksTopicViewModel>>(tasks);
        }

        public IEnumerable<TasksTopicViewModel> GetAllRotationTasks(Guid rotationId, ModuleSourceType sourceType)
        {
            IEnumerable<RotationTask> tasks = _logicCore.RotationTaskCore.GetAllRotationTasks(rotationId, (TypeOfModuleSource)sourceType).ToList();

            if (sourceType == ModuleSourceType.Received)
            {
                tasks = tasks.Where(t => t.Status == StatusOfTask.Default && t.Name != "For information");
            }

            return _mapper.Map<IEnumerable<TasksTopicViewModel>>(sourceType == ModuleSourceType.Received ? tasks.Where(t => t.Status == StatusOfTask.Default) : tasks);
        }

        public bool IsShiftTask(Guid taskId)
        {
            return _logicCore.RotationTaskCore.GetTask(taskId).IsShiftTask();
        }

        public IEnumerable<TasksTopicViewModel> GetShiftReceivedTasks(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            List<Shift> shifts = rotation.RotationShifts.ToList();

            List<RotationTask> tasks = new List<RotationTask>();
            foreach (var shift in shifts)
            {
                List<RotationTask> shiftTasks = _logicCore.RotationTaskCore.GetAllShiftTasks(shift.Id, TypeOfModuleSource.Received)
                                                                                    .Where(t => t.Enabled)
                                                                                    .OrderBy(t => t.IsPinned.HasValue)
                                                                                    .ThenByDescending(t => t.IsPinned == true)
                                                                                    .ToList();
                tasks.AddRange(shiftTasks);
            }


            IEnumerable<TasksTopicViewModel> model = _mapper.Map<IEnumerable<TasksTopicViewModel>>(tasks.OrderBy(t => t.IsComplete.ToString()).ThenBy(t => t.Priority));

            tasks.ForEach(task => _logicCore.RotationTaskCore.MakeNowTaskOpened(task, false));
            _logicCore.SyncWithDatabase();

            return model;
        }

        public IEnumerable<TasksTopicViewModel> GetShiftReceivedTasks(Guid rotationId, Guid userId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            List<Shift> shifts = rotation.RotationShifts.ToList();

            List<RotationTask> tasks = new List<RotationTask>();

            foreach (var shift in shifts)
            {
                List<RotationTask> shiftTasks = _logicCore.RotationTaskCore.GetShiftReceivedTasksByCreator(shift.Id, userId).ToList();

                tasks.AddRange(shiftTasks);
            }

            IEnumerable<TasksTopicViewModel> model = _mapper.Map<IEnumerable<TasksTopicViewModel>>(tasks.OrderBy(t => t.IsComplete.ToString()).ThenBy(t => t.Priority));

            tasks.ForEach(task => _logicCore.RotationTaskCore.MakeNowTaskOpened(task, false));
            _logicCore.SyncWithDatabase();

            return model;
        }


        public void EditReceivedTask(Guid currentUserId, RotationTaskViewModel model)
        {
            if (model.Id.HasValue)
            {
                _logicCore.RotationTaskCore.UpdateRotationReceivedTask(currentUserId, model.Id.Value, model.AssignedTo, model.Deadline, model.Priority);
            }
        }

    }
}
