﻿using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Core.Models;
using System.Data.Entity.Core;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class DailyNoteService : IDailyNoteService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public DailyNoteService(IRepositoriesUnitOfWork repositoriesUnitOfWork, IServicesUnitOfWork services, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
            this._services = services;
        }

        public DailyNoteViewModel DailyNoteSlideForPanel(Guid dailyNoteId, Guid currentUserId, string safetyVesion)
        {
            RotationTopic dailyNoteEntity = _logicCore.RotationTopicCore.GetDailyNote(dailyNoteId);
            DailyNoteViewModel dailyNote = _mapper.Map<DailyNoteViewModel>(dailyNoteEntity);

            Rotation rotation = dailyNoteEntity.RotationTopicGroup.RotationModule.Rotation.RotationOwner.CurrentRotation;

            if (rotation.RotationOwnerId == currentUserId)
            {
                this.AddSafetyMessagesToDaylyNote(dailyNote, rotation, safetyVesion);
            }

            return dailyNote;
        }

        public DailyNotesPanelViewModel DailyNotesForPanel(Guid rotationId, Guid currentUserId, string safetyVesion)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            if (rotation != null)
            {
                var dailyNotes = _logicCore.RotationTopicCore.GetAllUserDailyNotes(rotation.RotationOwnerId)
                                                                .ToList()
                                                                .OrderBy(t => t.RotationTopicGroup.RotationModule.Rotation.CreatedUtc)
                                                                .ThenBy(d => DateTime.Parse(d.Name));

                IEnumerable<DailyNoteViewModel> dailyNotesModel = _mapper.Map<IEnumerable<DailyNoteViewModel>>(dailyNotes);

                if (rotation.StartDate.HasValue && rotation.RotationOwnerId == currentUserId)
                {
                    foreach (DailyNoteViewModel dailyNote in dailyNotesModel)
                    {
                        this.AddSafetyMessagesToDaylyNote(dailyNote, rotation, safetyVesion);
                    }
                }

                if (!dailyNotesModel.Any(d => d.IsActiveInPanel))
                {
                    dailyNotesModel.Last().IsActiveInPanel = true;
                }

                DailyNotesPanelViewModel model = new DailyNotesPanelViewModel
                {
                    ShowEndSlide = (rotation.StartDate.HasValue && (rotation.DayOn - (DateTime.Today - rotation.StartDate.Value).TotalDays <= 4)) 
                                        || (!rotation.StartDate.HasValue && !rotation.ConfirmDate.HasValue),
                    DailyNotes = dailyNotesModel
                };

                return model;
            }

            throw new ObjectNotFoundException(string.Format("Rotation with Id: {0} was not found.", rotationId));
        }

        private void AddSafetyMessagesToDaylyNote(DailyNoteViewModel dailyNote, Rotation rotation, string safetyVesion)
        {
            if (dailyNote.RotationId == rotation.Id && rotation.State == RotationState.Confirmed)
            {
                switch (safetyVesion)
                {
                    case "V1":
                        IEnumerable<SafetyMessage> safetyMessagesV1 = _logicCore.SafetyMessageCore.GetSafetyMessagesForUser(dailyNote.NormalDate, rotation.RotationOwner);
                        if (safetyMessagesV1.Any())
                        {
                            SafetyMessagesV1 safetyMessagesV1Model = new SafetyMessagesV1();
                            safetyMessagesV1Model.Version = "V1";
                            safetyMessagesV1Model.MessagesNumber = safetyMessagesV1.Count();

                            if (safetyMessagesV1Model.MessagesNumber == 1)
                                safetyMessagesV1Model.ImageId = safetyMessagesV1.First().CriticalControl.MajorHazard.Icon.ImageId;

                            dailyNote.SafetyMessages = safetyMessagesV1Model;
                        }
                        break;
                    case "V2":
                    case "ALL":
                        IEnumerable<SafetyMessageV2> safetyMessagesV2 = _logicCore.SafetyMessageV2Core.GetSafetyMessagesForUser(dailyNote.NormalDate, rotation.RotationOwner);
                        if (safetyMessagesV2.Any())
                        {
                            SafetyMessagesV2 safetyMessagesV2Model = new SafetyMessagesV2();
                            safetyMessagesV2Model.Version = "V2";
                            safetyMessagesV2Model.MessagesNumber = safetyMessagesV2.Count();

                            if (safetyMessagesV2Model.MessagesNumber == 1)
                                safetyMessagesV2Model.IconUrl = safetyMessagesV2.First().CriticalControl.MajorHazard.IconPath;

                            dailyNote.SafetyMessages = safetyMessagesV2Model;
                        }
                        break;
                }
            }
        }

        public IEnumerable<DailyNotesTopicViewModel> DailyNoteTopicsForRotation(Guid rotationId, ModuleSourceType sourceType)
        {
            IEnumerable<RotationTopic> dailyNotes = _logicCore.DailyNotesModuleCore.GetDailyNotesForRotation(rotationId, (TypeOfModuleSource)sourceType);

            try
            {
                if (dailyNotes != null && dailyNotes.Any())
                {
                    var model = _mapper.Map<IEnumerable<DailyNotesTopicViewModel>>(dailyNotes);

                    return model;

                    //return _mapper.Map<IEnumerable<DailyNotesTopicViewModel>>(sourceType == ModuleSourceType.Received
                    //     ? dailyNotes.Where(t => t.RotationTasks.Any(tt => tt.Status == StatusOfTask.Default)) 
                    //     : dailyNotes);
                }
            }
            catch
            {
                // ignored
            }

            return new List<DailyNotesTopicViewModel>();
        }

        public DailyNoteViewModel GetDailyNoteById(Guid Id)
        {
            RotationTopic dailyNote = _logicCore.DailyNotesModuleCore.GetDailyNote(Id);
            if (dailyNote != null)
            {
                return _mapper.Map<DailyNoteViewModel>(dailyNote);
            }

            return null;
        }

        public void UpdateDailyNote(DailyNoteViewModel dailyNote)
        {
            if (dailyNote.Id.HasValue)
            {
                RotationTopic dailyNoteEntity = _logicCore.DailyNotesModuleCore.GetDailyNote(dailyNote.Id.Value);
                if (dailyNoteEntity != null)
                {

                    if (dailyNoteEntity.Description != dailyNote.Notes)
                    {
                        dailyNoteEntity.FinalizeStatus = StatusOfFinalize.NotFinalized;
                    }

                    if (dailyNote.ShowInReport)
                    {
                        dailyNoteEntity.RotationTopicGroup.Enabled = true;
                        dailyNoteEntity.RotationTopicGroup.RotationModule.Enabled = true;
                    }
                    else
                    {
                        dailyNoteEntity.RotationTopicGroup.Enabled = false;
                    }

                    _mapper.Map<DailyNoteViewModel, RotationTopic>(dailyNote, dailyNoteEntity);
                    _logicCore.DailyNotesModuleCore.UpdateDailyNote(dailyNoteEntity);
                    return;
                }
            }

            throw new Exception(string.Format("Try update DailyNote entity without Id.", dailyNote.Id));
        }

        public DailyNotesTopicViewModel UpdateDailyNoteTopic(DailyNotesTopicViewModel dailyNote)
        {
            if (dailyNote.Id.HasValue)
            {
                RotationTopic dailyNoteEntity = _logicCore.DailyNotesModuleCore.GetDailyNote(dailyNote.Id.Value);
                if (dailyNoteEntity != null)
                {

                    if (dailyNoteEntity.Description != dailyNote.Notes)
                    {
                        dailyNoteEntity.FinalizeStatus = StatusOfFinalize.NotFinalized;
                        dailyNote.IsFinalized = false;
                    }

                    _mapper.Map<DailyNotesTopicViewModel, RotationTopic>(dailyNote, dailyNoteEntity);
                    _logicCore.DailyNotesModuleCore.UpdateDailyNote(dailyNoteEntity);
                    return _mapper.Map<DailyNotesTopicViewModel>(dailyNoteEntity);
                }
            }

            throw new Exception(string.Format("Try update DailyNote entity without Id.", dailyNote.Id));
        }

        public DailyNotesTopicViewModel GetDailyNoteTopic(Guid topicId)
        {
            RotationTopic dailyNoteEntity = _logicCore.DailyNotesModuleCore.GetDailyNote(topicId);
            if (dailyNoteEntity != null)
            {
                return _mapper.Map<DailyNotesTopicViewModel>(dailyNoteEntity);
            }

            return null;
        }
    }
}
