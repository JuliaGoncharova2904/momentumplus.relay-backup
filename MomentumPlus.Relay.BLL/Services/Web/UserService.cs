﻿using MomentumPlus.Relay.Interfaces.Services;
using System;
using MomentumPlus.Relay.Models;
using System.Collections.Generic;
using MomentumPlus.Core.Interfaces;
using System.Linq;
using AutoMapper;
using MomentumPlus.Relay.BLL.Mapper;
using System.Web.Mvc;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces;
using System.Web;
using Microsoft.AspNet.Identity;
using MomentumPlus.Core.Authorization.Models;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class UserService : IUserService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly IServicesUnitOfWork _services;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IMailerService _mailerService;

        public UserService(IRepositoriesUnitOfWork repositoriesUnitOfWork, IServicesUnitOfWork services, LogicCoreUnitOfWork logicCore, IMailerService mailerService)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._services = services;
            this._logicCore = logicCore;
            this._mailerService = mailerService;
        }

        public EmployeeViewModel PopulateEmployeeModel()
        {
            EmployeeViewModel model = new EmployeeViewModel();

            return PopulateEmployeeModel(model);
        }

        public EmployeeViewModel PopulateEmployeeModel(EmployeeViewModel model)
        {
            model.Companies = _services.CompanyService.GetCompaniesList();
            model.UserTypes = _mapper.Map<IEnumerable<SelectListItem>>(_logicCore.RoleManager.Roles.OrderBy(r => r.Name));
            model.Positions = _services.PositionService.GetPositionsList();
            model.RotationPatterns = _services.RotationPatternService.GetRotationPatternsList();
            model.Teams = _services.TeamService.GetTeamList();

            return model;
        }

        public IEnumerable<SelectListItem> GetEmployeesList()
        {
            IEnumerable<UserProfile> employees = _logicCore.UserProfileCore.GetRelayEmployees();

            return _mapper.Map<IEnumerable<SelectListItem>>(employees).OrderBy(em => em.Text);
        }

        public EmployeeViewModel GetEmployeeById(Guid Id)
        {
            UserProfile userProfile = _repositories.UserProfileRepository.Get(Id);

            if (userProfile != null)
            {
                EmployeeViewModel employeeViewModel = _mapper.Map<EmployeeViewModel>(userProfile);
                return PopulateEmployeeModel(employeeViewModel);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<EmployeeViewModel> GetAllEmployees()
        {
            return _mapper.Map<IEnumerable<EmployeeViewModel>>(_repositories.UserProfileRepository.GetAll());
        }

        public bool IsEmployeeExist(string login)
        {
            return _repositories.UserProfileRepository.Find(m => m.UserName == login).Any();
        }

        public bool IsEmployeeExist(string login, Guid exeptId)
        {
            return _repositories.UserProfileRepository.Find(m => m.UserName == login && m.Id != exeptId).Any();
        }

        public void AddEmployee(CreateEmployeeViewModel employeeViewModel)
        {
            UserProfile user = new UserProfile();

            _mapper.Map(employeeViewModel, user);

            user.Id = Guid.NewGuid();
            user.TaskBoard = new TaskBoard
            {
                Id = user.Id,
                Tasks = new HashSet<RotationTask>()
            };

            user.LastPasswordChangedDate = DateTime.Now;

            _repositories.UserProfileRepository.Add(user);
            _repositories.Save();


            var identityUser = new ApplicationUser
            {
                Id = user.Id,
                UserName = employeeViewModel.Email,
                Email = employeeViewModel.Email,
                EmailConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString()
            };

            var result = _logicCore.UserManager.Create(identityUser, employeeViewModel.Password);

            if (result.Succeeded)
            {
                var roleName = _logicCore.RoleManager.FindById(Guid.Parse(employeeViewModel.UserTypeId)).Name;
                _logicCore.UserManager.AddToRole(identityUser.Id, roleName);
            }

        }

        public bool UpdateEmployee(EmployeeViewModel employee)
        {
            if (employee.Id.HasValue)
            {
                UserProfile employeeEntity = _repositories.UserProfileRepository.Get(employee.Id.Value);
                if (employeeEntity != null)
                {
                    _mapper.Map(employee, employeeEntity);
                    _repositories.UserProfileRepository.Update(employeeEntity);
                    _repositories.Save();


                    var identityUser = _logicCore.UserManager.FindById(employee.Id.Value);

                    if (identityUser != null)
                    {
                        var currentRole = identityUser.Roles.SingleOrDefault().Role;

                        if (currentRole.Id != Guid.Parse(employee.UserTypeId))
                        {
                            _logicCore.UserManager.RemoveFromRole(identityUser.Id, currentRole.Name);

                            var newRoleName = _logicCore.RoleManager.FindById(Guid.Parse(employee.UserTypeId)).Name;

                            _logicCore.UserManager.AddToRole(identityUser.Id, newRoleName);
                        }
                        if (identityUser.Email != employee.Email)
                        {
                            identityUser.Email = employee.Email;
                            identityUser.UserName = employee.Email;

                            _logicCore.UserManager.Update(identityUser);
                        }

                    }

                    return true;
                }
            }

            return false;
        }

        public SecurityViewModel GetSecurityDetails(Guid personId)
        {
            var person = _repositories.UserProfileRepository.Get(personId);
            if (person != null)
            {
                return _mapper.Map<SecurityViewModel>(person);
            }

            return null;
        }

        public bool UpdateSecurityDetails(Guid personId, SecurityViewModel model)
        {
            UserProfile person = _repositories.UserProfileRepository.Get(personId);

            if (_repositories.UserProfileRepository.Find(u => u.UserName.Equals(model.RelayEmail)).Any())
            {
                return false;
            }
            if (person != null)
            {
                _mapper.Map(model, person);
                _repositories.UserProfileRepository.Update(person);
                _repositories.Save();

                var identityUser = _logicCore.UserManager.FindById(person.Id);

                if (identityUser != null)
                {
                    identityUser.Email = model.RelayEmail;
                    identityUser.UserName = model.RelayEmail;

                    _logicCore.UserManager.Update(identityUser);
                }

                return true;
            }

            return false;
        }

        public IEnumerable<UserDetailsViewModel> GetUsersDetails()
        {
            IQueryable<UserProfile> users = _logicCore.UserProfileCore.GetRelayEmployees();
            if (users.Any())
            {
                return _mapper.Map<IEnumerable<UserDetailsViewModel>>(users.ToList().OrderBy(u => u.FirstName));
            }

            return new List<UserDetailsViewModel>();
        }

        public bool RemoveUser(Guid userId)
        {
            UserProfile user = _repositories.UserProfileRepository.Get(userId);
            if (user != null)
            {
                user.DeletedUtc = DateTime.Now;
                _repositories.UserProfileRepository.Update(user);
                _repositories.Save();

                var identityUser = _logicCore.UserManager.FindById(userId);

                if (identityUser != null)
                {
                    _logicCore.UserManager.Delete(identityUser);
                }

                return true;
            }

            return false;
        }

        public Guid? GetEmployeeIdByResetHash(Guid resetPasswordId)
        {
            UserProfile user = _repositories.UserProfileRepository.Find(u => u.PasswordResetGuid == resetPasswordId && !u.DeletedUtc.HasValue).FirstOrDefault();

            if (user != null)
            {
                return user.Id;
            }

            return null;
        }



        public void ForgotPassword(Guid userId, Guid resetGuid, string resetUrl)
        {
            UserProfile user = _repositories.UserProfileRepository.Get(userId);
            if (user != null)
            {
                user.PasswordResetGuid = resetGuid;
                _repositories.UserProfileRepository.Update(user);
                _repositories.Save();

                _mailerService.SendForgotPasswordEmail(user.Email, user.FirstName, resetUrl);
            }
        }


        public PersonalDetailsViewModel GetPersonDetails(Guid personId)
        {
            UserProfile person = _repositories.UserProfileRepository.Get(personId);
            if (person != null)
            {
                return _mapper.Map<PersonalDetailsViewModel>(person).FillTitles();
            }

            return null;
        }

        public bool UpdatePersonDetails(Guid personId, PersonalDetailsViewModel personDetails, HttpPostedFileBase foto)
        {
            UserProfile person = _repositories.UserProfileRepository.Get(personId);
            if (person != null)
            {
                _mapper.Map(personDetails, person);
                if (foto != null)
                {
                    person.Avatar = _logicCore.MediaCore.AddImage("Avatar", foto.FileName, foto.ContentType, foto.InputStream, false);
                }

                _repositories.UserProfileRepository.Update(person);
                _repositories.Save();

                return true;
            }

            return false;
        }

        public IEnumerable<EmployeeViewModel> GetEmployeesForTeam(Guid teamId)
        {
            IQueryable<UserProfile> users = _repositories.UserProfileRepository.Find(m => m.TeamId == teamId);
            return _mapper.Map<IEnumerable<EmployeeViewModel>>(users.ToList().OrderBy(u => u.FirstName));
        }


        public IEnumerable<EmployeeViewModel> GetEmployeesForPosition(Guid positionId)
        {
            return _mapper.Map<IEnumerable<EmployeeViewModel>>(_repositories.UserProfileRepository.Find(c => c.PositionId == positionId));
        }

        public void ResetEmployeePassword(Guid userId, string newPassword)
        {
            var user = _logicCore.UserProfileCore.GetUser(userId);

            var identityUser = _logicCore.UserManager.FindById(userId);

            if (identityUser != null)
            {
                string resetToken = _logicCore.UserManager.GeneratePasswordResetToken(identityUser.Id);
                _logicCore.UserManager.ResetPassword(identityUser.Id, resetToken, newPassword);
            }

            user.PasswordResetGuid = null;
            user.LastPasswordChangedDate = DateTime.UtcNow;

            _repositories.UserProfileRepository.Update(user);
            _repositories.Save();
        }

        public List<TeamForWidgetViewModel> GetTeamForWidjet(Guid? rotationId, Guid? userId)
        {
            if (rotationId.HasValue && userId.HasValue)
            {
                var user = _logicCore.UserProfileCore.GetRelayEmployees().Where(u => u.Id == userId.Value).FirstOrDefault();

                var teamMember = user.Team.Users.Where(u => u.Id != userId.Value && !u.DeletedUtc.HasValue);

                var team = _mapper.Map<List<TeamForWidgetViewModel>>(teamMember.ToList().OrderBy(u => u.FirstName));
                foreach (var userMember in team)
                {
                    userMember.RotationId = rotationId.Value;
                    userMember.TeamStatus = "Team member";
                    if (userMember.UserId == user.CurrentRotation.DefaultBackToBackId)
                    {
                        userMember.TeamStatus = "Back to back";
                    }
                    if (userMember.UserId == user.CurrentRotation.LineManagerId)
                    {
                        userMember.TeamStatus = "Line Manager";
                    }
                }
                return team;
            }
            return null;
        }

        public void UpdateLastLoginDate(Guid userId)
        {
            var user = _logicCore.UserProfileCore.GetUser(userId);

            if (user != null)
            {
                user.LastLoginDate = DateTime.Now;


                _repositories.UserProfileRepository.Update(user);
                _repositories.Save();
            }
        }
    }
}
