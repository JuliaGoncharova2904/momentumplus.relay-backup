﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Models;
using RotationType = MomentumPlus.Relay.Models.RotationType;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class RotationModuleService : IRotationModuleService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public RotationModuleService(IRepositoriesUnitOfWork repositoriesUnitOfWork, IServicesUnitOfWork services, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            _logicCore = logicCore;
            _services = services;
        }

        public void ChangeTemplateModuleChildRotationModulesStatus(Guid templateModuleId, bool status)
        {
            var childModulesInRotations = _logicCore.RotationModuleCore.GetActiveRotationsModulesByTemplate(templateModuleId).ToList();

            var childModulesInShifts = _logicCore.RotationModuleCore.GetActiveShiftsModulesByTemplate(templateModuleId).ToList();

            childModulesInRotations.AddRange(childModulesInShifts);

            foreach (var childModule in childModulesInRotations)
            {
                ChangeRotationModuleStatus(childModule.Id, status);
            }

        }


        public void ChangeRotationModuleStatus(Guid moduleId, bool status)
        {
            var module = _logicCore.RotationModuleCore.GetModule(moduleId);

            if (status == false)
            {
                _services.RotationTopicGroupService.UpdateRotationModuleTopicGroupsStatus(moduleId, status);
            }

            module.Enabled = status;

            _repositories.RotationModuleRepository.Update(module);
            _repositories.Save();
        }



        private IEnumerable<RotationTopic> FiterModuleTopics(IEnumerable<RotationTopic> rotationTopic, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems)
        {
            IEnumerable<RotationTopic> result = new List<RotationTopic>();

            if (rotationTopic != null)
            {
                if (filter == HandoverReportFilterType.All)
                {
                    result = rotationTopic;
                }

                if (filter == HandoverReportFilterType.HandoverItems)
                {
                    result = rotationTopic.Where(topic => !topic.IsNullReport);
                }

                if (filter == HandoverReportFilterType.NrItems)
                {
                    result = rotationTopic.Where(topic => topic.IsNullReport);
                }
            }
                    
            return result;
        }

        /// <summary>
        /// Get HSE topics view model
        /// </summary>
        /// <param name="moduleOwnerId">Module owner ID</param>
        /// <param name="sourceType">Source type</param>
        /// <param name="rotationType">Rotation type</param>
        /// <param name="filter">Filter Items Type</param>
        /// <returns></returns>
        public IEnumerable<HSETopicViewModel> GetHseModuleTopics(Guid moduleOwnerId, ModuleSourceType sourceType, RotationType rotationType = RotationType.Swing, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems)
        {
            RotationModule rotationModule = rotationType == RotationType.Swing
                                            ? _logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.HSE, moduleOwnerId, (TypeOfModuleSource)sourceType)
                                            : _logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.HSE, moduleOwnerId, (TypeOfModuleSource)sourceType);

            if (rotationModule != null && rotationModule.RotationTopicGroups.Any())
            {
                IEnumerable<RotationTopic> rotationTopics = rotationModule.RotationTopicGroups
                                                                            .Where(tg => tg.Enabled)
                                                                            .SelectMany(tg => tg.RotationTopics)
                                                                            .Where(t => t.Enabled);
                if (sourceType == ModuleSourceType.Draft)
                {
                    rotationTopics = this.FiterModuleTopics(rotationTopics, filter);
                }

                return _mapper.Map<IEnumerable<HSETopicViewModel>>(rotationTopics);
            }

            return new List<HSETopicViewModel>();
        }

        /// <summary>
        /// Get Core topics view model
        /// </summary>
        /// <param name="moduleOwnerId">Module owner ID</param>
        /// <param name="sourceType">Source type</param>
        /// <param name="rotationType">Rotation type</param>
        /// <param name="filter">Filter Items Type</param>
        /// <returns></returns>
        public IEnumerable<CoreTopicViewModel> GetCoreModuleTopics(Guid moduleOwnerId, ModuleSourceType sourceType, RotationType rotationType = RotationType.Swing, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems)
        {
            RotationModule rotationModule = rotationType == RotationType.Swing
                                            ? _logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Core, moduleOwnerId, (TypeOfModuleSource)sourceType)
                                            : _logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.Core, moduleOwnerId, (TypeOfModuleSource)sourceType);

            if (rotationModule != null && rotationModule.RotationTopicGroups.Any())
            {
                IEnumerable<RotationTopic> rotationTopics = rotationModule.RotationTopicGroups
                                                                            .Where(tg => tg.Enabled)
                                                                            .SelectMany(tg => tg.RotationTopics)
                                                                            .Where(t => t.Enabled);

                if (sourceType == ModuleSourceType.Draft)
                {
                    rotationTopics = this.FiterModuleTopics(rotationTopics, filter);
                }

                return _mapper.Map<IEnumerable<CoreTopicViewModel>>(rotationTopics);
            }

            return new List<CoreTopicViewModel>();
        }

        /// <summary>
        /// Get Team topics view model
        /// </summary>
        /// <param name="moduleOwnerId">Module owner ID</param>
        /// <param name="sourceType">Source type</param>
        /// <param name="rotationType">Rotation type</param>
        /// <param name="filter">Filter Items Type</param>
        /// <returns></returns>
        public IEnumerable<TeamTopicViewModel> GetTeamModuleTopics(Guid moduleOwnerId, ModuleSourceType sourceType, RotationType rotationType = RotationType.Swing, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems)
        {
            RotationModule rotationModule = rotationType == RotationType.Swing
                                            ? _logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Team, moduleOwnerId, (TypeOfModuleSource)sourceType)
                                            : _logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.Team, moduleOwnerId, (TypeOfModuleSource)sourceType);

            if (rotationModule != null && rotationModule.RotationTopicGroups.Any())
            {
                IEnumerable<RotationTopic> rotationTopics = rotationModule.RotationTopicGroups
                    .Where(tg => tg.Enabled)
                    .SelectMany(tg => tg.RotationTopics)
                    .Where(t => t.Enabled);

                if (sourceType == ModuleSourceType.Draft)
                {
                    rotationTopics = this.FiterModuleTopics(rotationTopics, filter);
                }

                IEnumerable<TeamTopicViewModel> model = _mapper.Map<IEnumerable<TeamTopicViewModel>>(rotationTopics);

                foreach (TeamTopicViewModel topic in model)
                {
                    topic.TeamMember = topic.RelationId.HasValue == false || topic.RelationId.Value == Guid.Empty ?
                           "Other" :
                           _logicCore.UserProfileCore.GetUser(topic.RelationId.Value).FullName;
                }

                return model;
            }

            return new List<TeamTopicViewModel>();
        }

        /// <summary>
        /// Get Project topics view model
        /// </summary>
        /// <param name="moduleOwnerId">Module owner ID</param>
        /// <param name="sourceType">Source type</param>
        /// <param name="rotationType">Rotation type</param>
        /// <param name="filter">Filter Items Type</param>
        /// <returns></returns>
        public IEnumerable<ProjectsTopicViewModel> GetProjectModuleTopics(Guid moduleOwnerId, ModuleSourceType sourceType, RotationType rotationType = RotationType.Swing, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems)
        {
            RotationModule rotationModule = rotationType == RotationType.Swing
                                            ? _logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Project, moduleOwnerId, (TypeOfModuleSource)sourceType)
                                            : _logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.Project, moduleOwnerId, (TypeOfModuleSource)sourceType);

            if (rotationModule != null && rotationModule.RotationTopicGroups.Any())
            {
                IEnumerable<RotationTopic> rotationTopics = rotationModule.RotationTopicGroups
                                                                            .Where(tg => tg.Enabled)
                                                                            .SelectMany(tg => tg.RotationTopics)
                                                                            .Where(t => t.Enabled);

                if (sourceType == ModuleSourceType.Draft)
                {
                    rotationTopics = this.FiterModuleTopics(rotationTopics, filter);
                }

                return _mapper.Map<IEnumerable<ProjectsTopicViewModel>>(rotationTopics);
            }

            return new List<ProjectsTopicViewModel>();
        }


        public bool IsModuleEnabled(ModuleType moduleType, Guid moduleOwnerId, ModuleSourceType sourceType, RotationType rotationType = RotationType.Swing)
        {
            var rotationModule = rotationType == RotationType.Swing ?
                _logicCore.RotationModuleCore.GetRotationModule((TypeOfModule)moduleType, moduleOwnerId, (TypeOfModuleSource)sourceType) :
                _logicCore.RotationModuleCore.GetShiftModule((TypeOfModule)moduleType, moduleOwnerId, (TypeOfModuleSource)sourceType);

            return rotationModule != null && rotationModule.Enabled;
        }

        public int ModuleTopicsCount(Guid moduleOwnerId, ModuleType moduleType, ModuleSourceType sourceType, RotationType rotationType = RotationType.Swing)
        {
            var rotationModule = rotationType == RotationType.Swing ?
                _logicCore.RotationModuleCore.GetRotationModule((TypeOfModule)moduleType, moduleOwnerId, (TypeOfModuleSource)sourceType) :
                _logicCore.RotationModuleCore.GetShiftModule((TypeOfModule)moduleType, moduleOwnerId, (TypeOfModuleSource)sourceType);

            if (rotationModule != null && rotationModule.RotationTopicGroups != null && rotationModule.RotationTopicGroups.Any())
            {
                var rotationTopics = rotationModule.RotationTopicGroups.Where(tg => tg.RotationTopics != null)
                                                       .SelectMany(tg => tg.RotationTopics)
                                                       .Where(t => t.Enabled);

                return rotationTopics.Count();
            }

            return 0;
        }


        public int ModuleTasksCount(Guid rotationId, ModuleSourceType sourceType)
        {
            if (rotationId != Guid.Empty)
            {
                var taskCount = _logicCore.RotationTaskCore.GetAllRotationTasks(rotationId, (TypeOfModuleSource)sourceType).Count();
                if (taskCount != 0)
                {
                    return taskCount;
                }
            }

            return 0;
        }
    }

}
