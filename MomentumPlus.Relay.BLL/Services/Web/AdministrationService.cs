﻿using System.Linq;
using System.Web;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.Configuration;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class AdministrationService : IAdministrationService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;
        private readonly IMailerService _mailerService;



        public AdministrationService(IRepositoriesUnitOfWork repositoriesUnitOfWork, IServicesUnitOfWork services, LogicCoreUnitOfWork logicCore, IMailerService mailerService)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
            this._services = services;
            this._mailerService = mailerService;
        }


        public void InitializeAdminSettings()
        {
            _logicCore.AdministrationCore.InitializeAdminSettings();
        }



        public AccountDetailsViewModel PopulateAccountDetailsViewModel()
        {
            var adminSettings = _logicCore.AdministrationCore.GetAdminSettings();

            if (adminSettings != null)
            {
                var templetesCounter = _repositories.TemplateRepository.Count();

                var handoversCounter = _repositories.UserProfileRepository.Find(user => user.CurrentRotationId.HasValue).Count();

                var viewModel = new AccountDetailsViewModel
                {
                    PlanType = adminSettings.PlanType.ToString(),
                    LicenceEndDate = adminSettings.LicenceEndDate.HasValue ? adminSettings.LicenceEndDate.FormatWithDateMonthAndFullYear() : "-",
                    PurchaseDate = adminSettings.PurchaseDate.HasValue ? adminSettings.PurchaseDate.FormatWithDateMonthAndFullYear() : "-",
                    TechnicalSupport = adminSettings.SupportType.ToString(),
                    VersionDetails = "Relay Version 2.5.2",
                    Templates = adminSettings.TemplateLimit.HasValue ? templetesCounter + "/" + adminSettings.TemplateLimit : "0" + "/" + templetesCounter,
                    Handovers = adminSettings.HandoverLimit.HasValue ? handoversCounter + "/" + adminSettings.HandoverLimit : "0" + "/" + handoversCounter

                };

                return viewModel;
            }

            return null;
        }


        public VersionDetailsViewModel PopulateVersionDetailsViewModel()
        {
            var adminSettings = _logicCore.AdministrationCore.GetAdminSettings();

            if (adminSettings != null)
            {

                var viewModel = new VersionDetailsViewModel
                {
                    MaximumHandovers = adminSettings.HandoverLimit.Value,
                    MaximumTemplates = adminSettings.TemplateLimit.Value,
                    LicenceEndDate = adminSettings.LicenceEndDate,
                    HostingProvider = adminSettings.HostingProvider,
                    iHandoverAccountManager = adminSettings.AccountManager,
                    DomainUrl = HttpContext.Current.Request.Url.Host

                };

                return viewModel;
            }

            return null;
        }

        public void UpdateVersionDetails(VersionDetailsViewModel model)
        {
            var adminSettings = _logicCore.AdministrationCore.GetAdminSettings();

            if (adminSettings != null)
            {
                adminSettings.AccountManager = model.iHandoverAccountManager;
                adminSettings.LicenceEndDate = model.LicenceEndDate;
                adminSettings.TemplateLimit = model.MaximumTemplates;
                adminSettings.HandoverLimit = model.MaximumHandovers;


                _repositories.AdminSettingsRepository.Update(adminSettings);
                _repositories.Save();


            }
        }

        public ThirdPartySettingsViewModel PopulateThirdPartySettingsViewModel()
        {
            var adminSettings = _logicCore.AdministrationCore.GetAdminSettings();

            if (adminSettings != null)
            {

                var viewModel = new ThirdPartySettingsViewModel
                {
                    SMSPerUserPerDay = adminSettings.TwilioSettings.NumberSMSPerUserPerDay,
                    TwilioAccount = adminSettings.TwilioSettings.TwilioAccount,
                    SMSGateway = adminSettings.TwilioSettings.SMSGateway,
                    TwilioEndPoint = adminSettings.TwilioSettings.TwilioEndpoint,
                    TwilioPhoneNumber = adminSettings.TwilioSettings.TwilioPhoneNumber

                };

                return viewModel;
            }

            return null;
        }


        public void UpdateThirdPartySettings(ThirdPartySettingsViewModel model)
        {
            var adminSettings = _logicCore.AdministrationCore.GetAdminSettings();

            if (adminSettings != null)
            {
                adminSettings.TwilioSettings.NumberSMSPerUserPerDay = model.SMSPerUserPerDay;
                adminSettings.TwilioSettings.SMSGateway = model.SMSGateway;
                adminSettings.TwilioSettings.TwilioAccount = model.TwilioAccount;
                adminSettings.TwilioSettings.TwilioEndpoint = model.TwilioEndPoint;
                adminSettings.TwilioSettings.TwilioPhoneNumber = model.TwilioPhoneNumber;


                _repositories.AdminSettingsRepository.Update(adminSettings);
                _repositories.Save();


            }
        }



        public PreferencesSettingsViewModel PopulatePreferencesSettingsViewModel()
        {
            var adminSettings = _logicCore.AdministrationCore.GetAdminSettings();

            if (adminSettings != null)
            {
                var viewModel = _mapper.Map<PreferencesSettingsViewModel>(adminSettings);

                return viewModel;
            }

            return null;
        }


        public void UpdatePreferencesSettings(PreferencesSettingsViewModel model, HttpPostedFileBase logo)
        {
            var adminSettings = _logicCore.AdministrationCore.GetAdminSettings();

            if (adminSettings != null)
            {
                adminSettings.HandoverPreviewTime = model.HandoverPreviewTime;
                adminSettings.HandoverTriggerTime = model.HandoverTriggerTime;
                adminSettings.HandoverPreviewType = (TypeOfHandoverPreview)model.HandoverPreviewType;
                adminSettings.HandoverTriggerType = (TypeOfHandoverTrigger)model.HandoverTriggerType;

                adminSettings.AllowedFileExtensions = !string.IsNullOrEmpty(model.AllowedFileTypesString) ? string.Join(",", model.AllowedFileTypes) : null;

                if (logo != null)
                {
                    adminSettings.Logo = _logicCore.MediaCore.AddImage("Logo", logo.FileName, logo.ContentType, logo.InputStream, false);
                }

                _repositories.AdminSettingsRepository.Update(adminSettings);
                _repositories.Save();

                RelayConfig.GetConfig().JobsConfig.ReloadConfiguration();
            }
        }



    }
}
