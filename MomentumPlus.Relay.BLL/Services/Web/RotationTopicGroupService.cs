﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class RotationTopicGroupService : IRotationTopicGroupService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public RotationTopicGroupService(IRepositoriesUnitOfWork repositoriesUnitOfWork, IServicesUnitOfWork services, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            _logicCore = logicCore;
            _services = services;
        }

        public void UpdateRotationModuleTopicGroupsStatus(Guid rotationModuleId, bool status)
        {
            var rotationModule = _logicCore.RotationModuleCore.GetModule(rotationModuleId);

            foreach (var rotationTopicGroup in rotationModule.RotationTopicGroups)
            {
                UpdateRotationTopicGroupStatus(rotationTopicGroup.Id, status);
            }
        }

        public void UpdateRotationTopicGroupStatus(Guid rotationTopicGroupId, bool status)
        {
            var rotationTopicGroup = _logicCore.RotationTopicGroupCore.GetTopicGroup(rotationTopicGroupId);

            if (status == false)
            {
                _services.RotationTopicService.UpdateRotationTopicGroupTopicsStatus(rotationTopicGroup.Id, status);
            }

            rotationTopicGroup.Enabled = status;
            _repositories.RotationTopicGroupRepository.Update(rotationTopicGroup);

            _repositories.Save();
        }


        public void UpdateTemplateTopicGroupChildRotationTopicGroupsStatus(Guid templateTopicGroupId, bool status)
        {
            var templateTopicGroup = _logicCore.TopicGroupCore.GetTopicGroup(templateTopicGroupId);

            var rotationModules = _logicCore.RotationModuleCore.GetActiveRotationsModulesByTemplate(templateTopicGroup.ModuleId).ToList();

            var shiftModules = _logicCore.RotationModuleCore.GetActiveShiftsModulesByTemplate(templateTopicGroup.ModuleId).ToList();

            rotationModules.AddRange(shiftModules);

            foreach (var rotationModule in rotationModules)
            {
                var rotationTopicGroup = _logicCore.RotationTopicGroupCore.GetOrCreateRotationTopicGroup(rotationModule.Id, templateTopicGroup);

                if (status == true)
                {
                    _services.RotationModuleService.ChangeRotationModuleStatus(rotationModule.Id, true);
                }

                UpdateRotationTopicGroupStatus(rotationTopicGroup.Id, status);
            }

        }


        public IEnumerable<SelectListItem> GetRotationModuleTopicGroupsList(Guid moduleId)
        {
            return _mapper.Map<IEnumerable<SelectListItem>>(_logicCore.RotationTopicGroupCore.GetModuleTopicGroups(moduleId));
        }

    }
}
