﻿using System;
using System.Linq;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using System.Data.Entity.Core;
using MomentumPlus.Core.Models;
using Newtonsoft.Json;
using NLog;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class RotationTopicService : IRotationTopicService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        private static readonly Logger ActionLogger = LogManager.GetLogger("topic-logger");



        public RotationTopicService(IRepositoriesUnitOfWork repositoriesUnitOfWork, IServicesUnitOfWork services,
            LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            _logicCore = logicCore;
            _services = services;
        }


        public void UpdateRotationTopicGroupTopicsStatus(Guid rotationTopicGroupId, bool status)
        {
            var rotationTopicGroup = _logicCore.RotationTopicGroupCore.GetTopicGroup(rotationTopicGroupId);

            foreach (var rotationTopic in rotationTopicGroup.RotationTopics)
            {
                UpdateRotationTopicStatus(rotationTopic.Id, status);
            }
        }

        public void UpdateRotationTopicStatus(Guid rotationTopicId, bool status)
        {
            var rotationTopic = _logicCore.RotationTopicCore.GetTopic(rotationTopicId);

            if (status == false)
            {
                _services.RotationTaskService.UpdateRotationTopicTasksStatus(rotationTopic.Id, status);
            }

            rotationTopic.Enabled = status;
            _repositories.RotationTopicRepository.Update(rotationTopic);

            _repositories.Save();
        }


        public void UpdateTemplateTopicChildTopicsStatus(Guid templateTopicId, bool status)
        {
            var templateTopic = _logicCore.TopicCore.GetTopic(templateTopicId);

            var rotationTopicGroups =
                _logicCore.RotationTopicGroupCore.GetActiveRotationTopicGroupsByTemplate(templateTopic.TopicGroupId).ToList();

            var shiftTopicGroups = _logicCore.RotationTopicGroupCore.GetActiveShiftTopicGroupsByTemplate(templateTopic.TopicGroupId).ToList();

            rotationTopicGroups.AddRange(shiftTopicGroups);

            foreach (var rotationTopicGroup in rotationTopicGroups)
            {
                var rotationTopic = _logicCore.RotationTopicCore.GetOrCreateRotationTopic(rotationTopicGroup.Id,
                    templateTopic);

                if (status == true)
                {
                    _services.RotationModuleService.ChangeRotationModuleStatus(rotationTopicGroup.RotationModuleId, true);
                    _services.RotationTopicGroupService.UpdateRotationTopicGroupStatus(rotationTopicGroup.Id, true);
                }

                UpdateRotationTopicStatus(rotationTopic.Id, status);
            }
        }

        public bool IsTopicExist(Guid topicId)
        {
            return _logicCore.RotationTopicCore.IsTopicExist(topicId);
        }

        public bool IsTopicExist(Guid topicGroupId, Guid topicId, string name)
        {
            return _logicCore.RotationTopicCore.IsTopicExist(topicGroupId, topicId, name);
        }

        public bool IsTopicAssignedWithSameNameExistByTopic(Guid rotationTopicId, Guid assignedToId, string name)
        {
            return _repositories.RotationTopicRepository.Find(t => t.RelationId.Value == assignedToId && t.Id != rotationTopicId && t.Name.Equals(name)).Any();
        }


        public bool IsTopicAssignedWithSameNameExist(Guid destId, Guid assignedToId, string name, Models.RotationType rotationType = Models.RotationType.Swing)
        {

            RotationTopicGroup teamTopicGroup = (rotationType == Models.RotationType.Swing) ?
                                                    _logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Team, destId, TypeOfModuleSource.Draft)
                                                        .RotationTopicGroups.FirstOrDefault() :
                                                    _logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.Team, destId, TypeOfModuleSource.Draft)
                                                        .RotationTopicGroups.FirstOrDefault();

            return _repositories.RotationTopicRepository.Find(
                    t => t.RelationId == assignedToId && t.RotationTopicGroupId == teamTopicGroup.Id && t.Name.Equals(name)).Any();
        }

        public void UpdateHSETopic(HSETopicDialogViewModel model)
        {
            if (model.Id != null)
            {
                var topic = _logicCore.RotationTopicCore.GetTopic(model.Id.Value);

                _mapper.Map(model, topic);

                topic.FinalizeStatus = StatusOfFinalize.NotFinalized;

                _repositories.RotationTopicRepository.Update(topic);
                _repositories.Save();
            }
        }

        public HSETopicViewModel UpdateHSETopic(HSETopicViewModel model)
        {
            if (model.Id != null)
            {
                var topic = _logicCore.RotationTopicCore.GetTopic(model.Id.Value);


                if (topic.Description != model.Notes)
                {
                    topic.FinalizeStatus = StatusOfFinalize.NotFinalized;
                    model.IsFinalized = false;
                }


                if (topic.IsPinned == true && model.IsPinned == false)
                {
                    foreach (var task in topic.RotationTasks)
                    {
                        task.IsPinned = false;
                        _repositories.RotationTaskRepository.Update(task);
                    }
                }


                _mapper.Map(model, topic);

                if (model.IsNullReport)
                {
                    _logicCore.RotationTopicCore.CleaRotationTopicData(topic, false);
                    topic.FinalizeStatus = StatusOfFinalize.Finalized;
                }



                _logicCore.RotationTaskCore.UpdateTopicTasks(topic, false);

                _repositories.RotationTopicRepository.Update(topic);
                _repositories.Save();

                model = _mapper.Map<HSETopicViewModel>(topic);

                ActionLogger.Info("User Id: {0}, update Shift HSE Topic Id: {1}. Model - {2}.", model.OwnerId, model.Id, JsonConvert.SerializeObject(model));


            }
            return model;
        }

        public HSETopicViewModel GetHSETopic(Guid topicId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);
            if (topic != null)
            {
                return _mapper.Map<HSETopicViewModel>(topic);
            }

            return null;
        }

        public void AddHSETopic(HSEAddInlineTopicViewModel model)
        {
            if (model.RotationId.HasValue && model.Type.HasValue)
            {
                _logicCore.RotationTopicCore.AddRotationTopic(
                    model.RotationId.Value,
                    model.Type.Value,
                    _logicCore.RotationCore.GetRotation(model.RotationId.Value).DefaultBackToBackId,
                    model.Reference,
                    model.Notes);
            }
        }



        public CoreTopicViewModel UpdateCoreTopic(CoreTopicViewModel model)
        {
            if (model.Id != null)
            {
                var topic = _logicCore.RotationTopicCore.GetTopic(model.Id.Value);

                if (topic.Description != model.Notes)
                {
                    topic.FinalizeStatus = StatusOfFinalize.NotFinalized;
                    model.IsFinalized = false;
                }

                if (topic.IsPinned == true && model.IsPinned == false)
                {
                    foreach (var task in topic.RotationTasks)
                    {
                        task.IsPinned = false;
                        _repositories.RotationTaskRepository.Update(task);
                    }
                }


                _mapper.Map(model, topic);

                if (model.IsNullReport)
                {
                    _logicCore.RotationTopicCore.CleaRotationTopicData(topic, false);
                    topic.FinalizeStatus = StatusOfFinalize.Finalized;
                }


                _logicCore.RotationTaskCore.UpdateTopicTasks(topic, false);

                _repositories.RotationTopicRepository.Update(topic);
                _repositories.Save();

                model = _mapper.Map<CoreTopicViewModel>(topic);

                ActionLogger.Info("User Id: {0}, update Shift Core Topic Id: {1}. Model - {2}.", model.OwnerId, model.Id, JsonConvert.SerializeObject(model));

            }

            return model;
        }

        public CoreTopicViewModel GetCoreTopic(Guid topicId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);
            if (topic != null)
            {
                return _mapper.Map<CoreTopicViewModel>(topic);
            }

            return null;
        }

        public void AddRotationCoreTopic(RotationCoreAddInlineTopicViewModel model)
        {
            if (model.RotationId.HasValue && model.ProcessGroup.HasValue)
            {
                _logicCore.RotationTopicCore.AddRotationTopic(
                    model.RotationId.Value,
                    model.ProcessGroup.Value,
                    _logicCore.RotationCore.GetRotation(model.RotationId.Value).DefaultBackToBackId,
                    model.ProcessLocation,
                    model.Notes);
            }
        }

        public ProjectsTopicViewModel UpdateProjectTopic(ProjectsTopicViewModel model)
        {
            if (model.Id != null)
            {
                var topic = _logicCore.RotationTopicCore.GetTopic(model.Id.Value);

                if (topic.Description != model.Notes)
                {
                    topic.FinalizeStatus = StatusOfFinalize.NotFinalized;
                    model.IsFinalized = false;
                }



                if (topic.IsPinned == true && model.IsPinned == false)
                {
                    foreach (var task in topic.RotationTasks)
                    {
                        task.IsPinned = false;
                        _repositories.RotationTaskRepository.Update(task);
                    }
                }

                _mapper.Map(model, topic);

                if (model.IsNullReport)
                {
                    _logicCore.RotationTopicCore.CleaRotationTopicData(topic, false);
                    topic.FinalizeStatus = StatusOfFinalize.Finalized;
                }


                _logicCore.RotationTaskCore.UpdateTopicTasks(topic, false);

                _repositories.RotationTopicRepository.Update(topic);
                _repositories.Save();

                model = _mapper.Map<ProjectsTopicViewModel>(topic);

                ActionLogger.Info("User Id: {0}, update Shift Project Topic Id: {1}. Model - {2}.", model.OwnerId, model.Id, JsonConvert.SerializeObject(model));

            }
            return model;
        }

        public ProjectsTopicViewModel GetProjectTopic(Guid topicId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);
            if (topic != null)
            {
                return _mapper.Map<ProjectsTopicViewModel>(topic);
            }

            return null;
        }

        public void AddProjectTopic(ProjectAddInlineTopicViewModel model)
        {
            if (model.RotationId.HasValue && model.Project.HasValue)
            {
                _logicCore.RotationTopicCore.AddRotationTopic(
                    model.RotationId.Value,
                    model.Project.Value,
                    _logicCore.RotationCore.GetRotation(model.RotationId.Value).DefaultBackToBackId,
                    model.Reference,
                    model.Notes);
            }
        }



        public TeamTopicViewModel UpdateTeamTopic(TeamTopicViewModel model)
        {
            if (model.Id != null)
            {
                var topic = _logicCore.RotationTopicCore.GetTopic(model.Id.Value);

                if (topic.Description != model.Notes)
                {
                    topic.FinalizeStatus = StatusOfFinalize.NotFinalized;
                    model.IsFinalized = false;
                }


                if (topic.IsPinned == true && model.IsPinned == false)
                {
                    foreach (var task in topic.RotationTasks)
                    {
                        task.IsPinned = false;
                        _repositories.RotationTaskRepository.Update(task);
                    }
                }


                _mapper.Map(model, topic);

                if (model.IsNullReport)
                {
                    _logicCore.RotationTopicCore.CleaRotationTopicData(topic, false);
                    topic.FinalizeStatus = StatusOfFinalize.Finalized;
                }


                _logicCore.RotationTaskCore.UpdateTopicTasks(topic, false);

                _repositories.RotationTopicRepository.Update(topic);
                _repositories.Save();

                model = _mapper.Map<TeamTopicViewModel>(topic);

                model.TeamMember = topic.RelationId.HasValue == false || topic.RelationId.Value == Guid.Empty
                    ? "Other"
                    : _logicCore.UserProfileCore.GetUser(topic.RelationId.Value).FullName;

            }
            return model;
        }

        public TeamTopicViewModel GetTeamTopic(Guid topicId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);
            if (topic != null)
            {
                var model = _mapper.Map<TeamTopicViewModel>(topic);

                model.TeamMember = topic.RelationId.HasValue == false || topic.RelationId.Value == Guid.Empty
                    ? "Other"
                    : _logicCore.UserProfileCore.GetUser(topic.RelationId.Value).FullName;
                return model;
            }

            return null;
        }

        public void AddTeamTopic(TeamAddInlineTopicViewModel model)
        {

            if (model.RotationId.HasValue)
            {

                var rotationModule = _logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Team,
                                                                                        model.RotationId.Value,
                                                                                        TypeOfModuleSource.Draft);

                _logicCore.RotationTopicCore.AddRotationTopic(
                    model.RotationId.Value, rotationModule.RotationTopicGroups.FirstOrDefault(tg => tg.Enabled).Id,
                    rotationModule.Rotation.DefaultBackToBackId,
                    model.Reference,
                    model.Notes, model.TeamMember);
            }
        }


        public void UpdateCoreTopic(CoreTopicDialogViewModel model)
        {
            if (model.Id != null)
            {
                var topic = _logicCore.RotationTopicCore.GetTopic(model.Id.Value);

                _mapper.Map(model, topic);

                topic.FinalizeStatus = StatusOfFinalize.NotFinalized;

                _repositories.RotationTopicRepository.Update(topic);
                _repositories.Save();
            }

        }


        public void UpdateProjectTopic(ProjectTopicDialogViewModel model)
        {
            if (model.Id != null)
            {
                var topic = _logicCore.RotationTopicCore.GetTopic(model.Id.Value);

                _mapper.Map(model, topic);

                topic.FinalizeStatus = StatusOfFinalize.NotFinalized;

                _repositories.RotationTopicRepository.Update(topic);
                _repositories.Save();
            }

        }

        public void UpdateTeamTopic(TeamTopicDialogViewModel model)
        {
            if (model.Id != null)
            {
                var topic = _logicCore.RotationTopicCore.GetTopic(model.Id.Value);

                _mapper.Map(model, topic);

                topic.FinalizeStatus = StatusOfFinalize.NotFinalized;

                _repositories.RotationTopicRepository.Update(topic);

                _repositories.Save();
            }

        }


        public HSEAddInlineTopicViewModel PopulateHSETopicInlineModel(Guid rotationId)
        {
            var model = new HSEAddInlineTopicViewModel
            {
                RotationId = rotationId,
                Types =
                    _services.RotationTopicGroupService.GetRotationModuleTopicGroupsList(
                        _logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.HSE, rotationId, TypeOfModuleSource.Draft).Id)
            };

            return model;
        }

        public HSETopicDialogViewModel PopulateHSETopicDialogModel(Guid topicId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            var model = _mapper.Map<HSETopicDialogViewModel>(topic);

            model.RotationTopicGroups =
                _services.RotationTopicGroupService.GetRotationModuleTopicGroupsList(
                    topic.RotationTopicGroup.RotationModuleId);

            return model;
        }



        public CoreTopicDialogViewModel PopulateCoreTopicDialogModel(Guid topicId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            var model = _mapper.Map<CoreTopicDialogViewModel>(topic);

            model.RotationTopicGroups =
                _services.RotationTopicGroupService.GetRotationModuleTopicGroupsList(
                    topic.RotationTopicGroup.RotationModuleId);

            return model;
        }

        public RotationCoreAddInlineTopicViewModel PopulateCoreTopicInlineModel(Guid rotationId)
        {
            var model = new RotationCoreAddInlineTopicViewModel
            {
                RotationId = rotationId,

            };

            var processGroups = _services.RotationTopicGroupService.GetRotationModuleTopicGroupsList(
                _logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Core, rotationId, TypeOfModuleSource.Draft).Id);

            model.ProcessGroups = processGroups;


            return model;
        }


        public ProjectTopicDialogViewModel PopulateProjectTopicDialogModel(Guid topicId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            var model = _mapper.Map<ProjectTopicDialogViewModel>(topic);

            model.RotationTopicGroups =
                _services.RotationTopicGroupService.GetRotationModuleTopicGroupsList(
                    topic.RotationTopicGroup.RotationModuleId);

            return model;
        }


        public ProjectAddInlineTopicViewModel PopulateProjectTopicInlineModel(Guid rotationId)
        {
            var model = new ProjectAddInlineTopicViewModel
            {
                RotationId = rotationId,
                Projects =
                    _services.RotationTopicGroupService.GetRotationModuleTopicGroupsList(
                        _logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Project, rotationId, TypeOfModuleSource.Draft).Id)
            };

            return model;
        }


        public TeamTopicDialogViewModel PopulateTeamTopicDialogModel(Guid topicId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            var model = _mapper.Map<TeamTopicDialogViewModel>(topic);

            var userTeamId = topic.RotationTopicGroup.RotationModule.Rotation != null ?
                topic.RotationTopicGroup.RotationModule.Rotation.RotationOwner.TeamId :
                topic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwner.TeamId;

            if (userTeamId != null)
            {
                model.TeamMembers = _services.TeamService.GetTeamMembersListWidthOther(userTeamId);
            }

            return model;
        }

        /// <summary>
        /// Populate team topic from shift
        /// </summary>
        /// <param name="topicId"></param>
        /// <returns view-model="TeamTopicDialogViewModel"></returns>
        public TeamTopicDialogViewModel PopulateShiftTeamTopicDialogModel(Guid topicId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            var model = _mapper.Map<TeamTopicDialogViewModel>(topic);

            var userTeamId = topic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwner.TeamId;

            if (userTeamId != null)
            {
                model.TeamMembers = _services.TeamService.GetTeamMembersListWidthOther(userTeamId);
            }

            return model;
        }

        public TeamAddInlineTopicViewModel PopulateTeamTopicInlineModel(Guid rotationId)
        {

            var rotation = _logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Team, rotationId, TypeOfModuleSource.Draft).Rotation;

            var model = new TeamAddInlineTopicViewModel
            {
                RotationId = rotationId,
                TeamMembers = _services.TeamService.GetTeamMembersListWidthOther(rotation.RotationOwner.TeamId)
            };

            return model;
        }



        public ManagerCommentsViewModel PopulateManagerCommentViewModel(Guid topicId)
        {
            var model = new ManagerCommentsViewModel
            {
                Id = topicId,
                Notes = _logicCore.RotationTopicCore.GetTopicManagerComments(topicId)
            };

            return model;

        }


        public bool IsMyTopic(Guid userId, Guid topicId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            if (topic.RotationTopicGroup.RotationModule.Rotation != null)
            {
                if (topic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId == userId)
                {
                    return true;
                }
            }
            else
            {
                if (topic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId == userId)
                {
                    return true;
                }

            }


            return false;
        }

        public bool IsMyShiftTopic(Guid userId, Guid topicId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            if (topic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId == userId)
            {
                return true;
            }
            return false;
        }

        public void UpdateTopicManagerComment(ManagerCommentsViewModel model)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(model.Id);

            topic.ManagerComments = model.Notes;

            _repositories.RotationTopicRepository.Update(topic);
            _repositories.Save();


            var topicOwner = topic.RotationTopicGroup.RotationModule.Rotation != null
                ? topic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId
                : topic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId;

            _logicCore.NotificationCore.NotificationTrigger
                                        .Send_ManagerAddedComment(topicOwner, topic.Id);
        }

        public void CarryforwardTopic(Guid topicId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);
            if (topic == null)
            {
                throw new ObjectNotFoundException(string.Format("RotationTopic with Id: {0} was not found.", topicId));
            }

            if (!topic.TempateTopicId.HasValue)
            {
                _logicCore.RotationTopicCore.CarryforwardTopic(topic);
            }

        }

        public bool RemoveTopic(Guid topicId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);
            if (topic != null)
            {
                if (this.IsShiftTopic(topicId))
                    return _logicCore.RotationTopicCore.RemoveShiftTopic(topic);
                else
                    return _logicCore.RotationTopicCore.RemoveTopic(topic);
            }
            return false;
        }

        public ModuleType GetTopicModuleType(Guid topicid)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicid);
            ModuleType moduleType = (ModuleType)topic.RotationTopicGroup.RotationModule.Type;

            return moduleType;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="shiftId"></param>
        /// <returns></returns>
        public ShiftCoreAddInlineTopicViewModel PopulateShiftCoreTopicInlineModel(Guid shiftId)
        {
            var processGroups = _services.RotationTopicGroupService.GetRotationModuleTopicGroupsList(
                _logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.Core, shiftId, TypeOfModuleSource.Draft).Id);

            var model = new ShiftCoreAddInlineTopicViewModel
            {
                ShiftId = shiftId,
                ProcessGroups = processGroups
            };

            return model;
        }

        public void AddShiftCoreTopic(ShiftCoreAddInlineTopicViewModel model)
        {
            if (model.ShiftId.HasValue && model.ProcessGroup.HasValue)
            {
                _logicCore.RotationTopicCore.AddShiftTopic(
                    model.ShiftId.Value,
                    model.ProcessGroup.Value,
                    _logicCore.ShiftCore.GetShift(model.ShiftId.Value).ShiftRecipientId,
                    model.ProcessLocation,
                    model.Notes);
            }
        }

        public void AddShiftHSETopic(ShiftHSEAddInlineTopicViewModel model)
        {
            if (model.ShiftId.HasValue && model.Type.HasValue)
            {
                _logicCore.RotationTopicCore.AddShiftTopic(
                    model.ShiftId.Value,
                    model.Type.Value,
                    _logicCore.ShiftCore.GetShift(model.ShiftId.Value).ShiftRecipientId,
                    model.Reference,
                    model.Notes);
            }
        }

        public ShiftHSEAddInlineTopicViewModel PopulateShiftHSETopicInlineModel(Guid shiftId)
        {
            ShiftHSEAddInlineTopicViewModel model = new ShiftHSEAddInlineTopicViewModel
            {
                ShiftId = shiftId,
                Types = _services.RotationTopicGroupService.GetRotationModuleTopicGroupsList(
                            _logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.HSE, shiftId, TypeOfModuleSource.Draft).Id)
            };

            return model;
        }

        public ShiftProjectAddInlineTopicViewModel PopulateShiftProjectTopicInlineModel(Guid shiftId)
        {
            ShiftProjectAddInlineTopicViewModel model = new ShiftProjectAddInlineTopicViewModel
            {
                ShiftId = shiftId,
                Projects = _services.RotationTopicGroupService.GetRotationModuleTopicGroupsList(
                            _logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.Project, shiftId, TypeOfModuleSource.Draft).Id)
            };

            return model;
        }

        public void AddShiftProjectTopic(ShiftProjectAddInlineTopicViewModel model)
        {
            if (model.ShiftId.HasValue && model.Project.HasValue)
            {
                _logicCore.RotationTopicCore.AddShiftTopic(
                    model.ShiftId.Value,
                    model.Project.Value,
                    _logicCore.ShiftCore.GetShift(model.ShiftId.Value).ShiftRecipientId,
                    model.Reference,
                    model.Notes);
            }
        }

        public ShiftTeamAddInlineTopicViewModel PopulateShiftTeamTopicInlineModel(Guid shiftId)
        {
            Shift shift = _logicCore.ShiftCore.GetShift(shiftId);

            ShiftTeamAddInlineTopicViewModel model = new ShiftTeamAddInlineTopicViewModel
            {
                ShiftId = shiftId,
                TeamMembers = _services.TeamService.GetTeamMembersListWidthOther(shift.Rotation.RotationOwner.TeamId)
            };

            return model;
        }

        public void AddShiftTeamTopic(ShiftTeamAddInlineTopicViewModel model)
        {

            if (model.ShiftId.HasValue)
            {

                var shiftModule = _logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.Team,
                                                                                        model.ShiftId.Value,
                                                                                        TypeOfModuleSource.Draft);

                _logicCore.RotationTopicCore.AddShiftTopic(
                    model.ShiftId.Value, shiftModule.RotationTopicGroups.FirstOrDefault(tg => tg.Enabled).Id,
                    shiftModule.Shift.ShiftRecipientId,
                    model.Reference,
                    model.Notes, model.TeamMember);
            }
        }

        /// <summary>
        /// Check whether the topic is Shift
        /// </summary>
        /// <param name="topicId"></param>
        /// <returns></returns>
        public bool IsShiftTopic(Guid topicId)
        {
            return !_logicCore.RotationTopicCore.GetTopic(topicId).RotationTopicGroup.RotationModule.RotationId.HasValue;
        }

    }
}
