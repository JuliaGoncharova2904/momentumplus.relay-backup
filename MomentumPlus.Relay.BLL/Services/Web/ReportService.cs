﻿using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Core.Models;
using System.Text;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class ReportService : IReportService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public ReportService(IRepositoriesUnitOfWork repositoriesUnitOfWork, IServicesUnitOfWork services, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            _logicCore = logicCore;
            _services = services;
        }

        public ReportPreviewViewModel PopulateRotationReportViewModel(Guid rotationId, ModuleSourceType sourceType, Guid? selectedRotationId = null)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            ReportPreviewViewModel model = new ReportPreviewViewModel
            {
                IsReceived = sourceType == ModuleSourceType.Received,
                IsRotationFinished = rotation.State == RotationState.SwingEnded || rotation.State == RotationState.Expired,
                SelectedRotationId = selectedRotationId,
                CompanyLogoImageId = _logicCore.AdministrationCore.GetCompanyLogoImageId(),
                From = (sourceType == ModuleSourceType.Received) ? rotation.DefaultBackToBack.FullName : rotation.RotationOwner.FullName,
                To = (sourceType == ModuleSourceType.Received) ? rotation.RotationOwner.FullName : rotation.DefaultBackToBack.FullName,
                RotationId = rotation.Id,
                Dates = rotation.StartDate.HasValue ? "[" + rotation.StartDate.Value.FormatWithDateMonthAndFullYear() + " - " + rotation.StartDate.Value.AddDays(rotation.DayOn - 1).FormatWithDateMonthAndFullYear() + "]" : "...",
                HSEModule = _mapper.Map<ReportModuleViewModel>(_logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.HSE, rotationId, (TypeOfModuleSource)sourceType)),
                CoreModule = _mapper.Map<ReportModuleViewModel>(_logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Core, rotationId, (TypeOfModuleSource)sourceType)),
                ProjectModule = _mapper.Map<ReportModuleViewModel>(_logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Project, rotationId, (TypeOfModuleSource)sourceType)),
                TeamModule = _mapper.Map<ReportModuleViewModel>(_logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Team, rotationId, (TypeOfModuleSource)sourceType)),
                DailyNotesModule = _mapper.Map<ReportModuleViewModel>(_logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.DailyNote, rotationId, (TypeOfModuleSource)sourceType))
            };


            model.HSEModule = model.HSEModule ?? new ReportModuleViewModel { Name = "HSE" };
            model.CoreModule = model.CoreModule ?? new ReportModuleViewModel { Name = "Core" };
            model.ProjectModule = model.ProjectModule ?? new ReportModuleViewModel { Name = "Project" };
            model.TeamModule = model.TeamModule ?? new ReportModuleViewModel { Name = "Team" };
            model.DailyNotesModule = model.DailyNotesModule ?? new ReportModuleViewModel();
            model.DailyNotesModule.Name = "Daily Notes";


            if (model.DailyNotesModule.Topics != null && model.DailyNotesModule.Topics.Any())
            {
                model.DailyNotesModule.Topics = model.DailyNotesModule.Topics.OrderBy(t => DateTime.Parse(t.Name));
            }


            if (selectedRotationId.HasValue)
            {
                this.FilteRotationTopicsBySourceRotation(model, selectedRotationId.Value);
            }

            this.OrderModuleTopics(model.HSEModule);
            this.OrderModuleTopics(model.CoreModule);
            this.OrderModuleTopics(model.ProjectModule);
            this.OrderModuleTopics(model.TeamModule);

            this.FillTeamModuleTopicsNames(model.TeamModule);
            this.FillDailyNotesModuleTopicsNames(model.DailyNotesModule);

            return model;
        }

        private void FillTeamModuleTopicsNames(ReportModuleViewModel teamModule)
        {
            if (teamModule.Topics != null && teamModule.Topics.Any())
            {
                foreach (var teamTopic in teamModule.Topics)
                {
                    teamTopic.TopicGroupName = !teamTopic.RelationId.HasValue || teamTopic.RelationId.Value == Guid.Empty
                        ? "Other"
                        : _logicCore.UserProfileCore.GetUser(teamTopic.RelationId.Value).FullName;
                }
            }
        }

        private void OrderModuleTopics(ReportModuleViewModel rotationModule)
        {
            if (rotationModule.Topics != null && rotationModule.Topics.Any())
            {
                rotationModule.Topics = rotationModule.Topics.OrderBy(t => t.TopicGroupName).ThenBy(t => t.Name);
            }
        }

        private void FillDailyNotesModuleTopicsNames(ReportModuleViewModel dailyNotesModule)
        {
            if (dailyNotesModule.Topics != null && dailyNotesModule.Topics.Any())
            {
                foreach (var teamTopic in dailyNotesModule.Topics)
                {
                    teamTopic.Name = DateTime.Parse(teamTopic.TopicGroupName).FormatWithDayNameDateAndFullYear();
                    teamTopic.TopicGroupName = "";
                }
            }
        }

        private void FilteRotationTopicsBySourceRotation(ReportPreviewViewModel model, Guid sourceRotationId)
        {
            model.HSEModule.Topics = model.HSEModule.Topics.Where(t => t.FromRotationId == sourceRotationId);
            model.CoreModule.Topics = model.CoreModule.Topics.Where(t => t.FromRotationId == sourceRotationId);
            model.ProjectModule.Topics = model.ProjectModule.Topics.Where(t => t.FromRotationId == sourceRotationId);
            model.TeamModule.Topics = model.TeamModule.Topics.Where(t => t.FromRotationId == sourceRotationId);

            if (model.DailyNotesModule.Topics != null && model.DailyNotesModule.Topics.Any())
            {
                model.DailyNotesModule.Topics = model.DailyNotesModule.Topics.Where(t => t.FromRotationId == sourceRotationId).OrderBy(t => DateTime.Parse(t.Name));

            }
        }

        public ShiftReportPreviewViewModel PopulateShiftReportViewModel(Guid shiftId, ModuleSourceType sourceType, Guid? selectedShiftId = null)
        {
            Shift shift = _logicCore.ShiftCore.GetShift(shiftId);

            ShiftReportPreviewViewModel model = new ShiftReportPreviewViewModel
            {
                IsReceived = sourceType == ModuleSourceType.Received,
                IsShiftFinished = shift.State == ShiftState.Finished || shift.State == ShiftState.Break,
                SelectedShiftId = selectedShiftId,
                CompanyLogoImageId = _logicCore.AdministrationCore.GetCompanyLogoImageId(),
                From = (sourceType == ModuleSourceType.Received) ? shift.ShiftRecipient.FullName : shift.Rotation.RotationOwner.FullName,
                To = (sourceType == ModuleSourceType.Received) ? shift.Rotation.RotationOwner.FullName : shift.ShiftRecipient.FullName,
                ShiftId = shift.Id,
                Dates = shift.StartDateTime.HasValue ? "[" + shift.StartDateTime.Value.FormatWithDateMonthFullYearAndTime() + " - " + shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes).FormatWithDateMonthFullYearAndTime() + "]" : "...",
                HSEModule = _mapper.Map<ReportShiftModuleViewModel>(_logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.HSE, shiftId, (TypeOfModuleSource)sourceType)),
                CoreModule = _mapper.Map<ReportShiftModuleViewModel>(_logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.Core, shiftId, (TypeOfModuleSource)sourceType)),
                ProjectModule = _mapper.Map<ReportShiftModuleViewModel>(_logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.Project, shiftId, (TypeOfModuleSource)sourceType)),
                TeamModule = _mapper.Map<ReportShiftModuleViewModel>(_logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.Team, shiftId, (TypeOfModuleSource)sourceType)),
            };

            model.HSEModule = model.HSEModule ?? new ReportShiftModuleViewModel { Name = "HSE" };
            model.CoreModule = model.CoreModule ?? new ReportShiftModuleViewModel { Name = "Core" };
            model.ProjectModule = model.ProjectModule ?? new ReportShiftModuleViewModel { Name = "Project" };
            model.TeamModule = model.TeamModule ?? new ReportShiftModuleViewModel { Name = "Team" };

            if (selectedShiftId.HasValue)
            {
                this.FilteRotationTopicsBySourceShift(model, selectedShiftId.Value);
                model.ReceivedFrom = "from " + _logicCore.ShiftCore.GetShift(selectedShiftId.Value).Rotation.RotationOwner.FullName;
            }

            if (selectedShiftId.HasValue && sourceType == ModuleSourceType.Received)
            {
                Shift selectedShift = _logicCore.ShiftCore.GetShift(selectedShiftId.Value);



                model.Dates = selectedShift.StartDateTime.HasValue
                    ? "[" + selectedShift.StartDateTime.Value.FormatWithDateMonthFullYearAndTime() + " - " +
                      selectedShift.StartDateTime.Value.AddMinutes(selectedShift.WorkMinutes).FormatWithDateMonthFullYearAndTime() + "]"
                    : "...";
            }


            if (sourceType == ModuleSourceType.Received && !selectedShiftId.HasValue)
            {
                var handoverFromShift = shift.HandoverFromShifts.ToList().OrderByDescending(s => s.StartDateTime.Value.AddMinutes(shift.WorkMinutes)).FirstOrDefault();


                model.Dates = handoverFromShift?.StartDateTime != null
                    ? "[" + handoverFromShift.StartDateTime.Value.FormatWithDateMonthFullYearAndTime() + " - " +
                      handoverFromShift.StartDateTime.Value.AddMinutes(handoverFromShift.WorkMinutes).FormatWithDateMonthFullYearAndTime() + "]"
                    : "...";
            }

            this.OrderShiftModuleTopics(model.HSEModule);
            this.OrderShiftModuleTopics(model.CoreModule);
            this.OrderShiftModuleTopics(model.ProjectModule);
            this.OrderShiftModuleTopics(model.TeamModule);

            this.FillShiftTeamModuleTopicsNames(model.TeamModule);

            return model;
        }

        private void FilteRotationTopicsBySourceShift(ShiftReportPreviewViewModel model, Guid sourceShiftId)
        {
            model.HSEModule.Topics = model.HSEModule.Topics.Where(t => t.FromShiftId == sourceShiftId);
            model.CoreModule.Topics = model.CoreModule.Topics.Where(t => t.FromShiftId == sourceShiftId);
            model.ProjectModule.Topics = model.ProjectModule.Topics.Where(t => t.FromShiftId == sourceShiftId);
            model.TeamModule.Topics = model.TeamModule.Topics.Where(t => t.FromShiftId == sourceShiftId);
        }

        private void OrderShiftModuleTopics(ReportShiftModuleViewModel rotationModule)
        {
            if (rotationModule.Topics != null && rotationModule.Topics.Any())
            {
                rotationModule.Topics = rotationModule.Topics.OrderBy(t => t.TopicGroupName).ThenBy(t => t.Name);
            }
        }

        private void FillShiftTeamModuleTopicsNames(ReportShiftModuleViewModel teamModule)
        {
            if (teamModule.Topics != null && teamModule.Topics.Any())
            {
                foreach (var teamTopic in teamModule.Topics)
                {
                    teamTopic.TopicGroupName = !teamTopic.RelationId.HasValue || teamTopic.RelationId.Value == Guid.Empty
                        ? "Other"
                        : _logicCore.UserProfileCore.GetUser(teamTopic.RelationId.Value).FullName;
                }
            }
        }


        #region QME Report

        /// <summary>
        /// Get viewModel for QME pdf report 
        /// </summary>
        /// <param name="sourceId">Guid sourceId, rotation or shift Id</param>
        /// <param name="sourceType">ModuleSourceType sourceType, type of source (draft or received)</param>
        /// <param name="selectedSourceId">Guid selectedSourceId, if source have multiple embedded report this id of selected report</param>
        /// <param name="isShift"></param>
        /// <returns>QMEReportViewModel viewModel, viewModel of report</returns>
        public QMEReportViewModel PopulateQMERotationReportViewModel(Guid sourceId, ModuleSourceType sourceType, bool isShift, Guid? selectedSourceId = null)
        {
            Shift shift = isShift ? _logicCore.ShiftCore.GetShift(sourceId) : null;

            Rotation rotation = isShift
                ? _logicCore.ShiftCore.GetShift(sourceId).Rotation
                : _logicCore.RotationCore.GetRotation(sourceId);

            QMEReportViewModel model = new QMEReportViewModel
            {
                SourceId = sourceId,
                IsReceived = sourceType == ModuleSourceType.Received,
                ReporType = sourceType,
                SelectedSourceId = selectedSourceId,
                CompanyLogoImageId = _logicCore.AdministrationCore.GetCompanyLogoImageId(),
                From = isShift
                            ? (sourceType == ModuleSourceType.Received) ? shift.ShiftRecipient.FullName : shift.Rotation.RotationOwner.FullName
                            : (sourceType == ModuleSourceType.Received) ? rotation.DefaultBackToBack.FullName : rotation.RotationOwner.FullName,
                To = isShift
                            ? (sourceType == ModuleSourceType.Received) ? shift.Rotation.RotationOwner.FullName : shift.ShiftRecipient.FullName
                            : (sourceType == ModuleSourceType.Received) ? rotation.RotationOwner.FullName : rotation.DefaultBackToBack.FullName,
                SharedUserInitials = UserInitialsWhoReceivedSharedReport(sourceId),
                Dates = isShift
                            ? shift.StartDateTime.HasValue ? "[" + shift.StartDateTime.Value.FormatWithDateMonthFullYearAndTime() + " - " + shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes).FormatWithDateMonthFullYearAndTime() + "]" : "..."
                            : rotation.StartDate.HasValue ? "[" + rotation.StartDate.Value.FormatWithDateMonthAndFullYear() + " - " + rotation.StartDate.Value.AddDays(rotation.DayOn - 1).FormatWithDateMonthAndFullYear() + "]" : "...",
                HSEModule = isShift
                            ? ModuleMapToViewModel(_logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.HSE, sourceId, (TypeOfModuleSource)sourceType), isShift)
                            : ModuleMapToViewModel(_logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.HSE, sourceId, (TypeOfModuleSource)sourceType), isShift),
                CoreModule = isShift
                            ? ModuleMapToViewModel(_logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.Core, sourceId, (TypeOfModuleSource)sourceType), isShift)
                            : ModuleMapToViewModel(_logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Core, sourceId, (TypeOfModuleSource)sourceType), isShift),
                ProjectModule = isShift
                            ? ModuleMapToViewModel(_logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.Project, sourceId, (TypeOfModuleSource)sourceType), isShift)
                            : ModuleMapToViewModel(_logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Project, sourceId, (TypeOfModuleSource)sourceType), isShift),
                TeamModule = isShift
                            ? ModuleMapToViewModel(_logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.Team, sourceId, (TypeOfModuleSource)sourceType), isShift)
                            : ModuleMapToViewModel(_logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Team, sourceId, (TypeOfModuleSource)sourceType), isShift),
                TaskModule = TaskModuleMapToViewModel(sourceId, isShift, (TypeOfModuleSource)sourceType),
                DailyNotesModule = HandoverTopicModuleMapToViewModel(_logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.DailyNote, sourceId, (TypeOfModuleSource)sourceType), isShift)
            };

            model.HSEModule = model.HSEModule ?? new QMEReportModuleViewModel() { Name = "HSE" };
            model.CoreModule = model.CoreModule ?? new QMEReportModuleViewModel() { Name = "Core" };
            model.ProjectModule = model.ProjectModule ?? new QMEReportModuleViewModel() { Name = "Project" };
            model.TeamModule = model.TeamModule ?? new QMEReportModuleViewModel() { Name = "Team" };
            model.TaskModule = model.TaskModule ?? new QMEReportTaskModuleViewModel { Name = "Tasks" };
            model.DailyNotesModule = model.DailyNotesModule ?? new HandoverReportModuleViewModel() { Name = "DailyNote" };


            if (selectedSourceId.HasValue)
            {
                model.CoreModule.Topics = model.CoreModule.Topics != null
                                                ? model.CoreModule.Topics.Where(t => t.FromSourceId == selectedSourceId)
                                                : null;
                model.HSEModule.Topics = model.HSEModule.Topics != null
                                                ? model.HSEModule.Topics.Where(t => t.FromSourceId == sourceId)
                                                : new List<QMEReportTopicViewModel>();
                model.TeamModule.Topics = model.TeamModule.Topics != null
                                                ? model.TeamModule.Topics.Where(t => t.FromSourceId == selectedSourceId)
                                                : new List<QMEReportTopicViewModel>();
                model.ProjectModule.Topics = model.ProjectModule.Topics != null
                                                ? model.ProjectModule.Topics.Where(t => t.FromSourceId == selectedSourceId)
                                                : new List<QMEReportTopicViewModel>();
                model.DailyNotesModule.Topics = model.DailyNotesModule.Topics != null
               ? model.DailyNotesModule.Topics.Where(t => t.FromSourceId == selectedSourceId)
               : new List<HandoverReportTopicViewModel>();
            }

            model.HSEModule.Topics = model.HSEModule.Topics != null
                                                ? model.HSEModule.Topics.OrderBy(t => t.TopicGroupName).ThenBy(t => t.Name).ToList()
                                                : new List<QMEReportTopicViewModel>();
            model.CoreModule.Topics = model.CoreModule.Topics != null
                                                ? model.CoreModule.Topics.OrderBy(t => t.TopicGroupName).ThenBy(t => t.Name).ToList()
                                                : new List<QMEReportTopicViewModel>();
            model.TeamModule.Topics = model.TeamModule.Topics != null
                                                ? model.TeamModule.Topics.OrderBy(t => t.TopicGroupName).ThenBy(t => t.Name).ToList()
                                                : new List<QMEReportTopicViewModel>();
            model.ProjectModule.Topics = model.ProjectModule.Topics != null
                                                ? model.ProjectModule.Topics.OrderBy(t => t.TopicGroupName).ThenBy(t => t.Name).ToList()
                                                : new List<QMEReportTopicViewModel>();
            model.TaskModule.Tasks = model.TaskModule.Tasks != null
                                                ? model.TaskModule.Tasks.OrderBy(t => t.Reference).ThenBy(t => t.Name).ToList()
                                                : new List<QMEReportTaskViewModel>();
            model.DailyNotesModule.Topics = model.DailyNotesModule.Topics != null
              ? model.DailyNotesModule.Topics.OrderBy(t => t.TopicGroupName).ThenBy(t => t.Name).ToList()
              : new List<HandoverReportTopicViewModel>();

            if (model.TeamModule.Topics != null && model.TeamModule.Topics.Any())
            {
                foreach (var teamTopic in model.TeamModule.Topics)
                {
                    teamTopic.TopicGroupName = !teamTopic.RelationId.HasValue || teamTopic.RelationId.Value == Guid.Empty
                        ? "Other"
                        : _logicCore.UserProfileCore.GetUser(teamTopic.RelationId.Value).FullName;
                }
            }

            return model;
        }

        /// <summary>
        /// Get all topic from module and map to viewModel
        /// </summary>
        /// <param name="rotationModule">RotationModule rotationModule, rotation or shift module</param>
        /// <param name="isShift">bool isShift, if this module from shift</param>
        /// <returns>QMEReportModuleViewModel viewModel, viewModel with collection of topic and module name</returns>
        private QMEReportModuleViewModel ModuleMapToViewModel(RotationModule rotationModule, bool isShift)
        {
            if (rotationModule != null)
            {
                var viewModel = new QMEReportModuleViewModel
                {
                    Name = rotationModule.Type.ToString(),
                    IsTwoColInRow = rotationModule.Type == TypeOfModule.Core
                };

                viewModel.Topics = rotationModule.RotationTopicGroups.SelectMany(tg => tg.RotationTopics).Where(t => t.Enabled).Select(t => new
                    QMEReportTopicViewModel
                {
                    Name = t.Name,
                    Description = t.IsNullReport ? "Nothing to report this time." : t.Description,
                    TopicGroupName = t.RotationTopicGroup.Name,
                    IsNullReport = t.IsNullReport,
                    Tag = t.SearchTags != null ? t.SearchTags.Split(',').FirstOrDefault() : "",
                    FromSourceId = t.AncestorTopic != null
                            ? isShift
                                ? t.AncestorTopic.RotationTopicGroup.RotationModule.ShiftId
                                : t.AncestorTopic.RotationTopicGroup.RotationModule.RotationId
                            : (Guid?)null,
                    RelationId = t.RelationId
                });

                return viewModel;
            }
            return new QMEReportModuleViewModel();
        }

        /// <summary>
        /// Get all rotation task by sourceId (rotation of shift) and map to viewModel
        /// </summary>
        /// <param name="sourceId">Guid sourceId, rotation or shift id</param>
        /// <param name="isShift">bool isShift, if this source shift</param>
        /// <param name="sourceType">TypeOfModuleSource sourceType, type of rotation or shift modules</param>
        /// <returns>QMEReportTaskModuleViewModel viewModel, viewModel with collection of task and module name</returns>
        private QMEReportTaskModuleViewModel TaskModuleMapToViewModel(Guid sourceId, bool isShift, TypeOfModuleSource sourceType)
        {
            var tasks = isShift
                ? _logicCore.RotationTaskCore.GetAllShiftTasks(sourceId, sourceType).ToList()
                : _logicCore.RotationTaskCore.GetAllRotationTasks(sourceId, sourceType).ToList();

            if (sourceType == TypeOfModuleSource.Received)
            {
                tasks = tasks.Where(t => t.Status == StatusOfTask.Default && t.Name != "For information").ToList();
            }

            var model = new QMEReportTaskModuleViewModel
            {
                Name = "Tasks",
                Tasks = tasks != null
                    ? tasks.Select(t => new QMEReportTaskViewModel
                    {
                        Name = t.Name,
                        Description = t.Description,
                        Priority = t.Priority.ToString(),
                        Reference = t.RotationTopic != null ? t.RotationTopic.Name : "",
                        IsNullReport = t.IsNullReport,
                        Date = t.Deadline.FormatDayMonthYear()
                    })
                    : new List<QMEReportTaskViewModel>()
            };

            return model;
        }

        /// <summary>
        /// Concatenated all user initials who received share report
        /// </summary>
        /// <param name="sourceId">Guid sourceId, rotation or shift Id</param>
        /// <returns>string, concatenated all user initials who received share report</returns>
        private string UserInitialsWhoReceivedSharedReport(Guid sourceId)
        {
            var sharedReports = _repositories.ReportSharingRelationRepository.Find(s => s.ReportId == sourceId);

            if (sharedReports.Any())
            {
                var recipientUsers = sharedReports.Select(s => s.Recipient).ToList();

                List<string> sharedUserInitials = new List<string>();

                foreach (var user in recipientUsers)
                {
                    StringBuilder str = new StringBuilder();

                    if (!string.IsNullOrEmpty(user.FirstName))
                        str.Append(user.FirstName[0]);

                    if (!string.IsNullOrEmpty(user.LastName))
                        str.Append(user.LastName[0]);

                    sharedUserInitials.Add(str.ToString().ToUpper());
                }

                return string.Join(", ", sharedUserInitials);
            }

            return null;
        }

        #endregion

        #region Handover Report

        /// <summary>
        /// Get viewModel for Handover pdf report 
        /// </summary>
        /// <param name="sourceId">Guid sourceId, rotation or shift Id</param>
        /// <param name="sourceType">ModuleSourceType sourceType, type of source (draft or received)</param>
        /// <param name="selectedSourceId">Guid selectedSourceId, if source have multiple embedded report this id of selected report</param>
        /// <param name="isShift"></param>
        /// <returns>HandoverReportViewModel viewModel, viewModel of report</returns>
        public HandoverReportViewModel PopulateHandoverReportViewModel(Guid sourceId, ModuleSourceType sourceType, bool isShift, Guid? selectedSourceId = null)
        {
            Shift shift = isShift ? _logicCore.ShiftCore.GetShift(sourceId) : null;

            Rotation rotation = isShift
                ? _logicCore.ShiftCore.GetShift(sourceId).Rotation
                : _logicCore.RotationCore.GetRotation(sourceId);

            HandoverReportViewModel model = new HandoverReportViewModel
            {
                SourceId = sourceId,
                IsReceived = sourceType == ModuleSourceType.Received,
                ReporType = sourceType,
                SelectedSourceId = selectedSourceId,
                CompanyLogoImageId = _logicCore.AdministrationCore.GetCompanyLogoImageId(),
                From = isShift
                    ? (sourceType == ModuleSourceType.Received) ? shift.ShiftRecipient.FullName : shift.Rotation.RotationOwner.FullName
                    : (sourceType == ModuleSourceType.Received) ? rotation.DefaultBackToBack.FullName : rotation.RotationOwner.FullName,
                To = isShift
                    ? (sourceType == ModuleSourceType.Received) ? shift.Rotation.RotationOwner.FullName : shift.ShiftRecipient.FullName
                    : (sourceType == ModuleSourceType.Received) ? rotation.RotationOwner.FullName : rotation.DefaultBackToBack.FullName,
                SharedUserInitials = UserInitialsWhoReceivedSharedReport(sourceId),
                Dates = isShift
                    ? shift.StartDateTime.HasValue ? "[" + shift.StartDateTime.Value.FormatWithDateMonthFullYearAndTime() + " - " + shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes).FormatWithDateMonthFullYearAndTime() + "]" : "..."
                    : rotation.StartDate.HasValue ? "[" + rotation.StartDate.Value.FormatWithDateMonthAndFullYear() + " - " + rotation.StartDate.Value.AddDays(rotation.DayOn - 1).FormatWithDateMonthAndFullYear() + "]" : "...",
                HSEModule = isShift
                    ? HandoverTopicModuleMapToViewModel(_logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.HSE, sourceId, (TypeOfModuleSource)sourceType), isShift)
                    : HandoverTopicModuleMapToViewModel(_logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.HSE, sourceId, (TypeOfModuleSource)sourceType), isShift),
                CoreModule = isShift
                    ? HandoverTopicModuleMapToViewModel(_logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.Core, sourceId, (TypeOfModuleSource)sourceType), isShift)
                    : HandoverTopicModuleMapToViewModel(_logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Core, sourceId, (TypeOfModuleSource)sourceType), isShift),
                ProjectModule = isShift
                    ? HandoverTopicModuleMapToViewModel(_logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.Project, sourceId, (TypeOfModuleSource)sourceType), isShift)
                    : HandoverTopicModuleMapToViewModel(_logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Project, sourceId, (TypeOfModuleSource)sourceType), isShift),
                TeamModule = isShift
                    ? HandoverTopicModuleMapToViewModel(_logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.Team, sourceId, (TypeOfModuleSource)sourceType), isShift)
                    : HandoverTopicModuleMapToViewModel(_logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Team, sourceId, (TypeOfModuleSource)sourceType), isShift),
                TaskModule = HandoverTaskModuleMapToViewModel(sourceId, isShift, (TypeOfModuleSource)sourceType),
                DailyNotesModule = HandoverTopicModuleMapToViewModel(_logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.DailyNote, sourceId, (TypeOfModuleSource)sourceType), isShift)
            };

            model.HSEModule = model.HSEModule ?? new HandoverReportModuleViewModel() { Name = "HSE" };
            model.CoreModule = model.CoreModule ?? new HandoverReportModuleViewModel() { Name = "Core" };
            model.ProjectModule = model.ProjectModule ?? new HandoverReportModuleViewModel() { Name = "Project" };
            model.TeamModule = model.TeamModule ?? new HandoverReportModuleViewModel() { Name = "Team" };
            model.TaskModule = model.TaskModule ?? new HandoverReportTaskModuleViewModel() { Name = "Tasks" };
            model.DailyNotesModule = model.DailyNotesModule ?? new HandoverReportModuleViewModel() { Name = "DailyNote" };

            if (selectedSourceId.HasValue)
            {
                model.CoreModule.Topics = model.CoreModule.Topics != null
                    ? model.CoreModule.Topics.Where(t => t.FromSourceId == selectedSourceId)
                    : null;
                model.HSEModule.Topics = model.HSEModule.Topics != null
                    ? model.HSEModule.Topics.Where(t => t.FromSourceId == sourceId)
                    : new List<HandoverReportTopicViewModel>();
                model.TeamModule.Topics = model.TeamModule.Topics != null
                    ? model.TeamModule.Topics.Where(t => t.FromSourceId == selectedSourceId)
                    : new List<HandoverReportTopicViewModel>();
                model.ProjectModule.Topics = model.ProjectModule.Topics != null
                    ? model.ProjectModule.Topics.Where(t => t.FromSourceId == selectedSourceId)
                    : new List<HandoverReportTopicViewModel>();
                model.DailyNotesModule.Topics = model.DailyNotesModule.Topics != null
                  ? model.DailyNotesModule.Topics.Where(t => t.FromSourceId == selectedSourceId)
                  : new List<HandoverReportTopicViewModel>();
            }

            model.HSEModule.Topics = model.HSEModule.Topics != null
                ? model.HSEModule.Topics.OrderBy(t => t.TopicGroupName).ThenBy(t => t.Name).ToList()
                : new List<HandoverReportTopicViewModel>();
            model.CoreModule.Topics = model.CoreModule.Topics != null
                ? model.CoreModule.Topics.OrderBy(t => t.TopicGroupName).ThenBy(t => t.Name).ToList()
                : new List<HandoverReportTopicViewModel>();
            model.TeamModule.Topics = model.TeamModule.Topics != null
                ? model.TeamModule.Topics.OrderBy(t => t.TopicGroupName).ThenBy(t => t.Name).ToList()
                : new List<HandoverReportTopicViewModel>();
            model.ProjectModule.Topics = model.ProjectModule.Topics != null
                ? model.ProjectModule.Topics.OrderBy(t => t.TopicGroupName).ThenBy(t => t.Name).ToList()
                : new List<HandoverReportTopicViewModel>();
            model.TaskModule.Tasks = model.TaskModule.Tasks != null
                ? model.TaskModule.Tasks.OrderBy(t => t.Reference).ThenBy(t => t.Name).ToList()
                : new List<HandoverReportTaskViewModel>();
            model.DailyNotesModule.Topics = model.DailyNotesModule.Topics != null
              ? model.DailyNotesModule.Topics.OrderBy(t => t.TopicGroupName).ThenBy(t => t.Name).ToList()
              : new List<HandoverReportTopicViewModel>();

            if (model.TeamModule.Topics != null && model.TeamModule.Topics.Any())
            {
                foreach (var teamTopic in model.TeamModule.Topics)
                {
                    teamTopic.TopicGroupName = !teamTopic.RelationId.HasValue || teamTopic.RelationId.Value == Guid.Empty
                        ? "Other"
                        : _logicCore.UserProfileCore.GetUser(teamTopic.RelationId.Value).FullName;
                }
            }

            return model;
        }

        /// <summary>
        /// Get all topic from module and map to viewModel
        /// </summary>
        /// <param name="rotationModule">RotationModule rotationModule, rotation or shift module</param>
        /// <param name="isShift">bool isShift, if this module from shift</param>
        /// <returns>HandoverReportModuleViewModel viewModel, viewModel with collection of topic and module name</returns>
        private HandoverReportModuleViewModel HandoverTopicModuleMapToViewModel(RotationModule rotationModule, bool isShift)
        {
            if (rotationModule != null)
            {
                var viewModel = new HandoverReportModuleViewModel
                {
                    Name = rotationModule.Type.ToString()
                };

                viewModel.Topics = rotationModule.RotationTopicGroups.SelectMany(tg => tg.RotationTopics).Where(t => t.Enabled).Select(t => new
                    HandoverReportTopicViewModel
                    {
                        Name = t.Name,
                        Description = t.IsNullReport ? "Nothing to report this time." : t.Description,
                        TopicGroupName = t.RotationTopicGroup.Name,
                        IsNullReport = t.IsNullReport,
                        Tag = t.SearchTags != null ? t.SearchTags.Split(',').FirstOrDefault() : "",
                        FromSourceId = t.AncestorTopic != null
                            ? isShift
                                ? t.AncestorTopic.RotationTopicGroup.RotationModule.ShiftId
                                : t.AncestorTopic.RotationTopicGroup.RotationModule.RotationId
                            : (Guid?)null,
                        RelationId = t.RelationId,
                        IsPinned = t.IsPinned,
                        HasTask = t.RotationTasks.Any()
                    });

                return viewModel;
            }
            return new HandoverReportModuleViewModel();
        }

        /// <summary>
        /// Get all rotation task by sourceId (rotation of shift) and map to viewModel
        /// </summary>
        /// <param name="sourceId">Guid sourceId, rotation or shift id</param>
        /// <param name="isShift">bool isShift, if this source shift</param>
        /// <param name="sourceType">TypeOfModuleSource sourceType, type of rotation or shift modules</param>
        /// <returns>HandoverReportTaskModuleViewModel viewModel, viewModel with collection of task and module name</returns>
        private HandoverReportTaskModuleViewModel HandoverTaskModuleMapToViewModel(Guid sourceId, bool isShift, TypeOfModuleSource sourceType)
        {
            var tasks = isShift
                ? _logicCore.RotationTaskCore.GetAllShiftTasks(sourceId, sourceType).ToList()
                : _logicCore.RotationTaskCore.GetAllRotationTasks(sourceId, sourceType).ToList();

            if (sourceType == TypeOfModuleSource.Received)
            {
                tasks = tasks.Where(t => t.Status == StatusOfTask.Default && t.Name != "For information").ToList();
            }

            var model = new HandoverReportTaskModuleViewModel
            {
                Name = "Tasks",
                Tasks = tasks != null
                    ? tasks.Select(t => new HandoverReportTaskViewModel()
                    {
                        Name = t.Name,
                        Description = t.Description,
                        Priority = t.Priority.ToString(),
                        Reference = t.RotationTopic != null ? t.RotationTopic.Name : "",
                        IsNullReport = t.IsNullReport,
                        Date = t.Deadline.FormatDayMonthYear(),
                        IsPinned = t.IsPinned
                    })
                    : new List<HandoverReportTaskViewModel>()
            };

            return model;
        }
        #endregion
    }
}
