﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces.Services;
using AutoMapper;
using Microsoft.AspNet.Identity;
using MomentumPlus.Core.Authorization;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Constants;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using MvcPaging;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class TopicSearchService : ITopicSearchService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public TopicSearchService(IRepositoriesUnitOfWork repositoriesUnitOfWork, IServicesUnitOfWork services, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
            this._services = services;
        }


        public List<SearchBarItem> GetBaseTopicGroups()
        {
            var topicGroups = _logicCore.TopicGroupCore.GetBaseTopicGroups();


            return topicGroups.Select(topic => new SearchBarItem
            {
                Id = topic.Id,
                Name = topic.Name
            }).ToList();
        }

        public TopicSearchResultViewModel SearchTopics(Guid currentUserId, TopicSearchViewModel model)
        {
            var userRoles = _logicCore.UserManager.GetRoles(currentUserId);

            IQueryable<RotationTopic> topics = null;

            if (userRoles.Any(r => r == iHandoverRoles.Relay.HeadLineManager || r == iHandoverRoles.Relay.LineManager))
            {
                var usersUnderSupervisionIds = _repositories.RotationRepository.Find(rotation => rotation.LineManagerId == currentUserId
                                                                                && rotation.State != RotationState.Created).Select(rotation => rotation.RotationOwnerId)
                                                                                .GroupBy(id => id)
                                                                                .Select(group => group.FirstOrDefault());

                topics = _logicCore.TopicGroupCore.FindTopicGroupsTopics(model.SelectedTopicGroupsIds.ToArray(), usersUnderSupervisionIds.ToArray());
            }

            else if (userRoles.Any(r => r == iHandoverRoles.Relay.Administrator || r == iHandoverRoles.Relay.iHandoverAdmin || r == iHandoverRoles.Relay.ExecutiveUser))
            {
                topics = _logicCore.TopicGroupCore.FindTopicGroupsTopics(model.SelectedTopicGroupsIds.ToArray());
            }

            else
            {
                throw new Exception("User access denied!");
            }


            var result = new List<TopicSearchResultItem>();

            var page = model.PageNumber - 1 ?? 0;
            var pageSize = model.PageSize ?? NumericConstants.Paginator.PaginatorPageSizeForTopicSearch;

            model.PageNumber = page;
            model.PageSize = pageSize;

            if (topics != null)
            {
                foreach (var topic in topics.OrderByDescending(t => t.CreatedUtc))
                {
                    var topicCreator = _logicCore.RotationTopicCore.GetTopicOwner(topic);

                    result.Add(new TopicSearchResultItem
                    {
                        Id = topic.Id,
                        Name = topic.RotationTopicGroup.Name,
                        CreatorId = topicCreator.Id,
                        CreatorName = topicCreator.FullName,
                        Notes = topic.Description,
                        Reference = topic.Name,
                        CreatedTime = topic.CreatedUtc.FormatTo24Time(),
                        CreatedDate = topic.CreatedUtc.FormatDayMonthYear(),
                    });
                }
            }

            TopicSearchResultViewModel searchResultViewModel = new TopicSearchResultViewModel
            {
                ResultItems = result.ToPagedList(page, pageSize, result.Count),
                Params = model
            };

            return searchResultViewModel;
        }

        public TopicReportPdfViewModel PopulateTopicSearchReportViewModel(IEnumerable<Guid?> selectedTopicGroupsIds, IEnumerable<Guid?> selectedUserIds, TopicSearchDateRangeType? dateRangeType)
        {
            List<RotationTopic> topics = _logicCore.TopicGroupCore.FindTopicGroupsTopics(selectedTopicGroupsIds.ToArray()).OrderByDescending(t => t.CreatedUtc).ToList();

            List<TemplateTopicGroup> templateTopicGroups = (from selectedTopicGroupsId 
                                                            in selectedTopicGroupsIds
                                                            where selectedTopicGroupsId.HasValue
                                                            select _logicCore.TopicGroupCore.GetTopicGroup((Guid) selectedTopicGroupsId)).ToList();


            TopicReportPdfViewModel viewModel = new TopicReportPdfViewModel
            {
                Topics = new List<TopicSearchResultItem>(),
                ReportDate = DateTime.UtcNow.FormatDayMonthYear().Trim(),
                CompanyLogoImageId = _logicCore.AdministrationCore.GetCompanyLogoImageId(),
                TopicsInSearch = templateTopicGroups.Any() ? string.Join(", ", templateTopicGroups.Select(tg => tg.Name)) : " - "
            };


            foreach (var topic in topics)
            {
                var topicCreator = _logicCore.RotationTopicCore.GetTopicOwner(topic);

                viewModel.Topics.Add(new TopicSearchResultItem
                {
                    Id = topic.Id,
                    Name = topic.Name,
                    CreatorId = topicCreator.Id,
                    CreatorName = topicCreator.FullName,
                    Notes = string.IsNullOrWhiteSpace(topic.Description) ? string.Empty : topic.Description.Trim(),
                    Reference = string.IsNullOrWhiteSpace(topic.RotationTopicGroup.Name) ? string.Empty : topic.RotationTopicGroup.Name,
                    CreatedTime = string.IsNullOrWhiteSpace(topic.CreatedUtc.FormatTo24Time()) ? string.Empty : topic.CreatedUtc.FormatTo24Time().Trim(),
                    CreatedDate = string.IsNullOrWhiteSpace(topic.CreatedUtc.FormatDayMonthYear()) ? string.Empty : topic.CreatedUtc.FormatDayMonthYear().Trim(),
                });
            }

            return viewModel;
        }
    }
}
