﻿using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.BLL.Services.Web.TimeLine
{
    internal class TimeLineBuilder
    {
        private class MonthPeriod
        {
            public DateTime Start;
            public DateTime End;
        }

        private int _year;
        private DateTime _currentDate;
        private TimelinePeriodViewModel _prevPeriodModel;
        private TimelineMonthViewModel _currentMonthModel;
        private TimelineViewModel _timeLine;

        public static TimelineViewModel ConcatTimelines(List<TimelineViewModel> timelines)
        {
            TimelineViewModel newTimelineModel = new TimelineViewModel
            {
                Months = new List<TimelineMonthViewModel>()
            };

            foreach (var timeline in timelines)
            {
                newTimelineModel.Months.AddRange(timeline.Months);
            }

            return newTimelineModel;
        }

        public TimelineViewModel TimeLine
        {
            get
            {
                return this._timeLine;
            }
        }

        public TimeLineBuilder(int year)
        {
            this._year = year;
            this._currentDate = DateTime.Today;

            this._timeLine = new TimelineViewModel
            {
                Months = new List<TimelineMonthViewModel>()
            };
        }

        private int[] HolidaysDaysForPeriod(DateTime startDate, int daysCount)
        {
            List<int> holidaysOffsets = new List<int>();

            for (int i = 0; i < daysCount; ++i)
            {
                DateTime monthObj = startDate.AddDays(i);

                if (monthObj.DayOfWeek == DayOfWeek.Sunday || monthObj.DayOfWeek == DayOfWeek.Saturday)
                {
                    holidaysOffsets.Add(i);
                }
            }

            return holidaysOffsets.ToArray();
        }

        private void AddMonth(DateTime date)
        {
            this._currentMonthModel = new TimelineMonthViewModel
            {
                Month = date.Month,
                MonthLabel = date.ToString("MMM yyyy"),
                IsCurrentMonth = date.Month == this._currentDate.Month && this._currentDate.Year == this._year,
                Periods = new List<TimelinePeriodViewModel>()
            };

            this._timeLine.Months.Add(this._currentMonthModel);
        }

        private void AddMonthPeriod(MonthPeriod period, TimeLinePeriodType periodType, bool leftIndent, bool rightIndent)
        {
            if (this._currentMonthModel == null || this._currentMonthModel.Month != period.Start.Month)
            {
                this.AddMonth(period.Start);
            }

            TimelinePeriodViewModel periodModel = new TimelinePeriodViewModel
            {
                DaysCount = (period.End - period.Start).Days,
                FirstDayNumber = leftIndent ? period.Start.Day : 0,
                LastDayNumber = rightIndent ? period.End.AddDays(-1).Day : 0,
                CurrentDayOffset = (this._currentDate - period.Start).Days,
                Type = periodType
            };

            periodModel.Holidays = (periodType == TimeLinePeriodType.DayOn) ? this.HolidaysDaysForPeriod(period.Start, periodModel.DaysCount) : null;

            if(this._prevPeriodModel != null)
            {
                periodModel.prevPeriodIsDayOn = this._prevPeriodModel.Type == TimeLinePeriodType.DayOn;
                this._prevPeriodModel.nextPeriodIsDayOn = periodModel.Type == TimeLinePeriodType.DayOn;
            }

            this._prevPeriodModel = periodModel;

            this._currentMonthModel.Periods.Add(periodModel);
        }

        public void AddPeriod(DateTime startDate, DateTime endDate, TimeLinePeriodType periodType)
        {
            if (endDate <= startDate || endDate.Year < this._year || startDate.Year > this._year)
                return;

            DateTime prevEnd = startDate;
            int monthsInPeriod = endDate.AddDays(-1).Month - startDate.Month + 1;

            for(int i = 0; i < monthsInPeriod; ++i)
            {
                MonthPeriod monthPeriod = new MonthPeriod
                {
                    Start = prevEnd,
                    End = (prevEnd.Month == endDate.Month) ? endDate : new DateTime(prevEnd.AddMonths(1).Year, prevEnd.AddMonths(1).Month, 1)
                };

                this.AddMonthPeriod(monthPeriod, periodType, monthPeriod.Start == startDate, monthPeriod.End == endDate);

                prevEnd = monthPeriod.End;
            }
        }

    }
}
