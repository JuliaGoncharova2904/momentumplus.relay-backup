﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class TopicService : ITopicService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public TopicService(IRepositoriesUnitOfWork repositoriesUnitOfWork, IServicesUnitOfWork services, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._services = services;
            this._logicCore = logicCore;
        }

        public IEnumerable<rTopicViewModel> GetTopicGroupTopicsViewModels(Guid topicGroupId)
        {
            var topics = _repositories.TemplateTopicRepository.Find(t => t.TopicGroupId == topicGroupId).OrderBy(t => t.Name);

            return _mapper.Map<IEnumerable<rTopicViewModel>>(topics);
        }

        public rTopicViewModel GetTopicViewModel(Guid topicId)
        {
            var topic = _repositories.TemplateTopicRepository.Get(topicId);

            return _mapper.Map<rTopicViewModel>(topic);
        }

        public void AddTopic(rTopicViewModel model)
        {
            var topic = _mapper.Map<TemplateTopic>(model);
            topic.Id = Guid.NewGuid();

            _repositories.TemplateTopicRepository.Add(topic);
            _repositories.Save();
        }

        public void UpdateTopic(rTopicViewModel model)
        {
            var topic = _mapper.Map<TemplateTopic>(model);

            _repositories.TemplateTopicRepository.Update(topic);
            _repositories.Save();

            _logicCore.TopicCore.UpdateChildTopics(topic);
        }

        public bool TopicExist(rTopicViewModel model)
        {
            return _repositories.TemplateTopicRepository.Find(t => t.Id != model.Id && t.TopicGroupId == model.TopicGroupId && t.Name.Equals(model.Name)).Any();
        }


        public IEnumerable<rTopicViewModel> PopulateTemplateTopicGroupTopicsViewModels(Guid baseTopicGroupId, Guid templateModuleId)
        {
            var topics = GetTopicGroupTopicsViewModels(baseTopicGroupId);

            foreach (var topic in topics)
            {
                topic.Enabled = CheckChildTopicStatus(topic.Id, templateModuleId);
            }

            return topics;
        }


        public bool CheckChildTopicStatus(Guid parentTopicId, Guid templateModuleId)
        {
            var topic = _logicCore.TopicCore.GetChildTopicFromModule(parentTopicId, templateModuleId);

            if (topic != null)
            {
                return topic.Enabled;
            }

            return false;
        }


        public void UpdateChildTopicStatus(Guid baseTopicId, Guid templateId, bool status)
        {
            var baseTopic = _logicCore.TopicCore.GetTopic(baseTopicId);

            var templateModule = _logicCore.ModuleCore.GetTemplateModule(baseTopic.TopicGroup.Module.Type, templateId);

            var templateTopicGroup = _logicCore.TopicGroupCore.GetOrCreateTemplateTopicGroup(templateModule.Id, baseTopic.TopicGroup);

            var templateTopic = _logicCore.TopicCore.GetOrCreateTemplateTopicGroupTopic(templateTopicGroup.Id, baseTopic);


            if (status == true)
            {
                _services.ModuleService.ChangeModuleStatus(templateModule.Id, true);
                _services.TopicGroupService.UpdateTopicGroupStatus(templateTopicGroup.Id, true);
            }


            UpdateTopicStatus(templateTopic.Id, status);

        }


        public void UpdateTopicStatus(Guid topicId, bool status)
        {
            var topic = _logicCore.TopicCore.GetTopic(topicId);

            if (status == false)
            {
                _services.TaskService.UpdateTopicTasksStatus(topic.Id, status);
            }

            topic.Enabled = status;

            _repositories.Save();

            //------ Adding new topic items to rotations live -------
            if (status)
            {
                _services.RotationTopicService.UpdateTemplateTopicChildTopicsStatus(topic.Id, status);
            }
            //-------------------------------------------------------
        }


        public void UpdateTopicGroupTopicsStatus(Guid topicGroupId, bool status)
        {
            var topicGroup = _logicCore.TopicGroupCore.GetTopicGroup(topicGroupId);

            foreach (var topic in topicGroup.Topics)
            {
                UpdateTopicStatus(topic.Id, status);
            }
        }

    }
}
