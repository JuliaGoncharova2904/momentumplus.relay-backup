﻿using MomentumPlus.Relay.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Interfaces;
using AutoMapper;
using MomentumPlus.Relay.BLL.Mapper;
using System.Web.Mvc;
using MomentumPlus.Core.Authorization;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces;
using RotationType = MomentumPlus.Core.Models.RotationType;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class RotationService : IRotationService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public RotationService(IRepositoriesUnitOfWork repositoriesUnitOfWork, IServicesUnitOfWork services, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
            _services = services;
        }

        public EditRotationViewModel PopulateEditRotationModel(EditRotationViewModel model)
        {
            IQueryable<UserProfile> allEmployees = _logicCore.UserProfileCore.GetRelayEmployees();


            IQueryable<UserProfile> headLineManager = _logicCore.UserProfileCore.GetEmployeesByRole(allEmployees, iHandoverRoles.Relay.HeadLineManager);
            IQueryable<UserProfile> iHandoverAdmin = _logicCore.UserProfileCore.GetEmployeesByRole(allEmployees, iHandoverRoles.Relay.iHandoverAdmin).Union(headLineManager);
            IQueryable<UserProfile> executiveUsers = _logicCore.UserProfileCore.GetEmployeesByRole(allEmployees, iHandoverRoles.Relay.ExecutiveUser).Union(iHandoverAdmin);
            IQueryable<UserProfile> safetyManager = _logicCore.UserProfileCore.GetEmployeesByRole(allEmployees, iHandoverRoles.Relay.SafetyManager).Union(executiveUsers);
            IQueryable<UserProfile> administrator = _logicCore.UserProfileCore.GetEmployeesByRole(allEmployees, iHandoverRoles.Relay.Administrator).Union(safetyManager);
            IQueryable<UserProfile> lineManagers = _logicCore.UserProfileCore.GetEmployeesByRole(allEmployees, iHandoverRoles.Relay.LineManager).Union(administrator);

            IQueryable<Project> allProjects = _repositories.ProjectRepository.GetAll();

            model.Contributors = _mapper.Map<IEnumerable<SelectListItem>>(allEmployees.ToList().OrderBy(u => u.FirstName));
            model.LineManagers = _mapper.Map<IEnumerable<SelectListItem>>(lineManagers.ToList().OrderBy(u => u.FirstName));
            model.BackToBackEmployees = _mapper.Map<IEnumerable<SelectListItem>>(allEmployees.ToList().OrderBy(u => u.FirstName));
            model.Projects = _mapper.Map<IEnumerable<SelectListItem>>(allProjects.ToList());

            return model;
        }

        public CreateRotationViewModel PopulateEditRotationModel(CreateRotationViewModel model)
        {
            Position position = null;
            Workplace site = null;

            IQueryable<UserProfile> allEmployees = _logicCore.UserProfileCore.GetRelayEmployees();
            IQueryable<UserProfile> newEmployees = _logicCore.UserProfileCore.GetEmployeesWithoutRotation(allEmployees);

            IQueryable<UserProfile> headLineManager = _logicCore.UserProfileCore.GetEmployeesByRole(allEmployees, iHandoverRoles.Relay.HeadLineManager);
            IQueryable<UserProfile> iHandoverAdmin = _logicCore.UserProfileCore.GetEmployeesByRole(allEmployees, iHandoverRoles.Relay.iHandoverAdmin).Union(headLineManager);
            IQueryable<UserProfile> executiveUsers = _logicCore.UserProfileCore.GetEmployeesByRole(allEmployees, iHandoverRoles.Relay.ExecutiveUser).Union(iHandoverAdmin);
            IQueryable<UserProfile> safetyManager = _logicCore.UserProfileCore.GetEmployeesByRole(allEmployees, iHandoverRoles.Relay.SafetyManager).Union(executiveUsers);
            IQueryable<UserProfile> administrator = _logicCore.UserProfileCore.GetEmployeesByRole(allEmployees, iHandoverRoles.Relay.Administrator).Union(safetyManager);
            IQueryable<UserProfile> lineManagers = _logicCore.UserProfileCore.GetEmployeesByRole(allEmployees, iHandoverRoles.Relay.LineManager).Union(administrator);

            IQueryable<Project> allProjects = _repositories.ProjectRepository.GetAll();

            model.Contributors = _mapper.Map<IEnumerable<SelectListItem>>(allEmployees.ToList().OrderBy(u => u.FirstName));
            model.LineManagers = _mapper.Map<IEnumerable<SelectListItem>>(lineManagers.ToList().OrderBy(u => u.FirstName));
            model.Projects = _mapper.Map<IEnumerable<SelectListItem>>(allProjects.ToList().OrderBy(u => u.Name));

            if (model.PositionId.HasValue && (position = _repositories.PositionRepository.Get(model.PositionId.Value)) != null)
            {
                model.RotationOwnerEmployees = _mapper.Map<IEnumerable<SelectListItem>>(newEmployees.Where(c => c.PositionId == model.PositionId.Value).ToList());
                model.BackToBackEmployees = _mapper.Map<IEnumerable<SelectListItem>>(allEmployees.ToList().OrderBy(u => u.FirstName));

                model.PositionName = position.Name;
                model.PositionId = position.Id;

                model.SiteName = position.Workplace.Name;
                model.SiteId = position.Workplace.Id;
            }
            else if (model.SiteId.HasValue && (site = _repositories.WorkplaceRepository.Get(model.SiteId.Value)) != null)
            {
                model.RotationOwnerEmployees = _mapper.Map<IEnumerable<SelectListItem>>(allEmployees.Where(c => c.Position.WorkplaceId == model.SiteId.Value).ToList());
                model.BackToBackEmployees = new List<SelectListItem>();

                model.SiteName = site.Name;
                model.SiteId = site.Id;
            }
            else
            {
                model.RotationOwnerEmployees = _mapper.Map<IEnumerable<SelectListItem>>(newEmployees.ToList().OrderBy(u => u.FirstName)); ;
                model.BackToBackEmployees = new List<SelectListItem>();
            }

            return model;
        }

        public IEnumerable<ReceivedHistorySlideViewModel> GetRotationsReceivedHistorySlidesByUser(Guid userId)
        {
            List<ReceivedHistorySlideViewModel> rotationsViewModel = new List<ReceivedHistorySlideViewModel>();
            IEnumerable<Rotation> rotations = _logicCore.RotationCore.GetAllUserRotations(userId, true)
                                                                        .OrderByDescending(r => r.CreatedUtc.Value)
                                                                        .ToList();

            foreach (var rotation in rotations)
            {
                rotationsViewModel.Add(new ReceivedHistorySlideViewModel
                {
                    Id = rotation.Id,
                    Date = rotation.StartDate.Value.FormatWithDayMonthYear() + " - " + rotation.StartDate.Value.AddDays(rotation.DayOn - 1).FormatWithDayMonthYear(),
                    ReceivedFrom = String.Join(" / ", rotation.HandoverFromRotations.Where(r => r.RotationOwnerId != userId).Select(r => r.RotationOwner.FullName)),
                    ReceivedTopicCounter = _logicCore.RotationTopicCore.GetAllRotationTopic(rotation.Id, TypeOfModuleSource.Received).Count(),
                    ReceivedTaskCounter = _logicCore.RotationTaskCore.GetAllRotationTasks(rotation.Id, TypeOfModuleSource.Received).Count()
                });
            }

            return rotationsViewModel;
        }

        public IEnumerable<RotationHistorySlideViewModel> GetRotationsHistorySlidesByUser(Guid userId)
        {
            List<RotationHistorySlideViewModel> rotationsViewModel = new List<RotationHistorySlideViewModel>();
            List<Rotation> rotations = _logicCore.RotationCore.GetAllUserRotations(userId, true)
                                                                        .OrderByDescending(r => r.CreatedUtc.Value)
                                                                        .ToList();

            Rotation firstRotation = rotations.FirstOrDefault();

            if (firstRotation != null && firstRotation.State != RotationState.SwingEnded && firstRotation.State != RotationState.Expired)
            {
                rotations.Remove(firstRotation);
            }

            foreach (var rotation in rotations)
            {
                rotationsViewModel.Add(new RotationHistorySlideViewModel
                {
                    Id = rotation.Id,
                    Date = rotation.StartDate.Value.FormatWithDayMonthYear() + " - " + rotation.StartDate.Value.AddDays(rotation.DayOn - 1).FormatWithDayMonthYear(),
                    Contributors = String.Join(" / ", rotation.Contributors.Select(u => u.FullName)),
                    HandoverTo = rotation.DefaultBackToBack.FullName,
                    DraftTopicCounter = rotation.RotationModules.Where(m => m.SourceType == TypeOfModuleSource.Draft).SelectMany(m => m.RotationTopicGroups)
                                                                                            .SelectMany(tg => tg.RotationTopics)
                                                                                            .Count(t => t.Enabled),
                    DraftTaskCounter = rotation.RotationModules.Where(m => m.SourceType == TypeOfModuleSource.Draft).SelectMany(m => m.RotationTopicGroups)
                                                                                            .SelectMany(tg => tg.RotationTopics)
                                                                                            .SelectMany(t => t.RotationTasks)
                                                                                            .Count(t => t.Enabled)
                });
            }

            return rotationsViewModel;
        }

        public Guid? GetCurrentRotationIdForUser(Guid userId)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(userId);

            return user.CurrentRotationId;
        }

        public SummaryViewModel GetSummaryForRotation(Guid rotationId, Guid currentUserId, bool viewMode)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            SummaryViewModel model = new SummaryViewModel
            {
                RotationId = rotationId,
                CurrentShiftId = rotation.RotationType == RotationType.Shift ? _services.ShiftService.GetCurrentShiftIdForRotation(rotationId) : null,
                RotarionOwnerId = rotation.RotationOwnerId,
                IsSwingExpired = rotation.State == RotationState.SwingEnded,
                RotationOwnerFullName = rotation.RotationOwner.FullName,
                IsRotationExpiredOrNotStarted = !rotation.StartDate.HasValue || rotation.State == RotationState.Expired,
                IsViewMode = viewMode
            };

            return model;
        }

        public DashboardViewModel GetDashboardForRotation(Guid rotationId, Guid currentUserId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);
            Models.RotationType rotationType = (Models.RotationType)rotation.RotationType;

            DashboardViewModel model = new DashboardViewModel
            {
                UserId = currentUserId,
                RotationId = rotationId,
                RotationOwnerFullName = _services.RotationService.GetRotationOwnerFullName(rotationId),
                AllowEditRotation = !_services.RotationService.IsRotationExpiredOrNotStarted(rotationId)
            };

            switch (rotationType)
            {
                case Models.RotationType.Swing:
                    model.CoreCounter = _services.RotationModuleService.ModuleTopicsCount(rotationId, ModuleType.Core, ModuleSourceType.Draft, rotationType);
                    model.SafetyCounter = _services.RotationModuleService.ModuleTopicsCount(rotationId, ModuleType.HSE, ModuleSourceType.Draft, rotationType);
                    model.TaskCounter = _logicCore.RotationTaskCore.GetAllRotationTasks(rotationId, TypeOfModuleSource.Draft).Count();
                    break;
                case Models.RotationType.Shift:
                    Shift shift = _logicCore.ShiftCore.GetCurrentRotationShift(rotation)
                                    ?? rotation.RotationShifts.OrderByDescending(t => t.StartDateTime).FirstOrDefault();
                    if (shift != null)
                    {
                        model.CoreCounter = _services.RotationModuleService.ModuleTopicsCount(shift.Id, ModuleType.Core, ModuleSourceType.Draft, rotationType);
                        model.SafetyCounter = _services.RotationModuleService.ModuleTopicsCount(shift.Id, ModuleType.HSE, ModuleSourceType.Draft, rotationType);
                        model.TaskCounter = _logicCore.RotationTaskCore.GetAllShiftTasks(shift.Id, TypeOfModuleSource.Draft).Count();
                    }
                    break;
                default:
                    throw new Exception("Unknown rotation type.");
            }

            return model;
        }

        public Guid GetCurrentRotationId(Guid userAnyoneRotationId)
        {
            var rotation = _logicCore.RotationCore.GetRotation(userAnyoneRotationId);

            if (rotation.RotationOwnerId != null)
            {
                var user = _logicCore.UserProfileCore.GetUser(rotation.RotationOwnerId);

                if (user.CurrentRotationId != null)
                {
                    return user.CurrentRotationId.Value;
                }
            }

            return Guid.Empty;
        }

        public ConfirmRotationViewModel PopulateConfirmModelWithCurrentRotation(Guid employeeId)
        {
            UserProfile employee = _logicCore.UserProfileCore.GetUser(employeeId);

            if (employee.CurrentRotationId.HasValue)
            {
                Rotation rotation = employee.CurrentRotation;

                if (rotation.State == RotationState.Created)
                {
                    Rotation prevRotation = rotation.PrevRotation;

                    return new ConfirmRotationViewModel
                    {
                        StartSwingDate = rotation.StartDate,
                        EndSwingDate = rotation.StartDate.HasValue ? (DateTime?)rotation.StartDate.Value.AddDays(rotation.DayOn - 1) : null,
                        BackOnSiteDate = rotation.StartDate.HasValue ? (DateTime?)rotation.StartDate.Value.AddDays(rotation.DayOn + rotation.DayOff) : null,
                        PrevRotationEndDate = (prevRotation != null) ? prevRotation.StartDate.Value.AddDays(prevRotation.DayOn + prevRotation.DayOff) : (DateTime?)null,
                        RepeatTimes = rotation.RepeatTimes
                    };
                }
                if (rotation.State == RotationState.SwingEnded)
                {
                    ConfirmRotationViewModel model = new ConfirmRotationViewModel { PrevRotationEndDate = DateTime.Today };

                    if (rotation.RepeatTimes > 0)
                    {
                        DateTime newStartDate = rotation.StartDate.Value.AddDays(rotation.DayOn + rotation.DayOff);

                        model.StartSwingDate = newStartDate;
                        model.EndSwingDate = newStartDate.AddDays(rotation.DayOn - 1);
                        model.BackOnSiteDate = newStartDate.AddDays(rotation.DayOn + rotation.DayOff);
                        model.RepeatTimes = rotation.RepeatTimes - 1;
                    }

                    return model;
                }
            }

            throw new Exception(string.Format("Employee with Id: {0} does not have current rotation.", employeeId));
        }

        public EditRotationViewModel GetRotation(Guid Id)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(Id);

            EditRotationViewModel model = _mapper.Map<EditRotationViewModel>(rotation);
            return PopulateEditRotationModel(model);
        }

        public FromToBlockViewModel GetFromToBlock(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            return _mapper.Map<FromToBlockViewModel>(rotation);
        }

        public Guid? GetRotationOwnerId(Guid? rotationId)
        {
            if (rotationId.HasValue)
            {
                var rotation = _logicCore.RotationCore.GetRotation(rotationId.Value);
                if (rotation != null)
                {
                    return rotation.RotationOwnerId;
                }
            }

            return null;
        }

        public Guid? GetRotationBackToBackId(Guid? rotationId)
        {
            if (rotationId.HasValue)
            {
                var rotation = _logicCore.RotationCore.GetRotation(rotationId.Value);
                if (rotation != null)
                {
                    return rotation.DefaultBackToBackId;
                }
            }

            return null;
        }


        public bool UpdateRotation(EditRotationViewModel model)
        {
            if (model.Id != Guid.Empty)
            {
                Rotation rotation = _logicCore.RotationCore.GetRotation(model.Id);

                if (rotation != null)
                {
                    //---- notification 4 ------
                    Guid newBackToBackId = Guid.Parse(model.DefaultBackToBackId);

                    if (rotation.DefaultBackToBackId != newBackToBackId)
                    {
                        UserProfile newBackToBack = _logicCore.UserProfileCore.GetUser(newBackToBackId);
                        _logicCore.NotificationCore.NotificationTrigger.Send_ManagerReassignsHandover(rotation.RotationOwnerId,
                                                                                                        newBackToBack.FullName);

                        _logicCore.RotationCore.UpdateRotationItems(rotation, rotation.DefaultBackToBackId, newBackToBackId, false);
                    }
                    //--------------------------
                    _mapper.Map(model, rotation);

                    this.FillModelContributorsAndProjects(model, rotation);


                    if (rotation.RotationType != (RotationType)model.RotationType)
                    {
                        _logicCore.RotationCore.UpdateRotationType(rotation, (RotationType)model.RotationType);
                    }

                    if (rotation.NextRotationId.HasValue)
                    {
                        _mapper.Map(model, rotation.NextRotation);
                        _logicCore.RotationCore.UpdateRotationType(rotation, (RotationType)model.RotationType);
                        _repositories.RotationRepository.Update(rotation.NextRotation);

                        this.FillModelContributorsAndProjects(model, rotation.NextRotation);
                    }


                    _repositories.RotationRepository.Update(rotation);
                    _repositories.Save();

                    return true;
                }
            }

            return false;
        }

        public string GetRotationOwnerFullName(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            return rotation.RotationOwner.FullName;
        }

        private void FillModelContributorsAndProjects(BaseRotationViewModel viewModel, Rotation entity)
        {
        }

        public IEnumerable<EditRotationViewModel> GetRotationsForPosition(Guid positionId)
        {
            IEnumerable<Rotation> rotations = _logicCore.RotationCore.GetActiveRotationsForPosition(positionId).ToList().OrderBy(r => r.RotationOwner.FirstName);
            IEnumerable<EditRotationViewModel> model = _mapper.Map<IEnumerable<EditRotationViewModel>>(rotations);

            return model;
        }

        public bool CreateFirstRotation(CreateRotationViewModel model)
        {
            Rotation rotation = _mapper.Map<Rotation>(model);
            rotation.Id = Guid.NewGuid();
            rotation.Contributors = new HashSet<UserProfile>();
            rotation.State = RotationState.Created;
            rotation.RotationType = (RotationType)model.RotationType;
            this.FillModelContributorsAndProjects(model, rotation);

            return _logicCore.RotationCore.AssignFirstRotation(rotation);
        }

        public bool CheckRotationPatternCompatibility(Guid employeeId, ConfirmRotationViewModel model)
        {
            if (model.StartSwingDate.HasValue && model.EndSwingDate.HasValue && model.BackOnSiteDate.HasValue)
            {
                return _logicCore.RotationCore.CheckRotationPatternCompatibility(employeeId,
                                                                                model.StartSwingDate.Value,
                                                                                model.EndSwingDate.Value,
                                                                                model.BackOnSiteDate.Value);
            }

            return false;
        }

        public bool ConfirmRotation(Guid employeeId, ConfirmRotationViewModel model)
        {
            if (model.StartSwingDate.HasValue && model.EndSwingDate.HasValue && model.BackOnSiteDate.HasValue)
            {
                return _logicCore.RotationCore.ConfirmRotation(employeeId, model.StartSwingDate.Value,
                                                                                model.EndSwingDate.Value,
                                                                                model.BackOnSiteDate.Value,
                                                                                model.RepeatTimes);
            }

            return false;
        }

        public bool RotationIsConfirmed(Guid rotationid)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationid);

            if (rotation.State != RotationState.Created)
            {
                if (rotation.State == RotationState.Expired)
                    _logicCore.RotationCore.MoveToNextRotation(rotation.RotationOwner);
                else
                    return true;
            }

            return false;
        }

        public IEnumerable<TeamRotationViewModel> GetActiveRotations(Guid? userId)
        {
            if (userId.HasValue && userId.Value != Guid.Empty)
            {
                var user = _repositories.UserProfileRepository.Find(u => u.Id == userId.Value).FirstOrDefault();
                if (user.Role == iHandoverRoles.Relay.LineManager || user.Role == iHandoverRoles.Relay.HeadLineManager || user.Role == iHandoverRoles.Relay.SafetyManager)
                {
                    IEnumerable<Rotation> rotations = _logicCore.RotationCore.GetAllActiveRotations()
                        .Where(r => r.LineManagerId == userId.Value && r.RotationOwnerId != userId.Value && !r.RotationOwner.DeletedUtc.HasValue);

                    return _mapper.Map<IEnumerable<TeamRotationViewModel>>(rotations.ToList().OrderBy(r => r.RotationOwner.FirstName));
                }
                else
                {
                    IEnumerable<Rotation> rotations = _logicCore.RotationCore.GetAllActiveRotations()
                        .Where(r => r.RotationOwnerId != userId.Value && !r.RotationOwner.DeletedUtc.HasValue);

                    return _mapper.Map<IEnumerable<TeamRotationViewModel>>(rotations.ToList().OrderBy(r => r.RotationOwner.FirstName));
                }
            }

            return null;
        }
        public void ChangeFinalizeStatusForDraftTopicsAndTasks(Guid rotationId, FinalizeStatus status)
        {
            _logicCore.RotationCore.ChangeFinalizeStatusForDraftTopicsAndTasks(rotationId, (StatusOfFinalize)status);
        }

        public IEnumerable<ReceivedRotationSlideViewModel> GetRotationsWhoPopulateMyReceivedSection(Guid rotationId)
        {
            List<Rotation> rotationsWhoPopulateMyReceivedSection = _logicCore.RotationCore
                                                                                .GetRotation(rotationId)
                                                                                .HandoverFromRotations
                                                                                .ToList();

            IQueryable<RotationTask> receivedTasks = _logicCore.RotationTaskCore.GetAllRotationTasks(rotationId, TypeOfModuleSource.Received);
            IEnumerable<ReceivedRotationSlideViewModel> rotations = _mapper.Map<IEnumerable<ReceivedRotationSlideViewModel>>(rotationsWhoPopulateMyReceivedSection);

            foreach (var rotation in rotations)
            {
                rotation.TopicCounter = _logicCore.RotationTopicCore
                                                    .GetAllRotationTopic(rotationId, TypeOfModuleSource.Received)
                                                    .Count(t => t.AncestorTopic.RotationTopicGroup.RotationModule.Rotation.Id == rotation.Id && !t.DeletedUtc.HasValue);


                rotation.TaskCounter = receivedTasks.Count(t => t.Enabled &&
                                                                      t.RotationTopic.AncestorTopic.RotationTopicGroup.RotationModule.Rotation.Id == rotation.Id &&
                                                                      t.Status == StatusOfTask.Default ||
                                                                      t.RotationTopic.SuccessorTopic.RotationTopicGroup.RotationModule.Rotation.Id == rotation.Id &&
                                                                      t.Status == StatusOfTask.Default);
            }

            return rotations.OrderBy(r => r.HandoverFrom).ThenByDescending(r => r.TopicCounter);
        }

        /// <summary>
        /// Method only for test in future be removed
        /// </summary>
        /// <param name="rotationId"></param>
        /// <returns></returns>
        [Obsolete("Testing functionality")]
        public bool ExpireRotation(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            return _logicCore.RotationCore.ExpireRotation(rotation);
        }

        /// <summary>
        /// Method only for test in future be removed
        /// </summary>
        /// <param name="rotationId"></param>
        /// <returns></returns>
        [Obsolete("Testing functionality")]
        public bool EndSwingOfRotation(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            return _logicCore.RotationCore.EndSwingOfRotation(rotation);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rotationId"></param>
        /// <returns></returns>
        public bool IsRotationExpiredOrNotStarted(Guid rotationId)
        {
            var rotation = _logicCore.RotationCore.GetRotation(rotationId);

            return (rotation.State == RotationState.Created || rotation.State == RotationState.Expired);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rotationId"></param>
        /// <returns></returns>
        public bool IsRotationSwingEnd(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            if (rotation.RotationType == RotationType.Shift)
            {
                return rotation.State == RotationState.SwingEnded && !rotation.RotationShifts.Any(s => s.State == ShiftState.Confirmed);
            }
            else
            {
                return rotation.State == RotationState.SwingEnded;
            }
        }

        /// <summary>
        /// Return period of Rotation by Rotation Id.
        /// </summary>
        /// <param name="rotationId">Rotation Id</param>
        /// <returns></returns>
        public string GetRotationPeriod(Guid rotationId)
        {
            string period = "...";
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            if (rotation.StartDate.HasValue)
            {
                period = rotation.StartDate.Value.FormatWithDayMonthYear() + " - " +
                            rotation.StartDate.Value.AddDays(rotation.DayOn - 1).FormatWithDayMonthYear();
            }

            return period;
        }

        /// <summary>
        /// Return Period of received rotation by Rotation Id (if received rotation is one)
        /// </summary>
        /// <param name="rotationId">Rotation Id</param>
        /// <param name="sourceRotationId">Selected Source Rotation Id</param>
        /// <returns></returns>
        public string GetReceivedRotationPeriod(Guid? rotationId, Guid? sourceRotationId)
        {
            string period = "...";

            if (sourceRotationId.HasValue)
            {
                Rotation rotation = _logicCore.RotationCore.GetRotation(sourceRotationId.Value);

                period = rotation.StartDate.Value.FormatWithDayMonthYear() + " - " +
                            rotation.StartDate.Value.AddDays(rotation.DayOn - 1).FormatWithDayMonthYear();
            }
            else if (rotationId.HasValue)
            {
                Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId.Value);

                if (rotation.HandoverFromRotations.Count() == 1)
                {
                    Rotation receivedRotation = rotation.HandoverFromRotations.First();

                    period = receivedRotation.StartDate.Value.FormatWithDayMonthYear() + " - " +
                                receivedRotation.StartDate.Value.AddDays(receivedRotation.DayOn - 1).FormatWithDayMonthYear();
                }
            }

            return period;
        }

        public EditRotationDatesViewModel GetRotationDates(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            EditRotationDatesViewModel model = new EditRotationDatesViewModel
            {
                CurrentDate = DateTime.Today,
                StartSwingDate = rotation.StartDate.Value,
                EndSwingDate = rotation.StartDate.Value.AddDays(rotation.DayOn - 1),
                BackOnSiteDate = rotation.StartDate.Value.AddDays(rotation.DayOn + rotation.DayOff),
                RepeatTimes = rotation.RepeatTimes
            };

            model.EndSwingDateReadOnly = rotation.State == RotationState.SwingEnded || rotation.State == RotationState.Expired;
            model.BackOnSiteDateReadOnly = rotation.State == RotationState.Expired;
            model.FillRepeatRotationRange();

            return model;
        }

        public bool UpdateRotationDates(EditRotationDatesViewModel model)
        {
            var result = _logicCore.RotationCore.UpdateRotationDates(model.RotationId, model.StartSwingDate.Value, model.EndSwingDate.Value,
                                                                        model.BackOnSiteDate.Value, model.RepeatTimes);

            return result;
        }

        public bool IsShiftRotation(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            return (rotation.RotationType == RotationType.Shift);
        }

        public IEnumerable<ShiftReceivedSlideViewModel> GetShiftsReceivedHistorySlidesByUser(Guid userId)
        {
            List<ShiftReceivedSlideViewModel> rotationsViewModel = new List<ShiftReceivedSlideViewModel>();
            IEnumerable<Rotation> rotations = _logicCore.RotationCore.GetAllUserRotations(userId, true)
                                                                        .OrderByDescending(r => r.CreatedUtc.Value)
                                                                        .ToList();

            foreach (var rotation in rotations)
            {
                List<ShiftContributorForReceivedPanel> contributors = new List<ShiftContributorForReceivedPanel>();
                foreach (var contributor in rotation.Contributors)
                {
                    contributors.Add(new ShiftContributorForReceivedPanel
                    {
                        ContributorID = contributor.Id,
                        ContributorName = contributor.FullName
                    });
                }

                rotationsViewModel.Add(new ShiftReceivedSlideViewModel
                {
                    Id = rotation.Id,
                    Date = rotation.StartDate.Value.FormatWithMonth() + " - " + rotation.StartDate.Value.AddDays(rotation.DayOn - 1).FormatWithMonth(),
                    HandoverFromName = rotation.RotationOwner.FullName,
                    HandoverFromID = rotation.RotationOwnerId,
                    Contributors = contributors,
                    TopicCounter = _logicCore.RotationTopicCore.GetAllRotationTopic(rotation.Id, TypeOfModuleSource.Received).Count(),
                    TaskCounter = _logicCore.RotationTaskCore.GetAllRotationTasks(rotation.Id, TypeOfModuleSource.Received).Count()
                });
            }

            return rotationsViewModel;
        }

        /// <summary>
        /// Check is rotation have prev rotation
        /// </summary>
        /// <param name="rotationId"></param>
        /// <returns></returns>
        public bool IsRotationHavePrevRotation(Guid rotationId)
        {
            var rotation = _logicCore.RotationCore.GetRotation(rotationId);

            return rotation.PrevRotation != null;
        }

    }
}
