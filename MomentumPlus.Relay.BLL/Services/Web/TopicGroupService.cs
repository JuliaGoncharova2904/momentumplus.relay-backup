﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class TopicGroupService : ITopicGroupService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public TopicGroupService(IRepositoriesUnitOfWork repositoriesUnitOfWork, IServicesUnitOfWork services,
            LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            _logicCore = logicCore;
            _services = services;
        }

        public IEnumerable<rTopicGroupViewModel> GetModuleTopicGroupsViewModels(Guid moduleId)
        {
            var model = _repositories.TemplateTopicGroupRepository.Find(tg => tg.ModuleId == moduleId).OrderBy(tg => tg.Name);

            return _mapper.Map<IEnumerable<rTopicGroupViewModel>>(model);
        }


        public rTopicGroupViewModel GetTopicGroupViewModel(Guid topicGroupId)
        {
            var model = _repositories.TemplateTopicGroupRepository.Get(topicGroupId);

            return _mapper.Map<rTopicGroupViewModel>(model);
        }


        public IEnumerable<rTopicGroupViewModel> PopulateTemplateModuleTopicGroupsViewModels(Guid baseModuleId,
            Guid templateModuleId)
        {
            var topicGroups = GetModuleTopicGroupsViewModels(baseModuleId);

            foreach (var topicGroup in topicGroups)
            {
                topicGroup.Enabled = CheckChildTopicGroupStatus(topicGroup.Id, templateModuleId);
            }

            return topicGroups;
        }


        public void UpdateTopicGroupStatus(Guid topicGroupId, bool status)
        {
            var topicGroup = _logicCore.TopicGroupCore.GetTopicGroup(topicGroupId);

            if (status == false)
            {
                _services.TopicService.UpdateTopicGroupTopicsStatus(topicGroup.Id, status);
            }

            topicGroup.Enabled = status;
            _repositories.TemplateTopicGroupRepository.Update(topicGroup);

            _repositories.Save();

            //------- Adding new topic groups to rotations live -------
            if (status)
            {
                _services.RotationTopicGroupService.UpdateTemplateTopicGroupChildRotationTopicGroupsStatus(topicGroupId, status);
            }
            //---------------------------------------------------------
        }

        public void UpdateChildTopicGroupStatus(Guid baseTopicGroupId, Guid templateId, bool status)
        {
            var baseTopicGroup = _logicCore.TopicGroupCore.GetTopicGroup(baseTopicGroupId);

            var templateModule = _logicCore.ModuleCore.GetTemplateModule(baseTopicGroup.Module.Type, templateId);

            var templateTopicGroup = _logicCore.TopicGroupCore.GetOrCreateTemplateTopicGroup(templateModule.Id,
                baseTopicGroup);


            if (status == true)
            {
                _services.ModuleService.ChangeModuleStatus(templateModule.Id, true);
            }

            UpdateTopicGroupStatus(templateTopicGroup.Id, status);
        }



        public bool CheckTopicGroupStatus(Guid topicGroupId)
        {
            var topicGroup = _logicCore.TopicGroupCore.GetTopicGroup(topicGroupId);

            if (topicGroup != null)
            {
                return topicGroup.Enabled;
            }

            return false;
        }

        public void UpdateModuleTopicGroupsStatus(Guid moduleId, bool status)
        {
            var module = _logicCore.ModuleCore.GetModule(moduleId);

            foreach (var topicGroup in module.TopicGroups)
            {
                UpdateTopicGroupStatus(topicGroup.Id, status);
            }
        }


        public bool CheckChildTopicGroupStatus(Guid parentTopicGroupId, Guid moduleId)
        {
            var topicGroup = _logicCore.TopicGroupCore.GetChildTopicGroup(parentTopicGroupId, moduleId);

            if (topicGroup != null)
            {
                return topicGroup.Enabled;
            }

            return false;
        }


        public ProjectTopicGroupViewModel PopulateProjectTopicGroupViewModel(ProjectTopicGroupViewModel topicModel)
        {
            var projectTopicGroupModel = new ProjectTopicGroupViewModel
            {
                Projects = _services.ProjectService.GetProjectList(),
                Name =
                    topicModel.RelationId != null
                        ? _services.ProjectService.GetProjectName((Guid)topicModel.RelationId)
                        : _services.ProjectService.GetProjectName(new Guid(topicModel.SelectedProject)),
                ModuleId = topicModel.ModuleId,
                RelationId = topicModel.RelationId ?? new Guid(topicModel.SelectedProject),
                SelectedProject = topicModel.SelectedProject ?? topicModel.RelationId.ToString(),
                Description = topicModel.Description,
                Id = topicModel.Id
            };

            return projectTopicGroupModel;
        }

        public TeamTopicGroupViewModel PopulateTeamTopicGroupViewModel(TeamTopicGroupViewModel topicModel)
        {
            var teamTopicGroupModel = new TeamTopicGroupViewModel
            {
                Teams = _services.TeamService.GetTeamList(),
                Name =
                    topicModel.RelationId != null
                        ? _services.TeamService.GetTeamName((Guid)topicModel.RelationId)
                        : _services.TeamService.GetTeamName(new Guid(topicModel.SelectedTeam)),
                ModuleId = topicModel.ModuleId,
                RelationId = topicModel.RelationId ?? new Guid(topicModel.SelectedTeam),
                SelectedTeam = topicModel.SelectedTeam ?? topicModel.RelationId.ToString(),
                Description = topicModel.Description,
                Id = topicModel.Id
            };

            return teamTopicGroupModel;
        }

        public void AddTopicGroup(rTopicGroupViewModel model)
        {
            var topicGroup = _mapper.Map<TemplateTopicGroup>(model);
            topicGroup.Id = Guid.NewGuid();

            _repositories.TemplateTopicGroupRepository.Add(topicGroup);
            _repositories.Save();
        }


        public void UpdateTopicGroup(rTopicGroupViewModel model)
        {
            var topicGroup = _mapper.Map<TemplateTopicGroup>(model);

            _repositories.TemplateTopicGroupRepository.Update(topicGroup);
            _repositories.Save();

            _logicCore.TopicGroupCore.UpdateChildTopicGroups(topicGroup);
        }

        public bool TopicGroupExist(rTopicGroupViewModel model)
        {
            return
                _repositories.RotationTopicGroupRepository.Find(
                    tg => tg.Id != model.Id && tg.RotationModuleId == model.ModuleId && tg.Name.Equals(model.Name)).Any();
        }

        public bool TopicGroupRelationExist(rTopicGroupViewModel model)
        {
            return
                _repositories.RotationTopicGroupRepository.Find(
                    tg => tg.Id != model.Id && tg.RotationModuleId == model.ModuleId && tg.RelationId == model.RelationId).Any();
        }

    }
}