﻿using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class VoiceMessageService : IVoiceMessageService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;

        public VoiceMessageService(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
        }

        public IEnumerable<VoiceMessageViewModel> GetVoiceMessagesForTopic(Guid topicId)
        {
            RotationTopic topic = _repositories.RotationTopicRepository.Get(topicId);
            if (topic != null)
                return _mapper.Map<IEnumerable<VoiceMessageViewModel>>(topic.VoiceMessages);

            return null;
        }

        public IEnumerable<VoiceMessageViewModel> GetVoiceMessagesForTask(Guid taskId)
        {
            RotationTask task = _repositories.RotationTaskRepository.Get(taskId);
            if (task != null)
                return _mapper.Map<IEnumerable<VoiceMessageViewModel>>(task.VoiceMessages);

            return null;
        }

        public bool AddVoiceMessageToTopic(VoiceMessageViewModel voiceMessage)
        {
            RotationTopic topic = _repositories.RotationTopicRepository.Get(voiceMessage.TargetId.Value);
            if (topic != null)
            {
                VoiceMessage voiceMessageEntity = _mapper.Map<VoiceMessage>(voiceMessage);
                voiceMessageEntity.Id = Guid.NewGuid();
                voiceMessageEntity.CreatedUtc = DateTime.Now;
                //voiceMessageEntity.FileName = attachment.File.FileName;
                voiceMessageEntity.File = _logicCore.MediaCore.AddFile(voiceMessage.File.FileName,
                                                                        voiceMessage.File.ContentType,
                                                                        voiceMessage.File.InputStream,
                                                                        false);
                _repositories.VoiceMessageRepository.Add(voiceMessageEntity);
                topic.VoiceMessages.Add(voiceMessageEntity);
                _repositories.Save();

                return true;
            }

            return false;
        }

        public bool AddVoiceMessageToTask(VoiceMessageViewModel voiceMessage)
        {
            RotationTask task = _repositories.RotationTaskRepository.Get(voiceMessage.TargetId.Value);
            if (task != null)
            {
                VoiceMessage voiceMessageEntity = _mapper.Map<VoiceMessage>(voiceMessage);
                voiceMessageEntity.Id = Guid.NewGuid();
                voiceMessageEntity.CreatedUtc = DateTime.Now;
                //voiceMessageEntity.FileName = voiceMessage.File.FileName;
                voiceMessageEntity.File = _logicCore.MediaCore.AddFile(voiceMessage.File.FileName,
                                                                        voiceMessage.File.ContentType,
                                                                        voiceMessage.File.InputStream,
                                                                        false);
                _repositories.VoiceMessageRepository.Add(voiceMessageEntity);
                task.VoiceMessages.Add(voiceMessageEntity);
                _repositories.Save();

                return true;
            }

            return false;
        }

        public bool RemoveVoiceMessageFromTopic(Guid topicId, Guid voiceMessageId)
        {
            RotationTopic topic = _repositories.RotationTopicRepository.Get(topicId);
            if (topic != null)
            {
                VoiceMessage voiceMessage = topic.VoiceMessages.FirstOrDefault(at => at.Id == voiceMessageId);
                if (voiceMessage != null)
                {
                    if (voiceMessage.File != null)
                        _repositories.FileRepository.Delete(voiceMessage.File);

                    _repositories.VoiceMessageRepository.Delete(voiceMessage);
                    _repositories.Save();

                    return true;
                }
            }

            return false;
        }

        public bool RemoveVoiceMessageFromTask(Guid taskId, Guid voiceMessageId)
        {
            RotationTask task = _repositories.RotationTaskRepository.Get(taskId);
            if (task != null)
            {
                VoiceMessage voiceMessage = task.VoiceMessages.FirstOrDefault(at => at.Id == voiceMessageId);
                if (voiceMessage != null)
                {
                    if (voiceMessage.File != null)
                        _repositories.FileRepository.Delete(voiceMessage.File);

                    _repositories.VoiceMessageRepository.Delete(voiceMessage);
                    _repositories.Save();

                    return true;
                }
            }

            return false;
        }
    }
}
