﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL
{
    public class CompanyService : ICompanyService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly IServicesUnitOfWork _services;

        public CompanyService(IRepositoriesUnitOfWork repositoriesUnitOfWork, IServicesUnitOfWork services)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._services = services;
        }

        public CompanyViewModel GetCompany(Guid companyId)
        {
            var company = _repositories.CompanyRepository.Get(companyId);

            return _mapper.Map<CompanyViewModel>(company);
        }

        public IEnumerable<SelectListItem> GetCompaniesList()
        {
            var companies = _repositories.CompanyRepository.GetAll().OrderBy(c => c.Name);

            return _mapper.Map<IEnumerable<SelectListItem>>(companies);
        }


        public IEnumerable<CompanyViewModel> GetCompanies()
        {
            var companies = _repositories.CompanyRepository.GetAll().OrderBy(c => c.Name);

            return _mapper.Map<IEnumerable<CompanyViewModel>>(companies);
        }

        public bool CompanyExist(CompanyViewModel model)
        {
            return _repositories.CompanyRepository.Find(c => c.Id != model.Id.Value && c.Name.Equals(model.Name)).Any();
        }


        public void AddCompany(CompanyViewModel model)
        {
            var company = _mapper.Map<Company>(model);

            _repositories.CompanyRepository.Add(company);
            _repositories.Save();
        }


        public void UpdateCompany(CompanyViewModel model)
        {
            var company = _mapper.Map<Company>(model);

            _repositories.CompanyRepository.Update(company);
            _repositories.Save();
        }


        public bool IsDefaultCompanyExist(CompanyViewModel model)
        {
            var companies = _repositories.CompanyRepository.Find(c => c.Id != model.Id.Value && c.IsDefault);

            return companies.Any();
        }

    }
}