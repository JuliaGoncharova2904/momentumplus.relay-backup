﻿using MomentumPlus.Core.Authorization;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MvcPaging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class TeamRotationsService : ITeamRotationsService
    {
        private readonly LogicCoreUnitOfWork _logicCore;

        public TeamRotationsService(LogicCoreUnitOfWork logicCore)
        {
            this._logicCore = logicCore;
        }

        /// <summary>
        /// Create Team Rotations view model.
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <param name="isViewMode">Is View mode flag</param>
        /// <param name="page">page number</param>
        /// <param name="pageSize">page size</param>
        /// <returns></returns>
        public MyTeamRotationViewModel GetMyTeamRotations(Guid userId, bool isViewMode, int page, int pageSize)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(userId);
            IQueryable<Rotation> rotations = _logicCore.TeamRotationsCore.GetActiveTeamRotations(user, isViewMode);

            IEnumerable<Rotation> pagedRotations = rotations?.OrderBy(r => r.RotationOwner.FirstName)
                .ThenBy(r => r.RotationOwner.LastName)
                .Skip(page * pageSize)
                .Take(pageSize)
                .ToList() ?? new List<Rotation>();

            MyTeamRotationViewModel model = new MyTeamRotationViewModel
            {
                IsViewMode = isViewMode,
                OwnerName = user.FullName,
                TeamRotations = this.MapRotationsToViewModel(user, pagedRotations).ToPagedList(page, pageSize, rotations.Count())
            };

            return model;
        }

        /// <summary>
        /// Map rotations to TeamRotation view model.
        /// </summary>
        /// <param name="user">Admin user</param>
        /// <param name="rotations">Rotation entities</param>
        /// <returns></returns>
        private IEnumerable<TeamRotationViewModel> MapRotationsToViewModel(UserProfile user, IEnumerable<Rotation> rotations)
        {
            List<TeamRotationViewModel> viewModels = new List<TeamRotationViewModel>();

            foreach (Rotation rotation in rotations)
            {
                UserProfile owner = rotation.RotationOwner;

                viewModels.Add(new TeamRotationViewModel
                {
                    ID = rotation.Id,
                    OwnerId = owner.Id,
                    OwnerFullName = owner.FullName,
                    OwnerEmail = owner.Email,
                    OwnerPosition = owner.PositionName,
                    BackToBackId = rotation.DefaultBackToBackId,
                    BackToBackName = rotation.DefaultBackToBack.FullName,
                    IsActive = rotation.State == RotationState.Confirmed,
                    StateMessage = this.RotationStateInfo(rotation),
                    IsLineManager = owner.Role == iHandoverRoles.Relay.LineManager ||
                                    owner.Role == iHandoverRoles.Relay.HeadLineManager ||
                                    owner.Role == iHandoverRoles.Relay.SafetyManager,
                    IsAdmin = owner.Role == iHandoverRoles.Relay.Administrator ||
                              owner.Role == iHandoverRoles.Relay.iHandoverAdmin,
                    IsEnable = user.Role != iHandoverRoles.Relay.ExecutiveUser
                });
            }

            return viewModels;
        }

        /// <summary>
        /// Get rotation state info.
        /// </summary>
        /// <param name="rotation">Rotation entity</param>
        /// <returns></returns>
        private string RotationStateInfo(Rotation rotation)
        {
            string res = string.Empty;

            switch (rotation.State)
            {

                case RotationState.Confirmed:
                    res = "Until " + rotation.StartDate.Value.AddDays(rotation.DayOn - 1).ToString("dd/MM");
                    break;
                case RotationState.SwingEnded:
                    res = "Back " + rotation.StartDate.Value.AddDays(rotation.DayOn + rotation.DayOff).ToString("dd/MM");
                    break;
                case RotationState.Expired:
                case RotationState.Created:
                default:
                    res = "Rotation not confirmed";
                    break;
            }

            return res;
        }
    }
}
