﻿using MomentumPlus.Relay.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Interfaces;
using AutoMapper;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class SiteService : ISiteService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;

        public SiteService(IRepositoriesUnitOfWork repositoriesUnitOfWork)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
        }

        public IEnumerable<SiteViewModel> GetAllSites()
        {
            return _mapper.Map<IEnumerable<SiteViewModel>>(_repositories.WorkplaceRepository.GetAll().ToList().OrderBy(s => s.Name));
        }

        public IEnumerable<SelectListItem> GetSitesList()
        {
            var sites = _repositories.WorkplaceRepository.GetAll().OrderBy(c => c.Name);

            return _mapper.Map<IEnumerable<SelectListItem>>(sites);
        }

        public SiteViewModel GetSiteForPosition(Guid positionId)
        {
            Position position = _repositories.PositionRepository.Get(positionId);
            if(position != null)
            {
                return _mapper.Map<SiteViewModel>(position.Workplace);
            }

            return null;
        }

        public bool AddSite(SiteViewModel site)
        {
            if (!_repositories.WorkplaceRepository.Find(m => m.Name == site.Name).Any())
            {
                Workplace siteEntity = _mapper.Map<Workplace>(site);
                siteEntity.Id = Guid.NewGuid();

                _repositories.WorkplaceRepository.Add(siteEntity);
                _repositories.Save();

                return true;
            }

            return false;
        }

        public SiteViewModel GetSite(Guid siteId)
        {
            Workplace site = _repositories.WorkplaceRepository.Get(siteId);
            if(site != null)
            {
                return _mapper.Map<SiteViewModel>(site);
            }

            return null;
        }

        public bool UpdateSite(SiteViewModel site)
        {
            if(site.Id.HasValue)
            {
                Workplace siteEntity = _repositories.WorkplaceRepository.Get(site.Id.Value);
                if(siteEntity != null)
                {
                    _mapper.Map(site, siteEntity);

                    _repositories.WorkplaceRepository.Update(siteEntity);
                    _repositories.Save();

                    return true;
                }
            }

            return false;
        }
    }
}
