﻿using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class DailyReportService : IDailyReportService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public DailyReportService(IRepositoriesUnitOfWork repositoriesUnitOfWork, IServicesUnitOfWork services, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
            this._services = services;
        }

        public DailyReportsPanelViewModel PopulateDailyReportsPanelViewModel(Guid userId, bool showSafetyMessages, string safetyVesion)
        {
            UserProfile owner = _logicCore.UserProfileCore.GetUser(userId);
            IEnumerable<Rotation> userRotations = _logicCore.RotationCore.GetAllUserRotations(userId, true).ToList();

            IEnumerable<Shift> shifts = userRotations.SelectMany(r => r.RotationShifts)
                                                     .Where(shift => shift.State != ShiftState.Created)
                                                     .OrderBy(shift => shift.Rotation.CreatedUtc)
                                                     .ThenBy(shift => shift.CreatedUtc)
                                                     .ToList();

            IEnumerable<DailyReportViewModel> dailyReports = _mapper.Map<IEnumerable<DailyReportViewModel>>(shifts);

            if (showSafetyMessages)
            {
                foreach (DailyReportViewModel dailyReport in dailyReports.Reverse().GroupBy(dr => dr.StartDateTime.Date).Select(dr => dr.First()))
                {
                    this.AddSafetyMessagesToDailyReport(dailyReport, owner, safetyVesion);
                }
            }

            DailyReportsPanelViewModel model = new DailyReportsPanelViewModel
            {
                ShowFirstSlide = false,
                UnreadSafetyMessagesCounter = dailyReports.Sum(dr => dr.SafetyMessages != null ? dr.SafetyMessages.MessagesNumber : 0),
                DailyReports = dailyReports
            };

            return model;
        }

        public DailyReportViewModel PopulateDailyReportViewModel(Guid shiftId, Guid currentUserId, string safetyVesion)
        {
            Shift shift = _logicCore.ShiftCore.GetShift(shiftId);
            DailyReportViewModel dailyReport = _mapper.Map<DailyReportViewModel>(shift);
            this.AddSafetyMessagesToDailyReport(dailyReport, shift.Rotation.RotationOwner, safetyVesion);

            return dailyReport;
        }

        public int GetUnreadSafetyMessages(Guid userId, string safetyVesion)
        {
            UserProfile owner = _logicCore.UserProfileCore.GetUser(userId);

            int result = 0;

            switch(safetyVesion)
            {
                case "V1":
                    result = _logicCore.SafetyMessageCore.CountSafetyMessagesForUser(owner);
                    break;
                case "V2":
                case "ALL":
                    result = _logicCore.SafetyMessageV2Core.CountSafetyMessagesForUser(owner);
                    break;
            }

            return result;
        }

        private void AddSafetyMessagesToDailyReport(DailyReportViewModel dailyReport, UserProfile owner, string safetyVesion)
        {
            if (owner.CurrentRotationId.HasValue && owner.CurrentRotation.State == RotationState.Confirmed)
            {
                switch (safetyVesion)
                {
                    case "V1":
                        IEnumerable<SafetyMessage> safetyMessagesV1 = _logicCore.SafetyMessageCore.GetSafetyMessagesForUser(dailyReport.StartDateTime.Date, owner);
                        if (safetyMessagesV1.Any())
                        {
                            SafetyMessagesV1 safetyMessagesV1Model = new SafetyMessagesV1();
                            safetyMessagesV1Model.Version = "V1";
                            safetyMessagesV1Model.MessagesNumber = safetyMessagesV1.Count();

                            if (safetyMessagesV1Model.MessagesNumber == 1)
                                safetyMessagesV1Model.ImageId = safetyMessagesV1.First().CriticalControl.MajorHazard.Icon.ImageId;

                            dailyReport.SafetyMessages = safetyMessagesV1Model;
                        }
                        break;
                    case "V2":
                    case "ALL":
                        IEnumerable<SafetyMessageV2> safetyMessagesV2 = _logicCore.SafetyMessageV2Core.GetSafetyMessagesForUser(dailyReport.StartDateTime.Date, owner);
                        if (safetyMessagesV2.Any())
                        {
                            SafetyMessagesV2 safetyMessagesV2Model = new SafetyMessagesV2();
                            safetyMessagesV2Model.Version = "V2";
                            safetyMessagesV2Model.MessagesNumber = safetyMessagesV2.Count();

                            if (safetyMessagesV2Model.MessagesNumber == 1)
                                safetyMessagesV2Model.IconUrl = safetyMessagesV2.First().CriticalControl.MajorHazard.IconPath;

                            dailyReport.SafetyMessages = safetyMessagesV2Model;
                        }
                        break;
                }
            }
        }

    }
}
