﻿using MomentumPlus.Relay.Interfaces.Services;
using System;
using MomentumPlus.Relay.Models;
using System.IO;
using MomentumPlus.Core.Interfaces;
using AutoMapper;
using MomentumPlus.Relay.BLL.Mapper;
using System.Web.Helpers;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using System.Text;
using System.Web;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class MediaService : IMediaService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IMapper _mapper;

        public MediaService(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
        }

        public Guid AddImage(string title, HttpPostedFileBase imageFile)
        {
            Core.Models.File image = _logicCore.MediaCore.AddImage(title, imageFile.FileName, imageFile.ContentType, imageFile.InputStream);
            return image.Id;
        }

        public Guid AddFile(HttpPostedFileBase imageFile)
        {
            Core.Models.File file = _logicCore.MediaCore.AddFile(imageFile.FileName, imageFile.ContentType, imageFile.InputStream);
            return file.Id;
        }

        public FileViewModel GetImage(Guid imageId, int? width = null, int? height = null)
        {
            Core.Models.File image = _repositories.FileRepository.Get(imageId);
            if (image != null)
            {
                if (width.HasValue && height.HasValue && image.FileType != ".svg")
                {
                    WebImage img = new WebImage(new MemoryStream(image.BinaryData));
                    img.Resize(width.Value, height.Value);
                    image.BinaryData = img.GetBytes();
                }

                return _mapper.Map<FileViewModel>(image);
            }

            return null;
        }

        public FileViewModel GetProjectAvatarImage(Guid projectId, int? width, int? height)
        {
            Project project = _repositories.ProjectRepository.Get(projectId);
            if (project != null)
            {
                string[] projectWords = project.Name.Split(' ');
                string projectLeters = projectWords[0][0].ToString();
                if (projectWords.Length >= 2)
                    projectLeters += projectWords[1][0];

                FileViewModel image = new FileViewModel();
                image.ContentType = ".png";

                WebImage img = new WebImage(this.GenerateGrayImage(256, 256));
                img.AddTextWatermark(projectLeters.ToUpper(), fontColor: "White", fontSize: 80, horizontalAlign: "Center", verticalAlign: "Middle");

                if (width.HasValue && height.HasValue)
                    img.Resize(width.Value, height.Value);

                image.BinaryData = img.GetBytes();

                return image;
            }

            return null;
        }

        public FileViewModel GetEmployeeAvatarImage(Guid employeeId, int? width, int? height)
        {
            UserProfile employee = _repositories.UserProfileRepository.Get(employeeId);
            if (employee != null)
            {
                if (employee.AvatarId.HasValue)
                {
                    Core.Models.File image = _repositories.FileRepository.Get(employee.AvatarId.Value);
                    if (width.HasValue && height.HasValue && image.FileType != ".svg")
                    {
                        WebImage img = new WebImage(new MemoryStream(image.BinaryData));
                        img.Resize(width.Value, height.Value);
                        image.BinaryData = img.GetBytes();
                    }

                    return _mapper.Map<FileViewModel>(image);
                }
                else
                {
                    FileViewModel image = new FileViewModel();
                    image.ContentType = ".png";

                    WebImage img = new WebImage(this.GenerateGrayImage(256, 256));
                    img.AddTextWatermark(GenerateLeters(employee), fontColor: "White", fontSize: 80, horizontalAlign: "Center", verticalAlign: "Middle");

                    if (width.HasValue && height.HasValue)
                        img.Resize(width.Value, height.Value);

                    image.BinaryData = img.GetBytes();

                    return image;
                }
            }

            return null;
        }

        private string GenerateLeters(UserProfile employee)
        {
            StringBuilder str = new StringBuilder();

            if (!string.IsNullOrEmpty(employee.FirstName))
                str.Append(employee.FirstName[0]);

            if (!string.IsNullOrEmpty(employee.LastName))
                str.Append(employee.LastName[0]);

            return str.ToString().ToUpper();
        }

        private MemoryStream GenerateGrayImage(int width, int height)
        {
            MemoryStream stream = new MemoryStream();

            int bufferSize = width * height * 4;
            byte[] imageBuffer = new byte[bufferSize];

            for (int i = 0; i < bufferSize; ++i)
            {
                if ((i + 1) % 4 == 0)
                    imageBuffer[i] = 255;
                else
                    imageBuffer[i] = 60;
            }

            IntPtr unmanagedPointer = Marshal.AllocHGlobal(imageBuffer.Length);
            Marshal.Copy(imageBuffer, 0, unmanagedPointer, imageBuffer.Length);

            using (Bitmap image = new Bitmap(width, height, 256 * 4, PixelFormat.Format32bppRgb, unmanagedPointer))
            {
                image.Save(stream, ImageFormat.Png);
            }

            Marshal.FreeHGlobal(unmanagedPointer);

            return stream;
        }

        public void RemoveImage(Guid imageId)
        {
            _repositories.FileRepository.Delete(imageId);
            _repositories.Save();
        }

        public FileViewModel GetFile(Guid fileId)
        {
            Core.Models.File file = _repositories.FileRepository.Get(fileId);
            if (file != null)
            {
                return _mapper.Map<FileViewModel>(file);
            }

            return null;
        }

    }
}
