﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces;
using System.Linq;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.BLL.Services.Web.Projects;
using MomentumPlus.Relay.BLL.LogicCore.AccessValidation;
using MomentumPlus.Relay.BLL.Exceptions;
using MomentumPlus.Relay.BLL.Extensions.EntityExtensions;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class ProjectService : IProjectService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IServicesUnitOfWork _services;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IMapper _mapper;

        public ProjectService(IRepositoriesUnitOfWork repositoriesUnitOfWork, IServicesUnitOfWork services, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
            this._services = services;
        }

        [Obsolete]
        public IEnumerable<SelectListItem> GetProjectList()
        {
            return _mapper.Map<IEnumerable<SelectListItem>>(_repositories.ProjectRepository.GetAll());
        }

        [Obsolete]
        public string GetProjectName(Guid projectId)
        {
            var projectName = _repositories.ProjectRepository.Get(projectId).Name;

            return projectName;
        }

        public ProjectDialogViewModel PopulateEditProjectModel()
        {
            return this.PopulateEditProjectModel(new ProjectDialogViewModel());
        }

        public ProjectDialogViewModel PopulateEditProjectModel(ProjectDialogViewModel model)
        {
            model.Employees = _services.UserService.GetEmployeesList();
            return model;
        }

        public ProjectDialogViewModel GetProject(Guid projectId)
        {
            Project project = _logicCore.ProjectCore.GetProject(projectId);

            ProjectDialogViewModel model = _mapper.Map<ProjectDialogViewModel>(project);

            model.Employees = _services.UserService.GetEmployeesList();

            return model;
        }

        public Guid AddProject(ProjectDialogViewModel model, Guid creatorId)
        {
            Project project = _mapper.Map<Project>(model);
            project.CreatorId = creatorId;

            return _logicCore.ProjectCore.AddProject(project);
        }

        public void UpdateProject(ProjectDialogViewModel model)
        {
            Project project = _logicCore.ProjectCore.GetProject(model.Id);
            _mapper.Map(model, project);
            _logicCore.ProjectCore.UpdateProject(project);
        }

        public IEnumerable<ProjectSlideViewModel> GetProjectsSlides()
        {
            IEnumerable<Project> projects = _logicCore.ProjectCore.GetAllProjets().OrderBy(p => p.Name);

            return _mapper.Map<IEnumerable<ProjectSlideViewModel>>(projects);
        }

        public IEnumerable<ProjectSlideViewModel> GetProjectsSlidesForProjectManager(Guid projectManagerId)
        {
            IEnumerable<Project> projects = _logicCore.ProjectCore.GetProjetsByProjectManager(projectManagerId).OrderBy(p => p.Name);

            return _mapper.Map<IEnumerable<ProjectSlideViewModel>>(projects);
        }

        public IEnumerable<ProjectSlideViewModel> GetProjectsSlidesForLineManager(Guid lineManagerId)
        {
            IEnumerable<Project> projects = _logicCore.ProjectCore.GetProjetsByLineManager(lineManagerId).OrderBy(p => p.Name);

            return _mapper.Map<IEnumerable<ProjectSlideViewModel>>(projects);
        }

        public ProjectSlideViewModel GetProjectSlide(Guid projectId)
        {
            Project project = _logicCore.ProjectCore.GetProject(projectId);

            return _mapper.Map<ProjectSlideViewModel>(project);
        }

        public ProjectFullSlideViewModel GetFullProjectSlide(Guid projectId)
        {
            Project project = _logicCore.ProjectCore.GetProject(projectId);

            return _mapper.Map<ProjectFullSlideViewModel>(project);
        }

        public ProjectTeamPanelViewModel GetProjectFollowers(Guid projectid, int page, int pageSize)
        {
            IQueryable<UserProfile> followers = _logicCore.ProjectCore.GetFollowersOfProject(projectid, null)
                                                                        ;

            ProjectTeamPanelViewModel model = new ProjectTeamPanelViewModel
            {
                TotalFolowersCounter = followers.Count(),
                Followers = followers
                                    .OrderBy(f => f.FirstName)
                                    .ThenBy(f => f.LastName)
                                    .Skip(page * pageSize)
                                    .Take(pageSize)
                                    .ToList()
                                    .Select(u => new ProjectFollowerViewModel
                                    {
                                        Name = u.FullName,
                                        Position = u.PositionName,
                                        Id = u.Id
                                    })
            };

            return model;
        }

        public ProjectFollowersTabViewModel GetTeamsMembersForProject(Guid projectid, string searchQuery)
        {
            ProjectFollowersTabViewModel model = new ProjectFollowersTabViewModel
            {
                ProjectId = projectid,
                Followers = new List<ProjectFollowerViewModel>()
            };

            IEnumerable<UserProfile> followers = _logicCore.ProjectCore.GetFollowersOfProject(projectid, searchQuery)
                                                                .OrderBy(f => f.Team.Name)
                                                                .ThenBy(f => f.FirstName)
                                                                .ThenBy(f => f.LastName)
                                                                .ToList();

            IEnumerable<UserProfile> noFollowers = _logicCore.UserProfileCore.GetAllUsers(searchQuery)
                                                                .Where(u => !followers.Select(f => f.Id).Contains(u.Id))
                                                                .OrderBy(f => f.Team.Name)
                                                                .ThenBy(f => f.FirstName)
                                                                .ThenBy(f => f.LastName);


            AddUsersToFolloversViewList(model.Followers as List<ProjectFollowerViewModel>, followers, true);
            AddUsersToFolloversViewList(model.Followers as List<ProjectFollowerViewModel>, noFollowers, false);

            return model;
        }

        private void AddUsersToFolloversViewList(List<ProjectFollowerViewModel> list, IEnumerable<UserProfile> users, bool selected)
        {
            foreach (UserProfile user in users)
            {
                ProjectFollowerViewModel followerView = new ProjectFollowerViewModel
                {
                    Id = user.Id,
                    Name = user.FullName,
                    Position = user.Position.Name,
                    Team = user.Team.Name,
                    OnSite = user.CurrentRotation == null ? false : user.CurrentRotation.State == RotationState.Confirmed,
                    Select = selected
                };

                list.Add(followerView);
            }
        }

        public void UpdateFollowersOfProject(ProjectFollowersTabViewModel model)
        {
            Project project = _logicCore.ProjectCore.GetProject(model.ProjectId);

            if (model.SelectedFollowers != null)
                _logicCore.ProjectCore.AddFollowersToProject(project, model.SelectedFollowers, false);

            if (model.UnselectedFollowers != null)
                _logicCore.ProjectCore.RemoveFollowersFromProject(project, model.UnselectedFollowers, false);

            if (model.SelectedFollowers != null || model.UnselectedFollowers != null)
                _repositories.Save();
        }

        public ProjectsTeamsTabViewModel GetTeamsForProject(Guid projectId)
        {
            Project project = _logicCore.ProjectCore.GetProject(projectId);
            IEnumerable<Team> teams = _logicCore.TeamCore.GetAllTeams();
            IEnumerable<Team> selectedTeams = _logicCore.ProjectCore.GetSelectedTeamsForProject(project, teams);

            ProjectsTeamsTabViewModel model = new ProjectsTeamsTabViewModel
            {
                ProjectId = projectId,
                Teams = new List<ProjectTeamViewModel>()
            };

            foreach (Team team in teams)
            {
                ProjectTeamViewModel teamView = new ProjectTeamViewModel
                {
                    Id = team.Id,
                    Name = team.Name,
                    IsSelected = selectedTeams.Where(st => st.Id == team.Id).Any()
                };

                (model.Teams as List<ProjectTeamViewModel>).Add(teamView);
            }

            return model;
        }

        public void UpdateTeamsForProject(ProjectsTeamsTabViewModel model)
        {
            Project project = _logicCore.ProjectCore.GetProject(model.ProjectId);

            if (model.SelectedTeams != null)
                _logicCore.ProjectCore.AddTeamsToProject(project, model.SelectedTeams, false);

            if (model.UnselectedTeams != null)
                _logicCore.ProjectCore.RemoveTeamsFromProject(project, model.UnselectedTeams, false);

            if (model.SelectedTeams != null || model.UnselectedTeams != null)
                _repositories.Save();
        }

        /// <summary>
        /// Return Planned tasks list
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="projectId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterOptions"></param>
        /// <param name="sortOptions"></param>
        /// <returns></returns>
        public ProjectTasksPanelViewModel GetPlannedTasksForProject(Guid userId,
                                                                    Guid projectId,
                                                                    int page,
                                                                    int pageSize,
                                                                    ProjectTasksFilterOptions filterOptions,
                                                                    ProjectTasksSortOptions sortOptions)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(userId);

            IQueryable<RotationTask> tasksList = _logicCore.ProjectCore.GetTaskBoardTasksForProject(user, projectId);

            tasksList = ProjectTasksFilter.ApplyTo(tasksList, filterOptions);
            tasksList = ProjectTasksSorter.ApplyTo(tasksList, sortOptions);

            return this.MapTasksToViewModel(tasksList, page, pageSize);
        }

        /// <summary>
        /// Return Ad Hoc tasks list
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filterOptions"></param>
        /// <param name="sortOptions"></param>
        /// <returns></returns>
        public ProjectTasksPanelViewModel GetAdHocTasksForProject(Guid projectId,
                                                                    int page,
                                                                    int pageSize,
                                                                    ProjectTasksFilterOptions filterOptions,
                                                                    ProjectTasksSortOptions sortOptions)
        {
            IQueryable<RotationTask> tasksList = _logicCore.ProjectCore.GetSwingAndShiftTasksForProject(projectId);

            tasksList = ProjectTasksFilter.ApplyTo(tasksList, filterOptions);
            tasksList = ProjectTasksSorter.ApplyTo(tasksList, sortOptions);

            return this.MapTasksToViewModel(tasksList, page, pageSize);
        }

        /// <summary>
        /// Mapper for ProjectTasksPanelViewModel model from tasks query.
        /// </summary>
        /// <param name="tasksList"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        private ProjectTasksPanelViewModel MapTasksToViewModel(IQueryable<RotationTask> tasksList, int page, int pageSize)
        {
            return new ProjectTasksPanelViewModel
            {
                TotalTasksCounter = tasksList.Count(),
                Tasks = tasksList.Skip(page * pageSize).Take(pageSize).ToList().Select(this.MapTaskToViewModel)
            };
        }

        /// <summary>
        /// Mapper for ProjectTaskTopicViewModel model from taskEntity.
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        private ProjectTaskTopicViewModel MapTaskToViewModel(RotationTask task)
        {
            return new ProjectTaskTopicViewModel
            {
                Id = task.Id,
                ReadOnly = !task.IsEditable(),
                IsFeedbackRequired = task.IsFeedbackRequired,
                Name = task.Name,
                CompleteStatus = task.TaskBoardTaskType == TaskBoardTaskType.MyTask
                                    ? task.IsComplete ? "Complete" : "Incomplete"
                                    : task.TaskBoardTaskType == TaskBoardTaskType.Pending ? "Pending"
                                    : task.SuccessorTaskId.HasValue && task.SuccessorTask.IsComplete ? "Complete" : "Incomplete",
                Priority = task.Priority.ToString(),
                Deadline = task.Deadline.FormatWithMonth(),
                TeammateId = task.AssignedToId,
                IsInArchive = task.IsInArchive,
                HasAttachments = task.Attachments != null && task.Attachments.Any(),
                HasVoiceMessages = task.VoiceMessages != null && task.VoiceMessages.Any()
            };
        }

        /// <summary>
        /// Return project task
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public ProjectTaskTopicViewModel GetProjectTask(Guid userId, Guid taskId)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(userId);
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);

            if (!task.IsAssignedToProject())
                throw new Exception(string.Format("Task with ID:{0} is not assigned to project", task.Id));

            var projectId = task.ProjectId.HasValue ? task.ProjectId.Value : task.RotationTopic.RotationTopicGroup.RelationId.Value;
            if (!user.HasAccessToProject(projectId))
                throw new Exception(string.Format("User with ID:{0} is not has access to project with ID:{1}", userId, projectId));

            return this.MapTaskToViewModel(task);
        }

        /// <summary>
        /// Update project task
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="taskModel"></param>
        /// <returns></returns>
        public ProjectTaskTopicViewModel UpdateProjectTask(Guid userId, ProjectTaskTopicViewModel taskModel)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(userId);
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskModel.Id);

            if (!task.IsAssignedToProject())
                throw new Exception(string.Format("Task with ID:{0} is not assigned to project", task.Id));

            if (!user.HasAccessToProject(task.ProjectId.Value))
                throw new Exception(string.Format("User with ID:{0} is not has access to project with ID:{1}", userId, task.ProjectId.Value));

            task.IsInArchive = taskModel.IsInArchive;
            task.IsFeedbackRequired = taskModel.IsFeedbackRequired;
            _logicCore.RotationTaskCore.Update(task);

            return this.MapTaskToViewModel(task);
        }

        /// <summary>
        /// Return list of project followers.
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetProjectFollowerForAssigning(Guid projectId)
        {
            IEnumerable<SelectListItem> model = _logicCore.ProjectCore.GetFollowersOfProject(projectId, null)
                                                                        .ToList()
                                                                        .Select(u => new SelectListItem { Value = u.Id.ToString(), Text = u.FullName })
                                                                        .OrderBy(f => f.Text);
            return model;
        }

        #region Add dialog

        /// <summary>
        /// Populate model for add task dialogs in project admin 
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <returns>model ProjectAdminAddTaskDialogViewModel</returns>
        public ProjectAdminAddTaskDialogViewModel PopulateAddTaskModel(Guid userId, Guid projectId)
        {
            ProjectAdminAddTaskDialogViewModel model = new ProjectAdminAddTaskDialogViewModel
            {
                OwnerId = userId,
                StartLimitDate = DateTime.Now,
                SendingOption = TaskSendingOptions.Now,
                ProjectId = projectId
            };

            return model;
        }

        /// <summary>
        /// Add new task to project
        /// </summary>
        /// <param name="task">view model ProjectAdminAddTaskDialogViewModel</param>
        public void AddTask(ProjectAdminAddTaskDialogViewModel task)
        {
            RotationTask taskEntity = new RotationTask();

            UserProfile user = _logicCore.UserProfileCore.GetUser(task.OwnerId);

            taskEntity.Name = task.Name;
            taskEntity.Description = task.Notes;
            taskEntity.AssignedToId = task.AssignedTo.Value;
            taskEntity.Deadline = task.Deadline.Value;
            taskEntity.Priority = (PriorityOfTask)task.Priority;
            taskEntity.SearchTags = task.Tags != null ? string.Join(",", task.Tags) : string.Empty;
            taskEntity.ProjectId = task.ProjectId;
            taskEntity.TaskBoardId = task.OwnerId;
            taskEntity.TaskBoard = user.TaskBoard;

            if (task.OwnerId == task.AssignedTo.Value)
            {
                taskEntity.TaskBoardTaskType = TaskBoardTaskType.MyTask;
                _logicCore.TaskBoardCore.AddTask(taskEntity);
            }
            else
            {
                switch (task.SendingOption)
                {
                    case TaskSendingOptions.Now:
                        taskEntity.TaskBoardTaskType = TaskBoardTaskType.Draft;
                        _logicCore.TaskBoardCore.AddTask(taskEntity);
                        _logicCore.TaskBoardCore.HandoverTask(taskEntity);
                        break;
                    case TaskSendingOptions.Pending:
                        taskEntity.TaskBoardTaskType = TaskBoardTaskType.Pending;
                        taskEntity.DeferredHandoverTime = task.SendDate;
                        _logicCore.TaskBoardCore.AddTask(taskEntity);
                        break;
                    default:
                        throw new Exception("Bad sanding option of task.");
                }
            }

            _logicCore.RotationTaskLogCore.AddMessageToTaskLog(taskEntity, $"<strong>{user.FullName}</strong> created the task.");
        }

        #endregion

        #region Edit dialog

        /// <summary>
        /// Get task for project admin edit task dialog
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="userId"></param>
        /// <returns>view model ProjectAdminEditTaskDialogViewModel</returns>
        public ProjectAdminEditTaskDialogViewModel PopulateEditTaskModel(Guid userId, Guid taskId)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(userId);
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);

            if (!task.IsAssignedToProject())
                throw new Exception($"Task with ID:{task.Id} is not assigned to project");

            if (!user.HasAccessToProject(task.ProjectId.Value))
                throw new Exception($"User with ID:{userId} is not has access to project with ID:{task.ProjectId.Value}");

            ProjectAdminEditTaskDialogViewModel model = new ProjectAdminEditTaskDialogViewModel
            {
                TaskDetailTab = new ProjectAdminEditTaskDetailTabViewModel
                {
                    TaskId = task.Id,
                    OwnerId = userId,
                    Name = task.Name,
                    Notes = task.Description,
                    Deadline = task.Deadline,
                    SendDate = task.DeferredHandoverTime,
                    StartLimitDate = DateTime.Now,
                    AssignedTo = task.AssignedToId,
                    Priority = (TaskPriority)task.Priority,
                    Tags = string.IsNullOrEmpty(task.SearchTags) ? null : task.SearchTags.Split(','),
                },
                ProjectDetailsTab = new ProjectAdminProjectDetailsTabViewModel
                {
                    ProjectDescription = task.Project.Description,
                    ProjectName = task.Project.Name,
                    ProjectId = task.Project.Id
                },
                OutcomesTab = new ProjectAdminOutcomesTabViewModel
                {
                    Feedback = task.Feedback,
                    IsFeedbackRequired = task.IsFeedbackRequired,
                    TaskId = task.Id
                }
            };

            switch (task.TaskBoardTaskType)
            {
                case TaskBoardTaskType.NotSet:
                case TaskBoardTaskType.Draft:
                case TaskBoardTaskType.MyTask:
                    model.TaskDetailTab.SendingOption = TaskSendingOptions.Now;
                    break;
                case TaskBoardTaskType.Pending:
                    model.TaskDetailTab.SendingOption = TaskSendingOptions.Pending;
                    break;
                case TaskBoardTaskType.Received:
                    model.TaskDetailTab.SendingOption = TaskSendingOptions.Now;
                    break;
                default:
                    throw new Exception("Not editable task type.");
            }

            return model;
        }

        /// <summary>
        /// Update task
        /// </summary>
        /// <param name="model">Edit task view model</param>
        public void UpdateTask(ProjectAdminEditTaskDialogViewModel model)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(model.TaskDetailTab.OwnerId);
            RotationTask task = _logicCore.RotationTaskCore.GetTask(model.TaskDetailTab.TaskId);

            if (!user.IsOwnerOfTask(task))
            {
                throw new AccessDeniedException($"User with ID: {user.Id} is not owner of task with ID: {task.Id}");
            }

            if (!task.IsEditable())
            {
                throw new Exception("Task can not be edited.");
            }

            task.Name = model.TaskDetailTab.Name;
            task.Description = model.TaskDetailTab.Notes;
            task.Deadline = model.TaskDetailTab.Deadline.Value;
            task.Priority = (PriorityOfTask)model.TaskDetailTab.Priority;
            task.SearchTags = model.TaskDetailTab.Tags != null && model.TaskDetailTab.Tags.Any() ? string.Join(",", model.TaskDetailTab.Tags) : string.Empty;

            _logicCore.TaskBoardCore.UpdateTask(task, user, model.TaskDetailTab.AssignedTo.Value, null, model.TaskDetailTab.SendDate);
        }

        #endregion
    }
}
