﻿using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces.Services;
using System;
using System.Linq;
using MomentumPlus.Core.Authorization;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore.AccessValidation;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class AccessService : IAccessService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public AccessService(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._logicCore = logicCore;
            this._repositories = repositoriesUnitOfWork;
        }

        public bool UserIsProjectManager(Guid userId)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(userId);

            return user.ManagedProjects != null && user.ManagedProjects.Any();
        }

        public bool UserHasAccessToProject(Guid userId, Guid projectId, bool throwException = false)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(userId);

            if (!user.HasAccessToProject(projectId))
            {
                if(throwException)
                {
                    throw new Exception(string.Format("User with ID:{0} is not has access to project with ID:{1}", userId, projectId));
                }
                return false;
            }
            else
            {
                return true;
            }
        }


        public bool SafetyMessageArchiveV2IsEnable()
        {
            var result = _repositories.SafetyStatV2Repository.Count() > 0;

            return result;
        }

        public bool IsGlobalReportHistoryAndReceivedDisable(Guid userId)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(userId);
            string userRole = user.Role;

            bool userIsAdminOrLineManager = iHandoverRoles.RelayGroups.Admins.Split(new[] { ',' }).Contains(userRole)
                                          || iHandoverRoles.RelayGroups.LineManagers.Split(new[] { ',' }).Contains(userRole);

            return userIsAdminOrLineManager && !user.CurrentRotationId.HasValue;
        }

        public bool IsGlobalReportAvailable(Guid reportId, Guid userId, Models.RotationType reportType)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(userId);
            string userRole = user.Role;

            bool isAvailable = false;
            bool userIsSimpleUser = iHandoverRoles.RelayGroups.SimpleUser.Split(new[] { ',' }).Contains(userRole);

            switch (reportType)
            {
                case Models.RotationType.Swing:
                    Rotation rotation = _logicCore.RotationCore.GetRotation(reportId);
                    isAvailable = rotation.RotationOwnerId == userId || !userIsSimpleUser;
                    break;
                case Models.RotationType.Shift:
                    Shift shift = _logicCore.ShiftCore.GetShift(reportId);
                    isAvailable = shift.Rotation.RotationOwnerId == userId || !userIsSimpleUser;
                    break;
                default:
                    throw new Exception("Unsapported rotation type.");
            }

            return isAvailable;
        }

        public bool IsReportSharedForUser(Guid reportId, Guid userId)
        {
            return _repositories.ReportSharingRelationRepository.GetAll().Any(rs => rs.ReportId == reportId && rs.RecipientId == userId);
        }
    }
}
