﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Constants;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MvcPaging;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class ModuleService : IModuleService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public ModuleService(IRepositoriesUnitOfWork repositoriesUnitOfWork, IServicesUnitOfWork services, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            _logicCore = logicCore;
            _services = services;
        }

        public rModuleViewModel GetBaseModuleFromTypeViewModel(ModuleType type)
        {
            var module = _repositories.TemplateModuleRepository.Find(m => m.Type == (TypeOfModule)type && !m.ParentModuleId.HasValue).FirstOrDefault();

            return _mapper.Map<rModuleViewModel>(module);
        }

        public rModuleViewModel GetModuleViewModel(Guid moduleId)
        {
            var module = _repositories.TemplateModuleRepository.Get(moduleId);

            return _mapper.Map<rModuleViewModel>(module);
        }

        public IEnumerable<rModuleViewModel> GetBaseModulesViewModels()
        {
            var modules = _repositories.TemplateModuleRepository.Find(m => !m.ParentModuleId.HasValue && !m.DeletedUtc.HasValue && m.Type != TypeOfModule.Project);

            return _mapper.Map<IEnumerable<rModuleViewModel>>(modules);
        }


        public rModuleViewModel GetTemplateModuleViewModel(ModuleType type, Guid templateId)
        {
            var module = _repositories.TemplateModuleRepository.Find(m => m.TemplateId == templateId && m.Type == (TypeOfModule)type && !m.DeletedUtc.HasValue).FirstOrDefault();

            return _mapper.Map<rModuleViewModel>(module);
        }


        public rModuleViewModel PopulateBaseModuleViewModel(ModuleType moduleType, Guid? activeTopicGroupId, Guid? activeTopicId,
                int? topicGroupsPageNo, int? topicsPageNo, int? tasksPageNo)
        {
            var baseModule = _logicCore.ModuleCore.GetBaseModuleFromType(moduleType);

            var model = _mapper.Map<rModuleViewModel>(baseModule);

            model.ActiveTopicGroupId = activeTopicGroupId;
            model.ActiveTopicId = activeTopicId;

            model.TopicGroupsList = _services.TopicGroupService.GetModuleTopicGroupsViewModels(baseModule.Id)
                .ToPagedList(topicGroupsPageNo - 1 ?? 0, NumericConstants.Paginator.PaginatorPageSize);

            if (activeTopicGroupId.HasValue)
            {
                model.TopicsList = _services.TopicService.GetTopicGroupTopicsViewModels((Guid)activeTopicGroupId)
                    .ToPagedList(topicsPageNo - 1 ?? 0, NumericConstants.Paginator.PaginatorPageSize);
            }

            if (activeTopicId.HasValue)
            {
                model.TasksList = _services.TaskService.GetTopicTasksViewModels((Guid)activeTopicId)
                    .ToPagedList(tasksPageNo - 1 ?? 0, NumericConstants.Paginator.PaginatorPageSize);
            }


            return model;
        }

        public rModuleViewModel PopulateTemplateModuleViewModel(Guid templateId, ModuleType moduleType, Guid? activeTopicGroupId,
            Guid? activeTopicId, int? topicGroupsPageNo, int? topicsPageNo, int? tasksPageNo)
        {
            var baseModule = _logicCore.ModuleCore.GetBaseModuleFromType(moduleType);

            var templateModule = _logicCore.ModuleCore.GetOrCreateTemplateModule(templateId, baseModule);

            var model = _mapper.Map<rModuleViewModel>(templateModule);

            model.Enabled = templateModule.Enabled;

            model.ActiveTopicGroupId = activeTopicGroupId;
            model.ActiveTopicId = activeTopicId;

            var topicGroupsViewModels =
                _services.TopicGroupService.PopulateTemplateModuleTopicGroupsViewModels(baseModule.Id, templateModule.Id);

            model.EnabledTopicGroupsCounter = topicGroupsViewModels.Count(m => m.Enabled);

            model.TopicGroupsList = topicGroupsViewModels.ToPagedList(topicGroupsPageNo - 1 ?? 0, NumericConstants.Paginator.PaginatorPageSize);

            if (activeTopicGroupId.HasValue)
            {
                model.TopicsList = _services.TopicService.PopulateTemplateTopicGroupTopicsViewModels((Guid)activeTopicGroupId, templateModule.Id)
                    .ToPagedList(topicsPageNo - 1 ?? 0, NumericConstants.Paginator.PaginatorPageSize);
            }

            if (activeTopicId.HasValue)
            {
                model.TasksList = _services.TaskService.PopulateTemplateTopicTasksViewModels((Guid)activeTopicId, templateModule.Id)
                    .ToPagedList(tasksPageNo - 1 ?? 0, NumericConstants.Paginator.PaginatorPageSize);
            }


            return model;
        }


        public void ChangeTemplateModuleStatus(Guid baseModuleId, Guid templateId, bool status)
        {
            var baseModule = _logicCore.ModuleCore.GetModule(baseModuleId);

            var templateModule = _logicCore.ModuleCore.GetOrCreateTemplateModule(templateId, baseModule);

            //----- Adding Module to rotations live ----
            if (status)
            {
                _services.RotationModuleService.ChangeTemplateModuleChildRotationModulesStatus(templateModule.Id, status);
            }
            //------------------------------------------
            ChangeModuleStatus(templateModule.Id, status);
        }


        public void ChangeModuleStatus(Guid moduleId, bool status)
        {
            var module = _logicCore.ModuleCore.GetModule(moduleId);

            if (status == false)
            {
                _services.TopicGroupService.UpdateModuleTopicGroupsStatus(moduleId, status);
            }

            module.Enabled = status;

            _repositories.TemplateModuleRepository.Update(module);
            _repositories.Save();
        }


    }
}
