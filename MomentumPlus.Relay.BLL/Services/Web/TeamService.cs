﻿using MomentumPlus.Relay.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Interfaces;
using AutoMapper;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Core.Models;
using System.Web.Mvc;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces;
using System.Data.Entity.Core;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class TeamService : ITeamService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public TeamService(IRepositoriesUnitOfWork repositoriesUnitOfWork, IServicesUnitOfWork services, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            _logicCore = logicCore;
            _services = services;
        }

        public IEnumerable<TeamViewModel> GetAllTeams()
        {
            return _mapper.Map<IEnumerable<TeamViewModel>>(_repositories.TeamRepository.GetAll().ToList().OrderBy(t => t.Name));
        }

        public IEnumerable<SelectListItem> GetTeamList()
        {
            return _mapper.Map<IEnumerable<SelectListItem>>(_repositories.TeamRepository.GetAll().OrderBy(t => t.Name));
        }

        public IEnumerable<SelectListItem> GetTeamListByCriticalControl(Guid criticalControlId)
        {
            CriticalControl criticalControl = _repositories.CriticalControlRepository.Get(criticalControlId);
            if(criticalControl != null)
            {
                return _mapper.Map<IEnumerable<SelectListItem>>(criticalControl.Teams.OrderBy(t => t.Name));
            }

            throw new ObjectNotFoundException(string.Format("CriticalControl with Id: {0} was not found.", criticalControlId));
        }

        public string GetTeamName(Guid teamId)
        {
            var teamName = _repositories.TeamRepository.Get(teamId).Name;

            return teamName;
        }

        public bool AddTeam(TeamViewModel team)
        {
            if (!_repositories.TeamRepository.Find(m => m.Name == team.Name).Any())
            {
                Team teamEntity = _mapper.Map<Team>(team);
                teamEntity.Id = Guid.NewGuid();

                _repositories.TeamRepository.Add(teamEntity);
                _repositories.Save();

                return true;
            }

            return false;
        }


        public IEnumerable<SelectListItem> GetTeamMembersList(Guid teamId)
        {
            return _mapper.Map<IEnumerable<SelectListItem>>(_logicCore.TeamCore.GetTeamMembers(teamId));
        }


        public IEnumerable<SelectListItem> GetTeamMembersListWidthOther(Guid teamId)
        {
            IEnumerable<UserProfile> users = _logicCore.TeamCore.GetTeamMembers(teamId);
            List<SelectListItem> teamMembers = _mapper.Map<List<SelectListItem>>(users);
            teamMembers = new List<SelectListItem>(teamMembers.OrderBy(t => t.Text));

            teamMembers.Add(new SelectListItem { Text = "Other", Value = Guid.Empty.ToString() });

            return teamMembers;
        }



        public TeamViewModel GetTeam(Guid teamId)
        {
            Team team = _repositories.TeamRepository.Get(teamId);
            if(team != null)
            {
                return _mapper.Map<TeamViewModel>(team);
            }

            return null;
        }

        public bool UpdateTeam(TeamViewModel team)
        {
            if(team.Id.HasValue)
            {
                Team teamEntity = _repositories.TeamRepository.Get(team.Id.Value);
                if(teamEntity != null)
                {
                    _mapper.Map(team, teamEntity);

                    _repositories.TeamRepository.Update(teamEntity);
                    _repositories.Save();

                    return true;
                }
            }

            return false;
        }

        public TeammatesDialogViewModel PopulateTeammatesDialogViewModel(Guid userId, bool showMe, bool showOnSite, bool showOffSite, bool showWithoutRotation)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(userId);

            IEnumerable<UserProfile> userTeamMembers = _logicCore.TeamCore.GetTeamMembers(user.TeamId).Where(u => u.Id != userId).OrderBy(u => u.FirstName);

            IEnumerable<UserProfile> onSiteTeammates = userTeamMembers.Where(u => u.CurrentRotation != null && u.CurrentRotation.State == RotationState.Confirmed);
            IEnumerable<UserProfile> offSiteTeammates = userTeamMembers.Where(u => u.CurrentRotation != null && u.CurrentRotation.State != RotationState.Confirmed);
            IEnumerable<UserProfile> withOutRotation = userTeamMembers.Where(u => !u.HandoverSourceRotations.Any());

            TeammatesDialogViewModel model = new TeammatesDialogViewModel
            {
                RequestOwner = showMe ? new TeammateViewModel { Id = user.Id, FullName = user.FullName } : null,
                OnSiteTeammates = showOnSite ? onSiteTeammates.Select(u => new TeammateViewModel { Id = u.Id, FullName = u.FullName }) : null,
                OffSiteTeammates = showOffSite ? offSiteTeammates.Select(u => new TeammateViewModel { Id = u.Id, FullName = u.FullName }) : null,
                WithoutRotation = showWithoutRotation ? withOutRotation.Select(u => new TeammateViewModel { Id = u.Id, FullName = u.FullName }) : null,
            };

            return model;
        }

    }
}
