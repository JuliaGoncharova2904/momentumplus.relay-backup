﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.Interfaces;
using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Mapper;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class ChecklistService : IChecklistService
    {
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public ChecklistService(IServicesUnitOfWork services, LogicCoreUnitOfWork logicCore)
        {
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
            _services = services;
        }

        public ChecklistViewModel GetChecklistForRotation(Guid rotationId, Guid currentUserId)
        {
            ChecklistViewModel model = new ChecklistViewModel
            {
                RotationId = rotationId,
                CurrentRotationId = _services.RotationService.GetCurrentRotationId(rotationId),
                RotationOwnerFullName = _services.RotationService.GetRotationOwnerFullName(rotationId)
            };

            return model;
        }

        public ChecklistRotationFilterViewModel GetRotationsForFilter(Guid? rotationId)
        {
            if (rotationId.HasValue)
            {
                Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId.Value);
                if (rotation != null)
                {
                    ChecklistRotationFilterViewModel model = _mapper.Map<ChecklistRotationFilterViewModel>(rotation);
                    model.IsCurrentRotation = false;
                    if (rotation.RotationOwner.CurrentRotationId.HasValue && rotation.RotationOwner.CurrentRotationId == rotationId.Value)
                    {
                        model.IsCurrentRotation = true;
                    }

                    return model;
                }
            }

            return null;
        }

        public TeamCheckListViewModel GetTeamForCheckList(Guid? rotationId)
        {
            if (rotationId.HasValue)
            {
                Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId.Value);

                bool isShift = _services.RotationService.IsShiftRotation(rotationId.Value);

                UserProfile user = _logicCore.UserProfileCore.GetRelayEmployees().Where(u => u.Id == rotation.RotationOwnerId).FirstOrDefault();
                TeamCheckListViewModel model = new TeamCheckListViewModel();

                BackToBackForTeamViewModel backTobackViewModel = _mapper.Map<BackToBackForTeamViewModel>(user.CurrentRotation.DefaultBackToBack);
                backTobackViewModel.CountReceivedTask = isShift 
                    ? _services.RotationTaskService.GetShiftReceivedTasks(rotationId.Value, user.CurrentRotation.DefaultBackToBack.Id).Count()
                    : _logicCore.RotationTaskCore.GetReceivedTasksByCreator(rotationId.Value, user.CurrentRotation.DefaultBackToBack.Id).Count();
                model.BackToBack = backTobackViewModel;

                LineManagerForTeamViewModel lineManagerViewModel = _mapper.Map<LineManagerForTeamViewModel>(user.CurrentRotation.LineManager);
                lineManagerViewModel.CountReceivedTask = isShift
                    ? _services.RotationTaskService.GetShiftReceivedTasks(rotationId.Value, user.CurrentRotation.LineManager.Id).Count()
                    : _logicCore.RotationTaskCore.GetReceivedTasksByCreator(rotationId.Value, user.CurrentRotation.LineManager.Id).Count();
                model.LineManager = lineManagerViewModel;

                IEnumerable<UserProfile> employees = _logicCore.UserProfileCore.GetEmployeesByTeam(user.TeamId)
                    .Where(u => u.CurrentRotation != null && u.Id != user.CurrentRotation.DefaultBackToBack.Id && u.Id != user.Id && u.Id != user.CurrentRotation.LineManager.Id)
                    .ToList()
                    .OrderBy(u => u.FullName);
                IEnumerable<TeamMemberViewModel> teamMember = _mapper.Map<IEnumerable<TeamMemberViewModel>>(employees);
                foreach (var userMember in teamMember)
                {
                    userMember.CountReceivedTask = isShift
                        ? _services.RotationTaskService.GetShiftReceivedTasks(rotationId.Value, userMember.UserId).Count()
                        : _logicCore.RotationTaskCore.GetReceivedTasksByCreator(rotationId.Value, userMember.UserId).Count();
                }
                model.TeamMember = teamMember;
                model.RotationId = rotationId.Value;

                return model;
            }
            return null;
        }

    }
}
