﻿using MomentumPlus.Relay.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Interfaces;
using AutoMapper;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class RotationPatternService : IRotationPatternService
    {
        private IRepositoriesUnitOfWork _repositories;
        private IMapper _mapper;

        public RotationPatternService(IRepositoriesUnitOfWork repositoriesUnitOfWork)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
        }

        public IEnumerable<RotationPatternViewModel> GetAllRotationPatterns()
        {
            IQueryable<RotationPattern> rotationPatterns = _repositories.RotationPatternRepository.GetAll().OrderBy(p => p.DayOn);
            return _mapper.Map<IEnumerable<RotationPatternViewModel>>(rotationPatterns);
        }

        public IEnumerable<SelectListItem> GetRotationPatternsList()
        {
            var rotationPatterns = _repositories.RotationPatternRepository.GetAll().OrderBy(p => p.DayOn);
            return _mapper.Map<IEnumerable<SelectListItem>>(rotationPatterns);
        }


        public bool RotationPatternExist(RotationPatternViewModel model)
        {
            return _repositories.RotationPatternRepository.Find(m => m.DayOn == model.DayOn &&
                                                                        m.DayOff == model.DayOff &&
                                                                        m.Id != model.Id
                                                                ).Any();
        }

        public void AddRotationpattern(RotationPatternViewModel model)
        {
            RotationPattern rotationPatern = _mapper.Map<RotationPattern>(model);
            _repositories.RotationPatternRepository.Add(rotationPatern);
            _repositories.Save();
        }

        public RotationPatternViewModel GetRotationPaternById(Guid Id)
        {
            RotationPattern rotationPatern = _repositories.RotationPatternRepository.Get(Id);
            if (rotationPatern != null)
            {
                return _mapper.Map<RotationPatternViewModel>(rotationPatern);
            }

            return null;
        }

        public void UpdateRotationPattern(RotationPatternViewModel model)
        {
            RotationPattern rotationPatern = _mapper.Map<RotationPattern>(model);
            _repositories.RotationPatternRepository.Update(rotationPatern);
            _repositories.Save();
        }
    }
}
