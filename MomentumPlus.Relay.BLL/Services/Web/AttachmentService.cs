﻿using System;
using System.Collections.Generic;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Models;
using System.Linq;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.BLL.LogicCore.AccessValidation;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class AttachmentService : IAttachmentService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;

        public AttachmentService(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
        }

        /// <summary>
        /// Return attachments view models for topic.
        /// </summary>
        /// <param name="topicId">Topic ID</param>
        /// <returns></returns>
        public IEnumerable<AttachmentViewModel> GetAttachmentForTopic(Guid topicId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            return this.MapAttachmentsToViewModel(topic.Attachments);
        }

        /// <summary>
        /// Return attachments view models for task.
        /// </summary>
        /// <param name="taskId">Task ID</param>
        /// <returns></returns>
        public IEnumerable<AttachmentViewModel> GetAttachmentForTask(Guid taskId)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);

            return this.MapAttachmentsToViewModel(task.Attachments);
        }

        /// <summary>
        /// Map attachment entities to view models.
        /// </summary>
        /// <param name="attachments">Attachments collection</param>
        /// <returns></returns>
        private IEnumerable<AttachmentViewModel> MapAttachmentsToViewModel(IEnumerable<Attachment> attachments)
        {
            List<AttachmentViewModel> attachViewModels = new List<AttachmentViewModel>();

            foreach (Attachment attachment in attachments)
            {
                attachViewModels.Add(new AttachmentViewModel
                {
                    Id = attachment.Id,
                    Name = attachment.Name,
                    FileName = attachment.FileName,
                    AddedTime = attachment.CreatedUtc.FormatDayMonth(),
                    FileId = attachment.FileId
                });
            }

            return attachViewModels;
        }

        /// <summary>
        /// Add attachment to topic from view model
        /// </summary>
        /// <param name="attachment">Attachment view model</param>
        /// <returns></returns>
        public void AddAttachmentToTopic(AttachmentViewModel attachment)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(attachment.TargetId.Value);


            File file = _logicCore.MediaCore.AddFile(attachment.File.FileName,
                                                    attachment.File.ContentType,
                                                    attachment.File.InputStream,
                                                    false);

            Attachment attachEntity = _logicCore.AttachmentCore.AddAttachment(attachment.Name, attachment.File.FileName, file);

            topic.Attachments.Add(attachEntity);
            _logicCore.RotationTopicCore.UpdateTopic(topic);

            //if (topic.ChildRotationTopics.Any())
            //{
            //    RotationTopic childTopic = topic.ChildRotationTopics.FirstOrDefault();
            //    Attachment childAttachEntity = _logicCore.AttachmentCore.AddAttachment(attachment.Name, attachment.File.FileName, file);

            //    childTopic.Attachments.Add(childAttachEntity);
            //    _logicCore.RotationTopicCore.UpdateTopic(childTopic);
            //}
        }

        /// <summary>
        /// Add attachment to task from view model
        /// </summary>
        /// <param name="attachment">Attachment view model</param>
        public void AddAttachmentToTask(AttachmentViewModel attachment)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(attachment.TargetId.Value);

            if (!task.IsEditable())
                throw new Exception($"Task ID: {task.Id} is not editable");

            File file = _logicCore.MediaCore.AddFile(attachment.File.FileName,
                                                        attachment.File.ContentType,
                                                        attachment.File.InputStream,
                                                        false);

            Attachment attachEntity = _logicCore.AttachmentCore.AddAttachment(attachment.Name, attachment.File.FileName, file);

            task.Attachments.Add(attachEntity);
            _logicCore.RotationTaskCore.Update(task);

            var childTask = task.SuccessorTask;

            while (childTask != null)
            {
                Attachment childAttachEntity = _logicCore.AttachmentCore.AddAttachment(attachment.Name, attachment.File.FileName, file);

                childTask.Attachments.Add(childAttachEntity);
                _logicCore.RotationTaskCore.Update(childTask);

                childTask = childTask.SuccessorTask;
            }
        }

        /// <summary>
        /// Remove attachment from topic by id.
        /// </summary>
        /// <param name="topicId">Task ID</param>
        /// <param name="attachmentId">Attachment ID</param>
        public void RemoveAttachmentFromTopic(Guid topicId, Guid attachmentId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            if (!topic.Attachments.Any(a => a.Id == attachmentId))
                throw new Exception(string.Format("Topic ID: {0} is not contain attachment with ID: {1}", topic.Id, attachmentId));

            Attachment attachment = _logicCore.AttachmentCore.GetAttachment(attachmentId);
            _logicCore.AttachmentCore.RemoveAttachment(attachment);
        }

        /// <summary>
        /// Remove attachment from task by id.
        /// </summary>
        /// <param name="taskId">Task Id</param>
        /// <param name="attachmentId">Attachment ID</param>
        public void RemoveAttachmentFromTask(Guid taskId, Guid attachmentId)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);

            if (!task.IsEditable())
                throw new Exception($"Task ID: {task.Id} is not editable");

            if (!task.Attachments.Any(a => a.Id == attachmentId))
                throw new Exception($"Task ID: {task.Id} is not contain attachment with ID: {attachmentId}");

            Attachment attachment = _logicCore.AttachmentCore.GetAttachment(attachmentId);
            _logicCore.AttachmentCore.RemoveAttachment(attachment);
        }
    }
}
