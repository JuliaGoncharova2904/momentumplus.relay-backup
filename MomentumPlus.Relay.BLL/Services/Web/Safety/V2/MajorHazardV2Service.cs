﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class MajorHazardV2Service : IMajorHazardV2Service
    {
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;

        public MajorHazardV2Service(LogicCoreUnitOfWork logicCore)
        {
            this._logicCore = logicCore;
            this._mapper = WebAutoMapperConfig.GetMapper();
        }

        public IEnumerable<MajorHazardV2ViewModel> GetAllMajorHazards()
        {
            IEnumerable<MajorHazardV2> majorHazards = _logicCore.MajorHazardV2Core.GetAllMajorHazards();

            return _mapper.Map<IEnumerable<MajorHazardV2ViewModel>>(majorHazards);
        }

        public MajorHazardV2ViewModel GetMajorHazard(Guid majorHazardId)
        {
            MajorHazardV2 majorHazard = _logicCore.MajorHazardV2Core.GetMajorHazard(majorHazardId);

            return _mapper.Map<MajorHazardV2ViewModel>(majorHazard);
        }

        public EditMajorHazardV2ViewModel GetMajorHazardForEdit(Guid majorHazardId)
        {
            IEnumerable<UserProfile> allUsers = _logicCore.UserProfileCore.GetAllUsers().OrderBy(u => u.FullName);
            MajorHazardV2 majorHazard = _logicCore.MajorHazardV2Core.GetMajorHazard(majorHazardId);
            IEnumerable<CriticalControlV2> criticalContols = _logicCore.CriticalControlV2Core.GetAllCriticalControls(majorHazard);

            EditMajorHazardV2ViewModel model = new EditMajorHazardV2ViewModel
            {
                Id = majorHazardId,
                OwnerId = majorHazard.OwnerId.HasValue ? majorHazard.OwnerId.Value : Guid.Empty,
                Owners = _mapper.Map<IEnumerable<SelectListItem>>(allUsers),
                CriticalControls = _mapper.Map<IEnumerable<CriticalControlV2ViewModel>>(criticalContols)
            };

            return model;
        }

        public void UpdateMajorHazard(EditMajorHazardV2ViewModel model)
        {
            MajorHazardV2 majorHazard = _logicCore.MajorHazardV2Core.GetMajorHazard(model.Id);
            majorHazard.OwnerId = model.OwnerId != Guid.Empty ? (Guid?)model.OwnerId : null;
            _logicCore.MajorHazardV2Core.UpdateMajorHazard(majorHazard);
        }
    }
}
