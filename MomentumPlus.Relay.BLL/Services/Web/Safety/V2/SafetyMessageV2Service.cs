﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class SafetyMessageV2Service : ISafetyMessageV2Service
    {
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;

        public SafetyMessageV2Service(LogicCoreUnitOfWork logicCore)
        {
            this._logicCore = logicCore;
            this._mapper = WebAutoMapperConfig.GetMapper();
        }

        public EditSafetyMessageV2ViewModel PopulateEditSafetyMessageModel(DateTime? date)
        {
            EditSafetyMessageV2ViewModel model = new EditSafetyMessageV2ViewModel
            {
                Date = date.HasValue ? date.Value : DateTime.Now
            };

            return this.PopulateEditSafetyMessageModel(model);
        }

        public EditSafetyMessageV2ViewModel PopulateEditSafetyMessageModel(EditSafetyMessageV2ViewModel model)
        {
            IEnumerable<MajorHazardV2> majorHazards = _logicCore.MajorHazardV2Core.GetAllMajorHazards().OrderBy(mh => mh.Name);
            model.MajorHazards = _mapper.Map<IEnumerable<SelectListItem>>(majorHazards);

            MajorHazardV2 majorHazard = majorHazards.Where(mh => mh.Id == model.MajorHazardId).FirstOrDefault();

            if (majorHazard != null)
            {
                IEnumerable<CriticalControlV2> criticalControls = _logicCore.CriticalControlV2Core.GetAllCriticalControls(majorHazard);
                model.CriticalControls = _mapper.Map<IEnumerable<SelectListItem>>(criticalControls);

                CriticalControlV2 criticalControl = criticalControls.Where(cc => cc.Id == model.CriticalControlId).FirstOrDefault();

                if (criticalControl != null)
                {
                    model.CriticalControlOwnerName = criticalControl.Owner.FullName;
                    model.CriticalControlChampions = criticalControl.Champions.Select(c => c.FullName).ToList();
                }
                else
                {
                    model.CriticalControlChampions = new string[0];
                }
            }
            else
            {
                model.CriticalControls = new SelectListItem[0];
                model.CriticalControlChampions = new string[0];
            }

            return model;
        }

        public SafetyMessageV2ViewModel GetSafetyMessage(Guid safetyMessageId)
        {
            SafetyMessageV2 safetyMessage = _logicCore.SafetyMessageV2Core.GetSafetyMessage(safetyMessageId);

            return _mapper.Map<SafetyMessageV2ViewModel>(safetyMessage);
        }

        public IEnumerable<SafetyMessageV2ViewModel> GetSafetyMessagesForUser(DateTime date, Guid userId)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(userId);
            IEnumerable<SafetyMessageV2> safetyMessages = _logicCore.SafetyMessageV2Core.GetSafetyMessagesForUser(date, user);

            return _mapper.Map<IEnumerable<SafetyMessageV2ViewModel>>(safetyMessages);
        }

        public void SetSafetyMessageShowForUser(Guid safetyMessageId, Guid userId)
        {
            SafetyMessageV2 safetyMessage = _logicCore.SafetyMessageV2Core.GetSafetyMessage(safetyMessageId);
            SafetyRecipientStateV2 state = _logicCore.SafetyMessageStatV2Core.CalcViewingState(safetyMessage, userId, DateTime.Today);
            _logicCore.SafetyMessageStatV2Core.SetRecipientState(safetyMessage, userId, state);
        }

        public SafetyMessageRescheduleV2ViewModel GetSafetyMessageForRescheduling(Guid safetyMessageId)
        {
            SafetyMessageV2 safetyMessage = _logicCore.SafetyMessageV2Core.GetSafetyMessage(safetyMessageId);

            if (safetyMessage.Date > DateTime.Today)
                throw new Exception(string.Format("Safety Message with Id: {0} cannt be rescheduled.", safetyMessageId));

            SafetyMessageRescheduleV2ViewModel model = new SafetyMessageRescheduleV2ViewModel
            {
                Date = DateTime.Today.AddDays(1),
                MajorHazardId = safetyMessage.CriticalControl.MajorHazardId,
                MajorHazardName = safetyMessage.CriticalControl.MajorHazard.Name,
                CriticalControlId = safetyMessage.CriticalControlId,
                CriticalControlName = safetyMessage.CriticalControl.Name,
                CriticalControlOwnerName = safetyMessage.CriticalControl.Owner.FullName,
                CriticalControlChampions = safetyMessage.CriticalControl.Champions.Select(c => c.FullName).ToList(),
                Message = safetyMessage.Message
            };

            return model;
        }

        public void RescheduleSafetyMessage(SafetyMessageRescheduleV2ViewModel model)
        {
            SafetyMessageV2 safetyMessage = new SafetyMessageV2
            {
                Date = model.Date,
                CriticalControlId = model.CriticalControlId,
                Message = model.Message
            };

            _logicCore.SafetyMessageV2Core.AddSafetyMessage(safetyMessage);
        }

        public EditSafetyMessageV2ViewModel GetSafetyMessageForEdit(Guid safetyMessageId)
        {
            SafetyMessageV2 safetyMessage = _logicCore.SafetyMessageV2Core.GetSafetyMessage(safetyMessageId);

            if (safetyMessage.Date <= DateTime.Today)
                throw new Exception(string.Format("Safety Message with Id: {0} cannt be edited.", safetyMessageId));

            EditSafetyMessageV2ViewModel model = new EditSafetyMessageV2ViewModel
            {
                Id = safetyMessage.Id,
                Date = safetyMessage.Date,
                MajorHazardId = safetyMessage.CriticalControl.MajorHazardId,
                CriticalControlId = safetyMessage.CriticalControlId,
                CriticalControlOwnerName = safetyMessage.CriticalControl.Owner.FullName,
                Message = safetyMessage.Message
            };

            return this.PopulateEditSafetyMessageModel(model);
        }

        public void UpdateSafetyMessage(EditSafetyMessageV2ViewModel model)
        {
            if (model.Date <= DateTime.Today)
                throw new Exception("Wrong scheduling date of safety message.");

            SafetyMessageV2 safetyMessage = _logicCore.SafetyMessageV2Core.GetSafetyMessage(model.Id);

            safetyMessage.Date = model.Date;
            safetyMessage.Message = model.Message;
            safetyMessage.CriticalControlId = model.CriticalControlId;

            _logicCore.SafetyMessageV2Core.UpdateSafetyMessage(safetyMessage);
        }

        public void AddSafetyMessage(EditSafetyMessageV2ViewModel model)
        {
            if (model.Date <= DateTime.Today)
                throw new Exception("Wrong scheduling date of safety message.");

            SafetyMessageV2 safetyMessage = new SafetyMessageV2
            {
                Date = model.Date,
                CriticalControlId = model.CriticalControlId,
                Message = model.Message
            };

            _logicCore.SafetyMessageV2Core.AddSafetyMessage(safetyMessage);
        }

        public SafetyCalendarV2ViewModel GetSafetyCalendar(DateTime date)
        {
            DateTime[] dates = GenerateDatesArray(date);
            IEnumerable<SafetyMessageV2> messages = _logicCore.SafetyMessageV2Core.GetSafetyMessagesForMonth(date.Year, date.Month);

            SafetyCalendarV2ViewModel model = new SafetyCalendarV2ViewModel
            {
                MonthYear = string.Format("{0:MMM yyyy}", date),
                Date = date,
                CurrentDate = DateTime.Today,
                Dates = new SafetyCalendarDateV2ViewModel[dates.Length]
            };

            for (int i = 0; i < dates.Length; ++i)
            {
                int messagesNumber = messages.Where(msg => msg.Date == dates[i]).Count();

                model.Dates[i] = new SafetyCalendarDateV2ViewModel
                {
                    Date = dates[i],
                    MessagesNumber = messagesNumber
                };

                if (messagesNumber == 1)
                {
                    SafetyMessageV2 message = messages.Where(msg => msg.Date == dates[i]).FirstOrDefault();
                    model.Dates[i].IconUrl = message.CriticalControl.MajorHazard.IconPath;
                }
            }

            return model;
        }

        private DateTime[] GenerateDatesArray(DateTime date)
        {
            DateTime[] dates = new DateTime[42];
            DateTime startDate = new DateTime(date.Year, date.Month, 1);

            int pervMonthDays = (startDate.DayOfWeek == 0) ? 6 : (int)startDate.DayOfWeek - 1;

            startDate = startDate.AddDays(-pervMonthDays);

            for (int i = 0; i < dates.Length; ++i)
            {
                dates[i] = startDate.AddDays(i);
            }

            return dates;
        }

        public SafetyMessagesForDateV2ViewModel GetSafetyMessagesForDate(DateTime date)
        {
            IEnumerable<SafetyMessageV2> messages = _logicCore.SafetyMessageV2Core.GetSafetyMessagesForDate(date);

            SafetyMessagesForDateV2ViewModel model = new SafetyMessagesForDateV2ViewModel
            {
                DateString = date.ToString("dd MMM yy"),
                IsExpired = date.Date <= DateTime.Today,
                SafetyMessages = _mapper.Map<IEnumerable<SafetyMessageV2ViewModel>>(messages)
            };

            return model;
        }
    }
}
