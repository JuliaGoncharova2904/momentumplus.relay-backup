﻿using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Services.Web
{ 
    public class SafetyStatV2Service : ISafetyStatV2Service
    {
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;

        public SafetyStatV2Service(LogicCoreUnitOfWork logicCore)
        {
            this._logicCore = logicCore;
            this._mapper = WebAutoMapperConfig.GetMapper();
        }

        public IEnumerable<ArchivedSafetyMessageV2ViewModel> PopulateArchivedSafetyMessagesViewModel()
        {
            var archivedSafetyMessages = _logicCore.SafetyMessageStatV2Core.GetAllArchivedSafetyMessages().OrderByDescending(m => m.Date);

            return _mapper.Map<IEnumerable<ArchivedSafetyMessageV2ViewModel>>(archivedSafetyMessages);
        }
    }
}
