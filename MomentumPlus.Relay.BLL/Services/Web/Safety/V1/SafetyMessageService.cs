﻿using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Web.Mvc;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class SafetyMessageService : ISafetyMessageService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public SafetyMessageService(IRepositoriesUnitOfWork repositoriesUnitOfWork, IServicesUnitOfWork services, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            _logicCore = logicCore;
            _services = services;
        }

        public EditSafetyMessageViewModel PopulateSafetyMessageModel()
        {
            return this.PopulateSafetyMessageModel(new EditSafetyMessageViewModel());
        }

        public EditSafetyMessageViewModel PopulateSafetyMessageModel(EditSafetyMessageViewModel model)
        {
            model.MajorHazards = _services.MajorHazardService.GetMajorHazardsList();
            if (model.MajorHazard != Guid.Empty)
            {
                model.CriticalControls = _services.CriticalControlService.GetCriticalControlsListByMajorHazard(model.MajorHazard);
                if (model.CriticalControl != Guid.Empty)
                {
                    model.AllTeams = _services.TeamService.GetTeamListByCriticalControl(model.CriticalControl);
                }
                else
                {
                    model.AllTeams = new SelectListItem[0];
                }
            }
            else
            {
                model.AllTeams = new SelectListItem[0];
                model.CriticalControls = new SelectListItem[0];
            }

            return model;
        }

        public SafetyCalendarViewModel GetSafetyCalendar(DateTime date, Guid? teamId = null)
        {
            DateTime[] dates = GenerateDatesArray(date);
            IQueryable<SafetyMessage> messagesQuery = _repositories.SafetyMessageRepository.Find(sm => dates.Contains(sm.Date));

            if (teamId.HasValue)
            {
                messagesQuery = messagesQuery.Where(sm => sm.Teams.Select(t => t.Id).Contains(teamId.Value));
            }

            IEnumerable<SafetyMessage> messages = messagesQuery.ToList();

            SafetyCalendarViewModel model = new SafetyCalendarViewModel
            {
                Month = string.Format("{0:MMMM}", date),
                MonthYear = string.Format("{0:MMM yyyy}", date),
                Date = date,
                Dates = new SafetyCalendarDateViewModel[dates.Length]
            };

            for (int i = 0; i < dates.Length; ++i)
            {
                int messagesNumber = messages.Where(msg => msg.Date == dates[i]).Count();

                model.Dates[i] = new SafetyCalendarDateViewModel
                {
                    DayNumber = dates[i].Day.ToString(),
                    Date = dates[i],
                    MessagesNumber = messagesNumber
                };

                if (messagesNumber == 1)
                {
                    SafetyMessage message = messages.Where(msg => msg.Date == dates[i]).FirstOrDefault();
                    model.Dates[i].MessaageIconId = message.CriticalControl.MajorHazard.Icon.ImageId;
                    model.Dates[i].MessageId = message.Id;
                }
            }

            return model;
        }

        private DateTime[] GenerateDatesArray(DateTime date)
        {
            DateTime[] dates = new DateTime[42];
            DateTime startDate = new DateTime(date.Year, date.Month, 1);

            int pervMonthDays = (startDate.DayOfWeek == 0) ? 6 : (int)startDate.DayOfWeek - 1;

            startDate = startDate.AddDays(-pervMonthDays);

            for (int i = 0; i < dates.Length; ++i)
            {
                dates[i] = startDate.AddDays(i);
            }

            return dates;
        }

        public IEnumerable<SelectListItem> GetSafetyMessagesList()
        {
            IEnumerable<SafetyMessage> safetyMessages = _repositories.SafetyMessageRepository.GetAll().ToList();
            return _mapper.Map<IEnumerable<SelectListItem>>(safetyMessages);
        }

        public SafetyMessagesViewModel GetSafetyMessagesByDateAndTeam(DateTime date, Guid teamId)
        {
            IEnumerable<SafetyMessage> safetyMessages = _repositories.SafetyMessageRepository
                                                                     .Find(s => s.Date == date && s.Teams.Select(t => t.Id).Contains(teamId)).ToList();
            SafetyMessagesViewModel model = new SafetyMessagesViewModel
            {
                Date = date.FormatWithDayMonthYear(),
                SafetyMessages = _mapper.Map<IEnumerable<SelectListItem>>(safetyMessages)
            };

            return model;
        }

        public SafetyMessageViewModel GetSafetyMessageForPreview(Guid safetyMessageId)
        {
            SafetyMessage safetyMessage = _repositories.SafetyMessageRepository.Get(safetyMessageId);
            if (safetyMessage != null)
            {
                return _mapper.Map<SafetyMessageViewModel>(safetyMessage);
            }

            throw new ObjectNotFoundException(string.Format("SafetyMessage with Id: {0} was not found.", safetyMessageId));
        }

        public EditSafetyMessageViewModel GetSafetyMessage(Guid safetyMessageId)
        {
            SafetyMessage safetyMessage = _repositories.SafetyMessageRepository.Get(safetyMessageId);
            if (safetyMessage != null)
            {
                EditSafetyMessageViewModel model = _mapper.Map<EditSafetyMessageViewModel>(safetyMessage);
                return this.PopulateSafetyMessageModel(model);
            }
            else
            {
                throw new ObjectNotFoundException(string.Format("SafetyMessage with Id: {0} was not found.", safetyMessageId));
            }
        }

        public void UpdateSafetyMessage(EditSafetyMessageViewModel model)
        {
            SafetyMessage safetyMessage = _repositories.SafetyMessageRepository.Get(model.Id);
            if (safetyMessage != null)
            {
                _mapper.Map(model, safetyMessage);

                safetyMessage.Teams.Clear();

                if (model.Teams != null && model.Teams.Count > 0)
                {
                    _repositories.TeamRepository
                        .Find(c => model.Teams.Contains(c.Id))
                        .ToList().ForEach(t =>
                        {
                            safetyMessage.Teams.Add(t);
                        });
                }

                _repositories.SafetyMessageRepository.Update(safetyMessage);
                _repositories.Save();
            }
            else
            {
                throw new ObjectNotFoundException(string.Format("SafetyMessage with Id: {0} was not found.", model.Id));
            }
        }

        public void AddSafetyMessage(EditSafetyMessageViewModel model)
        {
            CriticalControl criticalControl = _repositories.CriticalControlRepository.Get(model.CriticalControl);
            if (criticalControl != null)
            {
                SafetyMessage safetyMessage = new SafetyMessage
                {
                    Id = Guid.NewGuid(),
                    Date = model.Date,
                    Message = model.Message,
                    CriticalControlId = model.CriticalControl,
                    Teams = new HashSet<Team>()
                };

                if (model.Teams != null && model.Teams.Count > 0)
                {
                    _repositories.TeamRepository
                        .Find(c => model.Teams.Contains(c.Id))
                        .ToList().ForEach(t =>
                        {
                            safetyMessage.Teams.Add(t);
                        });
                }

                _repositories.SafetyMessageRepository.Add(safetyMessage);
                _repositories.Save();
            }
            else
            {
                throw new ObjectNotFoundException(string.Format("CriticalControl with Id: {0} was not found.", model.CriticalControl));
            }
        }

        public IEnumerable<SafetyMessageViewModel> GetSafetyMessageForUser(Guid userId, DateTime date)
        {
            UserProfile user = _repositories.UserProfileRepository.Get(userId);
            if (user != null)
            {
                IQueryable<SafetyMessage> safetyMessages = _repositories.SafetyMessageRepository.Find(sm => sm.Date == date.Date &&
                                                                                                        sm.Teams.Any(t => t.Id == user.TeamId) &&
                                                                                                        !sm.Users.Any(u => u.Id == userId));
                return _mapper.Map<IEnumerable<SafetyMessageViewModel>>(safetyMessages);
            }

            throw new ObjectNotFoundException(string.Format("User with Id: {0} was not found.", userId));
        }

        public void SetSafetyMessageShowForUser(Guid userId, Guid safetyMessageId)
        {
            SafetyMessage safetyMessage = _repositories.SafetyMessageRepository.Get(safetyMessageId);
            UserProfile user = _repositories.UserProfileRepository.Get(userId);

            if (safetyMessageId == null)
            {
                throw new ObjectNotFoundException(string.Format("SafetyMessage with Id: {0} was not found.", safetyMessageId));
            }

            if (user != null && safetyMessageId != null)
            {
                if (safetyMessage.Users == null)
                {
                    safetyMessage.Users = new List<UserProfile>();
                }
                safetyMessage.Users.Add(user);
                safetyMessage.OpenCounter++;
                _repositories.SafetyMessageRepository.Update(safetyMessage);
                _repositories.Save();
            }
            else
            {
                throw new ObjectNotFoundException(string.Format("User with Id: {0} was not found.", userId));
            }
        }
    }
}
