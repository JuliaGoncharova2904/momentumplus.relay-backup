﻿using System;
using System.Collections.Generic;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Models;
using System.Web.Mvc;
using System.Linq;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class MajorHazardService : IMajorHazardService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public MajorHazardService(IRepositoriesUnitOfWork repositoriesUnitOfWork, IServicesUnitOfWork services, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            _logicCore = logicCore;
            _services = services;
        }

        public IEnumerable<SelectListItem> GetMajorHazardsList()
        {
            IQueryable<MajorHazard> majorHazard = _repositories.MajorHazardRepository.GetAll();
            return _mapper.Map<IEnumerable<SelectListItem>>(majorHazard);
        }

        public IEnumerable<MajorHazardViewModel> GetAll()
        {
            var majorHazards = _repositories.MajorHazardRepository.GetAll();

            return _mapper.Map<IEnumerable<MajorHazardViewModel>>(majorHazards);
        }

        public EditMajorHazardViewModel GetMajorHazard(Guid majorHazardId)
        {
            MajorHazard majorHazard = _repositories.MajorHazardRepository.Get(majorHazardId);
            if(majorHazard != null)
            {
                return _mapper.Map<EditMajorHazardViewModel>(majorHazard);
            }

            return null;
        }

        public MajorHazardViewModel UpdateMajorHazard(MajorHazardViewModel viewModel)
        {
            MajorHazard model = _mapper.Map<MajorHazard>(viewModel);
            _repositories.MajorHazardRepository.Update(model);
            _repositories.Save();
            return viewModel;
        }

        public void AddMajorHazard(EditMajorHazardViewModel viewModel) 
        {

            File image = _logicCore.MediaCore.AddImage("MajorHazardIcon", viewModel.Icon.FileName, viewModel.Icon.ContentType, viewModel.Icon.InputStream, false);

            BankOfIconItem bankOfIconItem = new BankOfIconItem
            {
                Id = Guid.NewGuid(),
                Name = "icon",
                Image = image 
            };

            _repositories.BankOfIconRepository.Add(bankOfIconItem);

            var majorHazard = new MajorHazard
            {
                Id = Guid.NewGuid(),
                Name = viewModel.Name,
                Icon = bankOfIconItem
                
            };
            _repositories.MajorHazardRepository.Add(majorHazard);
            _repositories.Save();
        }
    }
}
