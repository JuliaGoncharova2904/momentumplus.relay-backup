﻿using System;
using System.Collections.Generic;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Models;
using System.Linq;
using System.Data.Entity.Core;
using System.Web.Mvc;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class CriticalControlService : ICriticalControlService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public CriticalControlService(IRepositoriesUnitOfWork repositoriesUnitOfWork, IServicesUnitOfWork services, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            _logicCore = logicCore;
            _services = services;
        }

        public EditCriticalControlViewModel PopulateCriticalControlModel()
        {
            return this.PopulateCriticalControlModel(new EditCriticalControlViewModel());
        }

        public EditCriticalControlViewModel PopulateCriticalControlModel(EditCriticalControlViewModel model)
        {
            model.AllTeams = _services.TeamService.GetTeamList();
            return model;
        }

        public IEnumerable<SelectListItem> GetCriticalControlsList()
        {
            IQueryable<CriticalControl> criticalControls = _repositories.CriticalControlRepository.GetAll();
            return _mapper.Map<IEnumerable<SelectListItem>>(criticalControls);
        }

        public IEnumerable<SelectListItem> GetCriticalControlsListByMajorHazard(Guid majorHazardId)
        {
            MajorHazard majorHazard = _repositories.MajorHazardRepository.Get(majorHazardId);
            if(majorHazard != null)
            {
                return _mapper.Map<IEnumerable<SelectListItem>>(majorHazard.CriticalControls);
            }

            throw new ObjectNotFoundException(string.Format("MajorHazard with Id: {0} was not found.", majorHazardId));
        }

        public IEnumerable<CriticalControlViewModel> GetCriticalControlsByMajorHazard(Guid majorHazardId)
        {
            MajorHazard majorHazard = _repositories.MajorHazardRepository.Get(majorHazardId);
            if (majorHazard != null)
            {
                return _mapper.Map<IEnumerable<CriticalControlViewModel>>(majorHazard.CriticalControls);
            }

            throw new ObjectNotFoundException(string.Format("MajorHazard with Id: {0} was not found.", majorHazardId));
        }

        public EditCriticalControlViewModel GetCriticalControl(Guid criticalControlId)
        {
            CriticalControl criticalControl = _repositories.CriticalControlRepository.Get(criticalControlId);
            if(criticalControl != null)
            {
                EditCriticalControlViewModel model = new EditCriticalControlViewModel
                {
                    Id = criticalControl.Id,
                    Name = criticalControl.Name,
                    Teams = criticalControl.Teams.Select(t => t.Id).ToList()
                };

                return this.PopulateCriticalControlModel(model);
            }

            return null;
        }

        public void UpdateCriticalControl(EditCriticalControlViewModel model)
        {
            CriticalControl criticalControl = _repositories.CriticalControlRepository.Get(model.Id);
            if (criticalControl != null)
            {
                criticalControl.Name = model.Name;
                criticalControl.Teams.Clear();

                if (model.Teams != null && model.Teams.Count > 0)
                {
                    _repositories.TeamRepository
                        .Find(c => model.Teams.Contains(c.Id))
                        .ToList().ForEach(e =>
                        {
                            criticalControl.Teams.Add(e);
                        });
                }

                _repositories.CriticalControlRepository.Update(criticalControl);
                _repositories.Save();
            }
            else
            {
                throw new ObjectNotFoundException(string.Format("CriticalControl with Id: {0} was not found.", model.Id));
            }
        }

        public void AddCriticalControl(EditCriticalControlViewModel model)
        {
            MajorHazard majorHazard = _repositories.MajorHazardRepository.Get(model.MajorHazardId);
            if (majorHazard != null)
            {
                CriticalControl criticalControl = new CriticalControl
                {
                    Id = Guid.NewGuid(),
                    Name = model.Name,
                    MajorHazard = majorHazard,
                    Teams = new HashSet<Team>()
                };

                if (model.Teams != null && model.Teams.Count > 0)
                {
                    _repositories.TeamRepository
                        .Find(c => model.Teams.Contains(c.Id))
                        .ToList().ForEach(e =>
                        {
                            criticalControl.Teams.Add(e);
                        });
                }

                _repositories.CriticalControlRepository.Add(criticalControl);
                _repositories.Save();
            }
            else
            {
                throw new ObjectNotFoundException(string.Format("MajorHazard with Id: {0} was not found.", model.MajorHazardId));
            }
        }
    }
}
