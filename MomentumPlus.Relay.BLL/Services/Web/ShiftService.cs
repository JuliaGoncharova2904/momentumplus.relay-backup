﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class ShiftService : IShiftService
    {
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;

        public ShiftService(LogicCoreUnitOfWork logicCore)
        {
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
        }

        /// <summary>
        /// Populate ConfirmShiftViewModel
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns></returns>
        public ConfirmShiftViewModel PopulateConfirmShiftViewModel(Guid userId)
        {
            var user = _logicCore.UserProfileCore.GetUser(userId);

            if (!user.CurrentRotationId.HasValue)
                throw new NullReferenceException(string.Format("User with Id: {0} don't have rotation.", userId));

            var model = new ConfirmShiftViewModel
            {
                StartLimitDate = _logicCore.RotationCore.RotationLastShiftEndDate(user.CurrentRotation),
                EndLimitDate = _logicCore.RotationCore.RotationSwingEndDate(user.CurrentRotation).AddMinutes(-60),
                BeginningOfNextShiftLimitDate = _logicCore.RotationCore.RotationExpiredDate(user.CurrentRotation)
            };

            Shift nextShift = _logicCore.ShiftCore.GetOrCreateNextShift(user.CurrentRotation);

            if (nextShift.StartDateTime.HasValue)
            {
                model.StartShiftDate = nextShift.StartDateTime;
                model.EndShiftDate = nextShift.StartDateTime.Value.AddMinutes(nextShift.WorkMinutes);
                model.NextShiftDate = nextShift.StartDateTime.Value.AddMinutes(nextShift.WorkMinutes + nextShift.BreakMinutes);
                model.RepeatTimes = nextShift.RepeatTimes;
            }

            return model;
        }

        /// <summary>
        /// Populate ChooseShiftRecipientViewModel
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <returns></returns>
        public ChooseShiftRecipientModel PopulateChooseShiftRecipientViewModel(Guid shiftId)
        {
            Shift shift = _logicCore.ShiftCore.GetShift(shiftId);

            Rotation shiftRotation = shift.Rotation;

            UserProfile shiftOwner = shiftRotation.RotationOwner;

            var rotationShiftsRecipients = shiftRotation.RotationShifts.Where(s => s.ShiftRecipientId.HasValue)
                                                                .Select(s => s.ShiftRecipient).Where(user => user.Id != shiftOwner.Id)
                                                                .Where(user => user.CurrentRotationId.HasValue
                                                                    && user.CurrentRotation.RotationType == Core.Models.RotationType.Shift);

            var shiftOwnerTeamMembers = _logicCore.TeamCore.GetTeamMembers(shiftOwner.TeamId)
                                                                .Where(user => user.Id != shiftOwner.Id)
                                                                .Where(user => user.CurrentRotationId.HasValue 
                                                                                && user.CurrentRotation.RotationType == Core.Models.RotationType.Shift);

            var resultList = rotationShiftsRecipients.Concat(shiftOwnerTeamMembers)
                                                     .OrderBy(user => user.FirstName)
                                                     .GroupBy(x => x.Id)
                                                     .OrderByDescending(group => group.Count())
                                                     .Select(g => g.First());


            ChooseShiftRecipientModel model = new ChooseShiftRecipientModel
            {
                ShiftId = shiftId,
                ShiftRecipientId = shift.ShiftRecipientId,
                TeamMembers = _mapper.Map<IEnumerable<DropDownListItemModel>>(resultList)
            };

            return model;
        }

        /// <summary>
        /// Is shift working time ended
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <returns></returns>
        public bool IsShiftWorkTimeEnd(Guid shiftId)
        {
            Shift shift = _logicCore.ShiftCore.GetShift(shiftId);

            return shift.State == ShiftState.Break;
        }

        /// <summary>
        /// Is shift ended
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <returns></returns>
        public bool IsShiftEnd(Guid shiftId)
        {
            Shift shift = _logicCore.ShiftCore.GetShift(shiftId);

            return shift.State == ShiftState.Finished;
        }

        /// <summary>
        /// Return current shift Id for rotation or NULL if current shift is nit exist.
        /// </summary>
        /// <param name="rotationId">Rotation Id</param>
        /// <returns></returns>
        public Guid? GetCurrentShiftIdForRotation(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);
            Shift shift = _logicCore.ShiftCore.GetCurrentRotationShift(rotation);

            return (shift != null) ? (Guid?)shift.Id : null;
        }

        /// <summary>
        /// Populate EditShiftDatesViewModel model from Shift entity.
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <returns></returns>
        public EditShiftDatesViewModel GetShiftDates(Guid shiftId)
        {
            Shift shift = _logicCore.ShiftCore.GetShift(shiftId);
            Rotation rotation = shift.Rotation;

            EditShiftDatesViewModel model = new EditShiftDatesViewModel
            {
                ShiftId = shift.Id,
                StartShiftDate = shift.StartDateTime.Value,
                EndShiftDate = shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes),
                NextShiftDate = shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes + shift.BreakMinutes),
                BeginningOfNextShiftLimitDate = _logicCore.RotationCore.RotationExpiredDate(rotation),
                CurrentDate = DateTime.Now,
                EndLimitDate = _logicCore.RotationCore.RotationSwingEndDate(rotation).AddMinutes(-60),
                RepeatTimes = shift.RepeatTimes,
                EndShiftDateReadOnly = shift.State == ShiftState.Break || shift.State == ShiftState.Finished,
                NextShiftDateReadOnly = shift.State == ShiftState.Finished
            };

            return model;
        }

        public void UpdateRotationDates(EditShiftDatesViewModel model)
        {
            _logicCore.ShiftCore.UpdateShiftDates(model.ShiftId, model.StartShiftDate.Value, model.EndShiftDate.Value,
                                                            model.NextShiftDate.Value, model.RepeatTimes);
        }

        /// <summary>
        /// Rotation Has Working Shift
        /// </summary>
        /// <param name="rotationId">Rotation Id</param>
        /// <returns></returns>
        public bool RotationHasWorkingShift(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            return _logicCore.ShiftCore.RotationHasWorkingShift(rotation);
        }

        /// <summary>
        /// Shift has recipient
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <returns></returns>
        public bool ShiftHasRecipient(Guid shiftId)
        {
            Shift shift = _logicCore.ShiftCore.GetShift(shiftId);

            return shift.ShiftRecipientId.HasValue;
        }

        /// <summary>
        /// Return rotation Id for shift.
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <returns></returns>
        public Guid ShiftRotationId(Guid shiftId)
        {
            Shift shift = _logicCore.ShiftCore.GetShift(shiftId);

            return shift.RotationId;
        }


        public void UpdateShiftRecipient(ChooseShiftRecipientModel model)
        {
            if (model.ShiftRecipientId.HasValue)
            {
                _logicCore.ShiftCore.UpdateShiftRecipient(model.ShiftId, model.ShiftRecipientId.Value);
            }
        }

        public void ConfirmShift(Guid userId, ConfirmShiftViewModel model)
        {
            _logicCore.ShiftCore.ConfirmShift(userId, model.StartShiftDate.Value, model.EndShiftDate.Value,
                                                    model.NextShiftDate.Value, model.RepeatTimes);
        }


        /// <summary>
        /// Return period of Shift by Shift Id.
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <returns></returns>
        public string GetShiftPeriod(Guid? shiftId)
        {
            string period = "...";

            if (shiftId.HasValue)
            {
                Shift shift = _logicCore.ShiftCore.GetShift(shiftId.Value);

                if (shift.StartDateTime.HasValue)
                {
                    period = shift.StartDateTime.Value.FormatWithDayMonthYear();
                }
            }

            return period;
        }

        /// <summary>
        /// Return Handover Recipient Full Name of Shift by Shift Id.
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <returns></returns>
        public string GetShiftHandoverRecipientName(Guid? shiftId)
        {
            string handoverRecipientName = String.Empty;

            if (shiftId.HasValue)
            {
                Shift shift = _logicCore.ShiftCore.GetShift(shiftId.Value);

                if (shift.ShiftRecipientId.HasValue)
                {
                    handoverRecipientName = shift.ShiftRecipient.FullName;
                }
            }

            return handoverRecipientName;
        }


        /// <summary>
        /// Return Full Name of Shift Owner by Shift Id.
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <returns></returns>
        public string GetShiftOwnerName(Guid shiftId)
        {
            Shift shift = _logicCore.ShiftCore.GetShift(shiftId);
            var ownerName = shift.Rotation.RotationOwner.FullName;

            return ownerName;
        }

        /// <summary>
        /// End Shift
        /// </summary>
        /// <param name="shiftId"></param>
        /// <returns></returns>
        public void ExpireShift(Guid shiftId)
        {
            Shift shift = _logicCore.ShiftCore.GetShift(shiftId);

            _logicCore.ShiftCore.EndShift(shift);
        }

        public List<ShiftReceivedSlideViewModel> PopulateReceivedShiftsPanel(Guid shiftId)
        {
            var handoverFromShifts = _logicCore.ShiftCore.GetShift(shiftId).HandoverFromShifts.ToList().OrderByDescending(shift => shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes));
            List<ShiftReceivedSlideViewModel> model = new List<ShiftReceivedSlideViewModel>();

            foreach (var shift in handoverFromShifts)
            {
                List<ShiftContributorForReceivedPanel> contributors = new List<ShiftContributorForReceivedPanel>();
                foreach (var contributor in shift.Rotation.Contributors.ToList())
                {
                    contributors.Add(new ShiftContributorForReceivedPanel
                    {
                        ContributorID = contributor.Id,
                        ContributorName = contributor.FullName
                    });
                }

                ShiftReceivedSlideViewModel slide = new ShiftReceivedSlideViewModel
                {
                    Id = shift.Id,
                    HandoverFromID = shift.Rotation.RotationOwnerId,
                    HandoverFromName = shift.Rotation.RotationOwner.FullName,
                    TopicCounter = _logicCore.RotationTopicCore.GetAllShiftTopic(shiftId, TypeOfModuleSource.Received)
                                                    .Count(t => t.Enabled && t.AncestorTopic.RotationTopicGroup.RotationModule.ShiftId == shift.Id && !t.DeletedUtc.HasValue),
                    TaskCounter = _logicCore.RotationTaskCore.GetAllShiftTasks(shiftId, TypeOfModuleSource.Received)
                                                     .Count(t => t.Enabled && t.AncestorTask.RotationTopic.RotationTopicGroup.RotationModule.ShiftId == shift.Id && !t.DeletedUtc.HasValue),
                    Date = shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes).FormatWithDayMonthYear(),
                    Contributors = contributors
                };

                model.Add(slide);
            }

            return model;
        }

        /// <summary>
        /// Change Finalize Status For Draft Topics And Tasks
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <param name="status">Finalize status</param>
        public void SetFinalizeStatusForDraftItems(Guid shiftId, FinalizeStatus status)
        {
            _logicCore.ShiftCore.SetFinalizeStatusForDraftItems(shiftId, (StatusOfFinalize)status);
        }



        /// <summary>
        /// Return Period of received rotation by Shift Id
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <param name="sourceShiftId">Selected Source Shift Id</param>
        /// <returns></returns>
        public string GetReceivedShiftPeriod(Guid? shiftId, Guid? sourceShiftId)
        {
            string period = null;

            if (sourceShiftId.HasValue)
            {
                Shift shift = _logicCore.ShiftCore.GetShift(sourceShiftId.Value);

                if (shift.StartDateTime != null)
                    period = shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes).FormatDayMonthYear();
            }
            else if (shiftId.HasValue)
            {
                Shift shift = _logicCore.ShiftCore.GetShift(shiftId.Value);

                if (shift.HandoverFromShifts.Count() == 1)
                {
                    Shift receivedShift = shift.HandoverFromShifts.First();

                    if (receivedShift.StartDateTime != null)
                        period = receivedShift.StartDateTime.Value.AddMinutes(receivedShift.WorkMinutes + receivedShift.BreakMinutes).FormatDayMonthYear();
                }
            }

            return period;
        }

    }
}
