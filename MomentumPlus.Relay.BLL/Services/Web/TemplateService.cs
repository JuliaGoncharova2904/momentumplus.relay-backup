﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class TemplateService : ITemplateService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly IServicesUnitOfWork _services;
        private readonly LogicCoreUnitOfWork _logicCore;

        public TemplateService(IRepositoriesUnitOfWork repositoriesUnitOfWork, IServicesUnitOfWork services, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._services = services;
            this._logicCore = logicCore;
        }

        public TemplateViewModel GetTemplateViewModel(Guid templateId)
        {
            var model = _repositories.TemplateRepository.Get(templateId);

            return _mapper.Map<TemplateViewModel>(model);
        }

  

        public IEnumerable<SelectListItem> GetTemplateList()
        {
            return _mapper.Map<IEnumerable<SelectListItem>>(_repositories.TemplateRepository.Find(p => !p.DeletedUtc.HasValue).OrderBy(t => t.Name));
        }

        public TemplatesViewModel PopulateTemplatesViewModel(
            string selectedTemplate,
            ModuleType? selectedModule,
            int? topicGroupsPageNo,
            int? topicsPageNo,
            int? tasksPageNo,
            Guid? topic = null,
            Guid? topicGroup = null)
        {
            var model = new TemplatesViewModel
            {
                Templates = GetTemplateList(),
                SelectedTemplate = selectedTemplate,
                Module = null,
                Template = null
            };

            if (!string.IsNullOrWhiteSpace(selectedTemplate))
            {
                model.Template = GetTemplateViewModel(new Guid(selectedTemplate));
                model.Template.Modules = _services.ModuleService.GetBaseModulesViewModels();

                foreach (var module in model.Template.Modules)
                {
                    module.Enabled = _logicCore.ModuleCore.CheckChildModuleStatus(module.Id,
                        new Guid(selectedTemplate));

                    module.EnabledTopicGroupsCounter = _logicCore.ModuleCore.EnabledTopicsGroupsForModuleCounter(module.Id, new Guid(selectedTemplate));

                }
            }

            if (selectedModule.HasValue && !string.IsNullOrWhiteSpace(selectedTemplate))
            {
                model.Module = _services.ModuleService.PopulateTemplateModuleViewModel(new Guid(selectedTemplate), (ModuleType)selectedModule, topicGroup, topic, topicGroupsPageNo, topicsPageNo, tasksPageNo);
                model.SelectedModule = selectedModule;
            }

            return model;
        }


        public void AddTemplate(TemplateViewModel model)
        {
            var template = _mapper.Map<Template>(model);
            if (!model.Id.HasValue)
            {
                template.Id = Guid.NewGuid();
            }

            _repositories.TemplateRepository.Add(template);
            _repositories.Save();
        }

        public void UpdateTemplate(TemplateViewModel model)
        {
            var template = _mapper.Map<Template>(model);

            _repositories.TemplateRepository.Update(template);
            _repositories.Save();
        }

        public bool TemplateExist(TemplateViewModel model)
        {
            return _repositories.TemplateRepository.Find(t => t.Id != model.Id && t.Name.Equals(model.Name)).Any();
        }
    }
}
