﻿using MomentumPlus.Relay.Interfaces.Services;
using System;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.Mapper;
using AutoMapper;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class LocationService : ILocationService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;

        public LocationService(IRepositoriesUnitOfWork repositoriesUnitOfWork)
        {
            this._repositories = repositoriesUnitOfWork;
            this._mapper = WebAutoMapperConfig.GetMapper();
        }

        public LocationViewModel GetLocationForTopic(Guid topicId)
        {
            RotationTopic topic = _repositories.RotationTopicRepository.Get(topicId);
            if(topic != null)
            {
                Location location = topic.Location;
                if(location != null)
                {
                    return _mapper.Map<LocationViewModel>(location);
                }
            }

            return null;
        }

        public bool CreateLocationForTopic(Guid topicId, LocationViewModel model)
        {
            RotationTopic topic = _repositories.RotationTopicRepository.Get(topicId);
            if (topic != null)
            {
                Location location = new Location();
                location.Id = Guid.NewGuid();

                _mapper.Map(model, location);
                topic.Location = location;
                _repositories.LocationRepository.Add(location);
                _repositories.Save();

                return true;
            }

            return false;
        }

        public bool UpdateLocation(LocationViewModel model)
        {
            if(model.Id.HasValue)
            {
                Location location = _repositories.LocationRepository.Get(model.Id.Value);
                if(location != null)
                {
                    _mapper.Map(model, location);
                    _repositories.LocationRepository.Update(location);
                    _repositories.Save();

                    return true;
                }
            }

            return false;
        }
    }
}
