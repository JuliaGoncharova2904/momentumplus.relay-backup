﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Exceptions;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.LogicCore.AccessValidation;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MvcPaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class TaskBoardService : ITaskBoardService
    {
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;

        public TaskBoardService(LogicCoreUnitOfWork logicCore)
        {
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
        }

        #region My tasks tabs

        /// <summary>
        /// Return tasks view models for Received tasks.
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <param name="page">Page number</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="sortStrategy">Sort strategy</param>
        /// <param name="filter">Filter option</param>
        /// <returns></returns>
        public IPagedList<TaskBoardTaskTopicViewModel> GetReceivedTasks(Guid userId, int page, int pageSize, TaskBoardSortOptions sortStrategy, TaskBoardFilterOptions filter)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(userId);
            List<TaskBoardTaskTopicViewModel> model = new List<TaskBoardTaskTopicViewModel>();

            IQueryable<RotationTask> workItemTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromActiveWorkItems(user, TaskBoardTaskType.Received);
            IQueryable<RotationTask> taskBoardTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(user, TaskBoardTaskType.Received);
            IQueryable<RotationTask> tasks = workItemTasks != null ? workItemTasks.Union(taskBoardTasks) : taskBoardTasks;

            tasks = TaskBoardSort.ApplySortOption(TaskBoardFilter.ApplyFilter(tasks, filter), sortStrategy);

            int taskCounter = tasks.Count();

            if (pageSize > 16 && filter == TaskBoardFilterOptions.AllTasks)
            {
                pageSize = taskCounter;
            }

            tasks = tasks.Skip(page * pageSize).Take(pageSize);

            foreach (RotationTask task in tasks.ToList())
            {
                UserProfile taskOwner = _logicCore.RotationTaskCore.GetOwnerForTask(task);

                model.Add(new TaskBoardTaskTopicViewModel
                {
                    Id = task.Id,
                    ReadOnly = task.IsComplete,
                    IsTaskBoardTask = task.TaskBoardId.HasValue,
                    IsFeedbackRequired = task.IsFeedbackRequired,
                    Reference = task.RotationTopicId.HasValue ? task.RotationTopic.Name : string.Empty,
                    Name = task.Name,
                    CompleteStatus = task.IsComplete ? "Complete" : "Incomplete",
                    Priority = (TaskPriority)task.Priority,
                    DueDate = task.Deadline.FormatWithMonth(),
                    TaskOwnerId = taskOwner != null ? taskOwner.Id : Guid.Empty,
                    IsInArchive = task.IsInArchive,
                    HasAttachments = task.Attachments.Any(),
                    HasVoiceMessages = task.VoiceMessages.Any(),
                    TaskOwnerName = taskOwner != null ? taskOwner.FullName : ""
                });
            }

            return model.ToPagedList(page, pageSize, taskCounter);
        }

        /// <summary>
        /// Return tasks view models for Assigned tasks.
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <param name="page">Page number</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="sortStrategy">Sort strategy</param>
        /// <param name="filter">Filter option</param>
        /// <returns></returns>
        public IPagedList<TaskBoardTaskTopicViewModel> GetAssignedTasks(Guid userId, int page, int pageSize, TaskBoardSortOptions sortStrategy, TaskBoardFilterOptions filter)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(userId);
            List<TaskBoardTaskTopicViewModel> model = new List<TaskBoardTaskTopicViewModel>();

            IQueryable<RotationTask> workItemTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromActiveWorkItems(user, TaskBoardTaskType.Draft);
            IQueryable<RotationTask> taskBoardDraftTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(user, TaskBoardTaskType.Draft);
            IQueryable<RotationTask> taskBoardPendingTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(user, TaskBoardTaskType.Pending);

            IQueryable<RotationTask> tasks = (workItemTasks != null ? workItemTasks.Union(taskBoardDraftTasks) : taskBoardDraftTasks).Union(taskBoardPendingTasks);

            
            tasks = TaskBoardSort.ApplySortOption(TaskBoardFilter.ApplyFilter(tasks, filter), sortStrategy);

            int taskCounter = tasks.Count();

            if (pageSize > 16 && filter == TaskBoardFilterOptions.AllTasks)
            {
                pageSize = taskCounter;
            }

            tasks = tasks.Skip(page * pageSize).Take(pageSize);
            
            foreach (RotationTask task in tasks.ToList())
            {
                model.Add(new TaskBoardTaskTopicViewModel
                {
                    Id = task.Id,
                    IsTaskBoardTask = task.IsTaskBoardTask(),
                    ReadOnly = !task.IsEditable(),
                    IsCustom = !task.IsTemplateTask(),
                    IsFeedbackRequired = task.IsFeedbackRequired,
                    Reference = task.RotationTopicId.HasValue ? task.RotationTopic.Name : string.Empty,
                    Name = task.Name,
                    CompleteStatus = task.TaskBoardTaskType == TaskBoardTaskType.Pending
                                ? "Pending"
                                : task.SuccessorTaskId != null
                                ? task.SuccessorTask.IsComplete
                                ? "Complete"
                                : "Incomplete"
                                : "Incomplete",
                    Priority = (TaskPriority)task.Priority,
                    DueDate = task.Deadline.FormatWithMonth(),
                    AssigneeId = task.AssignedToId.Value,
                    AssigneeName = task.AssignedTo.FullName,
                    IsInArchive = task.IsInArchive,
                    HasAttachments = task.Attachments.Any(),
                    HasVoiceMessages = task.VoiceMessages.Any(),
                    CarryforwardCounter = task.ForkParentTaskId.HasValue ? task.ForkParentTask.ForkCounter : task.ForkCounter
                });
            }

            return model.ToPagedList(page, pageSize, taskCounter);
        }

        /// <summary>
        /// Return tasks view models for My Tasks.
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <param name="page">Page number</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="sortStrategy">Sort strategy</param>
        /// <param name="filter">Filter option</param>
        /// <returns></returns>
        public IPagedList<TaskBoardTaskTopicViewModel> GetMyTasks(Guid userId, int page, int pageSize, TaskBoardSortOptions sortStrategy, TaskBoardFilterOptions filter)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(userId);
            List<TaskBoardTaskTopicViewModel> model = new List<TaskBoardTaskTopicViewModel>();

            IQueryable<RotationTask> tasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(user, TaskBoardTaskType.MyTask);

            tasks = TaskBoardSort.ApplySortOption(TaskBoardFilter.ApplyFilter(tasks, filter), sortStrategy);

            int taskCounter = tasks.Count();

            if (pageSize > 16 && filter == TaskBoardFilterOptions.AllTasks)
            {
                pageSize = taskCounter;
            }

            tasks = tasks.Skip(page * pageSize).Take(pageSize);

            foreach (RotationTask task in tasks.ToList())
            {
                model.Add(new TaskBoardTaskTopicViewModel
                {
                    Id = task.Id,
                    ReadOnly = !task.IsEditable(),
                    Name = task.Name,
                    CompleteStatus = task.IsComplete ? "Complete" : "Incomplete",
                    Priority = (TaskPriority)task.Priority,
                    DueDate = task.Deadline.FormatWithMonth(),
                    IsInArchive = task.IsInArchive,
                    HasAttachments = task.Attachments.Any(),
                    HasVoiceMessages = task.VoiceMessages.Any()
                });
            }

            return model.ToPagedList(page, pageSize, taskCounter);
        }


        #endregion

        #region Get/Update task topic

        /// <summary>
        /// Get task topic
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public TaskBoardTaskTopicViewModel GetTaskTopic(Guid taskId)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);

            TaskBoardTaskTopicViewModel taskTopic = new TaskBoardTaskTopicViewModel
            {
                Id = task.Id,
                Name = task.Name,
                Reference = task.RotationTopicId.HasValue ? task.RotationTopic.Name : string.Empty,
                IsFeedbackRequired = task.IsFeedbackRequired,
                CarryforwardCounter = task.ForkParentTaskId.HasValue ? task.ForkParentTask.ForkCounter : task.ForkCounter,
                CompleteStatus = task.TaskBoardTaskType == TaskBoardTaskType.Pending
                                ? "Pending"
                                : task.IsComplete
                                ? "Complete"
                                : task.SuccessorTaskId != null
                                ? task.SuccessorTask.IsComplete
                                ? "Complete"
                                : "Incomplete"
                                : "Incomplete",
                Priority = (TaskPriority)task.Priority,
                DueDate = task.Deadline.FormatWithMonth(),
                AssigneeId = task.AssignedToId.Value,
                IsInArchive = task.IsInArchive,
                HasAttachments = task.Attachments.Any(),
                HasVoiceMessages = task.VoiceMessages.Any()
            };

            return taskTopic;
        }

        /// <summary>
        /// Update task topic
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public TaskBoardTaskTopicViewModel UpdateTaskTopic(TaskBoardTaskTopicViewModel model)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(model.Id);

            if (task.IsComplete || (task.SuccessorTaskId != null && task.SuccessorTask.IsComplete))
            {
                task.IsInArchive = model.IsInArchive;
            }
            else
            {
                task.IsFeedbackRequired = model.IsFeedbackRequired;
            }

            _logicCore.RotationTaskCore.Update(task);

            model.IsInArchive = task.IsInArchive;
            model.IsFeedbackRequired = task.IsFeedbackRequired;

            return model;
        }

        #endregion

        #region Add task dialog

        /// <summary>
        /// Populate model for task board dialogs
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <returns></returns>
        public TaskBoardAddTaskDialogViewModel PopulateAddTaskModel(Guid userId)
        {
            TaskBoardAddTaskDialogViewModel model = new TaskBoardAddTaskDialogViewModel
            {
                OwnerId = userId,
                StartLimitDate = DateTime.Now,
                SendingOption = TaskSendingOptions.Now
            };

            return model;
        }

        /// <summary>
        /// Add new task to TaskBoard
        /// </summary>
        /// <param name="task">Task view model</param>
        public void AddTask(TaskBoardAddTaskDialogViewModel task)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(task.OwnerId);
            RotationTask taskEntity = new RotationTask
            {
                Name = task.Name,
                Description = task.Notes,
                AssignedToId = task.AssignedTo.Value,
                Deadline = task.Deadline.Value,
                Priority = (PriorityOfTask)task.Priority,
                SearchTags = task.Tags != null ? string.Join(",", task.Tags) : string.Empty,
                TaskBoardId = task.OwnerId,
                TaskBoard = user.TaskBoard
            };

            if (task.OwnerId == task.AssignedTo.Value)
            {
                taskEntity.TaskBoardTaskType = TaskBoardTaskType.MyTask;
                _logicCore.TaskBoardCore.AddTask(taskEntity);
            }
            else
            {
                switch (task.SendingOption)
                {
                    case TaskSendingOptions.Now:
                        taskEntity.TaskBoardTaskType = TaskBoardTaskType.Draft;
                        _logicCore.TaskBoardCore.AddTask(taskEntity);
                        _logicCore.TaskBoardCore.HandoverTask(taskEntity);
                        break;
                    case TaskSendingOptions.Pending:
                        taskEntity.TaskBoardTaskType = TaskBoardTaskType.Pending;
                        taskEntity.DeferredHandoverTime = task.SendDate;
                        _logicCore.TaskBoardCore.AddTask(taskEntity);
                        break;
                    default:
                        throw new Exception("Bad sanding option of task.");
                }
            }

            _logicCore.RotationTaskLogCore.AddMessageToTaskLog(taskEntity, $"<strong>{user.FullName}</strong> created the task.");

        }

        #endregion

        #region Edit task dialog

        /// <summary>
        /// Get task for task board edit task dialog
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public TaskBoardEditTaskDialogViewModel PopulateEditTaskModel(Guid userId, Guid taskId)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(userId);
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);
            IQueryable<RotationTopic> topics = _logicCore.TaskBoardCore.GetTopicsFromActiveWorkItem(user);

            TaskBoardEditTaskDialogViewModel model = new TaskBoardEditTaskDialogViewModel
            {
                ID = task.Id,
                OwnerId = userId,
                Name = task.Name,
                Notes = task.Description,
                Deadline = task.Deadline,
                SendDate = task.DeferredHandoverTime,
                StartLimitDate = DateTime.Now,
                AssignedTo = task.AssignedToId,
                Priority = (TaskPriority)task.Priority,
                SelectedTopicId = task.RotationTopicId,
                Tags = string.IsNullOrEmpty(task.SearchTags) ? null : task.SearchTags.Split(','),
                TopicsForAssignee = topics?.ToList().Select(t => new SelectListItem { Text = t.Name, Value = t.Id.ToString() }) ?? new SelectListItem[0],
                CanChangeAssignee = task.CanChangeAssignee(),
                IsProjectTask = task.ProjectId.HasValue,
                IsFeedbackRequired = task.IsFeedbackRequired,
                AttacmentsCounter = task.Attachments.Count,
                VoiceMessagesCounter = task.VoiceMessages.Count,
                ItemDetailsTab = task.RotationTopicId.HasValue
                    ? new TaskBoardCompleteItemTabViewModel
                    {
                        IsFeedbackRequired = task.RotationTopic.IsFeedbackRequired,
                        VoiceMessagesCounter = task.RotationTopic.VoiceMessages.Count,
                        AttacmentsCounter = task.RotationTopic.Attachments.Count,
                        Notes = task.RotationTopic.Description,
                        ItemName = task.RotationTopic.Name,
                        Tags = string.IsNullOrEmpty(task.RotationTopic.SearchTags) ? null : task.RotationTopic.SearchTags.Split(','),
                        TasksCounter = task.RotationTopic.RotationTasks.Count,
                        ManagerComments = task.RotationTopic.ManagerComments,
                        Section = task.RotationTopic.RotationTopicGroup.RotationModule.Type.GetEnumDescription()
                    }
                    : null,
                ItemId = task.RotationTopicId.HasValue ? task.RotationTopicId.Value : Guid.Empty
            };

            switch (task.TaskBoardTaskType)
            {
                case TaskBoardTaskType.NotSet:
                case TaskBoardTaskType.Draft:
                case TaskBoardTaskType.MyTask:
                    model.SendingOption = TaskSendingOptions.Now;
                    break;
                case TaskBoardTaskType.Pending:
                    model.SendingOption = TaskSendingOptions.Pending;
                    break;
                case TaskBoardTaskType.Received:
                    model.SendingOption = TaskSendingOptions.Now;
                    break;
                default:
                    throw new Exception("Not editable task type.");
            }

            return model;
        }

        /// <summary>
        /// Populate Edit task view model.
        /// </summary>
        /// <param name="model">Edit task view model</param>
        /// <returns></returns>
        public TaskBoardEditTaskDialogViewModel PopulateEditTaskModel(TaskBoardEditTaskDialogViewModel model)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(model.OwnerId);
            IQueryable<RotationTopic> topics = _logicCore.TaskBoardCore.GetTopicsFromActiveWorkItem(user);

            model.TopicsForAssignee = topics?.ToList().Select(t => new SelectListItem { Text = t.Name, Value = t.Id.ToString() }) ?? new SelectListItem[0];

            return model;
        }

        /// <summary>
        /// Get task for task board edit task dialog
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public TaskBoardEditTaskDialogViewModel PopulateReceivedEditTaskModel(Guid userId, Guid taskId)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(userId);
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);
            IQueryable<RotationTopic> topics = _logicCore.TaskBoardCore.GetTopicsFromActiveWorkItem(user, TypeOfModuleSource.Received);

            TaskBoardEditTaskDialogViewModel model = new TaskBoardEditTaskDialogViewModel
            {
                ID = task.Id,
                OwnerId = userId,
                Name = task.Name,
                Notes = task.Description,
                Deadline = task.Deadline,
                SendDate = task.DeferredHandoverTime,
                SendingOption = TaskSendingOptions.Now,
                StartLimitDate = DateTime.Now,
                AssignedTo = task.AssignedToId,
                Priority = (TaskPriority)task.Priority,
                SelectedTopicId = task.RotationTopicId,
                Tags = string.IsNullOrEmpty(task.SearchTags) ? null : task.SearchTags.Split(','),
                TopicsForAssignee = topics?.ToList().Select(t => new SelectListItem { Text = t.Name, Value = t.Id.ToString() }) ?? new SelectListItem[0],
                CanChangeAssignee = task.CanChangeAssignee(),
                IsFeedbackRequired = task.IsFeedbackRequired,
                AttacmentsCounter = task.Attachments.Count,
                VoiceMessagesCounter = task.VoiceMessages.Count,
                ItemDetailsTab = task.RotationTopicId.HasValue
                                    ? new TaskBoardCompleteItemTabViewModel
                                    {
                                        IsFeedbackRequired = task.RotationTopic.IsFeedbackRequired,
                                        VoiceMessagesCounter = task.RotationTopic.VoiceMessages.Count,
                                        AttacmentsCounter = task.RotationTopic.Attachments.Count,
                                        Notes = task.RotationTopic.Description,
                                        ItemName = task.RotationTopic.Name,
                                        Tags = string.IsNullOrEmpty(task.RotationTopic.SearchTags) ? null : task.RotationTopic.SearchTags.Split(','),
                                        TasksCounter = task.RotationTopic.RotationTasks.Count,
                                        ManagerComments = task.RotationTopic.ManagerComments,
                                        Section = task.RotationTopic.RotationTopicGroup.RotationModule.Type.GetEnumDescription()
                                    }
                                    : null,
                ItemId = task.RotationTopicId.HasValue ? task.RotationTopicId.Value : Guid.Empty
            };

            return model;
        }


        public TaskBoardEditTaskDialogViewModel PopulateReceivedEditTaskModel(TaskBoardEditTaskDialogViewModel model)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(model.OwnerId);
            IQueryable<RotationTopic> topics = _logicCore.TaskBoardCore.GetTopicsFromActiveWorkItem(user, TypeOfModuleSource.Received);

            model.TopicsForAssignee = topics?.ToList().Select(t => new SelectListItem { Text = t.Name, Value = t.Id.ToString() }) ?? new SelectListItem[0];

            return model;
        }


        public void EditReceivedTask(Guid currentUserId, TaskBoardEditTaskDialogViewModel model)
        {
            _logicCore.RotationTaskCore.UpdateReceivedTask(currentUserId, model.ID, model.AssignedTo, model.Deadline, model.Priority);
        }




        /// <summary>
        /// Update task.
        /// </summary>
        /// <param name="model">Edit task view model</param>
        public void UpdateTask(TaskBoardEditTaskDialogViewModel model)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(model.OwnerId);
            RotationTask task = _logicCore.RotationTaskCore.GetTask(model.ID);

            if (!user.IsOwnerOfTask(task))
            {
                throw new AccessDeniedException($"User with ID: {user.Id} is not owner of task with ID: {task.Id}");
            }

            if (!task.IsEditable())
            {
                throw new Exception("Task can not be edited.");
            }

            task.Name = model.Name;
            task.Description = model.Notes;
            task.Deadline = model.Deadline.Value;
            task.Priority = (PriorityOfTask)model.Priority;
            task.SearchTags = model.Tags != null && model.Tags.Any() ? string.Join(",", model.Tags) : string.Empty;

            _logicCore.TaskBoardCore.UpdateTask(task, user, model.AssignedTo.Value, model.SelectedTopicId, model.SendDate);
        }

        #endregion

        #region Complete task dialog

        /// <summary>
        /// Get task for complete checklist task dialog 
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public TaskBoardCompleteTaskDialogViewModel PopulateCompleteChecklistTaskViewModel(Guid taskId)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);
            UserProfile user = _logicCore.RotationTaskCore.GetOwnerForTask(task);

            TaskBoardCompleteTaskDialogViewModel model = new TaskBoardCompleteTaskDialogViewModel
            {
                TaskDetailTab = new TaskBoardCompleteTaskDetailTabViewModel
                {
                    Name = task.Name,
                    Deadline = task.Deadline,
                    TaskNotes = task.Description,
                    Tags = string.IsNullOrEmpty(task.SearchTags) ? null : task.SearchTags.Split(','),
                    AssignedFrom = user.Id,
                    Priority = (TaskPriority)task.Priority,
                    CanChangeAssignee = task.CanChangeAssignee(),
                    IsFeedbackRequired = task.IsFeedbackRequired,
                    VoiceMessagesCounter = task.VoiceMessages.Count,
                    AttacmentsCounter = task.Attachments.Count,
                    ItemId = task.RotationTopicId.HasValue ? task.RotationTopicId.Value : Guid.Empty
                },
                TaskOutcomesTab = new TaskBoardCompleteOutcomesTabViewModel
                {
                    ID = task.Id,
                    Feedback = task.Feedback,
                    IsFeedbackRequired = task.IsFeedbackRequired,
                    VoiceMessagesCounter = task.VoiceMessages.Count,
                    AttacmentsCounter = task.Attachments.Count
                },
                HasTopic = task.RotationTopicId.HasValue,
                SelectedTabNumber = (task.IsFeedbackRequired) ? 2 : 0
            };

            if (task.ProjectId.HasValue)
            {
                model.ProjectDetailsTab = new ProjectAdminProjectDetailsTabViewModel
                {
                    ProjectId = task.Project.Id,
                    ProjectDescription = task.Project.Description,
                    ProjectName = task.Project.Name
                };
            }
            return model;
        }

        /// <summary>
        /// Popoulate task board complete task dialog with topic details tab
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public TaskBoardCompleteHandbackTaskDialogViewModel PopulateCompleteHandbackTaskViewModel(Guid taskId)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);

            TaskBoardCompleteHandbackTaskDialogViewModel model = new TaskBoardCompleteHandbackTaskDialogViewModel
            {
                TopicDetailTab = new TaskBoardCompleteItemTabViewModel
                {
                    ItemName = task.RotationTopic.Name,
                    Tags = string.IsNullOrEmpty(task.RotationTopic.SearchTags) ? null : task.RotationTopic.SearchTags.Split(','),
                    TasksCounter = task.RotationTopic.RotationTasks.Count,
                    ManagerComments = task.RotationTopic.ManagerComments,
                    AttacmentsCounter = task.RotationTopic.Attachments.Count,
                    VoiceMessagesCounter = task.RotationTopic.VoiceMessages.Count,
                    IsFeedbackRequired = task.RotationTopic.IsFeedbackRequired,
                    Section = task.RotationTopic.RotationTopicGroup.RotationModule.Type.GetEnumDescription(),
                    ItemId = task.RotationTopicId.Value
                },

                TaskDetailTab = new TaskBoardCompleteTaskDetailTabViewModel
                {
                    Name = task.Name,
                    Deadline = task.Deadline,
                    TaskNotes = task.Description,
                    Tags = string.IsNullOrEmpty(task.SearchTags) ? null : task.SearchTags.Split(','),
                    Priority = (TaskPriority)task.Priority,
                    AssignedFrom = _logicCore.RotationTaskCore.GetOwnerForTask(task).Id,
                    CanChangeAssignee = task.CanChangeAssignee(),
                    AttacmentsCounter = task.Attachments.Count,
                    VoiceMessagesCounter = task.VoiceMessages.Count,
                    IsFeedbackRequired = task.IsFeedbackRequired
                },

                TaskOutcomesTab = new TaskBoardCompleteOutcomesTabViewModel
                {
                    ID = task.Id,
                    Feedback = task.Feedback,
                    IsFeedbackRequired = task.IsFeedbackRequired,
                    VoiceMessagesCounter = task.VoiceMessages.Count,
                    AttacmentsCounter = task.Attachments.Count
                },

                SelectedTabNumber = (task.IsFeedbackRequired) ? 2 : 1
            };

            return model;
        }

        public TaskBoardCompleteItemTabViewModel PopulateTaskBoardTopicDetailsDialogViewModel(Guid taskId)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);
            TaskBoardCompleteItemTabViewModel model = task.RotationTopicId.HasValue
                                ? new TaskBoardCompleteItemTabViewModel
                                {
                                    IsFeedbackRequired = task.RotationTopic.IsFeedbackRequired,
                                    VoiceMessagesCounter = task.RotationTopic.VoiceMessages.Count,
                                    AttacmentsCounter = task.RotationTopic.Attachments.Count,
                                    Notes = task.RotationTopic.Description,
                                    ItemName = task.RotationTopic.Name,
                                    Tags = string.IsNullOrEmpty(task.RotationTopic.SearchTags)
                                                ? null
                                                : task.RotationTopic.SearchTags.Split(','),
                                    TasksCounter = task.RotationTopic.RotationTasks.Count,
                                    ManagerComments = task.RotationTopic.ManagerComments,
                                    Section = task.RotationTopic.RotationTopicGroup.RotationModule.Type.GetEnumDescription(),
                                    ItemId = task.RotationTopicId.Value
                                }
                                : null;

            return model;
        }

        /// <summary>
        /// Save/Complite task for task board
        /// </summary>
        /// <param name="model"></param>
        /// <param name="completeOption"></param>
        public void SaveCompleteTaskBoardTask(TaskBoardCompleteOutcomesTabViewModel model, CompleteDialogOption completeOption)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(model.ID);

            switch (completeOption)
            {
                case CompleteDialogOption.Save:
                    _logicCore.TaskBoardCore.SaveTaskFeedback(task, model.Feedback);
                    break;
                case CompleteDialogOption.Complete:
                    _logicCore.TaskBoardCore.CompleteTask(task, model.Feedback);
                    break;
                default:
                    throw new Exception("Bad complete status info");
            }
        }

        #endregion

        #region Details dialog

        /// <summary>
        /// Get task for task board task details dialog 
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public TaskBoardTaskDetailsDialogViewModel PopulateTaskBoardTaskDetailsDialogViewModel(Guid taskId)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);

            TaskBoardTaskDetailsDialogViewModel model = new TaskBoardTaskDetailsDialogViewModel
            {
                TaskDetailTab = new TaskBoardCompleteTaskDetailTabViewModel
                {
                    Name = task.Name,
                    Deadline = task.Deadline,
                    TaskNotes = task.Description,
                    Tags = string.IsNullOrEmpty(task.SearchTags) ? null : task.SearchTags.Split(','),
                    AssignedFrom = _logicCore.RotationTaskCore.GetOwnerForTask(task).Id,
                    Priority = (TaskPriority)task.Priority,
                    CanChangeAssignee = task.CanChangeAssignee(),
                    IsFeedbackRequired = task.IsFeedbackRequired,
                    VoiceMessagesCounter = task.VoiceMessages.Count,
                    AttacmentsCounter = task.Attachments.Count
                },

                TaskOutcomesTab = new TaskBoardCompleteOutcomesTabViewModel
                {
                    ID = task.Id,
                    Feedback = task.SuccessorTaskId != null
                                ? task.SuccessorTask.Feedback
                                : task.Feedback,
                    IsFeedbackRequired = task.IsFeedbackRequired,
                    VoiceMessagesCounter = task.VoiceMessages.Count,
                    AttacmentsCounter = task.Attachments.Count
                }
            };

            if (task.ProjectId.HasValue)
            {
                model.ProjectDetailsTab = new ProjectAdminProjectDetailsTabViewModel
                {
                    ProjectId = task.Project.Id,
                    ProjectDescription = task.Project.Description,
                    ProjectName = task.Project.Name
                };
            }

            return model;
        }

        #endregion

        public bool IsTaskBoardTask(Guid taskId)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);

            return task.TaskBoardTaskType != TaskBoardTaskType.NotSet;
        }

        public TaskBoardPdfReportViewModel TaskBoardTasksForPdfReport(Guid userId, TaskBoardSection section, TaskBoardFilterOptions filter)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(userId);

            IQueryable<RotationTask> tasks = null;
            switch (section)
            {
                case TaskBoardSection.Assigned:
                    IQueryable<RotationTask> workItemAssignedTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromActiveWorkItems(user, TaskBoardTaskType.Draft);
                    IQueryable<RotationTask> taskBoardAssignedDraftTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(user, TaskBoardTaskType.Draft);
                    IQueryable<RotationTask> taskBoardAssignedPendingTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(user, TaskBoardTaskType.Pending);
                    tasks = (workItemAssignedTasks != null ? workItemAssignedTasks.Union(taskBoardAssignedDraftTasks) : taskBoardAssignedDraftTasks).Union(taskBoardAssignedPendingTasks);
                    break;
                case TaskBoardSection.Received:
                    IQueryable<RotationTask> workReceivedItemTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromActiveWorkItems(user, TaskBoardTaskType.Received);
                    IQueryable<RotationTask> taskBoardReceivedTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(user, TaskBoardTaskType.Received);
                    tasks = workReceivedItemTasks != null ? workReceivedItemTasks.Union(taskBoardReceivedTasks) : taskBoardReceivedTasks;
                    break;
                case TaskBoardSection.MyTasks:
                    tasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(user, TaskBoardTaskType.MyTask);
                    break;
                default:
                    tasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(user, TaskBoardTaskType.MyTask);
                    break;
            }

            tasks = TaskBoardSort.ApplySortOption(TaskBoardFilter.ApplyFilter(tasks, filter), TaskBoardSortOptions.NotSort);

            tasks = tasks.OrderBy(t => t.Name);

            TaskBoardPdfReportViewModel model = new TaskBoardPdfReportViewModel
            {
                Tasks = tasks.ToList().Select(t => new TaskBoardPdfReportItemViewModel
                {
                    TaskPriority = t.Priority.ToString(),
                    TaskName = t.Name,
                    TaskNotes = t.Description,
                    TopicName = t.RotationTopicId.HasValue ? t.RotationTopic.Name : "",
                    TaskDate = t.Deadline.FormatDayMonthYear()
                }),
                ReportDate = DateTime.Now.FormatDayMonthYear()
            };

            return model;
        }
    }
}
