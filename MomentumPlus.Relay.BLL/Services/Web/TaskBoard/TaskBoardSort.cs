﻿using MomentumPlus.Relay.Models;
using System;
using System.Linq;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    internal static class TaskBoardSort
    {
        /// <summary>
        /// Sort tasks by strategy.
        /// </summary>
        /// <param name="tasks">Tasks</param>
        /// <param name="sortStrategy">Sort strategy</param>
        /// <returns></returns>
        public static IQueryable<RotationTask> ApplySortOption(IQueryable<RotationTask> tasks, TaskBoardSortOptions sortStrategy)
        {
            switch (sortStrategy)
            {
                case TaskBoardSortOptions.NotSort:
                    tasks = tasks.OrderBy(t => t.Name);
                    break;
                case TaskBoardSortOptions.PriorityDirect:
                    tasks = tasks.OrderByDescending(t => t.Priority);
                    break;
                case TaskBoardSortOptions.PriorityReverse:
                    tasks = tasks.OrderBy(t => t.Priority);
                    break;
                case TaskBoardSortOptions.DueDateDirect:
                    tasks = tasks.OrderByDescending(t => t.Deadline);
                    break;
                case TaskBoardSortOptions.DueDateReverse:
                    tasks = tasks.OrderBy(t => t.Deadline);
                    break;
                default:
                    throw new Exception("Unknown TaskBoardFilter option.");
            }

            return tasks;
        }
    }
}
