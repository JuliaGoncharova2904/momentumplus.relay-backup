﻿using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Core.Authorization;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class TaskLogService : ITaskLogService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public TaskLogService(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._logicCore = logicCore;
            this._repositories = repositoriesUnitOfWork;
        }

        public IEnumerable<string> GetTaskLogs(Guid taskId)
        {
            return _logicCore.RotationTaskLogCore.GetTaskLogs(taskId);
        }
    }
}
