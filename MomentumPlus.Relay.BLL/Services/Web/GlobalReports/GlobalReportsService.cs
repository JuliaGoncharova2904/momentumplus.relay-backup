﻿using System;
using AutoMapper;
using System.Collections.Generic;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Services.Web.GlobalReports;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MvcPaging;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class GlobalReportsService : IGlobalReportsService
    {
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;

        public GlobalReportsService(LogicCoreUnitOfWork logicCore)
        {
            this._logicCore = logicCore;
            this._mapper = WebAutoMapperConfig.GetMapper();
        }

        /// <summary>
        /// Populate view models for History section.
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns></returns>
        public IPagedList<HistoryGlobalReportItemViewModel> GetHistoryReportsItemsForUser(Guid userId, int? page, int? pageSize)
        {
            HistoryReportsFilter filter = new HistoryReportsFilter(_logicCore);

            IEnumerable<HistoryGlobalReportItemViewModel> reportItems = filter.GetReportsPage(userId, page, pageSize);

            return reportItems.ToPagedList(filter.Page, filter.PageSize, filter.TotalSize);
        }

        /// <summary>
        /// Populate view models for Received section.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public IPagedList<ReceivedGlobalReportItemViewModel> GetReceivedReportsItemsForUser(Guid userId, int? page, int? pageSize)
        {
            ReceivedReportsFilter filter = new ReceivedReportsFilter(_logicCore);

            IEnumerable<ReceivedGlobalReportItemViewModel> reportItems = filter.GetReportsPage(userId, page, pageSize);

            return reportItems.ToPagedList(filter.Page, filter.PageSize, filter.TotalSize);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public IPagedList<SharedGlobalReportItemViewModel> GetSharedReportsItemsForUser(Guid userId, int? page, int? pageSize)
        {
            SharedReportsFilter filter = new SharedReportsFilter(_logicCore);

            IEnumerable<SharedGlobalReportItemViewModel> reportItems = filter.GetReportsPage(userId, page, pageSize);

            return reportItems.ToPagedList(filter.Page, filter.PageSize, filter.TotalSize);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="filterUserId"></param>
        /// <param name="filterRotationType"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public GlobalReportsListViewModel GetTeamReportsItemsForUser(Guid userId, Guid? filterUserId, RotationType? filterRotationType, int? page, int? pageSize)
        {
            TeamReportsFilter filter = new TeamReportsFilter(_logicCore);

            IEnumerable<TeamGlobalReportItemViewModel> reportItems = filter.GetReportsPage(userId, page, pageSize, filterRotationType, filterUserId);

            GlobalReportsListViewModel model = new GlobalReportsListViewModel
            {
                Users = _mapper.Map<IEnumerable<DropDownListItemModel>>(filter.Users),
                FilterUserId = filterUserId,
                FilterRotationType = filterRotationType,
                ReportsItems = reportItems.ToPagedList(filter.Page, filter.PageSize, filter.TotalSize)
            };

            return model;
        }

    }
}
