﻿using MomentumPlus.Relay.Models;
using System;
using System.Linq;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;

namespace MomentumPlus.Relay.BLL.Services.Web.GlobalReports
{
    public class SharedReportsFilter : ReportsFilteTemplate<SharedGlobalReportItemViewModel>
    {
        public SharedReportsFilter(LogicCoreUnitOfWork logicCore) : base(logicCore)
        {
        }

        private Guid _userId;

        protected override IQueryable<Rotation> BuildRotationsQuery(IQueryable<UserProfile> users)
        {
            return _logicCore.SharingReportCore.GetSharedRotationReportsForUser(this._userId);
        }

        protected override IQueryable<Shift> BuildShiftsQuery(IQueryable<UserProfile> users)
        {
            return _logicCore.SharingReportCore.GetSharedShiftReportsForUser(this._userId);
        }

        protected override IQueryable<UserProfile> BuildUsersQuery(Guid userId, Guid? filterUserId)
        {
            this._userId = userId;

            return null;
        }

        protected override SharedGlobalReportItemViewModel GetReportItemFromRotation(Rotation rotation)
        {
            return new SharedGlobalReportItemViewModel
            {
                CreatedTime = rotation.StartDate.Value,
                DateString = rotation.StartDate.Value.FormatWithDayMonthYear(),
                HandoverCreatorId = rotation.RotationOwnerId,
                HandoverCreatorName = rotation.RotationOwner.FullName,
                HandoverRecipientId = rotation.DefaultBackToBackId,
                HandoverRecipientName = rotation.DefaultBackToBack.FullName,
                PositionName = rotation.RotationOwner.Position.Name,
                RotationType = Models.RotationType.Swing,
                ItemsNumber = rotation.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                                                .Count(),
                TasksNumber = rotation.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                                                .SelectMany(t => t.RotationTasks.Where(ts => ts.Enabled))
                                                .Count(),
                SourceId = rotation.Id
            };
        }

        protected override SharedGlobalReportItemViewModel GetReportItemFromShift(Shift shift)
        {
            return new SharedGlobalReportItemViewModel
            {
                CreatedTime = shift.StartDateTime.Value,
                DateString = shift.StartDateTime.Value.FormatWithDayMonthYear(),
                HandoverCreatorId = shift.Rotation.RotationOwnerId,
                HandoverCreatorName = shift.Rotation.RotationOwner.FullName,
                HandoverRecipientId = shift.ShiftRecipientId.HasValue ? shift.ShiftRecipientId.Value : Guid.Empty,
                HandoverRecipientName = shift.ShiftRecipientId.HasValue ? shift.ShiftRecipient.FullName : string.Empty,
                PositionName = shift.Rotation.RotationOwner.Position.Name,
                RotationType = Models.RotationType.Shift,
                ItemsNumber = shift.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                                                .Count(),
                TasksNumber = shift.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                                                .SelectMany(t => t.RotationTasks.Where(ts => ts.Enabled))
                                                .Count(),
                SourceId = shift.Id
            };
        }
    }
}
