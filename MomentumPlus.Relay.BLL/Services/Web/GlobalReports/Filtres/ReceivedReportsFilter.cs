﻿using MomentumPlus.Relay.Models;
using System;
using System.Linq;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;

namespace MomentumPlus.Relay.BLL.Services.Web.GlobalReports
{
    public class ReceivedReportsFilter : ReportsFilteTemplate<ReceivedGlobalReportItemViewModel>
    {
        public ReceivedReportsFilter(LogicCoreUnitOfWork logicCore) : base(logicCore)
        {
        }

        private Guid _userId;

        protected override IQueryable<Rotation> BuildRotationsQuery(IQueryable<UserProfile> users)
        {
            return _logicCore.RotationCore.GetAllRotationsForUsers(users.Select(u => u.Id).ToList())
                                                                .Where(r => r.State != RotationState.Created &&
                                                                            r.RotationType == Core.Models.RotationType.Swing)
                                                                .SelectMany(r => r.HandoverFromRotations);
        }

        protected override IQueryable<Shift> BuildShiftsQuery(IQueryable<UserProfile> users)
        {
            return _logicCore.RotationCore.GetAllRotationsForUsers(users.Select(u => u.Id).ToList())
                                                                .Where(r => r.State != RotationState.Created &&
                                                                            r.RotationType == Core.Models.RotationType.Shift)
                                                                .SelectMany(r => r.RotationShifts)
                                                                .Where(s => s.State != ShiftState.Created)
                                                                .SelectMany(s => s.HandoverFromShifts);
        }

        protected override IQueryable<UserProfile> BuildUsersQuery(Guid userId, Guid? filterUserId)
        {
            this._userId = userId;

            return _logicCore.UserProfileCore.GetRelayEmployees().Where(u => u.Id == userId);
        }

        protected override ReceivedGlobalReportItemViewModel GetReportItemFromRotation(Rotation rotation)
        {
            return new ReceivedGlobalReportItemViewModel
            {
                CreatedTime = rotation.StartDate.Value,
                DateString = rotation.StartDate.Value.AddDays(rotation.DayOn).FormatWithDayMonthYear(),
                ReceivedFromId = rotation.RotationOwnerId,
                ReceivedFromName = rotation.RotationOwner.FullName,
                PositionName = rotation.RotationOwner.Position.Name,
                RotationType = Models.RotationType.Swing,
                ItemsNumber = rotation.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                                                .Count(),
                TasksNumber = rotation.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                                                .SelectMany(t => t.RotationTasks.Where(ts => ts.Enabled))
                                                .Count(),
                SourceId = rotation.HandoverToRotations.Where(r => r.RotationOwnerId == this._userId).First().Id,
                FromSourceId = rotation.Id
            };
        }

        protected override ReceivedGlobalReportItemViewModel GetReportItemFromShift(Shift shift)
        {
            return new ReceivedGlobalReportItemViewModel
            {
                CreatedTime = shift.StartDateTime.Value,
                DateString = shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes).FormatWithDayMonthYear(),
                ReceivedFromId = shift.Rotation.RotationOwnerId,
                ReceivedFromName = shift.Rotation.RotationOwner.FullName,
                PositionName = shift.Rotation.RotationOwner.Position.Name,
                RotationType = Models.RotationType.Shift,
                ItemsNumber = shift.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                                                .Count(),
                TasksNumber = shift.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                                                .SelectMany(t => t.RotationTasks.Where(ts => ts.Enabled))
                                                .Count(),
                SourceId = shift.HandoverToShifts.Where(s => s.Rotation.RotationOwnerId == this._userId).First().Id,
                FromSourceId = shift.Id
            };
        }
    }
}
