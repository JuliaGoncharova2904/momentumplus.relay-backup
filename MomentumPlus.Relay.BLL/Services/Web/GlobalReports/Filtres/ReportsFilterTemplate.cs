﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Constants;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;

namespace MomentumPlus.Relay.BLL.Services.Web.GlobalReports
{
    public abstract class ReportsFilteTemplate<TModel> where TModel : BaseGlobalReportItemViewModel
    {
        protected class ReportItem
        {
            public Guid Id;
            public Models.RotationType Type;
            public DateTime CreatedTime;
        }

        protected IQueryable<UserProfile> _users;
        protected readonly LogicCoreUnitOfWork _logicCore;

        public IEnumerable<UserProfile> Users
        {
            get
            {
                if (_users != null)
                    return _users.ToList().OrderBy(u => u.FullName);
                else
                    return new UserProfile[0];
            }
        }
        public int Page { get; private set; }
        public int PageSize { get; private set; }
        public int TotalSize { get; private set; }

        public ReportsFilteTemplate(LogicCoreUnitOfWork logicCore)
        {
            this._logicCore = logicCore;
        }

        public IEnumerable<TModel> GetReportsPage(Guid userId, int? page, int? pageSize, Models.RotationType? filterRotationType = null, Guid? filterUserId = null)
        {
            List<TModel> reportsItems = new List<TModel>();

            this.Page = page - 1 ?? 0;
            this.PageSize = pageSize ?? NumericConstants.Paginator.PaginatorPageSizeForGlobalReport;
            this._users = _logicCore.UserProfileCore.GetRelayEmployees().Where(u => u.CurrentRotation != null);
            IQueryable<UserProfile> users = this.BuildUsersQuery(userId, filterUserId);

            IEnumerable<ReportItem> reportItemsPage = this.ReportsPagination(users, filterRotationType, this.Page, this.PageSize);

            IQueryable<Shift> shiftQuery = _logicCore.ShiftCore.GetShiftsByIds(reportItemsPage.Where(ri => ri.Type == Models.RotationType.Shift)
                                                                                                            .Select(ri => ri.Id));
            IQueryable<Rotation> rotationsQuery = _logicCore.RotationCore.GetRotationsByIds(reportItemsPage.Where(ri => ri.Type == Models.RotationType.Swing)
                                                                                                            .Select(ri => ri.Id));
            string qmeReport = WebConfigurationManager.AppSettings["qmeReport"];

            foreach (Shift shift in shiftQuery.ToList())
            {
                var shiftItem = this.GetReportItemFromShift(shift);
                shiftItem.IsQME = Boolean.Parse(qmeReport);

                reportsItems.Add(shiftItem);
            }

            foreach (Rotation rotation in rotationsQuery.ToList())
            {
                var rotationItem = this.GetReportItemFromRotation(rotation);
                rotationItem.IsQME = Boolean.Parse(qmeReport);
                reportsItems.Add(rotationItem);
            }

            return reportsItems.OrderByDescending(r => r.CreatedTime);
        }

        private IEnumerable<ReportItem> ReportsPagination(IQueryable<UserProfile> users, Models.RotationType? filterRotationType, int page, int pageSize)
        {
            List<ReportItem> reportItems = new List<ReportItem>();

            this.TotalSize = 0;

            IQueryable<Shift> shiftQuery = this.BuildShiftsQuery(users);
            IQueryable<Rotation> rotationsQuery = this.BuildRotationsQuery(users);

            if (!filterRotationType.HasValue || filterRotationType.Value == Models.RotationType.Swing)
            {
                this.TotalSize += rotationsQuery.Count();
                reportItems.AddRange(rotationsQuery.Select(r => new ReportItem { Id = r.Id, Type = Models.RotationType.Swing, CreatedTime = r.StartDate.Value }));
            }
            if (!filterRotationType.HasValue || filterRotationType.Value == Models.RotationType.Shift)
            {
                this.TotalSize += shiftQuery.Count();
                reportItems.AddRange(shiftQuery.Select(s => new ReportItem { Id = s.Id, Type = Models.RotationType.Shift, CreatedTime = s.StartDateTime.Value }));
            }

            IEnumerable<ReportItem> reportItemsPage = reportItems.OrderByDescending(rep => rep.CreatedTime)
                                                                    .Skip(page * pageSize)
                                                                    .Take(pageSize);

            return reportItemsPage;
        }

        protected abstract IQueryable<Rotation> BuildRotationsQuery(IQueryable<UserProfile> users);

        protected abstract IQueryable<Shift> BuildShiftsQuery(IQueryable<UserProfile> users);

        protected abstract IQueryable<UserProfile> BuildUsersQuery(Guid userId, Guid? filterUserId);

        protected abstract TModel GetReportItemFromRotation(Rotation rotation);

        protected abstract TModel GetReportItemFromShift(Shift shift);
    }
}
