﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MvcPaging;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class NotificationService : INotificationService
    {
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public NotificationService(IServicesUnitOfWork services, LogicCoreUnitOfWork logicCore)
        {
            this._mapper = WebAutoMapperConfig.GetMapper();
            _logicCore = logicCore;
            _services = services;
        }

        public IPagedList<NotificationMessageItemViewModel> GetNotificationsByUser(Guid userId, int page, int pageSize)
        {
            IEnumerable<Notification> notifications = _logicCore.NotificationCore.GetNotificationsByUser(userId, page, pageSize);

            int totalCount = _logicCore.NotificationCore.GetTotalCountNotificationByUser(userId);
            return _mapper.Map<IEnumerable<NotificationMessageItemViewModel>>(notifications).ToPagedList(page, pageSize, totalCount);
        }

        public IEnumerable<NotificationMessageItemViewModel> GetNotificationsByUserForPopover(Guid userId, int page, int pageSize)
        {
            IEnumerable<Notification> notifications = _logicCore.NotificationCore.GetNotificationsByUser(userId, page, pageSize);

            return notifications.Any() ? _mapper.Map<IEnumerable<NotificationMessageItemViewModel>>(notifications) : new List<NotificationMessageItemViewModel>();
        }

        /// <summary>
        /// Count Not Shown Notifications For User
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns></returns>
        public int CountNotShownNotificationsForUser(Guid userId)
        {
            return _logicCore.NotificationCore.CountNotShownNotificationsForRecipient(userId);
        }

        /// <summary>
        /// Make Last Notifications Shown for User
        /// </summary>
        /// <param name="userId">User Id</param>
        public void MakeLastNotificationsShownForUser(Guid userId)
        {
            _logicCore.NotificationCore.MakeLastNotificationsShownForRecipient(userId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="notificationId"></param>
        public void MakeNotificationOpenedForUser(Guid userId, Guid notificationId)
        {
            _logicCore.NotificationCore.MakeNotificationOpenedForRecipient(userId, notificationId);
        }
    }
}
