﻿using System;
using System.Linq;
using AutoMapper;
using MomentumPlus.Core.Authorization;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.Api;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.ApiServices;
using MomentumPlus.Relay.Models.Api;
using MomentumPlus.Relay.Models.Api.Enums;

namespace MomentumPlus.Relay.BLL.Services.Api
{
    public class HomeService : IHomeService
    {
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public HomeService(LogicCoreUnitOfWork logicCore, IServicesUnitOfWork services = null)
        {
            this._mapper = ApiAutoMapperConfig.GetMapper();
            _logicCore = logicCore;
            _services = services;
        }

        public HomePageViewModel PopulateHomePage(Guid userId)
        {
            var user = _logicCore.UserProfileCore.GetUser(userId);

            string userRole = user.Role;

            bool userIsAdmin = iHandoverRoles.RelayGroups.Admins.Split(new[] { ',' }).Contains(userRole);
            bool userIsManager = iHandoverRoles.RelayGroups.LineManagers.Split(new[] { ',' }).Contains(userRole);

            var model = new HomePageViewModel
            {
                UserId = userId,
                Avatar = user.AvatarId.GetImagePath(),
                NotViewedNotificationsCount = _logicCore.NotificationCore.CountNotShownNotificationsForRecipient(userId),
                HandoverType = GetHandoverType(user.CurrentRotation),
                IsAdminOrManager = userIsAdmin || userIsManager,
                CurrentWorkPeriod = user.CurrentRotation.GetRotationPeriod()
            };

            switch (model.HandoverType)
            {
                case HandoverType.Shift:
                    var currentShift = _logicCore.ShiftCore.GetCurrentRotationShift(user.CurrentRotation);
                    model.CurrentHandoverId = currentShift?.Id;
                    if (currentShift != null)
                    {
                        model.HandoverTasksCount = _logicCore.RotationTaskCore.GetAllShiftTasks(currentShift.Id, TypeOfModuleSource.Draft).Count();
                    }
                    break;
                case HandoverType.Swing:
                    model.CurrentHandoverId = user.CurrentRotationId;
                    break;
            }

            return model;
        }

        private HandoverType GetHandoverType(Rotation rotation)
        {
            var handoverType = HandoverType.None;

            if (rotation != null)
            {
                switch (rotation.RotationType)
                {
                    case RotationType.Swing:
                        return HandoverType.Swing;
                    case RotationType.Shift:
                        return HandoverType.Shift;
                }
            }

            return handoverType;
        }

    }
}
