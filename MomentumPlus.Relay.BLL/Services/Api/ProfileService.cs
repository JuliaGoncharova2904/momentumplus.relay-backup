﻿using System;
using System.Drawing;
using System.IO;
using AutoMapper;
using MomentumPlus.Relay.BLL.Extensions.Api;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.ApiServices;
using MomentumPlus.Relay.Models.Api;
using File = MomentumPlus.Core.Models.File;

namespace MomentumPlus.Relay.BLL.Services.Api
{
    public class ProfileService : IProfileService
    {
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public ProfileService(LogicCoreUnitOfWork logicCore, IServicesUnitOfWork services = null)
        {
            this._mapper = ApiAutoMapperConfig.GetMapper();
            _logicCore = logicCore;
            _services = services;
        }

        public ProfilePageViewModel PopulateProfileInfo(Guid userId)
        {
            var userProfile = _logicCore.UserProfileCore.GetUser(userId);

            var model = new ProfilePageViewModel
            {
                Avatar = userProfile.AvatarId.GetImagePath(),
                Name = userProfile.FullName,
                Surname = userProfile.LastName,
                Email = userProfile.Email,
                Title = userProfile.Title ?? "Not set",
                Position = userProfile.PositionName,
                Company = userProfile.Company.Name,
                Team = userProfile.Team.Name
            };

            return model;
        }


        public string UpdateAvatar(string base64ImageData, Guid userId)
        {
            var user = _logicCore.UserProfileCore.GetUser(userId);

            byte[] imageData = System.Convert.FromBase64String(base64ImageData);

            MemoryStream imageStream = new MemoryStream(imageData);

            File newAvatar = _logicCore.MediaCore.AddImage("Avatar", "Avatar.jpeg", "image/jpeg", imageStream);

            _logicCore.UserProfileCore.UpdateUserAvatar(user, newAvatar);

            return user.AvatarId.GetImagePath();
        }

    }
}
