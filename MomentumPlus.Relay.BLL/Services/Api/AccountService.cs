﻿using AutoMapper;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.ApiServices;

namespace MomentumPlus.Relay.BLL.Services.Api
{
    public class AccountService : IAccountService
    {
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public AccountService(LogicCoreUnitOfWork logicCore, IServicesUnitOfWork services = null)
        {
            this._mapper = ApiAutoMapperConfig.GetMapper();
            _logicCore = logicCore;
            _services = services;
        }
    }
}
