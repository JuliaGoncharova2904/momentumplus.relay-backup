﻿using System.Web;
using Microsoft.AspNet.Identity.Owin;
using MomentumPlus.Core.Authorization;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.LogicCore.Cores;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Logic;

namespace MomentumPlus.Relay.BLL.LogicCore
{
    public class LogicCoreUnitOfWork : ILogicCoreUnitOfWork
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly INotificationAdapter _notificationAdapter;
        public readonly IMailerService _mailerService;

        private ModuleCore _moduleCore;
        private TopicGroupCore _topicGroupCore;
        private TopicCore _topicCore;
        private TaskCore _taskCore;

        private TemplateCore _templateCore;
        private RotationCore _rotationCore;
        private PositionCore _positionCore;
        private UserProfileCore _userProfileCore;

        private RotationModuleCore _rotationModuleCore;
        private RotationTopicGroupCore _rotationTopicGroupCore;
        private RotationTopicCore _rotationTopicCore;
        private RotationTaskCore _rotationTaskCore;
        private DailyNotesModuleCore _dailyNotesModuleCore;

        private TeamCore _teamCore;

        private MediaCore _mediaCore;


        private AdministrationCore _administrationCore;

        private SafetyMessageCore _safetyMessageCore;
        // Safety V2
        private MajorHazardV2Core _majorHazardV2Core;
        private CriticalControlV2Core _criticalControlV2Core;
        private SafetyStatV2Core _safetyMessageStatV2Core;
        private SafetyMessageV2Core _safetyMessageV2Core;
        //

        private ProjectCore _projectCore;
        private NotificationCore _notificationCore;
        private RotationTopicSharingRelationCore _rotationTopicSharingRelationCore;

        private ShiftCore _shiftCore;
        private SharingReportCore _sharingReportCore;
        private ApplicationUserManager _userManager;

        public ApplicationRoleManager _roleManager;


        private TaskBoardCore _taskBoardCore;
        private AttachmentCore _attachmentCore;
        private TeamRotationsCore _teamRotationsCore;

        private RotationTaskLogCore _taskLogCore;

        public LogicCoreUnitOfWork(IRepositoriesUnitOfWork repositoriesUnitOfWork, INotificationAdapter notificationAdapter, IMailerService mailerService)
        {
            this._repositories = repositoriesUnitOfWork;
            this._notificationAdapter = notificationAdapter;
            this._mailerService = mailerService;
        }

        public void SyncWithDatabase()
        {
            _repositories.Save();
        }

        #region Cores


        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public RotationTaskLogCore RotationTaskLogCore
        {
            get
            {
                return _taskLogCore ?? (_taskLogCore = new RotationTaskLogCore(this._repositories, this));
            }
        }

        public TeamRotationsCore TeamRotationsCore
        {
            get
            {
                return _teamRotationsCore ?? (_teamRotationsCore = new TeamRotationsCore(this._repositories, this));
            }
        }

        public AttachmentCore AttachmentCore
        {
            get
            {
                return _attachmentCore ?? (_attachmentCore = new AttachmentCore(this._repositories, this));
            }
        }

        public TaskBoardCore TaskBoardCore
        {
            get
            {
                return _taskBoardCore ?? (_taskBoardCore = new TaskBoardCore(this._repositories, this));
            }
        }


        public SharingReportCore SharingReportCore
        {
            get
            {
                return _sharingReportCore ?? (_sharingReportCore = new SharingReportCore(this._repositories, this));
            }
        }

        public ShiftCore ShiftCore
        {
            get
            {
                return _shiftCore ?? (_shiftCore = new ShiftCore(this._repositories, this));
            }
        }

        public RotationTopicSharingRelationCore RotationTopicSharingRelationCore
        {
            get
            {
                return _rotationTopicSharingRelationCore ?? (_rotationTopicSharingRelationCore = new RotationTopicSharingRelationCore(this._repositories, this));
            }
        }

        public NotificationCore NotificationCore
        {
            get
            {
                return _notificationCore ?? (_notificationCore = new NotificationCore(this._repositories, this, _notificationAdapter));
            }
        }

        public SafetyStatV2Core SafetyMessageStatV2Core
        {
            get
            {
                return _safetyMessageStatV2Core ?? (_safetyMessageStatV2Core = new SafetyStatV2Core(this._repositories, this));
            }
        }

        public MajorHazardV2Core MajorHazardV2Core
        {
            get
            {
                return _majorHazardV2Core ?? (_majorHazardV2Core = new MajorHazardV2Core(this._repositories, this));
            }
        }

        public CriticalControlV2Core CriticalControlV2Core
        {
            get
            {
                return _criticalControlV2Core ?? (_criticalControlV2Core = new CriticalControlV2Core(this._repositories, this));
            }
        }

        public SafetyMessageV2Core SafetyMessageV2Core
        {
            get
            {
                return _safetyMessageV2Core ?? (_safetyMessageV2Core = new SafetyMessageV2Core(this._repositories, this));
            }
        }


        public SafetyMessageCore SafetyMessageCore
        {
            get
            {
                return _safetyMessageCore ?? (_safetyMessageCore = new SafetyMessageCore(this._repositories, this));
            }
        }

        public AdministrationCore AdministrationCore
        {
            get
            {
                return _administrationCore ?? (_administrationCore = new AdministrationCore(this._repositories, this));
            }
        }

        public TeamCore TeamCore
        {
            get
            {
                return _teamCore ?? (_teamCore = new TeamCore(this._repositories, this));
            }
        }

        public MediaCore MediaCore
        {
            get
            {
                return _mediaCore ?? (_mediaCore = new MediaCore(this._repositories, this));
            }
        }

        public RotationTopicGroupCore RotationTopicGroupCore
        {
            get
            {
                return _rotationTopicGroupCore ?? (_rotationTopicGroupCore = new RotationTopicGroupCore(this._repositories, this));
            }
        }
        public RotationTaskCore RotationTaskCore
        {
            get
            {
                return _rotationTaskCore ?? (_rotationTaskCore = new RotationTaskCore(this._repositories, this));
            }
        }

        public RotationTopicCore RotationTopicCore
        {
            get
            {
                return _rotationTopicCore ?? (_rotationTopicCore = new RotationTopicCore(this._repositories, this));
            }
        }

        public RotationModuleCore RotationModuleCore
        {
            get
            {
                return _rotationModuleCore ?? (_rotationModuleCore = new RotationModuleCore(this._repositories, this));
            }
        }

        public RotationCore RotationCore
        {
            get
            {
                return _rotationCore ?? (_rotationCore = new RotationCore(this._repositories, this, _mailerService));
            }
        }


        public TemplateCore TemplateCore
        {
            get
            {
                return _templateCore ?? (_templateCore = new TemplateCore(this._repositories, this));
            }
        }

        public TaskCore TaskCore
        {
            get
            {
                return _taskCore ?? (_taskCore = new TaskCore(this._repositories, this));
            }
        }

        public ModuleCore ModuleCore
        {
            get
            {
                return _moduleCore ?? (_moduleCore = new ModuleCore(this._repositories, this));
            }
        }

        public TopicGroupCore TopicGroupCore
        {
            get
            {
                return _topicGroupCore ?? (_topicGroupCore = new TopicGroupCore(this._repositories, this));
            }
        }

        public TopicCore TopicCore
        {
            get
            {
                return _topicCore ?? (_topicCore = new TopicCore(this._repositories, this));
            }
        }

        public PositionCore PositionCore
        {
            get
            {
                return _positionCore ?? (_positionCore = new PositionCore(this._repositories, this));
            }
        }

        public UserProfileCore UserProfileCore
        {
            get
            {
                return _userProfileCore ?? (_userProfileCore = new UserProfileCore(this._repositories, this));
            }
        }

        public DailyNotesModuleCore DailyNotesModuleCore
        {
            get
            {
                return _dailyNotesModuleCore ?? (_dailyNotesModuleCore = new DailyNotesModuleCore(this._repositories, this));
            }
        }
        public ProjectCore ProjectCore
        {
            get
            {
                return _projectCore ?? (_projectCore = new ProjectCore(this._repositories, this));
            }
        }

        #endregion
    }
}
