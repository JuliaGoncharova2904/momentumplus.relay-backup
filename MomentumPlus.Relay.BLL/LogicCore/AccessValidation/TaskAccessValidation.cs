﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.EntityExtensions;

namespace MomentumPlus.Relay.BLL.LogicCore.AccessValidation
{
    public static class TaskAccessValidation
    {
        public static bool IsAssignedToProject(this RotationTask task)
        {
            return task.ProjectId.HasValue || task.RotationTopic.RotationTopicGroup.RelationId.HasValue;
        }

        public static bool CanChangeAssignee(this RotationTask task)
        {
            return (!task.AncestorTaskId.HasValue || !task.SuccessorTaskId.HasValue || !task.SuccessorTask.SuccessorTaskId.HasValue);
        }

        public static bool IsEditable(this RotationTask task)
        {
            switch(task.TaskBoardTaskType)
            {
                case TaskBoardTaskType.NotSet:
                    RotationModule module = task.RotationTopic.RotationTopicGroup.RotationModule;

                    return module.SourceType != TypeOfModuleSource.Received
                            && (module.RotationId.HasValue 
                                ? module.Rotation.State == RotationState.Confirmed
                                : module.Shift.State == ShiftState.Confirmed)
                            && !(task.SuccessorTask != null && task.SuccessorTask.IsComplete);
                case TaskBoardTaskType.Draft:
                case TaskBoardTaskType.Pending:
                case TaskBoardTaskType.MyTask:
                    return !task.IsComplete && !(task.SuccessorTask != null && task.SuccessorTask.IsComplete) && !task.HasParent();
                case TaskBoardTaskType.Received:
                    return task.CanChangeAssignee();
                default:
                    return false;
            }
        }

        public static bool IsShiftTask(this RotationTask task)
        {
            return task.RotationTopic.RotationTopicGroup.RotationModule.ShiftId.HasValue;
        }

        public static bool IsRotationTask(this RotationTask task)
        {
            return task.RotationTopic.RotationTopicGroup.RotationModule.RotationId.HasValue;
        }

        public static bool IsTaskBoardTask(this RotationTask task)
        {
            return task.TaskBoardId.HasValue;
        }

        public static bool IsTemplateTask(this RotationTask task)
        {
            return task.TemplateTaskId.HasValue;
        }
    }
}
