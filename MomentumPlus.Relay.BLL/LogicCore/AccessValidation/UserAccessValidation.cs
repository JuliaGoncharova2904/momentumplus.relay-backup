﻿using MomentumPlus.Core.Authorization;
using MomentumPlus.Core.Models;
using System;
using System.Linq;

namespace MomentumPlus.Relay.BLL.LogicCore.AccessValidation
{
    public static class UserAccessValidation
    {
        public static bool HasAccessToProject(this UserProfile user, Guid projectId)
        {
            return iHandoverRoles.RelayGroups.Admins.Split(new[] { ',' }).Contains(user.Role)
                    || iHandoverRoles.RelayGroups.LineManagers.Split(new[] { ',' }).Contains(user.Role)
                    || user.ManagedProjects.Any(p => p.Id == projectId);
        }

        public static bool IsOwnerOfTask(this UserProfile user, RotationTask task)
        {
            bool result = false;

            switch(task.TaskBoardTaskType)
            {
                case TaskBoardTaskType.NotSet:
                    RotationModule module = task.RotationTopic.RotationTopicGroup.RotationModule;
                    Guid ownerId = module.RotationId.HasValue
                                    ? module.Rotation.RotationOwnerId
                                    : module.Shift.Rotation.RotationOwnerId;
                    result = user.Id == ownerId;
                    break;
                case TaskBoardTaskType.Draft:
                case TaskBoardTaskType.Pending:
                case TaskBoardTaskType.MyTask:
                    result = user.Id == task.TaskBoardId;
                    break;
                case TaskBoardTaskType.Received:
                    result = user.Id == task.AncestorTask.TaskBoardId;
                    break;
                default:
                    throw new Exception("Unsupported task type of TaskBoard.");
            }

            return result;
        }


    }
}
