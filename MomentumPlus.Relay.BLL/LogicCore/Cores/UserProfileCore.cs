﻿using MomentumPlus.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class UserProfileCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public UserProfileCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public IQueryable<UserProfile> GetRelayEmployees()
        {
            IQueryable<UserProfile> employees = _repositories.UserProfileRepository.Find(e => e.RotationPatternId != null);
            return employees;
        }

        public IQueryable<UserProfile> GetEmployeesWithoutRotation(IQueryable<UserProfile> employees)
        {
            return employees.Where(c => c.CurrentRotationId == null);
        }

        public IQueryable<UserProfile> GetEmployeesByRole(IQueryable<UserProfile> employees, string role)
        {
            var users = _logicCore.RoleManager.Roles.Where(r => r.Name == role).SelectMany(r => r.Users).Select(ur => ur.User).Select(u => u.Id).ToList();

            var filteredUsers = from user in employees
                                  where users.Contains(user.Id)
                                  select user;

            return filteredUsers;
        }

        public IQueryable<UserProfile> GetEmployeesByTeam(Guid teamId)
        {
            return this.GetRelayEmployees().Where(e => e.TeamId == teamId);
        }

        public IEnumerable<UserProfile> GetEmployeesByIds(IEnumerable<Guid> employeesIds)
        {
            return this.GetRelayEmployees().Where(e => employeesIds.Contains(e.Id)).ToList();
        }

        public UserProfile GetUser(Guid userId)
        {
            UserProfile user = _repositories.UserProfileRepository.Get(userId);

            if (user == null)
                throw new ObjectNotFoundException(string.Format("User with Id: {0} was not found.", userId));

            return user;
        }

        public UserProfile GetUserProfile(Guid userId)
        {
            UserProfile userProfile = _repositories.UserProfileRepository.Get(userId);

            if (userProfile == null)
                throw new ObjectNotFoundException(string.Format("User with Id: {0} was not found.", userId));

            return userProfile;
        }


        public IEnumerable<UserProfile> GetAllUsers(string searchQuery = null)
        {
            IQueryable<UserProfile> users = _repositories.UserProfileRepository.GetAll();

            if (!string.IsNullOrEmpty(searchQuery))
            {
                users = users.Where(f => f.FirstName.Contains(searchQuery) ||
                                        f.LastName.Contains(searchQuery) ||
                                        f.Position.Name.Contains(searchQuery) ||
                                        f.Team.Name.Contains(searchQuery));
            }

            return users.ToList();
        }

        public void UpdateUserAvatar(UserProfile user, File avatar)
        {
            user.Avatar = avatar;
            
            _repositories.UserProfileRepository.Update(user);

            _repositories.Save();
        }
    }
}
