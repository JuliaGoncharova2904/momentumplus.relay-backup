﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class TopicCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public TopicCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public TemplateTopic GetTopic(Guid topicId)
        {
            var topic = _repositories.TemplateTopicRepository.Get(topicId);

            return topic;
        }


        public IQueryable<TemplateTopic> GetTopicGroupTopics(Guid topicGroupId)
        {
            var topics = _repositories.TemplateTopicRepository.Find(t => t.TopicGroupId == topicGroupId).OrderBy(t => t.Name);

            return topics;
        }


        public TemplateTopic GetChildTopicFromModule(Guid parentTopicId, Guid templateModuleId)
        {
            var topic = _repositories.TemplateTopicRepository.Find(t => t.ParentTopicId == parentTopicId && t.TopicGroup.Module.Id == templateModuleId && !t.DeletedUtc.HasValue).FirstOrDefault();

            return topic;
        }

        public TemplateTopic GetChildTopicFromTopicGroup(Guid parentTopicId, Guid templateTopicGroupId)
        {
            var topic = _repositories.TemplateTopicRepository.Find(t => t.ParentTopicId == parentTopicId && t.TopicGroup.Id == templateTopicGroupId && !t.DeletedUtc.HasValue).FirstOrDefault();

            return topic;
        }


        public TemplateTopic AddChildTopic(TemplateTopic baseTopic, Guid templateTopicGroupId, bool status = false)
        {
            var topic = new TemplateTopic
            {
                Id = Guid.NewGuid(),
                Enabled = status,
                Description = baseTopic.Description,
                Name = baseTopic.Name,
                ParentTopicId = baseTopic.Id,
                TopicGroupId = templateTopicGroupId

            };

            _repositories.TemplateTopicRepository.Add(topic);
            _repositories.Save();

            return topic;
        }


        /// <summary>
        /// Get Or Create Template Topic From Base Topic
        /// </summary>
        /// <param name="templateTopicGroupId"></param>
        /// <param name="baseTopic"></param>
        /// <returns></returns>

        public TemplateTopic GetOrCreateTemplateTopicGroupTopic(Guid templateTopicGroupId, TemplateTopic baseTopic)
        {
            var templateTopic = GetChildTopicFromTopicGroup(baseTopic.Id, templateTopicGroupId) ??
                                AddChildTopic(baseTopic, templateTopicGroupId);

            return templateTopic;
        }

        public IQueryable<TemplateTopic> GetChildTopics(Guid topicId)
        {
            var topics = _repositories.TemplateTopicRepository.Find(tg => tg.ParentTopicId == topicId);

            return topics;
        }

        public void UpdateChildTopics(TemplateTopic topic)
        {
            var childTopics = GetChildTopics(topic.Id).ToList();

            foreach (var childTopic in childTopics)
            {
                UpdateChildTopic(topic, childTopic);
            }
        }

        public void UpdateChildTopic(TemplateTopic parentTopic, TemplateTopic childTopic)
        {
            childTopic.Description = parentTopic.Description;
            childTopic.Name = parentTopic.Name;
            childTopic.ParentTopicId = parentTopic.Id;
            childTopic.Id = childTopic.Id;

            _repositories.TemplateTopicRepository.Update(childTopic);
            _repositories.Save();
        }
    }
}