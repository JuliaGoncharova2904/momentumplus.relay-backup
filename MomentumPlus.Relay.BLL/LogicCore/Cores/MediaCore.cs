﻿using MomentumPlus.Core.Interfaces;
using System;
using System.IO;
using File = MomentumPlus.Core.Models.File;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class MediaCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public MediaCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public File AddImage(string title, string fileName, string contentType, Stream inputStream, bool save = true)
        {
            MemoryStream memoryStream = new MemoryStream();
            inputStream.CopyTo(memoryStream);

            File image = new File();

            image.Id = Guid.NewGuid();
            image.Title = title;
            image.BinaryData = memoryStream.ToArray();
            image.FileType = Path.GetExtension(fileName);
            image.ContentType = contentType;

            _repositories.FileRepository.Add(image);

            if (save)
                _repositories.Save();

            return image;
        }

        public File AddFile(string fileName, string contentType, Stream inputStream, bool save = true)
        {
            MemoryStream memoryStream = new MemoryStream();
            inputStream.CopyTo(memoryStream);

            File file = new File();

            file.Id = Guid.NewGuid();
            file.BinaryData = memoryStream.ToArray();
            file.FileType = Path.GetExtension(fileName);
            file.ContentType = contentType;

            _repositories.FileRepository.Add(file);

            if (save)
                _repositories.Save();

            return file;
        }
    }
}