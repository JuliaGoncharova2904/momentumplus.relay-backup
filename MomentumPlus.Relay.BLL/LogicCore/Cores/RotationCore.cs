﻿using System;
using System.Linq;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using System.Data.Entity;
using System.Collections.Generic;
using MomentumPlus.Relay.Interfaces;
using System.Data.Entity.Core;
using MomentumPlus.Relay.BLL.Jobs;
using NLog;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class RotationCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IMailerService _mailerService;
        private static readonly Logger HandoverLogger = LogManager.GetLogger("handover-logger");

        public RotationCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore, IMailerService mailerService)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
            this._mailerService = mailerService;
        }

        public IQueryable<Rotation> GetTemplateRotations(Guid templateId)
        {
            var positions = _repositories.PositionRepository.Find(p => p.TemplateId == templateId);

            var users = _repositories.UserProfileRepository.GetAll();

            var rotations = from position in positions
                            where position.TemplateId == templateId
                            from user in users
                            where user.PositionId == position.Id && user.CurrentRotationId.HasValue
                            select user.CurrentRotation;

            return rotations;
        }

        public Rotation GetRotation(Guid rotationId)
        {
            Rotation rotation = _repositories.RotationRepository.Get(rotationId);

            if (rotation == null)
                throw new ObjectNotFoundException(string.Format("Rotation with Id: {0} was not found.", rotationId));

            return rotation;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rotationsIds"></param>
        /// <returns></returns>
        public IQueryable<Rotation> GetRotationsByIds(IEnumerable<Guid> rotationsIds)
        {
            return _repositories.RotationRepository.Find(r => rotationsIds.Contains(r.Id));
        }

        public IQueryable<Rotation> GetActiveRotationsForPosition(Guid positionId)
        {
            IQueryable<Rotation> rotations = _repositories.UserProfileRepository
                                                .Find(c => c.PositionId == positionId && c.CurrentRotation != null)
                                                .Include(e => e.CurrentRotation)
                                                .Select(e => e.CurrentRotation);

            return rotations;
        }

        public IQueryable<Rotation> GetAllActiveRotations()
        {
            IQueryable<Rotation> rotations = _logicCore.UserProfileCore.GetRelayEmployees()
                                                .Where(c => c.CurrentRotationId != null)
                                                .Include(e => e.CurrentRotation)
                                                .Select(e => e.CurrentRotation);

            return rotations;
        }

        public IQueryable<Rotation> GetAllRotationsForUsers(IEnumerable<Guid> usersIds)
        {
            IQueryable<Rotation> rotations = _repositories.RotationRepository.Find(r => usersIds.Contains(r.RotationOwnerId));

            return rotations;
        }

        public IQueryable<Rotation> GetRotationsByState(RotationState state)
        {
            IQueryable<Rotation> rotations = _repositories.RotationRepository.GetAll()
                                                            .Where(r => r.State == state);

            return rotations;
        }

        #region Test functionality

        [Obsolete("Testing functionality")]
        public bool ExpireRotation(Rotation rotation)
        {
            this.EndSwingOfRotation(rotation);

            if (rotation.State == RotationState.SwingEnded)
            {
                DateTime currentDate = DateTime.Today;
                int passedDays = (currentDate - rotation.StartDate.Value).Days;

                if (passedDays < (rotation.DayOn + rotation.DayOff))
                {
                    rotation.DayOff = passedDays - rotation.DayOn;
                    rotation.RepeatTimes = 0;
                    rotation.State = RotationState.Expired;

                    _repositories.RotationRepository.Update(rotation);
                    _repositories.Save();

                    return true;
                }
            }

            return false;
        }

        [Obsolete("Testing functionality")]
        public bool EndSwingOfRotation(Rotation rotation)
        {
            if (rotation.State == RotationState.Confirmed)
            {
                DateTime currentDate = DateTime.Today;

                int workDays = (currentDate - rotation.StartDate.Value).Days;

                if (workDays < rotation.DayOn)
                {
                    rotation.DayOff += rotation.DayOn - workDays;
                    rotation.DayOn = workDays;
                    rotation.RepeatTimes = 0;
                    rotation.State = RotationState.SwingEnded;

                    _logicCore.DailyNotesModuleCore.UpdateDailyNotesForRotation(rotation, false);
                    this.AutoFinalizeAll(rotation, false);

                    _repositories.RotationRepository.Update(rotation);
                    _repositories.Save();

                    this.HandoverReportFromRotation(rotation);
                    this.EndAllRotationShifts(rotation);

                    new NotificationJobs(_logicCore).EndOfSwingNotificationSubJob(rotation);

                    _mailerService.SendRotationShareReportEmail(rotation.Id);

                    return true;
                }
            }

            return false;
        }

        public void EndAllRotationShifts(Rotation rotation)
        {
            foreach (var shift in rotation.RotationShifts.Where(shift => shift.State == ShiftState.Confirmed || shift.State == ShiftState.Break))
            {
                _logicCore.ShiftCore.EndShift(shift);
            }

            _repositories.Save();
        }

        #endregion

        public void SetPopulatedRotationForRotation(Rotation sourceRotation, Rotation destRotation, bool save = true)
        {
            sourceRotation.HandoverToRotations.Add(destRotation);
            _repositories.RotationRepository.Update(sourceRotation);

            if (save)
                _repositories.Save();
        }

        public bool IsPopulated(Rotation sourceRotation, Rotation destRotation)
        {
            return sourceRotation.HandoverToRotations.Select(r => r.Id).Contains(destRotation.Id);
        }

        public void SetRotationState(Rotation rotation, RotationState state, bool save = true)
        {
            rotation.State = state;

            if (save)
                _repositories.Save();
        }

        public void AutoFinalizeAll(Rotation rotation, bool save = true)
        {
            IQueryable<RotationModule> draftModules = _logicCore.RotationModuleCore.GetRotationModules(rotation.Id, TypeOfModuleSource.Draft)
                                                                                    .Where(rm => rm.Enabled);

            IEnumerable<RotationTopic> draftTopics = draftModules.SelectMany(rm => rm.RotationTopicGroups)
                                                                    .Where(rtg => rtg.Enabled)
                                                                    .SelectMany(rtg => rtg.RotationTopics)
                                                                    .Where(rt => rt.Enabled)
                                                                    .ToList();

            IEnumerable<RotationTask> draftTasks = draftModules.SelectMany(rm => rm.RotationTopicGroups)
                                                                    .Where(rtg => rtg.Enabled)
                                                                    .SelectMany(rtg => rtg.RotationTopics)
                                                                    .Where(rt => rt.Enabled)
                                                                    .SelectMany(rt => rt.RotationTasks)
                                                                    .Where(t => t.Enabled)
                                                                    .ToList();

            foreach (RotationTopic draftTopic in draftTopics)
            {
                if (draftTopic.FinalizeStatus == StatusOfFinalize.NotFinalized)
                {
                    draftTopic.FinalizeStatus = StatusOfFinalize.AutoFinalized;
                    _repositories.RotationTopicRepository.Update(draftTopic);
                }
            }

            foreach (RotationTask draftTask in draftTasks)
            {
                if (draftTask.FinalizeStatus == StatusOfFinalize.NotFinalized)
                {
                    draftTask.FinalizeStatus = StatusOfFinalize.AutoFinalized;
                    _repositories.RotationTaskRepository.Update(draftTask);
                }
            }

            if (save)
                _repositories.Save();
        }

        public void FinishCurrentRotationForUser(UserProfile user)
        {
            if (user != null)
            {
                Rotation rotation = user.CurrentRotation;

                if (rotation != null && rotation.State == RotationState.SwingEnded)
                {
                    rotation.DayOff -= (int)(rotation.StartDate.Value.AddDays(rotation.DayOn + rotation.DayOff).Date - DateTime.Today).TotalDays;
                    rotation.State = RotationState.Expired;

                    _repositories.RotationRepository.Update(rotation);
                    _repositories.Save();

                    this.MoveToNextRotation(user);
                }
            }
        }

        private void InitRotationItemsFromTemplate(Guid rotationId)
        {
            var template = _logicCore.TemplateCore.GetRotationTemplate(rotationId);

            foreach (var templateModule in template.Modules)
            {
                var rotationModule = _logicCore.RotationModuleCore.GetOrCreateRotationModule(rotationId, templateModule);

                if (templateModule.TopicGroups != null && templateModule.TopicGroups.Any())
                {
                    foreach (var templateTopicGroup in templateModule.TopicGroups)
                    {
                        var rotationTopicGroup = _logicCore.RotationTopicGroupCore.GetOrCreateRotationTopicGroup(rotationModule.Id, templateTopicGroup);

                        foreach (var templateTopic in templateTopicGroup.Topics)
                        {
                            var rotationTopic = _logicCore.RotationTopicCore.GetOrCreateRotationTopic(rotationTopicGroup.Id, templateTopic);

                            foreach (var templateTask in templateTopic.Tasks)
                            {
                                _logicCore.RotationTaskCore.GetOrCreateRotationTask(rotationTopic.Id, templateTask);
                            }
                        }
                    }
                }
            }
        }



        public void UpdateRotationItems(Rotation rotation, Guid oldDefaultBackToBackId, Guid newDefaultBackToBackId, bool save = true)
        {
            var rotationTopics = rotation.RotationModules.Where(m => m.SourceType == TypeOfModuleSource.Draft)
                                              .SelectMany(m => m.RotationTopicGroups)
                                              .SelectMany(tg => tg.RotationTopics)
                                              .Where(topic => topic.AssignedToId.HasValue
                                                           && topic.AssignedToId == oldDefaultBackToBackId);


            var rotationTasks = rotation.RotationModules.Where(m => m.SourceType == TypeOfModuleSource.Draft)
                                  .SelectMany(m => m.RotationTopicGroups)
                                  .SelectMany(tg => tg.RotationTopics)
                                  .SelectMany(t => t.RotationTasks)
                                  .Where(task => task.AssignedToId.HasValue
                                              && task.AssignedToId == oldDefaultBackToBackId);

            foreach (var rotationTopic in rotationTopics)
            {
                rotationTopic.AssignedToId = newDefaultBackToBackId;
                _repositories.RotationTopicRepository.Update(rotationTopic);
            }

            foreach (var rotationTask in rotationTasks)
            {
                rotationTask.AssignedToId = newDefaultBackToBackId;
                _repositories.RotationTaskRepository.Update(rotationTask);
            }

            if (save)
            {
                _repositories.Save();
            }

        }



        #region Rotation logic

        public bool AssignFirstRotation(Rotation rotation, bool save = true)
        {
            UserProfile userProfile = _logicCore.UserProfileCore.GetRelayEmployees()
                            .FirstOrDefault(e => e.Id == rotation.RotationOwnerId);

            if (userProfile != null && !userProfile.CurrentRotationId.HasValue)
            {
                userProfile.CurrentRotation = rotation;
                _repositories.RotationRepository.Add(rotation);

                this.InitRotationItemsFromTemplate(rotation.Id);
                _logicCore.ProjectCore.InitProjectModuleTopicGroups(userProfile, false);


                if (save)
                    _repositories.Save();

                return true;
            }

            return false;
        }

        public bool CheckRotationPatternCompatibility(Guid employeeId, DateTime startDate, DateTime endDate, DateTime backDate)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(employeeId);

            if (user != null)
            {
                RotationPattern pattern = user.RotationPattern;
                int dayOn = endDate.Subtract(startDate).Days + 1;
                int dayOff = backDate.Subtract(endDate).Days - 1;

                return pattern.DayOn == dayOn && pattern.DayOff == dayOff;
            }

            throw new Exception(string.Format("Employee with Id: {0} was not found.", employeeId));
        }

        public bool ConfirmRotation(Guid employeeId, DateTime startDate, DateTime endDate, DateTime backDate, int repeatTimes, bool save = true)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(employeeId);

            this.FinishCurrentRotationForUser(user);

            if (user != null && user.CurrentRotation != null && user.CurrentRotation.State == RotationState.Created)
            {
                if (startDate.Date <= DateTime.Now.Date && endDate.Date >= DateTime.Now.Date)
                {
                    Rotation rotation = user.CurrentRotation;

                    rotation.StartDate = startDate;
                    rotation.DayOn = endDate.Subtract(startDate).Days + 1;
                    rotation.DayOff = backDate.Subtract(endDate).Days - 1;
                    rotation.RepeatTimes = repeatTimes;
                    rotation.State = RotationState.Confirmed;
                    rotation.ConfirmDate = DateTime.Today;

                    _logicCore.DailyNotesModuleCore.CreateDailyNotesForRotation(rotation);

                    _repositories.RotationRepository.Update(rotation);

                    if (save)
                        _repositories.Save();

                    return true;
                }
            }

            return false;
        }


        public bool UpdateRotationDates(Guid rotationId, DateTime startDate, DateTime endDate, DateTime backDate, int RepeatTimes, bool save = true)
        {
            Rotation rotation = GetRotation(rotationId);

            if (rotation != null)
            {
                if (rotation.RotationOwner.CurrentRotationId.HasValue &&
                    rotation.Id == rotation.RotationOwner.CurrentRotationId.Value &&
                    rotation.State != RotationState.Created)
                {
                    rotation.DayOn = endDate.Subtract(startDate).Days + 1;
                    rotation.DayOff = backDate.Subtract(endDate).Days - 1;
                    rotation.RepeatTimes = RepeatTimes;

                    _repositories.RotationRepository.Update(rotation);

                    _logicCore.DailyNotesModuleCore.UpdateDailyNotesForRotation(rotation, false);

                    _logicCore.RotationTaskCore.RecountTasksDueDate(rotation, false);

                    if (save)
                        _repositories.Save();

                    return true;
                }

                return false;
            }

            throw new ObjectNotFoundException(string.Format("Rotation with Id: {0} was not found.", rotationId));
        }

        public DateTime RotationLastShiftEndDate(Rotation rotation)
        {
            if (rotation.State == RotationState.Confirmed && rotation.RotationType == RotationType.Shift)
            {
                Shift lastShift = rotation.RotationShifts.Where(s => s.State == ShiftState.Finished || s.State == ShiftState.Break)
                                                            .OrderBy(s => s.StartDateTime)
                                                            .FirstOrDefault();

                var lastShiftEndDate = DateTime.Now;

                if (lastShift != null &&  lastShift.State == ShiftState.Finished)
                {
                    lastShiftEndDate = lastShift.StartDateTime.Value.AddMinutes(lastShift.WorkMinutes + lastShift.BreakMinutes + 60);

                    return lastShiftEndDate;
                }

                if (lastShift != null && lastShift.State == ShiftState.Break)
                {
                    lastShiftEndDate = DateTime.Now;

                    return lastShiftEndDate;
                }



                return rotation.StartDate.Value;
            }

            throw new Exception();
        }

        public DateTime RotationSwingEndDate(Rotation rotation)
        {
            if (rotation.StartDate.HasValue)
            {
                DateTime expireDate = rotation.StartDate.Value.AddDays(rotation.DayOn);

                return expireDate;
            }

            return rotation.CreatedUtc.Value.AddDays(rotation.DayOn);
        }

        public DateTime RotationExpiredDate(Rotation rotation)
        {
            if (rotation.StartDate.HasValue)
            {
                DateTime expireDate = rotation.StartDate.Value.AddDays(rotation.DayOn + rotation.DayOff);

                return expireDate;
            }

            return rotation.CreatedUtc.Value.AddDays(rotation.DayOn + rotation.DayOff);
        }


        private Rotation CreateNextRotation(Rotation rotation)
        {
            if (rotation.NextRotation == null)
            {
                Rotation nextRotation = new Rotation();
                nextRotation.Id = Guid.NewGuid();
                //---- Init Link Mechanism --------
                rotation.NextRotation = nextRotation;
                nextRotation.PrevRotation = rotation;
                nextRotation.NextRotation = null;
                //---- Copy Default Fields --------
                nextRotation.RotationOwnerId = rotation.RotationOwnerId;
                nextRotation.LineManagerId = rotation.LineManagerId;
                nextRotation.DefaultBackToBack = rotation.DefaultBackToBack;
                nextRotation.Contributors = rotation.Contributors;
                nextRotation.RotationType = rotation.RotationType;
                //---- Init shift collection ------
                nextRotation.RotationShifts = new HashSet<Shift>();
                //---- Populate Specific Fields ---
                if (rotation.RepeatTimes > 0)
                {
                    nextRotation.StartDate = rotation.StartDate.Value.AddDays(rotation.DayOn + rotation.DayOff);
                    nextRotation.DayOn = rotation.DayOn;
                    nextRotation.DayOff = rotation.DayOff;
                    nextRotation.RepeatTimes = rotation.RepeatTimes - 1;
                }
                else
                {
                    nextRotation.StartDate = null;
                    nextRotation.DayOn = 0;
                    nextRotation.DayOff = 0;
                    nextRotation.RepeatTimes = 0;
                }
                nextRotation.RotationModules = new HashSet<RotationModule>();
                //---- Add to repository ----------
                _repositories.RotationRepository.Add(nextRotation);
                //---------------------------------
                return nextRotation;
            }

            return rotation.NextRotation;
        }

        public Rotation CreateNextRotation(UserProfile user, bool save = true)
        {
            Rotation rotation = CreateNextRotation(user.CurrentRotation);

            if (save)
                _repositories.Save();

            return rotation;
        }



        public void UpdateRotationType(Rotation currentRotation, RotationType rotationType, bool save = true)
        {
            var nextRotation = CreateNextRotation(currentRotation);

            nextRotation.RotationType = rotationType;

            if (save)
                _repositories.Save();
        }

        public void InitPinItems(Rotation currentRotation, Rotation nextRotation)
        {
            var pinnedTopicGroupsInCurrentRotation = _logicCore.RotationTopicGroupCore.GetRotationTopicGroupsWhereIsPinned(currentRotation.Id).ToList();

            var nextRotationTopicGroups = _logicCore.RotationTopicGroupCore.GetAllRotationTopicGroups(nextRotation.Id, TypeOfModuleSource.Draft).ToList();

            foreach (var pinnedTopicGroupInCurrentRotation in pinnedTopicGroupsInCurrentRotation)
            {
                if (nextRotationTopicGroups.Any(tg => tg.TempateTopicGroupId == pinnedTopicGroupInCurrentRotation.TempateTopicGroupId
                                                                             || pinnedTopicGroupInCurrentRotation.Name.Equals("Other")))
                {
                    foreach (var nextRotationTopicGroup in nextRotationTopicGroups.Where(tg => tg.RotationModule.Type == pinnedTopicGroupInCurrentRotation.RotationModule.Type))
                    {
                        foreach (var rotationTopic in pinnedTopicGroupInCurrentRotation.RotationTopics.Where(t => t.IsPinned.HasValue && t.IsPinned.Value && t.RotationTopicGroup.TempateTopicGroupId == nextRotationTopicGroup.TempateTopicGroupId).ToList())
                        {
                            var nextRotationCloneTopic = _logicCore.RotationTopicCore.GetOrCreatePinTopic(nextRotationTopicGroup.Id, rotationTopic, nextRotation.DefaultBackToBackId);

                            foreach (var rotationTask in rotationTopic.RotationTasks.Where(t => t.IsPinned.HasValue && t.IsPinned.Value).ToList())
                            {
                                var nextRotationCloneTask = _logicCore.RotationTaskCore.GetOrCreatePinTask(nextRotationCloneTopic.Id, rotationTask, nextRotation.DefaultBackToBackId);
                            }
                        }
                    }

                }
            }

            _repositories.Save();
        }

        public void MoveToNextRotation(UserProfile user, bool save = true)
        {
            if (user.CurrentRotation == null)
                return;

            Rotation nextRotation = this.CreateNextRotation(user);

            this.AutoFinalizeAll(user.CurrentRotation);
            this.InitRotationItemsFromTemplate(nextRotation.Id);
            this.InitPinItems(user.CurrentRotation, nextRotation);

            user.CurrentRotation = nextRotation;

            _logicCore.ProjectCore.InitProjectModuleTopicGroups(user, false);

            if (save)
                _repositories.Save();

        }

        #endregion

        public IQueryable<Rotation> GetAllUserRotations(Guid userId, bool OnlyConfirmed)
        {
            IQueryable<Rotation> rotations = _repositories.RotationRepository
                                                            .Find(r => r.RotationOwnerId == userId &&
                                                                        (!OnlyConfirmed || r.State != RotationState.Created));

            return rotations;
        }

        public Rotation GetFirstUserRotation(Guid userId, bool OnlyConfirmed)
        {
            Rotation rotation = _repositories.RotationRepository
                .Find(r => r.RotationOwnerId == userId &&
                           (!OnlyConfirmed || r.State != RotationState.Created))
                .OrderBy(r => r.ConfirmDate).FirstOrDefault();

            return rotation;
        }

        public Rotation GetLastConfirmedRotationForUser(Guid userId)
        {
            Rotation rotation = _repositories.RotationRepository.Find(r => r.RotationOwnerId == userId && r.State != RotationState.Created)
                                                                .OrderByDescending(r => r.CreatedUtc)
                                                                .FirstOrDefault();
            return rotation;
        }

        public void ChangeFinalizeStatusForDraftTopicsAndTasks(Guid rotationId, StatusOfFinalize status)
        {
            var rotationDraftModules = _logicCore.RotationModuleCore.GetRotationModules(rotationId, TypeOfModuleSource.Draft);

            var rotationTopics =
                rotationDraftModules.SelectMany(m => m.RotationTopicGroups).SelectMany(tg => tg.RotationTopics);

            foreach (var rotationTopic in rotationTopics.ToList())
            {
                _logicCore.RotationTopicCore.ChangeFinalizeStatusFoRotationTopic(rotationTopic, status, false);

                foreach (var rotationTask in rotationTopic.RotationTasks.ToList())
                {
                    _logicCore.RotationTaskCore.ChangeFinalizeStatusFoRotationTask(rotationTask, status, false);
                }
            }

            _repositories.Save();
        }

        /// <summary>
        /// Return rotations with penultimate day of swing for specific Date
        /// </summary>
        /// <param name="date">Penultimate day of swing Date</param>
        /// <returns></returns>
        public IEnumerable<Rotation> RotationsWithPenultimateDayOfSwingByDate(DateTime date)
        {
            IEnumerable<Rotation> rotations = _repositories.RotationRepository.Find(r => r.State == RotationState.Confirmed && !r.NextRotationId.HasValue)
                                                                                .ToList()
                                                                                .Where(r => r.StartDate.Value.AddDays(r.DayOn - 2).Date == date);

            return rotations;
        }

        #region Handover mechanism

        /// <summary>
        /// Handover rotation report
        /// </summary>
        /// <param name="rotation">Source rotation</param>
        public void HandoverReportFromRotation(Rotation rotation)
        {
            List<Guid> recipientsIds = this.GetRecipientsForRotation(rotation);

            foreach (Guid recipientId in recipientsIds)
            {
                this.HandoverReportFromRotationToRecipient(rotation, recipientId);
            }

        }

        /// <summary>
        /// Extract recipients from rotation
        /// </summary>
        /// <param name="rotation">Source rotation</param>
        /// <returns></returns>
        private List<Guid> GetRecipientsForRotation(Rotation rotation)
        {
            List<Guid> recipients = new List<Guid>();

            List<Guid?> recipientsFoRotationTopics = rotation.RotationModules.Where(rm => rm.SourceType == TypeOfModuleSource.Draft && rm.Enabled)
                                                             .SelectMany(rm => rm.RotationTopicGroups.Where(rtg => rtg.Enabled))
                                                             .SelectMany(rtg => rtg.RotationTopics.Where(rt => rt.Enabled))
                                                             .Select(rt => rt.AssignedToId)
                                                             .ToList();

            List<Guid?> recipientsFoRotationTasks = rotation.RotationModules.Where(rm => rm.SourceType == TypeOfModuleSource.Draft && rm.Enabled)
                                                             .SelectMany(rm => rm.RotationTopicGroups.Where(rtg => rtg.Enabled))
                                                             .SelectMany(rtg => rtg.RotationTopics.Where(rt => rt.Enabled))
                                                             .SelectMany(rt => rt.RotationTasks.Where(rta => rta.Enabled))
                                                             .Select(rta => rta.AssignedToId)
                                                             .ToList();

            recipientsFoRotationTopics.RemoveAll(item => item == null);
            recipientsFoRotationTasks.RemoveAll(item => item == null);

            recipients.AddRange(recipientsFoRotationTopics.Select(item => item.Value));
            recipients.AddRange(recipientsFoRotationTasks.Select(item => item.Value));

            return recipients.Distinct().ToList();
        }

        /// <summary>
        /// Handover rotation report to recipient
        /// </summary>
        /// <param name="rotation">Source rotation</param>
        /// <param name="recipientId">Recipient Id</param>
        private void HandoverReportFromRotationToRecipient(Rotation rotation, Guid recipientId)
        {
            Rotation destRotation = this.GetDestinationRotation(recipientId);

            if (destRotation != null && !_logicCore.RotationCore.IsPopulated(rotation, destRotation))
            {

                HandoverLogger.Info("System send Handover (Draft Report) from {0}, Email: {1} , RotationId: {2} . Recipient is {3}, email: {4} , RotationId: {5}",
                                     rotation.RotationOwner.FullName,
                                     rotation.RotationOwner.Email,
                                     rotation.Id,
                                     destRotation.RotationOwner.FullName,
                                     destRotation.RotationOwner.Email,
                                     destRotation.Id);

                IEnumerable<RotationModule> sourceModules = _logicCore.RotationModuleCore.GetRotationModules(rotation.Id, TypeOfModuleSource.Draft)
                                                                            .Where(rm => rm.Enabled)
                                                                            .ToList();

                foreach (RotationModule sourceModule in sourceModules)
                {
                    RotationModule destModule = _logicCore.RotationModuleCore.GetOrCreateRotationModule(destRotation.Id, sourceModule.Type, TypeOfModuleSource.Received);

                    this.HandoverModule(sourceModule, destModule, recipientId);
                }

                _logicCore.RotationCore.SetPopulatedRotationForRotation(rotation, destRotation);
            }
        }

        /// <summary>
        /// Get destination rotation for recipient
        /// </summary>
        /// <param name="recipientId">Recipient Id</param>
        /// <returns></returns>
        private Rotation GetDestinationRotation(Guid recipientId)
        {
            UserProfile backToBack = _logicCore.UserProfileCore.GetUser(recipientId);

            if (!backToBack.DeletedUtc.HasValue && backToBack.CurrentRotationId.HasValue)
            {
                Rotation rotation = backToBack.CurrentRotation;

                if (rotation.State == RotationState.SwingEnded || rotation.State == RotationState.Expired)
                {
                    rotation = _logicCore.RotationCore.CreateNextRotation(backToBack);
                }

                return rotation;
            }

            return null;
        }

        /// <summary>
        /// Handove rotation module
        /// </summary>
        /// <param name="sourceModule">Source module</param>
        /// <param name="destModule">Destination module</param>
        /// <param name="recipientId">Recipient Id</param>
        private void HandoverModule(RotationModule sourceModule, RotationModule destModule, Guid recipientId)
        {
            IEnumerable<RotationTopicGroup> sourceTopicGroups = sourceModule.RotationTopicGroups.Where(tg => tg.Enabled);

            foreach (RotationTopicGroup sourceTopicGroup in sourceTopicGroups)
            {
                var sourceTopics = sourceTopicGroup.RotationTopics.Where(t => t.Enabled && t.AssignedToId == recipientId).ToList();

                var sourceTasks = sourceTopicGroup.RotationTopics.Where(t => t.Enabled)
                                                                                .SelectMany(t => t.RotationTasks)
                                                                                .Where(t => t.Enabled && t.AssignedToId == recipientId).ToList();

                if (!sourceTopics.Any() && !sourceTasks.Any())
                    continue;


                RotationTopicGroup destTopicGroup = _logicCore.RotationTopicGroupCore.GetOrCreateChildTopicGroup(destModule.Id, sourceTopicGroup);

                foreach (RotationTask sourceTask in sourceTasks)
                {
                    RotationTopic destTopic = _logicCore.RotationTopicCore.GetOrCreateChildTopic(destTopicGroup.Id,
                                                                                                    sourceTask.RotationTopic,
                                                                                                    StatusOfFinalize.AutoFinalized);


                    _logicCore.RotationTaskCore.GetOrCreateChildTask(destTopic.Id, sourceTask, StatusOfFinalize.AutoFinalized);

                }

                foreach (RotationTopic sourceTopic in sourceTopics)
                {
                    RotationTopic destTopic = _logicCore.RotationTopicCore.GetOrCreateChildTopic(destTopicGroup.Id,
                                                                                                    sourceTopic,
                                                                                                    StatusOfFinalize.AutoFinalized);

                    if (destTopic.IsFeedbackRequired)
                    {
                        _logicCore.RotationTaskCore.GetOrCreateHandBackTask(destTopic);
                    }
                }
            }

        }

        #endregion

    }
}
