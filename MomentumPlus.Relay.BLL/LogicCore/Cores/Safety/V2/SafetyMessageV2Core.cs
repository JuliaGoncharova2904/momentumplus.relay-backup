﻿using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class SafetyMessageV2Core
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public SafetyMessageV2Core(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public int CountSafetyMessagesForUser(UserProfile user)
        {
            IQueryable<SafetyStatV2> stats = _repositories.SafetyStatV2Repository.Find(
                                                           ss => ss.RecipientId == user.Id &&
                                                           ss.State == SafetyRecipientStateV2.NoViewed);

            if (user.CurrentRotation.RotationType == RotationType.Shift)
            {
                IEnumerable<Rotation> userRotations = _logicCore.RotationCore.GetAllUserRotations(user.Id, true).ToList();

                IEnumerable<DateTime> shiftTimes = userRotations.SelectMany(r => r.RotationShifts)
                                                         .Where(shift => shift.State != ShiftState.Created)
                                                         .Select(shift => shift.StartDateTime.Value.Date)
                                                         .ToList();
                stats = stats.Where(stat => shiftTimes.Contains(stat.SafetyMessage.Date));
            }

            return stats.Count();
        }

        public IEnumerable<SafetyMessageV2> GetSafetyMessagesForUser(DateTime date, UserProfile user)
        {
            Rotation currentRotation = user.CurrentRotation;

            IQueryable<SafetyMessageV2> safetyMessages = _repositories.SafetyStatV2Repository
                                                                        .Find(ss => ss.RecipientId == user.Id && ss.State == SafetyRecipientStateV2.NoViewed)
                                                                        .Select(ss => ss.SafetyMessage);

            if (currentRotation.ConfirmDate.HasValue && currentRotation.ConfirmDate.Value == date)
            {
                Rotation prevRotation = currentRotation.PrevRotation;
                if (prevRotation != null)
                {
                    DateTime prevSwingEndDate = prevRotation.StartDate.Value.AddDays(prevRotation.DayOn - 1);
                    DateTime startDate = currentRotation.StartDate.Value;

                    safetyMessages = safetyMessages.Where(sm => sm.Date == date || sm.Date > prevSwingEndDate && sm.Date < startDate);

                    return safetyMessages.ToList();
                }
            }

            safetyMessages = safetyMessages.Where(sm => sm.Date == date);

            return safetyMessages.ToList();
        }

        public IEnumerable<SafetyMessageV2> GetSafetyMessagesForDate(DateTime scheduledDate)
        {
            IEnumerable<SafetyMessageV2> safetyMessages = _repositories.SafetyMessageV2Repository
                                                                        .Find(sm => sm.Date == scheduledDate)
                                                                        .ToList();
            return safetyMessages;
        }

        public IEnumerable<SafetyMessageV2> GetSafetyMessagesForMonth(int year, int month)
        {
            IEnumerable<SafetyMessageV2> safetyMessages = _repositories.SafetyMessageV2Repository
                                                                        .Find(sm => sm.Date.Year == year && sm.Date.Month == month)
                                                                        .ToList();
            return safetyMessages;
        }

        public SafetyMessageV2 GetSafetyMessage(Guid safetyMessageId)
        {
            SafetyMessageV2 safetyMessage = _repositories.SafetyMessageV2Repository.Get(safetyMessageId);

            if (safetyMessage == null)
                throw new ObjectNotFoundException(string.Format("SafetyMessageV2 with Id: {0} was not found.", safetyMessageId));

            return safetyMessage;
        }

        public void AddSafetyMessage(SafetyMessageV2 safetyMessage, bool save = true)
        {
            safetyMessage.Id = Guid.NewGuid();

            _repositories.SafetyMessageV2Repository.Add(safetyMessage);

            if (save)
                _repositories.Save();
        }

        public void UpdateSafetyMessage(SafetyMessageV2 safetyMessage, bool save = true)
        {
            _repositories.SafetyMessageV2Repository.Update(safetyMessage);

            if (save)
                _repositories.Save();
        }
    }
}
