﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class TopicForkCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public TopicForkCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        private RotationTopic GetOrCreateForkChildTopic(Guid topicGroupId, RotationTopic parentTopic, StatusOfFinalize finalizeStatus = StatusOfFinalize.NotFinalized)
        {
            var childTopic = GetForkChildTopic(topicGroupId, parentTopic) ?? AddChildForkTopic(topicGroupId, parentTopic, finalizeStatus);

            return childTopic;
        }



        private RotationTopic GetForkChildTopic(Guid topicGroupId, RotationTopic parentTopic)
        {
            var childForkTopic = _repositories.RotationTopicRepository.Find(topic => topic.RotationTopicGroupId == topicGroupId
                                                            && topic.ForkParentTopicId.HasValue
                                                            && topic.ForkParentTopicId == parentTopic.ForkParentTopicId).FirstOrDefault();
            return childForkTopic;
        }

        private RotationTopic AddChildForkTopic(Guid topicGroupId, RotationTopic parentTopic, StatusOfFinalize finalizeStatus = StatusOfFinalize.NotFinalized)
        {
            Guid? assignedToId = null;

            if (parentTopic.RotationTopicGroup.RotationModule.RotationId.HasValue)
            {
                assignedToId = parentTopic.RotationTopicGroup.RotationModule.Rotation.NextRotationId.HasValue ?
                                                            parentTopic.RotationTopicGroup.RotationModule.Rotation.NextRotation.DefaultBackToBackId :
                                                            parentTopic.AssignedToId;
            }
            else if (parentTopic.RotationTopicGroup.RotationModule.ShiftId.HasValue)
            {
                assignedToId = parentTopic.RotationTopicGroup.RotationModule.Shift.ShiftRecipientId;
            }

            RotationTopic childTopic = parentTopic.Clone();
            childTopic.AssignedToId = assignedToId;
            childTopic.RotationTopicGroupId = topicGroupId;
            childTopic.IsPinned = false;

            childTopic.IsFeedbackRequired = false;
            childTopic.FinalizeStatus = StatusOfFinalize.NotFinalized;

            childTopic.ManagerComments = null;


            childTopic.ForkParentTopicId = parentTopic.ForkParentTopicId.HasValue ? parentTopic.ForkParentTopicId : parentTopic.Id;

            if (parentTopic.ForkParentTopicId.HasValue)
            {
                parentTopic.ForkParentTopic.ForkCounter = parentTopic.ForkParentTopic.ForkCounter + 1;

                _repositories.RotationTopicRepository.Update(parentTopic.ForkParentTopic);
            }
            else
            {
                parentTopic.ForkCounter = parentTopic.ForkCounter + 1;
                _repositories.RotationTopicRepository.Update(parentTopic);
            }

            _repositories.RotationTopicRepository.Add(childTopic);
            _repositories.Save();

            return childTopic;
        }




        public RotationTopic CarryforwardTopic(RotationTopic topic, bool save = true)
        {
            var topicOwner = topic.RotationTopicGroup.RotationModule.Rotation.RotationOwner;

            if (topicOwner != null)
            {
                var currentRotation = topicOwner.CurrentRotation;

                var existingTopic = _logicCore.RotationTopicCore.GetAllRotationTopic(currentRotation.Id, TypeOfModuleSource.Draft)
                                    .FirstOrDefault(t => t.ForkParentTopicId.HasValue &&
                                                            t.ForkParentTopicId == topic.Id &&
                                                            t.RotationTopicGroup.RotationModule.Type == topic.RotationTopicGroup.RotationModule.Type ||
                                                            t.ForkParentTopicId.HasValue &&
                                                            t.ForkParentTopicId == topic.ForkParentTopicId &&
                                                            t.RotationTopicGroup.RotationModule.Type == topic.RotationTopicGroup.RotationModule.Type ||
                                                            topic.TempateTopicId.HasValue &&
                                                            t.TempateTopicId.HasValue &&
                                                            t.TempateTopicId == topic.TempateTopicId);

                if (existingTopic == null)
                {
                    var topicGroupFromNewRotation = _logicCore.RotationTopicGroupCore.TopicGroupFromNewRotation(currentRotation.Id, topic.RotationTopicGroup);

                    if (topicGroupFromNewRotation != null)
                    {
                        var carryforwardTopic = GetOrCreateForkChildTopic(topicGroupFromNewRotation.Id, topic);

                        if (save)
                        {
                            _repositories.Save();
                        }

                        return carryforwardTopic;
                    }
                }

                return existingTopic;
            }

            return null;

        }

        public RotationTopic GetOrCreatePinTopic(Guid topicGroupId, RotationTopic parentTopic, Guid? assignedToId = null)
        {
            var childForkTopic = GetForkChildTopic(topicGroupId, parentTopic);

            if (childForkTopic == null)
            {
                RotationTopic childTopic = parentTopic.Clone();
                childTopic.RotationTopicGroupId = topicGroupId;
                childTopic.AssignedToId = assignedToId;

                childTopic.Description = null;
                childTopic.IsFeedbackRequired = false;
                childTopic.FinalizeStatus = StatusOfFinalize.NotFinalized;

                childTopic.Attachments = null;

                childTopic.Location = null;
                childTopic.LocationId = null;

                childTopic.ManagerComments = null;

                childTopic.ForkParentTopicId = parentTopic.ForkParentTopicId.HasValue ? parentTopic.ForkParentTopicId : parentTopic.Id;

                if (parentTopic.ForkParentTopicId.HasValue)
                {
                    parentTopic.ForkParentTopic.ForkCounter = parentTopic.ForkParentTopic.ForkCounter + 1;

                    _repositories.RotationTopicRepository.Update(parentTopic.ForkParentTopic);
                }
                else
                {
                    parentTopic.ForkCounter = parentTopic.ForkCounter + 1;
                    _repositories.RotationTopicRepository.Update(parentTopic);
                }

                _repositories.RotationTopicRepository.Add(childTopic);
                _repositories.Save();

                return childTopic;
            }

            return childForkTopic;
        }
    }
}
