﻿
using System;
using System.Collections.Generic;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using System.Data.Entity.Core;
using System.Linq;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class TeamCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public TeamCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public Team GetTeam(Guid teamId)
        {
            Team team = _repositories.TeamRepository.Get(teamId);

            if(team == null)
                throw new ObjectNotFoundException(string.Format("Team with Id: {0} was not found.", teamId));

            return team;
        }

        public IEnumerable<Team> GetAllTeams()
        {
            return _repositories.TeamRepository.GetAll().ToList();
        }

        public IEnumerable<UserProfile> GetTeamMembers(Guid teamId, EntityAccess access = EntityAccess.NoDeleted)
        {
            IQueryable<UserProfile> users = _repositories.UserProfileRepository.Find(u => u.TeamId == teamId);

            if (access == EntityAccess.NoDeleted)
            {
                users = users.Where(u => !u.DeletedUtc.HasValue);
            }
            else if (access == EntityAccess.Deleted)
            {
                users = users.Where(u => u.DeletedUtc.HasValue);
            }

            return users.ToList();
        }

    }
}
