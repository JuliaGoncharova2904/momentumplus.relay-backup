﻿using MomentumPlus.Core.Authorization;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using System.Linq;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class TeamRotationsCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public TeamRotationsCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        /// <summary>
        /// Return rotations of users team.
        /// </summary>
        /// <param name="user">User entity</param>
        /// <param name="isViewMode">Is view mode flag</param>
        /// <returns></returns>
        public IQueryable<Rotation> GetActiveTeamRotations(UserProfile user, bool isViewMode)
        {
            bool userIsManager = user.Role == iHandoverRoles.Relay.LineManager
                                    || user.Role == iHandoverRoles.Relay.HeadLineManager
                                    || user.Role == iHandoverRoles.Relay.SafetyManager;

            bool userIsAdmin = user.Role == iHandoverRoles.Relay.Administrator
                                    || user.Role == iHandoverRoles.Relay.iHandoverAdmin
                                    || user.Role == iHandoverRoles.Relay.ExecutiveUser;

            IQueryable <Rotation> rotations = _logicCore.RotationCore.GetAllActiveRotations()
                                                                        .Where(r => r.RotationOwnerId != user.Id
                                                                                    && !r.RotationOwner.DeletedUtc.HasValue);
            if (userIsManager || (isViewMode && userIsAdmin))
            {
                rotations = rotations.Where(r => r.LineManagerId == user.Id);
            }
            else if(!userIsAdmin)
            {
                rotations = null;
            }

            return rotations;
        }

    }
}
