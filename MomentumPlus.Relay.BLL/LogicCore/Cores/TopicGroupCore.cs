﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.EntityExtensions;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class TopicGroupCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public TopicGroupCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public IQueryable<TemplateTopicGroup> GetModuleTopicGroups(Guid moduleId)
        {
            var model = _repositories.TemplateTopicGroupRepository.Find(tg => tg.ModuleId == moduleId).OrderBy(tg => tg.Name);

            return model;
        }


        public IQueryable<TemplateTopicGroup> GetBaseTopicGroups()
        {
            var model = _repositories.TemplateTopicGroupRepository.Find(tg => !tg.ParentTopicGroupId.HasValue && tg.Module.Type == TypeOfModule.Core 
                                                                           || !tg.ParentTopicGroupId.HasValue && tg.Module.Type == TypeOfModule.HSE).OrderBy(tg => tg.Name);

            return model;
        }


        public TemplateTopicGroup GetTopicGroup(Guid topicGroupId)
        {
            var topicGroup = _repositories.TemplateTopicGroupRepository.Get(topicGroupId);

            return topicGroup;
        }

        public TemplateTopicGroup GetChildTopicGroup(Guid parentTopicGroupId, Guid moduleId)
        {
            var topicGroup = _repositories.TemplateTopicGroupRepository.Find(tg => tg.ParentTopicGroupId == parentTopicGroupId && tg.ModuleId == moduleId).FirstOrDefault();

            return topicGroup;
        }

        public TemplateTopicGroup AddChildTopicGroup(TemplateTopicGroup baseTopicGroup, Guid moduleId, bool status = false)
        {
            var topicGroup = new TemplateTopicGroup
            {
                Id = Guid.NewGuid(),
                Enabled = status,
                ParentTopicGroupId = baseTopicGroup.Id,
                ModuleId = moduleId,
                RelationId = baseTopicGroup.RelationId,
                Description = baseTopicGroup.Description,
                Name = baseTopicGroup.Name
            };

            _repositories.TemplateTopicGroupRepository.Add(topicGroup);
            _repositories.Save();

            return topicGroup;
        }

        public IQueryable<TemplateTopicGroup> GetChildTopicGroups(Guid topicGroupId)
        {
            var topicGroups = _repositories.TemplateTopicGroupRepository.Find(tg => tg.ParentTopicGroupId == topicGroupId);

            return topicGroups;
        }

        public void UpdateChildTopicGroups(TemplateTopicGroup topicGroup)
        {
            var childTopicGroups = GetChildTopicGroups(topicGroup.Id).ToList();

            foreach (var childTopicGroup in childTopicGroups)
            {
                UpdateChildTopicGroup(topicGroup, childTopicGroup);
            }
        }

        public void UpdateChildTopicGroup(TemplateTopicGroup parentTopicGroup, TemplateTopicGroup childTopicGroup)
        {
            childTopicGroup.Description = parentTopicGroup.Description;
            childTopicGroup.Name = parentTopicGroup.Name;
            childTopicGroup.RelationId = parentTopicGroup.RelationId;
            childTopicGroup.ParentTopicGroupId = parentTopicGroup.Id;
            childTopicGroup.Id = childTopicGroup.Id;

            _repositories.TemplateTopicGroupRepository.Update(childTopicGroup);
            _repositories.Save();
        }



        /// <summary>
        /// Get Or Create Template Topic Group From Base Topic Group
        /// </summary>
        /// <param name="templateModuleId"></param>
        /// <param name="baseTopicGroup"></param>
        /// <returns></returns>

        public TemplateTopicGroup GetOrCreateTemplateTopicGroup(Guid templateModuleId, TemplateTopicGroup baseTopicGroup)
        {
            var templateTopicGroup = GetChildTopicGroup(baseTopicGroup.Id, templateModuleId) ??
                                     AddChildTopicGroup(baseTopicGroup, templateModuleId);

            return templateTopicGroup;
        }


        public IQueryable<RotationTopic> FindTopicGroupsTopics(Guid?[] topicGroupsIds, Guid[] userFilterIds = null)
        {
            var topics = _repositories.RotationTopicGroupRepository.Find(tg => tg.Enabled
                                                                            && tg.RotationModule.SourceType == TypeOfModuleSource.Draft
                                                                            && topicGroupsIds.Contains(tg.TempateTopicGroup.ParentTopicGroupId))
                                                                              .SelectMany(tg => tg.RotationTopics)
                .Where(t => t.Enabled && t.RotationTopicGroup.RotationModule.RotationId.HasValue && t.RotationTopicGroup.RotationModule.Rotation.State != RotationState.Created && t.RotationTopicGroup.RotationModule.Rotation.RotationType == RotationType.Swing
                || t.Enabled && t.RotationTopicGroup.RotationModule.ShiftId.HasValue && t.RotationTopicGroup.RotationModule.Shift.State != ShiftState.Created && t.RotationTopicGroup.RotationModule.Shift.Rotation.State != RotationState.Created);




            //var topics = _repositories.RotationTopicGroupRepository.Find(tg => tg.Enabled
            //                                                                  && tg.RotationModule.SourceType == TypeOfModuleSource.Draft
            //                                                                  && topicGroupsIds.Contains(tg.TempateTopicGroup.ParentTopicGroupId))
            //                                                                    .SelectMany(tg => tg.RotationTopics)
            //      .Where(t => t.Enabled && t.RotationTopicGroup.RotationModule.RotationId.HasValue && t.RotationTopicGroup.RotationModule.Rotation.State != RotationState.Created && t.RotationTopicGroup.RotationModule.Rotation.RotationType == RotationType.Swing
            //      || t.Enabled && t.RotationTopicGroup.RotationModule.ShiftId.HasValue && t.RotationTopicGroup.RotationModule.Shift.State != ShiftState.Created && t.RotationTopicGroup.RotationModule.Shift.Rotation.State != RotationState.Created && t.RotationTopicGroup.RotationModule.Rotation.RotationType == RotationType.Shift);

            if (userFilterIds != null)
            {
                topics = topics.Where(t => t.RotationTopicGroup.RotationModule.RotationId.HasValue
                && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId)
                  || t.RotationTopicGroup.RotationModule.ShiftId.HasValue
                && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId));

            }

            return topics;
        }

    }
}