﻿using System;
using System.Globalization;
using System.Linq;
using System.Web;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class AdministrationCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public AdministrationCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        /// <summary>
        /// Initialize Admin settings
        /// </summary>
        public void InitializeAdminSettings(bool save = true)
        {
            var existingAdminSettings = _repositories.AdminSettingsRepository.GetAll().FirstOrDefault();

            if (existingAdminSettings == null)
            {
                var adminSettings = new AdminSettings
                {
                    Id = Guid.NewGuid(),
                    PlanType = TypeOfPlan.Enterprise,
                    SupportType = TypeOfSupport.Premium,
                    HandoverPreviewType = TypeOfHandoverPreview.PenultimateDayOfSwing,
                    HandoverTriggerType = TypeOfHandoverTrigger.FinalDayOfSwing,
                    HostingProvider = "Azure",
                    HandoverLimit = 100,
                    TemplateLimit = 50,
                    DomainUrl = string.Format("{0}://{1}",
                                 HttpContext.Current.Request.Url.Scheme,
                                 HttpContext.Current.Request.Url.Host),
                    TwilioSettings = new TwilioSettings()
                };

                _repositories.AdminSettingsRepository.Add(adminSettings);
            }

            else if (string.IsNullOrEmpty(existingAdminSettings.DomainUrl))
            {
                existingAdminSettings.DomainUrl = string.Format("{0}://{1}",
                                 HttpContext.Current.Request.Url.Scheme,
                                 HttpContext.Current.Request.Url.Host);

                _repositories.AdminSettingsRepository.Update(existingAdminSettings);
            }

            if (save)
            {
                _repositories.Save();
            }

        }


        /// <summary>
        /// Get admin settings
        /// </summary>
        /// <returns> The <see cref="rAdminSettings"/> object. </returns>
        public AdminSettings GetAdminSettings()
        {
            var adminSettings = _repositories.AdminSettingsRepository.GetAll().FirstOrDefault();

            return adminSettings;
        }

        /// <summary>
        /// Return handover trigger time
        /// </summary>
        /// <returns> Handover Trigger Time</returns>
        public DateTime GetHandoverTriggerTime()
        {
            var adminSettings = GetAdminSettings();

            var handoverTriggerTime = string.IsNullOrEmpty(adminSettings.HandoverTriggerTime)
                ? "11:59pm"
                : adminSettings.HandoverTriggerTime;

            var triggerTime = DateTime.ParseExact(handoverTriggerTime, "hh:mmtt", CultureInfo.InvariantCulture);

            return triggerTime;
        }

        /// <summary>
        /// Get company logo image Id
        /// </summary>
        /// <returns>Company Logo Guid</returns>
        public Guid? GetCompanyLogoImageId()
        {
            var adminSettings = GetAdminSettings();

            return adminSettings.LogoId;
        }



        /// <summary>
        /// Return handover preview time
        /// </summary>
        /// <returns> Handover Preview Time</returns>
        public DateTime? GetHandoverPreviewTime()
        {
            DateTime? triggerTime = null;
            AdminSettings adminSettings = GetAdminSettings();

            if (!string.IsNullOrEmpty(adminSettings.HandoverPreviewTime))
                triggerTime = DateTime.ParseExact(adminSettings.HandoverPreviewTime, "hh:mmtt", CultureInfo.InvariantCulture);

            return triggerTime;
        }


    }
}
