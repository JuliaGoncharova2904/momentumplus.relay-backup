﻿using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class DailyNotesModuleCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public DailyNotesModuleCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        #region Create DailyNotes For Rotation

        public bool CreateDailyNotesForRotation(Rotation rotation)
        {
            if (!rotation.StartDate.HasValue)
                throw new Exception("Daily notes cannot be created because rotation does not have start time.");

            var module = _logicCore.RotationModuleCore.GetOrCreateRotationModule(rotation.Id, TypeOfModule.DailyNote, TypeOfModuleSource.Draft);

            var topicGroups = CreateTopicGroups(rotation.StartDate.Value, rotation.DayOn, rotation.DefaultBackToBackId);


            foreach (var topicGroup in topicGroups)
            {
                topicGroup.RotationModuleId = module.Id;

                _repositories.RotationTopicGroupRepository.Add(topicGroup);
            } 

            _repositories.Save();


            return true;
        }


        public void UpdateDailyNotesForRotation(Rotation rotation, bool save = true)
        {
            IEnumerable<RotationTopic> rotationDailyNotes = _logicCore.RotationTopicCore
                                                                        .GetAllRotationDailyNotes(rotation.Id)
                                                                        .ToList()
                                                                        .OrderBy(r => DateTime.Parse(r.Name));

            var dayOn = rotation.DayOn >= 1 ? rotation.DayOn - 1 : 0;


            DateTime rotationEndDate = rotation.StartDate.Value.AddDays(dayOn);
            RotationTopic lastRotationDailyNote = rotationDailyNotes.LastOrDefault();

            if (lastRotationDailyNote != null)
            {
                DateTime lastRotationDailyNoteDate = DateTime.Parse(lastRotationDailyNote.Name);

                if (lastRotationDailyNoteDate > rotationEndDate)
                {
                    rotationDailyNotes.Where(d => DateTime.Parse(d.Name) > rotationEndDate)
                                  .ToList().ForEach(d => _logicCore.RotationTopicCore.RemoveTopic(d, false));
                }
                else if (lastRotationDailyNoteDate < rotationEndDate)
                {
                    int counter = (int)(rotationEndDate - lastRotationDailyNoteDate).TotalDays;
                    RotationModule dailyNoteModule = _logicCore.RotationModuleCore.GetOrCreateRotationModule(rotation.Id, TypeOfModule.DailyNote, TypeOfModuleSource.Draft);

                    ICollection<RotationTopicGroup> newDailyNotes = CreateTopicGroups(lastRotationDailyNoteDate.AddDays(1), counter, rotation.RotationOwnerId);
                    ((HashSet<RotationTopicGroup>)dailyNoteModule.RotationTopicGroups).UnionWith(newDailyNotes);

                    _repositories.RotationModuleRepository.Update(dailyNoteModule);
                }
            }
            else
            {
                RotationModule dailyNoteModule = _logicCore.RotationModuleCore.GetOrCreateRotationModule(rotation.Id, TypeOfModule.DailyNote, TypeOfModuleSource.Draft);

                dailyNoteModule.RotationTopicGroups = CreateTopicGroups(rotation.StartDate.Value, rotation.DayOn, rotation.RotationOwnerId);
                _repositories.RotationModuleRepository.Update(dailyNoteModule);
            }


            if (save)
                _repositories.Save();

        }

        private ICollection<RotationTopicGroup> CreateTopicGroups(DateTime startDate, int dayOn, Guid backToBackId)
        {
            ICollection<RotationTopicGroup> topicGroups = new HashSet<RotationTopicGroup>();

            for (int i = 0; i < dayOn; ++i)
            {
                topicGroups.Add(CreateTopicGroup(startDate.AddDays(i).ToShortDateString(), backToBackId));
            }

            return topicGroups;
        }

        private RotationTopicGroup CreateTopicGroup(string name, Guid backToBackId)
        {
            RotationTopicGroup topicGroup = new RotationTopicGroup();

            topicGroup.Id = Guid.NewGuid();
            topicGroup.Name = name;
            topicGroup.Enabled = true;
            topicGroup.RelationId = null;
            topicGroup.TempateTopicGroup = null;

            topicGroup.RotationTopics = new HashSet<RotationTopic>();
            topicGroup.RotationTopics.Add(CreateTopic(name, backToBackId));

            return topicGroup;
        }

        private RotationTopic CreateTopic(string name, Guid backToBackId)
        {
            RotationTopic topic = new RotationTopic();

            topic.Id = Guid.NewGuid();
            topic.Name = name;
            topic.Enabled = false;
            topic.IsPinned = false;
            topic.TempateTopic = null;
            topic.AssignedToId = backToBackId;

            return topic;
        }

        #endregion

        public IEnumerable<RotationTopic> GetDailyNotesForRotation(Guid rotationId, TypeOfModuleSource sourceType)
        {
            var rotationModule = _logicCore.RotationModuleCore.GetOrCreateRotationModule(rotationId, TypeOfModule.DailyNote, sourceType);

            if (rotationModule != null && rotationModule.RotationTopicGroups != null)
            {
                var rotationTopicGroupsWidthTopics = rotationModule.RotationTopicGroups.Where(tg => tg.RotationTopics != null);

                if (rotationModule.RotationTopicGroups.Any() && rotationTopicGroupsWidthTopics.Any())
                {
                    var dailyNotes =
                        rotationModule.RotationTopicGroups.SelectMany(tg => tg.RotationTopics).Where(t => t.Enabled);

                    return dailyNotes;
                }
            }

            return new List<RotationTopic>();
        }

        public RotationTopic GetDailyNote(Guid dailyNoteId)
        {
            return _repositories.RotationTopicRepository.Get(dailyNoteId);
        }

        public void UpdateDailyNote(RotationTopic dailyNote)
        {
            _repositories.RotationTopicRepository.Update(dailyNote);
            _repositories.Save();
        }

    }
}
