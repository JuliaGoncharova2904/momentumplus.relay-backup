﻿using System;
using System.Linq;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using System.Data.Entity.Core;
using System.Collections.Generic;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class RotationTopicGroupCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public RotationTopicGroupCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public IQueryable<RotationTopicGroup> GetModuleTopicGroups(Guid moduleId)
        {
            var topicGroups = _repositories.RotationTopicGroupRepository.Find(tg => tg.RotationModuleId == moduleId && tg.Enabled).OrderBy(tg => tg.SearchTags).ThenBy(tg => tg.Name);

            return topicGroups;
        }

        public IQueryable<RotationTopicGroup> GetRotationTopicGroupsWhereIsPinned(Guid rotationId)
        {
            var topicGroups = _repositories.RotationTopicGroupRepository.Find(tg => tg.RotationModule.RotationId == rotationId && tg.RotationModule.SourceType == TypeOfModuleSource.Draft && tg.Enabled && tg.RotationTopics.Any(t => t.IsPinned.HasValue && t.IsPinned.Value && t.Enabled));

            return topicGroups;
        }

        public IQueryable<RotationTopicGroup> GetShiftTopicGroupsWhereIsPinned(Guid shiftId)
        {
            var topicGroups = _repositories.RotationTopicGroupRepository.Find(tg => tg.RotationModule.ShiftId == shiftId && tg.Enabled
                                                                                    && tg.RotationModule.SourceType != TypeOfModuleSource.Received
                                                                                    && tg.RotationTopics.Any(t => t.IsPinned.HasValue && t.IsPinned.Value
                                                                                                                || t.RotationTasks.Any(ts => ts.IsPinned.HasValue && tg.Enabled
                                                                                                                                                && ts.IsPinned.Value)));
            return topicGroups;
        }

        public IQueryable<RotationTopicGroup> GetAllRotationTopicGroups(Guid rotationId, TypeOfModuleSource sourceType)
        {
            var topicGroups = _logicCore.RotationModuleCore.GetRotationModules(rotationId, sourceType).SelectMany(tg => tg.RotationTopicGroups);

            return topicGroups;
        }

        public IQueryable<RotationTopicGroup> GetAllShiftTopicGroups(Guid shiftId, TypeOfModuleSource sourceType)
        {
            var topicGroups = _logicCore.RotationModuleCore.GetShiftModules(shiftId, sourceType).SelectMany(tg => tg.RotationTopicGroups);

            return topicGroups;
        }


        public bool TopicGroupIsAvailable(Guid newRotationId, RotationTopicGroup topicGroupFromOldRotation)
        {
            var rotationTopicGroups = this.GetAllRotationTopicGroups(newRotationId, TypeOfModuleSource.Draft);

            if (rotationTopicGroups.Any(tg => tg.TempateTopicGroupId == topicGroupFromOldRotation.TempateTopicGroupId || topicGroupFromOldRotation.Name.Equals("Other")))
            {
                return true;
            }

            return false;
        }


        public RotationTopicGroup TopicGroupFromNewRotation(Guid newRotationId, RotationTopicGroup topicGroupFromOldRotation)
        {
            var rotationTopicGroups = this.GetAllRotationTopicGroups(newRotationId, TypeOfModuleSource.Draft);

            var topicGroup = rotationTopicGroups.FirstOrDefault(tg =>
            tg.TempateTopicGroupId.HasValue && tg.TempateTopicGroupId == topicGroupFromOldRotation.TempateTopicGroupId && tg.Enabled ||
            tg.Name == "Other" && topicGroupFromOldRotation.Name == "Other" && tg.RotationModule.Type == topicGroupFromOldRotation.RotationModule.Type ||
            tg.ParentRotationTopicGroupId.HasValue && tg.ParentRotationTopicGroupId == topicGroupFromOldRotation.Id && tg.Enabled ||
            tg.RelationId.HasValue && tg.RelationId == topicGroupFromOldRotation.RelationId && tg.Enabled);

            return topicGroup;
        }



        public RotationTopicGroup GetTopicGroup(Guid topicGroupId)
        {
            RotationTopicGroup topicGroup = _repositories.RotationTopicGroupRepository.Get(topicGroupId);

            if (topicGroup == null)
                throw new ObjectNotFoundException(string.Format("RotationTopicGroup with Id: {0} was not found.", topicGroupId));

            return topicGroup;
        }

        public RotationTopicGroup GetRotationModuleTopicGroup(Guid templateTopicGroupId, Guid rotationModuleId)
        {
            var topicGroup = _repositories.RotationTopicGroupRepository.Find(tg => tg.TempateTopicGroupId == templateTopicGroupId && tg.RotationModuleId == rotationModuleId).FirstOrDefault();

            return topicGroup;
        }

        public IQueryable<RotationTopicGroup> GetActiveRotationTopicGroupsByTemplate(Guid templateTopicGroupId)
        {
            IQueryable<Rotation> activeRotations = _logicCore.RotationCore.GetAllActiveRotations();

            IQueryable<RotationTopicGroup> topicGroups = activeRotations.SelectMany(r => r.RotationModules)
                                                                        .SelectMany(rm => rm.RotationTopicGroups)
                                                                        .Where(tg => tg.TempateTopicGroupId == templateTopicGroupId);

            return topicGroups;
        }


        public IQueryable<RotationTopicGroup> GetActiveShiftTopicGroupsByTemplate(Guid templateTopicGroupId)
        {
            IQueryable<Shift> activeShifts = _logicCore.ShiftCore.GetAllActiveShifts();

            IQueryable<RotationTopicGroup> topicGroups = activeShifts.SelectMany(r => r.RotationModules)
                                                                        .SelectMany(rm => rm.RotationTopicGroups)
                                                                        .Where(tg => tg.TempateTopicGroupId == templateTopicGroupId);

            return topicGroups;
        }



        public IQueryable<RotationTopicGroup> GetTopicGroupsByRelationId(Guid relationId)
        {
            var topicGroups = _repositories.RotationTopicGroupRepository.Find(tg => tg.RelationId == relationId);

            return topicGroups;
        }

        public Guid GetDefaultAssigneeByTopicGroupId(Guid topicGroupId)
        {
            var rotationModule = GetTopicGroup(topicGroupId).RotationModule;

            if (rotationModule.RotationId.HasValue)
            {
                var rotationBackToBackId = rotationModule.Rotation.DefaultBackToBackId;

                return rotationBackToBackId;
            }

            var shiftRecipientId = rotationModule.Shift.ShiftRecipientId;

            return shiftRecipientId.GetValueOrDefault();
        }

        public RotationTopicGroup AddRotationTopicGroupFromTemplateTopicGroup(TemplateTopicGroup templateTopicGroup, Guid rotationModuleId)
        {
            RotationTopicGroup topicGroup = new RotationTopicGroup
            {
                Id = Guid.NewGuid(),
                Enabled = templateTopicGroup.Enabled,
                RotationModuleId = rotationModuleId,
                RelationId = templateTopicGroup.RelationId,
                Description = templateTopicGroup.Description,
                Name = templateTopicGroup.Name,
                TempateTopicGroupId = templateTopicGroup.Id,
                RotationTopics = new HashSet<RotationTopic>()
            };

            _repositories.RotationTopicGroupRepository.Add(topicGroup);
            _repositories.Save();

            return topicGroup;
        }


        public RotationTopicGroup AddOtherRotationTopicGroup(Guid rotationModuleId)
        {
            var topicGroup = new RotationTopicGroup
            {
                Id = Guid.NewGuid(),
                Enabled = true,
                RotationModuleId = rotationModuleId,
                ParentRotationTopicGroupId = null,
                RelationId = null,
                Description = null,
                Name = "Other",
                TempateTopicGroupId = null,
                SearchTags = "Other",
                RotationTopics = new HashSet<RotationTopic>()
            };

            _repositories.RotationTopicGroupRepository.Add(topicGroup);
            _repositories.Save();

            return topicGroup;
        }


        public void UpdateTopicGroupsNameFromRelation(Guid relationId, string name, bool save = true)
        {
            var topicGroups = this.GetTopicGroupsByRelationId(relationId).ToList();

            foreach (var topicGroup in topicGroups)
            {
                topicGroup.Name = name;
                _repositories.RotationTopicGroupRepository.Update(topicGroup);
            }

            if (save)
            {
                _repositories.Save();
            }
        }


        public RotationTopicGroup GetOrCreateRotationTopicGroup(Guid rotationModuleId, TemplateTopicGroup templateTopicGroup)
        {
            var rotationTopicGroup = GetRotationModuleTopicGroup(templateTopicGroup.Id, rotationModuleId) ??
                                     AddRotationTopicGroupFromTemplateTopicGroup(templateTopicGroup, rotationModuleId);

            return rotationTopicGroup;
        }

        public RotationTopicGroup GetChildTopicGroup(Guid moduleId, RotationTopicGroup parenTopicGroup)
        {
            var childTopicGroup =
                _repositories.RotationTopicGroupRepository.Find(
                    tg => tg.RotationModuleId == moduleId && tg.ParentRotationTopicGroupId == parenTopicGroup.Id).FirstOrDefault();

            return childTopicGroup;
        }

        public RotationTopicGroup AddChildRotationTopicGroup(Guid rotationModuleId, RotationTopicGroup parentRotationTopicGroup)
        {
            var topicGroup = new RotationTopicGroup
            {
                Id = Guid.NewGuid(),
                Enabled = parentRotationTopicGroup.Enabled,
                RotationModuleId = rotationModuleId,
                RelationId = parentRotationTopicGroup.RelationId,
                Description = parentRotationTopicGroup.Description,
                Name = parentRotationTopicGroup.Name,
                TempateTopicGroupId = null,
                ParentRotationTopicGroupId = parentRotationTopicGroup.Id
            };

            _repositories.RotationTopicGroupRepository.Add(topicGroup);
            _repositories.Save();

            return topicGroup;
        }




        public RotationTopicGroup GetOrCreateChildTopicGroup(Guid moduleId, RotationTopicGroup parenTopicGroup)
        {
            var childTopicGroup = GetChildTopicGroup(moduleId, parenTopicGroup) ??
                                     AddChildRotationTopicGroup(moduleId, parenTopicGroup);

            return childTopicGroup;
        }


    }
}
