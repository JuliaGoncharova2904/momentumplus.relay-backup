﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using System.Data.Entity.Core;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.BLL.LogicCore.AccessValidation;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class RotationTaskCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        private readonly TaskForkCore _forkCore;
        private readonly TaskHandoverCore _handoverCore;
        private readonly TaskHandBackCore _handBackCore;
        private readonly TaskShareCore _shareCore;
        private readonly TaskCompleteCore _completeCore;

        private readonly TaskDeleteCore _deleteCore;

        public RotationTaskCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;

            this._forkCore = new TaskForkCore(repositoriesUnitOfWork, _logicCore);
            this._handoverCore = new TaskHandoverCore(repositoriesUnitOfWork, _logicCore);
            this._handBackCore = new TaskHandBackCore(repositoriesUnitOfWork, _logicCore);
            this._shareCore = new TaskShareCore(repositoriesUnitOfWork, _logicCore);
            this._completeCore = new TaskCompleteCore(repositoriesUnitOfWork, _logicCore);
            this._deleteCore = new TaskDeleteCore(repositoriesUnitOfWork, _logicCore);

        }

        public IQueryable<RotationTask> GetTopicTasks(Guid topicId)
        {
            var tasks = _repositories.RotationTaskRepository.Find(t => t.RotationTopicId == topicId)
                                                            .Where(t => t.Enabled)
                                                            .OrderBy(tg => tg.Name);

            return tasks;
        }

        public bool AddTopicTask(Guid topicId, RotationTask task, bool save = true)
        {
            RotationTopic topic = _repositories.RotationTopicRepository.Get(topicId);
            if (topic != null)
            {
                topic.RotationTasks.Add(task);

                this.HandoverNowRotationTask(task);

                if (save)
                    _repositories.Save();

                return true;
            }

            return false;
        }

        public bool IsTopicTaskExist(Guid topicId, string name)
        {
            RotationTopic topic = _repositories.RotationTopicRepository.Get(topicId);
            if (topic != null)
            {
                return topic.RotationTasks.Any(t => t.Name == name);
            }

            throw new Exception($"Topic with Id: {topicId} is not exist.");
        }

        public bool IsTaskNameExist(Guid taskId, string name)
        {
            RotationTask task = _repositories.RotationTaskRepository.Get(taskId);
            if (task != null)
            {
                return task.RotationTopic.RotationTasks.Any(t => t.Name == name && t.Id != taskId);
            }

            throw new Exception($"Task with Id: {taskId} is not exist.");
        }

        public bool IsChildTaskComplited(RotationTask task)
        {
            return task.SuccessorTaskId.HasValue && task.SuccessorTask.IsComplete;
        }

        public IQueryable<RotationTask> GetTemplateTaskChildRotationTasks(Guid templateTaskId)
        {
            var tasks = _repositories.RotationTaskRepository.Find(t => t.TemplateTaskId == templateTaskId);

            return tasks;
        }

        public IQueryable<RotationTask> GetAllRotationTasks(Guid rotationId, TypeOfModuleSource sourceType)
        {
            var tasks = _repositories.RotationTaskRepository.Find(t => t.RotationTopic.RotationTopicGroup.RotationModule.RotationId == rotationId
                                                                    && t.Enabled
                                                                    && t.RotationTopic.RotationTopicGroup.RotationModule.SourceType == sourceType);

            return tasks;
        }

        public IQueryable<RotationTask> GetAllShiftTasks(Guid shiftId, TypeOfModuleSource sourceType)
        {
            var tasks = _repositories.RotationTaskRepository.Find(t => t.RotationTopic.RotationTopicGroup.RotationModule.ShiftId == shiftId
                                                                    && t.Enabled
                                                                    && t.RotationTopic.RotationTopicGroup.RotationModule.SourceType == sourceType);

            return tasks;
        }

        public IQueryable<RotationTask> GetActiveReceivedNowTasks()
        {
            IQueryable<RotationTask> tasks = _repositories.RotationRepository.GetAll()
                                                        .Where(r => r.State == RotationState.Confirmed)
                                                        .SelectMany(r => r.RotationModules)
                                                        .Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Received)
                                                        .SelectMany(rm => rm.RotationTopicGroups)
                                                        .Where(rtg => rtg.Enabled)
                                                        .SelectMany(rtg => rtg.RotationTopics)
                                                        .Where(rt => rt.Enabled)
                                                        .SelectMany(rt => rt.RotationTasks)
                                                        .Where(t => t.Enabled && (t.Status == StatusOfTask.Now || t.Status == StatusOfTask.NewNow));
            return tasks;
        }

        /// <summary>
        /// Get task by task id
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public RotationTask GetTask(Guid taskId)
        {
            RotationTask task = _repositories.RotationTaskRepository.Get(taskId);

            if (task == null)
                throw new ObjectNotFoundException($"RotationTask with Id: {taskId} was not found.");

            return task;
        }

        public RotationTask GetRotationTask(Guid templateTaskId, Guid rotationTopicId)
        {
            var topic = _repositories.RotationTaskRepository.Find(t => t.TemplateTaskId == templateTaskId && t.RotationTopicId == rotationTopicId && !t.DeletedUtc.HasValue).FirstOrDefault();

            return topic;
        }

        /// <summary>
        /// Return owner of task.
        /// </summary>
        /// <param name="task">RotationTask Entity</param>
        /// <returns></returns>
        public UserProfile GetOwnerForTask(RotationTask task)
        {
            UserProfile assignee = null;

            if (task.TaskBoardTaskType != TaskBoardTaskType.NotSet)
            {
                assignee = task.AncestorTaskId.HasValue && task.AncestorTask.TaskBoard != null
                            ? task.AncestorTask.TaskBoard.User
                            : task.TaskBoard.User;
            }
            else
            {
                RotationModule module = null;
                //--- Get owner module -----
                if (task.AncestorTaskId.HasValue)
                {
                    module = task.AncestorTask.RotationTopic.RotationTopicGroup.RotationModule;
                }
                else if (task.RotationTopic.ShareSourceTopicId.HasValue)
                {
                    module = task.RotationTopic.ShareSourceTopic.RotationTopicGroup.RotationModule;
                }
                else
                {
                    module = task.RotationTopic.RotationTopicGroup.RotationModule;
                }
                //--- shift or rotation ----
                if (module.RotationId.HasValue)
                {
                    assignee = module.Rotation.RotationOwner;
                }
                else if (module.ShiftId.HasValue)
                {
                    assignee = module.Shift.Rotation.RotationOwner;
                }
            }

            return assignee;
        }

        public RotationTask AddRotationTaskFromTemplateTask(TemplateTask templateTask, Guid rotationTopicId)
        {

            var task = new RotationTask
            {
                Id = Guid.NewGuid(),
                Enabled = templateTask.Enabled,

                Description = templateTask.Description,
                Name = templateTask.Name,
                TemplateTaskId = templateTask.Id,
                Deadline = DateTime.UtcNow.AddDays(1),
                Priority = templateTask.Priority,
                RotationTopicId = rotationTopicId,
                AssignedToId = _logicCore.RotationTopicCore.GetDefaultAssigneeByTopicId(rotationTopicId)
            };

            _repositories.RotationTaskRepository.Add(task);
            _repositories.Save();

            return task;
        }

        public void UpdateRotationTask(RotationTask task, Guid prevRecipientId)
        {
            if (task.Enabled)
            {
                this.UpdateNowRotationTask(task, prevRecipientId);

                task.FinalizeStatus = StatusOfFinalize.NotFinalized;

                _repositories.RotationTaskRepository.Update(task);
                _repositories.Save();
            }
        }

        public void Update(RotationTask task)
        {
            _repositories.RotationTaskRepository.Update(task);

            var childTask = task.SuccessorTask;

            while (childTask != null)
            {
                childTask.AssignedToId = task.AssignedToId;
                childTask.Name = task.Name;
                childTask.Description = task.Description;
                childTask.Deadline = task.Deadline;
                childTask.Priority = task.Priority;
                childTask.SearchTags = task.SearchTags;
                childTask.IsFeedbackRequired = task.IsFeedbackRequired;

                _repositories.RotationTaskRepository.Update(childTask);

                childTask = childTask.SuccessorTask;
            }



            _repositories.Save();
        }
        #region Now tasks

        public void MakeNowTaskOpened(RotationTask task, bool save = true)
        {
            if (task.Status != StatusOfTask.NewNow)
                return;

            task.Status = StatusOfTask.Now;
            _repositories.RotationTaskRepository.Update(task);

            if (save)
                _repositories.Save();
        }

        private void HandoverNowRotationTask(RotationTask task, bool save = true)
        {
            if (task.Status != StatusOfTask.Now || !task.AssignedToId.HasValue)
                return;

            UserProfile user = _logicCore.UserProfileCore.GetUser(task.AssignedToId.Value);
            Rotation destRotation = user.CurrentRotation;

            if (destRotation == null)
                return;

            if (destRotation.State == RotationState.SwingEnded || destRotation.State == RotationState.Expired)
            {
                destRotation = _logicCore.RotationCore.CreateNextRotation(user);
            }

            RotationTopic sourceTopic = task.RotationTopic;
            RotationTopicGroup sourceTopicGroup = sourceTopic.RotationTopicGroup;
            RotationModule sourceModule = sourceTopicGroup.RotationModule;

            RotationModule destModule = _logicCore.RotationModuleCore.GetOrCreateRotationModule(destRotation.Id, sourceModule.Type, TypeOfModuleSource.Received);
            RotationTopicGroup destTopicGroup = _logicCore.RotationTopicGroupCore.GetOrCreateChildTopicGroup(destModule.Id, sourceTopicGroup);
            RotationTopic destTopic = _logicCore.RotationTopicCore.GetOrCreateChildTopic(destTopicGroup.Id, sourceTopic, StatusOfFinalize.AutoFinalized);
            RotationTask destTask = _logicCore.RotationTaskCore.GetOrCreateChildTask(destTopic.Id, task, StatusOfFinalize.AutoFinalized);

            destTask.AncestorTask = task;
            destTask.Status = StatusOfTask.NewNow;

            if (save)
                _repositories.Save();

            //---- notification 10 ----
            string creatorName = sourceModule.Rotation.RotationOwner.FullName;
            _logicCore.NotificationCore.NotificationTrigger.Send_AssignedNowTaskForYou(user.Id, creatorName, task.Deadline, destTask.Id);
            //-------------------------
        }

        private void UpdateNowRotationTask(RotationTask task, Guid prevRecipientId)
        {
            if (task.Status != StatusOfTask.Now)
            {
                _deleteCore.RemoveRotationTaskChild(task, true);
                return;
            }

            if (task.AssignedToId != prevRecipientId)
            {
                _deleteCore.RemoveRotationTaskChild(task, false, false);
                HandoverNowRotationTask(task, false);
                _repositories.Save();

                //---- notification 14 ----
                _logicCore.NotificationCore.NotificationTrigger.Send_CreatorReassignedNowTask(prevRecipientId);
                //-------------------------
            }
            else
            {
                RotationTask destTask = task.SuccessorTask;

                if (destTask != null)
                {
                    //---- notification 13 ----
                    if (destTask.Deadline != task.Deadline)
                    {
                        _logicCore.NotificationCore.NotificationTrigger.Send_CreatorModifiedDueDate(task.AssignedToId.Value, destTask.Id);
                    }
                    //-------------------------

                    destTask.Attachments = task.Attachments;
                    destTask.Deadline = task.Deadline;
                    destTask.Description = task.Description;
                    destTask.IsFeedbackRequired = task.IsFeedbackRequired;
                    destTask.IsNullReport = destTask.IsNullReport;
                    destTask.IsPinned = task.IsPinned;
                    destTask.Name = task.Name;
                    destTask.Priority = task.Priority;
                    destTask.VoiceMessages = task.VoiceMessages;

                    _repositories.RotationTaskRepository.Update(destTask);
                    _repositories.Save();
                }
                else
                {
                    HandoverNowRotationTask(task);
                }
            }
        }


        #endregion

        public RotationTask GetOrCreateRotationTask(Guid rotationTopicId, TemplateTask templateTask)
        {
            var rotationTopic = GetRotationTask(templateTask.Id, rotationTopicId) ??
                                     AddRotationTaskFromTemplateTask(templateTask, rotationTopicId);

            return rotationTopic;
        }




        public RotationTask GetOrCreatePinTask(Guid topicId, RotationTask parenTask, Guid? assignedToId = null)
        {
            return _forkCore.GetOrCreatePinTask(topicId, parenTask, assignedToId);
        }

        public RotationTask GetOrCreateChildTask(Guid topicId, RotationTask parenTask, StatusOfFinalize finalizeStatus = StatusOfFinalize.NotFinalized)
        {
            var childTask = _handoverCore.GetOrCreateChildTask(topicId, parenTask, finalizeStatus);

            return childTask;
        }


        public IQueryable<RotationTask> GetReceivedTasksByCreator(Guid rotationId, Guid userId)
        {
            var rotationTasks = GetAllRotationTasks(rotationId, TypeOfModuleSource.Received).Where(t => t.RotationTopic.AncestorTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId == userId);

            return rotationTasks;
        }

        public void ChangeFinalizeStatusFoRotationTask(RotationTask task, StatusOfFinalize status, bool save = true)
        {
            task.FinalizeStatus = status;

            _repositories.RotationTaskRepository.Update(task);

            if (save)
            {
                _repositories.Save();
            }

        }

        public RotationTask GetOrCreateHandBackTask(RotationTopic topic)
        {
            var handBackTask = _handBackCore.GetOrCreateHandBackTask(topic);

            return handBackTask;
        }

        public RotationTask AddShareTask(RotationTopic topic)
        {
            return _shareCore.AddShareTask(topic);
        }


        public void UpdateTopicTasks(RotationTopic topic, bool save = true)
        {
            foreach (var task in topic.RotationTasks)
            {
                task.IsFeedbackRequired = topic.IsFeedbackRequired;

                _repositories.RotationTaskRepository.Update(task);
            }

            if (save)
            {
                _repositories.Save();
            }
        }


        public string GetTaskManagerComments(Guid taskId)
        {
            var task = GetTask(taskId);

            return task.ManagerComments;
        }

        public void CarryforwardTask(RotationTask task, bool save = true)
        {
            _forkCore.CarryforwardTask(task);
        }

        /// <summary>
        /// Recout Due Date for Tasks of Rotation (According rotation pattern)
        /// </summary>
        /// <param name="rotation">Rotation entity</param>
        /// <param name="save">Save to database (true - apply changes immediately; false - not apply changes now;)</param>
        public void RecountTasksDueDate(Rotation rotation, bool save = true)
        {
            IEnumerable<RotationTask> rotationTasks = GetAllRotationTasks(rotation.Id, TypeOfModuleSource.Draft).ToList();

            DateTime endRotationDate = rotation.StartDate.Value.AddDays(rotation.DayOn + rotation.DayOff - 1).Date;

            DateTime endSwingDate = rotation.StartDate.Value.AddDays(rotation.DayOn).Date;

            foreach (RotationTask task in rotationTasks)
            {
                if (task.Deadline.Date > endRotationDate)
                {
                    task.Deadline = endRotationDate;
                    _repositories.RotationTaskRepository.Update(task);
                }

                if (task.Deadline.Date < endSwingDate)
                {
                    task.Deadline = endSwingDate;
                    _repositories.RotationTaskRepository.Update(task);
                }
            }

            if (save)
                _repositories.Save();
        }

        /// <summary>
        /// Remove task
        /// </summary>
        /// <param name="task">Rotation task</param>
        /// <param name="save">Sync with database</param>
        /// <returns></returns>
        public bool RemoveRotationTask(RotationTask task, bool save = true)
        {
            return _deleteCore.RemoveRotationTask(task, save);
        }

        /// <summary>
        /// Remove Shift task
        /// </summary>
        /// <param name="task">Shift task</param>
        /// <param name="save">Sync with database</param>
        /// <returns></returns>
        public bool RemoveShiftTask(RotationTask task, bool save = true)
        {
            return _deleteCore.RemoveShiftTask(task, save);
        }

        /// <summary>
        /// Remove task board task.
        /// </summary>
        /// <param name="task">Task entity</param>
        /// <param name="save">Sync with database</param>
        /// <returns></returns>
        public bool RemoveTaskBoardTask(RotationTask task, bool save = true)
        {
            return _deleteCore.RemoveTaskBoardTask(task, save);
        }

        /// <summary>
        /// Get shift received tasks by creator
        /// </summary>
        /// <param name="shiftId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IQueryable<RotationTask> GetShiftReceivedTasksByCreator(Guid shiftId, Guid userId)
        {
            var rotationTasks = GetAllShiftTasks(shiftId, TypeOfModuleSource.Received).Where(t => t.RotationTopic.AncestorTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId == userId);

            return rotationTasks;
        }

        /// <summary>
        /// Save feedback task
        /// </summary>
        /// <param name="task">Task entity</param>
        /// <param name="feedback">FeedBack string</param>
        public void SaveTaskFeedback(RotationTask task, string feedback)
        {
            task.Feedback = feedback;
            _repositories.RotationTaskRepository.Update(task);
            _repositories.Save();
        }

        /// <summary>
        /// Complete rotaion task
        /// </summary>
        /// <param name="task">Task entity</param>
        /// <param name="feedBack">FeedBack string</param>
        public void CompleteTask(RotationTask task, string feedBack)
        {
            if (task.IsComplete)
                return;

            _completeCore.CompleteTask(task, feedBack);

            //------- notification 17 ----------
            if (task.SearchTags == "Shared" && task.RotationTopic.ShareSourceTopicId.HasValue)
            {
                string personName = task.RotationTopic.RotationTopicGroup.RotationModule.Rotation != null
                                    ? task.RotationTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwner.FullName
                                    : task.RotationTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwner.FullName;

                var notifyRecipientId = task.RotationTopic.ShareSourceTopic.RotationTopicGroup.RotationModule.Rotation != null
                                        ? task.RotationTopic.ShareSourceTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwner.Id
                                        : task.RotationTopic.ShareSourceTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwner.Id;

                _logicCore.NotificationCore.NotificationTrigger.Send_OneReadSharedInfoAndComplete(notifyRecipientId,
                                                                                                    task.Id,
                                                                                                    personName);
            }
        }



        public void UpdateReceivedTask(Guid currentUserId, Guid taskId, Guid? assignedToId, DateTime? taskDeadline, TaskPriority taskPriority)
        {
            RotationTask task = this.GetTask(taskId);

            UserProfile currentUser = _logicCore.UserProfileCore.GetUser(currentUserId);

            var firstTaskOwner = this.GetOwnerForTask(task);

            if (taskDeadline.HasValue && taskDeadline.Value != task.Deadline)
            {
                task.Deadline = taskDeadline.Value;

                _logicCore.RotationTaskLogCore.AddMessageToTaskLog(task, $"<strong>{currentUser.FullName}</strong> changed the due date to " + taskDeadline.Value.FormatWithMonth() + ".");


                _logicCore.NotificationCore.NotificationTrigger.Send_TaskDueDateIsChanged(firstTaskOwner.Id,
                                                                                            task.AssignedTo.FullName,
                                                                                            task.AncestorTaskId.Value);
            }

            if (taskPriority != (TaskPriority)task.Priority)
            {
                task.Priority = (PriorityOfTask)taskPriority;

                _logicCore.RotationTaskLogCore.AddMessageToTaskLog(task, $"<strong>{currentUser.FullName}</strong> changed the priority of the task to " + task.Priority.GetEnumDescription() + ".");
            }



            if (assignedToId.HasValue && assignedToId != task.AssignedToId)
            {
                var taskOwner = _logicCore.UserProfileCore.GetUser(task.AssignedToId.Value);

                task.AssignedToId = assignedToId;
                task.TaskBoardTaskType = TaskBoardTaskType.Draft;
                task.RotationTopic = null;
                task.RotationTopicId = null;
                task.TaskBoardId = taskOwner.Id;


                var handoveredTask = task.Clone();
                handoveredTask.TaskBoardTaskType = TaskBoardTaskType.Received;
                handoveredTask.TaskBoard = null;
                handoveredTask.TaskBoardId = assignedToId;
                handoveredTask.AssignedTo = null;
                handoveredTask.AssignedToId = assignedToId;
                handoveredTask.RotationTopicId = null;
                handoveredTask.RotationTopic = null;

                task.SuccessorTaskId = handoveredTask.Id;

                _repositories.RotationTaskRepository.Add(handoveredTask);
                _repositories.RotationTaskRepository.Update(task);
                _repositories.Save();

                _logicCore.RotationTaskLogCore.AddMessageToTaskLog(task, $"<strong>{taskOwner.FullName}</strong> reassigned the task to <strong>{task.AssignedTo.FullName}" + "</strong>.");

                _logicCore.NotificationCore.NotificationTrigger.Send_ReceivedTaskWasReassigned(firstTaskOwner.Id,
                                                                                                taskOwner.FullName,
                                                                                                task.AncestorTaskId.Value);

            }

            this.UpdateParentAssigneeAndDeadline(task);
        }


        public void UpdateRotationReceivedTask(Guid currentUserId, Guid taskId, Guid? assignedToId, DateTime? taskDeadline, TaskPriority taskPriority)
        {
            RotationTask task = this.GetTask(taskId);


            UserProfile currentUser = _logicCore.UserProfileCore.GetUser(currentUserId);

            var firstTaskOwner = this.GetOwnerForTask(task);


            if (taskDeadline.HasValue && taskDeadline.Value != task.Deadline)
            {
                task.Deadline = taskDeadline.Value;

                _logicCore.RotationTaskLogCore.AddMessageToTaskLog(task, $"<strong>{currentUser.FullName}</strong> changed the due date to " + taskDeadline.Value.FormatWithMonth() + ".");


                _logicCore.NotificationCore.NotificationTrigger.Send_TaskDueDateIsChanged(firstTaskOwner.Id,
                                                                                            task.AssignedTo.FullName,
                                                                                            task.AncestorTaskId.Value);

            }

            if (taskPriority != (TaskPriority)task.Priority)
            {
                task.Priority = (PriorityOfTask)taskPriority;

                _logicCore.RotationTaskLogCore.AddMessageToTaskLog(task, $"<strong>{currentUser.FullName}</strong> changed the priority of the task to " + task.Priority.GetEnumDescription() + ".");
            }


            if (assignedToId.HasValue && assignedToId != task.AssignedToId)
            {
                var taskOwner = _logicCore.UserProfileCore.GetUser(task.AssignedToId.Value);

                task.AssignedToId = assignedToId;
                task.TaskBoardTaskType = TaskBoardTaskType.Draft;
                task.RotationTopic = null;
                task.RotationTopicId = null;
                task.TaskBoardId = taskOwner.Id;


                var handoveredTask = task.Clone();
                handoveredTask.TaskBoardTaskType = TaskBoardTaskType.Received;
                handoveredTask.TaskBoard = null;
                handoveredTask.TaskBoardId = assignedToId;
                handoveredTask.AssignedTo = null;
                handoveredTask.AssignedToId = assignedToId;
                handoveredTask.RotationTopicId = null;
                handoveredTask.RotationTopic = null;

                task.SuccessorTaskId = handoveredTask.Id;

                _repositories.RotationTaskRepository.Add(handoveredTask);
                _repositories.RotationTaskRepository.Update(task);
                _repositories.Save();

                _logicCore.RotationTaskLogCore.AddMessageToTaskLog(task, $"<strong>{taskOwner.FullName}</strong> reassigned the task to <strong>{task.AssignedTo.FullName}" + "</strong>.");

                _logicCore.NotificationCore.NotificationTrigger.Send_ReceivedTaskWasReassigned(firstTaskOwner.Id,
                                                                                                taskOwner.FullName,
                                                                                                task.AncestorTaskId.Value);
            }

            this.UpdateParentAssigneeAndDeadline(task);
        }

        private void UpdateParentAssigneeAndDeadline(RotationTask task)
        {
            if (task.AncestorTaskId.HasValue)
            {
                var parentTask = task.AncestorTask;

                while (parentTask != null)
                {
                    parentTask.AssignedToId = task.AssignedToId;
                    parentTask.Deadline = task.Deadline;
                    parentTask.Priority = task.Priority;

                    _repositories.RotationTaskRepository.Update(parentTask);

                    parentTask = parentTask.AncestorTask;
                }
            }

            _repositories.Save();

        }

        public Guid? GetRotationModuleOwnerId(RotationModule rotationModule)
        {
            Guid? assignedFrom = null;

            if (rotationModule.RotationId.HasValue)
            {
                assignedFrom = rotationModule.Rotation.RotationOwnerId;
            }
            else if (rotationModule.ShiftId.HasValue)
            {
                assignedFrom = rotationModule.Shift.Rotation.RotationOwnerId;
            }

            return assignedFrom;
        }


    }


}


