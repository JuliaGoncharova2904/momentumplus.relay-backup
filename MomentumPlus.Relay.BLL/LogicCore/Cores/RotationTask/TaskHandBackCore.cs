﻿using System;
using System.Linq;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class TaskHandBackCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;


        public TaskHandBackCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public RotationTask GetOrCreateHandBackTask(RotationTopic topic)
        {
            var handBackTask = GetHandBackTask(topic) ??
                            AddHandBackTask(topic);

            return handBackTask;
        }

        public RotationTask GetHandBackTask(RotationTopic topic)
        {
            var existingTopicHandBackTask = _repositories.RotationTaskRepository.Find(t => t.RotationTopicId == topic.Id
                                                                                        && t.Name == "Handback").FirstOrDefault();

            return existingTopicHandBackTask;
        }



        public RotationTask AddHandBackTask(RotationTopic topic)
        {
            var draftTopic = topic.AncestorTopic;

            DateTime deadline = DateTime.Today;
            string destTaskDescription = "{0} has requested information back regarding this item. Please add notes in the Handback notes section.";
            if (topic.RotationTopicGroup.RotationModule.RotationId.HasValue)
            {
                destTaskDescription = string.Format(destTaskDescription, topic.RotationTopicGroup.RotationModule.Rotation.DefaultBackToBack.FirstName);
                deadline = _logicCore.RotationCore.RotationExpiredDate(topic.RotationTopicGroup.RotationModule.Rotation).AddDays(-1);
            }
            else if (topic.RotationTopicGroup.RotationModule.ShiftId.HasValue)
            {
                destTaskDescription = string.Format(destTaskDescription, topic.RotationTopicGroup.RotationModule.Shift.Rotation.DefaultBackToBack.FirstName);
                Shift shift = topic.RotationTopicGroup.RotationModule.Shift;
                deadline = shift.StartDateTime.HasValue ? shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes) : shift.CreatedUtc.Value;
            }
            else
            {
                destTaskDescription = string.Format(destTaskDescription, "Sender");
            }

            RotationTask draftTask = null;

            if (draftTopic != null)
            {
                draftTask = new RotationTask
                {
                    Id = Guid.NewGuid(),
                    Enabled = draftTopic.Enabled,
                    ForkCounter = 0,
                    RotationTopicId = draftTopic.Id,
                    Description = "You have requested to be updated on this item. Open the task to view the handback notes.",
                    Name = "Handback",
                    AssignedToId = draftTopic.AssignedToId,
                    IsNullReport = draftTopic.IsNullReport,
                    TemplateTaskId = null,
                    FinalizeStatus = draftTopic.FinalizeStatus,
                    IsFeedbackRequired = draftTopic.IsFeedbackRequired,
                    IsPinned = false,
                    ManagerComments = null,
                    SearchTags = "Handback",
                    Deadline = deadline,
                    IsComplete = false,
                    Priority = PriorityOfTask.Normal
                };

                _repositories.RotationTaskRepository.Add(draftTask);
                _repositories.Save();
            }



            var recivedTask = new RotationTask
            {
                Id = Guid.NewGuid(),
                Enabled = topic.Enabled,
                ForkCounter = 0,
                RotationTopicId = topic.Id,
                Description = destTaskDescription,
                Name = "Handback",
                AncestorTaskId = draftTask?.Id,
                ParentTaskId = draftTask?.Id,
                AssignedToId = topic.AssignedToId,
                IsNullReport = topic.IsNullReport,
                TemplateTaskId = null,
                FinalizeStatus = topic.FinalizeStatus,
                IsFeedbackRequired = topic.IsFeedbackRequired,
                IsPinned = false,
                ManagerComments = null,
                SearchTags = "Handback",
                Deadline = deadline,
                IsComplete = false,
                Priority = PriorityOfTask.Normal
            };


            if (draftTask != null)
            {
                draftTask.SuccessorTaskId = recivedTask.Id;

                _repositories.RotationTaskRepository.Update(draftTask);
            }
            _repositories.RotationTaskRepository.Add(recivedTask);
            _repositories.Save();

            return recivedTask;

        }

    }
}
