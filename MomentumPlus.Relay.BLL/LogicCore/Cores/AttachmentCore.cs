﻿using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class AttachmentCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public AttachmentCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        /// <summary>
        /// Return attachment entity by id.
        /// </summary>
        /// <param name="attachmentId">Attachment ID</param>
        /// <returns></returns>
        public Attachment GetAttachment(Guid attachmentId)
        {
            Attachment attachment = _repositories.AttachmentRepository.Get(attachmentId);

            if (attachment == null)
                throw new ObjectNotFoundException(string.Format("Attachment with ID: {0} was not found.", attachmentId));

            return attachment;
        }

        /// <summary>
        /// Add attachment entity.
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="fileName">File name</param>
        /// <param name="file">File entity</param>
        /// <returns></returns>
        public Attachment AddAttachment(string name, string fileName, File file)
        {
            Attachment attachment = new Attachment
            {
                Id = Guid.NewGuid(),
                Name = name,
                FileName = fileName,
                File = file,
                FileId = file.Id
            };

            _repositories.AttachmentRepository.Add(attachment);
            _repositories.Save();

            return attachment;
        }

        /// <summary>
        /// Remove attachment with relations.
        /// </summary>
        /// <param name="attachment">Attachment entity</param>
        public void RemoveAttachment(Attachment attachment)
        {
            File file = attachment.File;

            IEnumerable<Attachment> attachments = _repositories.AttachmentRepository
                                                                .Find(a => a.FileId == attachment.FileId)
                                                                .ToList();

            _repositories.AttachmentRepository.Delete(attachments);
            _repositories.FileRepository.Delete(file);
            _repositories.Save();
        }
    }
}
