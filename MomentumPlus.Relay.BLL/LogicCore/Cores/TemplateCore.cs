﻿using System;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class TemplateCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public TemplateCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public Template GetTemplate(Guid templateId)
        {
            var template = _repositories.TemplateRepository.Get(templateId);

            _logicCore.ModuleCore.InitTemplateModules(templateId);

            return template;
        }

        public Template GetRotationTemplate(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            var templateId = rotation.RotationOwner.Position.Template.Id;
            Template template = this.GetTemplate(templateId);

            return template;
        }


        public Template GetShiftTemplate(Guid shiftId)
        {
            var shift = _logicCore.ShiftCore.GetShift(shiftId);

            var template = shift.Rotation.RotationOwner.Position.Template;

            return template;

        }

    }
}
