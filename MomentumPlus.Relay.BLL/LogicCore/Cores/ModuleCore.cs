﻿using MomentumPlus.Core.Interfaces;
using System;
using System.Linq;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class ModuleCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public ModuleCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public TemplateModule GetModule(Guid moduleId)
        {
            var module = _repositories.TemplateModuleRepository.Get(moduleId);

            return module;
        }

        public Guid GetBaseModuleIdFromType(ModuleType type)
        {
            var module = _repositories.TemplateModuleRepository.Find(m => m.Type == (TypeOfModule)type && !m.ParentModuleId.HasValue).FirstOrDefault();

            return module.Id;
        }

        public TemplateModule GetBaseModuleFromType(ModuleType type)
        {
            var module = _repositories.TemplateModuleRepository.Find(m => m.Type == (TypeOfModule)type && !m.ParentModuleId.HasValue).FirstOrDefault();

            return module;
        }


        IQueryable<TemplateModule> GetBaseModules()
        {
            var modules = _repositories.TemplateModuleRepository.Find(m => !m.ParentModuleId.HasValue && !m.DeletedUtc.HasValue);

            return modules;
        }

        public TemplateModule GetTemplateModule(TypeOfModule type, Guid templateId)
        {
            var module = _repositories.TemplateModuleRepository.Find(m => m.TemplateId == templateId && m.Type == type && !m.DeletedUtc.HasValue).FirstOrDefault();

            return module;
        }

        public IQueryable<TemplateModule> GetTemplateModules(Guid templateId)
        {
            var modules = _repositories.TemplateModuleRepository.Find(m => m.TemplateId == templateId && !m.DeletedUtc.HasValue);

            return modules;
        }


        public int EnabledTopicsGroupsForModuleCounter(Guid baseModuleId, Guid templateId)
        {
            var baseModule = GetModule(baseModuleId);

            var templateModule = GetOrCreateTemplateModule(templateId, baseModule);
            if (templateModule == null || templateModule.TopicGroups == null)
            {
                return 0;
            }

            return templateModule.TopicGroups.Count(tg => tg.Enabled);
        }


        public bool CheckChildModuleStatus(Guid baseModuleId, Guid templateId)
        {
            var baseModule = GetModule(baseModuleId);

            var templateModule = GetOrCreateTemplateModule(templateId, baseModule);

            _logicCore.RotationModuleCore.PopulateRotationsModules(templateModule);

            return templateModule.Enabled;
        }

        public TemplateModule AddChildModule(TemplateModule baseModule, Guid templateId, bool status = true)
        {
            var module = new TemplateModule
            {
                Id = Guid.NewGuid(),
                Enabled = status,
                Type = baseModule.Type,
                ParentModuleId = baseModule.Id,
                TemplateId = templateId,
                TopicGroups = null
            };

            _repositories.TemplateModuleRepository.Add(module);
            _repositories.Save();

            return module;
        }

        /// <summary>
        /// Get Or Create Template Module From Base Module
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="baseModule"></param>
        /// <returns></returns>

        public TemplateModule GetOrCreateTemplateModule(Guid templateId, TemplateModule baseModule)
        {
            var templateModule = GetTemplateModule(baseModule.Type, templateId) ??
                                 AddChildModule(baseModule, templateId);

            return templateModule;
        }


        public void InitTemplateModules(Guid templateId)
        {
            var baseModules = GetBaseModules();


            foreach (var baseModule in baseModules.ToList())
            {
                GetOrCreateTemplateModule(templateId, baseModule);
            }
        }


    }
}