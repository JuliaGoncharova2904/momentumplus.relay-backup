﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class RotationTopicSharingRelationCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public RotationTopicSharingRelationCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }


        /// <summary>
        /// Return Sharing Topic Relation by Id or generate exception (ObjectNotFoundException)
        /// </summary>
        /// <param name="RotationTopicSharingRelationId"></param>
        /// <returns></returns>
        public RotationTopicSharingRelation GetRotationTopicSharingRelation(Guid RotationTopicSharingRelationId)
        {
            RotationTopicSharingRelation RotationTopicSharingRelation = _repositories.TopicSharingRelationRepository.Get(RotationTopicSharingRelationId);

            if (RotationTopicSharingRelation == null)
                throw new ObjectNotFoundException(string.Format("Sharing Topic Relation with Id: {0} was not found.", RotationTopicSharingRelationId));

            return RotationTopicSharingRelation;
        }


        public IQueryable<RotationTopicSharingRelation> GetRotationTopicSharingRelations(Guid topicId)
        {
            var topicRelations = _repositories.TopicSharingRelationRepository.Find(r => r.SourceTopicId == topicId);

            return topicRelations;
        }

        public void AddRotationTopicSharingRelation(RotationTopicSharingRelation RotationTopicSharingRelation, bool save = true)
        {
            RotationTopicSharingRelation.Id = Guid.NewGuid();

            _repositories.TopicSharingRelationRepository.Add(RotationTopicSharingRelation);

            if (save)
            {
                _repositories.Save();
            }
        }


        public void ChangeShareTopicRelations(RotationTopic topic, IEnumerable<Guid> recipientsIds, IEnumerable<Guid> deleteShareTopicRelationIds, bool save = true)
        {
            if (recipientsIds != null && recipientsIds.Any())
            {
                foreach (var recipientId in recipientsIds)
                {
                    this.AddShareTopicRelation(topic, recipientId, false);
                }
            }

            if (deleteShareTopicRelationIds != null && deleteShareTopicRelationIds.Any())
            {
                foreach (var deleteShareTopicRelationId in deleteShareTopicRelationIds)
                {
                    var shareTopicRelation = _logicCore.RotationTopicSharingRelationCore.GetRotationTopicSharingRelation(deleteShareTopicRelationId);

                    this.DeleteShareTopicRelation(shareTopicRelation, false);
                }
            }

            if (save)
            {
                _repositories.Save();
            }
        }

        public void AddShareTopicRelation(RotationTopic topic, Guid recipientId, bool save = true)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUser(recipientId);
            Rotation destRotation = user.CurrentRotation;

            if (destRotation == null)
                return;

            if (destRotation.State == RotationState.SwingEnded || destRotation.State == RotationState.Expired)
            {
                destRotation = _logicCore.RotationCore.CreateNextRotation(user);
            }

            if( destRotation.RotationModules.SelectMany(m => m.RotationTopicGroups)
                                                  .SelectMany(tg => tg.RotationTopics)
                                                  .Any(t => t.ShareSourceTopicId == topic.Id))
            {
                return;
            }
      

            RotationTopic sourceTopic = topic;
            RotationTopicGroup sourceTopicGroup = sourceTopic.RotationTopicGroup;
            RotationModule sourceModule = sourceTopicGroup.RotationModule;

            RotationModule destModule = _logicCore.RotationModuleCore.GetOrCreateRotationModule(destRotation.Id, sourceModule.Type, TypeOfModuleSource.Received);
            RotationTopicGroup destTopicGroup = _logicCore.RotationTopicGroupCore.GetOrCreateChildTopicGroup(destModule.Id, sourceTopicGroup);
            RotationTopic destTopic = _logicCore.RotationTopicCore.GetOrCreateChildTopic(destTopicGroup.Id, sourceTopic, StatusOfFinalize.AutoFinalized);

            destTopic.ShareSourceTopicId = sourceTopic.Id;

            _repositories.RotationTopicRepository.Update(destTopic);

            _logicCore.RotationTaskCore.AddShareTask(destTopic);


            _logicCore.NotificationCore.NotificationTrigger.Send_OneSharedInformationWithYou(recipientId);

            this.AddRotationTopicSharingRelation(new RotationTopicSharingRelation
            {
                Id = new Guid(),
                SourceTopicId = topic.Id,
                RecipientId = recipientId,
                RecipientEmail = null,
                DestinationTopicId = destTopic.Id
            });


            if (save)
            {
                _repositories.Save();
            }
        }


        public void DeleteShareTopicRelation(RotationTopicSharingRelation shareTopicRelation, bool save = true)
        {
            if (shareTopicRelation.DestinationTopicId.HasValue)
            {
                _logicCore.RotationTopicCore.RemoveTopic(shareTopicRelation.DestinationTopic);
            }

            _repositories.TopicSharingRelationRepository.Delete(shareTopicRelation.Id);

            if (save)
            {
                _repositories.Save();
            }
        }

        public void DeleteRotationTopicSharingRelations(RotationTopic topic, bool save = true)
        {
            var topicRelations = topic.TopicSharingRelations.ToList();

            if (topicRelations != null && topicRelations.Any())
            {
                foreach (var topicRelation in topicRelations)
                {
                    this.DeleteShareTopicRelation(topicRelation, save);
                }
            }

        }

        public void DeleteDestinationRotationTopicSharingRelations(RotationTopic topic, bool save = true)
        {
            var topicRelations = _repositories.TopicSharingRelationRepository.Find(r => r.DestinationTopicId.HasValue && r.DestinationTopic.Id == topic.Id).ToList();

            foreach (var topicRelation in topicRelations)
            {
                _logicCore.RotationTopicCore.RemoveTopic(topicRelation.DestinationTopic);

                _repositories.Save();
            }
        }


    }
}
