﻿using System;
using System.Data;
using System.IO;
using System.Xml;
using MomentumPlus.Relay.Configuration.Configurations.Jobs;

namespace MomentumPlus.Relay.Configuration
{
    /// <summary>
    /// All configuration from 'Configuration folder'
    /// </summary>
    public class RelayConfig
    {

        #region Singleton

        /// <summary>
        /// Instance of RelayConfig
        /// </summary>
        private static RelayConfig _relayConfig;

        /// <summary>
        /// Get Instance of RelayConfig
        /// </summary>
        /// <returns>Instance of RelayConfig</returns>
        public static RelayConfig GetConfig()
        {
            if (_relayConfig == null)
                _relayConfig = new RelayConfig();

            return _relayConfig;
        }

        #endregion

        #region Fields

        /// <summary>
        /// Configuration for task module
        /// </summary>
        public JobsConfig JobsConfig { get; private set; }

        #endregion


        /// <summary>
        /// Constructor
        /// </summary>
        private RelayConfig()
        {
            XmlDocument xDoc = GetConfigXml();

            if (xDoc != null)
            {
                this.JobsConfig = new JobsConfig(xDoc.SelectSingleNode("//jobsConfig"));
            }
        }

        /// <summary>
        /// Get configuration XML from all Config files
        /// </summary>
        /// <returns>Configuration XML</returns>
        private XmlDocument GetConfigXml()
        {
            DataSet ds = new DataSet();
            XmlDocument xDoc = new XmlDocument();

            string configFolderPath = AppDomain.CurrentDomain.BaseDirectory + "Configuration";

            if (!Directory.Exists(configFolderPath))
                return null;

            string[] allConfigFiles = Directory.GetFiles(configFolderPath, "*.config", SearchOption.AllDirectories);

            xDoc.Load(allConfigFiles[0]);

            return xDoc;
        }
    }
}
