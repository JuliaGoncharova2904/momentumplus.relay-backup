﻿using System.Xml;

namespace MomentumPlus.Relay.Configuration.Configurations.Jobs
{
    /// <summary>
    /// Job configuration
    /// </summary>
    public class Job
    {
        public string Id { get; private set; }
        public string Assembly { get; private set; }
        public string Class { get; private set; }
        public string Method { get; private set; }
        public string Cron { get; private set; }
        public CronProvider CronProvider { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="job">One task configuration XmlNode</param>
        internal Job(XmlNode job)
        {
            this.Id = job.Attributes["id"].Value;
            this.Assembly = job.Attributes["assembly"].Value;
            this.Class = job.Attributes["class"].Value;
            this.Method = job.Attributes["method"].Value;
            this.Cron = job.Attributes["cron"] != null ? job.Attributes["cron"].Value : null;

            if(this.Cron == null)
            {
                XmlNode cronProvider = job.SelectSingleNode("cronProvider");
                if (cronProvider != null)
                    this.CronProvider = new CronProvider(cronProvider);
            }
        }
    }
}
