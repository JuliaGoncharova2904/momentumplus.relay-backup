﻿using System.Xml;

namespace MomentumPlus.Relay.Configuration.Configurations.Jobs
{
    /// <summary>
    /// Cron Provider Configuration
    /// </summary>
    public class CronProvider
    {
        public string Type { get; private set; }
        public string Assembly { get; private set; }
        public string Class { get; private set; }
        public string Method { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="cronProvider">Cron Provider configuration XmlNode</param>
        internal CronProvider(XmlNode cronProvider)
        {
            this.Type = cronProvider.Attributes["type"].Value;
            this.Assembly = cronProvider.Attributes["assembly"].Value;
            this.Class = cronProvider.Attributes["class"].Value;
            this.Method = cronProvider.Attributes["method"].Value;
        }
    }
}
