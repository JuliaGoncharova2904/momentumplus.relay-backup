﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace MomentumPlus.Relay.Configuration.Configurations.Jobs
{
    public class JobsConfig
    {
        public IEnumerable<Job> Jobs { get; private set; }

        public event Action ReloadHandler;

        public void ReloadConfiguration()
        {
            if (ReloadHandler != null)
                ReloadHandler();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="jobsNode">Jobs XmlNode from common configuration</param>
        internal JobsConfig(XmlNode jobsNode)
        {
            this.Jobs = new List<Job>();

            if (jobsNode != null)
            {
                XmlNodeList jobNodes = jobsNode.SelectNodes("//job");

                foreach (XmlNode job in jobNodes)
                {
                    (this.Jobs as List<Job>).Add(new Job(job));
                }
            }
        }
    }
}
